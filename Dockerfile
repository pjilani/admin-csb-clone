# Stage 1
FROM node:16-alpine as builder

WORKDIR /app

COPY package.json .

RUN npm i --legacy-peer-deps && npm cache clean --force

COPY . .

RUN npm run build

# Stage 2
FROM nginx:alpine

COPY --from=builder /app/dist /var/www/

COPY default.conf /etc/nginx/conf.d/default.conf