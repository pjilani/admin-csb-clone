import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthorizationTokenHandler, ErrorHandler } from './helpers';
import { SharedModule } from './shared/shared.module';

import { NgProgressModule } from 'ngx-progressbar';
import { NgProgressHttpModule } from 'ngx-progressbar/http';
// import { NgProgressRouterModule } from 'ngx-progressbar/router';

@NgModule({

  declarations: [
    AppComponent
  ],

  imports: [
    BrowserModule,
    SharedModule,
    AppRoutingModule,
    NgProgressModule.withConfig({
      spinnerPosition: "right",
      color: "#f71cff"
    }),
    NgProgressHttpModule,
    // NgProgressRouterModule
  ],

  providers: [ ErrorHandler, AuthorizationTokenHandler ],

  bootstrap: [AppComponent]

})

export class AppModule { }
