import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from './../../environments/environment';
import { AdminAuthService } from '../modules/admin/services/admin-auth.service';
import { SuperAdminAuthService } from '../modules/super-admin/services/super-admin-auth.service';
import { generateApiUrl } from '../services/utils.service';

@Injectable()
export class AuthorizationHeaderInterceptor implements HttpInterceptor {
  constructor(
    private adminAuthService: AdminAuthService,
    private superAdminAuthService: SuperAdminAuthService
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
    const AdminAuthorization = 'Bearer ' + this.adminAuthService.adminTokenValue || '';
    const SuperAdminAuthorization = 'Bearer ' + this.superAdminAuthService.superAdminTokenValue || '';

    let Authorization = '';

    if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      Authorization = SuperAdminAuthorization;
    } else {
      Authorization = AdminAuthorization;
    }
    
    let api = generateApiUrl();

    if (Authorization && api) {
      request = request.clone({ setHeaders: { Authorization } });
    }

    request = request.clone({
      url: api + request.url
    });

    return next.handle(request);
  }
}

export const AuthorizationTokenHandler = {
  provide: HTTP_INTERCEPTORS,
  useClass: AuthorizationHeaderInterceptor,
  multi: true
};
