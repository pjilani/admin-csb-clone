import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { Router } from '@angular/router';
import { AdminAuthService } from './../modules/admin/services/admin-auth.service';
import { SuperAdminAuthService } from './../modules/super-admin/services/super-admin-auth.service';

declare const $: any;
declare const toastr: any;

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(
    private router: Router,
    private adminAuthService: AdminAuthService,
    private superAdminAuthService: SuperAdminAuthService,
  ) { }

  private logoutInitiated = false;

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(map((res) => {
      return res;
    }), catchError(err => {

      $('#preloader').css('height', 0);
      $('#preloader img').hide();

     

      let errorMessaege = '';
        
      if(err.status === 403) {
        this.adminAuthService.navigateToLogin();
      }


      if (err.status === 401) {
        const superAdminToken = this.superAdminAuthService.superAdminTokenValue;

        if (superAdminToken && superAdminToken.length > 0) {
          this.superAdminAuthService.navigateToLogin('Please login again!');
        } else {
          this.adminAuthService.navigateToLogin('Please login again!');
        }

      } else if (err.status === 404) {
        /* Please don't uncomment  code when api found 404 we are not redirect UI on 404*/
        // this.router.navigate(['/404']);
        //  console.log('Error Log', err);
      } else if(err.status === 429) {
        location.reload();
      }

      if(err.status === 301){
        this.router.navigate([''])
      }

      // console.log('Error Log', err);
      try {
        if(err?.error?.message) {
          errorMessaege += err.error.message;
        }
        if(err?.error?.record) {

          for(let e in err.error.record) {
            for(let em of err.error.record[e]) {
              // errmsg.concat(' '+em);
              errorMessaege +=' '+em;
            }
          }

        }
      }
      catch(err) {
        // console.log('Error Log', err);
      }
      
      errorMessaege = errorMessaege.trim() || err?.statusText;
      toastr.options = {
        "preventDuplicates": true,
        "preventOpenDuplicates": true
      };
      toastr.error(errorMessaege);

      return throwError(err);
    }));
  }
}

export const ErrorHandler = {
  provide: HTTP_INTERCEPTORS,
  useClass: ErrorInterceptor,
  multi: true
};
