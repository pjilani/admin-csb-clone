import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { SuperAdminAuthService } from '../modules/super-admin/services/super-admin-auth.service';

@Injectable({ providedIn: 'root' })

export class SuperAdminAuthGuard implements CanActivate, CanActivateChild {
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private superAdminAuthService: SuperAdminAuthService
  ) { }
    
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const superAdminToken = this.superAdminAuthService.superAdminTokenValue;
    
    if (superAdminToken && superAdminToken.length > 0) {
      let role = localStorage.getItem('role'); 
      if(role == 'manager' && (!state.url.includes('tenants'))){
        this.router.navigate(['/super-admin/tenants']);
        return true;
      }
      return true; 
    } else {
      // not logged in so redirect to login page with the return url
      this.router.navigate(['/'], { queryParams: { returnUrl: state.url } });
      return false;
    }
  }

       
  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean>|Promise<boolean>|boolean {
    return this.canActivate( route, state);
  }

    

}
