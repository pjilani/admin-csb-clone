export interface socialMedia {
    id: number;
    name: string;
    redirect_url: string;
    image: string;
    tenant_id: number;
    status:boolean;
    created_at: string;
    updated_at: string;
}
