export interface Transaction {
    id: number;
    _id: number;
    _source:any
    created_at: string;
    updated_at: string;
    actionee_id: number;
    actionee_type: string;
    amount: string;
    bet_type_id: number;
    comments: string;
    conversion_rate: string;
    credit_index: null
    debit_transaction_id: string;
    error_code: number;
    error_description: string;
    game_id: number;
    is_end_round: null
    payment_method: string;
    platform_id: null
    return_reason: null
    round_id: number;
    seat_id: null
    server_id: null
    source_after_balance: string;
    source_before_balance: string;
    source_currency_id: null
    source_wallet_id: null
    target_wallet_name: string
    target_wallet_name_admin: string
    status: string;
    success: null
    table_id: number;
    target_after_balance: string;
    target_before_balance: string;
    target_currency_id: null
    target_wallet_id: number;
    source_wallet_name: string;
    source_wallet_name_admin: string;
    tenant_id: number;
    timestamp: null
    transaction_id: string;
    transaction_type: number;
    actionee_name: string;
    source_wallet_name_super_admin:string;
    target_wallet_name_super_admin:string;

}
