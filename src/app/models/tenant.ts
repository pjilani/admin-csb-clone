export interface Tenant {
    id: number;
    name: string;
    active: boolean;
    created_at: string;
    domain: string;
    updated_at: string;
    configurations?: any;
    setting?: any;
}
