export interface Banner {
    banner_type: string;
    created_at: string;
    enabled: boolean;
    id: number;
    image_url: string;
    name: string;
    order: number;
    redirect_url: string;
    tenant_id: number;
    updated_at: string;
    url: string;
    game_name: string;
    open_table: string;
    provider_name: string;
}
