export interface Admin {
    name: string;
    first_name: string;
    email: string;
    token: string;
    ip_whitelist:any;
    created_at: string;                         
    id: number;
    last_name: string;
    parent_email: string;
    parent_id: number;
    parent_type: string;
    remember_created_at: string;
    reset_password_sent_at: string;
    reset_password_token: string;
    updated_at: string;
    last_deposited_amount: string;
    
    agent_name: string;
    phone: number;
    phone_verified: string;
    active: boolean;
    currency_id: number;
    currency_name: any;
    roles: any;
    ip_whitelist_type:any;
    is_applied_ip_whitelist:any;
    encrypted_password: string;
    tenant_id: number;

    affiliate_token:string;

    amount: string;
    non_cash_amount: string;
    owner_id: number;
    owner_type: string;
    primary: boolean;
    wallet_id: number;
    wallet_updated_at: string;
    rolesids: any;
    setting: any;
    tenant: any;
    kyc_regulated: boolean;
    wallets: any[];
    notification: any[];

    role_name: any;
    
    admin_role_id: null
    confirmation_sent_at: null
    confirmation_token: null
    confirmed_at: null
    deactivated_at: null
    deactivated_by_id: null
    deactivated_by_type: null
    unconfirmed_email: null
    timezone:string
    agent_type:number
    permissions:any | undefined,
    permissionroleid:any
    permissionRoleName:any

}
