export interface PermissionRole {
    id: number;
    role_type:number;
    permission: string;
    role_name: string;
    tenant_id: string;
    created_at: string;
    updated_at: string;
}
