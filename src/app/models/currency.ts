export interface Currency {
    code: string;
    created_at: string;
    exchange_rate: string;
    internal_code:string,
    id: number;
    name: string;
    primary: boolean;
    updated_at: string;
}
