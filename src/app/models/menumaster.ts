export interface MenuMaster {
    id: number;
    name: string;
    path: string;
    active: boolean;
    created_at: string;
    updated_at: string;

}
