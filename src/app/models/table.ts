export interface Table {
    casino_providers_name: string;
    created_at: string;
    game_id: string;
    id: number;
    name: string;
    updated_at: string;
    game_name: string;
    is_lobby: boolean;
    table_id: string;
}
