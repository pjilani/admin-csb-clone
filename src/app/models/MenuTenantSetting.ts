export interface MenuTenantSetting {
    id: number;
    menu_name: string;
    menu_path: string;
    ordering: number;
    tenant_id:number;
    menu_id:number;
    created_at: string;
    updated_at: string;

}