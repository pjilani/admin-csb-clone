export interface Deposite {
    max_deposit: any;
    code: string;
    created_at: string;
    currency_id: number;
    currency_name: string;
    enabled: boolean;
    id: number;
    image: string;
    kind: string;
    percentage: string;
    promotion_title: string;
    terms_and_conditions: string;
    updated_at: string;
    usage_count: number;
    valid_from: string;
    valid_upto: string;
    vip_levels: string;
    total_used: number;

    max_bonus: string;
    max_rollover_per_bet: string;
    min_deposit: string;
    rollover_multiplier: number;
    tenant_id: number;
    valid_for_days: number;
    setting: any;
    min_losing_amount:any;
    losing_percentage:any;
    claim_days:any;
}
