export interface User {
    name: string;
    first_name: string;
    email: string;
    token: string;
    is_verified: boolean;
    active: boolean;
    amount: string;
    currency_id: number;
    nick_name: string;
    user_name: string;
    currency_code: string;
    wallet_id: number;
    wallet_updated_at: string;
    non_cash_amount: string;
    email_verified:boolean;
    nocash_bonus_last_amount: number;
    zip_code: number;
    role:string;
    total_amount:string;
    created_at: string;                         
    id: number;
    last_name: string;
    parent_id: number;
    parent_type: string;
    parent_email: string;
    remember_created_at: string;
    reset_password_sent_at: string;
    reset_password_token: string;
    updated_at: string;
    demo: boolean;
    kyc_done: boolean;
    city: string;

    bonus: any;
    country_code: string;
    date_of_birth: string;
    deleted_at: string;
    last_deposited_amount: string;
    phone: string;
    sign_in_count: number;
    tenant_id: number;
    vip_level: number;
    
    disabled_at: null
    disabled_by_type: null
    gender: null
    last_login_date: null
    locale: null
    phone_code: null
    phone_verified: null
    self_exclusion: null
    setting: any;
    sign_in_ip: null
    user_documents: any;
    wallets:any;
    ip:any;
    version:any;
    agentname:any;
    network:any;
    player_commission_max_percentage:any;

}
