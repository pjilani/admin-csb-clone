export enum Role {
    SuperAdmin = 'super-admin',
    Admin = 'owner',
    Agent = 'agent',
    Subadmin = 'sub-admin',
    DepositAdmin = 'deposit-admin',
    WithdrawalAdmin = 'withdrawal-admin',
    Player = 'player'
}
