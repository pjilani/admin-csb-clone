export interface Theme {
    id: number;
    options: string;
    name: string;
    created_at: string;
    updated_at: string;
}
