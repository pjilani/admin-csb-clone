export interface Item {
    active: boolean;
    created_at: string;
    featured: boolean;
    has_lobby: boolean;
    id: number;
    image: string;
    name: string;
    tenant_id: number;
    updated_at: string;
    uuid: string;

    
    has_free_spins: null
    is_mobile: null
    item_order: null
    item_type: null
    provider: null
    technology: null
}
