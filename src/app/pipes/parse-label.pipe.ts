import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "parseLabel",
})
export class ParseLabelPipe implements PipeTransform {
  transform(
    value: number | string | undefined | null,
    labels: { value: number | string; label: string }[]
  ): string {    
    // if (!value) return "-";

    const found = labels.find((label) => label.value == value);

    if (found) return found.label;

    return "-";
  }
}
