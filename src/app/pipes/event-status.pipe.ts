import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'event_status', pure: false })

export class EventStatusPipe implements PipeTransform {
  transform(value: number): any {
    switch (value) {
      case 1:
        return 'Not started';
      case 2:
        return 'In progress';
      case 3:
        return 'Finished';
      case 4:
        return 'Cancelled';
      case 5:
        return 'Postponed';
      case 6:
        return 'Interrupted';
      case 7:
        return 'Abandoned';
      case 8:
        return 'Coverage lost';
      case 9:
        return 'About to start';
      default:
        return 'Not started';
    }
  }
}
