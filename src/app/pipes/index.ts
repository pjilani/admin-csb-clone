export * from './truncate-string.pipe';
export * from './bet-status.pipe';
export * from './event-status.pipe';
export * from './safe-html.pipe';
export * from './hide-string.pipe';
export * from './capitalize.pipe';
export * from './parse-label.pipe'
