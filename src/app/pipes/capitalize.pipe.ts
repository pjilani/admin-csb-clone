import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalize'
})
export class CapitalizePipe implements PipeTransform {
  transform(value: string|any): string {
    // Replace underscores with spaces
    let modifiedString = value.replace(/_/g, ' ');

    // Capitalize the first letter of each word
    modifiedString = modifiedString.replace(/\b\w/g, (match:any) => match.toUpperCase());

    return modifiedString;
  }
}
