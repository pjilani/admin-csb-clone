import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'hideChars' })
  
export class hideChars implements PipeTransform {
  constructor() { }

  transform(value: any, minChars = 3): string {
     
    const numHideChar = value.length - minChars;

    const result = [...value].map((char, index) => (index >= numHideChar ? char : '*'));

    return result.join('');
 }
}
