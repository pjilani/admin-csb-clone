import { NgModule } from '@angular/core';
import { TruncateStringPipe, BetSettlementStatusPipe, EventStatusPipe, SafeHtml, hideChars , CapitalizePipe, ParseLabelPipe} from './index';
import { SafeUrlPipe } from './safe-url.pipe';

@NgModule({

  declarations: [ TruncateStringPipe, BetSettlementStatusPipe, EventStatusPipe, SafeHtml, hideChars, CapitalizePipe, ParseLabelPipe, SafeUrlPipe],
  
  exports: [ TruncateStringPipe, BetSettlementStatusPipe, EventStatusPipe, SafeHtml, hideChars, CapitalizePipe, ParseLabelPipe, SafeUrlPipe],

})

export class PipeModule { }
