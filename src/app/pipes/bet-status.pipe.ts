import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'bet_settlement_status', pure: false })

export class BetSettlementStatusPipe implements PipeTransform {
  transform(value: number): any {
    switch (value) {
      case -1:
        return 'Cancelled';
      case 1:
        return 'Lost';
      case 2:
        return 'Won';
      case 3:
        return 'Refund';
      case 4:
        return 'Halflost';
      case 5:
        return 'Halfwon';
      default:
        return 'In Game';
    }
  }
}
