import { Component } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { AdminAuthService } from './modules/admin/services/admin-auth.service';
import { SuperAdminAuthService } from './modules/super-admin/services/super-admin-auth.service';
import { BehaviorSubject } from 'rxjs';
import { Admin, User } from './models';

declare const $: any;

@Component({
  selector: 'app-root',
  template: `<ng-progress [hidden] = "hideLoader" [spinner]="false"></ng-progress>
              <router-outlet></router-outlet>`,
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  
  constructor(private router: Router, private adminAuthService: AdminAuthService, private superAdminAuthService: SuperAdminAuthService) { }
  hideLoader:boolean = false;
  ngOnInit(): void {
    this.router.events.subscribe(evt => {
      
      if (evt instanceof NavigationStart) {
        // console.log(this.router.url);
      }
      
      if (evt instanceof NavigationEnd) { 
        // console.log(this.router.url);
        this.hideLoader = false;
        // $("html, body").animate({ scrollTop: 0 }, "slow");
        $(window).scrollTop(0);
      }
      
    })
    this.initialize();
  }

  // this function sets permission, userData, tenantData in localstorage if token found.
  initialize(){
    if(this.adminAuthService.adminTokenValue){ 
      const permission = localStorage.getItem('permissions');
      if(!permission){
        //permission api call
        this.adminAuthService.getPermissions().subscribe();
      }else{
        this.adminAuthService.adminPermissions = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('permissions') || '{}'))
      }

      const user = localStorage.getItem('userData');

      if(!user){
        //user api call
        this.adminAuthService.getPersonalDetails().subscribe()
      }else{
        this.adminAuthService.adminUserSubject = new BehaviorSubject<Admin>(JSON.parse(localStorage.getItem('userData') || '{}'));
        this.adminAuthService.adminUser = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('userData') || '{}'));
      }

      const tenant = localStorage.getItem('tenantData');

      if(!tenant){
        //user api call
        this.adminAuthService.getTenantData().subscribe();
        //save in local storage
        //set its subject
      }else{
        this.adminAuthService.tenantData = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('tenantData') || '{}'));
      }
    }
    if(this.superAdminAuthService.superAdminTokenValue){
      const user = localStorage.getItem('userData');

      if(!user){
        //user api call
        this.superAdminAuthService.getPersonalDetails().subscribe()
      }else{
        this.superAdminAuthService.superAdminUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('userData') || '{}'));
        this.superAdminAuthService.superAdminUser = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('userData') || '{}'));
      }
    }
  }

}
