import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {NoAuthGuard} from "./guards";


const url = window.location.host;
let routesArray =[];
if(url == '8dexsuperadmin.com' || url == 'staging.8dexsuperadmin.com'){
  routesArray = [
    { path: '', loadChildren: () => import('./shared/layout/layout.module').then(m => m.LayoutModule) },
    // { path: 'login',  loadChildren: () => import('./modules/admin/admin-modules/login/login.module').then( m => m.LoginModule), canActivate: [NoAuthGuard]},
    { path: 'login', redirectTo: 'super-admin/login' },
    { path: 'super-admin/login',  loadChildren: () => import('./modules/super-admin/super-admin-modules/login/login.module').then( m => m.LoginModule), canActivate: [NoAuthGuard]},
    { path: '404', loadChildren: () => import('./modules/not-found/not-found.module').then(m => m.NotFoundModule) },
    { path: '**', redirectTo: '404' },
  ];
}else{
  routesArray =[
    { path: '', loadChildren: () => import('./shared/layout/layout.module').then(m => m.LayoutModule) },
    { path: 'login',  loadChildren: () => import('./modules/admin/admin-modules/login/login.module').then( m => m.LoginModule), canActivate: [NoAuthGuard]},
    // { path: 'login', redirectTo: 'super-admin/login' },
    { path: 'super-admin/login',  loadChildren: () => import('./modules/super-admin/super-admin-modules/login/login.module').then( m => m.LoginModule), canActivate: [NoAuthGuard]},
    { path: '404', loadChildren: () => import('./modules/not-found/not-found.module').then(m => m.NotFoundModule) },
    {path: '**', redirectTo: '404'},
  ];
}
const routes: Routes = routesArray;

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
