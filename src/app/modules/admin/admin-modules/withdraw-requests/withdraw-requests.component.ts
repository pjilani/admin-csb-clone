import { AfterViewInit, Component, OnInit } from '@angular/core';
import { CurrenciesService } from 'src/app/modules/super-admin/super-admin-modules/currencies/currencies.service';
import { TransactionTypes, PageSizes, TIMEZONE } from 'src/app/shared/constants';
import { WithdrawRequestsService } from './withdraw-requests.service';

import Swal from 'sweetalert2';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';
import { AdminAuthService } from '../../services/admin-auth.service';
import * as moment from 'moment-timezone';
import { PermissionService } from 'src/app/services/permission.service';

declare const $: any;
declare const toastr: any;
@Component({
  selector: 'app-withdraw-requests',
  templateUrl: './withdraw-requests.component.html',
  styleUrls: ['./withdraw-requests.component.scss']
})

export class WithdrawRequestsComponent implements OnInit, AfterViewInit {
  [x: string]: any;

  transactionTypes = TransactionTypes;
  withdrawalPaymentList:any = '';
  pageSizes = PageSizes;

  paymentGatewayCategoriesEnable: boolean = false;
  userVipLevel:any;
  // Default page Title and type, And can change dynamic
  title:any ='Verified Withdrawal Request';
  type:any = 'verified';

  //Added New status for payment gateway rejected and pending by payment gateway
  allStatus = [
    { name: 'Approved', value: 'approved' },
    { name: 'Cancelled', value: 'cancelled' },
    { name: 'Pending', value: 'pending' },
    // { name: 'Rejected', value: 'rejected' },
    // { name: 'Rejected / Cancelled By Payment Gateway ', value: 'rejected_by_gateway' },
    { name: 'Pending by Payment Gateway', value: 'pending_by_gateway' }
  ];
  
  // Withdrawal type is used to check that request is manual type or by payment gateway
  withdrawalType = [
    { name: 'All', value: 'all' },
    { name: 'Manual', value: 'manual' },
    { name: 'Payment Gateway', value: 'payment_gateway' }
  ];

  withdRawreqs: any[] = [];
  gatewaysList: any[] = [];
  p: number = 1;
  selectedItemIndex: number | null = null;

  format: string = "YYYY-MM-DD HH:mm:ss";

  total: number = 0;
  totalWithdrwal: number = 0;
  TIMEZONE: any[] = TIMEZONE;
  params: any = {
    size: 10,
    page: 1,
    type:this.type,
    search: '',
    withdrawal_type:'all',
    status: '',
    datetime: {},
    time_period: {
      // start_date: '',
      // end_date: '',
    },
    sort_by: 'id',
    order: 'desc'
  };
  zone: any = '(GMT +00:00) UTC';
  isLoader:boolean = false;
  firstTimeApiCall = false;

  constructor(
    private withdrawsService: WithdrawRequestsService, 
    private currencySer: CurrenciesService,
    private adminAuthService: AdminAuthService,
    private route: ActivatedRoute,
    public permissionService: PermissionService) {  }

    breadcrumbs: Array<any> = [
      { title: 'Home', path: '/' },
      { title: this.title, path: '/withdraw-requests/'+this.type },
    ];

  ngOnInit(): void {

    this.paymentGatewayCategoriesEnable = (localStorage.getItem('allowedModules')?.includes('paymentGatewayCategoriesEnable') ? true : false);
    // Used Subscription  for dynamic page call, same function is used for verify/ pending, rejected page.
    userSubscription: Subscription;
    this.userSubscription = this.route.params.subscribe(
      (params: Params) => {
        // Manage Dynamic type and title for withdrawal pages
            this.type = 'verified';
            this.title ='Verified Withdrawal Request';
           if(params.type === 'unverified' || !params.type){
             this.type = 'unverified';
             this.title ='Unverified Withdrawal Request';
           }else if(params.type === 'rejected' || !params.type){
            this.type = 'rejected';
            this.title ='Rejected Withdrawal Request';
           } 
           this.params = { ...this.params, size: 10,
            page: 1,
            type:this.type,
            search: '',
            status: '',
            withdrawal_type:'all',
            time_period: {
              // start_date: '',
              // end_date: '',
            } };

            this.withdRawreqs = [];
            this.total = 0;
            this.totalWithdrwal = 0;
          // this.getWithdrawals();
           this.getWithdrawalsGateway();

    })

    this.adminAuthService.adminUser.subscribe((user: any) => {
      if(user && user.id) {
        // this.params = {...this.params, time_zone: user.timezone}
        setTimeout(() => {
          const _zone = TIMEZONE.find(t => t.zonename == user.timezone);
          $(`#time_zone`).val(_zone?.zonename).trigger('change');
        }, 100);
      }
    });
  }
  convertToUTC(inputDate: string, inputTimezone: string): string {
    const date = moment.tz(inputDate, inputTimezone);
    const utcDate = date.utc().format('YYYY-MM-DD HH:mm:ss');
    return utcDate;
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      $('#time_period').val('');
    }, 100);
  }

  selectDateRange(time_period: any) {
    this.params = { ...this.params, time_period }
    // this.getWithdrawals();
  }

  getWithdrawals() {
    this.isLoader = true
    this.firstTimeApiCall = true;
    if(this.params.time_period.start_date && this.params.time_period.end_date && this.params.time_zone_name && this.params.time_zone_name !== 'UTC +00:00'){
      this.params = {...this.params, datetime: {  start_date: this.convertToUTC(this.params.time_period.start_date, this.params.time_zone_name), end_date:this.convertToUTC(this.params.time_period.end_date, this.params.time_zone_name)}}
    } else{
      this.params = {...this.params, datetime: {  start_date: this.params.time_period.start_date, end_date:this.params.time_period.end_date}}
    }
    this.withdrawsService.getWithdrawals(this.params).subscribe((res: any) => {
      this.withdRawreqs = res.record.data;
      this.total = res.record.count;
      this.totalWithdrwal = res.record.total;
      this.isLoader = false;
    },(error:any) => {
      this.isLoader = false

    });  
  }
  exportAsXLSX() {
    if(this.params.time_period.start_date && this.params.time_period.end_date && this.params.time_zone_name && this.params.time_zone_name !== 'UTC +00:00'){
      this.params = {...this.params, datetime: {  start_date: this.convertToUTC(this.params.time_period.start_date, this.params.time_zone_name), end_date:this.convertToUTC(this.params.time_period.end_date, this.params.time_zone_name)}}
    } else{
      this.params = {...this.params, datetime: {  start_date: this.params.time_period.start_date, end_date:this.params.time_period.end_date}}
    }
    this.withdrawsService.downloadWithdrawals(this.params).subscribe(
      (res: any) => {
        window.location.href = res.record.url;
    });
  }

  toggleDetails(index: number) {
    this.selectedItemIndex = (this.selectedItemIndex === index) ? null : index;
  }
  
  getWithdrawalsGateway() {
    // Get withdrawal provider gateway list, that selected by tenant and make dropdown list html for model. 
    this.withdrawsService.getWithdrawalsGateway().subscribe((res: any) => {
      this.gatewaysList = res.record.paymentProvider;
      this.withdrawalPaymentList = '<option value="">Select Payment Provider</option>';
      for (let index = 0; index < this.gatewaysList.length; index++) {
        const element = this.gatewaysList[index];
        if(this.paymentGatewayCategoriesEnable && element.vip_level != null){
          if(this.userVipLevel?.toString()){
            if(JSON.parse(element.vip_level).includes(this.userVipLevel?.toString())){
              this.withdrawalPaymentList += '<option value="'+element.id+'">'+element.provider_name+'</option>'

            }
          }
        }
        else {
          this.withdrawalPaymentList += '<option value="'+element.id+'">'+element.provider_name+'</option>'
        }
        
      }
      
    });  
  }

  filter(evt: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p, type: this.type };
    this.getWithdrawals();
  }
  
  pageChanged(page: number) {
    this.params = { ...this.params, page,type :this.type };
    this.getWithdrawals();
  }

  resetFilter() {
    this.p = 1;
    $('#time_period').val('');
    // $( "#time_zone" ).val('UTC +00:00').trigger('change');

    this.params = {
      size: 10,
      page: 1,
      search: '',
      status: '',
      withdrawal_type:'all',
      type: this.type,
      time_zone: this.params.time_zone,
      time_zone_name: this.params.time_zone_name,
      datetime:{},
      time_period: {
        // start_date: '',
        // end_date: '',
      },
      sort_by: 'id',
      order: 'desc'
    };

    this.getWithdrawals();
  }

  // Model used for Manual Update withdrawal request
  updateWRStatus(transaction: any, status: string, page:string) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to ${status} it!`,
      html:
          `<h4>You want to ${status} it!</h4><br>` +
          '<label>Remark</label>' +
          '<textarea style="margin-top: 2px;margin-left:51px;" id="remark" placeholder="Remark" class="swal2-input" cols="21" rows="25"></textarea>' +
          '<label>Password<span class="text-red">*</span></label>' +
          '<input style="margin-left:31px;margin-top: 2px;-webkit-text-security: disc;text-security: disc;" type="text" autocomplete="new-password" id="password" placeholder="Enter Password*" class="swal2-input">',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, ${status} it!`,
      preConfirm: function () {
        return new Promise(function (resolve) {
          // Validate input
          if ($('#password').val() == '') {
              Swal.showValidationMessage("Please verify your password!"); // Show error when validation fails.
              Swal.enableButtons(); // Enable the confirm button again.
          } else {
              Swal.resetValidationMessage(); // Reset the validation message.
              resolve([
                  $('#remark').val(),
                  $('#password').val()
              ]);
          }
      })
      }
    }).then((result) => {
      if (result.isConfirmed) {

        this.withdrawsService.updateWRStatus({ id: transaction.id, status , page , data: result.value}).subscribe((res: any) => {
          toastr.success(res.message || 'Withdraw requests updated successfully' );
          this.withdRawreqs = this.withdRawreqs.map((f: any) => {
            if(f.id == transaction.id) {
              f.status = status;
            }
            return f;
          });
          this.getWithdrawals();
        });
        
      }else{
            Swal.fire('Request Not Approved', '', 'error')
      }

    });
  }


  // This model used for select type of withdrawal process 
  /* Manual 
     Payment Gateway 
  */ 
  updateApproveStatus(transaction: any, status: string) {
    this.getWithdrawalsGateway()
    this.userVipLevel = transaction.vip_level
    Swal.fire({
      title: 'Select the withdrawal type',
      icon: 'warning',
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Manual',
      denyButtonText: 'Payment Gateway',
    }).then((result) => {
      if (result.isConfirmed) {
        // Manual
        this.updateWRStatus(transaction,status,'');
      }else if (result.isDenied) {
        // Automatic Payment Gateway
        this.updatePaymentGatewayWRStatus(transaction,status);
        // Swal.fire('By Payment Gateway', '', 'info')
      }else{
        Swal.fire('Request Not Approved', '', 'error')
      }

    });
  }


  /* This model used for payment provider
      select withdrawal payment provider
      Enter remark
      Enter password
      Automatic call payment provider and update the status
  */
  updatePaymentGatewayWRStatus(transaction: any, status: string) {  
    Swal.fire({
      title: 'Select Payment Provider',
      html:
      `<h4>You are using automatic gateway!</h4><br>` +
      '<label for="gateway_id">Provider<span class="text-red">*</span></label>' +
      '<select style="margin-top: -2px;margin-bottom:12px;;width:60%" class="swal2-input" id="gateway_id">' +
      this.withdrawalPaymentList +
      '</select>' +
      '<br><label>Remark</label>' +
      '<textarea style="margin-top: 2px;margin-left:51px;;width:60%" id="remark" placeholder="Remark" class="swal2-input" cols="21" rows="25"></textarea>' +
      '<label>Password<span class="text-red">*</span></label>' +
      '<input style="margin-left:31px;margin-top: 2px;;width:60%;-webkit-text-security: disc;text-security: disc;" type="text" autocomplete="new-password" id="password" placeholder="Enter Password*" class="swal2-input">',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, ${status} it!`,
      preConfirm: function () {
        return new Promise(function (resolve) {
          // Validate input
          if ($('#gateway_id').val() == '') {
            Swal.showValidationMessage("Please select any provider!"); // Show error when validation fails.
            Swal.enableButtons(); // Enable the confirm button again.
          }else if ($('#password').val() == '') {
              Swal.showValidationMessage("Please verify your password!"); // Show error when validation fails.
              Swal.enableButtons(); // Enable the confirm button again.
          } else {
              Swal.resetValidationMessage(); // Reset the validation message.
              resolve([
                  $('#remark').val(),
                  $('#password').val(),
                  $('#gateway_id').val()
              ]);
          }
      })
      }
    }).then((result) => {
      if (result.isConfirmed) {
        this.withdrawsService.updateAutoPaymentWRStatus({ id: transaction.id, status , data: result.value }).subscribe((res: any) => {
          if(res.success == 1)
          toastr.success(res.message || 'Withdrawal requests updated successfully' );
          else
          toastr.error(res.message || 'Withdrawal requests updated successfully' );
          this.getWithdrawals();
        });
        
      }else{
        Swal.fire('Request Not Approved', '', 'error')
      }

    });
  }

  // This model is used for verify withdrawal request and unverify request
  updateVerifyWRStatus(transaction: any, status: string) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to ${status} it!`,
      html:
      `<h4>You want to ${status} it!</h4><br>` +
      '<label>Remark</label>' +
      '<textarea style="margin-top: 2px;margin-left:51px;" id="remark" placeholder="Remark" class="swal2-input" cols="21" rows="25"></textarea>' +
      '<label>Password<span class="text-red">*</span></label>' +
      '<input style="margin-left:31px;margin-top: 2px;-webkit-text-security: disc;text-security: disc;" type="text" autocomplete="new-password" id="password" placeholder="Enter Password*" class="swal2-input">',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, ${status} it!`,
      preConfirm: function () {
        return new Promise(function (resolve) {
          // Validate input
          if ($('#password').val() == '') {
              Swal.showValidationMessage("Please verify your password!"); // Show error when validation fails.
              Swal.enableButtons(); // Enable the confirm button again.
          } else {
              Swal.resetValidationMessage(); // Reset the validation message.
              resolve([
                  $('#remark').val(),
                  $('#password').val()
              ]);
          }
      })
      }
    }).then((result) => {
      if (result.isConfirmed) {
          
        this.withdrawsService.updateVerifyWRStatus({ id: transaction.id, status , data: result.value }).subscribe((res: any) => {
          toastr.success(res.message || 'Withdrawal requests updated successfully' );
          this.getWithdrawals();
        });
        
      }else{
        Swal.fire('Request Not Approved', '', 'error')
      }

    });
  }

  filterSelectTimeZone(zonename: any) {
    // this.p = 1;

    if(zonename) {
      const zone = TIMEZONE.find(t => t.zonename === zonename);
      this.zone = zone?.name;
      this.params = {...this.params, time_zone: zone?.value, time_zone_name: zone?.zonename};
    }else{
      this.zone='';
      this.params = {...this.params, time_zone: this.zone};
    }

    // this.getWithdrawals();
    }

    setOrder(sort: any) {
      this.p =1;
      this.params = { ...this.params, page: this.p, order: sort.order, sort_by: sort.column };
      this.getWithdrawals();
    }

}
