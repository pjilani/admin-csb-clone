import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })

export class WithdrawRequestsService {

  constructor(private http: HttpClient) { }

  getWithdrawals(params: any) {
    return this.http.post(`admin/transactions/withdrawal/lists`, params);
  }
  downloadWithdrawals(params: any) {
    return this.http.post(`admin/transactions/withdrawal/download`, params);
  }
 
  getWithdrawalsGateway() {
    return this.http.get(`admin/transactions/withdrawal/payment/lists`);
  }
  
  updateWRStatus(params: any) {
    return this.http.put(`admin/transactions/withdrawal/update/status`, params);
  }
  
  updateVerifyWRStatus(params: any) {
    return this.http.put(`admin/transactions/withdrawal/update/verify/status`, params);
  }
  
  updateAutoPaymentWRStatus(params: any) {
    return this.http.put(`admin/transactions/withdrawal/update/auto/verify/status`, params);
  }

}
