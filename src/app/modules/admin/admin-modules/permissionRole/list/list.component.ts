import { Component, OnInit } from '@angular/core';
import { PermissionRole } from 'src/app/models';
import { PermissionRoleService } from '../permissionRole.service';
import Swal from 'sweetalert2';
import { PermissionService } from 'src/app/services/permission.service';
declare const toastr: any;

@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {

  p: number = 1;

  params = {
    size: 10,
    page: 1,
    search_role_name: '',
    role_type: '',
  }

  total:number = 0;

  permissionRoles: PermissionRole[] = [];

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '' },
    { title: 'Permission Role', path: '/permissionRole' },
  ];

  roles: any[] = [{ name: 'Agent', value: 2 }, { name: 'Sub-admin', value: 3 }];

  isLoader:boolean = false;
  firstTimeApiCall = false;

  constructor(
    private permissionRoleService: PermissionRoleService,
    public permissionService: PermissionService) { }

  ngOnInit(): void {
    this.getPermissionRole();
  }

  getPermissionRole() {
    this.isLoader = true
    this.firstTimeApiCall = true;
    this.permissionRoleService.getPermissionRoles(this.params).subscribe((res: any) => {
      this.permissionRoles = res.record.data;
      this.total = res.record.total;
      this.isLoader = false;
    });
  } 

  delete(permissionRole: PermissionRole) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to delete ${permissionRole.role_name}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, delete it!`
    }).then((result) => {
      if (result.isConfirmed) {

        this.permissionRoleService.deletePermissionRole(permissionRole.id).subscribe((res: any) => {
          toastr.success( res.message || 'Permission Role Deleted Successfully.');
          this.getPermissionRole();
        });
        
      }
    });
    
  }

  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getPermissionRole();
  }

  getPermissionRoleType(role_type:any){ 
    return this.roles.find(role => role.value === role_type)?.name ?? '-'
  }

  filter(evt: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getPermissionRole();
  }

  resetFilter() {
    this.p = 1;
    this.params = {      
      size: 10,
      page: 1,
      search_role_name: '',
      role_type: '',
    };

    this.getPermissionRole();
  }

}
