import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PermissionRole } from 'src/app/models';
import { Constants, TENENT_PERMISSION } from 'src/app/shared/constants';
import { PermissionRoleService } from '../permissionRole.service';
import { AdminAuthService } from '../../../services/admin-auth.service';

declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})

export class EditComponent implements OnInit {

  id: number = 0;
  permissionRole!: PermissionRole;
  keyArr = (Object.keys(TENENT_PERMISSION));

  // intPermissionRole: any = { ...Constants.INIT_THEME };
  
  permissionRoleForm: FormGroup | any;
  submitted: boolean = false;
  permissionRoleLoader: boolean = false;

  selectedPermissions: any = {};
  permissions:any={};

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '' },
    { title: 'Permission Role', path: '/permissionRole' }
  ];

  roles: any[] = [{ name: 'Agent', value: 2 }, { name: 'Sub-admin', value: 3 }];

  permissionLabels = [
    {label: 'R', value: 'Read'},
    {label: 'C', value: 'Create'},
    {label: 'U', value: 'Update'},
    {label: 'D', value: 'Delete'},
    {label: 'T', value: 'Toggle'},
    {label: 'financial_activity', value: 'Financial Activity'},
    {label: 'gaming_activity', value: 'Gaming Activity'},
    {label: 'export', value: 'Export'},     
  ]

  initialPermissions = {
    "dashboard": [
      "R"
    ],
    "notifications": [
      "R"
    ]
  }

  constructor(private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private permissionRoleService: PermissionRoleService,
    private adminAuthService: AdminAuthService) {
     this.id = this.route.snapshot.params['id'];
     
     

    this.permissionRoleForm = this.formBuilder.group({
      role_name: ['', [ Validators.required, Validators.maxLength(30)]],
      permission: ['', [Validators.required]],
      role_type: ['', [Validators.required]]
    });

    this.permissionRoleForm.get('role_type').valueChanges.subscribe((value : any) => {
      if(value){
        this.permissionRoleService.getRoleWisePermissions({role_type: value}).subscribe((res:any) => {
        
          this.permissions = (res.record.permissions)          
          if(this.id <= 0) {
            this.selectedPermissions = {}
            this.togglePermission('dashboard', 'R')
            this.togglePermission('notifications', 'R')
          }
          // ? JSON.parse(res.record.permissions) : TENENT_PERMISSION
        })
      }
    })
  

  }

  ngOnInit(): void {
    // this.adminAuthService?.adminUser.subscribe((user:any) => {
    let title: string = 'Create';

    if(this.id > 0) {
      title = 'Edit';
      this.getPermissionRole();

      // $('#role_type').select2({
      //   disabled: true
      // });
      this.f.role_type.disable()
      
    }else{
      // this.permissions = TENENT_PERMISSION;
      // this.changeRoleType(this.roles[0].value)
      this.f.role_type.setValue(this.roles[0].value)

      this.setPermissions(this.permissions)

      this.togglePermission('dashboard', 'R')
      this.togglePermission('notifications', 'R')
    }

    // if(Object.keys(user).length){
    //   this.permissions = (user?.tenant?.tenantPermissions) ? JSON.parse(user?.tenant?.tenantPermissions) : TENENT_PERMISSION
    // }
      
    this.breadcrumbs.push({ title, path: `/permissionRole/${this.id}` });
    // });

    

    }

  getPermissionRole() {
    this.permissionRoleService.getPermissionRoleById(this.id).subscribe((res: any) => {
      this.permissionRole = res.record;
      this.permissionRoleForm.patchValue({
        role_name: this.permissionRole.role_name,
        role_type : this.permissionRole.role_type
      });

      this.selectedPermissions = JSON.parse(this.permissionRole.permission)
    });
  }

  get f() {
    return this.permissionRoleForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid

    this.f.permission.setErrors({ required: null });

    // console.log(this.adminForm.value);
    let isPermissionSelected = false;
    for (let sp in this.selectedPermissions) {
      if (this.selectedPermissions[sp].length > 0) {
        isPermissionSelected = true;
        break;
      }
    }
    const filteredPermissions = Object.entries(this.selectedPermissions).reduce(
      (acc:any, [key, value]:any) => {
        if (value.length > 0) {
          acc[key] = value;
        }
        return acc;
      },
      {}
    );
    
    
    if (isPermissionSelected) {
      this.f.permission.setValue(JSON.stringify(filteredPermissions));

      
    } else {
      this.f.permission.setErrors({
        required: 'permission are required',
      });
    }

    if (this.permissionRoleForm.invalid) return;    

    this.permissionRoleLoader = true;
 
    const role_name = this.permissionRoleForm.value.role_name;
    const permission = this.permissionRoleForm.value.permission;
    const role_type = this.permissionRoleForm.getRawValue().role_type;
    // let permissionRole_option = (this.permissionRole ? this.permissionRole : this.intPermissionRole)
    // const options = { ...permissionRole_option, role_name };

    const data: any = { role_name, permission, role_type};
    
    if(this.id > 0) {
      data.id = this.id;

      this.permissionRoleService.updatePermissionRole(data)
      .subscribe((response: any) => {
        toastr.success(response?.message || 'PermissionRole Updated Successfully!');
        this.permissionRoleLoader = false;
        // this.router.navigate(['/permissionRole']);
      }, (err: any) => {
        this.permissionRoleLoader = false; 
      });

    } else {

      this.permissionRoleService.createPermissionRole(data)
      .subscribe((response: any) => {
        toastr.success(response?.message || 'PermissionRole Created Successfully!');
        this.permissionRoleLoader = false;
        // this.router.navigate(['/permissionRole']);
      }, (err: any) => {
        this.permissionRoleLoader = false; 
      });

    }
  }


  setPermissions(permissions:any) {
    this.selectedPermissions = Object.assign({}, permissions);
  }

  getPermissionsFromValue(value: any) {
    return value;
  }

  isEmptyPermission(value: any) {
    
    return value?.length > 0;
  }

  isPermissionSelected(header: any, permission: any) {

    
    
    
    
    if (this.selectedPermissions.hasOwnProperty(header) && Array.isArray(this.selectedPermissions[header]) && this.selectedPermissions[header].length > 0) {
      // Check if the permission exists in the module's permissions array
      return this.selectedPermissions[header].includes(permission);
  }

  // Module not found in the configuration
  return false;
  }



  togglePermission(header: any, permission: any) {
    let isRemoved = false;
    
    if (!this.selectedPermissions[header]) {
      this.selectedPermissions[header] = [];
    }

    if ((header == 'fund_transfer_deposit' || header == 'fund_transfer_withdraw') && (!this.selectedPermissions['agents']?.includes('R') && !this.selectedPermissions['players']?.includes('R'))) {
      toastr.error(
        "Either Agents or Players should be selected before Fund Transfer Permissions"
      );
      return;
    }

    if (permission !== 'R' && !this.selectedPermissions[header].includes('R')) {
      // toastr.error(
      //   'Read Permissions is required first'
      // );
      // return;
      this.selectedPermissions[header].push('R');
    }

    if (header == 'dashboard' && permission == 'R' && this.selectedPermissions[header].includes('R')) {
      toastr.error(
        "Dashboard Read Permissions cannot be unselected"
      );
      return;
    }

    if (header == 'notifications' && permission == 'R' && this.selectedPermissions[header].includes('R')) {
      toastr.error(
        "Notifications Read Permissions cannot be unselected"
      );
      return;
    }

    this.selectedPermissions[header] = this.selectedPermissions[header].filter(
      (p:any) => {
        if (p == permission) {
          isRemoved = true;
          return false;
        }
        return true;
      }
    );

    if (isRemoved) {
      if (permission === 'R') {
        this.selectedPermissions[header] = [];

        if(!this.selectedPermissions['agents']?.length && !this.selectedPermissions['players']?.length){
          this.selectedPermissions['fund_transfer_deposit'] = []
          this.selectedPermissions['fund_transfer_withdraw'] = []
        }
      }
    } else {
      this.selectedPermissions[header].push(permission);
    }
  }

  getLabel(value: any): string {
    return this.permissionLabels.find(v => v.label == value)?.value || value;
  }

  compareTenantPermissionObjects(permissions1:any, permissions2:any) {
    const keys1 = Object.keys(permissions1);
    const keys2 = Object.keys(permissions2);
  
    if (keys1.length !== keys2.length) {
      return false;
    }
  
    for (const key of keys1) {
      if (!keys2.includes(key) || !this.arraysAreEqual(this.selectedPermissions[key], this.permissions[key])) {
        return false;
      }
    }
  
    return true;
  }
  
  arraysAreEqual(arr1:any, arr2:any) {
    if (arr1.length !== arr2.length) {
      return false;
    }
    
    const sortedArr1 = arr1.slice().sort();
    const sortedArr2 = arr2.slice().sort();
  
    return sortedArr1.every((value:any, index:any) => value === sortedArr2[index]);
  }

  toggleAllPermissions(event:any){
    if(event.target.checked){
      this.setPermissions(this.permissions)
    }
    else{
      this.selectedPermissions = {}
      this.togglePermission('dashboard', 'R')
      this.togglePermission('notifications', 'R')   
    }
  }

  }
