import {  Component,  OnInit } from '@angular/core';
import {  PageSizes } from 'src/app/shared/constants';
import { SiteMaintenanceServices } from './site-maintenance.service';

import Swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { PermissionService } from 'src/app/services/permission.service';
import { AdminAuthService } from '../../services/admin-auth.service';

declare const $: any;
declare const toastr: any;
@Component({
  selector: 'app-site-maintenance-requests',
  templateUrl: './site-maintenance.component.html',
  styleUrls: ['./site-maintenance.component.scss'],
  providers:[DatePipe]
})

export class SiteMaintenanceComponent implements OnInit {
  

  data:any = [];
  pageSizes = PageSizes;
  mModeForm: FormGroup | any;
  submitted: boolean = false;
  mModeLoader: boolean = false;


  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Site Maintenance', path: '/site-maintenance' },
  ];
 

  constructor(
    private SiteService: SiteMaintenanceServices,private formBuilder: FormBuilder,
    public permissionService: PermissionService,
    public adminAuthService: AdminAuthService) {
      this.mModeForm = this.formBuilder.group({
        title: ['', [ Validators.required ]],
        announcement_title: [''],
        active: [''],
        site_down: ['']
      });

      this.adminAuthService.adminPermissions.subscribe((res:any)=>{
        if(res && Object.keys(res).length){
          if(!this.permissionService.checkPermission('site_maintenance','U')){
            this.mModeForm.disable();
          }
        }
      })
     }
  
 

  ngOnInit(): void {
    this.getMaintenanceMode();
  }

  get f() {
    return this.mModeForm.controls;
  }


  getMaintenanceMode() {
      const that = this;
      this.SiteService.getMaintenanceMode().subscribe((res: any) => {
        const data = res.record.list;
        this.mModeForm.patchValue({
          'title' : (data?.title ? data?.title : ''),
          'announcement_title' : (data?.announcement_title ? data?.announcement_title : ''),
          'active' : (data?.is_announcement_active ? true : false),
          'site_down' : (data?.site_down ? true : false)
        });
      });  
      }

  onSubmit() {
    this.submitted = true;

    if (this.mModeForm.invalid){
      return;
    } 
    const data = this.mModeForm.value;
    this.updateWRStatus(data);    
  
  }

  updateWRStatus(data: any) {
    Swal.fire({
      title: 'Are you sure?',
      html:
      '<label>Password<span class="text-red">*</span></label>' +
      '<input style="margin-left:31px;margin-top: 2px;" type="password" autocomplete="off" id="password" placeholder="Enter Password*" class="swal2-input">',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes!`,
      preConfirm: function () {
        return new Promise(function (resolve) {
          // Validate input
          if ($('#password').val() == '') {
              Swal.showValidationMessage("Please verify your password!"); // Show error when validation fails.
              Swal.enableButtons(); // Enable the confirm button again.
          } else {
              Swal.resetValidationMessage(); // Reset the validation message.
              resolve([
                  $('#password').val()
              ]);
          }
      })
      }
    }).then((result) => {
      if (result.isConfirmed) {
          
        this.SiteService.updateModeStatus({ data , password: result.value }).subscribe((res: any) => {
          toastr.success(res.message || 'Site Maintenance Mode updated successfully' );
        });
        
      }

    });
  }

}
