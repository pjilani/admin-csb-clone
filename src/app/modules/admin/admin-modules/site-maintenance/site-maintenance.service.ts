import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })

export class SiteMaintenanceServices {

  constructor(private http: HttpClient) { }

  getMaintenanceMode() {
    return this.http.get(`admin/maintenance/list`);
  }
  
  updateModeStatus(params: any) {
    return this.http.put(`admin/maintenance/update`, params);
  }

}
