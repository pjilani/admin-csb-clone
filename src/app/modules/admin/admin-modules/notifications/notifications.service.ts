import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  unreadNotifications: any;

  constructor(private http: HttpClient) { }

  getUnReadNotification() {
    return this.http.get(`admin/notification/unread`).pipe(map((m: any) => this.unreadNotifications = m.record ));
  }

  getNotifications(params: any) {
    return this.http.get(`admin/notification`, { params });
  }

  readAllNotifications() {
    return this.http.put(`admin/notification/all-read`, {}).pipe(tap(t => this.unreadNotifications.count = 0 ) );
  }

}
