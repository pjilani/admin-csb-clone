import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationsComponent } from './notifications.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { ComponentsModule } from 'src/app/shared/components/components.module';

const notificationsRoutes: Routes = [
  { path: '', component: NotificationsComponent}
];

@NgModule({
  declarations: [
    NotificationsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(notificationsRoutes),
    ComponentsModule,
    SharedModule
  ]
})
export class NotificationsModule { }
