import { Component, OnInit } from '@angular/core';
import { NotificationsService } from './notifications.service';
import { PermissionService } from 'src/app/services/permission.service';

@Component({
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})

export class NotificationsComponent implements OnInit {

  notifications: any[] = [];
  
  p: number = 1;
  total: number = 0;
  
  params: any = {
    size: 10,
    page: 1,
  };
  
  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Notifications', path: '/notifications' }
  ];

  constructor(public nottificationsService: NotificationsService, public permissionService: PermissionService) { }

  ngOnInit(): void {
    this.getNotifications();
  }

  pageChanged(page: number) {
    this.p = page;
    this.params = { ...this.params, page };
    this.getNotifications();
  }

  getNotifications() {
    this.nottificationsService.getNotifications(this.params).subscribe((res: any) => {
      this.notifications = res.record.data;
      this.total = res.record.total;
    });
  }

}
