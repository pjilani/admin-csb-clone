import { AfterViewInit, Component, OnInit } from '@angular/core';
import { CurrenciesService } from 'src/app/modules/super-admin/super-admin-modules/currencies/currencies.service';
import {  PageSizes, TIMEZONE } from 'src/app/shared/constants';
import { DepositRequestsService } from './deposit-requests.service';

import Swal from 'sweetalert2';
import { AdminAuthService } from '../../services/admin-auth.service';
import * as moment from 'moment-timezone';
import { PermissionService } from 'src/app/services/permission.service';

declare const $: any;
declare const toastr: any;
@Component({
  selector: 'app-deposit-requests',
  templateUrl: './deposit-requests.component.html',
  styleUrls: ['./deposit-requests.component.scss']
})

export class DepositRequestsComponent implements OnInit, AfterViewInit {

  zone: any = '(GMT +00:00) UTC';
  TIMEZONE: any[] = TIMEZONE;
  pageSizes = PageSizes;
  tenantSourceWallet:any = '';
  depositTypeSelected:any = 'manual';
  admin:any;
  wallet:any = [];
  allStatus = [
    { name: 'Approved', value: 'completed' },
    { name: 'Pending', value: 'opened' },
    { name: 'Rejected', value: 'rejected' }
  ];
  allPaymentGatewayStatus = [
    { name: 'Opened', value: 'opened' },
    { name: 'Completed', value: 'completed' },
    { name: 'Cancelled', value: 'canceled' },
    { name: 'Failed', value: 'failed' },
    { name: 'Pending', value: 'in_process' }
  ];
  depositType = [
    { name: 'Manual', value: 'manual' },
    { name: 'Payment Gateway', value: 'payment_providers' }
  ];

  depositRequest: any[] = [];
  p: number = 1;

  format: string = "YYYY-MM-DD HH:mm:ss";

  total: number = 0;
  totalDeposit: any = 0;
  vipLevelSearch: any;

  params: any = {
    size: 10,
    page: 1,
    search: '',
    transaction_id: '',
    status: '',
    deposit_type: 'manual',
    datetime: {},
    time_period: {
      // start_date: '',
      // end_date: '',
    },
    vip_levels:'',
    sort_by: 'id',
    order: 'desc',
    datetimeActionedAt: {},
    timePeriodActionedAt: {
      // start_date: '',
      // end_date: '',
    }
  };

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Deposit Requests', path: '/deposit-requests' },
  ];

  isLoader:boolean = false;
  selectedTransactionReciept: any;
  firstTimeApiCall = false;

  constructor(
    private depositService: DepositRequestsService, 
    private adminAuthService: AdminAuthService,
    private currencySer: CurrenciesService,
    public permissionService: PermissionService) { }

  ngOnInit(): void {
    // this.adminAuthService.adminUser.subscribe((user: any) => {
    //   if(user && user.id) {
    //     this.admin = user;
    //     this.wallet = user.wallets;
        
        
    //     this.getTenantSourceWallet(this.wallet);
    //   }
    // });
    this.adminAuthService.adminUser.subscribe((user: any) => {
      if(user && user.id) {
        this.admin = user;
        // this.params = {...this.params, time_zone: user.timezone}
        setTimeout(() => {
          const _zone = TIMEZONE.find(t => t.zonename == user.timezone);
          $(`#time_zone`).val(_zone?.zonename).trigger('change');
        }, 100);
      }
    });

    this.adminAuthService?.walletData.subscribe((wallets: any) => {
      this.wallet = wallets;
      this.getTenantSourceWallet(this.wallet);
    });  
    // this.getDepositRequests();
  }
  convertToUTC(inputDate: string, inputTimezone: string): string {
    const date = moment.tz(inputDate, inputTimezone);
    const utcDate = date.utc().format('YYYY-MM-DD HH:mm:ss');
    return utcDate;
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      $('#time_period').val('');
      $('#time_period_actioned_at').val('')
    }, 100);
  }

  selectDateRange(time_period: any) {
    this.params = { ...this.params, time_period }
    // this.getDepositRequests();
  }
  selectDateRangeActionedAt(timePeriodActionedAt: any) {
    this.params = { ...this.params, timePeriodActionedAt }
    // this.getDepositRequests();
  }
  getDepositRequests() {
    this.isLoader = true
    this.firstTimeApiCall = true;
    if(this.params.time_period.start_date && this.params.time_period.end_date && this.params.time_zone_name && this.params.time_zone_name !== 'UTC +00:00'){
      this.params = {...this.params, datetime: {  start_date: this.convertToUTC(this.params.time_period.start_date, this.params.time_zone_name), end_date:this.convertToUTC(this.params.time_period.end_date, this.params.time_zone_name)}}
    } else{
      this.params = {...this.params, datetime: {  start_date: this.params.time_period.start_date, end_date:this.params.time_period.end_date}}
    }
    if(this.params.timePeriodActionedAt.start_date && this.params.timePeriodActionedAt.end_date && this.params.time_zone_name && this.params.time_zone_name !== 'UTC +00:00'){
      this.params = {...this.params, datetimeActionedAt: {  start_date: this.convertToUTC(this.params.timePeriodActionedAt.start_date, this.params.time_zone_name), end_date:this.convertToUTC(this.params.timePeriodActionedAt.end_date, this.params.time_zone_name)}}
    } else{
      this.params = {...this.params, datetimeActionedAt: {  start_date: this.params.timePeriodActionedAt.start_date, end_date:this.params.timePeriodActionedAt.end_date}}
    }
    this.depositService.getDepositRequests(this.params).subscribe((res: any) => {
      this.depositRequest = res.record.data;
      this.total = res.record.count;
      this.totalDeposit = parseFloat(res.record.totalDeposit).toFixed(2);
      this.isLoader = false;
    },
    (error:any) => {
      this.isLoader = false

    });  
  }
  exportAsXLSX() {
    if(this.params.time_period.start_date && this.params.time_period.end_date && this.params.time_zone_name && this.params.time_zone_name !== 'UTC +00:00'){
      this.params = {...this.params, datetime: {  start_date: this.convertToUTC(this.params.time_period.start_date, this.params.time_zone_name), end_date:this.convertToUTC(this.params.time_period.end_date, this.params.time_zone_name)}}
    } else{
      this.params = {...this.params, datetime: {  start_date: this.params.time_period.start_date, end_date:this.params.time_period.end_date}}
    }

    if(this.params.timePeriodActionedAt.start_date && this.params.timePeriodActionedAt.end_date && this.params.time_zone_name && this.params.time_zone_name !== 'UTC +00:00'){
      this.params = {...this.params, datetimeActionedAt: {  start_date: this.convertToUTC(this.params.timePeriodActionedAt.start_date, this.params.time_zone_name), end_date:this.convertToUTC(this.params.timePeriodActionedAt.end_date, this.params.time_zone_name)}}
    } else{
      this.params = {...this.params, datetimeActionedAt: {  start_date: this.params.timePeriodActionedAt.start_date, end_date:this.params.timePeriodActionedAt.end_date}}
    }
    this.depositService.downloadDepositRequests(this.params).subscribe(
      (res: any) => {
        window.location.href = res.record.url;
    });
  }

  filter(evt: any) {
    // this.p = 1;
    
    if(evt === 'manual'){
      this.depositTypeSelected = 'manual';
      this.params = { ...this.params, status:'' };
    }else if(evt === 'payment_providers'){
      this.depositTypeSelected = 'payment_providers';
      this.params = { ...this.params, status:'' };
    }
    // this.params = { ...this.params, page: this.p };
    // this.getDepositRequests();
  }

  submitFilters(){
    this.p = 1;
    this.vipLevelSearch = this.params.vip_levels.toString()
    this.params = { ...this.params, page: this.p, vip_levels:this.vipLevelSearch };
    this.getDepositRequests();
  }
  
  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getDepositRequests();
  }

  resetFilter() {
    this.p = 1;
    $('#time_period').val('');
    $('#time_period_actioned_at').val('');
    // $( "#time_zone" ).val('UTC +00:00').trigger('change');

    this.params = {
      size: 10,
      page: 1,
      search: '',
      status: '',
      transaction_id: '',
      deposit_type: 'manual',
      time_zone: this.params.time_zone,
      time_zone_name: this.params.time_zone_name,
      datetime:{},
      time_period: {
        // start_date: '',
        // end_date: '',
      },
      vip_levels: '',
      sort_by: 'id',
      order: 'desc',
      datetimeActionedAt: {},
      timePeriodActionedAt: {
        // start_date: '',
        // end_date: '',
      }
    }
    $('#time_period').val('');
    // $( "#time_zone" ).val('UTC +00:00').trigger('change');


    this.getDepositRequests();
    $('#vip_levels').val('').trigger('change');
  }

  getTenantSourceWallet(wallet:any) { 
      this.tenantSourceWallet = '<option value="">Select Source Wallet</option>';
      for (let index = 0; index < wallet.length; index++) {
        this.tenantSourceWallet += '<option value="'+wallet[index].id+'">'+wallet[index].currency_name+'</option>'
        
      }
 
  }

  updateWRStatus(transaction: any, status: string) {
    let htmlContent = `<h4>You want to ${status} it!</h4><br>` +
      '<label for="wallet_id">Wallet<span class="text-red">*</span></label>' +
      '<select style="margin-top: -2px;margin-bottom:12px;;width:64%;margin-left: 52px;" class="swal2-input" id="wallet_id">' +
      this.tenantSourceWallet +
      '</select>' +
      '<label style="position: relative;bottom: 20px;">Remark</label>' +
      '<textarea style="margin-top: 2px;margin-left:53px;width: 64%;" id="remark" placeholder="Remark" class="swal2-input" cols="21" rows="25"></textarea>' +
      '<label>Password<span class="text-red">*</span></label>' +
      '<input style="margin-left:31px;margin-top: 2px;-webkit-text-security: disc;text-security: disc;" autocomplete="new-password" type="text" autocomplete="off" id="password" placeholder="Enter Password*" class="swal2-input">';
  
    // Check if status is 'rejected' and exclude the select element
    if (status === 'rejected') {
      htmlContent = `<h4>You want to ${status} it!</h4><br>` +
        '<label style="position: relative;bottom: 20px;">Remark</label>' +
        '<textarea style="margin-top: 2px;margin-left:53px;width: 64%;" id="remark" placeholder="Remark" class="swal2-input" cols="21" rows="25"></textarea>' +
        '<label>Password<span class="text-red">*</span></label>' +
        '<input style="margin-left:31px;margin-top: 2px;-webkit-text-security: disc;text-security: disc;" autocomplete="new-password" type="text" autocomplete="off" id="password" placeholder="Enter Password*" class="swal2-input">';
    }
  
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to ${status} it!`,
      html: htmlContent,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, ${status} it!`,
      preConfirm: function () {
        return new Promise(function (resolve) {
          // Validate input
          if ($('#wallet_id').val() == '' && status !== 'rejected') {
            Swal.showValidationMessage("Please select your source wallet!"); // Show error when validation fails.
            Swal.enableButtons(); // Enable the confirm button again.
          }else if($('#password').val() == ''){
            Swal.showValidationMessage("Please verify your password!"); // Show error when validation fails.
            Swal.enableButtons(); // Enable the confirm button again.
          } else {
            Swal.resetValidationMessage(); // Reset the validation message.
             if (status === 'rejected') {
              resolve([
                $('#remark').val(),
                $('#password').val()
              ]);
            } else {
              resolve([
                $('#remark').val(),
                $('#password').val(),
                $('#wallet_id').val()
              ]);
            }
          }
        })
      }
    }).then((result) => {
      if (result.isConfirmed) {
        
        this.depositService.updateDRStatus({ id: transaction.id, status , data: result.value }).subscribe((res: any) => {
          toastr.success(res.message || 'Deposit requests updated successfully' );
          this.depositRequest = this.depositRequest.map((f: any) => {
            if(f.id == transaction.id) {
              f.status = status;
            }
            return f;
          });
  
          if(res.record.wallet){
            const wallets = this.adminAuthService?.walletData?.value.map((m: any) => {
              if (m.id == res.record.wallet.id) {
                m.amount = res.record.wallet.amount;
              }
              return m;
            });
            // this.admin.wallets = this.admin.wallets.map((m: any) => {
            //   if (m.id == res.record.wallet.id) {
            //     m.amount = res.record.wallet.amount;
            //   }
            //   return m;
            // });
            this.adminAuthService.walletData.next(wallets);
          }
          this.getDepositRequests();
        });

      }

    });
  }  

  filterSelectTimeZone(zonename: any) {
    // this.p = 1;

    if(zonename) {
      const zone = TIMEZONE.find(t => t.zonename === zonename);
      this.zone = zone?.name;
      this.params = {...this.params, time_zone: zone?.value, time_zone_name: zone?.zonename};
    }else{
      this.zone='';
      this.params = {...this.params, time_zone: this.zone};
    }

    // this.getDepositRequests();
}

openRecieptModal(transaction:any){
  this.selectedTransactionReciept = transaction.transaction_receipt;
  $('#reciept-modal').modal('show')
}

closeRecieptModal(){
  $('#reciept-modal').modal('hide')
  this.selectedTransactionReciept = null;
}

getNumArr(length: number) {
  return Array.from({ length }).map((_, i) => i);
 }
selectVipLevelSearch(vip_level: any) {
  this.params= {
    ...this.params,
    vip_levels: vip_level
  }
}

setOrder(sort: any) {
  this.p =1;
  this.params = { ...this.params, page: this.p, order: sort.order, sort_by: sort.column };
  this.getDepositRequests();
}
}
