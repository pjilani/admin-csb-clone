import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DepositRequestsComponent } from './deposit-requests.component';
import { RouterModule, Routes } from '@angular/router';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { SharedModule } from 'src/app/shared/shared.module';

const WRRoutes: Routes = [
  { path: '', component: DepositRequestsComponent }
];

@NgModule({
  declarations: [
    DepositRequestsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(WRRoutes),
    DirectivesModule,
    ComponentsModule,
    SharedModule
  ]
})

export class DepositRequestsModule { }
