import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })

export class DepositRequestsService {

  constructor(private http: HttpClient) { }

  getDepositRequests(params: any) {
    return this.http.post(`admin/transactions/deposit/request/lists`, params);
  }

  downloadDepositRequests(params: any) {
    return this.http.post(`admin/transactions/deposit/request/download`, params);
  }
  
  updateDRStatus(params: any) {
    return this.http.put(`admin/transactions/deposit/update/status`, params);
  }

}
