import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminAuthService } from '../../services/admin-auth.service';
import { CustomValidators } from 'src/app/shared/validators';
import { encryptPassword } from 'src/app/services/utils.service';
import { Role } from 'src/app/models';
declare const $: any;
declare const toastr: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit, AfterViewInit, OnDestroy {

  hideShowPass: boolean = false;
  
  returnUrl: string = '/';
  loginForm: FormGroup | any;
  submitted: boolean = false;
  loginLoader: boolean = false;
  roles:string = '';

  tenantDetails:any;
  
  constructor(private formBuilder: FormBuilder,
    private adminAuthService: AdminAuthService,
    private router: Router,
    private route: ActivatedRoute,
    ) {

      this.loginForm = this.formBuilder.group({
        email: ['', [ Validators.required, Validators.pattern(CustomValidators.emailRegEx) ]],
        password: ['', [ Validators.required ]],
        remember_me: [ false ],
      });
    
      const AAdata = localStorage.getItem('AADATA') || '';

      if (AAdata && AAdata.length > 0) {
        const jsonAAdata = JSON.parse(atob(AAdata));
        this.loginForm.patchValue(jsonAAdata);
      }
    
  }

  ngOnInit(): void {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  ngAfterViewInit(): void {
    $(document.body).addClass('hold-transition login-page');

    this.getTenantsDetails();
  }

  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) return;

    if (this.f.remember_me.value) {
      
      const AAdata = btoa( JSON.stringify({
        email: this.f.email.value,
        password: this.f.password.value,
        remember_me: this.f.remember_me.value
      }) );
      
      localStorage.setItem('AADATA', AAdata);
    } else {
      localStorage.removeItem('AADATA');
    }
    
    const data = this.loginForm.value;
    data.password = encryptPassword(data.password);

    this.loginLoader = true;
    this.adminAuthService.login(data)
      .subscribe((response: any) => {
        this.roles = localStorage.getItem('roles') || '';
        const roles = JSON.parse(this.roles);
        // if(roles && (roles.findIndex( (role: any) => role === Role.DepositAdmin ) > -1 || roles.findIndex( (role: any) => role === Role.WithdrawalAdmin ) > -1))
        // {
          // this.router.navigate([ '/agents' ]);
        // }else{
          this.router.navigate([ this.returnUrl ]);
        // }
      }, (err: any) => {
        this.loginForm.reset();
        this.loginForm.get('password').clearValidators();
        this.loginForm.get('password').updateValueAndValidity();
        this.loginForm.get('email').clearValidators();
        this.loginForm.get('email').updateValueAndValidity();
        this.loginLoader = false; 
      });
  }

  ngOnDestroy(): void {
    $(document.body).removeClass('login-page');
  }

  getTenantsDetails(){
    this.adminAuthService.getTenantsDetails().subscribe((res:any) => {
      this.tenantDetails = res.record;
      $("#appIcon").attr("href", this.tenantDetails.logo_url ?? "favicon.ico");  
    });
  }

}
