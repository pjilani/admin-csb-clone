import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CasinoService } from '../casino.service';
import { TenantService } from 'src/app/modules/tenant/tenant.service';
declare const $: any;
declare const toastr: any;

@Component({
  selector: 'app-page-list',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit{
  
  pages: any[] = [];
  page!: any;

  pageForm: FormGroup | any;
  submitted: boolean = false;
  pageLoader: boolean = false;

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Casino Top Menu', path: '/casino/pages' }
  ];

  constructor(
    private casinoService: CasinoService,public tenantService: TenantService) { }
    
  ngOnInit(): void {
    this.getPages();
  }

  getPages() {
    this.tenantService.getAdminMenuSetting().subscribe((res: any) => {
      this.pages = res.record;
      this.pages = this.pages.filter(e => e.menu_name !== 'Sports' && e.menu_name !== 'Promotion' );
    });
  }



}
