import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PageListComponent } from './page-list/page-list.component';
import { PagesComponent } from './pages/pages.component';
import { PageDetailsComponent } from './page-details/page-details.component';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { ItemsComponent } from './items/items.component';
import { MenusComponent } from './menus/menus.component';
import { PageMenuItemsComponent } from './page-menu-items/page-menu-items.component';

const casinoRoutes: Routes = [
  { path: '', redirectTo: 'pages', pathMatch: 'full' },
  { path: 'pages-menu/pages/:id', component: PageListComponent },
  { path: 'pages-menu', component: PagesComponent },
  { path: 'menus', component: MenusComponent },
  { path: 'items', component: ItemsComponent },
  { path: 'pages-menu/pages/:pageMenuId/:pageId', component: PageDetailsComponent },
  { path: 'pages-menu/pages/:pageMenuId/:pageId/:menuId', component: PageMenuItemsComponent },
]

@NgModule({
  declarations: [
    ItemsComponent,
    PagesComponent,
    MenusComponent,
    PageListComponent,
    PageDetailsComponent,
    PageMenuItemsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(casinoRoutes),
    ComponentsModule,
    DirectivesModule,
    SharedModule
  ]
})

export class CasinoModule { }
