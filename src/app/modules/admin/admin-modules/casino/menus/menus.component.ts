import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Menu } from 'src/app/models';
import { CasinoService } from 'src/app/modules/admin/admin-modules/casino/casino.service';
import { PermissionService } from 'src/app/services/permission.service';
import { CustomValidators } from 'src/app/shared/validators';

import Swal from 'sweetalert2';

declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './menus.component.html',
  styleUrls: ['./menus.component.scss']
})

export class MenusComponent implements OnInit {
  @ViewChild('file') logo_file!: ElementRef;
  imgURL: any;
  
  menus: Menu[] = [];
  menu: Menu | null = null;

  selectLogo: File | null = null;

  pageForm: FormGroup | any;
  submitted: boolean = false;
  pageLoader: boolean = false;

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Casino Menus', path: '/casino/menus' }
  ];

  constructor(
    private formBuilder: FormBuilder,
    private casinoService: CasinoService,
    public permissionService: PermissionService) { 
      this.pageForm = this.formBuilder.group({
        name: ['', [ Validators.required ]],
        logo: ['', [ Validators.required, CustomValidators.requiredFileType(CustomValidators.imgType)] ],
        enabled: [false]
      });
    }
    
  ngOnInit(): void {
    this.getMenus();
  }
  
  get f() {
    return this.pageForm.controls;
  }

  selectImage(files: any) {
    if (files.length === 0)
    return;
    const extension = files[0].name.split('.')[1].toLowerCase();
    const imgt = CustomValidators.imgType.includes(extension);

    if(imgt) {
      // this.selectLogo = files[0];
        const reader = new FileReader();
        // this.imagePath = files;
        reader.readAsDataURL(files[0]); 
        reader.onload = (_event) => { 
          this.imgURL = reader.result; 
        }
        this.selectLogo = files[0];
      } else {
        this.imgURL = '';
      }

  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.pageForm.invalid) return;
    
    // console.log(this.pageForm.value);

    const data = this.pageForm.value;
    const fd = new FormData();
    
    delete data.logo;

    if(this.selectLogo && this.selectLogo.name) {
      fd.append('image', this.selectLogo, this.selectLogo.name);
    }

    for(let key in data) {
      fd.append(key, data[key]);
    }

    this.pageLoader = true;
    if(this.menu && this.menu.id > 0) {
    this.casinoService.updateAdminMenu(this.menu.id, fd)
      .subscribe((res: any) => {
        toastr.success(res.message || 'Menu updated successfully.');

          this.menus = this.menus.map((m: Menu)=> {
            if(this.menu && this.menu.id === m.id) {
              m.name = this.f.name.value;
              m.enabled = this.f.enabled.value;
              m.image_url = res.record && res.record.image_url ? res.record.image_url : m.image_url ;
            }
            return m;
          });         
          
          this.resetPageForm();
        
      }, (err: any) => {
        this.pageLoader = false; 
      });

    } else {

      this.casinoService.createAdminMenu(fd)
      .subscribe((res: any) => {
        toastr.success(res.message || 'Menu created successfully.');

        this.resetPageForm();
        
        if(res?.record?.id) {
            this.menus.push(res.record);
        } else {
          this.getMenus();
        }
        
      }, (err: any) => {
        this.pageLoader = false; 
      });
      
    }
  }

  resetPageForm() {
    $('#modal-page').modal('hide');
    this.pageForm.reset();
    this.submitted = false;
    this.pageLoader = false;
  }

  getMenus() {
    this.casinoService.getAdminMenus().subscribe((res: any) => {
      this.menus = res.record;
    });
  }

  deleteMenu(menu: Menu) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to delete ${menu.name}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {

        this.casinoService.deleteAdminMenu(menu.id).subscribe((res: any) => {
          this.menus = this.menus.filter(f => f.id != menu.id);
          toastr.success(res.message || 'Menu deleted successfully.')
        });
        
      }
    });

  }

  selectMenu(menu: Menu) {
    this.menu = menu;
    const logoControl = this.pageForm.get('logo');
      logoControl.setValidators([ CustomValidators.requiredFileType(CustomValidators.imgType)]); 
      logoControl.updateValueAndValidity();
    this.pageForm.patchValue({
      name: menu.name,
      enabled: menu.enabled
    });
    this.imgURL = menu.image_url;
    this.selectLogo = null;
  }

  createNew() {
    this.menu = null;
    this.submitted = false;
    this.pageForm.patchValue({
      name: '',
      logo: '',
      enabled: false
    });
    this.imgURL = '';
    this.selectLogo = null;

    const logoControl = this.pageForm.get('logo');
      logoControl.setValidators([ Validators.required, CustomValidators.requiredFileType(CustomValidators.imgType)]); 
      logoControl.updateValueAndValidity();
  }
  
}
