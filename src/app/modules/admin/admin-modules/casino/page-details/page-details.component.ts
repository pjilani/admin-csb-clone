import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Menu } from 'src/app/models';
import { CasinoService } from '../casino.service';

import Swal from 'sweetalert2';
import { PermissionService } from 'src/app/services/permission.service';

declare const $: any;
declare const toastr: any;

@Component({
  selector: 'app-page-details',
  templateUrl: './page-details.component.html',
  styleUrls: ['./page-details.component.scss']
})

export class PageDetailsComponent implements OnInit {

  pageId: number = 0;
  topMenuId: number = 0;

  menus: any[] = [];
  menu: Menu | null = null;
  page!: any;

  drpMenus: Menu[] = [];

  pageForm: FormGroup | any;
  submitted: boolean = false;
  pageLoader: boolean = false;

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Casino Top Menus', path: '/casino/pages-menu' }
  ];

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private casinoService: CasinoService,
    public permissionService: PermissionService) { 
      this.pageForm = this.formBuilder.group({
        page_id: ['', [ Validators.required ]],
        name: ['', [ Validators.required ]],
        casino_menu_id: ['', [ Validators.required ]]
      });

      this.pageId = this.route.snapshot.params['pageId'];
      this.topMenuId = this.route.snapshot.params['pageMenuId'];
      this.f.page_id.setValue(this.pageId);
      this.breadcrumbs.push({ title: 'Casino Pages', path: `/casino/pages-menu/pages/${this.topMenuId}` });
      this.breadcrumbs.push({ title: 'Menus', path: `/casino/pages-menu/pages/${this.pageId}` });
    }
    
  ngOnInit(): void {
    this.getPage(this.pageId);
    this.getMenus();
  }
  
  ngAfterViewInit(): void {
    if(!this.isTouchDevice && this.permissionService.checkPermission('casino_management','U')){
      const that = this;

      $('#str tbody').sortable({
        helper: '.handle',
        zIndex: 999999,
        // items : "> [data-order]",
        update : function() {

          const ids = $(this).children().get().map(function(el: any) {
            return el.id;
          });
          that.updateOrder(ids);
        }
      }).disableSelection();
    }
  }

  updateOrder(idsArr: Array<string>) {
    this.casinoService.updatePageMenuOrder({ orders: idsArr }).subscribe((res: any) => {
      toastr.success(res.message || 'Page Menus orders updated successfully.');
    });
  }

  get f() {
    return this.pageForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.pageForm.invalid) return;
    
    this.pageLoader = true;
    if(this.menu && this.menu.id > 0) {
    this.casinoService.updateAdminPageMenu(this.menu.id, this.pageForm.value)
      .subscribe((res: any) => {
        toastr.success(res.message || 'Page Menu updated successfully.');
        
        this.menus = this.menus.map((m: any)=> {
          if(this.menu && this.menu.id === m.id) {
            m.name = this.f.name.value;
            m.casino_menu_id = this.f.casino_menu_id.value;
          }
          return m;
        });

        this.resetPageForm();
          
      }, (err: any) => {
        this.pageLoader = false; 
      });

    } else {

      this.casinoService.createAdminPageMenu(this.pageForm.value)
      .subscribe((res: any) => {
        toastr.success(res.message || 'Page Menu created successfully.');
        
        this.resetPageForm();
        
        if(res?.record?.id) {
            this.menus.push(res.record);
        } else {
          this.getPage(this.pageId);
        }
        
      }, (err: any) => {
        this.pageLoader = false; 
      });
      
    }
  }

  resetPageForm() {
    $('#modal-page').modal('hide');
    // this.pageForm.reset();
    this.pageLoader = false;
    this.submitted = false;
  }

  getPage(id: number) {
    this.casinoService.getAdminPageById(id).subscribe((res: any) => {
      this.page = res.record;
      this.menus = this.page && this.page.pages_manu ? this.page.pages_manu : [];
    });
  }

  getMenus() {
    this.casinoService.getAdminMenus().subscribe((res: any) => {
      this.drpMenus = res.record;
    });
  }

  deletePageMenu(menu: Menu) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to delete ${menu.name}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, delete it!`
    }).then((result) => {
      if (result.isConfirmed) {

        this.casinoService.deleteAdminPageMenu(menu.id).subscribe((res: any) => {
          this.menus = this.menus.filter(f => f.id != menu.id);
          toastr.success(res.message || 'Page Menu deleted successfully.')
        });
        
      }
    });

  }

  createPageForm() {
    this.menu = null;
    this.submitted = false;
    this.pageForm.patchValue({
      name: '',
      casino_menu_id: ''
    });
  }

  selectPage(menu: any) {
    this.menu = menu;
    this.pageForm.patchValue({
      name: menu.name,
      casino_menu_id: menu.casino_menu_id
    });
  }

  get isTouchDevice() {
    const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    return width < 1025;
  }

}
