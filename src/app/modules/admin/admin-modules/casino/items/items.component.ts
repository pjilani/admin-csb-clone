import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Item } from 'src/app/models';
import { CasinoService } from 'src/app/modules/admin/admin-modules/casino/casino.service';
import { PermissionService } from 'src/app/services/permission.service';
import { PageSizes } from 'src/app/shared/constants';
import { CustomValidators } from 'src/app/shared/validators';

import Swal from 'sweetalert2';

declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})

export class ItemsComponent implements OnInit {

  @ViewChild('file') logo_file!: ElementRef;
  Uuids: any[] = [];
  imgURL: any;
  
  items: Item[] = [];
  item: Item | null = null;

  selectLogo: File | null = null;;

  pageForm: FormGroup | any;
  submitted: boolean = false;
  pageLoader: boolean = false;

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Casino Tables', path: '/casino/items' }
  ];

  pageSizes: any[] = PageSizes;
  
  dataTableParams: any = {
    searching: false,
    info: true,
    lengthChange: true,
    autoWidth: false,
    responsive: true,
    "dom": '<"top"lp>t<"bottom"ifp><"clear">',
    lengthMenu: PageSizes.map(m => m.name),
    buttons: [{ extend: 'csv', text: 'Export as CSV', className: 'ctsmcsv mgt btn btn-info mt-0' }]
  };

  itemParams: any = {
    search: '',
    size: 10,
    page: 1,
  };

  itemP: number = 1;
  itemTotal: number = 0;

  isLoader:boolean = false;
  firstTimeApiCall = false;

  constructor(
    private formBuilder: FormBuilder,
    private casinoService: CasinoService,
    public permissionService: PermissionService) { 
      this.pageForm = this.formBuilder.group({
        name: ['', [ Validators.required ]],
        logo: ['', [ Validators.required, CustomValidators.requiredFileType(CustomValidators.imgType)] ],
        uuid: ['', [ Validators.required ]],
        active: [false]
      });

    }
    
  ngOnInit(): void { 
    // this.getItems();
    this.getUuids();
  }
  
  get f() {
    return this.pageForm.controls;
  }

  selectImage(files: any) {
    if (files.length === 0)
    return;
    const extension = files[0].name.split('.')[1].toLowerCase();
    const imgt = CustomValidators.imgType.includes(extension);

    if(imgt) {
      // this.selectLogo = files[0];
        const reader = new FileReader();
        // this.imagePath = files;
        reader.readAsDataURL(files[0]); 
        reader.onload = (_event) => { 
          this.imgURL = reader.result; 
        }
        this.selectLogo = files[0];
      } else {
        this.imgURL = '';
      }

  }

  selectUuid(uuid: any) {
    this.f.uuid.setValue(uuid);
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
  
    // console.log(this.pageForm.value);
  
    if (this.pageForm.invalid) return;
  
    const data = this.pageForm.value;
    const fd = new FormData();
    
    delete data.logo;
    delete data.theme;

    if(this.selectLogo && this.selectLogo.name) {
      fd.append('image', this.selectLogo, this.selectLogo.name);
    }

    for(let key in data) {
      fd.append(key, data[key]);
    }

    this.pageLoader = true;
    if(this.item && this.item.id > 0) {
    this.casinoService.updateAdminItem(this.item.id, fd)
      .subscribe((res: any) => {
        toastr.success(res.message || 'Setting updated successfully.');
        this.pageLoader = false;
        $('#modal-page').modal('hide');
        
        // if(res?.record?.id) {

            this.items = this.items.map((m: Item)=> {
              if(this.item && this.item.id === m.id) {
                m.name = this.f.name.value;
                m.image = res?.record?.image || m.image;
                m.uuid = this.f.uuid.value;
                m.active = this.f.active.value;
              }
              return m;
            });
          
        // } else {
        //   this.getItems();
        // }
        
      }, (err: any) => {
        this.pageLoader = false; 
      });

    } else {

      this.casinoService.createAdminItem(fd)
      .subscribe((res: any) => {
        toastr.success(res.message || 'Setting created successfully.');
        this.pageLoader = false;
        $('#modal-page').modal('hide');
        this.resetPageForm();
        
        this.getItems();
        
      }, (err: any) => {
        this.pageLoader = false; 
      });
      
    }
  }

  resetPageForm() {
    this.pageForm.reset();
    this.submitted = false;
  }

  createItemsForm() {
    this.item = null;
    this.submitted = false;
    this.pageForm.patchValue({
      name: '',
      logo: '',
      uuid: this.Uuids[0].table_id,
      active: false,
    });

    const logoControl = this.pageForm.get('logo');
      logoControl.setValidators([ Validators.required, CustomValidators.requiredFileType(CustomValidators.imgType)]); 
      logoControl.updateValueAndValidity();

    this.imgURL = '';
    this.selectLogo = null;

    $(`#uuid`).val(this.Uuids[0].table_id).trigger('change');
  }

  getItems() {
    this.isLoader = true
    this.firstTimeApiCall = true;
    this.casinoService.getAdminCasinoItems(this.itemParams).subscribe((res: any) => {
      this.items = res.record.data;
      this.itemTotal = res.record.total;
      this.isLoader = false;
    },
    (error:any) => {
      this.isLoader = false

    });
  }

  itemPageChanged(page: number) {
    this.itemParams = { ...this.itemParams, page };
    this.getItems();
  }

  filterItems(evt: any) {
    this.itemP = 1;
    this.itemParams = { ...this.itemParams, page: this.itemP };
    this.getItems();
  }

  itemResetFilter() {
    this.itemParams = {
      search: '',
      page:1,
      size:10
    };
    this.getItems();
  }

  getUuids() {
    this.casinoService.getAdminItemsTable().subscribe((res: any) => {
      this.Uuids = res.record;
      $(`#uuid`).val(this.Uuids[0].table_id).trigger('change');
      this.selectUuid(this.Uuids[0].table_id);
    });
  }

  deleteMenu(item: Item) {

    Swal.fire({
      title: 'Are you sure?',
      text: `You want to delete ${item.name}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {

        this.casinoService.deleteAdminItem(item.id).subscribe((res: any) => {
          this.items = this.items.filter(f => f.id != item.id);
          toastr.success(res.message || 'Item deleted successfully.')
        });
        
      }
    });
  }

  selectItem(item: Item) {
    this.item = item;
    this.pageForm.patchValue({
      name: item.name,
      uuid: item.uuid ,
      active: item.active 
    });
    this.imgURL = item.image;
    $(`#uuid`).val(item.uuid).trigger('change');
    this.selectLogo = null;

    const logoControl = this.pageForm.get('logo');
      logoControl.setValidators([ CustomValidators.requiredFileType(CustomValidators.imgType)]); 
      logoControl.updateValueAndValidity();
  }

}
