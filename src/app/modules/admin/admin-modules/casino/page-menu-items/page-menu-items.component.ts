import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Item, Menu } from 'src/app/models';
import { CasinoService } from '../casino.service';

import Swal from 'sweetalert2';
import { PageSizes } from 'src/app/shared/constants';
import { PermissionService } from 'src/app/services/permission.service';

declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './page-menu-items.component.html',
  styleUrls: ['./page-menu-items.component.scss']
})

export class PageMenuItemsComponent implements OnInit {

  pageId: number = 0;
  menuId: number = 0;
  topMenuId: number = 0;

  items: any[] = [];
  item: Item | null = null;

  menu: Menu | null = null;
  page!: any;

  drpItems: Item[] = [];

  pageForm: FormGroup | any;
  submitted: boolean = false;
  pageLoader: boolean = false;

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Casino Top Menus', path: '/casino/pages-menu' }
  ];

  pageSizes: any[] = PageSizes;

  menuItemsParams: any = {
    search: '',
    size: 10,
    page: 1,
  };

  menuItemsP: number = 1;
  menuItemsTotal: number = 0;

  isLoader:boolean = false;
  firstTimeApiCall = false;

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private casinoService: CasinoService,
    public permissionService: PermissionService) { 
      this.pageForm = this.formBuilder.group({
        page_id: ['', [ Validators.required ]],
        menu_id: ['', [ Validators.required ]],
        name: ['', [ Validators.required ]],
        casino_item_id: ['', [ Validators.required ]],
        active: [false],
        featured: [false],
        popular: [false]
      });

      this.topMenuId = this.route.snapshot.params['pageMenuId'];
      this.pageId = this.route.snapshot.params['pageId'];
      this.menuId = this.route.snapshot.params['menuId'];

      this.f.page_id.setValue(this.pageId);
      this.f.menu_id.setValue(this.menuId);

      this.breadcrumbs.push({ title: 'Casino Pages', path: `/casino/pages-menu/pages/${this.topMenuId}` });
      this.breadcrumbs.push({ title: 'Menus', path: `/casino/pages-menu/pages/${this.topMenuId}/${this.pageId}` });
      this.breadcrumbs.push({ title: 'Items', path: `/casino/pages-menu/pages/${this.pageId}/${this.menuId}` });
      
    }
    
  ngOnInit(): void {
    this.getMenu(this.pageId, this.menuId);
    this.getItems();
    // this.getMenuItems()
  }
  
  ngAfterViewInit(): void {
    if(!this.isTouchDevice && this.permissionService.checkPermission('casino_management','U')){
      const that = this;

      $('#str tbody').sortable({
        helper: '.handle',
        zIndex: 999999,
        // items : "> [data-order]",
        update : function() {
  
          const ids = $(this).children().get().map(function(el: any) {
            return el.id;
          });
  
          // const orders = $(this).sortable('toArray', {
          //   attribute: 'data-order'
          // });
  
          // console.log(ids);
          // console.log(orders);
  
          that.updateOrder(ids);
        }
      }).disableSelection();
    }
  }

  updateOrder(idsArr: Array<string>) {
    this.casinoService.updatePageMenuItemOrder({ orders: idsArr }).subscribe((res: any) => {
      toastr.success(res.message || 'Page Menu Items orders updated successfully.');
    });
  }

  get f() {
    return this.pageForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    // console.log(this.pageForm.value);
    if (this.pageForm.invalid) return;
    
    this.pageLoader = true;
    if(this.item && this.item.id > 0) {
    this.casinoService.updateAdminPageMenuItem(this.item.id, this.pageForm.value)
      .subscribe((res: any) => {
        toastr.success(res.message || 'Page Menu Item updated successfully.');
        
        this.items = this.items.map((m: any) => {
          if(this.item && this.item.id === m.id) {
            m.name = this.f.name.value;
            m.casino_item_id = this.f.casino_item_id.value;
            m.active = this.f.active.value;
            m.featured = this.f.featured.value;
            m.popular = this.f.popular.value;
            m.item = res.record.item
          }
          return m;
        });

        this.resetPageForm();
          
      }, (err: any) => {
        this.pageLoader = false; 
      });

    } else {

      this.casinoService.createAdminPageMenuItem(this.pageForm.value)
      .subscribe((res: any) => {
        toastr.success(res.message || 'Page Menu Item created successfully.');
        
        this.resetPageForm();
        
        // if(res?.record?.id) {
        //     this.items.push(res.record);
        // } else {
          this.getMenuItems();
        // }
        
      }, (err: any) => {
        this.pageLoader = false; 
      });
      
    }
  }

  resetPageForm() {
    $('#modal-page').modal('hide');
    // this.pageForm.reset();
    this.pageLoader = false;
    this.submitted = false;
  }

  getMenu(id: number, menuId: number) {
    this.casinoService.getAdminPageMenuById(id, menuId).subscribe((res: any) => {
      this.page = res.record;
      this.menu = res.record.page_menu;
    });
  }

  menuItemsPageChanged(page: number) {
    this.menuItemsParams = { ...this.menuItemsParams, page };
    this.getMenuItems();
  }

  filterMenuItems(evt: any) {
    this.menuItemsP = 1;
    this.menuItemsParams = { ...this.menuItemsParams, page: this.menuItemsP };
    this.getMenuItems();
  }


  getMenuItems() {
    this.isLoader = true
    this.firstTimeApiCall = true;
    let data = {
      menu_id: this.menuId,
      ...this.menuItemsParams
    }

    this.casinoService.getAdminPageMenuItems(data).subscribe((res: any) => {
      this.items = res.record.data;
      this.menuItemsTotal = res.record.total
      this.isLoader = false;
    }
    ,     (error:any) => {
      this.isLoader = false

    });
  }

  menuItemsResetFilter() {
    this.menuItemsParams = {
      search: '',
      page:1,
      size:10
    };
    this.getMenuItems();
  }

  getItems() {
    this.casinoService.getAdminItems().subscribe((res: any) => {
      this.drpItems = res.record;
          });
  }

  deletePage(item: Item) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to delete ${item.name}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {

        this.casinoService.deleteAdminPageMenuItem(item.id).subscribe((res: any) => {
          this.items = this.items.filter(f => f.id != item.id);
          toastr.success(res.message || 'Page Menu Item deleted successfully.')
          this.getMenuItems()
        });
        
      }
    });
    
  }

  createPageForm() {
    this.item = null;
    this.submitted = false;
    this.pageForm.patchValue({
      page_id: this.pageId,
      menu_id: this.menuId,
      name: '',
      casino_item_id: '',
      active: false,
      featured: false
    });
  }

  selectPage(item: any) {
    this.item = item;
    this.pageForm.patchValue({
      page_id: this.pageId,
      menu_id: this.menuId,
      name: item.name,
      casino_item_id: item.casino_item_id,
      active: item.active,
      featured: item.featured,
      popular: item.popular
    });

    $(`#casino_item_id`).val(item.casino_item_id).trigger('change');
  }

  selectItem(casino_item_id: any) {
    this.f.casino_item_id.setValue(casino_item_id);
  }

  get isTouchDevice() {
    try{ document.createEvent("TouchEvent"); return true; }
    catch(e){ return false; }
  }

}
