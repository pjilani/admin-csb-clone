import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CasinoService } from '../casino.service';
import Swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
import { Item } from 'src/app/models';
import { PermissionService } from 'src/app/services/permission.service';
import { CustomValidators } from 'src/app/shared/validators';
declare const $: any;
declare const toastr: any;

@Component({
  selector: 'app-page-list',
  templateUrl: './page-list.component.html',
  styleUrls: ['./page-list.component.scss']
})
export class PageListComponent implements OnInit, AfterViewInit{
  
  pages: any[] = [];
  page!: any;
  topPageId:any;
  imgURL: any;
  selectLogo!: File;
  pageForm: FormGroup | any;
  submitted: boolean = false;
  pageLoader: boolean = false;
  drpItems: Item[] = [];
  
  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Casino Top Menus', path: '/casino/pages-menu' }
  ];

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private casinoService: CasinoService,
    public permissionService: PermissionService
    ) {
        
      this.topPageId = this.route.snapshot.params['id'];
      if(!this.topPageId){
        this.router.navigate(['/pages-menu']);
      }
     
      this.pageForm = this.formBuilder.group({
        top_menu_id: [''],
        image: [''],
        enable_instant_game: [''],
        title: ['', [ Validators.required ]],
        enabled: [false]
      });

      this.breadcrumbs.push({ title: 'Casino Pages', path: `/casino/pages` });


    }
    
  ngOnInit(): void {

    this.getPages(this.topPageId);
    this.getItems();
  }

  getItems() {
    this.casinoService.getAdminItems().subscribe((res: any) => {
      this.drpItems = res.record;
    });
  }
  
  ngAfterViewInit(): void {
  // jQuery UI sortable for the todo list
  if(!this.isTouchDevice && this.permissionService.checkPermission('casino_management','U')){
    const that = this;
    
    $('table tbody').sortable({
      helper: '.handle',
      zIndex: 999999,
      // items : "> [data-order]",
      update : function() {

        const ids = $(this).children().get().map(function(el: any) {
          return el.id;
        });

        // const orders = $(this).sortable('toArray', {
        //   attribute: 'data-order'
        // });

        // console.log(ids);
        // console.log(orders);
        that.updateOrder(ids);
      }
    }).disableSelection();
  }

  }

  updateOrder(idsArr: Array<string>) {
    this.casinoService.updatePageOrder({ orders: idsArr }).subscribe((res: any) => {
      toastr.success(res.message || 'Pages orders updated successfully.');
    });
  }

  get f() {
    return this.pageForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.pageForm.invalid) return;

    if(this.page && this.page.id > 0) {
      const data = this.pageForm.value;
      const fd = new FormData();
      delete data.image;
      delete data.top_menu_id;
      fd.append('id', `${this.page.id}`);
      fd.append('top_menu_id', `${this.topPageId}`);
      if (this.selectLogo && this.selectLogo.name) {
        fd.append('image', this.selectLogo, this.selectLogo.name);
      }
  
      for (let key in data) {
        fd.append(key, data[key]);
      }
      this.pageLoader = true;
    this.casinoService.updateAdminPage(fd)
      .subscribe((res: any) => {
        toastr.success(res.message || 'Page updated successfully.');
        
        this.getPages(this.topPageId);

        this.resetPageForm();
        this.imgURL = '';
          
      }, (err: any) => {
        this.pageLoader = false; 
      });

    } else {

      const data = this.pageForm.value;
      const fd = new FormData();
      delete data.image;
      delete data.top_menu_id;
      fd.append('top_menu_id', `${this.topPageId}`);
      if (this.selectLogo && this.selectLogo.name) {
        fd.append('image', this.selectLogo, this.selectLogo.name);
      }
  
      for (let key in data) {
        fd.append(key, data[key]);
      }
      this.pageLoader = true;

      this.casinoService.createAdminPage(fd)
      .subscribe((res: any) => {
        toastr.success(res.message || 'Page created successfully.');
        
        this.resetPageForm();
        
        // if(res?.record?.id) {
        //     this.pages.push(res.record);
        // } else {
          this.getPages(this.topPageId);
          this.imgURL = '';
        // }
        
      }, (err: any) => {
        this.pageLoader = false; 
      });
      
    }
  }

  selectImage(files: any) {
    this.imgURL = '';
    if (files.length === 0)
      return;

    if ((files[0].size / 1000000) > 5) {

      this.f.image.setErrors({ 'required': 'File is required' });
      toastr.error( 'Max file size limit reached');
      // toastr.error(`Max file size is 5MB`);
      return;
    }

    const extension = files[0].name.split('.')[1].toLowerCase();
    let check = CustomValidators.imgType.includes(extension);

    if (check) {

      const reader = new FileReader();
      reader.readAsDataURL(files[0]);
      const img = new Image();
      img.src = window.URL.createObjectURL( files[0] );
      reader.onload = (_event) => {
      //   const width = img.naturalWidth;
      //    const height = img.naturalHeight;
      //    if( width > 30 || height > 30 ) {
      //       toastr.error("Icon should be less then 30 x 30 size");
      //     this.f.logo.setErrors({ 'dimension': 'Icon should be less then 30 x 30 size' });
      //     this.imgURL = '';
      //  }

        this.imgURL = reader.result;
      }
      this.selectLogo = files[0];
    } else {
      this.imgURL = '';
      toastr.error( 'Image extension should be svg or png');
      // toastr.error(`Please select ${this.f.banner_type.value}`);
    }

  }

  resetPageForm() {
    $('#modal-page').modal('hide');
    this.pageForm.reset();
    this.pageLoader = false;
    this.submitted = false;
  }

  getPages(pageId:any) {
    this.casinoService.getAdminPages(pageId).subscribe((res: any) => {
      this.pages = res.record;
    });
  }

  deletePage(page: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to delete ${page.title}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {

        this.casinoService.deleteAdminPage(page.id).subscribe((res: any) => {
          this.pages = this.pages.filter(f => f.id != page.id);
          toastr.success(res.message || 'Page deleted successfully.')
        });
        
      }
    });
  }

  createPageForm() {
    this.page = null;
    this.imgURL = null;
    this.submitted = false;
    this.pageForm.patchValue({
      title: '',
      enabled: true
    });
  }

  selectPage(page: any) {
    this.submitted = false;
    this.page = page;
    this.imgURL = this.page.image;
    this.pageForm.patchValue({
      title: page.title,
      enable_instant_game: page.uuid,
      enabled: page.enabled
    });
  }

  get isTouchDevice() {
    const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    return width < 1025;
  }

}
