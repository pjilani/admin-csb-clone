import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })

export class CasinoService {

  prefix: string = 'admin/casino/';

  constructor(private http: HttpClient) { }

  // Page
  createAdminPage(data: any) {
    return this.http.post(this.prefix + `pages/record/add`, data);
  }

  getAdminPages(id: number) {
    return this.http.get(this.prefix + `pages/byId/${id}`);
  }

  getAdminPageById(id: number) {
    return this.http.get(this.prefix + `pages/${id}`);
  }

  updateAdminPage(data: any) {
    return this.http.post(this.prefix + `pages/edit`, data);
  }

  deleteAdminPage(id: number) {
    return this.http.delete(this.prefix + `pages/${id}`);
  }

  updatePageOrder(data: any) {
    return this.http.put(this.prefix + `pages/update-order`, data);
  }

  // Page Menu
  createAdminPageMenu(data: any) {
    return this.http.post(this.prefix + `pages/record/page_menu/add`, data);
  }

  updateAdminPageMenu(id: number, data: any) {
    return this.http.post(this.prefix + `pages/record/page_menu/update/${id}`, data);
  }

  getAdminPageMenuById(id: number, menuId: number) {
    return this.http.get(this.prefix + `pages/record/page_menu/${id}/${menuId}`);
  }

  getAdminPageMenuItems(params:any){
    return this.http.post(this.prefix + `pages/record/page_menu_items`, params);
  }

  deleteAdminPageMenu(id: number) {
    return this.http.delete(this.prefix + `pages/record/page_menu/${id}`);
  }

  // Page Menu
  createAdminPageMenuItem(data: any) {
    return this.http.post(this.prefix + `pages/menu-item/add`, data);
  }

  updateAdminPageMenuItem(id: number, data: any) {
    return this.http.put(this.prefix + `pages/menu-item/update/${id}`, data);
  }

  updatePageMenuItemOrder(arr: any) {
    return this.http.put(this.prefix + `pages/menu-item/update-menu-item-order`, arr);
  }

  deleteAdminPageMenuItem(id: number) {
    return this.http.delete(this.prefix + `pages/menu-item/delete/${id}`);
  }

  // Menu
  createAdminMenu(data: any) {
    return this.http.post(this.prefix + `menus/add`, data);
  }

  getAdminMenus() {
    return this.http.get(this.prefix + `menus`);
  }

  updateAdminMenu(id: number, data: any) {
    return this.http.post(this.prefix + `menus/update/${id}`, data);
  }

  deleteAdminMenu(id: number) {
    return this.http.delete(this.prefix + `menus/${id}`);
  }

  updatePageMenuOrder(data: any) {
    return this.http.put(this.prefix + `pages/update-menu-order`, data);
  }

  // Item
  createAdminItem(data: any) {
    return this.http.post(this.prefix + `items/add`, data);
  }

  getAdminItems() {
    return this.http.get(this.prefix + `items`);
  }

  getAdminCasinoItems(params:any) {
    return this.http.post(this.prefix + `items/get`, params);
  }

  getAdminItemsTable() {
    return this.http.get(this.prefix + `items/tables`);
  }

  updateAdminItem(id: number, data: any) {
    return this.http.post(this.prefix + `items/edit/${id}`, data);
  }

  deleteAdminItem(id: number) {
    return this.http.delete(this.prefix + `items/${id}`);
  }

}
