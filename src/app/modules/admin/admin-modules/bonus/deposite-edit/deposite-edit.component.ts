import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomValidators } from 'src/app/shared/validators';
import { AdminAuthService } from '../../../services/admin-auth.service';
import { BonusService } from '../bonus.service';

declare const $: any; 
declare const toastr: any; 

@Component({
  templateUrl: './deposite-edit.component.html',
  styleUrls: ['./deposite-edit.component.scss']
})

export class DepositeEditComponent implements OnInit, AfterViewInit {
  @ViewChild('file') logo_file!: ElementRef;
  imgURL: any;

  format: string = "YYYY-MM-DD HH:mm:ss";

  startDate = new Date().toISOString().split('T')[0]+' 12:00:00';
  endDate = new Date().toISOString().split('T')[0]+' 23:59:59';

  kinds = [
    { name: 'Deposit for Casino', value: 'deposit' },
    { name: 'Deposit for Sport', value: 'deposit_sport' }
  ];

  id: number = 0;
  currencies: any[] = [];
  
  bonusForm: FormGroup | any;
  submitted: boolean = false;
  bonusLoader: boolean = false;
  selectLogo!: File;

  deposit: any | null = null;

  title: string = 'Create'

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Bonus Deposit', path: '/bonus/deposit-list' }
  ];

  constructor(private route: ActivatedRoute,
    private router: Router,
    private bonusService: BonusService,
    private formBuilder: FormBuilder,
    private adminAuthService: AdminAuthService
    ) {
     this.id = this.route.snapshot.params['id'];
    
    this.bonusForm = this.formBuilder.group({
      kind: ['', [ Validators.required ]],
      code: ['', [ Validators.required ]],
      percentage: ['', [ Validators.required, Validators.min(1) ]],
      currency_id: ['', [ Validators.required ]],
      vip_levels: [this.getNumArr(11), [ Validators.required ]],
      min_deposit: ['', [ Validators.required, Validators.min(1) ]],
      max_deposit: ['', [ Validators.required, Validators.min(1) ]],
      max_bonus: ['', [ Validators.required, Validators.min(1) ]],
      rollover_multiplier: ['', [ Validators.required, Validators.min(1) ]],
      max_rollover_per_bet: ['', [ Validators.required, Validators.min(1) ]],
      valid_for_days: ['', [ Validators.required, Validators.min(1) ]],
      valid_from: [this.startDate, [ Validators.required ]],
      valid_upto: [this.endDate, [Validators.required]],
      promotion_title: ['', [ Validators.required ]],
      logo: ['', [ CustomValidators.requiredFileType(CustomValidators.imgType) ]],
      terms_and_conditions: ['', [ Validators.required ]],
      // campaign_period: [''],
      enabled: [false],
    });



    if(this.id > 0) {
      this.title = 'Edit';

      this.getDeposit();
        
    } else {
      const logoControl = this.bonusForm.get('logo');
      logoControl.setValidators([Validators.required, CustomValidators.requiredFileType(CustomValidators.imgType)]); 
      logoControl.updateValueAndValidity();
    }

    this.breadcrumbs.push({ title: this.title, path: `/bonus/deposit/${this.id}` });

  }

  ngOnInit(): void {

      this.adminAuthService?.walletData.subscribe((wallets: any) => {
        this.currencies = wallets || [];
        
        this.currencies = this.currencies.map((m: any) => {
          m.id = m.currency_id;
          m.name = m.currency_name;
          return m;
        });

    });

    this.bonusForm.get('max_deposit').valueChanges
   .subscribe((value: number) => {
        const min = this.bonusForm.get('min_deposit').value


        if (value < parseFloat(min)) {
          this.bonusForm.get('max_deposit').setErrors({
            'minIsGreaterThanMax': true
          })
        }
   });

    // this.currenciesService.getAdminCurrencies().subscribe((res: any) => {
    //   this.currencies = res.record;
    // });
  }

  ngAfterViewInit(): void {
    $("#vip_levels").val(this.getNumArr(11)).trigger('change');
  }

  selectImage(files: any) {
    if (files.length === 0)
    return;
    const extension = files[0].type.split('/')[1].toLowerCase();
    const imgt = CustomValidators.imgType.includes(extension);

    if(imgt) {
      // this.selectLogo = files[0];
        const reader = new FileReader();
        // this.imagePath = files;
        reader.readAsDataURL(files[0]); 
        reader.onload = (_event) => { 
          this.imgURL = reader.result; 
        }
        this.selectLogo = files[0];
      } else {
        this.imgURL = '';
      }

  }

  selectDateRange(time_period: any) {
    // console.log(time_period);
    this.bonusForm.patchValue({
      valid_from: time_period.start_date,
      valid_upto: time_period.end_date
    });
  }

  getNumArr(length: number) {
   return Array.from({ length }).map((_, i) => i);
  }

  getDeposit() {
    this.bonusService.getDepositById(this.id).subscribe((res: any) => {
      this.deposit = res.record;
      
      this.deposit.vip_levels = JSON.parse(this.deposit.vip_levels.replace('{', '[').replace('}', ']'));

      // console.log(this.deposit);

      this.bonusForm.patchValue({
        kind: this.deposit.kind,
        code: this.deposit.code,
        percentage: this.deposit.percentage,
        currency_id: this.deposit.currency_id,
        vip_levels: this.deposit.vip_levels,
        min_deposit: this.deposit.setting.min_deposit,
        max_deposit: this.deposit.setting.max_deposit,
        max_bonus: this.deposit.setting.max_bonus,
        rollover_multiplier: this.deposit.setting.rollover_multiplier,
        max_rollover_per_bet: this.deposit.setting.max_rollover_per_bet,
        valid_for_days: this.deposit.setting.valid_for_days,
        valid_from: this.deposit.valid_from,
        valid_upto: this.deposit.valid_upto,
        promotion_title: this.deposit.promotion_title,
        terms_and_conditions: this.deposit.terms_and_conditions,
        enabled: this.deposit.enabled,
      });

      this.imgURL = this.deposit.image;

      setTimeout(() => {
        $("#vip_levels").val(this.deposit.vip_levels).trigger('change');
        $('#time_period').val(this.deposit.valid_from + ' - ' + this.deposit.valid_upto);
      }, 100);

    });
  }
  

  selectVipLevel(vip_levels: any) {
    this.f.vip_levels.setValue(vip_levels);
  }

  get f() {
    return this.bonusForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid

    // console.log(this.bonusForm.value);

    if (this.bonusForm.invalid) return;
    
    this.bonusLoader = true;

    const data = this.bonusForm.value;
    // data.vip_levels = JSON.stringify(data.vip_levels).replace('[', '{').replace(']', '}');
    data.vip_levels = JSON.stringify(data.vip_levels);
    
    const fd = new FormData();
    
    delete data.logo;

    if(this.selectLogo && this.selectLogo.name) {
      fd.append('image', this.selectLogo, this.selectLogo.name);
    }

    fd.append("campaign_period[valid_from]", this.f.valid_from.value);
    fd.append("campaign_period[valid_upto]", this.f.valid_upto.value);


    var valid_from = this.f.valid_from.value;
    var CurrentDate = new Date().getTime();
    valid_from = new Date(valid_from).getTime()    
    if(valid_from < CurrentDate){
      toastr.error('Campaign period from date should be greater than current date and time');
      this.bonusLoader = false;
      return;
    }



    delete data.valid_from;
    delete data.valid_upto;

    for(let key in data) {
      fd.append(key, data[key]);
    }
    
    if(this.id > 0) {
      this.bonusService.updateDeposit(this.id, fd)
      .subscribe((response: any) => {
        toastr.success(response?.message || 'Bonus Deposit Updated Successfully!');
        this.router.navigate(['/bonus/deposit-list']);
        this.bonusLoader = false;
      }, (err: any) => {
        this.bonusLoader = false;
      });

    } else {
      
      this.bonusService.createDeposit(fd)
      .subscribe((response: any) => {
        toastr.success(response?.message || 'Bonus Deposit Created Successfully!');
        this.router.navigate(['/bonus/deposit-list']);
        this.bonusLoader = false; 
      }, (err: any) => {
        this.bonusLoader = false; 
      });

    }
  }

}
