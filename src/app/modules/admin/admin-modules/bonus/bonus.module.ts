import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DepositeEditComponent } from './deposite-edit/deposite-edit.component';
import { LosingEditComponent } from './losing-edit/losing-edit.component';
import { JoiningEditComponent } from './joining-edit/joining-edit.component';
import { LosingListComponent } from './losing-list/losing-list.component';
import { JoiningListComponent } from './joining-list/joining-list.component';
import { DepositeListComponent } from './deposite-list/deposite-list.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { CKEditorModule } from 'ckeditor4-angular';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { PipeModule } from 'src/app/pipes/pipes.module';

const bonusRoutes: Routes = [
  { path: '', redirectTo: 'deposit-list', pathMatch: 'full' },
  { path: 'deposit-list', component: DepositeListComponent },
  { path: 'losing-list', component: LosingListComponent },
  { path: 'joining-list', component: JoiningListComponent },
  { path: 'deposit/:id', component: DepositeEditComponent },
  { path: 'losing/:id', component: LosingEditComponent },
  { path: 'joining/:id', component: JoiningEditComponent },
];

@NgModule({
  declarations: [
    DepositeEditComponent,
    LosingEditComponent,
    JoiningEditComponent,
    LosingListComponent,
    JoiningListComponent,
    DepositeListComponent
  ],
  imports: [
    CommonModule,
    DirectivesModule,
    CKEditorModule,
    RouterModule.forChild(bonusRoutes),
    SharedModule,
    PipeModule,
    ComponentsModule
  ]
})

export class BonusModule { }
