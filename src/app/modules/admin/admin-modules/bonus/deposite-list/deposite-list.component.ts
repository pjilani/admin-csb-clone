import { Component, OnInit } from '@angular/core';
import { Deposite } from 'src/app/models';
import { BonusService } from '../bonus.service';

import Swal from 'sweetalert2';
import { PermissionService } from 'src/app/services/permission.service';

declare const toastr: any;

@Component({
  templateUrl: './deposite-list.component.html',
  styleUrls: ['./deposite-list.component.scss']
})

export class DepositeListComponent implements OnInit {

  deposites: Deposite[] = [];
  deposite: Deposite | null = null;
  
  p: number = 1;
  total: number = 0;

  params: any = {
    size: 10,
    page: 1,
    kind: 'deposit'
  };

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Deposit Bonus', path: '/' },
  ];

  constructor(private bonusService: BonusService, public permissionService: PermissionService) { }

  ngOnInit(): void {
    this.getDeposit();
  }

  selectDateRange(time_period: any) {
    this.params = { ...this.params, time_period }
    this.getDeposit();
  }

  getDeposit() {
    this.bonusService.getBonus(this.params).subscribe((res: any) => {
      this.deposites = res.record.data;
      this.total = res.record.count;
    });  
  }

  filter(evt: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getDeposit();
  }
  
  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getDeposit();
  }

  resetFilter() {
    this.p = 1;
    this.params = {
      size: 10,
      page: 1,
      kind: 'deposit'
    };

    this.getDeposit();
  }

  deleteDeposite(deposite: Deposite) {

    Swal.fire({
      title: 'Are you sure?',
      text: "You want to delete this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {

        this.bonusService.deleteDeposit(deposite.id).subscribe((res: any) => {
          this.deposites = this.deposites.filter(f => f.id != deposite.id);
          toastr.success(res.message || 'Deposit deleted successfully.');

          // Swal.fire(
          //   'Deleted!',
          //   res.message || 'Deposit deleted successfully.',
          //   'success'
          // );

        });
        
      }
    });

  }

  selectDeposite(deposite: Deposite) {
    this.deposite = deposite;
  }

  getVipLvps(viplvpls: string | undefined) {
    return viplvpls?.replace('{', '').replace('}', '');
  }

  changeJoinStatua(join: Deposite, enabled: boolean) {
    Swal.fire({
      title: 'Are you sure?',
      html: `You want to ${join.enabled ? 'Disabled' : 'Enabled'}  <b>${join.code}</b>`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, ${join.enabled ? 'Disabled' : 'Enabled'} it!`
    }).then((result) => {
      if (result.isConfirmed) {

        this.bonusService.updateDepositBonusStatus({ id: join.id, enabled }).subscribe((res: any) => {
          toastr.success(res.message || 'Player updated successfully' );
          this.deposites = this.deposites.map((f: Deposite) => {
            if(f.id == join.id) {
              f.enabled = join.enabled ? false : true;
            }
            return f;
          });
        });
        
      }
    });

  }


}
