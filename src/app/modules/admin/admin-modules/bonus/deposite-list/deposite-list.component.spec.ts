import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepositeListComponent } from './deposite-list.component';

describe('DepositeListComponent', () => {
  let component: DepositeListComponent;
  let fixture: ComponentFixture<DepositeListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DepositeListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DepositeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
