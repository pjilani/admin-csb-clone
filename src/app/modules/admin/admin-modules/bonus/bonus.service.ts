import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BonusService {

  constructor(private http: HttpClient) { }

  // Deposit

  createDeposit(data: any) {
    return this.http.post(`admin/bones/deposit/add`, data);
  }

  createJoinBonus(data: any) {
    return this.http.post(`admin/bones/joining/add`, data);
  }

  getJoinById(id: number) {
    return this.http.get(`admin/bones/joining/${id}`);
  }

  updateJoinBonus(id: number, data: any) {
    return this.http.post(`admin/bones/joining/edit/${id}`, data);
  }

  updateJoinBonusStatus(data: any) {
    return this.http.post(`admin/bones/joining/update-status`, data);
  }

  updateDepositBonusStatus(data: any) {
    return this.http.post(`admin/bones/deposit/update-status`, data);
  }
  updateLosingBonusStatus(data: any) {
    return this.http.post(`admin/bones/losing/update-status`, data);
  }

  deleteBonus(id: number) {
    return this.http.delete(`admin/bones/${id}`);
  }
 
  getBonus(params: any) {
    return this.http.post(`admin/bones`, params);
  }
  
  // getDeposit(params: any) {
  //   return this.http.post(`admin/bones/deposit`, params);
  // }

  getDepositById(id: number) {
    return this.http.get(`admin/bones/deposit/${id}`);
  }

  deleteDeposit(id: number) {
    return this.http.delete(`admin/bones/deposit/${id}`);
  }

  updateDeposit(id: number, data: any) {
    return this.http.post(`admin/bones/deposit/edit/${id}`, data);
  }

  // Losing

  createLosing(data: any) {
    return this.http.post(`admin/bones/losing/add`, data);
  }

  // getLosing(params: any) {
  //   return this.http.post(`admin/bones/losing`, params);
  // }

  getLosingById(id: number) {
    return this.http.get(`admin/bones/losing/${id}`);
  }

  deleteLosing(id: number) {
    return this.http.delete(`admin/bones/losing/${id}`);
  }

  updateLosing(id: number, data: any) {
    return this.http.post(`admin/bones/losing/edit/${id}`, data);
  }

}
