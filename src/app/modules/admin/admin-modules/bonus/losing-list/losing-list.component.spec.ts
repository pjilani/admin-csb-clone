import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LosingListComponent } from './losing-list.component';

describe('LosingListComponent', () => {
  let component: LosingListComponent;
  let fixture: ComponentFixture<LosingListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LosingListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LosingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
