import { Component, OnInit } from '@angular/core';
import { Deposite } from 'src/app/models';
import { BonusService } from '../bonus.service';

import Swal from 'sweetalert2';
import { PermissionService } from 'src/app/services/permission.service';

declare const toastr: any;

@Component({
  selector: 'app-losing-list',
  templateUrl: './losing-list.component.html',
  styleUrls: ['./losing-list.component.scss']
})

export class LosingListComponent implements OnInit {
  
  losings: Deposite[] = [];
  losing_bonus_tier: any[] = [];
  deposite: Deposite | null = null;
  
  p: number = 1;
  total: number = 0;

  params: any = {
    size: 10,
    page: 1,
    kind: 'losing'
  };

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Losing Bonus', path: '/' },
  ];

  constructor(private bonusService: BonusService, public permissionService: PermissionService) { }

  ngOnInit(): void {
    this.getLosing();
  }

  selectDateRange(time_period: any) {
    this.params = { ...this.params, time_period }
    this.getLosing();
  }

  getLosing() {
    this.bonusService.getBonus(this.params).subscribe((res: any) => {
      this.losings = res.record.data;
      this.total = res.record.count;
    });  
  }

  filter(evt: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getLosing();
  }
  
  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getLosing();
  }

  resetFilter() {
    this.p = 1;
    this.params = {
      size: 10,
      page: 1,
      kind: 'losing'
    };

    this.getLosing();
  }

  deleteDeposite(deposite: Deposite) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You want to delete this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {

        this.bonusService.deleteLosing(deposite.id).subscribe((res: any) => {
          this.losings = this.losings.filter(f => f.id != deposite.id);
          toastr.success(res.message || 'losing deleted successfully.')
        });
        
      }
    });
  }

  selectDeposite(deposite:any) {
    this.deposite = deposite;
    this.losing_bonus_tier =  deposite?.losing_bonus_tier
  }

  getVipLvps(viplvpls: string | undefined) {
    return viplvpls?.replace('{', '').replace('}', '');
  }

  changeJoinStatua(join: Deposite, enabled: boolean) {
    Swal.fire({
      title: 'Are you sure?',
      html: `You want to ${join.enabled ? 'Disabled' : 'Enabled'}  <b>${join.code}</b>`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, ${join.enabled ? 'Disabled' : 'Enabled'} it!`
    }).then((result) => {
      if (result.isConfirmed) {

        this.bonusService.updateLosingBonusStatus({ id: join.id, enabled }).subscribe((res: any) => {
          toastr.success(res.message || 'Player updated successfully' );
          this.losings = this.losings.map((f: Deposite) => {
            if(f.id == join.id) {
              f.enabled = join.enabled ? false : true;
            }
            return f;
          });
        });
        
      }
    });

  }
}
