import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JoiningEditComponent } from './joining-edit.component';

describe('JoiningEditComponent', () => {
  let component: JoiningEditComponent;
  let fixture: ComponentFixture<JoiningEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JoiningEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JoiningEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
