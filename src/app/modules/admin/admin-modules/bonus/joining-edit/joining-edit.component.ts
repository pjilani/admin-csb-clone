import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomValidators } from 'src/app/shared/validators';
import { BonusService } from '../bonus.service';

declare const $: any; 
declare const toastr: any; 

@Component({
  templateUrl: './joining-edit.component.html',
  styleUrls: ['./joining-edit.component.scss']
})

export class JoiningEditComponent implements OnInit {
  @ViewChild('file') logo_file!: ElementRef;
  imgURL: any;

  id: number = 0;
  
  bonusForm: FormGroup | any;
  submitted: boolean = false;
  bonusLoader: boolean = false;

  selectLogo!: File;

  joining: any | null = null;

  title: string = 'Create'

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Joining Bonus', path: '/bonus/joining-list' }
  ];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private bonusService: BonusService,
    private formBuilder: FormBuilder
    ) {
     this.id = this.route.snapshot.params['id'];
    
    this.bonusForm = this.formBuilder.group({
      code: ['', [ Validators.required ]],
      flat: ['', [ Validators.required, Validators.min(1), Validators.max(999) ]],
      promotion_title: ['', [ Validators.required ]],
      logo: ['', [ CustomValidators.requiredFileType(CustomValidators.imgType) ]],
      terms_and_conditions: ['', [ Validators.required ]],
      enabled: [false],
    });

    if(this.id > 0) {
      this.title = 'Edit';

      this.getJoin();
        
    } else {
      const logoControl = this.bonusForm.get('logo');
      logoControl.setValidators([Validators.required, CustomValidators.requiredFileType(CustomValidators.imgType)]); 
      logoControl.updateValueAndValidity();
    }

    this.breadcrumbs.push({ title: this.title, path: `/bonus/joining-list/${this.id}` });

  }

  ngOnInit(): void {  }

  selectImage(files: any) {
    if (files.length === 0)
    return;

    const extension = files[0].type.split('/')[1].toLowerCase();
    const imgt = CustomValidators.imgType.includes(extension);

    if(imgt) {
      // this.selectLogo = files[0];
        const reader = new FileReader();
        // this.imagePath = files;
        reader.readAsDataURL(files[0]); 
        reader.onload = (_event) => { 
          this.imgURL = reader.result; 
        }
        this.selectLogo = files[0];
      } else {
        this.imgURL = '';
      }

  }

  onKeyPress(event: any) {
    const regexpNumber = /[0-9\+\-\ ]/;
    let inputCharacter = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !regexpNumber.test(inputCharacter)) {
      event.preventDefault();
    }
  }
  
  getJoin() {
    this.bonusService.getJoinById(this.id).subscribe((res: any) => {
      this.joining = res.record;
      
      // console.log(this.joining);

      this.bonusForm.patchValue({
        code: this.joining.code,
        promotion_title: this.joining.promotion_title,
        terms_and_conditions: this.joining.terms_and_conditions,
        flat: this.joining.flat,
        enabled: this.joining.enabled
      });

      this.imgURL = this.joining.image;

    });
  }
  
  get f() {
    return this.bonusForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid

    // console.log(this.bonusForm.value);

    if (this.bonusForm.invalid) return;
    
    this.bonusLoader = true;

    const data = this.bonusForm.value;
    
    const fd = new FormData();
    
    
    if(this.selectLogo && this.selectLogo.name) {
      fd.append('image', this.selectLogo, this.selectLogo.name);
    }
    
    delete data.logo;

    for(let key in data) {
      fd.append(key, data[key]);
    }
    
    if(this.id > 0) {
      this.bonusService.updateJoinBonus(this.id, fd)
      .subscribe((response: any) => {
        toastr.success(response?.message || 'joining Bonus Updated Successfully!');
        this.router.navigate(['/bonus/joining-list']);
        this.bonusLoader = false; 
      }, (err: any) => {
        this.bonusLoader = false;
      });

    } else {
      
      this.bonusService.createJoinBonus(fd)
      .subscribe((response: any) => {
        toastr.success(response?.message || 'joining Bonus Created Successfully!');
        this.router.navigate(['/bonus/joining-list']);
        this.bonusLoader = false; 
      }, (err: any) => {
        this.bonusLoader = false; 
      });

    }
  }

}
