import { Component, OnInit } from '@angular/core';
import { Deposite } from 'src/app/models';
import { BonusService } from '../bonus.service';

import Swal from 'sweetalert2';
import { PermissionService } from 'src/app/services/permission.service';

declare const toastr: any;

@Component({
  selector: 'app-joining-list',
  templateUrl: './joining-list.component.html',
  styleUrls: ['./joining-list.component.scss']
})

export class JoiningListComponent implements OnInit {

  Joining: any[] = [];
  join: any | null = null;
  
  p: number = 1;
  total: number = 0;

  params: any = {
    size: 10,
    page: 1,
    kind: 'joining'
  };

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Joining Bonus', path: '/bonus/joining-list' }
  ];

  constructor(private bonusService: BonusService, public permissionService: PermissionService) { }

  ngOnInit(): void {
    this.getJoining();
  }

  selectDateRange(time_period: any) {
    this.params = { ...this.params, time_period }
    this.getJoining();
  }

  getJoining() {
    this.bonusService.getBonus(this.params).subscribe((res: any) => {
      this.Joining = res.record.data;
      this.total = res.record.count;
    });  
  }

  filter(evt: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getJoining();
  }
  
  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getJoining();
  }

  resetFilter() {
    this.p = 1;
    this.params = {
      size: 10,
      page: 1,
      kind: 'joining'
    };

    this.getJoining();
  }

  deleteJoin(bonus: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to delete ${bonus.promotion_title}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {

        this.bonusService.deleteBonus(bonus.id).subscribe((res: any) => {
          this.Joining = this.Joining.filter(f => f.id != bonus.id);
          toastr.success(res.message || 'Joining Bonus deleted successfully.')
        });
        
      }
    });
  }

  selectJoin(deposite: Deposite) {
    this.join = deposite;
  }

  changeJoinStatua(join: Deposite, enabled: boolean) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to ${join.enabled ? 'Disabled' : 'Enabled'} ${join.promotion_title}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, ${join.enabled ? 'Disabled' : 'Enabled'} it!`
    }).then((result) => {
      if (result.isConfirmed) {

        this.bonusService.updateJoinBonusStatus({ id: join.id, enabled }).subscribe((res: any) => {
          toastr.success(res.message || 'Player updated successfully' );
          this.Joining = this.Joining.map((f: Deposite) => {
            if(f.id == join.id) {
              f.enabled = join.enabled ? false : true;
            }
            return f;
          });
        });
        
      }
    });

  }

}
