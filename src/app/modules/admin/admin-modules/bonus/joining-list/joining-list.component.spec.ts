import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JoiningListComponent } from './Joining-list.component';

describe('JoiningListComponent', () => {
  let component: JoiningListComponent;
  let fixture: ComponentFixture<JoiningListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JoiningListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JoiningListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
