import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomValidators } from 'src/app/shared/validators';
import { AdminAuthService } from '../../../services/admin-auth.service';
import { BonusService } from '../bonus.service';

declare const $: any; 
declare const toastr: any; 
@Component({
  selector: 'app-losing-edit',
  templateUrl: './losing-edit.component.html',
  styleUrls: ['./losing-edit.component.scss']
})

export class LosingEditComponent implements OnInit {
  @ViewChild('file') logo_file!: ElementRef;
  imgURL: any;

  format: string = "YYYY-MM-DD HH:mm:ss";
  
  startDate = new Date().toISOString().split('T')[0]+' 12:00:00';
  endDate = new Date().toISOString().split('T')[0]+' 23:59:59';

  id: number = 0;
  currencies: any[] = [];
  
  bonusForm: FormGroup | any;
  submitted: boolean = false;
  bonusLoader: boolean = false;
  selectLogo!: File;
  
  losing: any | null = null;

  title: string = 'Create'

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Losing Bonus', path: '/bonus/losing-list' }
  ];

  constructor(private route: ActivatedRoute,
    private router: Router,
    private bonusService: BonusService,
    private formBuilder: FormBuilder,
    private adminAuthService: AdminAuthService
    ) {
     this.id = this.route.snapshot.params['id'];
    
    this.bonusForm = this.formBuilder.group({
      code: ['', [ Validators.required ]],
      currency_id: ['', [ Validators.required ]],
      
      vip_levels: [this.getNumArr(11), [ Validators.required ]],
      valid_from: [this.startDate, [ Validators.required ]],
      valid_upto: [this.endDate, [Validators.required]],
      claim_days: ['', [ Validators.required, Validators.min(1) ]],
      promotion_title: ['', [ Validators.required ]],
      logo: ['', [ CustomValidators.requiredFileType(CustomValidators.imgType) ]],
      terms_and_conditions: ['', [ Validators.required ]],
      // campaign_period: [''],
      enabled: [false],
      tiers: this.formBuilder.array([]),
    });

    if(this.id > 0) {
      this.title = 'Edit';

      this.getLosing();
        
    } else {
      this.addTiers();
      
      const logoControl = this.bonusForm.get('logo');
      logoControl.setValidators([Validators.required, CustomValidators.requiredFileType(CustomValidators.imgType)]); 
      logoControl.updateValueAndValidity();
    }

    this.breadcrumbs.push({ title: this.title, path: `/bonus/losing-edit/${this.id}` });

  }

  ngOnInit(): void {
    this.adminAuthService?.walletData.subscribe((wallets: any) => {
        this.currencies = wallets || [];
        
        this.currencies = this.currencies.map((m: any) => {
          m.id = m.currency_id;
          m.name = m.currency_name;
          return m;
        });

    });
    // this.currenciesService.getAdminCurrencies().subscribe((res: any) => {
    //   this.currencies = res.record;
    // });
  }

  ngAfterViewInit(): void {
    $("#vip_levels").val(this.getNumArr(11)).trigger('change');
  }

  get tiers() : FormArray {
    return this.bonusForm.get("tiers") as FormArray
  }

  newTier(): FormGroup {
    return this.formBuilder.group({
      min_losing_amount: ['', [ Validators.required, Validators.min(1)] ],
      percentage: ['', [Validators.required, Validators.min(1), Validators.max(99)]]
    })
  }
 
  addTiers() {
    this.tiers.push(this.newTier());
  }
  
  removeTier(i:number) {
    this.tiers.removeAt(i);
  }

  selectImage(files: any) {
    if (files.length === 0)
    return;
    const extension = files[0].name.split('.')[1].toLowerCase();
    const imgt = CustomValidators.imgType.includes(extension);

    if(imgt) {
      // this.selectLogo = files[0];
        const reader = new FileReader();
        // this.imagePath = files;
        reader.readAsDataURL(files[0]); 
        reader.onload = (_event) => { 
          this.imgURL = reader.result; 
        }
        this.selectLogo = files[0];
      } else {
        this.imgURL = '';
      }

  }

  selectDateRange(time_period: any) {
    this.bonusForm.patchValue({
      valid_from: time_period.start_date,
      valid_upto: time_period.end_date
    });
  }

  getNumArr(length: number) {
   return Array.from({ length }).map((_, i) => i);
  }

  getLosing() {
    this.bonusService.getLosingById(this.id).subscribe((res: any) => {
      this.losing = res.record;

      this.losing.vip_levels = JSON.parse(this.losing.vip_levels.replace('{', '[').replace('}', ']'));

      // console.log(this.losing);

      this.bonusForm.patchValue({
        code: this.losing.code,
        currency_id: this.losing.currency_id,
        vip_levels: this.losing.vip_levels,
        claim_days: this.losing.setting.claim_days,
        valid_from: this.losing.valid_from,
        valid_upto: this.losing.valid_upto,
        promotion_title: this.losing.promotion_title,
        // logo: this.losing.image,
        terms_and_conditions: this.losing.terms_and_conditions,
        enabled: this.losing.enabled,
      });

      for(let i = 0; i < this.losing.tiers.length; i++) {
        this.tiers.push(this.formBuilder.group({
          min_losing_amount: [this.losing.tiers[i].min_losing_amount, [ Validators.required, Validators.min(1)] ],
          percentage: [this.losing.tiers[i].percentage, [Validators.required, Validators.min(1), Validators.max(99)]]
        }))
      }

      this.imgURL = this.losing.image;

      setTimeout(() => {
        $("#vip_levels").val(this.losing.vip_levels).trigger('change');
        $('#time_period').val(this.losing.valid_from + ' - ' + this.losing.valid_upto);
      }, 100);

    });
  }

  selectVipLevel(vip_levels: any) {
    this.f.vip_levels.setValue(vip_levels);
  }

  get f() {
    return this.bonusForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid

    // console.log(this.bonusForm.value);

    if (this.bonusForm.invalid) return;
    
    this.bonusLoader = true;

    const data = this.bonusForm.value;

    // data.vip_levels = JSON.stringify(data.vip_levels).replace('[', '{').replace(']', '}');
    data.vip_levels = JSON.stringify(data.vip_levels);

    const fd = new FormData();
    var valid_from = this.f.valid_from.value;
    var CurrentDate = new Date().toJSON().slice(0, 10);
    valid_from = valid_from.slice(0, 10);
    if(valid_from < CurrentDate){
      toastr.error('Campaign period from date can not be greater then today date');
      this.bonusLoader = false;
      return;
    }
    delete data.logo;

    if(this.selectLogo && this.selectLogo.name) {
      fd.append('image', this.selectLogo, this.selectLogo.name);
    }

    fd.append("campaign_period[valid_from]", this.f.valid_from.value);
    fd.append("campaign_period[valid_upto]", this.f.valid_upto.value);

    delete data.valid_from;
    delete data.valid_upto;
    
    for(let key in data.tiers) {
      // console.log(`tiers[${key}]['min_losing_amount']`, data.tiers[key].min_losing_amount);
      // console.log(`tiers[${key}]['percentage']`, data.tiers[key].percentage);

      fd.append(`tiers[${key}][min_losing_amount]`, data.tiers[key].min_losing_amount);
      fd.append(`tiers[${key}][percentage]`, data.tiers[key].percentage);
    }
    
    delete data.tiers;

    for(let key in data) {
      // console.log(key, data[key]);
      fd.append(key, data[key]);
    }

    if(this.id > 0) {
      this.bonusService.updateLosing(this.id, fd)
      .subscribe((response: any) => {
        toastr.success(response?.message || 'Bonus Losing Updated Successfully!');
        this.router.navigate(['/bonus/losing-list']);
        this.bonusLoader = false; 
      }, (err: any) => {
        this.bonusLoader = false;
      });

    } else {
      
      this.bonusService.createLosing(fd)
      .subscribe((response: any) => {
        toastr.success(response?.message || 'Bonus Losing Created Successfully!');
        this.router.navigate(['/bonus/losing-list']);
        this.bonusLoader = false; 
      }, (err: any) => {
        this.bonusLoader = false; 
      });

    }
  }

}
