import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LosingEditComponent } from './losing-edit.component';

describe('LosingEditComponent', () => {
  let component: LosingEditComponent;
  let fixture: ComponentFixture<LosingEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LosingEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LosingEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
