import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Admin, Role, User } from 'src/app/models';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
declare const toastr: any;

@Injectable({ providedIn: 'root' })

export class AdminAuthService {

  public adminUserSubject!: BehaviorSubject<Admin>;
  public adminUser = new BehaviorSubject({});

  private adminTokenSubject!: BehaviorSubject<String>;
  public adminToken!: Observable<String>;
  
  public adminRole = new BehaviorSubject('');

  public adminReports = new BehaviorSubject([]);
  public adminPermissions = new BehaviorSubject({});

  public tenantData:any = new BehaviorSubject(null);
  public walletData:any = new BehaviorSubject([]);

    constructor(private http: HttpClient, private route: Router) {
          this.adminTokenSubject = new BehaviorSubject<String>(localStorage.getItem('AdminAuthToken') || '');
          this.adminToken = this.adminTokenSubject.asObservable();
          
          this.setRoleAndReports();
    }

    public get adminTokenValue(): String {
        return this.adminTokenSubject.value;
    }

    public get adminUserValue(): Admin {
      return this.adminUserSubject?.value;
    }
  
    public get adminRoleValue(): String {
        return this.adminRole.value;
    }
  
    public get adminReportsValue(): any {
          return this.adminReports.value;
    }

    public get tenantDataValue(): any {
      return this.tenantData.value;
    } 

    public get adminPermissionsValue(): any {
      return this.adminPermissions.value;
    }

    public get walletDataValue(): any {
      return this.walletData.value;
    }
  
  setRoleAndReports() {
    const roles: string = localStorage.getItem('roles') || '';
    // const report_enable_ids: string = localStorage.getItem('report_enable_ids') || '';

    if(roles) {
        const myroles = JSON.parse(roles);

        if(myroles && myroles.findIndex( (role: any) => role === Role.Admin ) > -1) { 
          this.adminRole.next(Role.Admin);
        } else {
          this.adminRole.next(Role.Agent);
        }

    }

    // if (report_enable_ids) {
    //   this.adminReports.next(JSON.parse(report_enable_ids));
    // }
    
  }

  login(userParams: User): Observable<any> {
    return this.http.post<any>(`login/admin`, userParams)
    .pipe(map(userData => {

        localStorage.setItem('AdminAuthToken', userData?.record?.token || '');
        localStorage.setItem('roles', JSON.stringify(userData?.record?.user.roles || ''));
      
        localStorage.setItem('allowedModules', userData?.record?.tenant?.allowed_modules  || '');
        localStorage.setItem('domain', userData?.record?.tenant?.domain  || '');
        this.adminTokenSubject.next(userData?.record?.token || null);
        this.setRoleAndReports();
        
        localStorage.setItem('userData', JSON.stringify(userData?.record?.user || {}))
        this.adminUserSubject = new BehaviorSubject<Admin>(userData?.record?.user);
        this.updateUser(userData?.record?.user);

        localStorage.setItem('tenantData', JSON.stringify(userData?.record?.tenant || {}))
        this.tenantData = new BehaviorSubject<any>(userData?.record?.tenant);

        localStorage.setItem('permissions', JSON.stringify(userData?.record?.permissions || {}))
        this.setPermissions(userData?.record?.permissions);

        toastr.success(userData?.message || 'Login Successfull');
        
        return userData;
    }));
  }

  logout() {
      return this.http.post(`logout/admin`, {})
      .pipe(map((user: any) => {
        toastr.success('Logout Successfull');
        this.navigateToLogin();
        return user;
      }));
  }

  navigateToLogin(message?: string) {
      this.route.navigate(['/login']);

      localStorage.removeItem('AdminAuthToken');
      localStorage.removeItem('roles');
      localStorage.removeItem('report_enable_ids');
      localStorage.removeItem('allowedModules');
      localStorage.removeItem('domain');
      localStorage.removeItem('userData');
      localStorage.removeItem('tenantData');
      localStorage.removeItem('permissions');

      this.adminTokenSubject.next('');
      this.adminRole.next('');
      this.adminReports.next([]);
      this.adminPermissions.next({});
      this.tenantData.next(null);
      
      if(message) {
        toastr.options = {
          "preventDuplicates": true,
          "preventOpenDuplicates": true
        };
        toastr.error(message);
      }
  }

  getPersonalDetails() {
    return this.http.get('admin/user').pipe(map((res: any) => {
      // this.adminUserSubject = new BehaviorSubject<Admin>(res.record);
      localStorage.setItem('roles', JSON.stringify(res.record.roles || ''));

      localStorage.setItem('userData', JSON.stringify(res.record));
      this.adminUserSubject = new BehaviorSubject<Admin>(JSON.parse(localStorage.getItem('userData') || '{}'));
      this.adminUser = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('userData') || '{}'));
      
      // if (res?.record?.tenant?.report_enable_ids) {
      //   localStorage.setItem('report_enable_ids', JSON.stringify(res?.record?.tenant?.report_enable_ids.split(",")) || '');
      // }
      this.setRoleAndReports();

      // this.updateUser(res.record);

      // this.setPermissions(res.record?.permissions);
      return res;
    }));
  }

  updateUser(admin: Admin) {
    this.adminUser.next(admin);
  }

  setPermissions(permissions: any){
    this.adminPermissions.next(permissions)
  }

  getTenantsDetails(){
    return this.http.get('admin/tenantDetailsFromDomain')
  }

  getWalletDetails(){
    return this.http.get('admin/user/wallet').pipe(map((res:any) => {
      this.walletData.next(res.record);
      return res
    }));
  }

  getPermissions(){
    return this.http.get('admin/user/permissions').pipe(map((res:any) => {
      localStorage.setItem('permissions', JSON.stringify(res.record));
      this.adminPermissions = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('permissions') || '{}'));
      return res
    }));
  }

  getTenantData(){
    return this.http.get('admin/user/tenant').pipe(map((res:any) => {
      localStorage.setItem('tenantData', JSON.stringify(res.record));
      this.tenantData = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('tenantData') || '{}'));
      return res
    }));
  }


}
