import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminAuthGuard, AdminAgentCommonAuthGuard, AdminDepositWithdrawalAuthGuard} from 'src/app/guards';

const adminRoutes: Routes = [
  {
    path: '',
    loadChildren: () => import('./../dashboard/dashboard.module').then(m => m.DashboardModule),
    pathMatch: 'full',
    canActivate: [AdminAgentCommonAuthGuard]
  },
  {
    path: 'withdraw-requests',
    loadChildren: () => import('./admin-modules/withdraw-requests/withdraw-requests.module').then(m => m.WithdrawRequestsModule),
    canActivate: [AdminAgentCommonAuthGuard]
  },
  {
    path: 'deposit-requests',
    loadChildren: () => import('./admin-modules/deposit-requests/deposit-requests.module').then(m => m.DepositRequestsModule),
    canActivate: [AdminAgentCommonAuthGuard]
  },
  {
    path: 'casino',
    loadChildren: () => import('./admin-modules/casino/casino.module').then(m => m.CasinoModule),
    canActivate: [AdminAgentCommonAuthGuard]
  },
  {
    path: 'site-maintenance',
    loadChildren: () => import('./admin-modules/site-maintenance/site-maintenance.module').then(m => m.SiteMaintenanceModule),
    canActivate: [AdminAgentCommonAuthGuard]
  },
  {
    path: 'permissionRole',
    loadChildren: () => import('./admin-modules/permissionRole/permissionRole.module').then(m => m.PermissionRoleModule),
    canActivate: [AdminAuthGuard]
  },
  {
    path: 'notifications',
    loadChildren: () => import('./admin-modules/notifications/notifications.module').then(m => m.NotificationsModule),
    canActivate: [AdminAgentCommonAuthGuard]
  },
  {
    path: 'bonus',
    loadChildren: () => import('./admin-modules/bonus/bonus.module').then(m => m.BonusModule),
    canActivate: [AdminAgentCommonAuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule]
})

export class AdminRoutingModule { }
