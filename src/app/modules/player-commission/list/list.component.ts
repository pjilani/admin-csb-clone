import { TIMEZONE } from '../../../shared/constants';
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminAuthService } from '../../admin/services/admin-auth.service';
import { PlayerCommissionService } from '../player-commission.service';
import { PageSizes, WALLET_TYPES } from 'src/app/shared/constants';
import { Role } from 'src/app/models';
import { DomainForDisableModule } from 'src/app/shared/constants';
import Swal from 'sweetalert2';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { invalid } from 'moment';
import { CurrenciesService } from '../../super-admin/super-admin-modules/currencies/currencies.service';
import { ReportService } from '../../reports/reports.service';
import { PermissionService } from 'src/app/services/permission.service';
import * as moment from 'moment-timezone';
import { DatePipe } from '@angular/common';
declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit, AfterViewInit {
  playersCommission: any = [];
  selectedPlayerCommission: Map<number, Array<number>>|any = new Map();
  userNames: Map<number , any> = new Map()
  userList :any[]=[];
  weekList :any[]=[];
  monthList :any[]=[];
  pageSizes = PageSizes;
  playerCommissionParams: any = {
    size: 10,
    page: 1,
    search: '',
    status: '',
    agent_id: '',
    week_id: '',
    month_id: '',
    interval_type: 'daily',
    date_range: {},
    time_period: [],
    datetime: {},
    tenant_id: "",
  };

  playerCommissionP: number = 1;
  playerCommissionTotal: number = 0;
  dataTableParams: any = {
    searching: false,
    info: true,
    lengthChange: true,
    autoWidth: false,
    responsive: true,
    "dom": '<"top"lp>t<"bottom"ip><"clear">',
    lengthMenu: PageSizes.map(m => m.name),
    buttons: [{ extend: 'csv', text: 'Export as CSV', className: 'ctsmcsv mgt btn btn-info mt-0' }]
  };
  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Players', path: '/players-commission' },
    { title: 'Player Commission', path: '/players-commission' }
  ];
  totalPending:number = 0;
  perPageTotalPending:number = 0;

  roles: string = localStorage.getItem('roles') || '';
  format: string = "YYYY-MM-DD HH:mm:ss";
  isLoader:boolean = false
  maxSelectableDateTime!: any;

  statusLoader:boolean = false;
  firstTimeApiCall = false;
  
  constructor(
    private route: ActivatedRoute,
    private adminAuthService: AdminAuthService,
    public superAdminAuthService: SuperAdminAuthService,
    private playerCommissionService: PlayerCommissionService,
    private ReportService: ReportService,
    public permissionService: PermissionService,
    public datePipe: DatePipe
  ) {
  }
  ngAfterViewInit(): void {
    $('[data-toggle="tooltip"]').tooltip()

    $('#time_period').val('');
    $( "#interval_type" ).val('daily').trigger('change');
  }

  ngOnInit(): void {
    const currentDate = new Date();
    currentDate.setDate(currentDate.getDate() - 1);
    currentDate.setHours(23, 59, 59);
    this.maxSelectableDateTime = currentDate;

    this.adminAuthService.adminUser.subscribe((user: any) => {
      if (user && user.id) {
        // this.getPlayersCommission();
        this.getAgentUserList();
        this.getMonthList()
        this.getWeekList()
      }
    });
  }

  playerCommissionPageChanged(page: number) {
    this.playerCommissionParams = { ...this.playerCommissionParams, page };
    this.getPlayersCommission();
  }

  filterPlayersCommission(evt?:any) {
    this.playerCommissionP = 1;    
    this.playerCommissionParams = { ...this.playerCommissionParams, page: this.playerCommissionP };
    this.selectedPlayerCommission.clear();
    this.userNames.clear()
    this.getPlayersCommission();
  }

  filterSelectAgent(evt: any) {
    this.playerCommissionP = 1;
    this.playerCommissionParams = {...this.playerCommissionParams, page: this.playerCommissionP, agent_id:evt};
    this.selectedPlayerCommission.clear();
    this.userNames.clear()
    // if(evt)
    // this.getPlayersCommission();    
  }
  filterSelectWeek(evt: any) {
    this.playerCommissionP = 1;
    this.playerCommissionParams = {...this.playerCommissionParams, page: this.playerCommissionP, week_id:evt};
    this.selectedPlayerCommission.clear();
    this.userNames.clear()   
  }
  filterSelectMonth(evt: any) {
    this.playerCommissionP = 1;
    this.playerCommissionParams = {...this.playerCommissionParams, page: this.playerCommissionP, month_id:evt};
    this.selectedPlayerCommission.clear();
    this.userNames.clear()   
  }
  filterSelectInterval(evt: any) {
    this.playerCommissionP = 1;
    this.playerCommissionParams = {...this.playerCommissionParams, page: this.playerCommissionP, interval_type:evt};
    this.selectedPlayerCommission.clear();
    this.userNames.clear()   
  }
  setTimePeriod(date_range:any){
    this.playerCommissionP = 1
    this.playerCommissionParams = { ...this.playerCommissionParams, time_period: date_range, page:this.playerCommissionP}
    this.selectedPlayerCommission.clear();
    this.userNames.clear()
  }

  getPlayersCommission() {
    this.isLoader = true
    this.firstTimeApiCall = true;
      this.playerCommissionParams = {...this.playerCommissionParams, datetime : {
        start_date: this.playerCommissionParams.time_period?.start_date,
        end_date:this.playerCommissionParams.time_period?.end_date
      }}
    this.playerCommissionService
      .getAdminPlayerCommission(this.playerCommissionParams)
      .subscribe((res: any) => {
        this.playersCommission = res.record.data.data;
        this.playerCommissionTotal = res.record.data.total;
        this.totalPending = res.record.total_pending;
        this.perPageTotalPending = res.record.per_page_total_pending
        this.isLoader = false;
      },(error:any) => {
        this.isLoader = false

      });
  }

  playerCommissionResetFilter() {
    this.playerCommissionP = 1;
    this.playerCommissionParams = {
      size: 10,
      page: 1,
      search: '',
      status: '',
      agent_id: '',
      week_id: '',
      month_id: '',
      interval_type: 'daily',
      phone_verified: '',
      email_verified: '',
      kyc_done: '',
      date_range:{},
    };
    this.selectedPlayerCommission.clear();
    this.userNames.clear()
    this.getPlayersCommission();
    $('#time_period').val('').trigger('change');
    $( "#agent_id" ).val('').trigger('change');
    $( "#week_id" ).val('').trigger('change');
    $( "#month_id" ).val('').trigger('change');
    $( "#interval_type" ).val('daily').trigger('change');
  }

  updatePlayerCommissionStatus(player: any, status: number) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to ${status == 1 ? 'Approve' : 'Reject'} Commission for ${
        player.user.user_name
      }!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, ${status == 1 ? 'Approve' : 'Reject'} it!`,
    }).then((result) => {
      if (result.isConfirmed) {
        const data = {
          data : [{
            id : player.id,
            user_id : player.user_id,
            status: status,
            amount: player.commission_amount
          }],
          status : status
        }
        this.statusLoader = true;
        this.playerCommissionService
          .bulkStatusUpdatePlayerCommission(data)
          .subscribe((res: any) => {
            toastr.success(res.message || 'Player Commission updated successfully');
            this.playersCommission = this.playersCommission.map((f: any) => {
              if (f.id == player.id) {
                f.status = status
              }
              return f;
            });
            this.selectedPlayerCommission.clear();
            this.userNames.clear();
            this.statusLoader = false;
          });
      }
    });
  }


  exportAsXLSX() {
      this.playerCommissionParams = {...this.playerCommissionParams, datetime : {
        start_date: this.playerCommissionParams.time_period?.start_date,
        end_date:this.playerCommissionParams.time_period?.end_date
      }}
    this.playerCommissionService
      .downloadUserPlayersCommission(this.playerCommissionParams)
      .subscribe((res: any) => {
        window.location.href = res.record.url;
      });
  }

  selectUser(user: any, evt: any){
    if(evt.target.checked)
    {
      this.selectedPlayerCommission.set(this.playerCommissionP, (this.selectedPlayerCommission.has(this.playerCommissionP)) ? [ ...this.selectedPlayerCommission.get(this.playerCommissionP) , user] : [user]);
      this.userNames.set(user.id, user);
    }
    else{
      this.selectedPlayerCommission.has(this.playerCommissionP)?
      this.selectedPlayerCommission.set(this.playerCommissionP, this.selectedPlayerCommission.get(this.playerCommissionP).filter((v:any)=> v.id !== user.id)):
      this.selectedPlayerCommission.delete(this.playerCommissionP);
      this.userNames.delete(user.id);
    }
  }

  toggleAll(evt:any){
    if(evt.target.checked) {
      const ids = this.playersCommission.filter((value:any)=> {
      if(value.status === 0){
        this.userNames.set(value.id, value)
        return value
      }
    });
      this.selectedPlayerCommission.set(this.playerCommissionP,ids); 
    } else {
      this.selectedPlayerCommission.get(this.playerCommissionP).forEach((user: any) => this.userNames.delete(user.id))
      this.selectedPlayerCommission.delete(this.playerCommissionP)
    };    
  }

  get size(){
    let arr: string | any[] = [];
    this.selectedPlayerCommission.forEach((d:any)=>{
      arr = [...arr,...d];
    });
    return arr.length;
  }

  getAgentUserList(){
      this.ReportService.getAdminUserList().subscribe((res: any) => {
        this.userList = res.record;
      });
  }
  removeplayer(id:any)
  {
    this.userNames.delete(id);
    this.selectedPlayerCommission.has(this.playerCommissionP)?
    this.selectedPlayerCommission.set(this.playerCommissionP, this.selectedPlayerCommission.get(this.playerCommissionP).filter((v:any)=> v.id!== id)):
    this.selectedPlayerCommission.delete(this.playerCommissionP);
  }


  bulkStatusUpdate(status:any){
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to ${status == 1 ? 'Approve' : 'Reject'} Selected Player Commissions!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, ${status == 1 ? 'Approve' : 'Reject'} it!`,
    }).then((result) => {
      if (result.isConfirmed) {
        const data = {
          data : this.transformData(this.selectedPlayerCommission,status),
          status : status
        }
        this.statusLoader = true;
        this.playerCommissionService.bulkStatusUpdatePlayerCommission(data).subscribe((res:any)=>{
          toastr.success(res.message || 'Bulk Status Updated Successfully');
          this.selectedPlayerCommission.clear();
          this.userNames.clear();
          this.getPlayersCommission()
          this.statusLoader = false;
        }, err =>{
          this.selectedPlayerCommission.clear();
          this.userNames.clear();
          this.statusLoader = false;
        })
      }
    });
  }

isIdPresent(data:any, idToCheck:any) {
    const found = data.find((itemValue:any) => itemValue?.id === idToCheck);
    if (found) {
      return true;
    }
  return false;
}

transformData(data:any, status:any) {
  let transformedData: any[] = [];
  for (const value of data.values()) {
    transformedData = transformedData.concat(
      value.map((val:any) => ({
        id: val.id,
        user_id: val.user_id,
        status: status,
        amount: val.commission_amount
      }))
    );
  }
  return transformedData;
}

bulkStatusAllUpdate(status:any){
  Swal.fire({
    title: 'Are you sure?',
    text: `You want to ${status == 1 ? 'Approve' : 'Reject'} All Pending Player Commission!`,
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: `Yes, ${status == 1 ? 'Approve' : 'Reject'} it!`,
  }).then((result) => {
    if (result.isConfirmed) {
      const data = {
        bulk_all: (status == 1) ? 1 : 2,
        status : status
      }
      this.statusLoader = true;
      this.playerCommissionService.bulkStatusUpdatePlayerCommission(data).subscribe((res:any)=>{
        toastr.success(res.message || 'Bulk Status Updated Successfully');
        this.selectedPlayerCommission.clear();
        this.userNames.clear();
        this.getPlayersCommission()
        this.statusLoader = false;
      }, err =>{
        this.selectedPlayerCommission.clear();
        this.userNames.clear();
        this.statusLoader = false;
      })
    }
  });
}
  getWeekList(){
  this.playerCommissionService.Dates({type: 'weekly'}).subscribe((res: any) => {
    this.weekList = res.record;      
  });
}
  getMonthList(){
  this.playerCommissionService.Dates({type: 'monthly'}).subscribe((res: any) => {
  this.monthList = res.record;
});
}
}
