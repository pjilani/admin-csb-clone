import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ListComponent } from './list/list.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { AdminAgentCommonAuthGuard } from 'src/app/guards';
import { PipeModule } from 'src/app/pipes/pipes.module';
import { JobCommissionComponent } from './job-commission/job-commission.component';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
const playerCommissionRoutes: Routes = [
  { path: '', component: ListComponent, canActivate: [AdminAgentCommonAuthGuard] },
  { path: 'jobs', component: JobCommissionComponent, canActivate: [AdminAgentCommonAuthGuard] },
];

@NgModule({
  declarations: [ ListComponent, JobCommissionComponent ],
  imports: [
    CommonModule,
    RouterModule.forChild(playerCommissionRoutes),
    SharedModule,
    ComponentsModule,
    PipeModule,
    DirectivesModule,
    NgxJsonViewerModule
  ],
  providers: [
    DatePipe, 
  ],
})

export class PlayerCommissionModule { }
