import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SuperAdminAuthService } from '../super-admin/services/super-admin-auth.service';

@Injectable({ providedIn: 'root' })

export class PlayerCommissionService {

  constructor(private http: HttpClient,
  private superAdminAuthService: SuperAdminAuthService) { }

  // ====================== Admin url ==============================
  
  getAdminPlayerCommission(params: any) {
    return this.http.post(`admin/usersadmin/player-commission`, params);
  }
  
  updateAdminPlayerCommissionStatus(params:any) {
   
    if (this.superAdminAuthService?.superAdminTokenValue?.length > 0) {
      return this.http.post(`super/admin/bulk/player-commission/status`, params);
    } else {
      return this.http.post(`admin/bulk/player-commission/status`, params);
    }
  }

  downloadPlayersCommission(params: any) {
    if(this.superAdminAuthService && this.superAdminAuthService.superAdminTokenValue) {
      // ====================== Super Admin url ==============================
      return this.http.post(`super/admin/players/download`, params) ;
    } else {
      // ====================== Admin url ==============================
      return this.http.post(`admin/players/download`, params);
    }
  }

  downloadUserPlayersCommission(params: any) {
    return this.http.post(`admin/usersadmin/player-commission/download`, params);
  }

  bulkStatusUpdate(params:any){
    return this.http.post(`admin/bulk/player-commission/status`, params)
  }

  bulkStatusUpdatePlayerCommission(params:any){
    return this.http.post(`admin/bulk/player-commission/all`, params)
  }

  listJobs(params:any){
    return this.http.post(`admin/bulk/get-jobs`, params)
  }
  getUserbyJobId(params:any, id:any){
    return this.http.post(`admin/bulk/jobs/${id}`, params)
  }
  retryJob(job_id:any){
    return this.http.post(`admin/bulk/retry/${job_id}`, { id: job_id})
  }
  Dates(type:any){
    return this.http.post(`admin/bulk/get-dates`, type)
  }

}
