import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { BetListComponent } from './Betlist/Betlist.component';
import { Routes, RouterModule } from '@angular/router';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { TransferFundComponent } from './transfer-fund/transfer-fund.component';
import { ListCloneComponent } from './list-clone/list-clone.component';

const transactionsRoutes: Routes = [
  { path: '', redirectTo: 'list' },
  { path: 'list', component: ListComponent },
  { path: 'record-list', component: ListCloneComponent },
  { path: 'bet-transactions', component: BetListComponent },
  { path: 'transfer-fund', component: TransferFundComponent },
]

@NgModule({
  declarations: [
    ListComponent, TransferFundComponent, BetListComponent, ListCloneComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(transactionsRoutes),
    ComponentsModule,
    SharedModule,
    DirectivesModule
  ]
})

export class TransactionsModule { }
