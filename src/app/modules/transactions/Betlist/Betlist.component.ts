import {AfterViewInit, Component, OnInit} from "@angular/core";
import {Currency, Tenant, Bettransaction} from "src/app/models";
import {PageSizes, TransactionTypes, TIMEZONE} from "src/app/shared/constants";
import {SuperAdminAuthService} from "../../super-admin/services/super-admin-auth.service";
import {CurrenciesService} from "../../super-admin/super-admin-modules/currencies/currencies.service";
import {TenantService} from "../../tenant/tenant.service";
import {TransactionService} from "../transaction.service";
import {ActivatedRoute} from "@angular/router";
import { AdminAuthService } from "../../admin/services/admin-auth.service";
import * as moment from 'moment-timezone';
import { PermissionService } from "src/app/services/permission.service";

declare const $: any;

@Component({
  templateUrl: './Betlist.component.html',
  styleUrls: ['./list.component.scss']
})

export class BetListComponent implements OnInit, AfterViewInit{

  pageSizes: any[] = PageSizes;

  transactionTypes = TransactionTypes;

  currencies: Currency[] = [];
  transactions: Bettransaction[] = [];
  transaction!: Bettransaction;

  p: number = 1;

  format: string = "YYYY-MM-DD HH:mm:ss";
  TIMEZONE: any[] = TIMEZONE;
  zone: any = '(GMT +00:00) UTC';
  tenants: Tenant[] = [];
  total: number = 0;

  userId: number = 0;

  params: any = {
    size: 10,
    page: 1,
    search: '',
    tenant_id: '',
    currency_id: '',
    action_type: '',
    emailsearch: '',
    datetime: {},
    time_period: {
      // start_date: '',
      // end_date: '',
    },
    order: 'desc',
    sort_by: 'created_at'
  };

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Bet Transactions', path: '/transactions/bet-transactions' },
  ];

  isLoader:boolean = false;
  firstTimeApiCall = false;

  constructor(private tenantSer: TenantService,
              private route: ActivatedRoute,
              private transactionService: TransactionService,
              public adminAuthService: AdminAuthService,
              private currencySer: CurrenciesService,
              public superAdminAuthService: SuperAdminAuthService,
              public permissionService: PermissionService) {
    const state = history.state;
    if (state) {
      this.params.wallet_id = state.wallet_id;
      this.params.search = state.search;
    }
  }

  ngOnInit(): void {
    // this.getTransactions();
    if(this.superAdminAuthService?.superAdminTokenValue?.length > 0) {
      this.getTenantsAll();
      this.getCurrencyList('0');
    }else{
      this.getCurrencyList();
    }
    if (this.adminAuthService.adminTokenValue && this.adminAuthService.adminTokenValue.length > 0) {
     
      this.adminAuthService.adminUser.subscribe((user: any) => {
        if(user && user.id) {
          // this.params = {...this.params, time_zone: user.timezone}
          setTimeout(() => {
            const _zone = TIMEZONE.find(t => t.zonename == user.timezone);
            $(`#time_zone`).val(_zone?.zonename).trigger('change');
          }, 100);
        }
      });
    }
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      $('#time_period').val('');
    }, 100);
  }

  selectDateRange(time_period: any) {
    this.params = { ...this.params, time_period }
    // this.getTransactions();
  }

  getTenantsAll() {
    this.tenantSer.getTenantsAll().subscribe((res: any) => {
      this.tenants = res.record;
    });
  }

  getCurrencies() {
    if(this.superAdminAuthService?.superAdminTokenValue?.length > 0) {
      this.currencySer.getCurrencies().subscribe((res: any) => {
        this.currencies = res.record;
      });
    } else {
      this.currencySer.getAdminCurrencies().subscribe((res: any) => {
        this.currencies = res.record;
      });
    }
  }

  onKeyPress(event: any) {
    const regexpNumber = /[0-9\+\-\ ]/;
    let inputCharacter = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !regexpNumber.test(inputCharacter)) {
      event.preventDefault();
    }
  }

  getTransactions() {
    this.isLoader = true
    this.firstTimeApiCall = true;
    if(this.params.time_period.start_date && this.params.time_period.end_date && this.params.time_zone_name && this.params.time_zone_name !== 'UTC +00:00'){
      this.params = {...this.params, datetime: {  start_date: this.convertToUTC(this.params.time_period.start_date, this.params.time_zone_name), end_date:this.convertToUTC(this.params.time_period.end_date, this.params.time_zone_name)}}
    } else{
      this.params = {...this.params, datetime: {  start_date: this.params.time_period.start_date, end_date:this.params.time_period.end_date}}
    }
    if(this.superAdminAuthService?.superAdminTokenValue?.length > 0) {
      this.transactionService.getBetTransactions(this.params).subscribe((res: any) => {
        this.transactions = res.record.hits.hits;
        this.total = res.record.hits.total.value;
        this.isLoader = false
      },(error:any) => {
        this.isLoader = false

      });
    } else {
      this.transactionService.getBetAdminTransactions(this.params).subscribe((res: any) => {
        this.transactions = res.record.hits.hits;
        this.total = res.record.hits.total.value;
        this.isLoader = false
      },(error:any) => {
        this.isLoader = false

      });
    }    
  }

  filter(evt: any) {
      this.p = 1;
      this.params = {...this.params, page: this.p};
      this.getTransactions();
  }
  
  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getTransactions();
  }

  getActionTypeName(id:any){
    let str='-';
    if (id == 1 || id == 9) {
      str = "BET PLACEMENT";
    } else if (id == 2) {
      str = "WON";
    } else if (id == 3) {
      str = "CASHOUT";
    } else if (id == 4) {
      str = "REFUND";
    } else if (id == 5) {
      str = "LOST BY RESETTLEMENT";
    } else if (id == 6) {
      str = "Deposit Bonus Claim";
    } else if (id == 7) {
      str = "Deposit Bonus Pending";
    } else if (id == 8) {
      str = "Deposit Bonus Cancel";
    }


    return str;
    //return TransactionTypes.filter(element => element.title === id)[0]['name']
  }

  calAmountNoncash(a:any,b:any){
    if(b)
      return (parseFloat(a)+parseFloat(b));
    else
      return (parseFloat(a));
  }

  resetFilter() {
    this.p = 1;
    $('#time_period').val('');
    this.params = {      
      size: 10,
      page: 1,
      search: '',
      tenant_id: '',
      currency_id: '',
      action_type: '',
      emailsearch: '',
      time_zone: this.params.time_zone,
      time_zone_name: this.params.time_zone_name,
      datetime:{},
      time_period: {
        // start_date: '',
        // end_date: '',
      },
      order: 'desc',
      sort_by: 'created_at'
    };
    // $( "#time_zone" ).val('UTC +00:00').trigger('change');
    this.getTransactions();
  }

  setTransaction(transaction: any) {
    this.transaction = transaction;
  }

  filterSelectTimeZone(zonename: any) {
    // this.p = 1;

    if(zonename) {
      const zone = TIMEZONE.find(t => t.zonename === zonename);
      this.zone = zone?.name;
      this.params = {...this.params, time_zone: zone?.value, time_zone_name: zone?.zonename};
    }else{
      this.zone='';
      this.params = {...this.params, time_zone: this.zone};
    }

    // this.getTransactions();
}
  exportAsXLSX() {
    if(this.params.time_period.start_date && this.params.time_period.end_date && this.params.time_zone_name && this.params.time_zone_name !== 'UTC +00:00'){
      this.params = {...this.params, datetime: {  start_date: this.convertToUTC(this.params.time_period.start_date, this.params.time_zone_name), end_date:this.convertToUTC(this.params.time_period.end_date, this.params.time_zone_name)}}
    } else{
      this.params = {...this.params, datetime: {  start_date: this.params.time_period.start_date, end_date:this.params.time_period.end_date}}
    }
    this.transactionService.downloadSportTransactions(this.params).subscribe(
      (res: any) => {
        window.location.href = res.record.url;
    });
  }
  convertToUTC(inputDate: string, inputTimezone: string): string {
    const date = moment.tz(inputDate, inputTimezone);
    const utcDate = date.utc().format('YYYY-MM-DD HH:mm:ss');
    return utcDate;
  }

  tenantFilter(evnt: any) {
    this.p = 1;
    this.params.page = this.p;
    this.params.tenant_id = evnt;
    this.getCurrencyList(evnt);
  }

  getCurrencyList(evnt=''){
    if(this.superAdminAuthService.superAdminTokenValue &&
        this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.transactionService.getCurrencyList(evnt).subscribe((res: any) => {
        this.currencies = res.record;
      });

    }else {

      this.transactionService.getCurrencyList().subscribe((res: any) => {
        this.currencies = res.record;
      });
    }
  }

  setOrder(sort: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p, order: sort.order, sort_by: sort.column };
    this.getTransactions();
  }

}
