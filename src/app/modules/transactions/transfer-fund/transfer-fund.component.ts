import { decimalDigest } from '@angular/compiler/src/i18n/digest';
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Admin, Currency } from 'src/app/models';
import { CustomValidators } from 'src/app/shared/validators';
import { environment } from 'src/environments/environment';
import { AdminAuthService } from '../../admin/services/admin-auth.service';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
import { CurrenciesService } from '../../super-admin/super-admin-modules/currencies/currencies.service';
import { TransactionService } from '../transaction.service';
import { Role } from 'src/app/models';
import { PermissionService } from 'src/app/services/permission.service';
import { generateApiUrl } from 'src/app/services/utils.service';

declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './transfer-fund.component.html',
  styleUrls: ['./transfer-fund.component.scss']
})

export class TransferFundComponent implements OnInit, AfterViewInit {

  admin!: Admin;
  superAdmin!: Admin;
  searchLeaguesUrl
  isAgent:boolean = false;
  isSubAdmin:boolean = false;
  transferAmount: number = 0;
  roles: string = localStorage.getItem('roles') || '';
  adminTargetWallete: any | null = null;
  targetWallete: any | null = null;
  type: any | null = null;
  isSelfTransfer:boolean = false;
  isSelfFundingEnable:boolean = false;
  adminWalletId:any = false;
  queryParams: any = { type: 'Deposit' };

  transferForm: FormGroup | any;
  submitted: boolean = false;
  transferLoader: boolean = false;

  wallets: any[] = [];
  currencies: any[] = [];

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'transactions', path: '/transactions' },
    { title: 'Transfer Fund', path: '/transactions' }
  ];

  constructor(private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    public adminAuthService: AdminAuthService,
    public superAdminAuthService: SuperAdminAuthService,
    private currenciesService: CurrenciesService,
    private transactionService: TransactionService,
    public permissionService: PermissionService) {

    if (this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.transferForm = this.formBuilder.group({
        target_type: ['AdminUser', [ Validators.required ]],
        target_email: ['', [ Validators.required ]],
        target_currency_id: ['', [ Validators.required ]],
        amount: ['', [ Validators.required, Validators.min(1) ]],
        comment: ['', [ Validators.required ]],
      });

      this.searchLeaguesUrl = generateApiUrl() + `super/admin/transactions/users/new?owner_type=${this.f.target_type.value}`;

    } else {

      this.isSelfFundingEnable = (localStorage.getItem('allowedModules')?.includes('selfFunding') ? true : false);


      this.transferForm = this.formBuilder.group({
        target_type: ['AdminUser', [ Validators.required ]],
        target_email: ['', [ Validators.required ]],
        target_currency_id: ['', [ Validators.required ]],
        amount: ['', [ Validators.required, Validators.min(1) ]],
        source_currency_id: [''],
        comment: ['', [ Validators.required ]],
      });

        if(this.roles) {
          const roles = JSON.parse(this.roles);
          if(roles && roles.findIndex( (role: any) => role === Role.Admin ) == -1 && roles.findIndex( (role: any) => role === Role.Agent ) > -1) {
            this.isAgent = true;
          }

        this.isSubAdmin= !(roles && roles.findIndex( (role: any) => role === Role.Admin ) == -1 || (roles.findIndex( (role: any) => role === Role.DepositAdmin ) > -1 && roles.findIndex( (role: any) => role === Role.WithdrawalAdmin ) > -1));

  
        }
      this.searchLeaguesUrl = generateApiUrl() + `admin/transactions/users/new?owner_type=${this.f.target_type.value}`;
    }
  }

  ngOnInit(): void {
    this.adminAuthService.adminUser.subscribe((user: any) => {
      if(user && user.id) {
        this.admin = user;
      }
    });
    this.getCurrencies();
  }

  ngAfterViewInit(): void {
    setTimeout(() => {

      this.route.queryParams.subscribe(params => {
        if (params.email && params.owner_id && params.owner_type && params.type) {
          this.queryParams = params;

          this.f.target_type.setValue(params.owner_type);

          this.changeType(params.owner_type);

          setTimeout(() => {
            this.f.target_email.setValue(params.owner_id);
          }, 50);

          var $newOption = $("<option></option>").val(params.owner_id).text(params.email)
          $("#target_email").append($newOption).trigger('change');

          this.getWallts(this.queryParams.owner_id);
        }

      });

      this.type = this.route.snapshot.queryParamMap.get('type');
      if(!this.type){
        this.type = 'deposit';
      }

        if (!this.superAdminAuthService.superAdminTokenValue) {
          this.getLogedWallet();
        }
    }, 0);
  }

  /*get getWall(){
    console.log(this.wallets);
    return this.wallets;
  }*/

  getLogedWallet() {
    this.transactionService.getLogedUserWallets().subscribe((res: any) => {
      this.currencies = res.record.wallets;
    });
  }

  changeType(event: any) {
    this.isSelfTransfer = false;
    // $("#target_email").val(null).trigger('change');
    $("#target_email"). empty();
    this.f.target_currency_id.setValue('');
    this.f.target_email.setValue('');
    if (this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.searchLeaguesUrl = generateApiUrl() + `super/admin/transactions/users/new?owner_type=${this.f.target_type.value}`;
    } else {
      if(this.f.target_type.value != 'self'){
        
        this.searchLeaguesUrl = generateApiUrl() + `admin/transactions/users/new?owner_type=${this.f.target_type.value}`;
      }
      else{
        this.isSelfTransfer = true;
        this.f.target_type.value = 'AdminUser';
        var $newOption = $("<option></option>").val(this.admin.id).text(this.admin.email)
        $("#target_email").append($newOption).trigger('change');
        this.f.target_email.setValue(this.admin.id)

        this.getWallts(this.admin.id);

        // this.searchLeaguesUrl = generateApiUrl() + `super/admin/transactions/users/new?owner_type=${this.f.target_type.value}`;
      }
    }
  }

  selectEmail(id: any) {
    this.f.target_email.setValue(id);
    this.getWallts(id);
  }

  getWallts(id: number) {
    let userType = this.f.target_type.value;
    this.f.target_currency_id.setValue('');
    
    if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.transactionService.getSuperAgentWallets(id, userType).subscribe((res: any) => {
        this.wallets = res.record;
        if (this.f.target_type.value === 'User') {
          this.f.target_currency_id.setValue(this.wallets[0].id);
        }
      });
    } else {
      this.transactionService.getAgentWallets(id, userType).subscribe((res: any) => {
        this.wallets = res.record;
        if (this.f.target_type.value === 'User' && this.wallets[0]) {
          this.f.target_currency_id.setValue(this.wallets[0].id);
        }
      });
    }
  }

  selectUserWallete(event: any) {
    this.adminWalletId = event.target.value;
    if(this.queryParams && this.queryParams.type == 'Withdraw' ) {
      this.targetWallete = this.wallets.find(f => f.id == event.target.value);
      // if(this.f.amount.value) {
      //   this.transferAmount = this.targetWallete.amount - parseFloat(this.f.amount.value);
      // } else {
      //   this.transferAmount = this.targetWallete.amount;
      // }
    }
  }

  selectAdminWallete(event: any) {
    this.adminTargetWallete = this.currencies.find(f => f.id == event.target.value);
    if(this.queryParams && this.queryParams.type == 'Deposit' ) {
      this.targetWallete = this.currencies.find(f => f.id == event.target.value);
        // if(this.f.amount.value) {
        //   this.transferAmount = this.targetWallete.amount - parseFloat(this.f.amount.value);
        // } else {
        //   this.transferAmount = this.targetWallete.amount;
        // }
    }
  }

  chAmount(event: any) {
    const amt = parseFloat(this.f.amount.value);
    // if(this.targetWallete?.amount) {
    //   this.transferAmount = this.targetWallete.amount - amt;
    // }
    //  else {
    //   this.transferAmount = 0;
    // }
  }

  getCurrencies() {
    if (this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.currenciesService.getCurrencies().subscribe((res: any) => {
        this.currencies = res.record;
      });
    } else {
      this.adminAuthService?.walletData.subscribe((wallets: any) => {
        this.currencies = wallets;
      });
    }
  }

  get f() {
    return this.transferForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid

    // console.log(this.transferForm.value);

    if (this.transferForm.invalid) return;

    const data = {
      ...this.transferForm.value,
      transaction_amount: this.f.amount.value,
      transaction_comments: this.f.comment.value,
    };

    this.adminAuthService.adminUser.subscribe((admin: any) => {
      this.admin = admin;
    });
    
    this.superAdminAuthService.superAdminUser.subscribe((superAdmin: any) => {
      this.superAdmin = superAdmin;
    });

    if (this.queryParams && this.queryParams.type == 'Withdraw') {
      data.source_wallete_id = this.f.target_currency_id.value;
      data.target_wallete_id = null;
      data.transaction_type = "withdraw";
      if (!this.superAdminAuthService.superAdminTokenValue) {
        data.target_wallete_id = this.f.source_currency_id.value;
      }

    } else {
      data.target_wallete_id = this.f.target_currency_id.value;
      data.transaction_type = "deposit";

      if (!this.superAdminAuthService.superAdminTokenValue) {
        data.source_wallete_id = this.f.source_currency_id.value;
      }

    }

    delete data.amount;
    delete data.comment;
    delete data.target_currency_id;
    delete data.source_currency_id;

    if(!this.superAdminAuthService.superAdminTokenValue ) {
      delete data.source_currency_id;
    }

    if(this.isSelfTransfer == true){
      data.transferType = 'self';
    }

    this.transferLoader = true;

    if (this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.transactionService.createSuperTransferFund(data) .subscribe((response: any) => {

        toastr.success(response ?.message || 'Fund Transferred Successfully!');
        this.router.navigate(['/transactions']);

        if (this.queryParams && this.queryParams.type == 'Withdraw') {

          const wallets = this.superAdminAuthService?.walletData?.value.map((m: any) => {
            m.amount = parseFloat(m.amount) + parseFloat(this.f.amount.value);
            return m;
          });

          this.superAdminAuthService.walletData.next(wallets);
            // this.superAdmin.wallets = this.superAdmin.wallets.map((m: any) => {
            //     m.amount = parseFloat(m.amount) + parseFloat(this.f.amount.value);
            //   return m;
            // });

        } else {
            const wallets = this.superAdminAuthService?.walletData?.value.map((m: any) => {
              m.amount = parseFloat(m.amount) - parseFloat(this.f.amount.value);
            return m;
          });

            this.superAdminAuthService.walletData.next(wallets);

            // this.superAdmin.wallets = this.superAdmin.wallets.map((m: any) => {
            //     m.amount = parseFloat(m.amount) - parseFloat(this.f.amount.value);
            //   return m;
            // });
        }

        
        // this.adminAuthService.updateUser(this.admin);
        // this.superAdminAuthService.updateUser(this.superAdmin);
      }, (err: any) => {
        this.transferLoader = false;
      });


    } else {
      this.transactionService.createTransferFund(data).subscribe((response: any) => {

        toastr.success(response ?.message || 'Fund Transferred Successfully!');

        if (this.queryParams && this.queryParams.type == 'Withdraw') {

          if (!this.superAdminAuthService.superAdminTokenValue) {
            data.target_wallete_id = this.f.source_currency_id.value;

            const wallets = this.adminAuthService?.walletData?.value.map((m: any) => {
              if (m.id == this.adminTargetWallete.id) {
                m.amount = parseFloat(m.amount) + parseFloat(this.f.amount.value);
              }
              return m;
            });
  
            this.adminAuthService.walletData.next(wallets);

            // this.admin.wallets = this.admin.wallets.map((m: any) => {
            //   if (m.id == this.adminTargetWallete.id) {
            //     m.amount = parseFloat(m.amount) + parseFloat(this.f.amount.value);
            //   }
            //   return m;
            // });
          }

        } else {

          if (!this.superAdminAuthService.superAdminTokenValue) {
            if(this.isSelfTransfer === false){
              data.source_wallete_id = this.f.source_currency_id.value;

              const wallets = this.adminAuthService?.walletData?.value.map((m: any) => {
                if (m.id == this.adminTargetWallete.id) {
                  m.amount = parseFloat(m.amount) - parseFloat(this.f.amount.value);
                }
                return m;
              });
    
              this.adminAuthService.walletData.next(wallets);

              // this.admin.wallets = this.admin.wallets.map((m: any) => {
              //   if (m.id == this.adminTargetWallete.id) {
              //     m.amount = parseFloat(m.amount) - parseFloat(this.f.amount.value);
              //   }
              //   return m;
              // });
            }else{
              const wallets = this.adminAuthService?.walletData?.value.map((m: any) => {
                if (m.id == this.adminWalletId) {
                  m.amount = parseFloat(m.amount) + parseFloat(this.f.amount.value);
                }
                return m;
              });
    
              this.adminAuthService.walletData.next(wallets);

              // this.admin.wallets = this.admin.wallets.map((m: any) => {
              //   console.log('-------------------fist',this.adminWalletId);
              //   console.log('===============================Main',m.id);
                
              //   if (m.id == this.adminWalletId) {
              //     m.amount = parseFloat(m.amount) + parseFloat(this.f.amount.value);
              //   }
              //   return m;
              // });
            }
           
          }
        }

        this.router.navigate(['/transactions']);
        // this.adminAuthService.updateUser(this.admin);

      }, (err: any) => {
        this.transferLoader = false;
      });

    }
  }

}
