import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SuperAdminAuthService } from '../super-admin/services/super-admin-auth.service';

@Injectable({ providedIn: 'root' })

export class TransactionService {

  constructor(private http: HttpClient,
  public superAdminAuthService: SuperAdminAuthService) { }

  // Admin URL

  getAdminTransactions(params: any) {
    return this.http.post(`admin/transactions`, params);
  }

  getAdminTransactionsByES(params: any) {
    return this.http.post(`admin/transactions/record`, params);
  }

  getBetAdminTransactions(params: any) {
    return this.http.post(`admin/transactions/list`, params);
  }

  // Super Admin URL

  getTransactions(params: any) {
    return this.http.post(`super/admin/transactions`, params);
  }

  getTransactionsByES(params: any) {
    return this.http.post(`super/admin/transactions/record`, params);
  }

  getBetTransactions(params: any) {
    return this.http.post(`super/admin/transactions/list`, params);
  }

  createTransferFund(fund: any) {//super
    return this.http.post(`admin/transactions/transfer-fund/new`, fund);
  }

  createSuperTransferFund(fund: any) {
    return this.http.post(`super/admin/transactions/transfer-fund/new`, fund);
  }

  createAdminTransferFund(fund: any) {
    return this.http.post(`admin/transactions/transfer-fund/new`, fund);
  }

  getTransfer(id: number) {
    return this.http.get(`super/admin/transactions/${id}`);
  }

  getLogedUserWallets() {
    return this.http.get(`admin/user`);
  }

  // getAgentByEmail(params: any) {
  //   return this.http.post(`admin/transactions/users/new`, params);
  // }

  getAgentWallets(id: number, type:any) {
      let typeStr='?type='+type;
    return this.http.get(`admin/transactions/users/wallets/${id}`+typeStr);
  }

  // Super
  getSuperAgentWallets(id: number, type:any) {
    let typeStr='?type='+type
    return this.http.get(`super/admin/transactions/users/wallets/${id}`+typeStr);
  }

  downloadTransactions(params: any) {
    if(this.superAdminAuthService && this.superAdminAuthService.superAdminTokenValue) {
      // ====================== Super Admin url ==============================
      return this.http.post(`super/admin/transactions/download`, params) ;
    } else {
      // ====================== Admin url ==============================
      return this.http.post(`admin/transactions/download`, params);
    }
  }
  downloadTransactionsByES(params: any) {
    if(this.superAdminAuthService && this.superAdminAuthService.superAdminTokenValue) {
      // ====================== Super Admin url ==============================
      return this.http.post(`super/admin/transactions/downloadByES`, params) ;
    } else {
      // ====================== Admin url ==============================
      return this.http.post(`admin/transactions/downloadByES`, params);
    }
  }
  downloadSportTransactions(params: any) {
    if(this.superAdminAuthService && this.superAdminAuthService.superAdminTokenValue) {
      // ====================== Super Admin url ==============================
      return this.http.post(`super/admin/transactions/listdownload`, params) ;
    } else {
      // ====================== Admin url ==============================
      return this.http.post(`admin/transactions/listdownload`, params);
    }
  }

  getCurrencyList(tenant_id='') {
    if(tenant_id){
      return this.http.get(`super/admin/tenants/currencies/list?tenant_id=`+tenant_id);
    }else{
      return this.http.get(`admin/tenants/currencies`);
    }
  }
  

}
