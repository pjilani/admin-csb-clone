import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCloneComponent } from './list-clone.component';

describe('ListCloneComponent', () => {
  let component: ListCloneComponent;
  let fixture: ComponentFixture<ListCloneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListCloneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCloneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
