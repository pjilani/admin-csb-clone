import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Currency, Tenant, Transaction } from 'src/app/models';
import { TransactionTypes, PageSizes, TIMEZONE, getTransactionType } from 'src/app/shared/constants';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
import { CurrenciesService } from '../../super-admin/super-admin-modules/currencies/currencies.service';
import { TenantService } from '../../tenant/tenant.service';
import { TransactionService } from '../transaction.service';
import { Role } from 'src/app/models';
import { DomainForDisableModule } from 'src/app/shared/constants';
import { AdminAuthService } from '../../admin/services/admin-auth.service';
import * as moment from 'moment-timezone';
import { PermissionService } from 'src/app/services/permission.service';

declare const $: any;

@Component({
  selector: 'app-list-clone',
  templateUrl: './list-clone.component.html',
  styleUrls: ['./list-clone.component.scss']
})

export class ListCloneComponent implements OnInit, AfterViewInit{

  pageSizes: any[] = PageSizes;

  transactionTypes = TransactionTypes;
  isAgent:boolean = false;
  isAllowedModule:any = '';
  zone: any = '(GMT +00:00) UTC';

  DomainForDisableModule:any = DomainForDisableModule;
  currencies: Currency[] = [];
  transactions: Transaction[] = [];
  p: number = 1;
  TIMEZONE: any[] = TIMEZONE;

  format: string = "YYYY-MM-DD HH:mm:ss";

  tenants: Tenant[] = [];
  total: number = 0;

  userId: number = 0;
  time:any;
  params: any = {
    size: 10,
    page: 1,
    search: '',
    round_id: '',
    id:'',
    tenant_id: '',
    datetime: {},
    order: 'desc',
    sort_by: 'created_at',
    currency_id: '',
    action_type: '',
    time_period: {
      // start_date: '',
      // end_date: '',
    }
  };

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Transactions', path: '/transactions' },
  ];

  roles: string = localStorage.getItem('roles') || '';
  showActionTypeFilter:boolean=true;
  isLoader:boolean = false;
  firstTimeApiCall = false;

  constructor(private tenantSer: TenantService,
    private transactionService: TransactionService, 
    public adminAuthService: AdminAuthService,
    private currencySer: CurrenciesService,
    public superAdminAuthService: SuperAdminAuthService,
    public permissionService: PermissionService) {
    const state = history.state;
      if (state) {
        this.params.wallet_id = state.wallet_id;
        this.params.search = state.search;
      }
    }

  ngOnInit(): void {
    // this.getTransactions();
    if(this.superAdminAuthService?.superAdminTokenValue?.length > 0) {
      this.getTenantsAll();
      this.getCurrencyList('0');
    }else{
      this.getCurrencyList();
    }

    const roles = JSON.parse(this.roles);
    this.showActionTypeFilter= !(roles && roles.findIndex( (role: any) => role === Role.Admin ) == -1 || (roles.findIndex( (role: any) => role === Role.DepositAdmin ) > -1 && roles.findIndex( (role: any) => role === Role.WithdrawalAdmin ) > -1));


    this.isAllowedModule = (localStorage.getItem('domain') ? localStorage.getItem('domain') : '');
    
  
    if (this.adminAuthService.adminTokenValue && this.adminAuthService.adminTokenValue.length > 0) {

      this.adminAuthService.adminUser.subscribe((user: any) => {
        if(user && user.id) {
          // this.params = {...this.params, time_zone: user.timezone}
          setTimeout(() => {
            const _zone = TIMEZONE.find(t => t.zonename == user.timezone);
            $(`#time_zone`).val(_zone?.zonename).trigger('change');
          }, 100);
        }
      });
      if(this.roles) {
        const roles = JSON.parse(this.roles);
        if(roles && roles.findIndex( (role: any) => role === Role.Admin ) == -1 && roles.findIndex( (role: any) => role === Role.Agent ) > -1) {
          this.isAgent = true;
        }

      }

    }
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      $('#time_period').val('');
    }, 100);
  }

  
  getTenantsAll() {
    this.tenantSer.getTenantsAll().subscribe((res: any) => {
      this.tenants = res.record;
    });
  }

  getCurrencies() {
    if(this.superAdminAuthService?.superAdminTokenValue?.length > 0) {
      this.currencySer.getCurrencies().subscribe((res: any) => {
        this.currencies = res.record;
      });
    } else {
      this.currencySer.getAdminCurrencies().subscribe((res: any) => {
        this.currencies = res.record;
      });
    }
  }
  convertToUTC(inputDate: string, inputTimezone: string): string {
    const date = moment.tz(inputDate, inputTimezone);
    const utcDate = date.utc().format('YYYY-MM-DD HH:mm:ss');
    return utcDate;
  }

  getTransactions() {
    this.isLoader = true
    this.firstTimeApiCall = true;
    if(this.params.time_period.start_date && this.params.time_period.end_date && this.params.time_zone_name && this.params.time_zone_name !== 'UTC +00:00'){
      this.params = {...this.params, datetime: {  start_date: this.convertToUTC(this.params.time_period.start_date, this.params.time_zone_name), end_date:this.convertToUTC(this.params.time_period.end_date, this.params.time_zone_name)}}
    } else{
      this.params = {...this.params, datetime: {  start_date: this.params.time_period.start_date, end_date:this.params.time_period.end_date}}
    }
    if(this.superAdminAuthService?.superAdminTokenValue?.length > 0) {
      this.transactionService.getTransactionsByES(this.params).subscribe((res: any) => {
        this.transactions = res.record.hits.hits;
        this.total = res.record.hits.total.value;
        this.isLoader = false
      },
      (error:any) => {
        this.isLoader = false
      });
    } else {
      this.transactionService.getAdminTransactionsByES(this.params).subscribe((res: any) => {
        this.transactions = res.record.hits.hits;
        this.total = res.record.hits.total.value;
        this.isLoader = false
      },
      (error:any) => {
        this.isLoader = false

      });
    }    
  }

  filter(evt: any) {
    this.p = 1;
    if(this.time != null){
    this.params = { ...this.params, time_period: this.time };
    }
    this.params = { ...this.params, page: this.p };
    this.getTransactions();
  }

  selectDateRange(time_period: any) {
    this.time = time_period
    // this.p = 1;
    // this.params = { ...this.params, time_period, page: this.p }
    // this.getTransactions();
  }
  
  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getTransactions();
  }
  
  getActionTypeName(id:number){
    return TransactionTypes.filter(element => element.value === id)[0]['name']
  }
  setOrder(sort: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p, order: sort.order, sort_by: sort.column };
    this.getTransactions();
  }
  resetFilter() {
    this.p = 1;
    $('#time_period').val('');
    this.params = {      
      size: 10,
      page: 1,
      search: '',
      order: 'desc',
      sort_by: 'created_at',
      round_id: '',
      tenant_id: '',
      id:'',
      currency_id: '',
      datetime:{},
      action_type: '',
      time_period: {
        // start_date: '',
        // end_date: '',
      }
    };

    this.getTransactions();
  }
  filterSelectTimeZone(zonename: any) {
    // this.p = 1;

    if(zonename) {
      const zone = TIMEZONE.find(t => t.zonename === zonename);
      this.zone = zone?.name;
      this.params = {...this.params, time_zone: zone?.value, time_zone_name: zone?.zonename};
    }else{
      this.zone='';
      this.params = {...this.params, time_zone: this.zone};
    }

    // this.getTransactions();
}

  exportAsXLSX() {
    if(this.params.time_period.start_date && this.params.time_period.end_date && this.params.time_zone_name && this.params.time_zone_name !== 'UTC +00:00'){
      this.params = {...this.params, datetime: {  start_date: this.convertToUTC(this.params.time_period.start_date, this.params.time_zone_name), end_date:this.convertToUTC(this.params.time_period.end_date, this.params.time_zone_name)}}
    } else{
      this.params = {...this.params, datetime: {  start_date: this.params.time_period.start_date, end_date:this.params.time_period.end_date}}
    }
    this.transactionService.downloadTransactionsByES(this.params).subscribe(
      (res: any) => {
        window.location.href = res.record.url;
    });
  }

  trnType(value:any){
    return getTransactionType(value)
  }

  tenantFilter(evnt: any) {
    this.p = 1;
    this.params.page = this.p;
    this.params.tenant_id = evnt;
    this.getCurrencyList(evnt);
  }

  getCurrencyList(evnt=''){
    if(this.superAdminAuthService.superAdminTokenValue &&
        this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.transactionService.getCurrencyList(evnt).subscribe((res: any) => {
        this.currencies = res.record;
      });

    }else {

      this.transactionService.getCurrencyList().subscribe((res: any) => {
        this.currencies = res.record;
      });
    }
  }
}

