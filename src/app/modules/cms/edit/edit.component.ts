import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Currency, Language, Layout, Admin, Theme } from 'src/app/models';
import { Constants } from 'src/app/shared/constants';
import { CustomValidators } from 'src/app/shared/validators';
import { CmsService } from '../cms.service';
import Swal from 'sweetalert2';
declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})

export class EditComponent implements OnInit {
  
  
  id: number = 0;
  cms:any = [];
  content:any = [];
  cmsForm: FormGroup | any;
  submitted: boolean = false;
  cmsLoader: boolean = false;

 

  constructor(private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private CmsService: CmsService,
  ) {
    
    this.id = this.route.snapshot.params['id'];
    this.cmsForm = this.formBuilder.group({
      title: ['', [ Validators.required ]],
      slug: ['', [ Validators.required ]],
      active: [''],
      id: [''],
      enable_flag: [''],
      content: this.formBuilder.group({}),
    });

    let title: string = 'Create';
    
    if(this.id > 0) {
      title = "Edit";
    }

    this.breadcrumbs.push({ title, path: `/cms/${this.id}` });

  }


  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'CMS', path: '/cms' }
  ];

  ngOnInit(): void { 
    if(this.id > 0) {
      this.getCmsById(this.id);
    }else{
      this.content = [
        {
          "type" : "EN",
          "value" : ""
        }
      ]
      this.content.forEach((data: { value: string; type: any; }) => {
        (this.cmsForm.get("content") as FormGroup).addControl(
          data.type,
          new FormControl(data.value, Validators.required)
        );
        
      });
    } 
  }


  get f() {
    return this.cmsForm.controls;
  }
 

  getCmsById(id:any) {
    this.CmsService.getCmsPages({'id':id}).subscribe((res: any) => {
      this.cms = res.record.list[0];
      this.content = JSON.parse(this.cms.content);
      this.content = Object.keys(this.content).map(key => ({type: key, value: this.content[key]}));;
      
      // console.log(this.content);
      this.content.forEach((data: { value: string; type: any; }) => {
        (this.cmsForm.get("content") as FormGroup).addControl(
          data.type,
          new FormControl(data.value, Validators.required)
        );
        
      });

      this.cmsForm.patchValue({
        title: this.cms.title,
        slug: this.cms.slug,
        active: this.cms.active,
        id: this.cms.id,
        enable_flag: this.cms.enable_cms_for_register,
      });
      
    });
  }

  userWarning(event:any){
    let pMessage = '';
    if(event.target.checked){
      pMessage = `You want to Enable it!, If you Enable this flag, if any of flag is already active, then it will be disabled. And this page is visible to all the player's`;
    }else{
      pMessage = "You want to Disable it! If you Disable this flag, No page will be displayed on the player's side registration.";
    }
    Swal.fire({
      title: 'Are you sure?',
      text: pMessage,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, `+ (event.target.checked ? 'Activate ' : 'Deactivate') +` it!`
    }).then((result) => {
      if (result.isConfirmed) {

        this.cmsForm.patchValue({
          enable_flag: (event.target.checked ? true : false),
        });
        
      }else{
        this.cmsForm.patchValue({
          enable_flag: (event.target.checked ? false : true),
        });
      }

    });
  }

  removeSpace(event:any){
    var k;  
    k = event.charCode;
    return(((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 45 || k== 95) && event.keyCode != 32 ? event:event.preventDefault());

  }
 
  onSubmit() {
    this.submitted = true;

    if (this.cmsForm.invalid){
      return;
    } 
    this.cmsLoader = true;
    
    const data = this.cmsForm.value;
      this.CmsService.createCms(data)
      .subscribe((response: any) => {
        if(this.id > 0) {
          toastr.success(response?.message || 'CMS Page Updated Successfully!');
        }else{
          toastr.success(response?.message || 'CMS Page Created Successfully!');
        }
        this.router.navigate(['/cms']);
        this.cmsLoader = false; 
      }, (err: any) => {
        this.cmsLoader = false; 
      });
  
  }

}
