import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })

export class CmsService {

 

  constructor(private http: HttpClient) { }

  
  // ====================== Admin url ==============================

  
  getCmsPages(params: any = []) {
    return this.http.get(`admin/cms/list`, {params: params});
  }

  createCms(data: any) {
    return this.http.post(`admin/cms/add`, data);
  }

  updateCmsStatus(data:any){
    return this.http.post(`admin/cms/status`, data);
      
  }


}
