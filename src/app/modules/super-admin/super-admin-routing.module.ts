import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const superAdminRoutes: Routes = [
  {
    path: '',
    loadChildren: () => import('./../dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./super-admin-modules/profile/profile.module').then(m => m.ProfileModule)
  },
  {
    path: 'tenants',
    loadChildren: () => import('./super-admin-modules/tenants/tenants.module').then(m => m.TenantsModule)
  },
  {
    path: 'payment-providers',
    loadChildren: () => import('./super-admin-modules/payment-providers/payment-providers.module').then(m => m.PaymentProvidersModule)
  },
  {
    path: 'admins',
    loadChildren: () => import('./super-admin-modules/admins/admins.module').then(m => m.AdminsModule)
  },
  {
    path: 'themes',
    loadChildren: () => import('./super-admin-modules/themes/themes.module').then(m => m.ThemesModule)
  },
  {
    path: 'currencies',
    loadChildren: () => import('./super-admin-modules/currencies/currencies.module').then(m => m.CurrenciesModule)
  },
  {
    path: 'menu',
    loadChildren: () => import('./super-admin-modules/menumaster/menu.module').then(m => m.MenuModule)
  },
  {
    path: 'casino',
    loadChildren: () => import('./super-admin-modules/casino/casino.module').then(m => m.CasinoModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('./super-admin-modules/setting/setting.module').then(m => m.SettingModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(superAdminRoutes)],
  exports: [RouterModule]
})

export class SuperAdminRoutingModule { }
