import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/app/models';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
declare const toastr: any;

@Injectable({ providedIn: 'root' })

export class SuperAdminAuthService {

  private superAdminTokenSubject!: BehaviorSubject<String>;
  public superAdminToken!: Observable<String>;
  public superAdminUser = new BehaviorSubject({});
  public superAdminUserSubject!: BehaviorSubject<User>;
  // public superAdminUser!: Observable<User>;

  public walletData:any = new BehaviorSubject([]);

    constructor(
      private http: HttpClient,
        private route: Router) {
          this.superAdminTokenSubject = new BehaviorSubject<String>(localStorage.getItem('SuperAdminAuthToken') || '');
          this.superAdminToken = this.superAdminTokenSubject.asObservable();
        }

    public get superAdminTokenValue(): String {
        return this.superAdminTokenSubject.value;
    }

    public get superAdminUserValue(): User {
      return this.superAdminUserSubject?.value;
    }

  login(userParams: User): Observable<any> {
    return this.http.post<any>(`login/super`, userParams)
    .pipe(map(userData => {

        localStorage.setItem('SuperAdminAuthToken', userData?.record?.token || '');
        localStorage.setItem('role', userData?.record?.user?.role || '');
        this.superAdminTokenSubject.next(userData?.record?.token || null);

        localStorage.setItem('userData', JSON.stringify(userData?.record?.user || {}))
        this.superAdminUserSubject = new BehaviorSubject<User>(userData?.record?.user);
        this.updateUser(userData?.record?.user);

        toastr.success(userData?.message || 'Login Successfull');
        
        return userData;
    }));
  }

  logout() {
      return this.http.post(`logout/admin/super`, {})
      .pipe(map((user: any) => {
        toastr.success('Logout Successfull');
        this.navigateToLogin();
        return user;
      }));
  }

  navigateToLogin(message?: string) {
      this.route.navigate(['/super-admin/login']);

      localStorage.removeItem('SuperAdminAuthToken');
      this.superAdminTokenSubject.next('');
      
      if(message) {
        toastr.options = {
          "preventDuplicates": true,
          "preventOpenDuplicates": true
        };
        toastr.error(message);
      }
  }

  getPersonalDetails() {
    return this.http.get('super/admin/user').pipe(map((res: any) => {
      localStorage.setItem('userData', JSON.stringify(res.record || {}))
      this.superAdminUserSubject = new BehaviorSubject<User>(res.record);
      this.updateUser(res.record);
    }));
  }

  updateUser(admin: any) {
    this.superAdminUser.next(admin);
  }

  getWalletDetails(){
    return this.http.get('super/admin/user/wallet').pipe(map((res:any) => {
      this.walletData.next(res.record)
    }));
  }

}
