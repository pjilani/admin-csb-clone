import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SuperAdminAuthService } from './../../services/super-admin-auth.service';
import { CustomValidators } from 'src/app/shared/validators';
import { encryptPassword } from 'src/app/services/utils.service';
declare const $: any;
declare const toastr: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit, AfterViewInit, OnDestroy {
  hideShowPass: boolean = false;
  
  returnUrl: string = '/super-admin';
  managerReturnUrl: string = '/super-admin/tenants';
  loginForm: FormGroup | any;
  submitted: boolean = false;
  loginLoader: boolean = false;

  constructor(private formBuilder: FormBuilder,
    private superAdminAuthService: SuperAdminAuthService,
    private router: Router,
    private route: ActivatedRoute,
    ) {
      this.loginForm = this.formBuilder.group({
        email: ['', [ Validators.required, Validators.pattern(CustomValidators.emailRegEx) ]],
        password: ['', [ Validators.required ]],
        remember_me: [ false ],
      });
    
      const SAAdata = localStorage.getItem('SAADATA') || '';

      if (SAAdata && SAAdata.length > 0) {
        const jsonSAAdata = JSON.parse(atob(SAAdata));
        this.loginForm.patchValue(jsonSAAdata);
      }

    }

  ngOnInit(): void {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/super-admin';
  }

  ngAfterViewInit(): void {
    $(document.body).addClass('hold-transition login-page');
    $("#appIcon").attr("href", "./assets/dist/img/s_logo.ico");
  }

  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) return;

    if (this.f.remember_me.value) {
      
      const SAAdata = btoa( JSON.stringify({
        email: this.f.email.value,
        password: this.f.password.value,
        remember_me: this.f.remember_me.value
      }) );
      
      localStorage.setItem('SAADATA', SAAdata);
    } else {
      localStorage.removeItem('SAADATA');
    }

    const data = this.loginForm.value;
    data.password = encryptPassword(data.password);
    
    this.loginLoader = true;
    this.superAdminAuthService.login(data)
      .subscribe((response: any) => {
        
       if(response.record.user.role == 'manager'){
         this.router.navigate([ this.managerReturnUrl ]);

       }else{
         this.router.navigate([ this.returnUrl ]);

       }
        

      }, (err: any) => {
        this.loginLoader = false; 
      });
  }

  ngOnDestroy(): void {
    $(document.body).removeClass('login-page');
  }

}
