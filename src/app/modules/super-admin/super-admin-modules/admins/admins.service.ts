import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })

export class AdminsService {

 

  constructor(private http: HttpClient) { }

  
  // ====================== Super Admin url ==============================

  
  createAdmins(admin: any) {
    return this.http.post(`super/admin/admins`, admin);
  }

  getAdmins(params: any) {
    return this.http.post(`super/admin/admins/lists`, params);
  }

  getRoles(params: any = []) {
    return this.http.post(`super/admin/admins/roles/list`, params);
  }


  updateAdmins(id: number, admin: any) {
    return this.http.post(`super/admin/admins/${id}`, admin);
  }

  deactiveAdminStatus(params: any) {
    return this.http.post(`super/admin/admins/update/status`, params);
  }


}
