import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Currency, Language, Layout, Admin, Theme } from 'src/app/models';
import { Constants } from 'src/app/shared/constants';
import { CustomValidators } from 'src/app/shared/validators';
import { CurrenciesService } from '../../currencies/currencies.service';
import { ThemesService } from '../../themes/themes.service';
import { AdminsService } from '../admins.service';
import { encryptPassword, truncateString } from 'src/app/services/utils.service';
declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})

export class EditComponent implements OnInit, AfterViewInit {
  @ViewChild('file') logo_file!: ElementRef;
  
  imgURL: any;
  id: number = 0;
  editType: number;
  role: any[] = [];
  hideShowPass: boolean = false;
  admin!: any;
  adminRecords: any;
  roles: any;
  adminForm: FormGroup | any;
  submitted: boolean = false;
  adminLoader: boolean = false;
  selectLogo!: File;
  theme: any = { ...Constants.INIT_THEME };

 

  constructor(private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private adminService: AdminsService,
    private themesService: ThemesService,
    private currenciesService: CurrenciesService,
  ) {
    
     this.id = this.route.snapshot.params['id'];
     this.editType = this.route.snapshot.params['type'];
    this.adminForm = this.formBuilder.group({
      first_name: ['', [ Validators.required ]],
      last_name: ['', [ Validators.required ]],
      email: ['', [ Validators.required ]],
      password: ['', [ Validators.pattern(CustomValidators.passwordRegex) ]],
      // role: ['', [ Validators.required ]]
    });

    let title: string = 'Create';
    
    if(this.id > 0) {
      title = "Edit";
    

    }else{
      const passControl = this.adminForm.get('password');
      passControl.setValidators([Validators.required, Validators.pattern(CustomValidators.passwordRegex)]); 
      passControl.updateValueAndValidity();
    }

  this.breadcrumbs.push({ title, path: `/super-admin/admins/${this.id}` });

  }


  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: (this.route.snapshot.params['type'] == 0)? 'Manager': 'Super Admin', path: '/super-admin/'+(this.route.snapshot.params['type'] == 0 ?"admins" : 'profile/'+this.route.snapshot.params['id'])  }
  ];

  ngOnInit(): void { 
    this.getRoles();
  }

  ngAfterViewInit(): void {

  
    if(this.id > 0) {

      this.adminService.getAdmins({"id":this.id}).subscribe((res: any) => {

        this.admin = res.record.data[0];

        this.adminForm.patchValue({
          first_name: (this.admin.first_name ? this.admin.first_name:''),
          last_name: (this.admin.last_name ? this.admin.last_name:''),
          email: (this.admin.email ? this.admin.email:''),
          // role: (this.admin.super_role_id ? this.admin.super_role_id:'')
        });
        // setTimeout(() => {
        //   $("#role").val(this.admin.super_role_id).trigger('change');
        // }, 100);

      });

    } 
  }

 
  // selectRole(role: any) {
  //   this.f.role.setValue(role);
  // }

  get f() {
    return this.adminForm.controls;
  }

  getRoles() {
    // this.adminService.getRoles().subscribe((res: any) => {
    //   this.roles = res.record;
      
    // });
  }

  onSubmit() {

 
    
    this.submitted = true;

    if (this.adminForm.invalid){
      return;
    } 
    
    this.adminLoader = true;

    const data = this.adminForm.value;
    const fd = new FormData();
   
    if(data.password) {
      data.password = encryptPassword(data.password);
    }

    data.role = 2; // for manager role 

    
    if(this.id > 0) {

      this.adminService.updateAdmins(this.id, data)
      .subscribe((response: any) => {
        toastr.success(response?.message || 'Manager Updated Successfully!');
        if(this.editType == 0){

          this.router.navigate(['/super-admin/admins']);
        }else{
          this.router.navigate(['/super-admin/profile/'+this.id]);

        }
        this.adminLoader = false; 
      }, (err: any) => {
        this.adminLoader = false; 
      });
      

    } else {
      
      this.adminService.createAdmins(data)
      .subscribe((response: any) => {
        toastr.success(response?.message || 'Manager Created Successfully!');
        this.router.navigate(['/super-admin/admins']);
        this.adminLoader = false; 
      }, (err: any) => {
        this.adminLoader = false; 
      });

    }
  }

}
