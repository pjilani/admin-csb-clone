import { Component, OnInit } from '@angular/core';
import { AdminsService } from '../admins.service';
import { PageSizes } from 'src/app/shared/constants';
import { SuperAdminAuthService } from '../../../../super-admin/services/super-admin-auth.service';
import Swal from 'sweetalert2';
declare const toastr: any;

@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {
  pageSizes: any[] = PageSizes;

  params: any = {
    size: 10,
    page: 1,
    name: '',
    role: ''
  };

  p: number = 1;
  total: number = 0;
  admins: any[] = [];
  roles: any[] = [];
  firstTimeApiCall = false;

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Admins', path: '/super-admin/admins' },
  ];

  constructor(private adminService: AdminsService,public superAdminAuthService: SuperAdminAuthService) { }

  ngOnInit(): void {
    // this.getAdmins();
    this.getRoles();
  }

  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getAdmins();
  }

  getAdmins() {
    this.firstTimeApiCall = true;
    this.adminService.getAdmins(this.params).subscribe((res: any) => {
      this.admins = res.record.data;
      this.total = res.record.count;
    });
  }
 
  getRoles() {
    this.adminService.getRoles(this.params).subscribe((res: any) => {
      this.roles = res.record;
      
    });
  }

  filter(event: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getAdmins();
  }

  resetFilter() {
    this.p = 1;
    this.params = { 
      ...this.params, 
      size: 10,
      page: 1,
      name: '',
      role: '' 
    }
    this.getAdmins();
  }


  deactiveManagerStatus(manager: any,status:boolean) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to ${manager.active ? 'Deactivate' : 'Active'} ${manager.first_name}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, ${manager.active ? 'Deactivate' : 'Active'} it!`
    }).then((result) => {
      if (result.isConfirmed) {

        this.adminService.deactiveAdminStatus({'id':manager.id,'status':status}).subscribe((res: any) => {
        toastr.success(res.message || 'Manager updated successfully' );
        this.getAdmins();
      });
        
      }
    });
    
  }


}
