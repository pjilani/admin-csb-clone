import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { RouterModule, Routes } from '@angular/router';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { EditComponent } from './edit/edit.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { PipeModule } from 'src/app/pipes/pipes.module';

const tenantsRoutes: Routes = [
  { path: '', component: ListComponent },
  { path: ':id/:type', component: EditComponent }
]

@NgModule({
  declarations: [
    ListComponent,
    EditComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(tenantsRoutes),
    SharedModule,
    ComponentsModule,
    PipeModule,
    DirectivesModule,
  ]
})

export class AdminsModule { }
