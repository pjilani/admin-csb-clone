import { AfterViewInit, Component, OnInit } from '@angular/core';
import { AdminAuthService } from 'src/app/modules/admin/services/admin-auth.service';
import { TIMEZONE } from 'src/app/shared/constants';
import { CurrenciesService } from '../currencies.service';
import { DatePipe } from '@angular/common';
declare const $: any;

@Component({
  selector: 'app-conversion-history',
  templateUrl: './conversion-history.component.html',
  styleUrls: ['./conversion-history.component.scss']
})
export class ConversionHistoryComponent implements OnInit, AfterViewInit {
 
  constructor(public adminAuthService: AdminAuthService, private currenciesService: CurrenciesService, 
    private datePipe: DatePipe
    
    ) { }
  isLoader:boolean = false;
  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Currencies', path: '/super-admin/currencies' },
    { title: 'Conversion-history', path: '/super-admin/currencies/conversion-history' }
  ];
  zone: any = '(GMT +00:00) UTC';
  p: number = 1;
  selectedTimeZone: any = 'UTC +00:00';
  TIMEZONE: any[] = TIMEZONE;
  conversionHistoryData:any;
  currencyResData:any
  total: any;
  params: any = {
    size: 10,
    page: 1,
    order: 'desc',
    sort_by: 'id',
    datetime: {},
    datetimeUpdatedAt: {},
    search:'',
    exchange_rate: '',
    time_period: {},
    time_period_updated_at: {},
    code:'',
  };
  currencies:any
  format: string = "YYYY-MM-DD HH:mm:ss";
  
  ngOnInit(): void {
    this.adminAuthService.adminUser.subscribe((user: any) => {
      if(user && user.id) {
        // this.params = {...this.params, time_zone: user.timezone}
        setTimeout(() => {
          const _zone = TIMEZONE.find(t => t.zonename == user.timezone);
          this.selectedTimeZone = _zone?.zonename
          $(`#time_zone`).val(_zone?.zonename).trigger('change');
        }, 100);
      }
    });
    this.getConversionHistory()
    this.getCurrencies()

  }
  getCurrencies() {
    this.currenciesService.getCurrencies().subscribe((res: any) => {
      this.currencies = res.record;
    });
  }
  ngAfterViewInit() :void {
    $('#time_period_created_at').val('');
  $('#time_period_updated_at').val('');
  }
  getConversionHistory() {
       this.isLoader = true
      if(this.params.time_period.start_date && this.params.time_period.end_date && this.params.time_zone_name && this.params.time_zone_name !== 'UTC +00:00'){
        this.params = {...this.params, datetime: {  start_date: this.params.time_period.start_date, end_date: this.params.time_period.end_date}}
      } else{
        this.params = {...this.params, datetime: {  start_date: this.params.time_period.start_date, end_date:this.params.time_period.end_date}}
      }
      if(this.params.time_period_updated_at.start_date && this.params.time_period_updated_at.end_date && this.params.time_zone_name && this.params.time_zone_name !== 'UTC +00:00'){
        this.params = {...this.params, datetimeUpdatedAt: {  start_date: this.params.time_period_updated_at.start_date, end_date: this.params.time_period_updated_at.end_date}}
      } else{
        this.params = {...this.params, datetimeUpdatedAt: {  start_date: this.params.time_period_updated_at.start_date, end_date:this.params.time_period_updated_at.end_date}}
      }
      this.currenciesService.conversionHistory(this.params).subscribe((res: any) => {
        this.conversionHistoryData = res.record.data.data;
        this.total = res.record.data.total;
        this.isLoader = false
     
        },
        (error:any) => {
          this.isLoader = false
  
        }
        );
    
    }
  filter(evt: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getConversionHistory();
  }

  filterSelectTimeZone(zonename: any) {
    this.p = 1;

    if(zonename) {
      const zone = TIMEZONE.find(t => t.zonename === zonename);
      this.zone = zone?.name;
      this.selectedTimeZone = zone?.zonename
      this.params = {...this.params, page: this.p, time_zone: zone?.value, time_zone_name: zone?.zonename};
    }else{
      this.zone='';
      this.params = {...this.params, page: this.p, time_zone: this.zone};
    }

    this.getConversionHistory();
}

selectDateRange(time_period: any) {
  this.p = 1;
  this.params = { ...this.params, time_period, page: this.p }
  this.getConversionHistory();
}
selectDateRangeUpdatedAt(time_period: any) {
  this.p = 1;
  this.params = { ...this.params, time_period, page: this.p }
  this.getConversionHistory();
}
resetFilter() {
  this.p = 1;
  $('#time_period_created_at').val('');
  $('#time_period_updated_at').val('');
  this.params = {      
    size: 10,
    page: 1,
    search: '',
    datetime:{},
    datetimeUpdatedAt:{},
    order: 'desc',
    sort_by: 'id',
    time_period: {},
    time_period_updated_at: {},
    code:'',
    exchange_rate: '',
  };

  this.getConversionHistory();
}
pageChanged(page: number) {
  this.params = { ...this.params, page };
  this.getConversionHistory();
}
setOrder(sort: any) {
  this.p = 1;
  this.params = { ...this.params, page: this.p, order: sort.order, sort_by: sort.column };
  this.getConversionHistory();
}

formatDateInSelectedTimeZone(date: string): string {
  const formattedDate = this.datePipe.transform(date, 'dd-MM-yyyy HH:mm:ss', this.selectedTimeZone);
  return formattedDate || '-'; // Return '-' if formatting fails
}
currencyResponse(id:any) {
  this.isLoader = true
  this.currenciesService.currencyResponse(id).subscribe((res: any) => {
    this.currencyResData = res.record;
    this.isLoader = false
    
    })
}

}
