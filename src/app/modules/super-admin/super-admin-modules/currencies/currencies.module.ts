import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './list/list.component';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { EditComponent } from './edit/edit.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PipeModule } from 'src/app/pipes/pipes.module';
import { ConversionHistoryComponent } from './conversion-history/conversion-history.component';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { NgxJsonViewerModule } from 'ngx-json-viewer';

const currenciesRoutes: Routes = [
  { path: 'conversion-history', component: ConversionHistoryComponent },
  { path: '', component: ListComponent },
  { path: ':id', component: EditComponent },
]

@NgModule({
  declarations: [ListComponent, EditComponent, ConversionHistoryComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(currenciesRoutes),
    ComponentsModule,
    PipeModule,
    DirectivesModule,
    SharedModule,
    NgxJsonViewerModule,

  ],
  providers: [
    DatePipe, 
  ],
})

export class CurrenciesModule { }
