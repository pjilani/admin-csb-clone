import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Currency } from 'src/app/models';

@Injectable({ providedIn: 'root' })

export class CurrenciesService {

    constructor(private http: HttpClient) { }

  // Admin ===================================
  
  getAdminCurrencies() {
    return this.http.get(`admin/tenants/currencies`);
  }
  
  // Super Admin ===================================

  getCurrencies() {
    return this.http.get(`super/admin/currencies`);
  }

  getCurrencyById(id: number) {
    return this.http.get(`super/admin/currencies/${id}`);
  }

  createCurrency(currency: Currency) {
    return this.http.post(`super/admin/currency`, currency);
  }

  updateCurrency(currency: Currency) {
    return this.http.post(`super/admin/currency/${currency.id}`, currency);
  }

  deleteCurrency(currency: Currency) {
    return this.http.delete(`super/admin/currency/${currency.id}`);
  }
  conversionHistory(data: any) {
    return this.http.post(`super/admin/list-conversion-history`, data);
  }
  currencyResponse(id: any) {
    return this.http.get(`super/admin/currency-response/${id}`);
  }
}
