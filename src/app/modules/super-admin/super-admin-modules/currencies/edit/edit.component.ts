import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Currency } from 'src/app/models';
import { CustomValidators } from 'src/app/shared/validators';
import { CurrenciesService } from '../currencies.service';

declare const $: any;
declare const toastr: any;

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  id: number = 0;
  currency!: Currency;
  
  currencyForm: FormGroup | any;
  submitted: boolean = false;
  currencyLoader: boolean = false;

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Currencies', path: '/super-admin/currencies' }
  ];

  constructor(private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private currenciesService: CurrenciesService) {
     this.id = this.route.snapshot.params['id'];
     
     let title: string = 'Create';

    this.currencyForm = this.formBuilder.group({
      name: ['', [ Validators.required ]],
      code: ['', [ Validators.required ]],
      internal_code: ['', [ Validators.required ]],
      exchange_rate: ['', [ Validators.required, Validators.min(0.000001) ]],
      // primary: [ true ]
    });

    if(this.id > 0) {
      title = 'Edit';
      this.getCurrency();
    }

  this.breadcrumbs.push({ title, path: `/super-admin/currencies/${this.id}` });

  }

  ngOnInit(): void {  }

  getCurrency() {
    this.currenciesService.getCurrencyById(this.id).subscribe((res: any) => {
      this.currency = res.record;
      this.currencyForm.patchValue({
        name: this.currency.name,
        code: this.currency.code,
        internal_code: this.currency.code,
        exchange_rate: this.currency.exchange_rate,
        primary: this.currency.primary
      });
    });
  }

  get f() {
    return this.currencyForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid

    // console.log(this.currencyForm.value);

    if (this.currencyForm.invalid) return;    

    this.currencyLoader = true;
 
    if(this.id > 0) {
      const data = this.currencyForm.value;
      data.id = this.id;

      this.currenciesService.updateCurrency(data)
      .subscribe((response: any) => {
        toastr.success(response?.message || 'Currency Updated Successfully!');
        this.router.navigate(['/super-admin/currencies']);
      }, (err: any) => {
        this.currencyLoader = false; 
      });

    } else {

      this.currenciesService.createCurrency(this.currencyForm.value)
      .subscribe((response: any) => {
        toastr.success(response?.message || 'Currency Created Successfully!');
        this.router.navigate(['/super-admin/currencies']);
      }, (err: any) => {
        this.currencyLoader = false; 
      });

    }
  }

}
