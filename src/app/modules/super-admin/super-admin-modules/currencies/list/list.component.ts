import { Component, OnInit } from '@angular/core';
import { Currency } from 'src/app/models';
import { CurrenciesService } from '../currencies.service';
declare const toastr: any;
import Swal from 'sweetalert2';

@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {

  p: number = 1;
  
  currencies: Currency[] = [];

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Currencies', path: '/super-admin/currencies' },
  ];

  constructor(private currencyService: CurrenciesService) { }

  ngOnInit(): void {
    this.getCurrencies();
  }

  getCurrencies() {
    this.currencyService.getCurrencies().subscribe((res: any) => {
      this.currencies = res.record;
    });
  }

  delete(currency: Currency) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to delete ${currency.name}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, delete it!`
    }).then((result) => {
      if (result.isConfirmed) {

        this.currencyService.deleteCurrency(currency).subscribe((res: any) => {
          toastr.success( res.message || 'Currency Deleted Successfully.');
          this.getCurrencies();
        });
        
      }
    });

  }

  

}
