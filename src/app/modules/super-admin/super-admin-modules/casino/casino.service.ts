import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })

export class CasinoService {

  prefix: string = 'super/admin/casino/';

  constructor(private http: HttpClient) { }

  // Providers
  createAdminProvider(data: any) {
    return this.http.post(this.prefix + `providers/record/add`, data);
  }

  getAdminProviders() {
    return this.http.get(this.prefix + `providers`);
  }

  updateAdminProvider(id: number, data: any) {
    return this.http.put(this.prefix + `providers/edit/${id}`, data);
  }

  deleteAdminProvider(id: number) {
    return this.http.delete(this.prefix + `providers/${id}`);
  }

// Games
  createAdminGame(data: any) {
    return this.http.post(this.prefix + `games/add`, data);
  }

  getAdminGames(params: any) {
    return this.http.post(this.prefix + `games`, params);
  }

  getAdminGamesByProvider(id: number) {
    return this.http.get(this.prefix + `games/list/all/${id}`);
  }

  updateAdminGame(id: number, data: any) {
    return this.http.put(this.prefix + `games/edit/${id}`, data);
  }

  deleteAdminGame(id: number) {
    return this.http.delete(this.prefix + `games/${id}`);
  }

  // Table
  createAdminTable(data: any) {
    return this.http.post(this.prefix + `tables/add`, data);
  }

  getAdminTables(params: any) {
    return this.http.post(this.prefix + `tables`, params);
  }

  updateAdminTable(id: number, data: any) {
    return this.http.put(this.prefix + `tables/edit/${id}`, data);
  }

  deleteAdminTable(id: number) {
    return this.http.delete(this.prefix + `tables/${id}`);
  }

}
