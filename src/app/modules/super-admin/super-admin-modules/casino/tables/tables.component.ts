import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Game, Table } from 'src/app/models';
import { CasinoService } from '../casino.service';
import { PageSizes } from 'src/app/shared/constants';
import Swal from 'sweetalert2';
declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.scss']
})

export class TablesComponent implements OnInit {
  
  pageSizes: any[] = PageSizes;

  providers: any[] = [];
  
  games: Game[] = [];
  
  tables: Table[] = [];
  table: any;

  pageForm: FormGroup | any;
  submitted: boolean = false;
  pageLoader: boolean = false;

  params: any = {
    size: 10,
    page: 1,
    search:'',
    provider_id: '',
    game_id: ''
  };

  p: number = 1;
  total: number = 0;

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Casino Tables', path: '/super-admin/casino/tables' }
  ];

  firstTimeApiCall = false;

  constructor(
    private formBuilder: FormBuilder,
    private casinoService: CasinoService) { 
      this.pageForm = this.formBuilder.group({
        name: ['', [ Validators.required ]],
        table_id: ['', [ Validators.required ]],
        provider_id: ['', [ Validators.required ]],
        game_id: ['', [ Validators.required ]],
        is_lobby: [false]
      });
    }
    
  ngOnInit(): void {
    // this.getTables();
    this.getProviders();
  }
  
  getProviders() {
    this.casinoService.getAdminProviders().subscribe((res: any) => {
      this.providers = res.record;
    });
  }

  get f() {
    return this.pageForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    // console.log(this.pageForm.value);
    if (this.pageForm.invalid) return;
    
    this.pageLoader = true;
    if(this.table && this.table.id > 0) {
    this.casinoService.updateAdminTable(this.table.id, this.pageForm.value)
      .subscribe((res: any) => {
        toastr.success(res.message || 'Table updated successfully.');

        const casPrd: any = this.providers.find(f => f.id == this.f.provider_id.value);
        const casgame: any = this.games.find(f => f.id == this.f.game_id.value);
      
          this.tables = this.tables.map((m: Table)=> {
            if(this.table && this.table.id === m.id) {
              m.name = this.f.name.value;
              m.game_name = casgame && casgame.game_name_id ? casgame.game_name_id : m.game_name;
              m.game_id = this.f.game_id.value;
              m.table_id = this.f.table_id.value;
              m.casino_providers_name = casPrd && casPrd.name ? casPrd.name : m.casino_providers_name;
              m.is_lobby = this.f.is_lobby.value;
            }
            return m;
          });

          this.resetPageForm();
          this.getTables();

      }, (err: any) => {
        this.pageLoader = false; 
      });

    } else {

      this.casinoService.createAdminTable(this.pageForm.value)
      .subscribe((res: any) => {
        toastr.success(res.message || 'Table created successfully.');

        this.resetPageForm();
        
        this.getTables();
        
      }, (err: any) => {
        this.pageLoader = false; 
      });
      
    }
  }

  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getTables();
  }

  resetPageForm() {
    $('#modal-table').modal('hide');
    this.pageForm.reset();
    this.pageLoader = false;
    this.submitted = false;
  }

  getTables() {
    this.firstTimeApiCall = true;
    this.casinoService.getAdminTables(this.params).subscribe((res: any) => {
      this.tables = res.record.data;
      this.total = res.record.count;
    });
  }

  deleteTable(table: Table) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to delete ${table.name}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, delete it!`
    }).then((result) => {
      if (result.isConfirmed) {

        this.casinoService.deleteAdminTable(table.id).subscribe((res: any) => {
          this.tables = this.tables.filter((f: Table) => f.id != table.id);
          toastr.success(res.message || 'Table deleted successfully.')
        });
        
      }
    });
    
  }

  createPageForm() {
    this.table = null;
    this.submitted = false;
    this.pageForm.patchValue({
      name: '',
      table_id: '',
      game_id: '',
      provider_id: '',
      is_lobby: true
    });
  }

  selectTable(table: Table) {
    this.table = table;
  }

  updateTable(table: Table) {
    this.submitted = false;
    
    const casPrd: any = this.providers.find(f => f.name == table.casino_providers_name);

    this.table = table;
    this.pageForm.patchValue({
      name: table.name,
      game_id: table.game_id,
      table_id: table.table_id,
      provider_id: casPrd ? casPrd.id : '',
      is_lobby: table.is_lobby,
    });

    if(casPrd?.id) {
      this.getGamesByPrid(casPrd.id);
    }
  }

  filter(evt: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getTables();
  }

  resetFilter() {
    this.p = 1;
    this.params = {
      size: 10,
      page: 1,
      search:'',
      provider_id: '',
      game_id: ''
    };
    this.getTables();
  }

  changeProvider(event: any) {
    const id = event.target.value;
    this.f.game_id.setValue('');
    this.getGamesByPrid(id);
  }

  selectProviderFilter(event: any) {
    const id = event.target.value;
    if (id) {
      this.getGamesByPrid(id);
    }
  }

  getGamesByPrid(id: number) {
    this.casinoService.getAdminGamesByProvider(id).subscribe((res: any) => {
      this.games = res.record;
    });
  }

}
