import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CasinoService } from '../casino.service';
import Swal from 'sweetalert2';
declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './provider.component.html',
  styleUrls: ['./provider.component.scss']
})

export class ProviderComponent implements OnInit {
  
  p: number = 1;
  providers: any[] = [];
  provider!: any;

  providerForm: FormGroup | any;
  submitted: boolean = false;
  providerLoader: boolean = false;

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Casino Provider', path: '/casino/provider' }
  ];

  constructor(
    private formBuilder: FormBuilder,
    private casinoService: CasinoService) { 
      this.providerForm = this.formBuilder.group({
        name: ['', [ Validators.required ]]
      });
    }
    
  ngOnInit(): void {
    this.getProviders();
  }
  
  get f() {
    return this.providerForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.providerForm.invalid) return;
    
    this.providerLoader = true;
    if(this.provider && this.provider.id > 0) {
      
    this.casinoService.updateAdminProvider(this.provider.id, this.providerForm.value)
      .subscribe((res: any) => {
        toastr.success(res.message || 'Provider updated successfully.');
        
          this.providers = this.providers.map((m: any)=> {
            if(this.provider && this.provider.id === m.id) {
              m.name = this.f.name.value;
            }
            return m;
          });

          this.resetPageForm();
          
      }, (err: any) => {
        this.providerLoader = false; 
      });

    } else {

      this.casinoService.createAdminProvider(this.providerForm.value)
      .subscribe((res: any) => {
        toastr.success(res.message || 'Provider created successfully.');
        
        this.resetPageForm();
        
        if(res?.record?.id && res?.record?.name) {

            this.providers.unshift(res.record);

        } else {
          this.getProviders();
        }
        
      }, (err: any) => {
        this.providerLoader = false; 
      });
      
    }
  }

  resetPageForm() {
    $('#modal-provider').modal('hide');
    this.providerForm.reset();
    this.providerLoader = false;
    this.submitted = false;
  }

  getProviders() {
    this.casinoService.getAdminProviders().subscribe((res: any) => {
      this.providers = res.record;
    });
  }

  deleteProvider(provider: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to delete ${provider.name}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, delete it!`
    }).then((result) => {
      if (result.isConfirmed) {

        this.casinoService.deleteAdminProvider(provider.id).subscribe((res: any) => {
          this.providers = this.providers.filter((f: any) => f.id != provider.id);
          toastr.success(res.message || 'Provider deleted successfully.')
        });
        
      }
    });
  }

  selectProvider(provider: any) {
    this.provider = provider;
  }

  createProviderForm() {
    this.provider = null;
    this.submitted = false;
    this.f.name.setValue('');
  }

  updateProvider(provider: any) {
    this.submitted = false;
    this.provider = provider;
    this.f.name.setValue(provider.name);
  }

}
