import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { ProviderComponent } from './provider/provider.component';
import { GamesComponent } from './games/games.component';
import { TablesComponent } from './tables/tables.component';

const casinoRoutes: Routes = [
  { path: '', redirectTo: 'providers', pathMatch: 'full' },
  { path: 'providers', component: ProviderComponent },
  { path: 'games', component: GamesComponent },
  { path: 'tables', component: TablesComponent },
]

@NgModule({
  declarations: [
    TablesComponent,
    GamesComponent,
    ProviderComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(casinoRoutes),
    ComponentsModule,
    DirectivesModule,
    SharedModule
  ]
})

export class CasinoModule { }
