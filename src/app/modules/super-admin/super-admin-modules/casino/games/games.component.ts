import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Game } from 'src/app/models';
import { CasinoService } from '../casino.service';
import { PageSizes } from 'src/app/shared/constants';
import Swal from 'sweetalert2';
declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.scss']
})

export class GamesComponent implements OnInit {
  
  pageSizes: any[] = PageSizes;

  providers: any[] = [];
  
  games: Game[] = [];
  game: any;

  gameForm: FormGroup | any;
  submitted: boolean = false;
  gameLoader: boolean = false;

  params: any = {
    size: 10,
    page: 1,
    search:'',
    provider_id: ''
  };

  p: number = 1;
  total: number = 0;
  firstTimeApiCall = false;

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Casino Games', path: '/super-admin/casino/games' }
  ];

  constructor(
    private formBuilder: FormBuilder,
    private casinoService: CasinoService) { 
      this.gameForm = this.formBuilder.group({
        name: ['', [ Validators.required ]],
        game_id: ['', [ Validators.required ]],
        casino_provider_id: ['', [ Validators.required ]]
      });
    }
    
  ngOnInit(): void {
    // this.getGames();
    this.getProviders();
  }

  getProviders() {
    this.casinoService.getAdminProviders().subscribe((res: any) => {
      this.providers = res.record;
    });
  }
  
  get f() {
    return this.gameForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.gameForm.invalid) return;
    
    this.gameLoader = true;
    if(this.game && this.game.id > 0) {
    this.casinoService.updateAdminGame(this.game.id, this.gameForm.value)
      .subscribe((res: any) => {
        toastr.success(res.message || 'Game updated successfully.');

        const casPrd: any = this.providers.find((f: any) => f.id == this.f.casino_provider_id.value);
      
          this.games = this.games.map((m: Game)=> {
            if(this.game && this.game.id === m.id) {
              m.name = this.f.name.value;
              m.game_id = this.f.game_id.value;
              m.casino_providers_name = casPrd && casPrd.name ? casPrd.name : m.casino_providers_name;
            }
            return m;
          });
          
          this.resetPageForm();

      }, (err: any) => {
        this.gameLoader = false; 
      });

    } else {

      this.casinoService.createAdminGame(this.gameForm.value)
      .subscribe((res: any) => {
        toastr.success(res.message || 'Game created successfully.');
        
        const casPrd: any = this.providers.find((f: any) => f.id == this.f.casino_provider_id.value);

        if(res?.record?.id && res?.record?.name && res?.record?.game_id && res?.record?.casino_provider_id ) {
          this.games.unshift({ ...res.record, casino_providers_name: casPrd.name });
        } else {
          this.getGames();
        }

        this.resetPageForm();
        
      }, (err: any) => {
        this.gameLoader = false; 
      });
      
    }
  }

  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getGames();
  }

  resetPageForm() {
    $('#modal-game').modal('hide');
    this.gameForm.reset();
    this.gameLoader = false;
    this.submitted = false;
  }

  getGames() {
    this.firstTimeApiCall = true;
    this.casinoService.getAdminGames(this.params).subscribe((res: any) => {
      this.games = res.record.data;
      this.total = res.record.count;
    });
  }

  deleteGame(game: Game) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to delete ${game.name}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, delete it!`
    }).then((result) => {
      if (result.isConfirmed) {

        this.casinoService.deleteAdminGame(game.id).subscribe((res: any) => {
          this.games = this.games.filter((f: Game) => f.id != game.id);
          toastr.success(res.message || 'Game deleted successfully.')
        });
        
      }
    });
    
  }

  selectGame(game: Game) {
    this.game = game;
  }

  createGameForm() {
    this.game = null;
    this.submitted = false;
    this.gameForm.patchValue({
      name: '',
      game_id: '',
      casino_provider_id: ''
    });
  }

  updateGame(game: Game) {
    this.submitted = false;
    this.game = game;
    this.gameForm.patchValue({
      name: game.name,
      game_id: game.game_id,
      casino_provider_id: game.casino_provider_id,
    });
  }

  filter(evt: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getGames();
  }

  resetFilter() {
    this.p = 1;
    this.params = {
      size: 10,
      page: 1,
      search:'',
      provider_id: ''
    };
    this.getGames();
  }
  
}
