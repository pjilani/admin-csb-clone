import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './list/list.component';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { EditComponent } from './edit/edit.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PipeModule } from 'src/app/pipes/pipes.module';

const menuRoutes: Routes = [
  { path: '', component: ListComponent },
  { path: ':id', component: EditComponent }
]

@NgModule({
  declarations: [ListComponent, EditComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(menuRoutes),
    ComponentsModule,
    PipeModule,
    SharedModule
  ]
})

export class MenuModule { }
