import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MenuMaster } from 'src/app/models';

@Injectable({ providedIn: 'root' })

export class MenuService {

    constructor(private http: HttpClient) { }

  // Admin ===================================

  getAdminMenus() {
    return this.http.get(`admin/tenants/currencies`);
  }
  
  // Super Admin ===================================

  getMenu() {
    return this.http.get(`super/admin/menu`);
  }

  getMenuById(id: number) {
    return this.http.get(`super/admin/menu/${id}`);
  }

  createMenu(menumaster: MenuMaster) {
    return this.http.post(`super/admin/menu/add`, menumaster);
  }

  updateMenu(menumaster: MenuMaster) {
    return this.http.post(`super/admin/menu/edit/${menumaster.id}`, menumaster);
  }

  deleteMenu(menumaster: MenuMaster) {
    return this.http.delete(`super/admin/menu/${menumaster.id}`);
  }

  updateMenuIcon(icon: any){
    return this.http.post(`super/admin/menu/update-icon`,icon);
  }

}
