import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuMaster } from 'src/app/models';
import { CustomValidators } from 'src/app/shared/validators';
import { MenuService } from '../menu.service';

declare const $: any;
declare const toastr: any;

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  id: number = 0;
  MenuMaster!: MenuMaster;
  
  menuForm: FormGroup | any;
  submitted: boolean = false;
  menuLoader: boolean = false;

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Menu Master', path: '/super-admin/menu' }
  ];

  constructor(private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private menuService: MenuService) {
     this.id = this.route.snapshot.params['id'];
     
     let title: string = 'Create';

    this.menuForm = this.formBuilder.group({
      name: ['', [ Validators.required ]],
      path: ['', [ Validators.required ]],
      active: [ true ]
    });

    if(this.id > 0) {
      title = 'Edit';
       this.getMenuMaster();
    }

  this.breadcrumbs.push({ title, path: `/super-admin/menu/${this.id}` });

  }

  ngOnInit(): void {  }

  getMenuMaster() {
    this.menuService.getMenuById(this.id).subscribe((res: any) => {
      this.MenuMaster = res.record;
      this.menuForm.patchValue({
        name: this.MenuMaster.name,
        path: this.MenuMaster.path,
        active: this.MenuMaster.active
      });
    });
  }

  get f() {
    return this.menuForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid

    if (this.menuForm.invalid) return;

    this.menuLoader = true;
 
    if(this.id > 0) {
      const data = this.menuForm.value;
      data.id = this.id;

      this.menuService.updateMenu(data)
      .subscribe((response: any) => {
        toastr.success(response?.message || 'Updated Successfully!');
        this.router.navigate(['/super-admin/menu']);
      }, (err: any) => {
        this.menuLoader = false;
      });

    } else {

      this.menuService.createMenu(this.menuForm.value)
      .subscribe((response: any) => {
        toastr.success(response?.message || 'Created Successfully!');
        this.router.navigate(['/super-admin/menu']);
      }, (err: any) => {
        this.menuLoader = false;
      });

    }
  }

}
