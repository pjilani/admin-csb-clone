import {Component, OnInit, ElementRef, ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {MenuService} from '../menu.service';
import Swal from 'sweetalert2';
import {MenuMaster} from "../../../../../models";
import { CustomValidators } from 'src/app/shared/validators';
declare const toastr: any;
declare const $: any;
@Component({
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {
    @ViewChild('file') logo_file!: ElementRef;
    p: number = 1;
    menuId: any;
    menuName:any;
    menu: MenuMaster[] = [];
    imageForm: FormGroup | any;
    submitted: boolean = false;
    menuLoader: boolean = false;
    imgURL: any;
    iconForm: FormGroup | any;
    selectLogo!: File;
    breadcrumbs: Array<any> = [
        {title: 'Home', path: '/super-admin'},
        {title: 'Menu', path: '/super-admin/menu'},
    ];

    constructor(private formBuilder: FormBuilder,private menuService: MenuService) {
        this.imageForm = this.formBuilder.group({
            image: ['', [ Validators.required ]]
          });
    }

    ngOnInit(): void {
        this.getMenus();
    }
    get f() {
        return this.imageForm.controls;
      }

    getMenus() {
        this.menuService.getMenu().subscribe((res: any) => {
            this.menu = res.record;
        });
    }

    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.imageForm.invalid) return;
        
        this.menuLoader = true;

    
        const data = this.imageForm.value;
        const fd = new FormData();
        fd.append('id', `${this.menuId}`);
        fd.append('name', `${this.menuName}`);
        if (this.selectLogo && this.selectLogo.name) {
          fd.append('image', this.selectLogo, this.selectLogo.name);
        }
    
        for (let key in data) {
          fd.append(key, data[key]);
        }
          this.menuService.updateMenuIcon(fd)
            .subscribe((response: any) => {
              if (response?.message) {
                toastr.success(response?.message || 'Image updated successfully.');
                $("#modal-provider").modal("hide");
                this.getMenus();
                // toastr.success(response?.message || 'Tenant Banner Updated Successfully!');
              }
              this.menuLoader = false;
            }, (err: any) => {
              this.menuLoader = false;
            });
      }

      selectImage(files: any) {
        this.imgURL = '';
        if (files.length === 0)
          return;
    
        if ((files[0].size / 1000000) > 5) {
    
          this.f.image.setErrors({ 'required': 'File is required' });
          toastr.error( 'Max file size limit reached');
          // toastr.error(`Max file size is 5MB`);
          return;
        }
    
        const extension = files[0].name.split('.')[1].toLowerCase();
        let check = CustomValidators.imgType.includes(extension);

        if (check) {
    
          const reader = new FileReader();
          reader.readAsDataURL(files[0]);
          const img = new Image();
          img.src = window.URL.createObjectURL( files[0] );
          reader.onload = (_event) => {
          //   const width = img.naturalWidth;
          //    const height = img.naturalHeight;
          //    console.log(width, height);
          //    if( width > 30 || height > 30 ) {
          //       toastr.error("Icon should be less then 30 x 30 size");
          //     this.f.logo.setErrors({ 'dimension': 'Icon should be less then 30 x 30 size' });
          //     this.imgURL = '';
          //  }
    
            this.imgURL = reader.result;
          }
          this.selectLogo = files[0];
        } else {
          this.imgURL = '';
          toastr.error( 'Image extension should be svg or png');
          // toastr.error(`Please select ${this.f.banner_type.value}`);
        }
    
      }

    delete(menu: MenuMaster) {
        Swal.fire({
            title: 'Are you sure?',
            text: `You want to delete ${menu.name}!`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: `Yes, delete it!`
        }).then((result) => {
            if (result.isConfirmed) {

                this.menuService.deleteMenu(menu).subscribe((res: any) => {
                    toastr.success(res.message || 'Currency Deleted Successfully.');
                    this.getMenus();
                });

            }
        });

    }

    setMenuId(menu:any){
        this.menuId = menu.id;
        this.menuName = menu.name;
        this.imgURL = menu.image;
    }
}
