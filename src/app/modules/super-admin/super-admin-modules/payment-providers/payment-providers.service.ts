import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class PaymentProvidersService {

  constructor(private http: HttpClient) { }


  getPaymentProviders(params: any) {
    return this.http.post(`super/admin/paymentProviders/lists`, params);
  }

  activeProviderStatus(data:any){
    return this.http.post(`super/admin/paymentProviders/update/status`, data);
  }
  
  
  deactiveProviderStatus(data:any){
    return this.http.post(`super/admin/paymentProviders/update/status`, data);
  }


  createPaymentProvider(admin: any) {
    
    return this.http.post(`super/admin/paymentProviders`, admin);
  }

  updatePaymentProvider(id: number, admin: any) {
    return this.http.post(`super/admin/paymentProviders/${id}`, admin);
  }
}
