import { TestBed } from '@angular/core/testing';

import { PaymentProvidersService } from './payment-providers.service';

describe('PaymentProvidersService', () => {
  let service: PaymentProvidersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PaymentProvidersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
