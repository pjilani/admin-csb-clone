import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { EditComponent } from './edit/edit.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { PipeModule } from 'src/app/pipes/pipes.module';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';

const paymentProvidersRoutes: Routes = [
  { path: '', component: ListComponent },
  { path: ':id', component: EditComponent }
]

@NgModule({
  declarations: [
    ListComponent,
    EditComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(paymentProvidersRoutes),
    SharedModule,
    ComponentsModule,
    PipeModule,
    DirectivesModule,
  ]
})
export class PaymentProvidersModule { }
