import { Component, OnInit } from '@angular/core';
import { PageSizes } from 'src/app/shared/constants';
import { PaymentProvidersService } from '../payment-providers.service';
import { SuperAdminAuthService } from '../../../../super-admin/services/super-admin-auth.service';
import Swal from 'sweetalert2';
declare const toastr: any;
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  constructor(private PaymentProvidersService: PaymentProvidersService,public superAdminAuthService: SuperAdminAuthService) { }

  
  ngOnInit(): void {
    // this.getPaymentProviders();
  }

  pageSizes: any[] = PageSizes;
  params: any = {
    size: 10,
    page: 1,
    search:''
  };

  p: any = 1;
  total: number = 0;
  paymentProviders: any[] = [];
  firstTimeApiCall = false;

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Payment Providers', path: '/super-admin/payment-providers' },
  ];

  pageChanged(page: any) {
    this.params = { ...this.params, page };
    this.getPaymentProviders();
  }

  getPaymentProviders() {
    this.firstTimeApiCall = true;
    this.PaymentProvidersService.getPaymentProviders(this.params).subscribe((res: any) => {
      this.paymentProviders = res.record.data;
      if(this.paymentProviders){
        for (let i = 0; i < this.paymentProviders.length; i++) {
          this.paymentProviders[i]['keys'] = JSON.parse(this.paymentProviders[i]['provider_keys']);
          
        }
      }
      this.total = res.record.count;
      
    });
  }

  filter(event: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getPaymentProviders();
  }

  resetFilter() {
    this.p = 1;
    this.params = { 
      ...this.params, 
      size: 10,
      page: 1,
      search: ''
    }
    this.getPaymentProviders();
  }



  activeProviderStatus(data: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to Active ${data.provider_name}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Active it!'
    }).then((result) => {
      if (result.isConfirmed) {

        let providerData = {
          "id" : data.id,
          "status": true
        };

        this.PaymentProvidersService.activeProviderStatus(providerData).subscribe((res: any) => {
          toastr.success(res.message || 'Payment Provider Activate successfully' );
          this.paymentProviders = this.paymentProviders.map((f: any) => {
            if(f.id == data.id) {
              f.active =  true;
            }
            return f;
          });
        });
        
      }
    });

  }

  deactiveProviderStatus(data: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to Deactive ${data.provider_name}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Deactivate it!'
    }).then((result) => {
      if (result.isConfirmed) {
        let providerData = {
          "id" : data.id,
          "status": false
        };
        this.PaymentProvidersService.deactiveProviderStatus(providerData).subscribe((res: any) => {
          toastr.success(res.message || 'Payment Provider Deactivated successfully' );
          this.paymentProviders = this.paymentProviders.map((f: any) => {
            if(f.id == data.id) {
              f.active = false;
            }
            return f;
          });
        });
        
      }
    });
    
  }

}
