
import {  Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PaymentProvidersService } from '../payment-providers.service';
import { CustomValidators } from 'src/app/shared/validators';
import { Currency } from 'src/app/models';
import { CurrenciesService } from '../../currencies/currencies.service';
declare const $: any;
declare const toastr: any;

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  id: number = 0;
  paymentProviders: any[] = [];
  providers!: any;
  providerForm: FormGroup | any;
  submitted: boolean = false;
  adminLoader: boolean = false;
  title: any = '';
  selectLogo!: File;
  imgURL: any;
  currencies: Currency[] = [];

  providerType = [
    { value: 'deposit', name: "Deposit" },
    { value: 'withdrawal', name: "Withdrawal" }
  ]

  constructor(private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private paymentProvidersService: PaymentProvidersService,
    private currenciesService: CurrenciesService,
  ) {

    this.id = this.route.snapshot.params['id'];
    this.providerForm = this.formBuilder.group({
      provider_name: ['', [Validators.required]], 
      provider_type: ['deposit', [Validators.required]], 
      phaseExecutions: this.formBuilder.group({
        PRE: this.formBuilder.array([])
      }),
      status: [true],
      logo: ['', [ CustomValidators.requiredFileType(CustomValidators.imgType)] ],
      currencies: [null,[Validators.required]]
    });

    this.title = 'Create';
    let title = 'Create';
    if (this.id > 0) {
      title = this.title = "Edit";
    }

    this.breadcrumbs.push({ title, path: `/super-admin/payment-providers/${this.id}` });

  }


  get provider_keys() {
    const control = <FormArray>(<FormGroup>this.providerForm.get('phaseExecutions')).get('PRE');
    return control;
  }

  get phaseExecutions(): FormArray {
    return this.providerForm.get('phaseExecutions').get('PRE') as FormArray;
  }

  removeSpace(event:any){
    var k;  
    k = event.charCode;
    return(((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 45 || k== 95) && event.keyCode != 32 ? event:event.preventDefault());

  }


  getProviderFormArr(index: any): FormGroup {
    const formGroup = this.phaseExecutions.controls[index] as FormGroup;
    return formGroup;
  }


  newProviderKeys(val: any = ''): any {
    return this.formBuilder.group({
      provider_keys: [(val != '' ? val : ''), [Validators.required]],
    })
  }

  addNewKey() {
    this.provider_keys.push(this.newProviderKeys());
  }

  removeProvider(i: number) {
    this.provider_keys.removeAt(i);
  }


  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Payment Providers', path: '/super-admin/payment-providers' }
  ];

  ngOnInit(): void { }

  ngAfterViewInit(): void {

    this.currenciesService.getCurrencies().subscribe((res: any) => {
      this.currencies = res.record;
    });


    if (this.id > 0) {

      this.paymentProvidersService.getPaymentProviders({ "id": this.id }).subscribe((res: any) => {

        this.providers = res.record.data[0];
        this.providers['provider_keys'] = JSON.parse(this.providers['provider_keys']);

        // console.log(this.providers['provider_keys']);


        for (let x in this.providers['provider_keys']) {
          this.provider_keys.push(this.newProviderKeys(this.providers['provider_keys'][x]));
        }


        this.providerForm.patchValue({
          provider_name: (this.providers.provider_name),
          provider_type: (this.providers.provider_type),
          status: (this.providers.active),
          currencies: (this.providers.currencies) ? JSON.parse(this.providers.currencies) : null
        });

        this.imgURL = res.record.data[0].logo;

        setTimeout(() => {
          var currencies = (this.providerForm.get('currencies').value)? this.providerForm.get('currencies').value.map(Number):null;
          $("#currencies").val(currencies).trigger('change');
        }, 100);

      });

    }else{
      const logoControl = this.providerForm.get('logo');
      logoControl.setValidators([Validators.required, CustomValidators.requiredFileType(CustomValidators.imgType)]);
      logoControl.updateValueAndValidity();
    }
  }


  get f() {
    return this.providerForm.controls;
  }

  selectImage(files: any) {
    if (files.length === 0)
    return;
    const extension = files[0].name.split('.')[1].toLowerCase();
    const imgt = CustomValidators.imgType.includes(extension);

    if(imgt) {
      // this.selectLogo = files[0];
        const reader = new FileReader();
        // this.imagePath = files;
        reader.readAsDataURL(files[0]); 
        reader.onload = (_event) => { 
          this.imgURL = reader.result; 
        }
        this.selectLogo = files[0];
      } else {
        this.imgURL = '';
      }

  }


  onSubmit() {
 

    this.submitted = true;

    if (this.providerForm.invalid) {
      return;
    }

    this.adminLoader = true;

    const data = this.providerForm.value;
    const fd = new FormData();
    if (!data.phaseExecutions.PRE.length) {
      toastr.error('Please Provide Keys for setup payment gateway!');
      this.adminLoader = false;
      return
    }

    delete data.logo;
    if(this.selectLogo && this.selectLogo.name) {
          fd.append('logo', this.selectLogo, this.selectLogo.name);
        }
        
    fd.append('provider_name', data.provider_name);
    fd.append('provider_type', data.provider_type);
    fd.append('phaseExecutions', JSON.stringify(data.phaseExecutions));
    fd.append('status', data.status);
    fd.append('currencies', JSON.stringify(data.currencies));
    if(this.id > 0) {

      this.paymentProvidersService.updatePaymentProvider(this.id, fd)
      .subscribe((response: any) => {
        toastr.success(response?.message || 'Payment Provider Updated Successfully!');
        
          this.router.navigate(['/super-admin/payment-providers/'+this.id]);
          this.adminLoader = false; 
      }, (err: any) => {
        this.adminLoader = false; 
      });


    } else {
     
      this.paymentProvidersService.createPaymentProvider(fd)
      .subscribe((response: any) => {
        toastr.success(response?.message || 'New Payment Provider Created Successfully!');
        this.router.navigate(['/super-admin/payment-providers']);
        this.adminLoader = false; 
      }, (err: any) => {
        this.adminLoader = false; 
      });

    }
  }

  selectCurrencies(currencies: any) {
    this.f.currencies.setValue(currencies);
  }

}
