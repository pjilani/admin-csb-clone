import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Currency, Language, Layout, Tenant, Theme } from 'src/app/models';
import { Constants } from 'src/app/shared/constants';
import { CustomValidators } from 'src/app/shared/validators';
import { CurrenciesService } from '../../currencies/currencies.service';
import { ThemesService } from '../../themes/themes.service';
import { TenantService } from 'src/app/modules/tenant/tenant.service';

declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})

export class EditComponent implements OnInit, AfterViewInit {
  @ViewChild('file') logo_file!: ElementRef;
  @ViewChild('fabFile') logo_fab_icon_file!: ElementRef;
  @ViewChild('signupPopupFile') logo_signup_popup_file!: ElementRef;

  signUpPopURL: any;
  imgURL: any;
  fabImgURL: any;
  id: number = 0;
  currencies: Currency[] = [];
  languages: Language[] = [];
  layouts: Layout[] = [];
  allGameProviders: any = [];
  themes: Theme[] = [];
  constTheme: any = { ...Constants.INIT_THEME };
  tenant!: Tenant;
  tenantRecords: any;
  selectSignupImg!: File;
  paymentProviders: any;

  tenantForm: FormGroup | any;
  submitted: boolean = false;
  tenantLoader: boolean = false;
  selectLogo!: File;
  selectFabIcon!: File;
  assignValue: any = [];
  assignPaymentValue: any = [];
  assignValueArray: any = [];
  assignPaymentArray: any = [];
  assignModulesValue: any = [];
  allowedModulesValues: any = ['subAgent', 'selfFunding', 'requiredKycForGame', 'requiredKycForWithdrawal'];
  theme: any = { ...Constants.INIT_THEME };
  selectedLayout: any = Constants.defaultLayout;
  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Tenants', path: '/super-admin/tenants' }
  ];

  constructor(private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private tenantService: TenantService,
    private themesService: ThemesService,
    private currenciesService: CurrenciesService,
  ) {

    this.id = this.route.snapshot.params['id'];
    //  let loginBaseTypeDisable = false;
    //  if(this.id >0){
    //   loginBaseTypeDisable = true;
    //  }

    this.tenantForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      whatsappNumber: ['', [Validators.min(1000000000), Validators.max(999999999999)]],
      poweredBy: [''],
      domain: ['', [Validators.required]],
      currencies: ['', [Validators.required]],
      language: ['', [Validators.required]],
      layout: ['', [Validators.required]],
      theme: ['', [Validators.required]],
      //loginMethod: [{value : '',disabled:loginBaseTypeDisable}, [ Validators.required ]],
      loginMethod: [{ value: '' }, [Validators.required]],
      smsGateway: ['', [Validators.required]],
        forgotPasswordOption: ['', [ Validators.required ]],
        assignedProvider: new FormArray([], Validators.required),
      assignedPaymentProvider: new FormArray([]),
      allowedModules: new FormArray([]),
      logo: ['', [CustomValidators.requiredFileType(CustomValidators.imgType)]],
      fabLogo: ['', [CustomValidators.requiredFileType(CustomValidators.imgType)]],
      signupPopup: ['', [CustomValidators.requiredFileType(CustomValidators.imgType)]],
      status: [true],
    });

    let title: string = 'Create';

    if (this.id > 0) {
      title = "Edit";
      
      const currenciesControl = this.tenantForm.get('currencies');
      currenciesControl.setValidators([]);
      currenciesControl.updateValueAndValidity();

    }

  this.breadcrumbs.push({ title, path: `/super-admin/tenants/${this.id}` });

  }

  onCheckChange(event: any, fieldName: string) {
    const formArray: FormArray = this.tenantForm.get(fieldName) as FormArray;
    /* Selected */
    if (event.target.checked) {
      // Add a new control in the arrayForm
      formArray.push(new FormControl(event.target.value));
    }
    /* unselected */
    else {
      // find the unselected element
      let i: number = 0;
      formArray.controls.forEach((ctrl: AbstractControl) => {
        if (ctrl.value == event.target.value) {
          // Remove the unselected element from the arrayForm
          formArray.removeAt(i);

          // condition to remove ezugiGamesLiveUpdates if both Evolution and Ezugi changes
          if (fieldName == 'assignedProvider' && (this.getIdFromName('Evolution') == event.target.value || this.getIdFromName('Ezugi') == event.target.value)) {
            if (!this.doesFormArrayContainControl(fieldName, 'Evolution') && !this.doesFormArrayContainControl(fieldName, 'Ezugi')) {
              const element = {
                target: {
                  checked: false,
                  value: 'ezugiGamesLiveUpdates'
                }
              };

              this.onCheckChange(element, 'allowedModules');
              this.assignModulesValue = this.assignModulesValue.split(',').filter((value: any) => value != 'ezugiGamesLiveUpdates').join(',')
            }
          }
          return;
        }
        i++;
      });
    }
  }

  doesFormArrayContainControl(formArrayName: string, controlName: string): boolean {
    const foundControl = this.tenantForm.get(formArrayName).controls.find((control: FormControl) => this.getIdFromName(controlName) == control.value);
    return !!foundControl;
  }

  getIdFromName(name: string) {
    const foundItem = this.allGameProviders.find((item: any) => item.name.toLowerCase() === name.toLowerCase());
    return foundItem ? foundItem.id : null;
  }




  ngOnInit(): void {
    this.getPaymentProviders();

    this.tenantService.getLanguages().subscribe((res: any) => {
      this.languages = res.record;
      if (this.id > 0 && this.tenantRecords) {
        $("#language").val(this.tenantRecords.setting.language_id).trigger('change');
      } else {
        this.f.language.setValue(this.languages[0].id);
      }
    });

    this.tenantService.getLayouts().subscribe((res: any) => {
      this.layouts = res.record;
      if (this.id > 0 && this.tenantRecords) {
        $("#layout").val(this.tenantRecords.setting.layout_id).trigger('change');
      } else {
        this.f.layout.setValue(this.layouts[0].id);
      }
    });


    this.tenantService.getAssignedProviders().subscribe((res: any) => {
      this.allGameProviders = res.record;

      if (this.id > 0 && this.tenantRecords) {
        // $("#layout").val(this.tenantRecords.setting.layout_id).trigger('change');
      } else {
        // this.f.assignedProvider.setValue(1);
        $("#checkboxPrimary0").attr("checked", true);
      }
    });

    this.themesService.getThemes().subscribe((res: any) => {
      this.themes = res.record;

      if (this.id > 0 && this.theme) {
        $("#theme").val(this.theme.name).trigger('change');
      } else {
        this.selectTheme(this.themes[0].name);
        setTimeout(() => {
          $("#theme").val(this.themes[0].name).trigger('change');
        }, 100);
      }

    });

    this.currenciesService.getCurrencies().subscribe((res: any) => {
      this.currencies = res.record;
      if (this.id > 0 && this.tenantRecords) {
        // $("#currencies").val(this.tenantRecords.configurations.map((m: Currency) => m.id)).trigger('change');

        for (const w of this.tenantRecords.configurations.map((m: Currency) => m.id)) {
          $(`#currencies>option[value=${w}]`).attr('disabled', 'disabled');
        }

      }
    });

  }

  ngAfterViewInit(): void {

    $("#currency").change(function (this: any) {
      $.each(this.options, function (i: any, item: any) {
        if (item.selected) {
          $(item).prop("disabled", true);
        } else {
          $(item).prop("disabled", false);
        }
      });
    });

    if (this.id > 0) {

      this.tenantService.getTenant(this.id).subscribe((res: any) => {

        this.tenant = res.record.tenants;
        this.tenantRecords = res.record;
        const theme = JSON.parse(res.record.setting.theme);
        const currencies = res.record.configurations.map((m: Currency) => m.id);
        const language = res.record.setting.language_id;
        const layout = res.record.setting.layout_id;
        const loginMethod = res.record.setting.user_login_type;
        const whatsappNumber = res.record.setting.whatsapp_number;
        const poweredBy = res.record.setting.powered_by;
        const smsGateway = res.record.setting.sms_gateway;
        const forgotPasswordOption = res.record.setting.forgot_password_option;
        const assignedProviderValues = (res.record.setting.assigned_providers)?.split(',');
        const allowedModules = (res.record.setting.allowed_modules)?.split(',');
        const assignPaymentArray = (res.record.setting.payment_providers)?.split(',');
        this.assignValue = res.record.setting.assigned_providers;
        this.assignValueArray = this.assignValue?.split(',').map(Number);
        this.assignPaymentValue = res.record.setting.payment_providers;
        this.assignPaymentArray = this.assignPaymentValue?.split(',').map(Number);
        this.assignModulesValue = res.record.setting.allowed_modules;

        this.tenantForm.patchValue({
          name: (this.tenant.name ? this.tenant.name : ''),
          whatsappNumber: whatsappNumber,
          poweredBy: poweredBy,
          domain: this.tenant.domain ? this.tenant.domain : '',
          currencies: currencies,
          language: language,
          layout: layout,
          loginMethod: (loginMethod ? loginMethod?.toString() : 0),
          smsGateway: (smsGateway ? smsGateway?.toString() : ''),
          forgotPasswordOption: (forgotPasswordOption ? forgotPasswordOption?.toString() : ''),
          theme: theme ? theme.name :'' ,
          status: this.tenant.active
        });

        // Allowed Casino Provider Setting
        for (let index = 0; index < assignedProviderValues?.length; index++) {
          const element = {
            target: {
              checked: true,
              value: assignedProviderValues[index]
            }
          };
          this.onCheckChange(element, 'assignedProvider');

        }
        // Allowed Module Setting
        for (let index = 0; index < allowedModules?.length; index++) {
          const element = {
            target: {
              checked: true,
              value: allowedModules[index]
            }
          };
          this.onCheckChange(element, 'allowedModules');

        }

        // Allowed payment Setting
        for (let index = 0; index < assignPaymentArray?.length; index++) {
          const element = {
            target: {
              checked: true,
              value: assignPaymentArray[index]
            }
          };
          this.onCheckChange(element, 'assignedPaymentProvider');

        }
        this.imgURL = res.record.setting.logo_url;
        this.fabImgURL = res.record.setting.fab_icon_url;
        this.signUpPopURL = res.record.setting.signup_popup_image_url;

        this.theme = theme;
        this.constTheme = theme;


        setTimeout(() => {

          $("#theme").val(this.theme.name).trigger('change');
          $("#layout").val(layout).trigger('change');
          $("#language").val(language).trigger('change');
          // $("#currencies").val(currencies).trigger('change');

          for (const w of currencies) {
            $(`#currencies>option[value=${w}]`).attr('disabled', 'disabled');
          }

        }, 100);

      });

    } else {
      const logoControl = this.tenantForm.get('logo');
      logoControl.setValidators([Validators.required, CustomValidators.requiredFileType(CustomValidators.imgType)]);
      logoControl.updateValueAndValidity();

      const fablogoControl = this.tenantForm.get('fabLogo');
      fablogoControl.setValidators([Validators.required, CustomValidators.requiredFileType(CustomValidators.imgType)]);
      fablogoControl.updateValueAndValidity();

      const popupImgControl = this.tenantForm.get('signupPopup');
      popupImgControl.setValidators([Validators.required, CustomValidators.requiredFileType(CustomValidators.imgType)]);
      popupImgControl.updateValueAndValidity();
    }
  }

  selectCurrencies(currencies: any) {
    this.f.currencies.setValue(currencies);
  }

  selectLanguage(language: any) {
    this.f.language.setValue(language);
  }

  selectLayout(layout: any) {
    this.selectedLayout = layout;
    this.f.layout.setValue(layout);
  }

  selectTheme(theme: any) {
    let findTheme: any = {};
    if (theme != 'Custom' || this.id > 0) {
      const data: any = this.themes.find(f => f.name === theme);
      if (!data) {
        findTheme = (this.constTheme.name == 'Custom' ? this.constTheme : { ...Constants.INIT_THEME });
      } else {
        findTheme = JSON.parse(data.options);
      }
    } else {
      findTheme = { ...Constants.INIT_THEME };
    }

    this.f.theme.setValue(theme);
    findTheme.name = theme;
    this.theme = findTheme;
  }

  get f() {
    return this.tenantForm.controls;
  }

  selectImage(files: any, flag: any = '') {

    if (files.length === 0)
      return;
    const extension = files[0].name.split('.')[1].toLowerCase();
    const imgt = CustomValidators.imgType.includes(extension);

    if (imgt) {
      // this.selectLogo = files[0];
      const reader = new FileReader();
      // this.imagePath = files;
      reader.readAsDataURL(files[0]);
      reader.onload = (_event) => {

        if (flag == 'logo') {
          this.imgURL = reader.result;
          this.selectLogo = files[0];
        } else if (flag == 'fab') {
          this.fabImgURL = reader.result;
          this.selectFabIcon = files[0];
        } else {
          this.signUpPopURL = reader.result;
          this.selectSignupImg = files[0];
        }

      }
    } else {
      if (flag == 'logo') this.imgURL = ''; else this.fabImgURL = '';
    }

  }

  changeTheme(theme: any) {
    this.theme = theme;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid

    this.theme.name = this.f.theme.value;

    if (this.tenantForm.invalid) return;

    if (this.tenantForm.value.assignedProvider?.length == 0) {
      toastr.error('Please Choose any one casino provider!');
      return
    }
    // console.log('theme ', this.theme);

    if (this.tenantForm.invalid) return;
    if (this.id == 0) {
      if (this.f.currencies.value.indexOf('1') == -1) {
        toastr.error('Please select EURO currency for Tenant!');
        return
      }
    }
    this.tenantLoader = true;

    const data = this.tenantForm.getRawValue();
    const fd = new FormData();
    if (data.loginMethod == true) {
      data.loginMethod = 1;
    } else {
      data.loginMethod = 0;
    }
    delete data.logo;
    delete data.theme;

    if (this.selectLogo && this.selectLogo.name) {
      fd.append('logo', this.selectLogo, this.selectLogo.name);
    }

    if (this.selectFabIcon && this.selectFabIcon.name) {
      fd.append('fab_icon_url', this.selectFabIcon, this.selectFabIcon.name);
    }


    if (this.selectSignupImg && this.selectSignupImg.name) {
      fd.append('signup_popup_image_url', this.selectSignupImg, this.selectSignupImg.name);
    }

    for (let key in data) {
      if (key !== 'currencies') fd.append(key, data[key]);
    }

    data.currencies.forEach((currency: any, i: number) => {
      fd.append(`currencies[${i}]`, currency);
    });

    fd.append('theme', JSON.stringify(this.theme));

    if (this.id > 0) {

      this.tenantService.updateTenants(this.id, fd)
        .subscribe((response: any) => {
          toastr.success(response?.message || 'Tenant Updated Successfully!');
          this.router.navigate(['/super-admin/tenants']);
          this.tenantLoader = false;
        }, (err: any) => {
          this.tenantLoader = false;
        });


    } else {

      this.tenantService.createTenants(fd)
        .subscribe((response: any) => {
          toastr.success(response?.message || 'Tenant Created Successfully!');
          this.router.navigate(['/super-admin/tenants']);
          this.tenantLoader = false;
        }, (err: any) => {
          this.tenantLoader = false;
        });

    }
  }

  getPaymentProviders() {

    this.tenantService.getPaymentProviders().subscribe((res: any) => {
      this.paymentProviders = res.record;
    });

  }
}
