import { Component, OnInit } from '@angular/core';
import { Tenant } from 'src/app/models';
import { TenantService } from 'src/app/modules/tenant/tenant.service';
import { PageSizes } from 'src/app/shared/constants';

declare const toastr: any;

@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {
  pageSizes: any[] = PageSizes;

  params: any = {
    size: 10,
    page: 1,
    name: '',
    domain: ''
  };

  p: number = 1;
  total: number = 0;
  tenants: Tenant[] = [];

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Tenants', path: '/super-admin/tenants' },
  ];

  constructor(private tenantService: TenantService) { }

  ngOnInit(): void {
    this.getTenants();
  }

  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getTenants();
  }

  getTenants() {
    this.tenantService.getTenants(this.params).subscribe((res: any) => {
      this.tenants = res.record.data;
      this.total = res.record.count;
    });
  }

  filter(event: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getTenants();
  }

  resetFilter() {
    this.p = 1;
    this.params = { 
      ...this.params, 
      size: 10,
      page: 1,
      name: '',
      domain: '' 
    }
    this.getTenants();
  }


}
