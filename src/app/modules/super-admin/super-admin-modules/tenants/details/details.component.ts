import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Currency, Tenant, User, Credentials, Admin, MenuMaster } from 'src/app/models';
import { TenantService } from 'src/app/modules/tenant/tenant.service';
import { MenuService } from 'src/app/modules/super-admin/super-admin-modules/menumaster/menu.service';
import { getRole, truncateString } from 'src/app/services/utils.service';
import { NoEditOptionForTenantCredentails, TENENT_PERMISSION } from 'src/app/shared/constants';
import Swal from 'sweetalert2';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})

export class DetailsComponent implements OnInit {

  id: number = 0;
  tenant!: Tenant;
  users: Admin[] = [];
  reportSettings: any[] = [];
  menu: any[] = [];
  disabledProviders:any;
  tenantRegistrationFieldsconfiguration: any[] = [];

  submitted: boolean = false;
  adminLoader: boolean = false;
  tenantRegistrationFields: FormGroup | any;
  

  configurationsCurrencies: Currency[] = [];
  credentials: Credentials[] = [];
  paymentProviders: any[] = [];
  setting: any
  NoEditOptionForTenantCredentails:any = NoEditOptionForTenantCredentails;
  p: number = 1;
  remainingCredentials: any[] = [];
  remainingPaymentProvider: any[] = [];
  sportsSetting: any[] = [];
  sports: any[] = [];
  menuSetting: any[] = [];


  tenantPermissionForm: FormGroup | any;
  tenantPermissionFormSubmitted: boolean = false;
  tenantPermissionLoader: boolean = false;

  selectedPermissions: any = {};
  permissions:any= {}
  keyArr = (Object.keys(TENENT_PERMISSION));

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' }
  ];

  permissionLabels = [
    {label: 'R', value: 'Read'},
    {label: 'C', value: 'Create'},
    {label: 'U', value: 'Update'},
    {label: 'D', value: 'Delete'},
    {label: 'T', value: 'Toggle'},
    {label: 'financial_activity', value: 'Financial Activity'},
    {label: 'gaming_activity', value: 'Gaming Activity'},
    {label: 'export', value: 'Export'},      
  ]

  intervalForm: FormGroup | any;
  paymentGatewayCategoriesEnable: boolean = false;

  constructor(private route: ActivatedRoute,
    public tenantService: TenantService,
    public menuService: MenuService,
    private formBuilder: FormBuilder,
  ) {
      this.id = this.route.snapshot.params['id'];
      this.breadcrumbs.push({ title: 'Tenants', path: '/super-admin/tenants' });


      this.tenantRegistrationFields = this.formBuilder.group({
        phaseExecutions: this.formBuilder.group({
          PRE: this.formBuilder.array([])
        }),
      });

      this.tenantPermissionForm = this.formBuilder.group({
        tenant_id: [this.id, [ Validators.required ]],
        permission: ['', [Validators.required]]
      });

      this.intervalForm = this.formBuilder.group({
        tenant_id: [this.id, [ Validators.required ]],
        interval: new FormControl('daily')
      });

    }

  ngOnInit(): void {
    this.getTenant();
    this.getCredential();
    this.getPaymentProviders();
    // this.getMenuList();    
  }


  get configuration_keys() {
    const control = <FormArray>(<FormGroup>this.tenantRegistrationFields.get('phaseExecutions')).get('PRE');
    return control;
  }

  get phaseExecutions(): FormArray {
    return this.tenantRegistrationFields.get('phaseExecutions').get('PRE') as FormArray;
  }

  getProviderFormArr(index: any): FormGroup {
    const formGroup = this.phaseExecutions.controls[index] as FormGroup;
    return formGroup;
  }

  newProviderKeys(key:any = '',val: any = '',loginType:any = 0): any {
    let disable = false;
    if(key === 'username' || key === 'password' || key === 'currency'){
        disable = true
    }
    if(loginType ==  1 && key == 'phone'){
      // Mobile Login Enable
      val = 2;
      disable = true;
    }else if(loginType ==  0 && key == 'email'){
      // Email Base Login
      val = 2;
      disable = true;
    }
    return this.formBuilder.group({
      [key]: [{value:(val !== '' ? val : ''),disabled:disable}, [Validators.required]],
    })
  }


  getControlByName(index: any): any {
    const formGroup = this.phaseExecutions.controls[index] as FormGroup;
    let finalObj = Object.keys(formGroup.value);
    return finalObj[0];
  }



  onSubmit() {

    this.submitted = true;

    if (this.tenantRegistrationFields.invalid) {
      return;
    }

    this.adminLoader = true;

    let data = this.tenantRegistrationFields.getRawValue();
    data.tenantId = this.id;
      
    if (!data.phaseExecutions.PRE.length) {
      toastr.error('Please Seelct All the field action');
      this.adminLoader = false;
      return
    }
    
    this.tenantService.updateRegistrationField(data)
    .subscribe((response: any) => {
      toastr.success(response?.message || 'Tenant Registration Field updated!');
        this.adminLoader = false; 
    }, (err: any) => {
      this.adminLoader = false; 
    });


    
  }



  getTenant() {
    this.tenantService.getSuperAdminTenant(this.id).subscribe((res: any) => {
      this.reportSettings = res.record.report_setting;
      this.tenant = res.record.tenants;
      this.users = res.record.users;
      this.configurationsCurrencies = res.record.configurations;
      this.credentials = res.record.credentials;
      this.paymentProviders = res.record.paymentProvider;
      this.setting = res.record.setting;
      this.sports = res.record.sports.result;
      this.sportsSetting = res.record.sports_bet_setting;
      this.menu = res.record.menu_setting;

      this.disabledProviders = res.record.setting.allowed_payment_providers;
      let result = this.disabledProviders.map((i:any)=>Number(i));
      this.paymentProviders.map(provider=>{
        result.includes(provider.provider_id) == true ? provider.disabledByAdmin=false: provider.disabledByAdmin=true;
      })

      if(res.record.playerCommissionInterval){
        this.intervalForm.get('interval').setValue(res.record.playerCommissionInterval)
      }

      // this.selectedPermissions = (res.record.tenantPermissions) ? JSON.parse(res.record.tenantPermissions) : {}

      if(res.record.tenantPermissions){
        this.selectedPermissions = JSON.parse(res.record.tenantPermissions)
      }else{
        this.togglePermission('dashboard', 'R')
        this.togglePermission('notifications', 'R')
        this.togglePermission('permission_role', 'R')
      }
      
      this.permissions = TENENT_PERMISSION

  
      this.tenantRegistrationFieldsconfiguration = res.record.tenantRegistrationFields;
      for (let x in this.tenantRegistrationFieldsconfiguration) {
        
        this.configuration_keys.push(this.newProviderKeys(x,this.tenantRegistrationFieldsconfiguration[x],this.setting.user_login_type));
      }

      this.paymentGatewayCategoriesEnable = this.setting?.allowed_modules?.includes('paymentGatewayCategoriesEnable');
      
      this.breadcrumbs.push({ title: truncateString(this.tenant.name, 10), path: `/super-admin/tenants/details/${this.id}` });
    });
  }

  addMenu(e:any,oldValue:any,index:number){
    let NewValue = e.target.checked;
    const param ={
      menu_id : e.target.id,
      selected : NewValue,
      tenant_id: this.id
    }
    e.target.checked = oldValue;
    this.tenantService.getSuperAdminTenantMenuSetting(param).subscribe((res: any) => {
      e.target.checked = NewValue;
    });

  }

  getCredential() {
    this.tenantService.getSuperAdminTenantRemCred(this.id).subscribe((res: any) => {
      this.remainingCredentials = res.record;
    });
  }


  getPaymentProviders() {
    this.tenantService.getSuperAdminTenantRemPaymentProvider(this.id).subscribe((res: any) => {
      this.remainingPaymentProvider = res.record;
        
      
    });
  }

  checkPermission(permission: any, report: string) {
    
    const data = {
      id: this.id,
      report: report,
      permission: permission.checked
    };

    // console.log('data ', data);

    // this.tenantService.updateTenantsConfigurations(data).subscribe((res: any) => {
    //   toastr.success( res.message || 'Configurations has been updated successfully');
    // });
  }
  
  getRole(role: any) {
    return getRole(role);
  }

  getRoles(roleIds: any) {
    if(roleIds) {
      if(roleIds && roleIds.length > 1) {
        return roleIds.split(',');
      } else {
        return roleIds.split('');
      }
    }
  }

  updateReportStatus(report: any, status: string) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to ${status}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, ${status} it!`
    }).then((result) => {
      if (result.isConfirmed) {

        this.tenantService.updateReportStatus({ report_id: report.id, tenant_id: this.tenant.id, status }).subscribe((res: any) => {
          toastr.success(res.message || 'Sport updated successfully' );
          this.reportSettings = this.reportSettings.map((f: any) => {
            if (f.id == report.id) {
              f.status = status;
            }
            return f;
          });
        });
        
      }
    });
  }



  activeProviderStatus(data: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to Active ${data.provider_name}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Active it!'
    }).then((result) => {
      if (result.isConfirmed) {

        let providerData = {
          "id" : data.id,
          "status": true
        };

        this.tenantService.tenantProviderStatus(providerData).subscribe((res: any) => {
          toastr.success(res.message || 'Payment Provider Activate successfully' );
          this.paymentProviders = this.paymentProviders.map((f: any) => {
            if(f.id == data.id) {
              f.active =  true;
            }
            return f;
          });
        });
        
      }
    });

  }

  deactiveProviderStatus(data: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to Deactive ${data.provider_name}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Deactivate it!'
    }).then((result) => {
      if (result.isConfirmed) {
        let providerData = {
          "id" : data.id,
          "status": false
        };
        this.tenantService.tenantProviderStatus(providerData).subscribe((res: any) => {
          toastr.success(res.message || 'Payment Provider Deactivated successfully' );
          this.paymentProviders = this.paymentProviders.map((f: any) => {
            if(f.id == data.id) {
              f.active = false;
            }
            return f;
          });
        });
        
      }
    });
    
  }



  getMenuList(){
    this.menu = [];
    this.menuService.getMenu().subscribe((res: any) => {
      this.menu = res.record;
    });
  }



  //for tenant permission

  getPermissionsFromValue(value: any) {
    return value;
  }

  isEmptyPermission(value: any) {
        
    return value?.length > 0;
  }

  isPermissionSelected(header: any, permission: any) {

    // console.log(Object.keys(this.selectedPermissions));
    

    // console.log(header, this.selectedPermissions.hasOwnProperty(header));

    // console.log(Array.isArray(this.selectedPermissions[header]));

    // console.log(this.selectedPermissions[header].length > 0);
    
    
    
    if (this.selectedPermissions.hasOwnProperty(header) && Array.isArray(this.selectedPermissions[header]) && this.selectedPermissions[header].length > 0) {
      // Check if the permission exists in the module's permissions array
      return this.selectedPermissions[header].includes(permission);
  }

  // Module not found in the configuration
  return false;
  }



  togglePermission(header: any, permission: any) {
    let isRemoved = false;
    
    if (!this.selectedPermissions[header]) {
      this.selectedPermissions[header] = [];
    }

    if ((header == 'fund_transfer_deposit' || header == 'fund_transfer_withdraw') && (!this.selectedPermissions['agents']?.includes('R') && !this.selectedPermissions['players']?.includes('R'))) {
      toastr.error(
        "Either Agents or Players should be selected before Fund Transfer Permissions"
      );
      return;
    }

    if (permission !== 'R' && !this.selectedPermissions[header].includes('R')) {
      // toastr.error(
      //   'Read Permissions is required first'
      // );
      // return;
      this.selectedPermissions[header].push('R');
    }
    
    if (header == 'dashboard' && permission == 'R' && this.selectedPermissions[header].includes('R')) {
      toastr.error(
        "Dashboard Read Permissions cannot be unselected"
      );
      return;
    }

    if (header == 'notifications' && permission == 'R' && this.selectedPermissions[header].includes('R')) {
      toastr.error(
        "Notifications Read Permissions cannot be unselected"
      );
      return;
    }

    if (header == 'permission_role' && permission == 'R' && this.selectedPermissions[header].includes('R')) {
      toastr.error(
        "Permission Role Read Permissions cannot be unselected"
      );
      return;
    }

    this.selectedPermissions[header] = this.selectedPermissions[header].filter(
      (p:any) => {
        if (p == permission) {
          isRemoved = true;
          return false;
        }
        return true;
      }
    );

    if (isRemoved) {
      if (permission === 'R') {
        this.selectedPermissions[header] = [];
        if(!this.selectedPermissions['agents']?.length && !this.selectedPermissions['players']?.length){
          this.selectedPermissions['fund_transfer_deposit'] = []
          this.selectedPermissions['fund_transfer_withdraw'] = []
        }
      }
    } else {
      this.selectedPermissions[header].push(permission);
    }
  }

  getLabel(value: any): string {
    return this.permissionLabels.find(v => v.label == value)?.value || value;
  }

  get f() {
    return this.tenantPermissionForm.controls;
  }

  submitPermission(){
    this.tenantPermissionFormSubmitted = true;

    this.f.permission.setErrors({ required: null });

    // console.log(this.adminForm.value);
    let isPermissionSelected = false;
    for (let sp in this.selectedPermissions) {
      if (this.selectedPermissions[sp].length > 0) {
        isPermissionSelected = true;
        break;
      }
    }


    const filteredPermissions = Object.entries(this.selectedPermissions).reduce(
      (acc:any, [key, value]:any) => {
        if (value.length > 0) {
          acc[key] = value;
        }
        return acc;
      },
      {}
    );
    
    
    if (isPermissionSelected) {
      this.f.permission.setValue(JSON.stringify(filteredPermissions));
      
    } else {
      this.f.permission.setErrors({
        required: 'permission are required',
      });
    }

    if (this.tenantPermissionForm.invalid) return;    

    this.tenantPermissionLoader = true;
 
    const tenant_id = this.tenantPermissionForm.value.tenant_id;
    const permission = this.tenantPermissionForm.value.permission;
    // let permissionRole_option = (this.permissionRole ? this.permissionRole : this.intPermissionRole)
    // const options = { ...permissionRole_option, role_name };

    const data: any = { tenant_id, permission};
    
    this.tenantService.updateTenantPermission(data)
    .subscribe((response: any) => {
      toastr.success(response?.message || 'Tenant Permissions updated!');
        this.tenantPermissionLoader = false; 
    }, (err: any) => {
      this.tenantPermissionLoader = false; 
    });
  }

  compareTenantPermissionObjects() {
    const keys1 = Object.keys(this.selectedPermissions);
    const keys2 = Object.keys(this.permissions);
  
    if (keys1.length !== keys2.length) {
      return false;
    }
  
    for (const key of keys1) {
      if (!keys2.includes(key) || !this.arraysAreEqual(this.selectedPermissions[key], this.permissions[key])) {
        return false;
      }
    }
  
    return true;
  }
  
  arraysAreEqual(arr1:any, arr2:any) {
    if (arr1.length !== arr2.length) {
      return false;
    }
    
    const sortedArr1 = arr1.slice().sort();
    const sortedArr2 = arr2.slice().sort();
  
    return sortedArr1.every((value:any, index:any) => value === sortedArr2[index]);
  }

  toggleAllPermissions(event:any){
    if(event.target.checked){
      this.setPermissions(this.permissions)
    }
    else{
      this.selectedPermissions = {}
      this.togglePermission('dashboard', 'R')
      this.togglePermission('notifications', 'R')   
      this.togglePermission('permission_role', 'R')
    }
  }

  setPermissions(permissions:any) {
    this.selectedPermissions = Object.assign({}, permissions);
  }

  updatePlayerCommissionInterval(interval:any){
    this.intervalForm.get('interval').setValue(interval);
    if(this.intervalForm.invalid){
      return
    }

    console.log(this.intervalForm.value);
    

    this.tenantService.updateTenantPlayerCommissionInterval(this.intervalForm.value).subscribe((res: any) => {
    });

  }

  jsonParse(value:any){   
    if(value != "null"){
      return JSON.parse(value)
    }
    else{
      return [];
    }
  }
}
