import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ThirdPartyAPIService } from 'src/app/services/third-party.service';

declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './reset-event.component.html',
  styleUrls: ['./reset-event.component.scss'],
  providers: [ ThirdPartyAPIService ]
})
  
export class ResetEventComponent implements OnInit {
  
  eventForm: FormGroup | any;
  submitted: boolean = false;
  eventLoader: boolean = false;

  resetEventTime: number = 16;

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Settings', path: '/super-admin/settings' }
  ];

  status: Array<any> = [
    { value: 1, name: 'Live' },
    { value: 2, name: 'PreMatch' }
  ];
  
  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private thirdPartyAPIService: ThirdPartyAPIService
  ) {
    
    const resetEventTime = localStorage.getItem('reset_event_time');
    
    if (resetEventTime) {
      const dateTwo: any = new Date();
      const dateOne: any = new Date(JSON.parse(resetEventTime));
      
      let msDifference =  dateTwo - dateOne;
      this.resetEventTime = Math.floor(msDifference / 1000 / 60);

      // console.log(this.resetEventTime);

    }
      this.eventForm = this.formBuilder.group({
        fixture_id: ['', [ Validators.required ]],
        status: ['', [ Validators.required ]],
      });
    }

  ngOnInit(): void { }
  
  get f() {
    return this.eventForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.eventForm.invalid) return;

    this.eventLoader = true;

    this.thirdPartyAPIService.resetEvent().subscribe((res: any) => {
      localStorage.setItem('reset_event_time', JSON.stringify(new Date()));
      toastr.success( res?.message || `Reset Event Successfully!`);
      this.eventLoader = false;
    }, err => {
      this.eventLoader = false;
    });
    
  }

}
