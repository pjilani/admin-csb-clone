import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SettingService } from '../setting.service';
declare const $: any;
declare const toastr: any;
@Component({
  selector: 'app-mail-configuration',
  templateUrl: './mail-configuration.component.html',
  styleUrls: ['./mail-configuration.component.scss']
})
export class MailConfigurationComponent implements OnInit {
  
  
  configuration!: any;
  mailConfigurationForm: FormGroup | any;
  submitted: boolean = false;
  adminLoader: boolean = false;


  constructor(
    private formBuilder: FormBuilder,
    private settingService: SettingService,
  ) {

    this.mailConfigurationForm = this.formBuilder.group({
      phaseExecutions: this.formBuilder.group({
        PRE: this.formBuilder.array([])
      }),
    });

  }
  
  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Settings', path: '/super-admin/settings' }
  ];

  ngOnInit(): void {
  }


  get configuration_keys() {
    const control = <FormArray>(<FormGroup>this.mailConfigurationForm.get('phaseExecutions')).get('PRE');
    return control;
  }

  get phaseExecutions(): FormArray {
    return this.mailConfigurationForm.get('phaseExecutions').get('PRE') as FormArray;
  }

  getProviderFormArr(index: any): FormGroup {
    const formGroup = this.phaseExecutions.controls[index] as FormGroup;
    return formGroup;
  }

  newProviderKeys(key:any = '',val: any = ''): any {
    return this.formBuilder.group({
      [key]: [(val != '' ? val : ''), [Validators.required]],
    })
  }


  getControlByName(index: any): any {
    const formGroup = this.phaseExecutions.controls[index] as FormGroup;
    let finalObj = Object.keys(formGroup.value);
    return finalObj[0];
  }


  ngAfterViewInit(): void {
    
    this.settingService.getMailConfiguration().subscribe((res: any) => {
        this.configuration = res.record.data;
        for (let x in this.configuration) {
          this.configuration_keys.push(this.newProviderKeys(this.configuration[x]['key_provided_by_account'],this.configuration[x]['value']));
        }
      });
  }

  get f() {
    return this.mailConfigurationForm.controls;
  }


  onSubmit() {

    this.submitted = true;

    if (this.mailConfigurationForm.invalid) {
      return;
    }

    this.adminLoader = true;

    const data = this.mailConfigurationForm.value;
    
    if (!data.phaseExecutions.PRE.length) {
      toastr.error('Please Provide Keys for setup Mail!');
      this.adminLoader = false;
      return
    }
    
    this.settingService.updateMailConfiguration(data)
    .subscribe((response: any) => {
      toastr.success(response?.message || 'Mail Configuration Updated Successfully!');
        this.adminLoader = false; 
    }, (err: any) => {
      this.adminLoader = false; 
    });


    
  }

}
