import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OldLogListComponent } from './old-log-list.component';

describe('OldLogListComponent', () => {
  let component: OldLogListComponent;
  let fixture: ComponentFixture<OldLogListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OldLogListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OldLogListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
