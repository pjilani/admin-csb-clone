import { AfterViewInit, Component, OnInit } from '@angular/core';
import { SettingService } from '../setting.service';
import { LogServices, PageSizes } from 'src/app/shared/constants';
import { TenantService } from 'src/app/modules/tenant/tenant.service';
import { Tenant } from 'src/app/models';
declare const $: any;

@Component({
  selector: 'app-log-list',
  templateUrl: './old-log-list.component.html',
  styleUrls: ['./old-log-list.component.scss']
})
export class OldLogListComponent implements OnInit,AfterViewInit {

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Settings', path: '/super-admin/settings' }
  ];
  format: string = "YYYY-MM-DD HH:mm:ss";
  displayRequestObject:any;
  jsons:any = {
    request: {},
    response: {}
  }
  displayResponseObject:any;
  tenants: Tenant[] = [];

  params: any = {
    size: 10,
    page: 1,
    search: '',
    tenant_id: '',
    request_json: '',
    response_json:'',
    service: '',
    action_type: '',
    time_period: {
      // start_date: '',
      // end_date: '',
    },
    res_status:'',
    error_code:'',
    res_code:'',
    id:'',
    order: 'desc',
    sort_by: 'id'
  };
  services = LogServices;
  logs:any[] = [];
  total: number = 0;
  p: number = 1;
  pageSizes: any[] = PageSizes;

  isLoader:boolean = false;
  firstTimeApiCall = false;


  constructor(private settingService: SettingService,private tenantSer: TenantService) { }

  ngOnInit(): void {
    // this.getOldLog();
    this.getTenantsAll()
  }
  ngAfterViewInit(): void {
    setTimeout(() => {
      $('#time_period_log').val('');
    }, 100);
  }
  convertToJSON(data:any) {    
    return JSON.parse(data);
  }
  getOldLog() {
    this.isLoader = true
    this.firstTimeApiCall = true;
    this.settingService.getOldLog(this.params).subscribe((res: any) => {
        this.logs = res.record.data;
        this.total = res.record.total;
        this.isLoader = false
      },(error:any) => {
        this.isLoader = false

      });
  }

  exportAsXLSX() {
    this.settingService
      .downloadLogOld(this.params)
      .subscribe((res: any) => {
        window.location.href = res.record.url;
      });
  }

  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getOldLog();
  }

  filter(evt: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getOldLog();
  }

  setOrder(sort: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p, order: sort.order, sort_by: sort.column };
    this.getOldLog();
  }

  resetFilter() {
    this.p = 1;
    $('#time_period_log').val('');
    this.params = {      
      size: 10,
      page: 1,
      search: '',
      order: 'desc',
      sort_by: 'id',
      request_json: '',
      response_json:'',  
      tenant_id: '',
      service: '',
      time_period: {
        // start_date: '',
        // end_date: '',
      },
      res_status:''
    };

    this.getOldLog();
  }
  selectDateRange(time_period: any) {
    this.params = { ...this.params, time_period }
    // this.getOldLog();
  }
  getTenantsAll() {
    this.tenantSer.getTenantsAll().subscribe((res: any) => {
      this.tenants = res.record;

    });
  }
  getTenantName(id:any){
    const foundElement = this.tenants.find(item => item.id === id)?.name;
    return foundElement
  }

}
