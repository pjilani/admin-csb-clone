import { Component, OnInit } from '@angular/core';
import { SettingService } from '../setting.service';
import { PageSizes, QUEUE_LOG_STATUSES, QUEUE_LOG_TYPES } from 'src/app/shared/constants';
import { TenantService } from 'src/app/modules/tenant/tenant.service';
import { Tenant } from 'src/app/models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
declare const $: any;
declare const toastr: any;

@Component({
  selector: 'app-queue-logs',
  templateUrl: './queue-logs.component.html',
  styleUrls: ['./queue-logs.component.scss']
})
export class QueueLogsComponent implements OnInit {

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Settings', path: '/super-admin/settings' }
  ];
  format: string = "YYYY-MM-DD HH:mm:ss";
  displayRequestObject:any;
  jsons:any = {
    request: {},
    response: {}
  }
  displayResponseObject:any;
  tenants: Tenant[] = [];

  params: any = {
    size: 10,
    page: 1,
    type: '',
    status: '',
    // time_period: {
    //   // start_date: '',
    //   // end_date: '',
    // },
    order: 'desc',
    sort_by: 'id'
  };
  queueLogs:any[] = [];
  total: number = 0;
  p: number = 1;
  pageSizes: any[] = PageSizes;

  isLoader:boolean = false;

  types:any = QUEUE_LOG_TYPES
  statuses:any = QUEUE_LOG_STATUSES
  form:FormGroup | any
  isModalOpen: boolean = false;
  submitted: boolean = false;
  uploadedFileName: any;
  selectFile:any;
  firstTimeApiCall = false;


  constructor(private settingService: SettingService,private tenantSer: TenantService, private formBuilder:FormBuilder) {
    this.form = this.formBuilder.group({
      type: ['', [Validators.required]],
      csv: [false, Validators.required],
      ids: [null, [Validators.required,Validators.pattern('^([0-9]+,)*[0-9]+$')]],
      csvFile: []
    });

    this.form.get('csv').valueChanges.subscribe((value:any) => {
      if(value){
        this.form.get('csvFile').setValidators([Validators.required]);
        this.form.get('ids').clearValidators();
      }else{
        this.form.get('ids').setValidators([Validators.required,Validators.pattern('^([0-9]+,)*[0-9]+$')]);
        this.form.get('csvFile').clearValidators();
      }

      this.form.get('ids').updateValueAndValidity();
      this.form.get('csvFile').updateValueAndValidity();
    })
   }

  ngOnInit(): void {
    // this.getQueueLogs();
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      $('#time_period_log').val('');
    }, 100);
  }

  convertToJSON(data:any) {    
    return JSON.parse(data);
  }
  getQueueLogs() {
    this.isLoader = true
    this.firstTimeApiCall = true;
    this.settingService.getQueueLogs(this.params).subscribe((res: any) => {
        this.queueLogs = res.record.data;
        this.total = res.record.total;
        this.isLoader = false
      },(error:any) => {
        this.isLoader = false

      });
  }

  exportAsXLSX() {
    this.settingService
      .downloadLog(this.params)
      .subscribe((res: any) => {
        window.location.href = res.record.url;
      });
  }

  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getQueueLogs();
  }

  filter(evt: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getQueueLogs();
  }

  setOrder(sort: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p, order: sort.order, sort_by: sort.column };
    this.getQueueLogs();
  }

  resetFilter() {
    this.p = 1;
    $('#time_period_log').val('');
    this.params = {      
      size: 10,
      page: 1,
      type: '',
      status: '',
      // time_period: {
      //   // start_date: '',
      //   // end_date: '',
      // },
      order: 'desc',
      sort_by: 'id'
    };

    this.getQueueLogs();
  }
  // selectDateRange(time_period: any) {
  //   this.params = { ...this.params, time_period }
  //   // this.getLogs();
  // }

  get f() {
    return this.form.controls;
  }

  openModal(){
    $('#modal-create-queue-log').modal('show');
    this.isModalOpen = true
  }

  closeModal(){
    $('#modal-create-queue-log').modal('hide');
    this.isModalOpen = false
    this.submitted =false
    this.resetForm();
  }

  resetForm(){
    this.form.reset({
      type:'',
      csv:false,
      ids: null,
      csvFile: null
    });

    this.selectFile = undefined
    this.uploadedFileName = undefined
  }

  submitForm(){
    this.submitted = true
    if(this.form.invalid){
      return
    }

    let data = this.form.value

    const fd = new FormData();

    delete data.csvFile;
    if(data.csv){
      delete data.ids
      if(this.selectFile && this.selectFile.name) {
        fd.append('csvFile', this.selectFile, this.selectFile.name);
      }
    }else{
      data.ids = data.ids.split(',')
    }

  
    

    for(let key in data) {
      if(key == 'ids'){
        data[key].forEach((value:any, index:any) => {
          fd.append('ids[]', value);
        });
      }else{
        fd.append(key, data[key]);
      }
    }

    this.settingService.createQueueLogs(fd).subscribe((res:any)=>{
      toastr.success(res.message || 'Logs Created Successfully');
      this.closeModal();
      this.getQueueLogs();
    }, err =>{
      this.closeModal();
    })
  }

  selectCsv(files: any) {
    if (files.length === 0)
    return;
    const extension = files[0].type.split('/')[1].toLowerCase();
    const imgt = ['csv'].includes(extension);

    if(imgt) {
      // this.selectLogo = files[0];
      this.uploadedFileName = files[0].name;
        const reader = new FileReader();
        // this.imagePath = files;
        reader.readAsDataURL(files[0]); 
        reader.onload = (_event) => { 

          // this.csvForm.csvFile.setValue(csvContent);

          // this.imgURL = reader.result; 
        }
        this.selectFile = files[0];
      } else {
        // this.imgURL = '';
        // this.csvForm.csvFile.setValue(null);
        this.uploadedFileName = undefined;
      }

  }

}
