import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResetEventComponent } from './reset-event/reset-event.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { MailConfigurationComponent } from './mail-configuration/mail-configuration.component';
import { LogListComponent } from './log-list/log-list.component';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { OldLogListComponent } from './old-log-list/old-log-list.component';
import { QueueLogsComponent } from './queue-logs/queue-logs.component';
import { PipeModule } from 'src/app/pipes/pipes.module';

const settingRoutes: Routes = [
  // { path: '', redirectTo: 'reset-event', pathMatch: 'full' },
  { path: '', redirectTo: 'mail-configuration', pathMatch: 'full' },
  { path: 'reset-event', component: ResetEventComponent },
  { path: 'mail-configuration', component: MailConfigurationComponent },
  { path: 'request-res-log', component:LogListComponent},
  { path: 'old-request-res-log', component:OldLogListComponent},
  { path: 'permissionRole', loadChildren: () => import('./permissionRole/permissionRole.module').then(m => m.PermissionRoleModule)},
  { path: 'queue-logs', component:QueueLogsComponent},
]

@NgModule({
  declarations: [
    ResetEventComponent,
    MailConfigurationComponent,
    LogListComponent,
    OldLogListComponent,
    QueueLogsComponent
  ],
  imports: [
    CommonModule,
    DirectivesModule,
    ComponentsModule,
    NgxJsonViewerModule,
    SharedModule,
    PipeModule,
    RouterModule.forChild(settingRoutes)
  ]
})

export class SettingModule { }
