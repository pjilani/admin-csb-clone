import { Component, Input, OnInit } from '@angular/core';
import { PermissionRole, Tenant } from 'src/app/models';
import { PermissionRoleService } from '../permissionRole.service';
import Swal from 'sweetalert2';
import { PermissionService } from 'src/app/services/permission.service';
import { TenantService } from 'src/app/modules/tenant/tenant.service';
import { ActivatedRoute } from '@angular/router';
declare const toastr: any;

@Component({
  selector: 'permission-role-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {

  @Input() tenant!: Tenant;

  p: number = 1;

  params = {
    size: 10,
    page: 1,
    search_role_name: '',
    tenant_id: 0,
    role_type: '',
  }

  total:number = 0;

  permissionRoles: PermissionRole[] = [];

  // breadcrumbs: Array<any> = [
  //   { title: 'Home', path: '' },
  //   { title: 'Permission Role', path: '/super-admin/settings/permissionRole' },
  // ];

  roles: any[] = [{ name: 'Agent', value: 2 }, { name: 'Sub-admin', value: 3 }];

  tenants: Tenant[] = [];

  isLoader:boolean = false;
  tenant_id!:any;

  constructor(
    private route: ActivatedRoute,
    private permissionRoleService: PermissionRoleService,
    public permissionService: PermissionService,
    private tenantSer: TenantService) { 
      this.tenant_id = this.route.snapshot.params['id'];
      this.params = { ...this.params, tenant_id: this.tenant_id };
    }

  ngOnInit(): void {
    this.getTenantsAll();
    // this.getPermissionRole();   
  }

  getPermissionRole() { 
    this.isLoader = true
    this.permissionRoleService.getPermissionRoles(this.params).subscribe((res: any) => {
      this.permissionRoles = res.record.data;
      this.total = res.record.total;
      this.isLoader = false
    });
  } 

  delete(permissionRole: PermissionRole) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to delete ${permissionRole.role_name}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, delete it!`
    }).then((result) => {
      if (result.isConfirmed) {

        this.permissionRoleService.deletePermissionRole(permissionRole.id).subscribe((res: any) => {
          toastr.success( res.message || 'Permission Role Deleted Successfully.');
          this.getPermissionRole();
        });
        
      }
    });
    
  }

  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getPermissionRole();
  }

  getPermissionRoleType(role_type:any){ 
    return this.roles.find(role => role.value === role_type)?.name ?? '-'
  }

  getTenantsAll() {
    this.tenantSer.getTenantsAll().subscribe((res: any) => {
      this.tenants = res.record;

    });
  }

  getTenantName(id:any){
    const foundElement = this.tenants.find(item => item.id === id)?.name;
    return foundElement
  }

  filter(evt: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getPermissionRole();
  }

  resetFilter() {
    this.p = 1;
    this.params = {      
      size: 10,
      page: 1,
      search_role_name: '',
      tenant_id: this.tenant_id,
      role_type: '',
    };

    this.getPermissionRole();
  }

}
