import { Component, OnInit } from '@angular/core';
import { PermissionRoleService } from '../permissionRole.service';
import { PermissionRole, Tenant } from 'src/app/models';
import { ActivatedRoute } from '@angular/router';
import { TenantService } from 'src/app/modules/tenant/tenant.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  id: number = 0;
  permissionRole!: PermissionRole;
  permissions:any={};

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '' },
    { title: 'Permission Role', path: '/permissionRole' }
  ];

  roles: any[] = [{ name: 'Agent', value: 2 }, { name: 'Sub-admin', value: 3 }];

  tenants: Tenant[] = [];

  permissionLabels = [
    {label: 'R', value: 'Read'},
    {label: 'C', value: 'Create'},
    {label: 'U', value: 'Update'},
    {label: 'D', value: 'Delete'},
    {label: 'T', value: 'Toggle'},
    {label: 'financial_activity', value: 'Financial Activity'},
    {label: 'gaming_activity', value: 'Gaming Activity'},
    {label: 'export', value: 'Export'},    
  ]

  constructor(private route: ActivatedRoute,private permissionRoleService: PermissionRoleService,private tenantSer: TenantService) {
    this.id = this.route.snapshot.params['id'];
   }

  ngOnInit(): void {
    this.getTenantsAll();
    this.getPermissionRole();
  }

  getPermissionRole() {
    this.permissionRoleService.getPermissionRoleById(this.id).subscribe((res: any) => {
      this.permissionRole = res.record;
      this.permissions = JSON.parse(this.permissionRole.permission)
    });
  }

  getPermissionsFromValue(value: any) {
    return value;
  }

  getPermissionRoleType(role_type:any){ 
    return this.roles.find(role => role.value === role_type)?.name ?? '-'
  }

  getLabel(value: any): string {
    return this.permissionLabels.find(v => v.label == value)?.value || value;
  }

  getTenantsAll() {
    this.tenantSer.getTenantsAll().subscribe((res: any) => {
      this.tenants = res.record;

    });
  }

  getTenantName(id:any){
    const foundElement = this.tenants.find(item => item.id === id)?.name;
    return foundElement
  }


}
