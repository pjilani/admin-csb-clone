import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { Routes, RouterModule } from '@angular/router';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { EditComponent } from './edit/edit.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PipeModule } from 'src/app/pipes/pipes.module';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { DetailsComponent } from './details/details.component';

const permissionRoleRoutes: Routes = [
  { path: ':tenantId/:id', component: EditComponent },
  { path: 'details/:tenantId/:id', component: DetailsComponent }
]

@NgModule({
  declarations: [
    EditComponent, DetailsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(permissionRoleRoutes),
    ComponentsModule,
    SharedModule,
    PipeModule
  ]
})
export class PermissionRoleModule { }
