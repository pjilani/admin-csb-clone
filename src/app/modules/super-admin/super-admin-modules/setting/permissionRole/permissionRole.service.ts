import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PermissionRole } from 'src/app/models';

@Injectable({ providedIn: 'root' })

export class PermissionRoleService {

  constructor(private http: HttpClient) { }

  // Super Admin ===================================
  
  getAdminPermissionRoles() {
    return this.http.get(`super/admin/permissionRole`);
  }

  getRoleWisePermissions(params:any) {
    return this.http.get(`super/admin/get-permissions-role-wise?role_type=${params.role_type}`);
  }

  getTenantAndRoleWisePermissions(params:any) {
    return this.http.get(`super/admin/get-permissions-tenant-and-role-wise?tenant_id=${params.tenant_id}&role_type=${params.role_type}`);
  }
  
  // Super Admin ===================================

  getPermissionRoles(params:any) {
    return this.http.get(`super/admin/permissionRole?size=${params.size}&page=${params.page}&tenant_id=${params.tenant_id}&role_type=${params.role_type}&search_role_name=${params.search_role_name}`);
  }

  getPermissionRoleById(id: number) {
    return this.http.get(`super/admin/permissionRole/${id}`);
  }

  createPermissionRole(permissionRole: PermissionRole) {
    return this.http.post(`super/admin/permissionRole`, permissionRole);
  }

  updatePermissionRole(permissionRole: PermissionRole) {
    return this.http.post(`super/admin/permissionRole/${permissionRole.id}`, permissionRole);
  }

  deletePermissionRole(id: number) {
    return this.http.delete(`super/admin/permissionRole/${id}`);
  }

}
