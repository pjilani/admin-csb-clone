import { AfterViewInit, Component, OnInit } from '@angular/core';
import { SettingService } from '../setting.service';
import { LogServices, PageSizes } from 'src/app/shared/constants';
import { TenantService } from 'src/app/modules/tenant/tenant.service';
import { Tenant } from 'src/app/models';
declare const $: any;

@Component({
  selector: 'app-log-list',
  templateUrl: './log-list.component.html',
  styleUrls: ['./log-list.component.scss']
})
export class LogListComponent implements OnInit,AfterViewInit {

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Settings', path: '/super-admin/settings' }
  ];
  format: string = "YYYY-MM-DD HH:mm:ss";
  displayRequestObject:any;
  jsons:any = {
    request: {},
    response: {}
  }
  displayResponseObject:any;
  tenants: Tenant[] = [];

  params: any = {
    size: 10,
    page: 1,
    search: '',
    tenant_id: '',
    request_json: '',
    response_json:'',
    service: '',
    action_type: '',
    time_period: {
      // start_date: '',
      // end_date: '',
    },
    res_status:'',
    error_code:'',
    res_code:'',
    id:'',
    order: 'desc',
    sort_by: 'id'
  };
  services = LogServices;
  logs:any[] = [];
  total: number = 0;
  p: number = 1;
  pageSizes: any[] = PageSizes;
  minSelectableDateTime!: any;


  isLoader:boolean = false;
  firstTimeApiCall = false;


  constructor(private settingService: SettingService,private tenantSer: TenantService) { }

  ngOnInit(): void {
    const currentDate = new Date();
    currentDate.setMonth(currentDate.getMonth() - 2);
    currentDate.setHours(23, 59, 59);
    this.minSelectableDateTime = currentDate;
    // this.getLogs();
    this.getTenantsAll()
  }
  ngAfterViewInit(): void {
    setTimeout(() => {
      $('#time_period_log').val('');
    }, 100);
  }
  convertToJSON(data:any) {    
    return JSON.parse(data);
  }
  getLogs() {
    this.isLoader = true
    this.firstTimeApiCall = true;
    this.settingService.getLog(this.params).subscribe((res: any) => {
      this.logs = res.record.data;
      this.total = res.record.total;
        this.isLoader = false
      },(error:any) => {
        this.isLoader = false
        
      });
    }
    
    exportAsXLSX() {
      this.settingService
      .downloadLog(this.params)
      .subscribe((res: any) => {
        window.location.href = res.record.url;
      });
  }

  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getLogs();
  }

  filter(evt: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getLogs();
  }

  setOrder(sort: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p, order: sort.order, sort_by: sort.column };
    this.getLogs();
  }

  resetFilter() {
    this.p = 1;
    $('#time_period_log').val('');
    this.params = {      
      size: 10,
      page: 1,
      search: '',
      order: 'desc',
      sort_by: 'id',
      request_json: '',
      response_json:'',  
      tenant_id: '',
      service: '',
      time_period: {
        // start_date: '',
        // end_date: '',
      },
      res_status:''
    };

    this.getLogs();
  }
  selectDateRange(time_period: any) {
    this.params = { ...this.params, time_period }
    // this.getLogs();
  }
  getTenantsAll() {
    this.tenantSer.getTenantsAll().subscribe((res: any) => {
      this.tenants = res.record;

    });
  }
  getTenantName(id:any){
    const foundElement = this.tenants.find(item => item.id === id)?.name;
    return foundElement
  }

}
