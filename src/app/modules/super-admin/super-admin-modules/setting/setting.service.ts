import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class SettingService {

  constructor( private http:HttpClient) { }

  getMailConfiguration(){
    return this.http.get(`super/admin/settings/mail_configuration`); 
  }
  updateMailConfiguration(data:any){
    return this.http.post(`super/admin/settings/update_mail_configuration`,data);
  }
  getLog(params:any){
    return this.http.post(`super/admin/log-list`, params);
  }
  getOldLog(params:any){
    return this.http.post(`super/admin/old-log-list`, params);
  }

  getQueueLogs(params:any){
    return this.http.post(`super/admin/queue-logs`, params);
  }

  createQueueLogs(params:any){
    return this.http.post(`super/admin/queue-logs/create`, params);
  }

  downloadLog(params:any){
    return this.http.post(`super/admin/log-download`, params);
  }
  downloadLogOld(params:any){
    return this.http.post(`super/admin/old-log-download`, params);
  }
}
