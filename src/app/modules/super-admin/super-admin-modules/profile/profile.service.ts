import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })

export class ProfileService {

 

  constructor(private http: HttpClient) { }

  
  // ====================== Super Admin url ==============================

  
  getProfile() {
   return this.http.get('super/admin/user');
  }
  SuperAdminWalletDeposit(params:any) {
   return this.http.post('super/admin/wallet/deposit',params);
  }
  getWalletDetails(params:any) {
   return this.http.get('super/admin/user/wallet/details/'+params);
  }
  getTransactionList(params:any) {
   return this.http.post('super/admin/transaction/details',params);
  }


}
