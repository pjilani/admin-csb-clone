import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { SuperAdminAuthService } from '../../../super-admin/services/super-admin-auth.service';
import { ProfileService } from '../profile/profile.service';

import { PageSizes } from 'src/app/shared/constants';
declare const $: any;
declare const toastr: any;
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, AfterViewInit {

  constructor(public superAdminAuthService: SuperAdminAuthService,
    private profileService: ProfileService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder) { }
  profile_id: number = 0;
  admin: any = [];
  params: any;
  awdSubmitted: boolean = false;
  awdLoader: boolean = false;
  txn: any = [];
  txnList: any = [];
  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Profile', path: '/super-admin/profile' },
  ];

  pageSizes: any[] = PageSizes;

  params_list: any = {
    size: 10,
    type: 'all',
    page: 1,
    name: ''
  };

  p: number = 1;
  total: number = 0;

  awdForm: FormGroup | any;
  ngOnInit(): void {

    this.route.params.subscribe(params => {
      this.profile_id = params['id'];
    });


    this.awdForm = this.formBuilder.group({
      wallet_id: ['0'],
      amount: ['', [Validators.required, Validators.min(1)]]
    });
  }

  pageChanged(page: number) {
    this.params_list = { ...this.params_list, page };
    this.transactionList();
  }

  filter(event: any) {
    this.p = 1;
    this.params_list = { ...this.params_list, page: this.p };
    this.transactionList();
  }

  resetFilter() {
    this.p = 1;
    this.params_list = { 
      ...this.params_list, 
      size: 10,
      page: 1,
      type: 'all',
      name:''
    }
    this.transactionList();
  }


  ngAfterViewInit(): void {
    this.profileService.getProfile().subscribe((res: any) => { this.admin = res.record; });
    this.walletDetail();
    this.transactionList();

  }

  walletDetail(){
    this.profileService.getWalletDetails(this.profile_id).subscribe((res: any) => {
      this.txn = res.record;

    });
  }
 
 
  transactionList(){
    this.profileService.getTransactionList(this.params_list).subscribe((res: any) => {
      this.txnList = res.record.data;
      this.total = res.record.count;
      
    });
  }

  get awd() { return this.awdForm.controls; }

  onDASubmit() {
    this.awdSubmitted = true;
    if (this.awdForm.invalid) return;

    this.awdLoader = true;
    
    this.params = {
      "target_wallete_id" : this.txn?.wallets?.id ?? 0,
      "transaction_amount" : this.awd.amount.value,
      "transaction_type" : 'deposit'
    }

    this.profileService.SuperAdminWalletDeposit(this.params).subscribe((res: any) => {
      toastr.success(res.message || 'Amount deposit successfully');
      this.walletDetail();
      this.transactionList();
      this.awdForm.reset();
      $('#amount').text('');
      $('#modal-ncb').modal('hide');
      this.awdSubmitted = false;
      this.awdLoader = false;
    }, err => {
      this.awdSubmitted = false;
      this.awdLoader = false;
    });

  }


  numberOnly(event:any): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
}
