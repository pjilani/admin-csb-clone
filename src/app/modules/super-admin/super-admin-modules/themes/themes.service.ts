import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Theme } from 'src/app/models';

@Injectable({ providedIn: 'root' })

export class ThemesService {

  constructor(private http: HttpClient) { }

  // Super Admin ===================================
  
  getAdminThemes() {
    return this.http.get(`admin/themes`);
  }
  
  // Super Admin ===================================

  getThemes() {
    return this.http.get(`super/admin/themes`);
  }

  getThemeById(id: number) {
    return this.http.get(`super/admin/themes/${id}`);
  }

  createTheme(theme: Theme) {
    return this.http.post(`super/admin/themes`, theme);
  }

  updateTheme(theme: Theme) {
    return this.http.post(`super/admin/themes/${theme.id}`, theme);
  }

  deleteTheme(id: number) {
    return this.http.delete(`super/admin/themes/${id}`);
  }

}
