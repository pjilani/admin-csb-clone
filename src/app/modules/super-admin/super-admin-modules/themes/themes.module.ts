import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { Routes, RouterModule } from '@angular/router';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { EditComponent } from './edit/edit.component';
import { SharedModule } from 'src/app/shared/shared.module';

const themesRoutes: Routes = [
  { path: '', component: ListComponent },
  { path: ':id', component: EditComponent }
]

@NgModule({
  declarations: [
    ListComponent, EditComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(themesRoutes),
    ComponentsModule,
    SharedModule
  ]
})
export class ThemesModule { }
