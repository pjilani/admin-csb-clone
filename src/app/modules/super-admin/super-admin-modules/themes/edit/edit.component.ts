import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Theme } from 'src/app/models';
import { Constants } from 'src/app/shared/constants';
import { ThemesService } from '../themes.service';

declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})

export class EditComponent implements OnInit {

  id: number = 0;
  theme!: Theme;

  intTheme: any = { ...Constants.INIT_THEME };
  
  themeForm: FormGroup | any;
  submitted: boolean = false;
  themeLoader: boolean = false;

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Themes', path: '/super-admin/themes' }
  ];

  constructor(private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private themeService: ThemesService) {
     this.id = this.route.snapshot.params['id'];
     
     let title: string = 'Create';

    this.themeForm = this.formBuilder.group({
      name: ['', [ Validators.required ]]
    });

    if(this.id > 0) {
      title = 'Edit';
      this.getTheme();
    }

  this.breadcrumbs.push({ title, path: `/super-admin/themes/${this.id}` });

  }

  ngOnInit(): void {  }

  getTheme() {
    this.themeService.getThemeById(this.id).subscribe((res: any) => {
      this.theme = res.record;
      this.intTheme = JSON.parse(this.theme.options);
      this.intTheme.name = 'Custom';
      this.themeForm.patchValue({
        name: this.theme.name
      });
    });
  }

  changeTheme(theme: any) {
    this.theme = theme;
  }

  get f() {
    return this.themeForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid

    if (this.themeForm.invalid) return;    

    this.themeLoader = true;
 
    const name = this.themeForm.value.name;
    // const options = { ...this.intTheme, name };
    let theme_option = (this.theme ? this.theme : this.intTheme)
    const options = { ...theme_option, name };

    const data: any = { options, name };
    
    if(this.id > 0) {
      data.id = this.id;

      this.themeService.updateTheme(data)
      .subscribe((response: any) => {
        toastr.success(response?.message || 'Theme Updated Successfully!');
        this.router.navigate(['/super-admin/themes']);
      }, (err: any) => {
        this.themeLoader = false; 
      });

    } else {

      this.themeService.createTheme(data)
      .subscribe((response: any) => {
        toastr.success(response?.message || 'Theme Created Successfully!');
        this.router.navigate(['/super-admin/themes']);
      }, (err: any) => {
        this.themeLoader = false; 
      });

    }
  }

}
