import { Component, OnInit } from '@angular/core';
import { Theme } from 'src/app/models';
import { ThemesService } from '../themes.service';
import Swal from 'sweetalert2';
declare const toastr: any;

@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {

  p: number = 1;

  themes: Theme[] = [];

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Themes', path: '/super-admin/themes' },
  ];

  constructor(private themeService: ThemesService) { }

  ngOnInit(): void {
    this.getThemes();
  }

  getThemes() {
    this.themeService.getThemes().subscribe((res: any) => {
      this.themes = res.record;
    });
  }

  delete(theme: Theme) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to delete ${theme.name}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, delete it!`
    }).then((result) => {
      if (result.isConfirmed) {

        this.themeService.deleteTheme(theme.id).subscribe((res: any) => {
          toastr.success( res.message || 'Currency Deleted Successfully.');
          this.getThemes();
        });
        
      }
    });
    
  }

}
