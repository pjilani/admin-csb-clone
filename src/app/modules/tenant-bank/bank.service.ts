import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })

export class BankService {

 

  constructor(private http: HttpClient) { }

  
  // ====================== Admin url ==============================

  
  getAllBank(params: any = []) {
    return this.http.get(`admin/bank/list`, {params: params});
  }

  createBank(data: any) {
    return this.http.post(`admin/bank/add`, data);
  }

  updateBankStatus(data:any){
    return this.http.post(`admin/bank/status`, data);
      
  }


}
