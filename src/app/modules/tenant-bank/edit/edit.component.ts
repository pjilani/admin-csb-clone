import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Currency, Language, Layout, Admin, Theme } from 'src/app/models';
import { Constants } from 'src/app/shared/constants';
import { CustomValidators } from 'src/app/shared/validators';
import { BankService } from '../bank.service';
declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})

export class EditComponent implements OnInit {
  
  
  id: number = 0;
  bank:any = [];
  bankForm: FormGroup | any;
  submitted: boolean = false;
  bankLoader: boolean = false;
  title:any = '';
 

  constructor(private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private bankService: BankService,
  ) {
    
    this.id = this.route.snapshot.params['id'];
    this.bankForm = this.formBuilder.group({
      bank_name: ['', [ Validators.required ]],
      bank_ifsc_code: ['', [ Validators.required ]],
      account_holder_name: ['', [ Validators.required ]],
      account_number: ['', [ Validators.required ]],
      active: [''],
      id: ['']
    });

    this.title = 'Add New Bank';
    
    if(this.id > 0) {
      this.title = "Edit Bank Details";
    }
    let page_title = this.title; 

    this.breadcrumbs.push({ page_title, path: `/bank/${this.id}` });

  }


  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Bank', path: '/bank/list' }
  ];

  ngOnInit(): void { 
    if(this.id > 0)
    this.getBankById(this.id);
    
  }


  get f() {
    return this.bankForm.controls;
  }
 

  getBankById(id:any) {
    this.bankService.getAllBank({'id':id}).subscribe((res: any) => {
      this.bank = res.record.list[0];

      this.bankForm.patchValue({
        bank_name: this.bank.bank_name,
        bank_ifsc_code: this.bank.bank_ifsc_code,
        account_number: this.bank.account_number,
        account_holder_name: this.bank.account_holder_name,
        active: this.bank.status,
        id: this.bank.id
      });
      
    });
  }

  removeSpace(event:any){
    var k;  
    k = event.charCode;
    return(((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 45 || k== 95) && event.keyCode != 32 ? event:event.preventDefault());

  }
 
  onSubmit() {
    this.submitted = true;

    if (this.bankForm.invalid){
      return;
    } 
    this.bankLoader = true;
    
    const data = this.bankForm.value;
      this.bankService.createBank(data)
      .subscribe((response: any) => {
        if(this.id > 0) {
          toastr.success(response?.message || 'Bank Detail Updated Successfully!');
        }else{
          toastr.success(response?.message || 'New Bank Created Successfully!');
        }
        this.router.navigate(['/bank/list']);
        this.bankLoader = false; 
      }, (err: any) => {
        this.bankLoader = false; 
      });
  
  }

}
