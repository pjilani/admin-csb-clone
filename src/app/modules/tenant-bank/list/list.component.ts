import { Component, OnInit } from '@angular/core';
import { BankService } from '../bank.service';
import { PageSizes } from 'src/app/shared/constants';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
declare const toastr: any;
import Swal from 'sweetalert2';
import { PermissionService } from 'src/app/services/permission.service';
@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {

  total: number = 0;
  list: any[] = [];

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Bank', path: '/bank/list' },
  ];

  constructor(private bankService: BankService,public superAdminAuthService: SuperAdminAuthService, public permissionService: PermissionService) { }

  ngOnInit(): void {
    this.getList();
  }



  getList() {
    this.bankService.getAllBank().subscribe((res: any) => {
      this.list = res.record.list;
      this.total = res.record.total;
    });
  }


  updateBankStatus(id: any, status: boolean) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to `+ (status == true ? 'Activate ' : 'Deactivate') +` it!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, `+ (status == true ? 'Activate ' : 'Deactivate') +` it!`
    }).then((result) => {
      if (result.isConfirmed) {

        this.bankService.updateBankStatus({ id: id, status :status }).subscribe((res: any) => {
          toastr.success(res.message || 'Bank list updated successfully' );
          this.getList();
          ;
        });
        
      }

    });
  }
 

}
