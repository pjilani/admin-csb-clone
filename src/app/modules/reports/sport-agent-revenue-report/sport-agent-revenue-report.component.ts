import { Component, OnInit, OnDestroy } from '@angular/core';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
import { ReportService } from '../reports.service';
import { PageSizes, TIMEZONE } from 'src/app/shared/constants';
import { AdminAuthService } from '../../admin/services/admin-auth.service';
import {Currency} from 'src/app/models';
import { PermissionService } from 'src/app/services/permission.service';

declare const $: any;

@Component({
  templateUrl: './sport-agent-revenue-report.component.html'
})
  
export class SportAgentRevenueReportComponent implements OnInit, OnDestroy {
  
  repLoader: boolean = true;
  firstTimeApiCall = false;
  dataTableParams: any = {
    searching: false,
    info: true,
    lengthChange: true,
    autoWidth: false,
    responsive: true,
    "dom": '<"top"lp>t<"bottom"ifp><"clear">',
    lengthMenu: PageSizes.map(m => m.name),
    buttons: []
  };

  totalGGR: number = 0;
  totalNGR: number = 0;
  totalRevenue: number = 0;
  gameProvider: any[] = [];
  gameType: any[] = [];
  pageSizes: any[] = PageSizes;
  TIMEZONE: any[] = TIMEZONE;
  zone: any = '(GMT +00:00) UTC';
  
  reports: any[] = [];
  allReports: any[] = [];
  tableList: any[] = [];
  gameList: any[] = [];
  provideList: any[] = [];
  isProvide:boolean=false;
  isGame:boolean=false;
  isTable:boolean=false;
  p: number = 1;
  currencies: Currency[] = [];
  format: string = "YYYY-MM-DD";
  userList :any[]=[];
  tenantsList :any[]=[];
  tenantsOwnerList :any[]=[];
  total: number = 0;
  pageNextTotal: number = 0;
  pageCurrentTotal: number = 0;
  tip: number = 0;
  tipCount: number = 0;
  customDate=false;
  params: any = {
    size: 10,
    page: 1,
    search: '',
    agent_id: '',
    currency: '',
    player_type: 'all',
    isDirectPlayer: '',
    time_zone: 'UTC +00:00',
    time_zone_name: 'UTC +00:00',
    tenant_id: '',
    owner_id: '',
    action_type: '',
    time_type: 'today',
    time_period: [],
    game_table:'',
    game_type:'',
    game_provider:'',
    // order: 'asc',
    // sort_by: 'agent_name'
            
  };

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Sport Agent Revenue Report', path: '/reports/sport-agent-revenue-report' },
  ];

  constructor(private ReportService: ReportService,
              private adminAuthService: AdminAuthService,
    public superAdminAuthService: SuperAdminAuthService,
    public permissionService: PermissionService) {
    this.params = { ...this.params, ...this.ReportService.agentSportRevenueParams };
  }

  ngOnInit(): void {

    if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.getTenantList();
      this.dataTableParams.buttons = [{ extend: 'csv', text: 'Export as CSV', className: 'ctsmcsv mgt btn btn-info mt-0' }];
    } else {
      // this.getReport();
      this.getCurrencyList();
      this.adminAuthService.adminPermissions.subscribe(res => {
        if(this.permissionService.checkPermission('sport_agent_revenue_report','export')){
          this.dataTableParams.buttons = [{ extend: 'csv', text: 'Export as CSV', className: 'ctsmcsv mgt btn btn-info mt-0' }];
        }
      });
    }

    this.getAgentUserList();
    this.pageNextTotal = this.params.size;
    this.pageCurrentTotal = this.p;
  }

  getAgentUserList(){

    if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.ReportService.getSuperAdminUserList({"owner_id":this.params.owner_id,"tenant_id":this.params.tenant_id}).subscribe((res: any) => {
        this.userList = res.record;
      });
    } else {
      this.ReportService.getAdminUserList().subscribe((res: any) => {
        this.userList = res.record;
      });
    }

  }

  getTenantList(){

    if(this.superAdminAuthService.superAdminTokenValue &&
        this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.ReportService.getSuperTenantsList().subscribe((res: any) => {
        this.tenantsList = res.record;
        if (this.tenantsList.length > 0) {
            this.params.tenant_id = this.tenantsList[0].id;
            this.getTenantOwnerList(this.tenantsList[0].id);
            this.getCurrencyList(this.tenantsList[0].id);
        }
      });
    }
  }

  getTenantOwnerList(id:number){

    if(this.superAdminAuthService.superAdminTokenValue &&
        this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.ReportService.getSuperTenantsOwnerList({id}).subscribe((res: any) => {
        this.tenantsOwnerList = res.record;
        this.params.owner_id = this.tenantsOwnerList.length > 0 ? this.tenantsOwnerList[0].id : '';
        // this.getReport();
        $('#owner_id option:eq(0)').prop('selected',true);
      });
    }
  }

  tenantFilter(evnt: any) {
    // this.p = 1;
    // this.params.page = this.p;
    this.params.tenant_id = evnt;
    this.getTenantOwnerList(evnt);
    this.getCurrencyList(evnt);
  }

  getCurrencyList(evnt=''){
    if(this.superAdminAuthService.superAdminTokenValue &&
        this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.ReportService.getCurrencyList(evnt).subscribe((res: any) => {
        this.currencies = res.record;
      });

    }else {

      this.ReportService.getCurrencyList().subscribe((res: any) => {
        this.currencies = res.record;
      });
    }
  }

  get getParam(){
    return this.params;
  }
  getReport() {
    this.repLoader = false;
    this.firstTimeApiCall = true;
    $('.ctsmcsv').remove();

    if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.ReportService.getSuperAdminAgentSportRevenueReport(this.getParam).subscribe((res: any) => {
        this. getPrepareRecord(res);
      }, err => this.repLoader = true );
    } else {
      this.ReportService.getAdminAgentSportRevenueReport(this.getParam).subscribe((res: any) => {
        this. getPrepareRecord(res);
      }, err => this.repLoader = true );
    }
  }

  getPrepareRecord(res: any) {
    
    this.allReports = res.record.resultPrepare;
    this.reports = this.allReports;

    // if(res.record.resultGameProvider)
    //   this.gameType = res.record.resultGameProvider.game_provider.buckets;

    // if(res.record.resultGameProvider)
    //   this.gameProvider = res.record.resultGameProvider.game_type.buckets;

    // console.log(this.allReports);

    this.total = res.record.length;
    // this.tip = res.record.aggregations.total_tip_amount.value;
    // this.tipCount = res.record.aggregations.total_tip_count.doc_count;

    this.repLoader = true;

    this.calsTotals();

    let limit = this.params.size;
    let offset = 1;
    if (this.p > 1) {
      offset = limit * (this.p - 1);
    }
    this.pageCurrentTotal = offset;

    this.pageNextTotal = (offset>1?offset:0) + parseInt(limit);
    if(this.total < this.pageNextTotal){
      this.pageNextTotal = this.total;
    }

  }

  calsTotals() {
    this.totalGGR = this.reports.map(m => m.ggr_in_EUR.value).reduce((a, b) => a + b, 0);
    this.totalNGR = this.reports.map(m => m.ngr_in_EUR.value).reduce((a, b) => a + b, 0);
    this.totalRevenue = this.reports.map(m => {

      const commission: number = 100 - parseFloat(m.key.split('-')[5]);
      return ( m.ngr_in_EUR.value  *  commission ) / 100;

    } ).reduce((a, b) => a + b, 0);
  }

  getRev(transaction: any) {
    const commission: number = 100 - parseFloat(transaction.key.split('-')[5]);
    return ( transaction.ngr.value * commission ) / 100;
  }

  getRevBase(transaction: any) {
    const commission: number = 100 - parseFloat(transaction.key.split('-')[5]);
    return ( transaction.ngr_in_EUR.value * commission ) / 100;
  }

  filter(evt: any,dateFilterType:boolean=false) {

    if(this.params.time_type=='custom'){
      this.customDate=true;
      if(dateFilterType){
        setTimeout(() => {    
          $('#time_period').val('');
        }, 10);
      }
    }else{
      this.customDate=false;
    }

  }

  submitFilter(){
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getReport();
  }

  filterSelectAgent(evt: any) {
    if(evt!=0) {


      // this.p = 1;
         this.params = {...this.params, agent_id:evt};

      //  if(evt)
      //  this.getReport();

    }

  }

  selectAgent(agent_id: any) {

    // this.p = 1;
    this.params = {...this.params, agent_id};

    if (agent_id) {
      // this.getReport();
      setTimeout(() => {
        $('#agent_id').val(agent_id).trigger('change');
      }, 0);
    }
  }

  selectDateRange(time_period: any) {
    this.params = {...this.params, time_period}
    // this.getReport();
  }

  filterSelectOwner(evt: any) {
    this.params.owner_id = evt;
    // this.p = 1;
    this.params = {...this.params, owner_id:evt};
    // this.getReport();
  }

  filterSelectTimeZone(evt: any) {
    // this.p = 1;
    let timeZoneValue='';
    if(evt){
      timeZoneValue=TIMEZONE.filter(element => element.value === evt)[0].zonename;
    }
    if(timeZoneValue) {
      this.zone=TIMEZONE.filter(element => element.value === evt)[0].name;
      this.params = {...this.params, time_zone:evt, time_zone_name: timeZoneValue};
    }else{
      this.zone='';
      this.params = {...this.params, time_zone:evt};
    }

    // if(evt)
    // this.getReport();
  }

  pageChanged(page: number) {
    // this.params = { ...this.params, page };
    // this.getReport();
  }

    resetFilter() {

        this.p = 1;
        this.params = {
            size: 10,
            page: 1,
            search: '',
            agent_id: '',
            currency: '',
            isDirectPlayer: '',
            time_zone: this.params.time_zone,
            time_zone_name: this.params.time_zone_name,
            tenant_id: this.params.tenant_id,
            owner_id: '',
            time_type: 'today',
            game_table:'',
            game_type:'',
            provider:''
            // order: 'asc',
            // sort_by: 'agent_name'
    
        };

        this.getReport();
        $( "#agent_id" ).val('').trigger('change');


        this.repLoader = false;
        $('.ctsmcsv').remove();

        this.reports = this.allReports.filter(f => f);
        setTimeout(() => {
          this.repLoader = true;
        }, 0);

      // setTimeout(() => {
      //   $( "#time_zone" ).val('UTC +00:00').trigger('change');
      // }, 5);

    }
    download(){
        let timeZoneValue='';
        if(this.params.time_zone){
            timeZoneValue=TIMEZONE.filter(element => element.value === this.params.time_zone)[0].zonename;
        }

        if(timeZoneValue) {
            this.params = {...this.params, time_zone_name: timeZoneValue};
        }

        if (this.superAdminAuthService.superAdminTokenValue &&
            this.superAdminAuthService.superAdminTokenValue.length > 0) {

            this.ReportService.getDownloadSuperAgentRevenueReport(this.params).subscribe((res: any) => {
                // window.open(res.record.url);
                window.location.href = res.record.url;

            }), (error: any) => //console.log('Error downloading the file'), //when you use stricter type checking
                () => console.info('File downloaded successfully');

        } else {

            this.ReportService.getDownloadAgentRevenueReport(this.params).subscribe((res: any) => {
                // window.open(res.record.url);
                window.location.href = res.record.url;

            }), (error: any) => //console.log('Error downloading the file'), //when you use stricter type checking
                () => console.info('File downloaded successfully');
        }
    }

  setOrder(sort: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p, order: sort.order, sort_by: sort.column };
    this.getReport();
  }

  // currencyFilter(c: any) {
    // this.repLoader = false;
    // $('.ctsmcsv').remove();
    
    // this.reports = this.allReports.filter(f => f.key.includes(c.value));
    // setTimeout(() => {
    //   this.repLoader = true;
    // },0);

    // this.calsTotals();
  // }
    
  ngOnDestroy(): void {
    this.ReportService.agentSportRevenueParams = { ...this.ReportService.agentSportRevenueParams, ...this.params };
  }

  
}
