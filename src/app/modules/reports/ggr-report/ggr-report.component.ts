import { Component, OnInit } from '@angular/core';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
import { ReportService } from '../reports.service';
import { PageSizes, TIMEZONE } from 'src/app/shared/constants';
import { AdminAuthService } from '../../admin/services/admin-auth.service';
import { Currency, Role } from 'src/app/models';
import * as moment from 'moment-timezone';
import { PermissionService } from 'src/app/services/permission.service';
declare const $: any;

@Component({
  selector: 'app-ggr-report',
  templateUrl: './ggr-report.component.html',
  styleUrls: ['./ggr-report.component.scss']
})
export class GgrReportComponent implements OnInit {
  roles: string = localStorage.getItem('roles') || '';
  isAgent: boolean = false;
  TIMEZONE: any[] = TIMEZONE;
  zone: any = '(GMT +00:00) UTC';
  reports: any[] = [];
  format: string = "YYYY-MM-DD HH:mm:ss";
  userList: any[] = [];
  tenantsList: any[] = [];
  total: number = 0;
  customDate = false;
  currencies: Currency[] = [];
  months = [
    'January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'
  ];
  years: number[] = [];
  p: number = 1;
  params: any = {
    size: 10,
    page: 1,
    agent_id: '',
    time_zone: 'UTC +00:00',
    time_zone_name: 'UTC +00:00',
    currency: '',
    tenant_id: '',
    time_type: 'today',
    time_period: '',
    datetime: {},
    month: '',
    year: ''
  };
  pageSizes = PageSizes;
  hideShow: boolean = true;
  isLoader: boolean = false;
  allReports: any[] = [];
  maxSelectableDateTime!: any;
  dateLimit:number = 30

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Ggr Report', path: '/reports/ggr-report' },
  ];

  constructor(private ReportService: ReportService,
    private adminAuthService: AdminAuthService,
    public superAdminAuthService: SuperAdminAuthService,
    public permissionService: PermissionService) {
    const currentYear = new Date().getFullYear();
    const startYear = currentYear - 1;
    for (let year = startYear; year <= currentYear; year++) {
      this.years.push(year);
    }
  }

  ngOnInit(): void {
    const currentDate = new Date();
    this.maxSelectableDateTime = currentDate;

    if (this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.getTenantList();
      this.hideShow = false;
    } else {
      this.getCurrencyList();
      this.getAgentUserList();

      // this.getReport();
    }
    if (this.adminAuthService.adminTokenValue && this.adminAuthService.adminTokenValue.length > 0) {
      this.adminAuthService.adminUser.subscribe((user: any) => {
        if (user && user.id) {
          this.params = { ...this.params, time_zone: user.timezone }
          setTimeout(() => {
            const _zone = TIMEZONE.find(t => t.zonename == user.timezone);
            $(`#time_zone`).val(_zone?.zonename).trigger('change');
          }, 100);
        }
      });
      if (this.roles) {
        const roles = JSON.parse(this.roles);
        if (roles && roles.findIndex((role: any) => role === Role.Admin) == -1 && roles.findIndex((role: any) => role === Role.Agent) > -1) {
          this.isAgent = true;
        }

      }

    }

  }

  onMonthChange(event: any) {
    if (event && event.length > 0) { 
      this.params.month = event;
    }
    // this.params.month = event.target.value;
  }

  onYearChange(event: any) {
    this.params.year = event.target.value;
  }

  getCurrencyList(evnt = '') {
    if (this.superAdminAuthService.superAdminTokenValue &&
      this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.ReportService.getCurrencyList(evnt).subscribe((res: any) => {
        this.currencies = res.record;        
        if(!this.currencies.some(element => element.code === this.params.currency)){
          this.params.currency = '';
        }
      });

    } else {

      this.ReportService.getCurrencyList().subscribe((res: any) => {
        this.currencies = res.record;
      });
    }
  }

  getTenantList() {

    if (this.superAdminAuthService.superAdminTokenValue &&
      this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.ReportService.getSuperTenantsList().subscribe((res: any) => {
        this.tenantsList = res.record;
        if (this.tenantsList.length > 0) {
          this.params.tenant_id = 0;
          this.getCurrencyList('0');
        }
      });
    }
  }

  tenantFilter(evnt: any) {
    this.params.tenant_id = parseInt(evnt);
    this.getCurrencyList(evnt);
    if (evnt != 0) {
      this.hideShow = true;
      this.getAgentUserList();

    }
    else {
      this.hideShow = false;
      this.userList = [];
    }
  }
  filterSelectAgent(evt: any) {
    this.params = { ...this.params, agent_id: evt };
  }
  getAgentUserList() {

    if (this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.ReportService.getSuperAdminUserList({ "tenant_id": this.params.tenant_id }).subscribe((res: any) => {
        this.userList = res.record;
      });
    } else {
      this.ReportService.getAdminUserList().subscribe((res: any) => {
        this.userList = res.record;
      });
    }

  }

  convertToUTC(inputDate: string, inputTimezone: string): string {
    const date = moment.tz(inputDate, inputTimezone);
    const utcDate = date.utc().format('YYYY-MM-DD HH:mm:ss');
    return utcDate;
  }

  getReport() {
    this.isLoader = true;

    if (
      this.params.time_period?.start_date &&
      this.params.time_period?.end_date &&
      this.params.time_zone_name &&
      this.params.time_zone_name !== 'UTC +00:00') {
      this.params = {
        ...this.params,
        datetime: {
          start_date: this.convertToUTC(this.params.time_period.start_date, this.params.time_zone_name),
          end_date: this.convertToUTC(this.params.time_period.end_date, this.params.time_zone_name)
        }
      }
    } else {
      this.params = {
        ...this.params, datetime: {
          start_date: this.params.time_period?.start_date,
          end_date: this.params.time_period?.end_date
        }
      }
    }
    if (this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.ReportService.getSuperAdminGgrReport(this.params).subscribe((res: any) => {
        this.getPrepareRecord(res);
      }, (error: any) => {
        this.isLoader = false

      });
    } else {
      this.ReportService.getAdminGgrReport(this.params).subscribe((res: any) => {
        this.getPrepareRecord(res);
      }, (error: any) => {
        this.isLoader = false

      });
    }
  }

  getPrepareRecord(res: any) {
    this.reports = res.record.data;
      this.total = res.record.total;
    this.isLoader = false
  }

  filter(evt: any, dateFilterType: boolean = false) {

    if (this.params.time_type == 'custom') {
      this.customDate = true;
      if (!dateFilterType)
        this.getReport();
    } else {
      this.customDate = false;
    }

  }

  submitFilter() {
    this.p = 1;
    this.params = { ...this.params };
    this.getReport();
  }

  selectDateRange(time_period: any) {
    this.p = 1
    this.params = { ...this.params, time_period }
    this.getReport();
  }

  filterSelectTimeZone(zonename: any) {
    this.p = 1;
    if (zonename) {
      const zone = TIMEZONE.find(t => t.zonename === zonename);
      this.zone = zone?.name;
      this.params = { ...this.params, page: this.p, time_zone: zone?.value, time_zone_name: zone?.zonename };
    } else {
      this.zone = '';
      this.params = { ...this.params, page: this.p, time_zone: this.zone };
    }

    // this.getReport();
  }


  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getReport();
  }
  
  isFutureMonth(monthIndex: number) {
    const currentYear = new Date().getFullYear();
    const currentMonthIndex = new Date().getMonth();
  
    if (this.params.year.toString()  === currentYear.toString()) {
      return monthIndex > currentMonthIndex;
    } else {
      return false;
    }
  }

  resetFilter() {
    this.p = 1;
    this.params = {
      size: 10,
      page: 1,
      agent_id: '',
      time_type: 'today',
      time_zone: this.params.time_zone,
      currency: '',
      time_zone_name: this.params.time_zone_name,
      tenant_id: this.params.tenant_id,
      datetime: {},
      month: '',
      year: ''
    };

    this.getReport();
    $("#agent_id").val('').trigger('change');
    // $("#time_zone").val('UTC +00:00').trigger('change');
  }

  download() {
    let timeZoneValue = '';
    if (this.params.time_zone) {
      timeZoneValue = TIMEZONE.filter(element => element.value === this.params.time_zone)[0].zonename;
    }

    if (timeZoneValue) {
      this.params = { ...this.params, time_zone_name: timeZoneValue };
    }
    if (
      this.params.time_period?.start_date &&
      this.params.time_period?.end_date &&
      this.params.time_zone_name &&
      this.params.time_zone_name !== 'UTC +00:00') {
      this.params = {
        ...this.params,
        datetime: {
          start_date: this.convertToUTC(this.params.time_period.start_date, this.params.time_zone_name),
          end_date: this.convertToUTC(this.params.time_period.end_date, this.params.time_zone_name)
        }
      }
    } else {
      this.params = {
        ...this.params, datetime: {
          start_date: this.params.time_period?.start_date,
          end_date: this.params.time_period?.end_date
        }
      }
    }
    if (this.superAdminAuthService.superAdminTokenValue &&
      this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.ReportService.getDownloadSuperGgrReport(this.params).subscribe((res: any) => {
        // window.open(res.record.url);
        window.location.href = res.record.url;

      });
    } else {

      this.ReportService.getDownloadGgrReport(this.params).subscribe((res: any) => {
        // window.open(res.record.url);
        window.location.href = res.record.url;

      });
    }
  }
}
