import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerRevenueReportComponent } from './player-revenue-report.component';

describe('PlayerRevenueReportComponent', () => {
  let component: PlayerRevenueReportComponent;
  let fixture: ComponentFixture<PlayerRevenueReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlayerRevenueReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerRevenueReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
