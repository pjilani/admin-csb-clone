import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild } from '@angular/core';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
import { ReportService } from '../reports.service';
import { PageSizes } from 'src/app/shared/constants';
import { SportService } from '../../sports/sport.service';
import { TenantService } from '../../tenant/tenant.service';
import { environment } from 'src/environments/environment';
import { Role } from 'src/app/models';
import { AdminAuthService } from '../../admin/services/admin-auth.service';
import { PermissionService } from 'src/app/services/permission.service';
import { generateApiUrl } from 'src/app/services/utils.service';
declare const $: any;

@Component({
  templateUrl: './bet-report.component.html',
  styleUrls: ['./bet-report.component.scss']
})

export class BetCloneReportComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('f') form!: any;
  isSubAgentModuleAllowed:boolean = true;
  roles: string = localStorage.getItem('roles') || '';
  isAgent:boolean = false;
  searchLeaguesUrl: string = '';
  possible_win_sum : number  =0;
  stake_sum : number  =0;
  selectedCoulumn: any;

  sports: any[] = [];
  userList: any[] = [];
  countries: any[] = [];
  leagues: any[] = [];
  matches: any[] = [];

  allTenants: any[] = [];

  allStatus = [
    { value: -2, name: "In Game" },
    { value: -1, name: "Cancelled" },
    { value: 1, name: "Lost" },
    { value: 2, name: "Won" },
    { value: 3, name: "Refund" },
    { value: 4, name: "Halflost" },
    { value: 5, name: "Halfwon" }
  ]
  
  pageSizes = PageSizes;
  
  reports: any[] = [];

  report: any;
  

  p: number = 1;

  format: string = "YYYY-MM-DD HH:mm";

  total: number = 0;
  pageNextTotal: number = 0;
  pageCurrentTotal: number = 0;
  params: any = {
    size: PageSizes[0].name,
    page: 1,
    agent_id:'',
    search: '',
    start_date: '',
    end_date: '',
    bet_type: 'not_combo',
    sport_id: '',
    country: '',
    tournament_id: '',
    match_id: '',
    stake_min_amt: '',
    stake_max_amt: '',
    winning_min_amt: '',
    winning_max_amt: '',
    tenant_id: '',
    status: '',
    sort_by: 'created_at',
    order: 'asc'
  };

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Bet Report', path: '/reports/bet-report' },
  ];

  constructor(public reportService: ReportService,
    // private excelService: ExcelService,
    private sportService: SportService,
    private tenantService: TenantService,
    private adminAuthService: AdminAuthService,
    public superAdminAuthService: SuperAdminAuthService,
    public permissionService: PermissionService) { }

  ngOnInit(): void {
    this.params = { ...this.params , ...this.reportService.betReportParams }
    this.getAllSport();
    this.getAllCountries();
    // this.getAllLeagues();
    this.pageNextTotal = this.params.size;
    this.pageCurrentTotal = this.p;
    if (this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.getTenantsAll();
    } else {
      // this.getReport();
      this.getAgentUserList();
      this.searchLeaguesUrl = generateApiUrl() + `admin/sports/leagues/search`;
    }


    if (this.adminAuthService.adminTokenValue && this.adminAuthService.adminTokenValue.length > 0) {

      if(this.roles) {
        const roles = JSON.parse(this.roles);
        if(roles && roles.findIndex( (role: any) => role === Role.Admin ) == -1 && roles.findIndex( (role: any) => role === Role.Agent ) > -1) {
          this.isAgent = true;
        }

      }

    }

    //console.log('created');

  }

  ngAfterViewInit(): void {
    const that = this;

    setTimeout(() => {

    // console.log('params ', this.reportService.betReportParams);

      // $("#tenant_id").val(this.params.tenant_id).trigger('change');
      // $("#sport_id").val(this.params.sport_id).trigger('change');
      // $("#country").val(this.params.country).trigger('change');
      // $("#tournament_id").val(this.params.tournament_id).trigger('change');
      // $("#match_id").val(this.params.match_id).trigger('change');
      
      if (this.params.start_date && this.params.end_date) {
        $('#filter_date').daterangepicker({
         startDate: new Date(this.params.start_date),
          endDate: new Date(this.params.end_date),
          locale: {
            format: this.format
          }
        }, function(start: any, end: any) {
          const start_date = start.format(that.format);
          const end_date = end.format(that.format);
          that.selectDateRange({ start_date, end_date });
        });
        // $('#filter_date').val({ startDate: this.params.start_date, endDate: this.params.end_date });
      } else {
        $('#filter_date').val('');
      }

    }, 50);
  }

  getTenantsAll() {
    this.tenantService.getTenantsAll().subscribe((res: any) => {
      this.allTenants = res.record;
      if (this.allTenants && this.allTenants.length > 0) {
        this.tenantFilter(this.allTenants[0].id);
        this.searchLeaguesUrl = generateApiUrl() + `super/admin/sports/leagues/search?tenant_id=${this.allTenants[0].id}`;
      }
    });
  }

  getAgentUserList(){

    if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.reportService.getSuperAdminAllAgentList({"tenant_id":this.params.tenant_id}).subscribe((res: any) => {
        this.userList = res.record;
      });
    } else {
      this.reportService.getAdminUserList().subscribe((res: any) => {
        this.userList = res.record;
      });
    }

  }

  /**
   *
   * @param evt
   * @returns {any}
   */
  filterSelectAgent(evt: any) {
    if (evt != 0) {

      // this.p = 1;
      this.params = {...this.params, agent_id: evt};

      // if (evt)
      //   this.getReport();
    }
  }
  selectReport(report: any) {
    this.report = report;
  }

  getReport() {
    
    const that = this;
    this.reportService.getBetReports(this.params).subscribe((res: any) => {

      this.reports = res.record.data;
      this.total = res.record.total;
      this.stake_sum = res.record.stake_sum;
      this.possible_win_sum = res.record.possible_win_sum;

    });

    let limit = this.params.size;
    let offset = 1;
    if (this.p > 1) {
      offset = limit * (this.p - 1);
    }
    this.pageCurrentTotal = offset;

    this.pageNextTotal = (offset>1?offset:0) + parseInt(limit);
    if(this.total < this.pageNextTotal){
      this.pageNextTotal = this.total;
    }
  }

  setOrder(sort: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p, order: sort.order, sort_by: sort.column };
    this.getReport();
  }

  getAllSport() {
    this.sportService.getAdminAllSports().subscribe((res: any) => {
      this.sports = res.record;
    });  
  }

  getAllCountries() {
    this.sportService.getAdminAllCountries().subscribe((res: any) => {
      this.countries = res.record;
    });
  }

  // getAllLeagues(tenant_id?: any) {
  //   this.sportService.getAdminAllLeagues({ tenant_id: tenant_id ? tenant_id : '' }).subscribe((res: any) => {
  //     this.leagues = res.record;
  //   });
  // }

  filter(evt: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getReport();
  }

  formValidFilter(evt: any) {
    if (this.form.valid) {
      // this.getReport();
    }
  }

  tenantFilter(tenant_id: any) {
    // this.p = 1;
    this.params = { ...this.params, tenant_id };
    if (tenant_id) { 
      // this.getAllLeagues(tenant_id);
      this.getAgentUserList();
      // setTimeout(() => {
      //   this.getReport();
      // }, 30);
    }
  }
  
  sportFilter(sport_id: any) {
    // this.p = 1;
    this.params = { ...this.params, sport_id };
    // if (sport_id) { 
    //   this.getReport(); 
    // }
  }

  selectCountry(country: any) {
    // this.p = 1;
    this.params = { ...this.params, country };
    // if (country) { 
    //   this.getReport();
    // }
  }

  selectTournament(league_id: any) {
    if(league_id) {
      this.params = { ...this.params, tournament_id: league_id };
      this.sportService.getAdminAllMatches({ league_id, tenant_id: this.params.tenant_id }).subscribe((res: any) => {
        this.matches = res.record;
      });
    }
  }

  selectMatch(match_id: any) {
    // this.p = 1;
    this.params = { ...this.params, match_id };
    // if (match_id) {  
    //   this.getReport();
    // }
  }

  selectDateRange(dates: any) {
    // this.p = 1;
    this.params = { ...this.params, start_date: dates.start_date, end_date: dates.end_date };
    // if (dates.start_date && dates.end_date) { 
    //   this.getReport();
    // }
  }

  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getReport();
  }

  resetFilter() {
    
    setTimeout(() => {
      $("#tenant_id").val(this.allTenants.length > 0 ? this.allTenants[0].id : '').trigger('change');
      $("#sport_id").val("").trigger('change');
      $("#country").val("").trigger('change');
      $("#tournament_id").val("").trigger('change');
      $("#match_id").val("").trigger('change');
      $('#filter_date').val('');
    }, 100);

    this.matches = [];

    this.p = 1;
    this.params = {
      size: 10,
      page: 1,

      search: '',
      start_date: '',
      end_date: '',
      bet_type: 'not_combo',
      sport_id: '',
      country: '',
      tournament_id: '',
      match_id: '',
      stake_min_amt: '',
      stake_max_amt: '',
      winning_min_amt: '',
      winning_max_amt: '',
      tenant_id: this.allTenants.length > 0 ? this.allTenants[0].id : '',
      status: '',
      sort_by: 'created_at',
      order: 'asc'
    };

    this.getReport();
  }

  exportAsXLSX() {
    this.reportService.getAllBetReports(this.params).subscribe(
      (res: any) => {
        window.location.href = res.record.url;
      // this.excelService.exportAsExcelFile(res.record, 'report');
    });
  }

  winningPrice(price: string, stake: string) {
    if(price && stake) {
      return (parseFloat(price) * parseFloat(stake)).toFixed(2);
    } else {
      return 'NA';
    }
  }

  getRevenue(report:any){
    return (parseFloat(report?.betslip?.stake) - parseFloat(report?.betslip?.possible_win_amount));
  }

  setBetType(bet_type: string) {
    // this.p = 1;

    this.params = { ...this.params, bet_type };
    // this.getReport();
  }

  ngOnDestroy(): void {
    this.reportService.betReportParams = this.params;

    // console.log('destroyed');

  }


}
