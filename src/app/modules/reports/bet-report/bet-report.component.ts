import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild } from '@angular/core';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
import { ReportService } from '../reports.service';
import { PageSizes, TIMEZONE } from 'src/app/shared/constants';
import { SportService } from './../../sports/sport.service';
import { TenantService } from '../../tenant/tenant.service';
import { environment } from 'src/environments/environment';
import { AdminAuthService } from '../../admin/services/admin-auth.service';
import { generateApiUrl } from 'src/app/services/utils.service';
import { PermissionService } from 'src/app/services/permission.service';

declare const $: any;

@Component({
  selector: 'app-bet-report',
  templateUrl: './bet-report.component.html',
  styleUrls: ['./bet-report.component.scss']
})

export class BetReportComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('f') form!: any;

  searchLeaguesUrl: string = '';
  possible_win_sum : number  =0;
  stake_sum : number  =0;
  selectedCoulumn: any;
  TIMEZONE: any[] = TIMEZONE;
  sports: any[] = [];
  userList: any[] = [];
  countries: any[] = [];
  leagues: any[] = [];
  matches: any[] = [];
  zone: any = '(GMT +00:00) UTC';
  allTenants: any[] = [];

  allStatus = [
    { value: -1, name: "Cancelled" },
    { value: -2, name: "In Game" },
    { value: 1, name: "Lost" },
    { value: 2, name: "Won" },
    { value: 3, name: "Refund" },
    { value: 4, name: "Halflost" },
    { value: 5, name: "Halfwon" }
  ]


  allStatusBetReport = [
    { value: 'in_game', name: "In Game" },
    { value: 'settledmarket', name: "SettledMarket" },
    // { value: 'lost', name: "Lost" },
    // { value: 'won', name: "Won" },
    // { value: 'refund', name: "Refund" },


    // { value: 'in-game', name: "In Game" },
    // { value: 'cashout', name: "cashout" },
    // { value: 'won', name: "Won" },
    // { value: 'PlaceMatchedBet', name: "Place Matched Bet" },
    // { value: 'CancelBet', name: "Cancel Bet" },
    // { value: 'SettledMarket', name: "Settled Market" },
    { value: 'cancelmarket', name: "Cancel Market" },
    { value: 'cancelsettledmarket', name: "CancelSettledMarket" }
  ]
  
  pageSizes = PageSizes;
  
  reports: any[] = [];

  report: any;
  

  p: number = 1;

  format: string = "YYYY-MM-DD HH:mm";

  total: number = 0;
  pageNextTotal: number = 0;
  pageCurrentTotal: number = 0;
  params: any = {
    size: PageSizes[0].name,
    page: 1,
    agent_id:'',
    search: '',
    start_date: '',
    end_date: '',
    bet_type: 'not_combo',
    sport_id: '',
    country: '',
    tournament_id: '',
    match_id: '',
    stake_min_amt: '',
    stake_max_amt: '',
    winning_min_amt: '',
    winning_max_amt: '',
    tenant_id: '',
    status: '',
    time_zone: 'UTC +00:00',
    time_zone_name: 'UTC +00:00',
    sort_by: 'created_at',
    order: 'asc'
  };

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Bet Report', path: '/reports/bet-report' },
  ];

  isLoader:boolean = false;
  firstTimeApiCall = false;

  constructor(public reportService: ReportService,
    // private excelService: ExcelService,
    private sportService: SportService,
    private tenantService: TenantService,
    public adminAuthService: AdminAuthService,
    public superAdminAuthService: SuperAdminAuthService,
    public permissionService: PermissionService) { }

  ngOnInit(): void {
    this.params = { ...this.params , ...this.reportService.betReportParams }
    this.getAllSport();
    this.getAllCountries();
    // this.getAllLeagues();
    this.pageNextTotal = this.params.size;
    this.pageCurrentTotal = this.p;
    if (this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.getTenantsAll();
    } else {
      // this.getReport();
      this.getAgentUserList();
      this.searchLeaguesUrl = generateApiUrl() + `admin/sports/leagues/search`;
    }

    if (this.adminAuthService.adminTokenValue && this.adminAuthService.adminTokenValue.length > 0) {
     
      this.adminAuthService.adminUser.subscribe((user: any) => {
        if(user && user.id) {
          // this.params = {...this.params, time_zone: user.timezone}
          setTimeout(() => {
            const _zone = TIMEZONE.find(t => t.zonename == user.timezone);
            $(`#time_zone`).val(_zone?.zonename).trigger('change');
          }, 100);
        }
      });
    }
    //console.log('created');

  }

  ngAfterViewInit(): void {
    const that = this;

    setTimeout(() => {

    // console.log('params ', this.reportService.betReportParams);

      // $("#tenant_id").val(this.params.tenant_id).trigger('change');
      // $("#sport_id").val(this.params.sport_id).trigger('change');
      // $("#country").val(this.params.country).trigger('change');
      // $("#tournament_id").val(this.params.tournament_id).trigger('change');
      // $("#match_id").val(this.params.match_id).trigger('change');
      
      if (this.params.start_date && this.params.end_date) {
        $('#filter_date').daterangepicker({
         startDate: new Date(this.params.start_date),
          endDate: new Date(this.params.end_date),
          locale: {
            format: this.format
          }
        }, function(start: any, end: any) {
          const start_date = start.format(that.format);
          const end_date = end.format(that.format);
          that.selectDateRange({ start_date, end_date });
        });
        // $('#filter_date').val({ startDate: this.params.start_date, endDate: this.params.end_date });
      } else {
        $('#filter_date').val('');
      }

      $('#time_period').val('');

    }, 50);
  }

  getTenantsAll() {
    this.tenantService.getTenantsAll().subscribe((res: any) => {
      this.allTenants = res.record;
      if (this.allTenants && this.allTenants.length > 0) {
        this.tenantFilter(this.allTenants[0].id);
        this.searchLeaguesUrl = generateApiUrl() + `super/admin/sports/leagues/search?tenant_id=${this.allTenants[0].id}`;
      }
    });
  }

  getAgentUserList(){

    if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.reportService.getSuperAdminAllAgentList({"tenant_id":this.params.tenant_id}).subscribe((res: any) => {
        this.userList = res.record;
      });
    } else {
      this.reportService.getAdminUserList().subscribe((res: any) => {
        this.userList = res.record;
      });
    }

  }

  /**
   *
   * @param evt
   * @returns {any}
   */
  filterSelectAgent(evt: any) {
    if (evt != 0) {

      // this.p = 1;
      this.params = {...this.params, agent_id: evt};

      // if (evt)
      //   this.getReport();
    }
  }
  selectReport(report: any) {
    this.report = report;
  }

  getReport() {
    this.isLoader = true
    this.firstTimeApiCall = true;
    const that = this;
    this.reportService.getBetByESReports(this.params).subscribe((res: any) => {
      this.reports = res.record.data;
      this.total = res.record.total;
      this.stake_sum = res.record.stake_sum;
      this.possible_win_sum = res.record.possible_win_sum;
      this.isLoader = false
    }, (error:any) => {
      this.isLoader = false

    });

    let limit = this.params.size;
    let offset = 1;
    if (this.p > 1) {
      offset = limit * (this.p - 1);
    }
    this.pageCurrentTotal = offset;

    this.pageNextTotal = (offset > 1 ? offset : 0) + parseInt(limit);
    if (this.total < this.pageNextTotal) {
      this.pageNextTotal = this.total;
    }
  }

  setOrder(sort: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p, order: sort.order, sort_by: sort.column };
    this.getReport();
  }

  getAllSport() {
    this.sportService.getAdminAllSports().subscribe((res: any) => {
      this.sports = res.record;
    });  
  }

  getAllCountries() {
    this.sportService.getAdminAllCountries().subscribe((res: any) => {
      this.countries = res.record;
    });
  }

  // getAllLeagues(tenant_id?: any) {
  //   this.sportService.getAdminAllLeagues({ tenant_id: tenant_id ? tenant_id : '' }).subscribe((res: any) => {
  //     this.leagues = res.record;
  //   });
  // }

  filter(evt: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getReport();
  }

  formValidFilter(evt: any) {
    if (this.form.valid) {
      this.getReport();
    }
  }

  tenantFilter(tenant_id: any) {
    // this.p = 1;
    this.params = { ...this.params, tenant_id };
    if (tenant_id) { 
      // this.getAllLeagues(tenant_id);
      this.getAgentUserList();
      // setTimeout(() => {
      //   this.getReport();
      // }, 30);
    }
  }

  selectTenantFilter(tenant_id: any) {
    // this.p = 1;
    this.params = { ...this.params, tenant_id };
    if (tenant_id) { 
      // this.getAllLeagues(tenant_id);
      this.getAgentUserList();
      // setTimeout(() => {
      //   this.getReport();
      // }, 30);
    }
  }
  
  sportFilter(sport_id: any) {
    // this.p = 1;
    this.params = { ...this.params, sport_id };
    // if (sport_id) { 
    //   this.getReport(); 
    // }
  }

  selectCountry(country: any) {
    // this.p = 1;
    this.params = { ...this.params, country };
    // if (country) { 
    //   this.getReport();
    // }
  }

  selectTournament(league_id: any) {
    if(league_id) {
      this.params = { ...this.params, tournament_id: league_id };
      this.sportService.getAdminAllMatches({ league_id, tenant_id: this.params.tenant_id }).subscribe((res: any) => {
        this.matches = res.record;
      });
    }
  }

  selectMatch(match_id: any) {
    // this.p = 1;
    this.params = { ...this.params, match_id };
    // if (match_id) {  
    //   this.getReport();
    // }
  }

  selectDateRange(dates: any) {
    // this.p = 1;
    this.params = { ...this.params, start_date: dates.start_date, end_date: dates.end_date };
    // if (dates.start_date && dates.end_date) { 
    //   this.getReport();
    // }
  }

  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getReport();
  }

  resetFilter() {
    
    setTimeout(() => {
      $("#tenant_id").val(this.allTenants.length > 0 ? this.allTenants[0].id : '').trigger('change');
      $("#sport_id").val("").trigger('change');
      $("#country").val("").trigger('change');
      $("#tournament_id").val("").trigger('change');
      $("#match_id").val("").trigger('change');
      $('#time_period').val('');
      // $( "#time_zone" ).val('UTC +00:00').trigger('change');

    }, 100);

    this.matches = [];

    this.p = 1;
    this.params = {
      size: 10,
      page: 1,

      search: '',
      start_date: '',
      end_date: '',
      bet_type: 'not_combo',
      sport_id: '',
      country: '',
      tournament_id: '',
      match_id: '',
      stake_min_amt: '',
      stake_max_amt: '',
      winning_min_amt: '',
      winning_max_amt: '',
      time_zone: this.params.time_zone,
      time_zone_name: this.params.time_zone_name,
      tenant_id: this.allTenants.length > 0 ? this.allTenants[0].id : '',
      status: '',
      sort_by: 'created_at',
      order: 'asc'
    };

    this.getReport();
  }

  exportAsXLSX() {
    this.reportService.getAllBetReports(this.params).subscribe(
      (res: any) => {
        window.location.href = res.record.url;
      // this.excelService.exportAsExcelFile(res.record, 'report');
    });
  }

  winningPrice(price: string, stake: string) {
    if(price && stake) {
      return (parseFloat(price) * parseFloat(stake)).toFixed(2);
    } else {
      return 'NA';
    }
  }

  getRevenue(report:any){
    return (parseFloat(report?.betslip?.stake) - parseFloat(report?.betslip?.possible_win_amount));
  }

  setBetType(bet_type: string) {
    // this.p = 1;
    this.reports = []
    this.total = 0;
    this.stake_sum = 0;
    this.possible_win_sum = 0;
    this.firstTimeApiCall = false;
    this.params = { ...this.params, bet_type };
    // this.getReport();
  }

  ngOnDestroy(): void {
    this.reportService.betReportParams = this.params;

    // console.log('destroyed');

  }

  filterSelectTimeZone(zonename: any) {
    // this.p = 1;

    if(zonename) {
      const zone = TIMEZONE.find(t => t.zonename === zonename);
      this.zone = zone?.name;
      this.params = {...this.params, time_zone: zone?.value, time_zone_name: zone?.zonename};
    }else{
      this.zone='';
      this.params = {...this.params, time_zone: this.zone};
    }

    // this.getReport();
}

}
