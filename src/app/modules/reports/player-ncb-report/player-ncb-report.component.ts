import { Component, OnInit,AfterViewInit, OnDestroy } from '@angular/core';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
import { ReportService } from '../reports.service';
import { PageSizes, TIMEZONE,getTransactionType } from 'src/app/shared/constants';
import { AdminAuthService } from '../../admin/services/admin-auth.service';
import {Currency} from 'src/app/models';
import { environment } from 'src/environments/environment';
import { Role } from 'src/app/models';
import * as moment from 'moment-timezone';
import { PermissionService } from 'src/app/services/permission.service';
declare const $: any;

@Component({
  selector: 'app-player-ncb-report',
  templateUrl: './player-ncb-report.component.html',
  styleUrls: ['./player-ncb-report.component.scss']
})
export class PlayerNcbReportComponent implements OnInit, OnDestroy {
  isSubAgentModuleAllowed:boolean = true;
  isPrimaryCurrencyDataEnable:boolean = true;
  roles: string = localStorage.getItem('roles') || '';
  isAgent:boolean = false;
  pageSizes: any[] = PageSizes;
  TIMEZONE: any[] = TIMEZONE;
  zone: any = '(GMT +00:00) UTC';
  reports: any[] = [];
  p: number = 1;
  currencies: Currency[] = [];
  format: string = "YYYY-MM-DD HH:mm:ss";
  userList :any[]=[];
  tenantsList :any[]=[];
  tenantsOwnerList :any[]=[];
  total: number = 0;
  customDate=false;
  bet: number = 0;
  win: number = 0;
  tip: number = 0;
  total_non_cash_granted_by_admin: number = 0;
  total_non_cash_withdraw_by_admin: number = 0;
  total_non_cash_player_commission: number = 0;
  total_non_cash_bonus_claim: number = 0;
  pageNextTotal: number = 0;
  pageCurrentTotal: number = 0;
  params: any = {
    size: 10,
    page: 1,
    search: '',
    agent_id: '',
    currency: '',
    player_type: 'all',
    isDirectPlayer: '',
    time_zone: 'UTC +00:00',
    time_zone_name: 'UTC +00:00',
    tenant_id: '',
    owner_id: '',
    action_type: '',
    time_type: 'today',
    time_period: [],
    datetime: {},
    order: 'asc',
    sort_by: 'player_details.player_id'
  };
  hideShow:boolean = true;

  dataTableParams: any = {
    searching: false,
    info: true,
    lengthChange: true,
    autoWidth: false,
    responsive: true,
    "dom": '<"top"lp>t<"bottom"ifp><"clear">',
    lengthMenu: PageSizes.map(m => m.name),
    buttons: [{ extend: 'csv', text: 'Export as CSV', className: 'ctsmcsv mgt btn btn-info mt-0' }]
  };
  isLoader: boolean = false;
  firstTimeApiCall = false;
  allReports: any[] = [];
  tenantBaseCurrency:any = '';

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Player NCB Report', path: '/reports/player-ncb-report' },
  ];

  constructor(private ReportService: ReportService,
              private adminAuthService: AdminAuthService,
    public superAdminAuthService: SuperAdminAuthService,
    public permissionService: PermissionService) {
    this.params = { ...this.params, ...this.ReportService.playerNCBReportParams };
    this.isSubAgentModuleAllowed = (localStorage.getItem('allowedModules')?.includes('subAgent') ? true : false);
    this.isPrimaryCurrencyDataEnable = (localStorage.getItem('allowedModules')?.includes('primaryCurrencyDataEnable') ? true : false);

  }

  ngOnInit(): void {

    if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.getTenantList();
      this.hideShow = false;
    } else {
      // this.getReport();
      this.getCurrencyList();
      this.getAgentUserList();
    }
    if (this.adminAuthService.adminTokenValue && this.adminAuthService.adminTokenValue.length > 0) {
      this.adminAuthService.adminUser.subscribe((user: any) => {
        if(user && user.id) {
          // this.params = {...this.params, time_zone: user.timezone}
          setTimeout(() => {
            const _zone = TIMEZONE.find(t => t.zonename == user.timezone);
            $(`#time_zone`).val(_zone?.zonename).trigger('change');
          }, 100);
        }
      });
        if(this.roles) {
          const roles = JSON.parse(this.roles);
          if(roles && roles.findIndex( (role: any) => role === Role.Admin ) == -1 && roles.findIndex( (role: any) => role === Role.Agent ) > -1) {
            this.isAgent = true;
          }
  
        }
  
      }
    this.pageNextTotal = this.params.size;
    this.pageCurrentTotal = this.p;

    

  }

  getAgentUserList(){

    if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.ReportService.getSuperAdminUserList({"owner_id":this.params.owner_id,"tenant_id":this.params.tenant_id}).subscribe((res: any) => {
        this.userList = res.record;
      });
    } else {
      this.ReportService.getAdminUserList().subscribe((res: any) => {
        this.userList = res.record;
      });
    }

  }

  search (searchValue: any) {

    // this.p = 1;
    this.params = {...this.params, search:searchValue};

    if (searchValue) {

      setTimeout(() => {
        $('#search').val(searchValue).trigger('keyup');
      }, 0);
      // this.getReport();
    }
  }

  getTenantList(){

    if(this.superAdminAuthService.superAdminTokenValue &&
        this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.ReportService.getSuperTenantsList().subscribe((res: any) => {
        this.tenantsList = res.record;
        if (this.tenantsList.length > 0) {
            this.params.tenant_id = 0;
            this.getTenantOwnerList(0);
            this.getCurrencyList('0');
        }
      });
    }
  }

  getTenantOwnerList(id:number){

    if(this.superAdminAuthService.superAdminTokenValue &&
        this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.ReportService.getSuperTenantsOwnerList({id}).subscribe((res: any) => {
        this.tenantsOwnerList = res.record;
        this.params.owner_id = this.tenantsOwnerList.length > 0 ? this.tenantsOwnerList[0].id : '';
        // this.getReport();
      });
    }
  }

  tenantFilter(evnt: any) {
    // this.p = 1;
    // this.params.page = this.p;
    this.params.tenant_id = parseInt(evnt);
    this.getTenantOwnerList(evnt);
    this.getCurrencyList(evnt);

    if(evnt != 0){
      this.hideShow = true;
      this.getAgentUserList();
    }
    else{
      this.hideShow = false;
      this.userList = [];
    }
  }

  getCurrencyList(evnt=''){
    if(this.superAdminAuthService.superAdminTokenValue &&
        this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.ReportService.getCurrencyList(evnt).subscribe((res: any) => {
        this.currencies = res.record;
      });

    }else {

      this.ReportService.getCurrencyList().subscribe((res: any) => {
        this.currencies = res.record;
      });
    }
  }

  convertToUTC(inputDate: string, inputTimezone: string): string {
    const date = moment.tz(inputDate, inputTimezone);
    const utcDate = date.utc().format('YYYY-MM-DD HH:mm:ss');
    return utcDate;
  }
  getReport() {
    this.isLoader = true;
    this.firstTimeApiCall = true;
    $('.ctsmcsv').remove();
    if(this.params.time_period?.start_date && this.params.time_period?.end_date && this.params.time_zone_name && this.params.time_zone_name !== 'UTC +00:00'){
      this.params = {...this.params, datetime : {
        start_date: this.convertToUTC(this.params.time_period.start_date, this.params.time_zone_name), end_date:this.convertToUTC(this.params.time_period.end_date, this.params.time_zone_name)
      }}
    } else {
      this.params = {...this.params, datetime : {
        start_date: this.params.time_period?.start_date,
        end_date:this.params.time_period?.end_date
      }}
    }
    if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.ReportService.getSuperAdminNCBPlayerReport(this.params).subscribe((res: any) => {
        this. getPrepareRecord(res);
      }, err => (error:any) => {
        this.isLoader = false

      });
    } else {
      this.ReportService.getAdminPlayerNCBReport(this.params).subscribe((res: any) => {
        this. getPrepareRecord(res);
      }, err => (error:any) => {
        this.isLoader = false

      });
    }
  }

  getPrepareRecord(res:any){
    this.reports = res.record.hits.hits;
     this.tenantBaseCurrency = res.record.tenant_base_currency
    this.total = res.record.hits.total.value;
    this.win = res.record.aggregations.total_win_non_cash.total.value;
    this.bet = res.record.aggregations.total_bet_non_cash.total.value;
    this.tip = res.record.aggregations.total_tip_non_cash.total.value;
    this.total_non_cash_granted_by_admin = res.record.aggregations.total_non_cash_granted_by_admin.total.value;
    this.total_non_cash_withdraw_by_admin = res.record.aggregations.total_non_cash_withdraw_by_admin.total.value;
    this.total_non_cash_bonus_claim = res.record.aggregations.total_non_cash_bonus_claim.total.value;
    this.total_non_cash_player_commission = res.record.aggregations.total_non_cash_bonus_player_commission_claim.total.value;

    let limit = this.params.size;
    let offset = 1;
    if (this.p > 1) {
      offset = limit * (this.p - 1);
    }
    this.pageCurrentTotal = offset;

    this.pageNextTotal = (offset>1?offset:0) + parseInt(limit);
    if(this.total < this.pageNextTotal){
      this.pageNextTotal = this.total;
    }
    this.isLoader = false
  }

  filter(evt: any,dateFilterType: boolean=false) {

    if(this.params.time_type=='custom'){
      this.customDate=true;
      if(dateFilterType){
        setTimeout(() => {    
          $('#time_period').val('');
        }, 10);
      }
      // if(!dateFilterType)
      // this.getReport();
    }else{
      this.customDate=false;
    }
  }

  submitFilter(){
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getReport();
  }

  filterSelectAgent(evt: any) {
    // this.p = 1
    this.params = {...this.params, agent_id:evt};

    // if(evt)
    // this.getReport();
  }

  selectDateRange(time_period: any) {
    this.params = {...this.params, time_period}
    // this.getReport();
  }

  filterSelectOwner(evt: any) {
    // this.p = 1;
    this.params = {...this.params, owner_id:evt};
    // this.getReport();
  }

  filterSelectTimeZone(zonename: any) {
    // this.p = 1;

    if(zonename) {
      const zone = TIMEZONE.find(t => t.zonename === zonename);
      this.zone = zone?.name;
      this.params = {...this.params, time_zone: zone?.value, time_zone_name: zone?.zonename};
    }else{
      this.zone='';
      this.params = {...this.params, time_zone: this.zone};
    }

    // this.getReport();
  }
  

  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getReport();
  }

    resetFilter() {

        this.p = 1;
        this.params = {
            size: 10,
            page: 1,
            search: '',
            agent_id: '',
            currency: '',
            isDirectPlayer: '',
            action_type: '',
            time_zone: this.params.time_zone,
            time_zone_name: this.params.time_zone_name,
            tenant_id: this.params.tenant_id,
            owner_id: '',
            datetime: {},
            order: 'asc',
            player_type: 'all',
            time_type: 'today',
            sort_by: 'player_details.player_id'
    
        };

        this.getReport();
        $( "#agent_id" ).val('').trigger('change');
        // $( "#time_zone" ).val('UTC +00:00').trigger('change');
    }
    download(){
        let timeZoneValue='';
        if(this.params.time_zone){
            timeZoneValue=TIMEZONE.filter(element => element.value === this.params.time_zone)[0].zonename;
        }

        if(timeZoneValue) {
            this.params = {...this.params, time_zone_name: timeZoneValue};
        }
      if(this.params.time_period?.start_date && this.params.time_period?.end_date && this.params.time_zone_name && this.params.time_zone_name !== 'UTC +00:00'){
        this.params = {...this.params, datetime : {
          start_date: this.convertToUTC(this.params.time_period.start_date, this.params.time_zone_name), end_date:this.convertToUTC(this.params.time_period.end_date, this.params.time_zone_name)
        }}
      } else {
        this.params = {...this.params, datetime : {
          start_date: this.params.time_period?.start_date,
          end_date:this.params.time_period?.end_date
        }}
      }
        if (this.superAdminAuthService.superAdminTokenValue &&
            this.superAdminAuthService.superAdminTokenValue.length > 0) {

            this.ReportService.getSuperAdminNCBPlayerReportDownload(this.params).subscribe((res: any) => {
                // window.open(res.record.url);
                window.location.href = res.record.url;

            }), (error: any) => // console.log('Error downloading the file'), //when you use stricter type checking
                () => console.info('File downloaded successfully');

        } else {

            this.ReportService.getAdminPlayerNCBReportDownload(this.params).subscribe((res: any) => {
                // window.open(res.record.url);
                window.location.href = res.record.url;

            }), (error: any) => // console.log('Error downloading the file'), //when you use stricter type checking
                () => console.info('File downloaded successfully');
        }
    }

  setOrder(sort: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p, order: sort.order, sort_by: sort.column };
    this.getReport();
  }
    
  ngOnDestroy(): void {
    this.ReportService.playerNCBReportParams = { ...this.ReportService.playerNCBReportParams, ...this.params };
  }
  trnType(value:any){
    return getTransactionType(value)
  }
}
