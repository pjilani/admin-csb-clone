import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerNcbReportComponent } from './player-ncb-report.component';

describe('PlayerNcbReportComponent', () => {
  let component: PlayerNcbReportComponent;
  let fixture: ComponentFixture<PlayerNcbReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlayerNcbReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerNcbReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
