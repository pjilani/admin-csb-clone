import {Component, OnDestroy, OnInit} from '@angular/core';
import {SuperAdminAuthService} from '../../super-admin/services/super-admin-auth.service';
import {ReportService} from '../reports.service';
import {AdminAuthService} from '../../admin/services/admin-auth.service';
import {PageSizes, TIMEZONE} from 'src/app/shared/constants';
import {Currency} from 'src/app/models';
import {environment} from 'src/environments/environment';
import { Role } from 'src/app/models';
import * as moment from 'moment-timezone';
import { PermissionService } from 'src/app/services/permission.service';
declare const $: any;
@Component({
  selector: 'app-game-transaction-report',
  templateUrl: './game-transaction-report.component.html',
  styleUrls: ['./game-transaction-report.component.scss']
})

export class GameTransactionReportComponent implements OnInit, OnDestroy {
  isSubAgentModuleAllowed:boolean = true;
  isPrimaryCurrencyDataEnable:boolean = true;
  roles: string = localStorage.getItem('roles') || '';
  isAgent:boolean = false;
  pageSizes = PageSizes;
  userList: any[] = [];
  tenant_base_currency:any = '';
  tenantsOwnerList: any[] = [];
  tenantsList: any[] = [];
  TIMEZONE: any[] = TIMEZONE;
  zone: any = '(GMT +00:00) UTC';
  currenciesResultArray: any[] = [];
  reports: any[] = [];
  gameProvider: any[] = [];
  gameDetails: any = '';
  gameType: any[] = [];
  p: number = 1;
  customDate=false;
  currencies: Currency[] = [];
  format: string = "YYYY-MM-DD HH:mm:ss";
  bet: number = 0;
  totalRefund:number = 0;
  win: number = 0;
  tip: number = 0;
  total: number = 0;
  pageNextTotal: number = 0;
  pageCurrentTotal: number = 0;
  params: any = {
    size: 10,
    page: 1,
    search: '',
    status: '',
    time_type : 'today',
    agent_id: '',
    currency: '',
    player_type: 'all',
    time_zone: 'UTC +00:00',
    time_zone_name: 'UTC +00:00',
    tenant_id: '',
    owner_id: '',
    action_type: '',
    time_period: [],
    datetime: [],
    internal_error_code: '',
    game_provider: '',
    game_type: '',
    order: 'desc',
    sort_by: 'created_at'
            
  };

  hideShow:boolean = true;

  dataTableParams: any = {
    searching: false,
    info: true,
    lengthChange: true,
    autoWidth: false,
    responsive: true,
    "dom": '<"top"lp>t<"bottom"ifp><"clear">',
    lengthMenu: PageSizes.map(m => m.name),
    buttons: [{ extend: 'csv', text: 'Export as CSV', className: 'ctsmcsv mgt btn btn-info mt-0' }]
  };
  repLoader: boolean = false;
  allReports: any[] = [];
  isLoader:boolean = false;
  firstTimeApiCall = false;
  gameDetailsLoader = false;

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Game Transaction Report', path: '/reports/game-transaction-report' },
  ];
  selectedTransactionGameProvider: any;

    constructor(private ReportService: ReportService,
                private adminAuthService: AdminAuthService,
      public superAdminAuthService: SuperAdminAuthService,
      public permissionService: PermissionService) {
      this.params = { ...this.params, ...this.ReportService.gameTransactionReportParams };
    this.isSubAgentModuleAllowed = (localStorage.getItem('allowedModules')?.includes('subAgent') ? true : false);
    this.isPrimaryCurrencyDataEnable = (localStorage.getItem('allowedModules')?.includes('primaryCurrencyDataEnable') ? true : false);


    }

  ngOnInit(): void {

    if (this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.getTenantList();
      this.hideShow = false;
    } else {
      // this.getReport();
      this.getCurrencyList();
      this.getAgentUserList();
    }
    if (this.adminAuthService.adminTokenValue && this.adminAuthService.adminTokenValue.length > 0) {
      this.adminAuthService.adminUser.subscribe((user: any) => {
        if(user && user.id) {
          // this.params = {...this.params, time_zone: user.timezone}
          setTimeout(() => {
            const _zone = TIMEZONE.find(t => t.zonename == user.timezone);
            $(`#time_zone`).val(_zone?.zonename).trigger('change');
          }, 100);
        }
      });
      if(this.roles) {
        const roles = JSON.parse(this.roles);
        if(roles && roles.findIndex( (role: any) => role === Role.Admin ) == -1 && roles.findIndex( (role: any) => role === Role.Agent ) > -1) {
          this.isAgent = true;
        }

      }

    }
    
    this.pageNextTotal = this.params.size;
    this.pageCurrentTotal = this.p;
    // this.getAgentUserList();
  }


  getTenantOwnerList(id: number) {

    if (this.superAdminAuthService.superAdminTokenValue &&
        this.superAdminAuthService.superAdminTokenValue.length > 0) {
        this.ReportService.getSuperTenantsOwnerList({id}).subscribe((res: any) => {
          this.tenantsOwnerList = res.record;
          this.params.owner_id = this.tenantsOwnerList.length > 0 ? this.tenantsOwnerList[0].id : '';
          // this.getReport();
        });
    }
  }

  selectDateRange(time_period: any) {
    this.params = {...this.params, time_period}
    // this.getReport();
  }

  getCurrencyList(evnt = '') {
    if (this.superAdminAuthService.superAdminTokenValue &&
        this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.ReportService.getCurrencyList(evnt).subscribe((res: any) => {
        this.currencies = res.record;
      });

    } else {

      this.ReportService.getCurrencyList().subscribe((res: any) => {
        this.currencies = res.record;
      });
    }
  }

  filterSelectAgent(evt: any) {
    // this.p = 1;
    this.params = {...this.params, agent_id: evt};

    // if(evt)
    // this.getReport();
  }

  getTenantList() {

    if (this.superAdminAuthService.superAdminTokenValue &&
        this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.ReportService.getSuperTenantsList().subscribe((res: any) => {
        this.tenantsList = res.record;
        if (this.tenantsList.length > 0) {
            // this.params.tenant_id = this.tenantsList[0].id;
            // this.getTenantOwnerList(this.tenantsList[0].id);
            // this.getCurrencyList(this.tenantsList[0].id);
            this.params.tenant_id = 0;
            this.getTenantOwnerList(0);
            this.getCurrencyList('0');
        }
      });
    }
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      $('#time_period').val('today');
    }, 100);
  }

  filter(evt: any,dateFilterType: boolean=false) {
      if(this.params.time_type=='custom'){
        this.customDate=true;
        if(dateFilterType){
          setTimeout(() => {    
            $('#time_period').val('');
          }, 10);
        }
        // if(!dateFilterType)
        // this.getReport();
      }else{
        this.customDate=false;
      }

  }

  submitFilter(){
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getReport();
  }
  
  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getReport();
  }

  tenantFilter(evnt: any) {
    // this.p = 1;
    // this.params.page = this.p;
    this.params.tenant_id = parseInt(evnt);
    this.getTenantOwnerList(evnt);
    this.getCurrencyList(evnt);
    if(evnt != 0){
      this.hideShow = true;
      this.getAgentUserList();
    }
    else{
      this.hideShow = false;
      this.userList = [];
    }
  }

  filterSelectOwner(evt: any) {
    // this.p = 1;
    this.params = {...this.params, owner_id: evt};
    this.getAgentUserList();
    // this.getReport();
  }

  filterSelectTimeZone(zonename: any) {
    // this.p = 1;

    if(zonename) {
      const zone = TIMEZONE.find(t => t.zonename === zonename);
      this.zone = zone?.name;
      this.params = {...this.params, time_zone: zone?.value, time_zone_name: zone?.zonename};
    }else{
      this.zone='';
      this.params = {...this.params, time_zone: this.zone};
    }

    // this.getReport();
  }
  
  convertToUTC(inputDate: string, inputTimezone: string): string {
    const date = moment.tz(inputDate, inputTimezone);
    const utcDate = date.utc().format('YYYY-MM-DD HH:mm:ss');
    return utcDate;
  }

  getReport() {
    if(this.params.game_provider === ''){
      this.params.game_type = ''
    }

    this.isLoader = true;
    this.firstTimeApiCall = true;
    
    $('.ctsmcsv').remove();

    if(this.params.time_period?.start_date && this.params.time_period?.end_date && this.params.time_zone_name && this.params.time_zone_name !== 'UTC +00:00'){
      this.params = {...this.params, datetime : {
        start_date: this.convertToUTC(this.params.time_period.start_date, this.params.time_zone_name),
        end_date:this.convertToUTC(this.params.time_period.end_date, this.params.time_zone_name)
      }}
    } else{      
      this.params = {...this.params, datetime : { start_date: this.params.time_period?.start_date, end_date:this.params.time_period?.end_date}}
    }
    if (this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.ReportService.getSuperAdminPlayerGameTransactionReport(this.params).subscribe((res: any) => {
        this.getPrepareRecord(res);
      }, (error:any) => {
        this.isLoader = false

      } );
    } else {
      this.ReportService.getAdminPlayerGameTransactionReport(this.params).subscribe((res: any) => {
        this.getPrepareRecord(res);
      }, (error:any) => {
        this.isLoader = false

      } );
    }


  }

  selectAgent(searchValue: any) {

    // this.p = 1;
    this.params = {...this.params, search:searchValue};

    if (searchValue) {

      setTimeout(() => {
        $('#search').val(searchValue).trigger('keyup');
      }, 0);
      // this.getReport();
    }
  }

  getPrepareRecord(res: any) {

    this.reports = res.record.hits.hits;
    this.total = res.record.hits.total.value;
    this.win = res.record.aggregations.total_wins.total.value;
    this.bet = res.record.aggregations.total_bets.total.value;
    this.totalRefund = res.record.aggregations.total_refund.total.value;
    this.tip = res.record.aggregations.total_tip.total.value;
    this.gameType = res.record.resultGameProvider.game_provider.buckets;
    this.gameProvider = res.record.resultGameProvider.game_type.buckets;
    this.tenant_base_currency = res.record.tenant_base_currency;
    //totalBalance: number = 0;
    // totalAmountBet: number = 0;
    let limit = this.params.size;
    let offset = 1;
    if (this.p > 1) {
      offset = limit * (this.p - 1);
    }
    this.pageCurrentTotal = offset;

    this.pageNextTotal = (offset>1?offset:0) + parseInt(limit);
    if(this.total < this.pageNextTotal){
      this.pageNextTotal = this.total;
    }
    this.isLoader = false;

    

  }
  resetFilter() {
    this.p = 1;
    

    
    this.params = {
      size: 10,
      page: 1,
      search: '',
      status: '',
      agent_id: '',
      currency: '',
      player_type: 'all',
      time_type : 'today',
      time_zone: this.params.time_zone,
      time_zone_name: this.params.time_zone_name,
      tenant_id: '',
      owner_id: '',
      action_type: '',
      time_period: '',
      datetime: {},
      internal_error_code: '',
      game_provider: '',
      order: 'desc',
      sort_by: 'created_at'
            
    };
    // $("#time_zone").val('UTC +00:00').trigger('change');
    this.getReport();
  }

  getAgentUserList(tenantid='') {

    if (this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {

      if(!tenantid){
        tenantid=this.params.tenant_id;
      }
      this.ReportService.getSuperAdminUserList({"owner_id":this.params.owner_id,"tenant_id":tenantid}).subscribe((res: any) => {
        this.userList = res.record;
      });
    } else {
      this.ReportService.getAdminUserList().subscribe((res: any) => {
        this.userList = res.record;
      });
    }
  }

  download(){
    let timeZoneValue='';
    if(this.params.time_zone){
        timeZoneValue=TIMEZONE.filter(element => element.value === this.params.time_zone)[0].zonename;
    }

    if(timeZoneValue) {
        this.params = {...this.params, time_zone_name: timeZoneValue};
    }

    if(this.params.time_period?.start_date && this.params.time_period?.end_date && this.params.time_zone_name && this.params.time_zone_name !== 'UTC +00:00'){
      this.params = {...this.params, datetime : {
        start_date: this.convertToUTC(this.params.time_period.start_date, this.params.time_zone_name),
        end_date:this.convertToUTC(this.params.time_period.end_date, this.params.time_zone_name)
      }}
    } else {
      this.params = {...this.params, datetime : { start_date: this.params.time_period.start_date, end_date:this.params.time_period.end_date}}
    }

    if (this.superAdminAuthService.superAdminTokenValue &&
        this.superAdminAuthService.superAdminTokenValue.length > 0) {

        this.ReportService.getDownloadSuperAdminPlayerGameTransactionReport(this.params).subscribe((res: any) => {
            // window.open(res.record.url);
            window.location.href = res.record.url;

        }), (error: any) => // console.log('Error downloading the file'), //when you use stricter type checking
            () => console.info('File downloaded successfully');

    } else {

        this.ReportService.getDownloadAdminPlayerGameTransactionReport(this.params).subscribe((res: any) => {
            // window.open(res.record.url);
            window.location.href = res.record.url;

        }), (error: any) => // console.log('Error downloading the file'), //when you use stricter type checking
            () => console.info('File downloaded successfully');
    }

}


  setOrder(sort: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p, order: sort.order, sort_by: sort.column };
    this.getReport();
  }

  getDetails(transaction: any) {
    let paramArray ={
      'GameProvider': transaction.game_provider,
      'RoundID':transaction.round_id,
      'TransactionID':transaction.transaction_id,
      'UID':transaction.actionee_id,
      'tenant_id':transaction.tenant_id,
      'GameType': transaction.game_type,
      'PlayerToken': transaction.transfer_method
    };
    this.selectedTransactionGameProvider = transaction.game_provider
    this.gameDetails = '';
    this.gameDetailsLoader = true;
    this.ReportService.getGameTransactionDetails(paramArray).subscribe((res: any) => {
      if(res?.record?.data){
        this.gameDetails = res.record.data;
      }
      this.gameDetailsLoader = false;
    },err=>{
      this.gameDetailsLoader = false;
    });
  }
  ngOnDestroy(): void {
    this.ReportService.gameTransactionReportParams = { ...this.ReportService.gameTransactionReportParams, ...this.params };
  }

  checkForEzugi(transaction:any){
    if(transaction.game_provider == 'Ezugi' && this.isLessThan2HoursFromCurrent(transaction.created_at)){
      return false;
    }
    return true;
  }

  isLessThan2HoursFromCurrent(givenDatetimeString:any) {
    let givenDatetime:any = new Date(givenDatetimeString);

    let currentDatetime:any = new Date();

    var timeDifference = currentDatetime - givenDatetime;

    var hoursDifference = timeDifference / (1000 * 60 * 60);

    return hoursDifference < 2;
}
  

}
