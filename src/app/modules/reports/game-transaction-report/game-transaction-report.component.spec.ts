import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameTransactionReportComponent } from './game-transaction-report.component';

describe('GameTransactionReportComponent', () => {
  let component: GameTransactionReportComponent;
  let fixture: ComponentFixture<GameTransactionReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GameTransactionReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameTransactionReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
