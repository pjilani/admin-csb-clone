import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BonusStatusPerPlayerReportComponent } from './bonus-status-per-player-report.component';

describe('BonusStatusPerPlayerReportComponent', () => {
  let component: BonusStatusPerPlayerReportComponent;
  let fixture: ComponentFixture<BonusStatusPerPlayerReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BonusStatusPerPlayerReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BonusStatusPerPlayerReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
