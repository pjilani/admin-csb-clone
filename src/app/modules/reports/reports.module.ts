import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PlayerReportComponent } from './player-report/player-report.component';
import { PlayerFinancialReportComponent } from './player-financial-report/player-financial-report.component';
import { PlayerRevenueReportComponent } from './player-revenue-report/player-revenue-report.component';
import { PlayerNcbReportComponent } from './player-ncb-report/player-ncb-report.component';
import { GameTransactionReportComponent } from './game-transaction-report/game-transaction-report.component';
import { TipReportComponent } from './tip-report/tip-report.component';
import { AgentRevenueReportComponent } from './agent-revenue-report/agent-revenue-report.component';
import { UnifiedTransactionReportComponent } from './unified-transaction-report/unified-transaction-report.component';
import { BonusStatusPerPlayerReportComponent } from './bonus-status-per-player-report/bonus-status-per-player-report.component';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { BetReportComponent } from './bet-report/bet-report.component';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { PipeModule } from 'src/app/pipes/pipes.module';
import { AdminAgentCommonAuthGuard, CommonAdminAuthGuard, ReportAuthGuard, SuperAdminCommonAuthGuard } from 'src/app/guards';
import { SportPlayerRevenueReportComponent } from './sport-player-revenue-report/sport-player-revenue-report.component';
import { SportAgentRevenueReportComponent } from './sport-agent-revenue-report/sport-agent-revenue-report.component';
import { BetCloneReportComponent } from './bet-clone-report/bet-clone-report.component';
import { GgrReportComponent } from './ggr-report/ggr-report.component';

const tenantsRoutes: Routes = [
  { path: '', redirectTo: 'player-report', pathMatch: 'full' },
  { path: 'player-report', component: PlayerReportComponent, canActivate: [CommonAdminAuthGuard] },
  { path: 'player-financial-report', component: PlayerFinancialReportComponent, canActivate: [CommonAdminAuthGuard] },
  { path: 'game-transaction-report', component: GameTransactionReportComponent, canActivate: [CommonAdminAuthGuard] },
  { path: 'player-ncb-report', component: PlayerNcbReportComponent, canActivate: [CommonAdminAuthGuard] },
  { path: 'tip-report', component: TipReportComponent, canActivate: [CommonAdminAuthGuard] },
  { path: 'player-revenue-report', component: PlayerRevenueReportComponent, canActivate: [CommonAdminAuthGuard] },
  { path: 'sport-player-revenue-report', component: SportPlayerRevenueReportComponent, canActivate: [CommonAdminAuthGuard] },
  { path: 'agent-revenue-report', component: AgentRevenueReportComponent, canActivate: [CommonAdminAuthGuard] },
  { path: 'sport-agent-revenue-report', component: SportAgentRevenueReportComponent, canActivate: [CommonAdminAuthGuard] },
  { path: 'unified-report', component: UnifiedTransactionReportComponent, canActivate: [CommonAdminAuthGuard] },
  { path: 'bonus-status-per-player-report', component: BonusStatusPerPlayerReportComponent, canActivate: [CommonAdminAuthGuard] },
  { path: 'bet-report', component: BetReportComponent, canActivate: [CommonAdminAuthGuard] },
  { path: 'bet', component: BetCloneReportComponent },
  { path: 'ggr-report', component: GgrReportComponent, canActivate: [CommonAdminAuthGuard] },
]

@NgModule({

  declarations: [
       PlayerReportComponent,
       PlayerFinancialReportComponent,
       PlayerRevenueReportComponent,
       SportPlayerRevenueReportComponent,
       PlayerNcbReportComponent,
       GameTransactionReportComponent,
       TipReportComponent,
       AgentRevenueReportComponent,
       SportAgentRevenueReportComponent,
       UnifiedTransactionReportComponent,
       BonusStatusPerPlayerReportComponent,
       BetReportComponent,
       BetCloneReportComponent,
       GgrReportComponent
  ],
  
  imports: [
    CommonModule,
    RouterModule.forChild(tenantsRoutes),
    ComponentsModule,
    DirectivesModule,
    PipeModule,
    SharedModule
  ]

})
  
export class ReportsModule { }
