import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SuperAdminAuthService } from './../super-admin/services/super-admin-auth.service';
import { PageSizes } from 'src/app/shared/constants';

@Injectable({ providedIn: 'root' })

export class ReportService {

  betReportParams = {
    size: PageSizes[0].name,
    page: 1,
    agent_id:'',
    search: '',
    start_date: '',
    end_date: '',
    bet_type: 'not_combo',
    sport_id: '',
    country: '',
    tournament_id: '',
    match_id: '',
    stake_min_amt: '',
    stake_max_amt: '',
    winning_min_amt: '',
    winning_max_amt: '',
    tenant_id: '',
    status: '',
    sort_by: 'created_at',
    order: 'desc'

  };

  userBetReportParams = {
    size: PageSizes[0].name,
    page: 1,

    search: '',
    user_id: 0,
    start_date: '',
    end_date: '',
    bet_type: 'not_combo',
    sport_id: '',
    country: '',
    tournament_id: '',
    match_id: '',
    stake_min_amt: '',
    stake_max_amt: '',
    winning_min_amt: '',
    winning_max_amt: '',
    tenant_id: '',
    status: '',
    sort_by: 'created_at',
    order: 'desc'

  };

  playerReportParams = {
        size: PageSizes[0].name,
        page: 1,
        search: '',
        agent_id: '',
        currency: '',
        isDirectPlayer: '',
        time_zone: 'UTC +00:00',
        tenant_id: '',
        owner_id: '',
        order: 'desc',
        sort_by: 'creation_date'
  };

  playerFinanceReportParams = {
        size: PageSizes[0].name,
        page: 1,
        search: '',
        agent_id: '',
        currency: '',
        isDirectPlayer: 'all',
        time_zone: 'UTC +00:00',
        tenant_id: '',
        owner_id: '',
        order: 'desc',
        sort_by: 'created_at'
  };

  gameTransactionReportParams = {
    size: 10,
    page: 1,
    search: '',
    status: '',
    time_type : 'today',
    agent_id: '',
    currency: '',
    player_type: 'all',
    time_zone: 'UTC +00:00',
    tenant_id: '',
    owner_id: '',
    action_type: '',
    time_period: [],

    internal_error_code: '',
    game_provider: '',
    game_type: '',
    order: 'desc',
    sort_by: 'created_at'
    
  }

  playerNCBReportParams = {
    size: 10,
    page: 1,
    search: '',
    agent_id: '',
    currency: '',
    player_type: 'all',
    isDirectPlayer: '',
    time_zone: 'UTC +00:00',
    tenant_id: '',
    owner_id: '',
    action_type: '',
    time_type: 'today',
    time_period: [],

    order: 'desc',
    sort_by: 'created_at'
      
  }

  tipReportParams: any = {
    size: 10,
    page: 1,
    search: '',
    agent_id: '',
    currency: '',
    player_type: 'all',
    isDirectPlayer: '',
    time_zone: 'UTC +00:00',
    tenant_id: '',
    owner_id: '',
    action_type: '',
    time_type: 'today',
    time_period: [],
    // order: 'asc',
    // sort_by: 'transaction.doc_count'
  };

  playerRevenueParams = {
    size: 10,
    page: 1,
    search: '',
    agent_id: '',
    currency: '',
    player_type: 'all',
    isDirectPlayer: '',
    time_zone: 'UTC +00:00',
    tenant_id: '',
    owner_id: '',
    action_type: '',
    time_type: 'today',
    time_period: [],
    // order: 'asc',
    // sort_by: 'player_name'
  };

  playerSportRevenueParams = {
    size: 10,
    page: 1,
    search: '',
    agent_id: '',
    currency: '',
    player_type: 'all',
    isDirectPlayer: '',
    time_zone: 'UTC +00:00',
    tenant_id: '',
    owner_id: '',
    action_type: '',
    time_type: 'today',
    time_period: [],
    // order: 'asc',
    // sort_by: 'player_name'
  };

  agentRevenueParams: any = {
    size: 10,
    page: 1,
    search: '',
    agent_id: '',
    currency: '',
    player_type: 'all',
    isDirectPlayer: '',
    time_zone: 'UTC +00:00',
    tenant_id: '',
    owner_id: '',
    action_type: '',
    time_type: 'today',
    time_period: [],
    game_table:'',
    game_type:'',
    provider:''
    // order: 'asc',
    // sort_by: 'agent_name'
  };

  agentSportRevenueParams: any = {
    size: 10,
    page: 1,
    search: '',
    agent_id: '',
    currency: '',
    player_type: 'all',
    isDirectPlayer: '',
    time_zone: 'UTC +00:00',
    tenant_id: '',
    owner_id: '',
    action_type: '',
    time_type: 'today',
    time_period: [],
    game_table:'',
    game_type:'',
    provider:''
    // order: 'asc',
    // sort_by: 'agent_name'
  };

  uTParams: any = {
    size: 10,
    page: 1,
    search: '',
    agent_id: '',
    currency: '',
    player_type: 'all',
    isDirectPlayer: '',
    time_zone: 'UTC +00:00',
    tenant_id: '',
    owner_id: '',
    action_type: '',
    type: '',
    time_type: 'today',
    internal_error_code: '',
    game_provider: '',
    time_period: '',
    order: 'desc',
    sort_by: 'created_at'
  };

  bonusStatusPerPlayerReportParams: any = {
    size: 10,
    page: 1,
    search: '',
    agent_id: '',
    currency: '',
    player_type: 'all',
    isDirectPlayer: '',
    time_zone: 'UTC +00:00',
    tenant_id: '',
    owner_id: '',
    action_type: '',
    type: '',
    time_type: 'today',
    internal_error_code: '',
    game_provider: '',
    time_period: '',
    order: 'asc',
    sort_by: 'id'
  };

  constructor(private http: HttpClient,
        public superAdminAuthService: SuperAdminAuthService) {
  }

  getAdminPlayerReport(params: any) {
    return this.http.post(`admin/reports/player`, params);
  }
  getAdminPlayerNCBReport(params: any) {
    return this.http.post(`admin/reports/player/ncb`, params);
  }
  getSuperAdminNCBPlayerReport(params: any) {
    return this.http.post(`super/admin/reports/player/ncb`, params);
  }
  getAdminPlayerNCBReportDownload(params: any) {
    return this.http.post(`admin/reports/player/ncb/download`, params);
  }
  getSuperAdminNCBPlayerReportDownload(params: any) {
    return this.http.post(`super/admin/reports/player/ncb/download`, params);
  }
  getAdminPlayerFinancialReport(params: any) {
    return this.http.post(`admin/reports/player/financial`, params);
  }

  getSuperAdminPlayerReport(params: any) {
    return this.http.post(`super/admin/reports/player`, params);
  }

  getSuperAdminPlayerFinancialReport(params: any) {
    return this.http.post(`super/admin/reports/player/financial`, params);
  }


  getAdminPlayerGameTransactionReport(params: any) {
    return this.http.post(`admin/reports/game/transaction`, params);
  }

  getSuperAdminPlayerGameTransactionReport(params: any) {
    return this.http.post(`super/admin/reports/game/transaction`, params);
  }

  getDownloadAdminPlayerGameTransactionReport(params: any) {
    return this.http.post(`admin/reports/game/transaction/download`, params);
  }

  getDownloadSuperAdminPlayerGameTransactionReport(params: any) {
    return this.http.post(`super/admin/reports/game/transaction/download`, params);
  }

  getGameTransactionDetails(params: any) {
    if (this.superAdminAuthService && this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      return this.http.post(`super/admin/reports/game/transaction/details`, params);
    } else {
      return this.http.post(`admin/reports/game/transaction/details`, params);
    }
  }

  getAdminAgentRevenueReport(params: any) {
    return this.http.post(`admin/reports/agent/revenue`, params);
  }
  getAdminAgentSportRevenueReport(params: any) {
    return this.http.post(`admin/reports/agentsport/revenue`, params);
  }
  getSuperAdminAgentRevenueReport(params: any) {
    return this.http.post(`super/admin/reports/agent/revenue`, params);
  }
  getSuperAdminAgentSportRevenueReport(params: any) {
    return this.http.post(`super/admin/reports/agentsport/revenue`, params);
  }

  getAdminPlayerRevenueReport(params: any) {
    return this.http.post(`admin/reports/player/revenue`, params);
  }

  getAdminPlayerSportRevenueReport(params: any) {
    return this.http.post(`admin/reports/playersport/revenue`, params);
  }

  getSuperAdminPlayerRevenueReport(params: any) {
    return this.http.post(`super/admin/reports/player/revenue`, params);
  }

  getSuperAdminPlayerSportRevenueReport(params: any) {
    return this.http.post(`super/admin/reports/playersport/revenue`, params);
  }

  getBetReports(params: any) {
  // ====================== Super Admin url ==============================
    if(this.superAdminAuthService && this.superAdminAuthService.superAdminTokenValue) {
      return this.http.post(`super/admin/betreport?page=${params.page}`, params) ;
    } else {
      // ====================== Admin url ==============================
      return this.http.post(`admin/betreport?page=${params.page}`, params);
    }
  }

  getBetByESReports(params: any) {
    // ====================== Super Admin url ==============================
    if(this.superAdminAuthService && this.superAdminAuthService.superAdminTokenValue) {
      return this.http.post(`super/admin/betreport?page=${params.page}`, params) ;
    } else {
      // ====================== Admin url ==============================
      return this.http.post(`admin/betreport?page=${params.page}`, params);
    }
  }

  getAllBetReports(params: any) {
    // console.log(params);
    // ====================== Super Admin url ==============================
    if(this.superAdminAuthService && this.superAdminAuthService.superAdminTokenValue) {
      return this.http.post(`super/admin/bet-reports/download`, params) ;
    } else {
      // ====================== Admin url ==============================
      return this.http.post(`admin/bet-reports/download`, params);
    }
  }

  getCurrencyList(tenant_id='') {
    if(tenant_id){
      return this.http.get(`super/admin/tenants/currencies/list?tenant_id=`+tenant_id);
    }else{
      return this.http.get(`admin/tenants/currencies`);
    }
  }

  getAdminUserList() {
    return this.http.get(`admin/reports/users/hierarchy`);
  }

  getSuperAdminUserList(params: any) {
    return this.http.get(`super/admin/reports/users/hierarchy`,{params});
  }

  getSuperAdminAllAgentList(params: any) {
    return this.http.get(`super/admin/reports/agent/all`,{params});
  }

  getSuperTenantsList() {
    return this.http.get(`super/admin/tenants`);
  }

  getSuperTenantsOwnerList(params: any) {
    return this.http.get(`super/admin/reports/owners`,{params});
  }

  getDownloadReport(params: any) {
    return this.http.post(`admin/reports/player/download`, params);
  }

  getDownloadSuperReport(params: any) {
    return this.http.post(`super/admin/reports/player/download`, params);
  }

  getDownloadReportFinancial(params: any) {
    return this.http.post(`admin/reports/player/financial/download`, params);
  }

  getDownloadSuperReportFinancial(params: any) {
    return this.http.post(`super/admin/reports/player/financial/download`, params);
  }

  getAdminPlayerTipReport(params: any) {
    return this.http.post(`admin/reports/tip`, params);
  }
  getSuperAdminTipPlayerReport(params: any) {
    return this.http.post(`super/admin/reports/tip`, params);
  }
  getDownloadSuperTipReport(params: any) {
    return this.http.post(`super/admin/reports/tip/download`, params);
  }
  getDownloadTipReport(params: any) {
    return this.http.post(`super/admin/reports/tip/download`, params);
  }

  getDownloadSuperPlayerRevenueReport(params: any) {
    return this.http.post(`super/admin/reports/player/revenue/download`, params);
  }
  getDownloadPlayerRevenueReport(params: any) {
    return this.http.post(`admin/reports/player/revenue/download`, params);
  }

  getDownloadSuperAgentRevenueReport(params: any) {
    return this.http.post(`super/admin/reports/agent/revenue/download`, params);
  }
  getDownloadAgentRevenueReport(params: any) {
    return this.http.post(`admin/reports/agent/revenue/download`, params);
  }

  getAdminUnifiedReport(params: any) {
    return this.http.post(`admin/reports/unified/transaction`, params);
  }
  getSuperAdminUnifiedReport(params: any) {
    return this.http.post(`super/admin/reports/unified/transaction`, params);
  }

  getDownloadSuperUnifiedReport(params: any) {
    return this.http.post(`super/admin/reports/unified/transaction/download`, params);
  }
  getDownloadUnifiedReport(params: any) {
    return this.http.post(`admin/reports/unified/transaction/download`, params);
  }

  getDownloadSuperBonusStatusReport(params: any) {
    return this.http.post(`super/admin/reports/player/bonusstatus/download`, params);
  }
  getDownloadBonusStatusReport(params: any) {
    return this.http.post(`super/admin/reports/player/bonusstatus/download`, params);
  }

  getAdminPlayerBonusStatusReport(params: any) {
    return this.http.post(`admin/reports/player/bonusstatus`, params);
  }
  getSuperAdminBonusStatusPlayerReport(params: any) {
    return this.http.post(`super/admin/reports/player/bonusstatus`, params);
  }
  
  getAdminGgrReport(params: any) {
    return this.http.post(`admin/reports/ggr`, params);
  }
  getSuperAdminGgrReport(params: any) {
    return this.http.post(`super/admin/reports/ggr`, params);
  }

  getDownloadSuperGgrReport(params: any) {
    return this.http.post(`super/admin/reports/ggr/download`, params);
  }
  getDownloadGgrReport(params: any) {
    return this.http.post(`admin/reports/ggr/download`, params);
  }
}
