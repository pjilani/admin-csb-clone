import { AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import { AdminAuthService } from '../../admin/services/admin-auth.service';
import {SuperAdminAuthService} from '../../super-admin/services/super-admin-auth.service';
import {ReportService} from '../reports.service';
import {PageSizes, TIMEZONE} from 'src/app/shared/constants';
import {Currency} from 'src/app/models';
import { Role } from 'src/app/models';
import Swal from 'sweetalert2';
import { PlayerService } from '../../player/player.service';
declare const $: any;
declare const toastr: any;
import * as moment from 'moment-timezone';
import { PermissionService } from 'src/app/services/permission.service';

@Component({
    templateUrl: './player-report.component.html',
    styleUrls: ['./player-report.component.scss']
})


export class PlayerReportComponent implements OnInit, AfterViewInit, OnDestroy {
  isSubAgentModuleAllowed:boolean = true;
  isPrimaryCurrencyDataEnable:boolean = true;
  roles: string = localStorage.getItem('roles') || '';
  isAgent:boolean = false;
    pageSizes: any[] = PageSizes;
    reports: any[] = [];
    p: number = 1;
    currenciesResultArray: any[]= [];
    TIMEZONE: any[] = TIMEZONE;
    zone: any = '(GMT +00:00) UTC';
    format: string = "YYYY-MM-DD HH:mm:ss";
    currencies: Currency[] = [];
    total: number = 0;
    pageNextTotal: number = 0;
    pageCurrentTotal: number = 0;
    totalBalance: number = 0;
    totalAmountBet: number = 0;
    userList :any[]=[];
    tenantsList :any[]=[];
    tenantsOwnerList :any[]=[];
    totalbetcount: number = 0;
    customDate=false;
    tenant_base_currency:any = '';
    params: any = {
        size: 10,
        page: 1,
        search: '',
        agent_id: '',
        time_type: 'today',
        currency: '',
        isDirectPlayer: '',
        time_zone: 'UTC +00:00',
        time_zone_name: 'UTC +00:00',
        tenant_id: '',
        action_type: '',
        owner_id: 1,
        time_period: [],
        datetime: {},
        order: 'desc',
        sort_by: 'player_id'
    };
    hideShow:boolean = true;
    breadcrumbs: Array<any> = [
        {title: 'Home', path: '/super-admin'},
        {title: 'Player Report', path: '/reports/player-report'},
    ];

    isLoader:boolean = false;
    firstTimeApiCall = false;
    constructor(private ReportService: ReportService,
        public adminAuthService: AdminAuthService,
                private playerService: PlayerService,
        public superAdminAuthService: SuperAdminAuthService,
        public permissionService: PermissionService) {
                this.params = { ...this.params, ...this.ReportService.playerReportParams };
    this.isSubAgentModuleAllowed = (localStorage.getItem('allowedModules')?.includes('subAgent') ? true : false);
    this.isPrimaryCurrencyDataEnable = (localStorage.getItem('allowedModules')?.includes('primaryCurrencyDataEnable') ? true : false);

  }

    ngOnInit(): void {

        if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
            this.getTenantList();
            this.hideShow = false;
        } else {
            // this.getReport();
            this.getCurrencyList();
            this.getAgentUserList();
        }

          if (this.adminAuthService.adminTokenValue && this.adminAuthService.adminTokenValue.length > 0) {
            this.adminAuthService.adminUser.subscribe((user: any) => {
                if(user && user.id) {
                  // this.params = {...this.params, time_zone: user.timezone}
                  setTimeout(() => {
                    const _zone = TIMEZONE.find(t => t.zonename == user.timezone);
                    $(`#time_zone`).val(_zone?.zonename).trigger('change');
                  }, 100);
                }
              });
              if(this.roles) {
                const roles = JSON.parse(this.roles);
                if(roles && roles.findIndex( (role: any) => role === Role.Admin ) == -1 && roles.findIndex( (role: any) => role === Role.Agent ) > -1) {
                  this.isAgent = true;
                }
        
              }

          }

        this.pageNextTotal = this.params.size;
        this.pageCurrentTotal = this.p;
    }

    ngAfterViewInit(): void {
        $('#owner_id option:eq(0)').prop('selected',true);
    }

    getAgentUserList(){

        if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {

            this.ReportService.getSuperAdminUserList({"owner_id":this.params.owner_id,"tenant_id":this.params.tenant_id}).subscribe((res: any) => {
                this.userList = res.record;
            });
        } else {
            this.ReportService.getAdminUserList().subscribe((res: any) => {
                this.userList = res.record;
            });
        }

    }

    getTenantList(){

        if(this.superAdminAuthService.superAdminTokenValue &&
            this.superAdminAuthService.superAdminTokenValue.length > 0) {

            this.ReportService.getSuperTenantsList().subscribe((res: any) => {
                this.tenantsList = res.record;
                if (this.tenantsList.length > 0) {
                    this.params.tenant_id = 0;
                    this.getTenantOwnerList(0);
                    this.getCurrencyList('0');
                }
            });
        }
    }

    getTenantOwnerList(id:number) {
        if(this.superAdminAuthService.superAdminTokenValue &&
            this.superAdminAuthService.superAdminTokenValue.length > 0) {
            this.ReportService.getSuperTenantsOwnerList({id}).subscribe((res: any) => {
                this.tenantsOwnerList = res.record;
                this.params.owner_id = this.tenantsOwnerList.length > 0 ? this.tenantsOwnerList[0].id : '';
                $('#owner_id option:eq(0)').prop('selected',true);
                // this.getReport();
            });
        }
    }

    tenantFilter( evnt : any ){
        this.getTenantOwnerList(evnt);
        this.getCurrencyList(evnt);
        // this.p = 1;
        // this.params.page = this.p;
        this.params.tenant_id = parseInt(evnt);

        if(evnt != 0){
            this.hideShow = true;
            this.getAgentUserList();
          }
          else{
            this.hideShow = false;
            this.userList = [];
          }
        // this.getReport();

    }

    getCurrencyList(evnt=''){
        if(this.superAdminAuthService.superAdminTokenValue &&
            this.superAdminAuthService.superAdminTokenValue.length > 0) {

            this.ReportService.getCurrencyList(evnt).subscribe((res: any) => {
                this.currencies = res.record;
            });

        }else {

            this.ReportService.getCurrencyList().subscribe((res: any) => {
                this.currencies = res.record;
            });
        }
    }

    get getParam(){
        // this.params.owner_id=$('#owner_id').find(':selected');
        // this.params.owner_id=$('#owner_id').select2('data');
        return this.params;
    }
    convertToUTC(inputDate: string, inputTimezone: string): string {
        const date = moment.tz(inputDate, inputTimezone);
        const utcDate = date.utc().format('YYYY-MM-DD HH:mm:ss');
        return utcDate;
      }
    getReport() {
        this.isLoader = true
        this.firstTimeApiCall = true;
        if(this.params.time_period?.start_date && this.params.time_period?.end_date && this.params.time_zone_name && this.params.time_zone_name !== 'UTC +00:00'){
            this.params = {...this.params, datetime : {
              start_date: this.convertToUTC(this.params.time_period.start_date, this.params.time_zone_name), end_date:this.convertToUTC(this.params.time_period.end_date, this.params.time_zone_name)
            }}
          } else {
            this.params = {...this.params, datetime : {
              start_date: this.params.time_period?.start_date,
              end_date:this.params.time_period?.end_date
            }}
          }
        if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {

            this.ReportService.getSuperAdminPlayerReport(this.getParam).subscribe((res: any) => {
                this. getPrepareRecord(res);
                this.isLoader = false
            }, (error:any) => {
        this.isLoader = false

      });
        } else {
            this.ReportService.getAdminPlayerReport(this.getParam).subscribe((res: any) => {
                this. getPrepareRecord(res);
                    this.isLoader = false
            }, (error:any) => {
        this.isLoader = false

      });
        }
    }

    getPrepareRecord(res:any){

        this.reports = res.record.hits.hits;
        this.total = res.record.hits.total.value;
        this.totalbetcount=res.record.aggregations.total_bet_count.value;
        this.currenciesResultArray=res.record.aggregations.currencies.buckets;
        //totalBalance: number = 0;
        // totalAmountBet: number = 0;
        this.totalBalance =res.record.total_balance ?? 0;
        this.tenant_base_currency = res.record.tenant_base_currency
        this.totalAmountBet =res.record.total_bet_amount_in_euro;
        /*if(this.currenciesResultArray.length) {
            for (let i = 0; i <= this.currenciesResultArray.length; i++) {
                if (this.currenciesResultArray[i] && this.currenciesResultArray[i].total_balance) {
                    this.totalBalance += parseFloat(this.currenciesResultArray[i].total_balance.value);
                }
                if (this.currenciesResultArray[i] && this.currenciesResultArray[i].doc_count) {
                    this.totalAmountBet += this.currenciesResultArray[i].doc_count;
                }

            }
        }*/

        let limit = this.params.size;
        let offset = 1;
        if (this.p > 1) {
            offset = limit * (this.p - 1);
        }
        this.pageCurrentTotal = offset;

        this.pageNextTotal = (offset>1?offset:0) + parseInt(limit);
        if(this.total < this.pageNextTotal){
            this.pageNextTotal = this.total;
        }

        this.isLoader = true

    }
    filter(evt: any,dateFilterType: boolean=false) {

        if(this.params.time_type=='custom'){
          this.customDate=true;
          if(dateFilterType){
            setTimeout(() => {    
              $('#time_period').val('');
            }, 10);
          }
        //   if(!dateFilterType)
        //   this.getReport();
        }else{
          this.customDate=false;
        }
      }
    
    submitFilter(){
        this.p = 1;
        this.params = { ...this.params, page: this.p };
        this.getReport();
    }

      selectDateRange(time_period: any) {
        this.params = {...this.params, time_period}
        // this.getReport();
      }

    filterSelectAgent(evt: any) {
        // this.p = 1;
        this.params = {...this.params, agent_id:evt};

        // if(evt)
        // this.getReport();
    }

    filterSelectOwner(evt: any) {
        // this.p = 1;
        this.params = {...this.params, owner_id:evt};
        // this.getReport();
    }

    filterSelectTimeZone(zonename: any) {
        // this.p = 1;
    
        if(zonename) {
          const zone = TIMEZONE.find(t => t.zonename === zonename);
          this.zone = zone?.name;
          this.params = {...this.params, time_zone: zone?.value, time_zone_name: zone?.zonename};
        }else{
          this.zone='';
          this.params = {...this.params, time_zone: this.zone};
        }
    
        // this.getReport();
      }
      

    pageChanged(page: number) {
        this.params = {...this.params, page};
        this.getReport();
    }

    resetFilter() {

        this.p = 1;
        this.params = {
            size: 10,
            page: 1,
            search: '',
            agent_id: '',
            currency: '',
            isDirectPlayer: '',
            time_zone: this.params.time_zone,
            time_zone_name: this.params.time_zone_name,
            tenant_id: this.params.tenant_id,
            owner_id: '',
            datetime: {},
            action_type: '',
            order: 'desc',
            time_type: 'today',
            sort_by: 'creation_date'
        };

        this.getReport();
        $( "#agent_id" ).val('').trigger('change');
        $( "#owner_id" ).val('1').trigger('change');
        // $( "#time_zone" ).val('UTC +00:00').trigger('change');
    }

    download(){
        let timeZoneValue='';
        if(this.params.time_zone){
            timeZoneValue=TIMEZONE.filter(element => element.value === this.params.time_zone)[0].zonename;
        }

        if(timeZoneValue) {
            this.params = {...this.params, time_zone_name: timeZoneValue};
        }
        if(this.params.time_period?.start_date && this.params.time_period?.end_date && this.params.time_zone_name && this.params.time_zone_name !== 'UTC +00:00'){
            this.params = {...this.params, datetime : {
              start_date: this.convertToUTC(this.params.time_period.start_date, this.params.time_zone_name), end_date:this.convertToUTC(this.params.time_period.end_date, this.params.time_zone_name)
            }}
          } else {
            this.params = {...this.params, datetime : {
              start_date: this.params.time_period?.start_date,
              end_date:this.params.time_period?.end_date
            }}
          }

        if (this.superAdminAuthService.superAdminTokenValue &&
            this.superAdminAuthService.superAdminTokenValue.length > 0) {

            this.ReportService.getDownloadSuperReport(this.params).subscribe((res: any) => {
                // window.open(res.record.url);
                window.location.href = res.record.url;

            }), (error: any) => // console.log('Error downloading the file'), //when you use stricter type checking
                () => console.info('File downloaded successfully');

        } else {

            this.ReportService.getDownloadReport(this.params).subscribe((res: any) => {
                // window.open(res.record.url);
                window.location.href = res.record.url;

            }), (error: any) => // console.log('Error downloading the file'), //when you use stricter type checking
                () => console.info('File downloaded successfully');
        }
    }

    updateStatus(report: any, status: number) {
        Swal.fire({
            title: 'Are you sure?',
            text: `You want to ${status == 0 ? 'Deactive' : 'Active'} ${report._source.user_name}!`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: `Yes, ${status == 0 ? 'Deactive' : 'Active'} it!`
            }).then((result) => {
            if (result.isConfirmed) {

                this.playerService.updateAdminPlayerStatus(report._source.player_id, status).subscribe((res: any) => {
                    toastr.success(res.message || 'Player updated successfully' );
                    this.reports = this.reports.map((f: any) => {
                        if(f._source.player_id == report._source.player_id) {
                            f._source.status = status == 0 ? 'Deactive' : 'Active';
                        }
                        return f;
                    });
                });

            }
        });
    }

  setOrder(sort: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p, order: sort.order, sort_by: sort.column };
    this.getReport();
  }

    ngOnDestroy(): void {
        this.ReportService.playerReportParams = { ...this.ReportService.playerReportParams, ...this.params };
    }

}
