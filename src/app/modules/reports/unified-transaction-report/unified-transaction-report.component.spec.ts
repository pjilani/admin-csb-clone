import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnifiedTransactionReportComponent } from './unified-transaction-report.component';

describe('UnifiedTransactionReportComponent', () => {
  let component: UnifiedTransactionReportComponent;
  let fixture: ComponentFixture<UnifiedTransactionReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UnifiedTransactionReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UnifiedTransactionReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
