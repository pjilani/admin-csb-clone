import { Component, OnInit,AfterViewInit, OnDestroy } from '@angular/core';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
import { ReportService } from '../reports.service';
import { PageSizes, TIMEZONE } from 'src/app/shared/constants';
import { AdminAuthService } from '../../admin/services/admin-auth.service';
import {Currency} from 'src/app/models';
import { environment } from 'src/environments/environment';
import { Role } from 'src/app/models';
import * as moment from 'moment-timezone';
import { PermissionService } from 'src/app/services/permission.service';
declare const $: any;

@Component({
  selector: 'app-tip-report',
  templateUrl: './tip-report.component.html',
  styleUrls: ['./tip-report.component.scss']
})
export class TipReportComponent implements OnInit, OnDestroy {
  isSubAgentModuleAllowed:boolean = true;
  isPrimaryCurrencyDataEnable:boolean = true;
  roles: string = localStorage.getItem('roles') || '';
  isAgent:boolean = false;
  repLoader: boolean = true;

  dataTableParams: any = {
    searching: false,
    info: true,
    lengthChange: true,
    autoWidth: false,
    responsive: true,
    "dom": '<"top"lp>t<"bottom"ifp><"clear">',
    lengthMenu: PageSizes.map(m => m.name),
    buttons: []
  };
  tenantBaseCurrency:any = '';
  pageSizes: any[] = PageSizes;
  TIMEZONE: any[] = TIMEZONE;
  zone: any = '(GMT +00:00) UTC';
  reports: any[] = [];
  allReports: any[] = [];
  p: number = 1;
  currencies: Currency[] = [];
  format: string = "YYYY-MM-DD HH:mm:ss";
  userList :any[]=[];
  tenantsList :any[]=[];
  tenantsOwnerList :any[]=[];
  total: number = 0;
  tip: number = 0;
  tipCount: number = 0;
  customDate=false;
  params: any = {
    size: 10,
    page: 1,
    search: '',
    datetime: {},
    agent_id: '',
    currency: '',
    player_type: 'all',
    isDirectPlayer: '',
    time_zone: 'UTC +00:00',
    time_zone_name: 'UTC +00:00',
    tenant_id: '',
    owner_id: '',
    action_type: '',
    time_type: 'today',
    time_period: [],
    // order: 'asc',
    // sort_by: 'transaction.doc_count'
            
  };
  hideShow:boolean = true;
  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Tip Report', path: '/reports/tip-report' },
  ];

  firstTimeApiCall = false;

  constructor(private ReportService: ReportService,
              private adminAuthService: AdminAuthService,
    public superAdminAuthService: SuperAdminAuthService,
    public permissionService: PermissionService) {
    this.params = { ...this.params, ...this.ReportService.tipReportParams };
    this.isSubAgentModuleAllowed = (localStorage.getItem('allowedModules')?.includes('subAgent') ? true : false);
    this.isPrimaryCurrencyDataEnable = (localStorage.getItem('allowedModules')?.includes('primaryCurrencyDataEnable') ? true : false);

  }

  ngOnInit(): void {

    if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.getTenantList();
      this.hideShow = false;
      this.dataTableParams.buttons = [{ extend: 'csv', text: 'Export as CSV', className: 'ctsmcsv mgt btn btn-info mt-0' }];
    } else {
      // this.getReport();
      this.getCurrencyList();
      this.getAgentUserList();
      this.adminAuthService.adminPermissions.subscribe(res => {
        if(this.permissionService.checkPermission('tip_report','export')){
          this.dataTableParams.buttons = [{ extend: 'csv', text: 'Export as CSV', className: 'ctsmcsv mgt btn btn-info mt-0' }];
        }
      });
    }
      if (this.adminAuthService.adminTokenValue && this.adminAuthService.adminTokenValue.length > 0) {
        this.adminAuthService.adminUser.subscribe((user: any) => {
          if(user && user.id) {
            // this.params = {...this.params, time_zone: user.timezone}
            setTimeout(() => {
              const _zone = TIMEZONE.find(t => t.zonename == user.timezone);
              $(`#time_zone`).val(_zone?.zonename).trigger('change');
            }, 100);
          }
        });
      if(this.roles) {
        const roles = JSON.parse(this.roles);
        if(roles && roles.findIndex( (role: any) => role === Role.Admin ) == -1 && roles.findIndex( (role: any) => role === Role.Agent ) > -1) {
          this.isAgent = true;
        }

      }

    }
    

  }

  getAgentUserList(){

    if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.ReportService.getSuperAdminUserList({"owner_id":this.params.owner_id,"tenant_id":this.params.tenant_id}).subscribe((res: any) => {
        this.userList = res.record;
      });
    } else {
      this.ReportService.getAdminUserList().subscribe((res: any) => {
        this.userList = res.record;
      });
    }

  }

  getTenantList(){

    if(this.superAdminAuthService.superAdminTokenValue &&
        this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.ReportService.getSuperTenantsList().subscribe((res: any) => {
        this.tenantsList = res.record;
        if (this.tenantsList.length > 0) {
            this.params.tenant_id = 0;
            this.getTenantOwnerList(0);
            this.getCurrencyList('0');
        }
      });
    }
  }

  getTenantOwnerList(id:number){

    if(this.superAdminAuthService.superAdminTokenValue &&
        this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.ReportService.getSuperTenantsOwnerList({id}).subscribe((res: any) => {
        this.tenantsOwnerList = res.record;
        this.params.owner_id = this.tenantsOwnerList.length > 0 ? this.tenantsOwnerList[0].id : '';
        // this.getReport();
      });
    }
  }

  tenantFilter(evnt: any) {
    // this.p = 1;
    // this.params.page = this.p;
    this.params.tenant_id = parseInt(evnt);
    this.getTenantOwnerList(evnt);
    this.getCurrencyList(evnt);

    if(evnt != 0){
      this.hideShow = true;
      this.getAgentUserList();
    }
    else{
      this.hideShow = false;
      this.userList = [];
    }
  }

  getCurrencyList(evnt=''){
    if(this.superAdminAuthService.superAdminTokenValue &&
        this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.ReportService.getCurrencyList(evnt).subscribe((res: any) => {
        this.currencies = res.record;
      });

    }else {

      this.ReportService.getCurrencyList().subscribe((res: any) => {
        this.currencies = res.record;
      });
    }
  }

  convertToUTC(inputDate: string, inputTimezone: string): string {
    const date = moment.tz(inputDate, inputTimezone);
    const utcDate = date.utc().format('YYYY-MM-DD HH:mm:ss');
    return utcDate;
  }
  
  getReport() {
    this.repLoader = false;
    this.firstTimeApiCall = true;
    $('.ctsmcsv').remove();

    if(this.params.time_period?.start_date && this.params.time_period?.end_date && this.params.time_zone_name && this.params.time_zone_name !== 'UTC +00:00'){
      this.params = {...this.params, datetime : {
        start_date: this.convertToUTC(this.params.time_period.start_date, this.params.time_zone_name), end_date:this.convertToUTC(this.params.time_period.end_date, this.params.time_zone_name)
      }}
    } else {
      this.params = {...this.params, datetime : {
        start_date: this.params.time_period?.start_date,
        end_date:this.params.time_period?.end_date
      }}
    }
    if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.ReportService.getSuperAdminTipPlayerReport(this.params).subscribe((res: any) => {
        this. getPrepareRecord(res);
      }, err => this.repLoader = true );
    } else {
      this.ReportService.getAdminPlayerTipReport(this.params).subscribe((res: any) => {
        this. getPrepareRecord(res);
      }, err => this.repLoader = true );
    }
  }

  getPrepareRecord(res:any) {
    this.allReports = res.record.aggregations.reports.buckets;
    this.reports = this.allReports;
    
    this.total = res.record.aggregations.reports.buckets.length;
    this.tip = res.record.aggregations.total_tip_amount.value;
    this.tipCount = res.record.aggregations.total_tip_count.doc_count;
    this.tenantBaseCurrency = res.record.tenant_base_currency;
    this.repLoader = true;
  }

  filter(evt: any,dateFilterType:boolean=false) {

    if(this.params.time_type=='custom'){
      this.customDate=true;
      if(dateFilterType){
        setTimeout(() => {    
          $('#time_period').val('');
        }, 10);
      }
      // if(!dateFilterType)
      // this.getReport();
    }else{
      this.customDate=false;
    }
  }

  submitFilter(){
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getReport();
  }

  filterSelectAgent(evt: any) {
    // if(evt !=0 ) {
      // this.p = 1;
      this.params = {...this.params, agent_id: evt};

      // if (evt)
      //   this.getReport();
    // }
    this.calTotalTip()
  }

  selectDateRange(time_period: any) {
    this.params = {...this.params, time_period}
    // this.getReport();
  }

  filterSelectOwner(evt: any) {
    // this.p = 1;
    this.params = {...this.params, owner_id:evt};
    // this.getReport();
  }

  filterSelectTimeZone(zonename: any) {
    // this.p = 1;

    if(zonename) {
      const zone = TIMEZONE.find(t => t.zonename === zonename);
      this.zone = zone?.name;
      this.params = {...this.params, time_zone: zone?.value, time_zone_name: zone?.zonename};
    }else{
      this.zone='';
      this.params = {...this.params, time_zone: this.zone};
    }

    // this.getReport();
  }
  

  pageChanged(page: number) {
    //this.params = { ...this.params, page };
   // this.getReport();
  }

    resetFilter() {

        this.p = 1;
        this.params = {
            size: 10,
            page: 1,
            datetime: {},
            search: '',
            agent_id: '',
            currency: '',
            isDirectPlayer: '',
            time_zone: this.params.time_zone,
            time_zone_name: this.params.time_zone_name,
            tenant_id: this.params.tenant_id,
            owner_id: '',
          time_type: 'today',
            // order: 'asc',
            // sort_by: 'transaction.doc_count',
            player_type: 'all'
    
        };

        this.getReport();
        $( "#agent_id" ).val('').trigger('change');

        // this.repLoader = false;
        $('.ctsmcsv').remove();
        
        // $( "#time_zone" ).val('UTC +00:00').trigger('change');
    }
    download(){
        let timeZoneValue='';
        if(this.params.time_zone){
            timeZoneValue=TIMEZONE.filter(element => element.value === this.params.time_zone)[0].zonename;
        }

        if(timeZoneValue) {
            this.params = {...this.params, time_zone_name: timeZoneValue};
        }
        if(this.params.time_period?.start_date && this.params.time_period?.end_date && this.params.time_zone_name && this.params.time_zone_name !== 'UTC +00:00'){
          this.params = {...this.params, datetime : {
            start_date: this.convertToUTC(this.params.time_period.start_date, this.params.time_zone_name), end_date:this.convertToUTC(this.params.time_period.end_date, this.params.time_zone_name)
          }}
        } else {
          this.params = {...this.params, datetime : {
            start_date: this.params.time_period?.start_date,
            end_date:this.params.time_period?.end_date
          }}
        } 

        if (this.superAdminAuthService.superAdminTokenValue &&
            this.superAdminAuthService.superAdminTokenValue.length > 0) {

            this.ReportService.getDownloadSuperTipReport(this.params).subscribe((res: any) => {
                // window.open(res.record.url);
                window.location.href = res.record.url;

            }), (error: any) => // console.log('Error downloading the file'), //when you use stricter type checking
                () => console.info('File downloaded successfully');

        } else {

            this.ReportService.getDownloadTipReport(this.params).subscribe((res: any) => {
                // window.open(res.record.url);
                window.location.href = res.record.url;

            }), (error: any) => // console.log('Error downloading the file'), //when you use stricter type checking
                () => console.info('File downloaded successfully');
        }
    }
  
  setOrder(sort: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p, order: sort.order, sort_by: sort.column };
    this.getReport();
  }

  // currencyFilter(c: any) {
  //   this.repLoader = false;
  //   $('.ctsmcsv').remove();
    
  //   this.reports = this.allReports.filter(f => f.key.includes(c.value));
  //   setTimeout(() => {
  //     this.repLoader = true;
  //   },0);

  //   this.calTotalTip();

  // }

  calTotalTip(){
    this.tip = 0;
    this.tipCount = 0;

    this.reports.forEach((keys : any, vals :any) => {

      this.tipCount +=keys.doc_count;
      this.tip +=keys.tip_amount_in_EUR.value;
    });
  }


  ngOnDestroy(): void {
    this.ReportService.tipReportParams = { ...this.ReportService.tipReportParams, ...this.params };
  }


}
