import { Component, OnInit } from '@angular/core';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
import { ReportService } from '../reports.service';
import { PageSizes, TIMEZONE } from 'src/app/shared/constants';
import { AdminAuthService } from '../../admin/services/admin-auth.service';
import {Currency} from 'src/app/models';
import { PermissionService } from 'src/app/services/permission.service';

declare const $: any;

@Component({
  templateUrl: './sport-player-revenue-report.component.html'
})
  
export class SportPlayerRevenueReportComponent implements OnInit {

  aggregations: any;
  
  repLoader: boolean = true;

  totalBet: number = 0;
  totalWin: number = 0;
  gameProvider: any[] = [];
  gameType: any[] = [];
  dataTableParams: any = {
    searching: false,
    info: true,
    lengthChange: true,
    autoWidth: false,
    responsive: true,
    "dom": '<"top"lp>t<"bottom"ip><"clear">',
    lengthMenu: PageSizes.map(m => m.name),
    buttons: []
  };
  
  pageSizes: any[] = PageSizes;
  TIMEZONE: any[] = TIMEZONE;
  zone: any = '(GMT +00:00) UTC';
  
  reports: any[] = [];
  allReports: any[] = [];

  p: number = 1;
  currencies: Currency[] = [];
  format: string = "YYYY-MM-DD";
  userList :any[]=[];
  tenantsList :any[]=[];
  tenantsOwnerList :any[]=[];
  total: number = 0;
  tip: number = 0;
  tipCount: number = 0;
  customDate=false;
  params: any = {
    size: 10,
    page: 1,
    search: '',
    agent_id: '',
    currency: '',
    player_type: 'all',
    isDirectPlayer: '',
    time_zone: 'UTC +00:00',
    time_zone_name: 'UTC +00:00',
    tenant_id: '',
    owner_id: '',
    action_type: '',
    time_type: 'today',
    time_period: [],
    game_provider:'',
    game_type:'',
    table_id:'',
    // order: 'asc',
    // sort_by: 'player_name'
  };

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Sport Player Revenue Report', path: '/reports/sport-player-revenue-report' },
  ];

  firstTimeApiCall = false;

  constructor(private ReportService: ReportService,
              private adminAuthService: AdminAuthService,
    public superAdminAuthService: SuperAdminAuthService,
    public permissionService: PermissionService) {
    this.params = { ...this.params, ...this.ReportService.playerSportRevenueParams };
  }

  ngOnInit(): void {

    if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.getTenantList();
      this.dataTableParams.buttons = [{ extend: 'csv', text: 'Export as CSV', className: 'ctsmcsv mgt btn btn-info mt-0' }];
    } else {
      // this.getReport();
      this.getCurrencyList();
      this.adminAuthService.adminPermissions.subscribe(res => {
        if(this.permissionService.checkPermission('sport_player_revenue_report','export')){
          this.dataTableParams.buttons = [{ extend: 'csv', text: 'Export as CSV', className: 'ctsmcsv mgt btn btn-info mt-0' }];
        }
      });
    }

    this.getAgentUserList();

  }

  getAgentUserList(){

    if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.ReportService.getSuperAdminUserList({"owner_id":this.params.owner_id,"tenant_id":this.params.tenant_id}).subscribe((res: any) => {
        this.userList = res.record;
      });
    } else {
      this.ReportService.getAdminUserList().subscribe((res: any) => {
        this.userList = res.record;
      });
    }

  }

  getTenantList(){

    if(this.superAdminAuthService.superAdminTokenValue &&
        this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.ReportService.getSuperTenantsList().subscribe((res: any) => {
        this.tenantsList = res.record;
        if (this.tenantsList.length > 0) {
            this.params.tenant_id = this.tenantsList[0].id;
            this.getTenantOwnerList(this.tenantsList[0].id);
            this.getCurrencyList(this.tenantsList[0].id);
        }
      });
    }
  }

  getTenantOwnerList(id:number){

    if(this.superAdminAuthService.superAdminTokenValue &&
        this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.ReportService.getSuperTenantsOwnerList({id}).subscribe((res: any) => {
        this.tenantsOwnerList = res.record;
        this.params.owner_id = this.tenantsOwnerList.length > 0 ? this.tenantsOwnerList[0].id : '';
        // this.getReport();
      });
    }
  }

  tenantFilter(evnt: any) {
    // this.p = 1;
    // this.params.page = this.p;
    this.params.tenant_id = evnt;
    this.getTenantOwnerList(evnt);
    this.getCurrencyList(evnt);
  }

  getCurrencyList(evnt=''){
    if(this.superAdminAuthService.superAdminTokenValue &&
        this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.ReportService.getCurrencyList(evnt).subscribe((res: any) => {
        this.currencies = res.record;
      });

    }else {

      this.ReportService.getCurrencyList().subscribe((res: any) => {
        this.currencies = res.record;
      });
    }
  }

  // currencyFilter(c: any) {
  //   this.repLoader = false;
  //   $('.ctsmcsv').remove();
    
  //   this.reports = this.allReports.filter(f => f.key.includes(c.value.toLocaleLowerCase()));
  //   setTimeout(() => {
  //     this.repLoader = true;
  //   },0);
  // }

  getReport() {
    this.repLoader = false;
    this.firstTimeApiCall = true;
    $('.ctsmcsv').remove();

    if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.ReportService.getSuperAdminPlayerSportRevenueReport(this.params).subscribe((res: any) => {
        this. getPrepareRecord(res);
      }, err => this.repLoader = true);
    } else {
      this.ReportService.getAdminPlayerSportRevenueReport(this.params).subscribe((res: any) => {
        this. getPrepareRecord(res);
      }, err => this.repLoader = true);
    }
  }

  getPrepareRecord(res: any) {
    
    this.allReports = res.record.aggregations.reports.buckets;
    this.reports = this.allReports;

    /*if(res.record.resultGameProvider)
      this.gameType = res.record.resultGameProvider.game_provider.buckets;

    if(res.record.resultGameProvider)
      this.gameProvider = res.record.resultGameProvider.game_type.buckets;*/
    // console.log(this.allReports);

    this.total = res.record.aggregations.reports.buckets.length;
    // this.tip = res?.record?.aggregations?.total_tip_amount?.value;
    // this.tipCount = res?.record?.aggregations?.total_tip_count?.doc_count;

    this.aggregations = res.record.aggregations;
    this.repLoader = true;
    
    this.totalBet = res.record.aggregations.total_bets_in_EUR.bet_amount.value;
    this.totalWin = res.record.aggregations.total_wins_in_EUR.win_amount.value;

  }

  // calTotals() {
    // const total_bet_in_primary_currency = this.aggregations.total_bets_in_EUR.bet_amount.value;
    // const total_refund_in_primary_currency = this.aggregations.total_refund_in_EUR.refund_amount.value;
    // const total_win_in_primary_currency = this.aggregations.total_wins_in_EUR.win_amount.value;
    // const total_bonus_claim_in_primary_currency = this.aggregations.total_bonus_withdraw_in_EUR.bonus_withdraw_amount.value;
    // const total_bonus_withdraw_in_primary_currency = this.aggregations.total_bonus_withdraw_in_EUR.bonus_withdraw_amount.value;

    // this.totalGGR = total_bet_in_primary_currency - total_refund_in_primary_currency - total_win_in_primary_currency;
    // this.totalNGR = this.totalGGR - (total_bonus_claim_in_primary_currency + total_bonus_withdraw_in_primary_currency);

    // this.calTotal();

  // }

  filter(evt: any,dateFilterType:boolean=false) {

    if(this.params.time_type=='custom'){
      this.customDate=true;
      if(dateFilterType){
        setTimeout(() => {    
          $('#time_period').val('');
        }, 10);
      }
    }else{
      this.customDate=false;
    }

  }

  submitFilter(){
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getReport();
  }

  filterSelectAgent(evt: any) {
    // this.p = 1;
    this.params = {...this.params, agent_id:evt};

    // if(evt)
    // this.getReport();    
  }

  selectDateRange(time_period: any) {
    this.params = {...this.params, time_period}
    // this.getReport();
  }

  filterSelectOwner(evt: any) {
    // this.p = 1;
    this.params = {...this.params, owner_id:evt};
    // this.getReport();
  }

  filterSelectTimeZone(evt: any) {
    // this.p = 1;
    let timeZoneValue='';
    if(evt){
      timeZoneValue=TIMEZONE.filter(element => element.value === evt)[0].zonename;
    }
    if(timeZoneValue) {
      this.zone=TIMEZONE.filter(element => element.value === evt)[0].name;
      this.params = {...this.params, time_zone:evt, time_zone_name: timeZoneValue};
    }else{
      this.zone='';
      this.params = {...this.params, time_zone:evt};
    }

    // if(evt)
    // this.getReport();
  }

  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getReport();
  }

    resetFilter() {

        this.p = 1;
        this.params = {
            size: 10,
            page: 1,
            search: '',
            agent_id: '',
            currency: '',
            isDirectPlayer: '',
            time_zone: this.params.time_zone,
            time_zone_name: this.params.time_zone_name,
            tenant_id: this.params.tenant_id,
            owner_id: '',
            time_type: 'today',
            player_type: 'all',
            // order: 'asc',
            // sort_by: 'player_name'
    
        };

        this.getReport();
        $( "#agent_id" ).val('').trigger('change');
        // $( "#time_zone" ).val('UTC +00:00').trigger('change');
    }
    download(){
        let timeZoneValue='';
        if(this.params.time_zone){
            timeZoneValue=TIMEZONE.filter(element => element.value === this.params.time_zone)[0].zonename;
        }

        if(timeZoneValue) {
            this.params = {...this.params, time_zone_name: timeZoneValue};
        }

        if (this.superAdminAuthService.superAdminTokenValue &&
            this.superAdminAuthService.superAdminTokenValue.length > 0) {

            this.ReportService.getDownloadSuperPlayerRevenueReport(this.params).subscribe((res: any) => {
                // window.open(res.record.url);
                window.location.href = res.record.url;

            }), (error: any) => // console.log('Error downloading the file'), //when you use stricter type checking
                () => console.info('File downloaded successfully');

        } else {

            this.ReportService.getDownloadPlayerRevenueReport(this.params).subscribe((res: any) => {
                // window.open(res.record.url);
                window.location.href = res.record.url;

            }), (error: any) => // console.log('Error downloading the file'), //when you use stricter type checking
                () => console.info('File downloaded successfully');
        }
    }
  
  setOrder(sort: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p, order: sort.order, sort_by: sort.column };
    this.getReport();
  }
    
  ngOnDestroy(): void {
    this.ReportService.playerSportRevenueParams = { ...this.ReportService.playerSportRevenueParams, ...this.params };
  }

  searchPlayer(transaction: any) {
    const search = transaction.key.split('-')[0];
    // console.log('search ', search);

    this.repLoader = false;
    $('.ctsmcsv').remove();
    
    this.reports = this.allReports.filter(f => f.key.includes(search));
    setTimeout(() => {
      this.repLoader = true;
    }, 0);

    // this.p = 1;
    this.params = {...this.params, search };
    // this.getReport();
  }

  searchAgent(transaction: any) {
    const search = transaction.key.split('-')[2];
    // console.log('search ', search);

    this.repLoader = false;
    $('.ctsmcsv').remove();
    
    this.reports = this.allReports.filter(f => f.key.includes(search));
    setTimeout(() => {
      this.repLoader = true;
    }, 0);

    // this.p = 1;
    this.params = {...this.params, search };
    // this.getReport();
  }

  searchFilter(evt: any) {

    this.repLoader = false;
    $('.ctsmcsv').remove();
    
    this.reports = this.allReports.filter(f => f.key.includes(evt.target.value));
    setTimeout(() => {
      this.repLoader = true;
    }, 0);
    // this.p = 1;
    this.params = {...this.params, search: evt.target.value };
  }

}
