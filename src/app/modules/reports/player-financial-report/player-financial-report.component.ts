import {Component, OnDestroy, OnInit} from '@angular/core';
import {SuperAdminAuthService} from '../../super-admin/services/super-admin-auth.service';
import {ReportService} from '../reports.service';
import {AdminAuthService} from '../../admin/services/admin-auth.service';
import {PageSizes, TIMEZONE} from 'src/app/shared/constants';
import {Currency} from 'src/app/models';
import {environment} from 'src/environments/environment';
import { Role } from 'src/app/models';
import * as moment from 'moment-timezone';
import { PermissionService } from 'src/app/services/permission.service';
declare const $: any;
@Component({
    selector: 'app-player-financial-report',
    templateUrl: './player-financial-report.component.html',
    styleUrls: ['./player-financial-report.component.scss']
})


export class PlayerFinancialReportComponent implements OnInit, OnDestroy {
  isSubAgentModuleAllowed:boolean = true;
  isPrimaryCurrencyDataEnable:boolean = true;

    roles: string = localStorage.getItem('roles') || '';
    isAgent:boolean = false;
    pageSizes = PageSizes;
    reports: any[] = [];
    userList: any[] = [];
    tenantsOwnerList: any[] = [];
    tenantsList: any[] = [];
    TIMEZONE: any[] = TIMEZONE;
    currenciesResultArray: any[] = [];
    p: number = 1;
    zone: any = '(GMT +00:00) UTC';
    currencies: Currency[] = [];
    format: string = "YYYY-MM-DD HH:mm:ss";
    totalbetcount: number = 0;
    total: number = 0;
    pageNextTotal: number = 0;
    pageCurrentTotal: number = 0;
    totalDeposit: number = 0;
    totalWithdraw: number = 0;
    totalWithdrawCancel: number = 0.00;
    customDate=false;

    isLoader: boolean = false;
    firstTimeApiCall: boolean = false;
    dataTableParams: any = {
        searching: false,
        info: true,
        lengthChange: true,
        autoWidth: false,
        responsive: true,
        "dom": '<"top"lp>t<"bottom"ip><"clear">',
        lengthMenu: PageSizes.map(m => m.name),
        buttons: [{ extend: 'csv', text: 'Export as CSV', className: 'ctsmcsv mgt btn btn-info mt-0' }]
      };

    params: any = {
        size: 10,
        page: 1,
        search: '',
        agent_id: '',
        currency: '',
        owner_id: '',
        time_zone: 'UTC +00:00',
        time_zone_name: 'UTC +00:00',
        tenant_id: '',
        action_type: '',
        status: '',
        time_period: {},
        datetime: {},
        isDirectPlayer: 'all',
        time_type: 'today',
        order: 'asc',
        sort_by: 'player_details.player_id'
    };
    tenantBaseCurrency:any = '';
    hideShow:boolean = true;
    breadcrumbs: Array<any> = [
        {title: 'Home', path: '/super-admin'},
        {title: 'Player Financial Report', path: '/reports/player-financial-report'},
    ];

    constructor(private ReportService: ReportService,
                private adminAuthService: AdminAuthService,
        public superAdminAuthService: SuperAdminAuthService,
        public permissionService: PermissionService) {
        this.params = { ...this.params, ...this.ReportService.playerFinanceReportParams };
        this.isSubAgentModuleAllowed = (localStorage.getItem('allowedModules')?.includes('subAgent') ? true : false);
        this.isPrimaryCurrencyDataEnable = (localStorage.getItem('allowedModules')?.includes('primaryCurrencyDataEnable') ? true : false);

    }

    ngOnInit(): void {

        if (this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
            this.getTenantList();
            this.hideShow = false;
        } else {
            // this.getReport();
            this.getCurrencyList();
            this.getAgentUserList();
        }
  
        if (this.adminAuthService.adminTokenValue && this.adminAuthService.adminTokenValue.length > 0) {
            this.adminAuthService.adminUser.subscribe((user: any) => {
                if(user && user.id) {
                  // this.params = {...this.params, time_zone: user.timezone}
                  setTimeout(() => {
                    const _zone = TIMEZONE.find(t => t.zonename == user.timezone);
                    $(`#time_zone`).val(_zone?.zonename).trigger('change');
                  }, 100);
                }
              });
          if(this.roles) {
            const roles = JSON.parse(this.roles);
            if(roles && roles.findIndex( (role: any) => role === Role.Admin ) == -1 && roles.findIndex( (role: any) => role === Role.Agent ) > -1) {
              this.isAgent = true;
            }
    
          }

        }
        this.pageNextTotal = this.params.size;
        this.pageCurrentTotal = this.p;

        

    }

    getTenantOwnerList(id: number) {

        if (this.superAdminAuthService.superAdminTokenValue &&
            this.superAdminAuthService.superAdminTokenValue.length > 0) {
            this.ReportService.getSuperTenantsOwnerList({id}).subscribe((res: any) => {
                this.tenantsOwnerList = res.record;
                this.params.owner_id = this.tenantsOwnerList.length > 0 ? this.tenantsOwnerList[0].id : '';
                // this.getReport();
            });
        }
    }


    getTenantList() {

        if (this.superAdminAuthService.superAdminTokenValue &&
            this.superAdminAuthService.superAdminTokenValue.length > 0) {

            this.ReportService.getSuperTenantsList().subscribe((res: any) => {
                this.tenantsList = res.record;
                if (this.tenantsList.length > 0) {
                    this.params.tenant_id = 0;
                    this.getTenantOwnerList(0);
                    this.getCurrencyList('0');
                }
            });
        }
    }

    selectAgent(searchValue: any) {

        // this.p = 1;
        this.params = {...this.params, search:searchValue};

        if (searchValue) {

            setTimeout(() => {
                $('#search').val(searchValue).trigger('keyup');
            }, 0);
            // this.getReport();
        }
    }

    ngAfterViewInit(): void {
        setTimeout(() => {
            $('#time_period').val('today');
        }, 100);
    }

    selectDateRange(time_period: any) {
        this.params = {...this.params, time_period}
        // this.getReport();
    }
    convertToUTC(inputDate: string, inputTimezone: string): string {
        const date = moment.tz(inputDate, inputTimezone);
        const utcDate = date.utc().format('YYYY-MM-DD HH:mm:ss');
        return utcDate;
      }
    getReport() {
      this.isLoader = true;
      this.firstTimeApiCall = true;

      $('.ctsmcsv').remove();
      if(this.params.time_period?.start_date && this.params.time_period?.end_date && this.params.time_zone_name && this.params.time_zone_name !== 'UTC +00:00'){
        this.params = {...this.params, datetime : {
          start_date: this.convertToUTC(this.params.time_period.start_date, this.params.time_zone_name),
            end_date:this.convertToUTC(this.params.time_period.end_date, this.params.time_zone_name)
        }}
      } else{
            this.params = {...this.params, datetime : { start_date: this.params.time_period?.start_date, end_date:this.params.time_period?.end_date}}
        }
        if (this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {

            this.ReportService.getSuperAdminPlayerFinancialReport(this.params).subscribe((res: any) => {
                this.getPrepareRecord(res);
            }, (error:any) => {
        this.isLoader = false

      });
        } else {
            this.ReportService.getAdminPlayerFinancialReport(this.params).subscribe((res: any) => {
                this.getPrepareRecord(res);
            }, (error:any) => {
        this.isLoader = false

      });
        }


    }

    getPrepareRecord(res: any) {

        this.reports = res.record.hits.hits;
        this.tenantBaseCurrency = res.record.tenant_base_currency
        this.total = res.record.hits.total.value;
        this.totalDeposit = res.record.aggregations.total_deposit.total.value;
        
        this.totalWithdraw =parseFloat(res.record.aggregations.total_withdraw.total.value);
        this.totalWithdrawCancel =parseFloat(res.record.aggregations.total_withdraw_cancel.total.value);
            // this.currenciesResultArray=res.record.aggregations.currencies.buckets;
        //totalBalance: number = 0;
        // totalAmountBet: number = 0;

        let limit = this.params.size;
        let offset = 1;
        if (this.p > 1) {
            offset = limit * (this.p - 1);
        }
        this.pageCurrentTotal = offset;

        this.pageNextTotal = (offset>1?offset:0) + parseInt(limit);
        if(this.total < this.pageNextTotal){
            this.pageNextTotal = this.total;
        }
        this.isLoader = false
    }


    filterSelectOwner(evt: any) {
        // this.p = 1;
        this.params = {...this.params, owner_id: evt};
        this.getAgentUserList();
        // this.getReport();
    }

    getAgentUserList(tenantid='') {

        if (this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {

            if(!tenantid){
                tenantid=this.params.tenant_id;
            }
            this.ReportService.getSuperAdminUserList({"owner_id":this.params.owner_id,"tenant_id":tenantid}).subscribe((res: any) => {
                this.userList = res.record;
            });
        } else {
            this.ReportService.getAdminUserList().subscribe((res: any) => {
                this.userList = res.record;
            });
        }

    }

    filterSelectAgent(evt: any) {
        // this.p = 1;
        this.params = {...this.params, agent_id: evt};

        // if(evt)
        // this.getReport();
    }

    filter(evt: any,dateFilterType: boolean=false) {

        if(this.params.time_type=='custom'){
            this.customDate=true;
            if(dateFilterType){
                setTimeout(() => {    
                  $('#time_period').val('');
                }, 10);
              }
            // if(!dateFilterType)
            // this.getReport();
        }else{
            this.customDate=false;
        }
    }

    submitFilter(){
        this.p = 1;
        this.params = { ...this.params, page: this.p };
        this.getReport();
    }

    pageChanged(page: number) {
        this.params = {...this.params, page};
        this.getReport();
    }

    resetFilter() {
        this.p = 1;
        this.params = {
            size: 10,
            page: 1,
            search: '',
            agent_id: '',
            currency: '',
            player_type: '',
            datetime:{},
            time_zone: this.params.time_zone,
            time_zone_name: this.params.time_zone_name,
            tenant_id: '',
            owner_id: '',
            action_type: '',
            status: '',
            time_period: '',
            time_type: 'today',
            isDirectPlayer: 'all',
            order: 'asc',
            sort_by: 'created_at'
        };
        $( "#agent_id" ).val('').trigger('change');
        // $( "#time_zone" ).val('UTC +00:00').trigger('change');
        this.getReport();
    }

    filterSelectTimeZone(zonename: any) {
        // this.p = 1;
    
        if(zonename) {
          const zone = TIMEZONE.find(t => t.zonename === zonename);
          this.zone = zone?.name;
          this.params = {...this.params, time_zone: zone?.value, time_zone_name: zone?.zonename};
        }else{
          this.zone='';
          this.params = {...this.params, time_zone: this.zone};
        }
    
        // this.getReport();
      }
      

    tenantFilter(evnt: any) {
        // this.p = 1;
        // this.params.page = this.p;
        this.params.tenant_id = parseInt(evnt);
        this.getTenantOwnerList(evnt);
        this.getCurrencyList(evnt);
        // this.getAgentUserList(evnt);
        if(evnt != 0){
            this.hideShow = true;
            this.getAgentUserList();
          }
          else{
            this.hideShow = false;
            this.userList = [];
          }
    }

    getCurrencyList(evnt = '') {
        if (this.superAdminAuthService.superAdminTokenValue &&
            this.superAdminAuthService.superAdminTokenValue.length > 0) {

            this.ReportService.getCurrencyList(evnt).subscribe((res: any) => {
                this.currencies = res.record;
            });

        } else {

            this.ReportService.getCurrencyList().subscribe((res: any) => {
                this.currencies = res.record;
            });
        }
    }

    download(){
        let timeZoneValue='';
        if(this.params.time_zone){
            timeZoneValue=TIMEZONE.filter(element => element.value === this.params.time_zone)[0].zonename;
        }

        if(timeZoneValue) {
            this.params = {...this.params, time_zone_name: timeZoneValue};
        }

        if (this.superAdminAuthService.superAdminTokenValue &&
            this.superAdminAuthService.superAdminTokenValue.length > 0) {

            this.ReportService.getDownloadSuperReportFinancial(this.params).subscribe((res: any) => {
                // window.open(res.record.url);
                window.location.href = res.record.url;

            }), (error: any) => // console.log('Error downloading the file'), //when you use stricter type checking
                () => console.info('File downloaded successfully');

        } else {

            this.ReportService.getDownloadReportFinancial(this.params).subscribe((res: any) => {
                // window.open(res.record.url);
                window.location.href = res.record.url;

            }), (error: any) => // console.log('Error downloading the file'), //when you use stricter type checking
                () => console.info('File downloaded successfully');
        }

    }

  setOrder(sort: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p, order: sort.order, sort_by: sort.column };
    this.getReport();
  }
    
    ngOnDestroy(): void {
        this.ReportService.playerFinanceReportParams = { ...this.ReportService.playerFinanceReportParams, ...this.params };
    }


}
