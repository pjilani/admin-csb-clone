import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerFinancialReportComponent } from './player-financial-report.component';

describe('PlayerFinancialReportComponent', () => {
  let component: PlayerFinancialReportComponent;
  let fixture: ComponentFixture<PlayerFinancialReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlayerFinancialReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerFinancialReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
