import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Admin, Currency, Tenant } from 'src/app/models';
import { encryptPassword, truncateString } from 'src/app/services/utils.service';
import { Roles, TENENT_PERMISSION, TIMEZONE } from 'src/app/shared/constants';
import { CustomValidators } from 'src/app/shared/validators';
import { AdminAuthService } from '../../admin/services/admin-auth.service';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
import { TenantService } from 'src/app/modules/tenant/tenant.service';
import { AgentService } from '../agent.service';
import { Role } from 'src/app/models';

declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './edit-admin.component.html',
  styleUrls: ['./edit-admin.component.scss']
})

export class EditAdminComponent implements OnInit, AfterViewInit {

  // queryParams: any;

  hideShowPass: boolean = false;

  tenantId: number = 0;
  adminId: number = 0;
  admin! : Admin;
  tenant!: Tenant;
  phoneMinLen: number = 5;
  
  currencies: Currency[] = [];
  TIMEZONE: any[] = TIMEZONE;
  tenantPermission = TENENT_PERMISSION

  roles: any[] = [ { name: 'Agent', value: 2 } ];
  permissionRoles: any;

  permissionLabels = [
    {label: 'R', value: 'Read'},
    {label: 'C', value: 'Create'},
    {label: 'U', value: 'Update'},
    {label: 'D', value: 'Delete'},
    {label: 'T', value: 'Toggle'},    
  ]
  
  adminForm: FormGroup | any;
  submitted: boolean = false;
  adminLoader: boolean = false;
  roleStored: string = localStorage.getItem('roles') || '';
  agentName: boolean = false;

  title: string = 'Create'
  showIpWhitelistType: boolean = false;
  showManualIpWhitelist: boolean = false;
  breadcrumbs: Array<any> = [ ];
  
  constructor(private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private agentService: AgentService,
    private tenantService: TenantService,
    public superAdminAuthService: SuperAdminAuthService,
    public adminAuthService: AdminAuthService
    ) {
     this.tenantId = this.route.snapshot.params['tenantId'];
     this.adminId = this.route.snapshot.params['adminId'];
     
    this.adminForm = this.formBuilder.group({
      tenant_id: [ '', Validators.required ],
      email: ['', [ Validators.required, Validators.pattern(CustomValidators.emailRegEx) ]],
      encrypted_password: ['', [ Validators.pattern(CustomValidators.passwordRegex) ]],
      agent_name: ['', [ Validators.required, Validators.minLength(3), Validators.maxLength(15) ]],
      first_name: ['', [ Validators.required, Validators.minLength(3), Validators.maxLength(15) ]],
      last_name: ['', [ Validators.required, Validators.minLength(3), Validators.maxLength(15) ]],
      phone: ['', [ Validators.required, Validators.minLength(this.phoneMinLen), Validators.pattern(CustomValidators.phoneRegex) ]],
      role: ['', [ Validators.required ]],
      agent_type: ['',  Validators.required ],
      permissionRole: [''],
      currency_id: ['', [ Validators.required ]],
      time_zone: ['', [Validators.required]],
      status: [ true ],
      phone_verified: [false],
      kyc_regulated: [false],
      enableWhitelist: [''],
      ipWhitelistType: [''],
      ip_address_group: this.formBuilder.group({
        ip_address: this.formBuilder.array([])
      }),
      // permissions: this.formBuilder.group({})
    });

    if(this.adminId > 0) {
      this.title = 'Edit';
      this.getAdmin();

      const currenciesControl = this.adminForm.get('currency_id');
      currenciesControl.setValidators([]);
      currenciesControl.updateValueAndValidity();

      const roleControl = this.adminForm.get('role');
      roleControl.setValidators([]);
      roleControl.updateValueAndValidity();

      const permissionRoleControl = this.adminForm.get('permissionRole');
      permissionRoleControl.setValidators([]);
      permissionRoleControl.updateValueAndValidity();

    } else {
      const passControl = this.adminForm.get('encrypted_password');
      passControl.setValidators([Validators.required, Validators.pattern(CustomValidators.passwordRegex)]); 
      passControl.updateValueAndValidity();
    }

    this.f.tenant_id.setValue(this.tenantId);


    // this.route.queryParams.subscribe(params => {
    //   if(params.admin) {
    //     this.queryParams = params;
    //     this.roles = Roles;
    //   }
    // });

  }

  ngOnInit(): void {
    
    if (this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {

      this.roles = [{ name: 'Owner', value: 1 }];

      this.f.role.setValue([this.roles[0].value.toString()])
      // this.buildPermissionForms(this.tenantPermission)

      const permissionRoleControl = this.adminForm.get('permissionRole');
      permissionRoleControl.setValidators([]);
      permissionRoleControl.updateValueAndValidity();
      this.getTenant();
    } else {

      if(this.isAgent){
        const permissionRoleControl = this.adminForm.get('permissionRole');
        permissionRoleControl.setValidators([]);
        permissionRoleControl.updateValueAndValidity();
      }
      else{
        const permissionRoleControl = this.adminForm.get('permissionRole');
        permissionRoleControl.setValidators([Validators.required]);
        permissionRoleControl.updateValueAndValidity();
      }
      // this.getAdminTenant();
      this.getPermissionRoles();
      this.breadcrumbs = [
        { title: 'Home', path: '/' },
        { title: 'Agents', path: '/agents' },
        { title: this.title, path: `/agents` }
      ];

      this.f.role.setValue([this.roles[0].value.toString()])

      this.adminAuthService?.walletData.subscribe((wallets: any) => {
        
        this.currencies = wallets || [];
        this.currencies = this.currencies.map((m: any) => {
          m.id = m.currency_id;
          m.name = m.currency_name;
          return m;
        });

        if (this.adminId > 0 && this.admin?.wallets) {
          const wc = this.admin.wallets.map(m => m.currency_id);
          setTimeout(() => {
            // $("#currency").val(wc).trigger('change');
            for (const w of wc) {
              $(`#currency>option[value=${w}]`).attr('disabled', 'disabled');
            }
          }, 100);


          this.changeCurrency(wc);
        }
        
        
        // if(Object.keys(user).length){
        //   this.buildPermissionForms((user?.permissions) ? JSON.parse(user?.permissions) : this.tenantPermission)
        // }
        

        // else {
        //   this.changeCurrency(this.currencies[0].id);
        //   }
        
      });

    }

    console.log(!(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) && !this.isAgent)
  }

  get ip_address() {
    const control = <FormArray>(<FormGroup>this.adminForm.get('ip_address_group')).get('ip_address');
    return control;
  }
  get ip_address_group(): FormArray {
    return this.adminForm.get('ip_address_group').get('ip_address') as FormArray;
  }
 
  getIpAddressFormArr(index: any): FormGroup {
    const formGroup = this.ip_address_group.controls[index] as FormGroup;
    return formGroup;
  }

  newIpAddressKeys(val: any = ''): any {
    return this.formBuilder.group({
      // ip_address: [(val != '' ? val : ''), [Validators.required,Validators.pattern('^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$')]],
      ip_address: [(val != '' ? val : '')],
    })
  }

  addNewKey() {
    this.ip_address.push(this.newIpAddressKeys());
  }

  removeIpAddress(i: number) {
    this.ip_address.removeAt(i);
  }

  ngAfterViewInit(): void {
    // $("#currency").change(function (this: any) {
    //   $.each(this.options, function (i: any, item: any) {
    //       if (item.selected) {
    //           $(item).prop("disabled", true); 
    //       }else {
    //           $(item).prop("disabled", false);
    //       }
    //     });
    // });
  }

  getAdmin() {
    if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.agentService.getSuperAdminAgent(this.adminId).subscribe((res: any) => {
        this.admin = res.record;
        this.setValue();
      });
    } else {
      this.agentService.getAdminAgent(this.adminId).subscribe((res: any) => {
        this.admin = res.record;
        this.setValue();
      });
    }
  }

  setValue() {

    // if(this.admin?.permissions){
    //   const permissions = JSON.parse(this.admin?.permissions);


    // for (const action in permissions) {
    //   permissions[action] = (permissions[action] as []).reduce(
    //     (perms, perm) => ({ ...perms, [perm]: true }),
    //     {}
    //   );
    // }


    // this.admin.permissions = permissions;
    // }

    const wc = this.admin.wallets.map(m => m.currency_id);


    this.admin['ip_whitelist'] = JSON.parse(this.admin['ip_whitelist']);
    for (let x in this.admin['ip_whitelist']) {
      this.ip_address.push(this.newIpAddressKeys(this.admin['ip_whitelist'][x]));
    }

    console.log(this.admin.rolesids);
    
    
    const valuesToPatch:any = {
      email: this.admin.email,
      agent_name: this.admin.agent_name,
      first_name: this.admin.first_name,
      last_name: this.admin.last_name,
      phone: this.admin.phone,
      role: this.getRoles(this.admin.rolesids),
      permissionRole: this.admin.permissionroleid,
      currency_id: wc,
      phone_verified: this.admin.phone_verified,
      status: this.admin.active,
      kyc_regulated: this.admin.kyc_regulated,
      enableWhitelist: this.admin.is_applied_ip_whitelist,
      ipWhitelistType: this.admin.ip_whitelist_type,
      time_zone: this.admin.timezone,
      agent_type: this.admin.agent_type,
    }

    // if (this.admin?.permissions) {
    //   valuesToPatch.permissions = this.admin.permissions;
    // }
    this.agentName = true;
    this.adminForm.patchValue(valuesToPatch);

    if(this.admin.is_applied_ip_whitelist){
        this.showIpWhitelistType = true;
        if(this.admin.ip_whitelist_type == 'manual'){
          this.showManualIpWhitelist = true;
        }
    }


    setTimeout(() => {
      // $("#currency").val(wc).trigger('change');
      $("#roles").val(this.getRoles(this.admin.rolesids)).trigger('change');
      $("#permission_roles").val(this.admin.permissionroleid).trigger('change');
      $("#time_zone").val(this.admin.timezone).trigger('change');
      $("#agent_type").val(this.admin.agent_type).trigger('change');

      
      for (const w of wc) {
        $(`#currency>option[value=${w}]`).attr('disabled', 'disabled');
      }
    }, 100);

  }

  getRoles(roleIds: any) {
    if(roleIds) {
      if(roleIds && roleIds.length > 1) {
        return roleIds.split(',');
      } else {
        return roleIds.split('');
      }
    }
  }


  // getAdminTenant() {
  //   this.agentService.getAdminTenant().subscribe((res: any) => {
  //     this.tenant = res.record.tenants;
  //     this.currencies = res.record.configurations;

  //     this.breadcrumbs = [
  //       { title: 'Home', path: '/' },
  //       { title: 'Agents', path: '/agents' },
  //       { title: this.title , path: `/agents` }
  //     ]
      
  //     if (this.adminId > 0 && this.admin?.wallets) {
  //       const wc = this.admin.wallets.map(m => m.currency_id);
  //       setTimeout(() => {
  //         $("#currency").val(wc).trigger('change');
  //       }, 100);
  //       this.changeCurrency(this.admin.currency_id);
  //     }else{
  //       this.changeCurrency(this.currencies[0].id);
  //     }

  //   });
  // }
  
  getTenant() {
    this.tenantService.getTenant(this.tenantId).subscribe((res: any) => {
      this.tenant = res.record.tenants;
      this.currencies = res.record.configurations;

      this.breadcrumbs = [
        { title: 'Home', path: '/super-admin' },
        { title: 'Tenants', path: '/super-admin/tenants' },
        { title: truncateString(this.tenant.name, 10), path: `/super-admin/tenants/details/${this.tenantId}` },
        { title: this.title , path: `/super-admin/tenants/details/${this.tenantId}` }
      ]
      
      if (this.adminId > 0 && this.admin?.wallets) {
        const wc = this.admin.wallets.map(m => m.currency_id);
        setTimeout(() => {
          // $("#currency").val(wc).trigger('change');
          for (const w of wc) {
              $(`#currency>option[value=${w}]`).attr('disabled', 'disabled');
            }
        }, 100);
        this.changeCurrency(wc);
      }
      // else {
      //   this.changeCurrency(this.currencies[0].id);
      // }

    });
  }

  changeRoles(roles: any) {
    this.f.role.setValue(roles);
  }

  changePermissionRoles(roles: any) {
    this.f.permissionRole.setValue(roles);
  }

  changeCurrency(currency: any) {
    if (currency && currency.length > 0) { 
      this.f.currency_id.setValue(currency);
    }
  }
  selectTimeZone(evt: any) {
    let timeZoneValue='';
    if(evt){
      timeZoneValue=TIMEZONE.filter(element => element.zonename === evt)[0].zonename;      
      this.f.time_zone.setValue(timeZoneValue);
    }
    // if(timeZoneValue) {
    //   this.zone=TIMEZONE.filter(element => element.value === evt)[0].name;
    //   this.params = {...this.params, page: this.p, time_zone:evt, time_zone_name: timeZoneValue};
    // }else{
    //   this.zone='';
    //   this.params = {...this.params, page: this.p, time_zone:evt};
    // }

    // if(evt)
    // this.getReport();
  }
  selectAgentType(evt: any) {
    console.log(evt);
    if(evt){
   
      this.f.agent_type.setValue(evt);
    }
  }

  get f() {
    return this.adminForm.controls;
  }

  
  changeIPWhitelistState(event:any){
    // Reset 
    this.showIpWhitelistType =  this.showManualIpWhitelist = false;

    if(event.currentTarget.checked){

      this.showIpWhitelistType = true;

      if(this.admin?.ip_whitelist_type == 'manual'){
        this.showManualIpWhitelist = true;
      }

      this.adminForm.patchValue({
        ipWhitelistType: this.admin?.ip_whitelist_type ?? ''
      });
      
    }
     
  }
 
 
  selectIPWhitelistTypeState(type:any){
    this.showManualIpWhitelist = true;
    if(type == 'global'){
      this.showManualIpWhitelist = false;
    }
     
  }



  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid

    console.log(this.adminForm);

    if (this.adminForm.invalid) return;    

    this.adminLoader = true;
    const data = this.adminForm.value;

    // data.permissions = JSON.stringify(this.buildPermissionsObject(data.permissions));
    
    if(data.enableWhitelist == true){
      if(data.ipWhitelistType != ''){
          if(data.ipWhitelistType == 'manual'){
              if(data.ip_address_group.ip_address.length == 0){
                    // error for set IP
                  toastr.error('Please enter Ip address');
                  this.adminLoader = false;
                  return;
              }
          }else{
            data.ip_address_group.ip_address = [];
          }
      }else{
        // error for Select Type
        toastr.error('Please select any type for IP whitelist');
        this.adminLoader = false;
        return;

      }
    }else{
      data.ipWhitelistType = null;
      data.ip_address_group.ip_address = [];
    }
    
    
    if(data.encrypted_password) {
      data.encrypted_password = encryptPassword(data.encrypted_password);
    }
    
    if(this.adminId > 0) {
      data.id = this.adminId;

      if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
        
        this.agentService.updateSuperAdminAgent(data)
        .subscribe((response: any) => {
          toastr.success(response?.message || 'Tenant Admin Updated Successfully!');
          // this.router.navigate(['/super-admin/tenants/details', this.tenantId]);
          this.adminLoader = false; 
        }, (err: any) => {
          this.adminLoader = false; 
        });

      } else {
        
        this.agentService.updateAdminAgent(data)
        .subscribe((response: any) => {
          toastr.success(response?.message || 'Admin Updated Successfully!');
          // this.router.navigate(['/agents', this.tenantId]);
          
          // this.router.navigate(['/agents']);
          this.adminLoader = false; 
        }, (err: any) => {
          this.adminLoader = false; 
        });

      }

    } else {
        
      if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
        this.agentService.addSuperAdminAgent(this.adminForm.value)
        .subscribe((response: any) => {
          toastr.success(response?.message || 'Tenant Admin Created Successfully!');
          this.router.navigate(['/super-admin/tenants/details', this.tenantId]);
          this.adminLoader = false;
        }, (err: any) => {
          this.adminLoader = false; 
        });
      } else {
        this.agentService.addAdminAgent(this.adminForm.value)
        .subscribe((response: any) => {
          toastr.success(response?.message || 'Admin Created Successfully!');
          // this.router.navigate(['/agents']);
          this.adminLoader = false; 
        }, (err: any) => {
          this.adminLoader = false; 
        });
      }

    }
  }


  buildPermissionsObject(permissions:any) {
    const _permissions:any = {};

    for (const module in permissions) {
      const actions: string[] = [];

      for (const perm in permissions[module]) {
        if (permissions[module][perm]) actions.push(perm);
      }

      if (actions.length) _permissions[module] = actions;
    }

    return _permissions;
  }


  buildPermissionForms(authPermissions: { [key: string]: string[] }) {
    
    for (const action in authPermissions) {
      if (authPermissions[action].length > 0) {
        const formGroup = this.formBuilder.group({});


        authPermissions[action].forEach((permission:any) =>
          formGroup.addControl(
            permission,
            this.formBuilder.control({ value: false, disabled: permission == 'R' ? false : true })
          )
        );


        (this.adminForm.get('permissions') as FormGroup).addControl(action, formGroup);


        formGroup.valueChanges.subscribe(perms => {
          Object.entries(formGroup.controls).forEach(([perm, control]) => {
            if (perm != 'R' && perms.R) control.enable({ emitEvent: false });
            if (perm != 'R' && !perms.R) control.disable({ emitEvent: false });
          });
        });
      }
    }
  }


  get permissionFormGroups(): FormGroup {
    return this.adminForm.get('permissions') as FormGroup;
  }


  get controls() {
    return this.adminForm.controls;
  }


  castToControl(control: any): FormControl {
    return control as FormControl;
  }


  getLabel(value: any): string {
    return this.permissionLabels.find(v => v.label == value)?.value || value;
  }

  getPermissionRoles(){
    this.agentService.getPermissionRoles({role_type: 2}).subscribe((res:any) => {
      this.permissionRoles = res.record
    })
  }


  get isAgent(){
    let roles = localStorage.getItem('roles') || '';
    if(roles) {
      const parsedRoles = JSON.parse(roles);
      if(parsedRoles && parsedRoles.findIndex( (role: any) => role === Role.Admin ) == -1 && parsedRoles.findIndex( (role: any) => role === Role.Agent ) > -1) {      
        return true;
      }
    }
    return false;
  }



}
