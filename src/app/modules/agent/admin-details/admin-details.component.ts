import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Tenant, User, Admin, Currency } from 'src/app/models';
import { getRole, truncateString } from 'src/app/services/utils.service';
import { Roles, PageSizes } from 'src/app/shared/constants';
import { AdminAuthService } from '../../admin/services/admin-auth.service';
import { PlayerService } from '../../player/player.service';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
import { TenantService } from 'src/app/modules/tenant/tenant.service';
import { AgentService } from '../agent.service';
import Swal from 'sweetalert2';
import { Role } from 'src/app/models';
import { PermissionService } from 'src/app/services/permission.service';

declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './admin-details.component.html',
  styleUrls: ['./admin-details.component.scss']
})

export class AdminDetailsComponent implements OnInit {
  setting: any;

  pageSizes = PageSizes;
  isSubAgentModuleAllowed:boolean = true;
  settingForm: FormGroup | any;
  submitted: boolean = false;
  settingLoader: boolean = false;

  adminRoles = Roles;
  tenantId: number = 0;
  adminId: number = 0;
  tenant!: Tenant;
  admin!: Admin;
  subAdmins: Admin[] = [];
  currencies: Currency[] = [];
  players: User[] = [];

  permissionLabels = [
    {label: 'R', value: 'Read'},
    {label: 'C', value: 'Create'},
    {label: 'U', value: 'Update'},
    {label: 'D', value: 'Delete'},
    {label: 'T', value: 'Toggle'},    
  ]
  
  subAdminParams: any = {
    size: 10,
    page: 1,
    search: '',
    tenant_id: this.tenantId,
    adminId: this.adminId,
  };
  // amountCheck:boolean = false;
  subAdminP: number = 1;
  subAdminTotal: number = 0;

  playerParams: any = {
    size: 10,
    page: 1,
    search: '',
    status: '',
    tenant_id: this.tenantId,
    adminId: this.adminId,
  };

  playerP: number = 1;
  playerTotal: number = 0;
  loggedInUserId:any
  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Agents', path: '/agents' },
    { title: 'Agent Details', path: '/agents' }
  ];

  roles: string = localStorage.getItem('roles') || '';
  showWithdrawalBtn:boolean=true;
  showDepositBtn:boolean=true;
  showEditBtn:boolean=true;
  showTreebtn:boolean=true;

  constructor(private route: ActivatedRoute,
    private tenantService: TenantService,
    private agentService: AgentService,
    private playerService: PlayerService,
    public adminAuthService: AdminAuthService,
    public superAdminAuthService: SuperAdminAuthService,
    private formBuilder: FormBuilder,
    public permissionService: PermissionService
    ) {
      this.tenantId = this.route.snapshot.params['tenantId'];
      this.adminId = this.route.snapshot.params['adminId']; 

      this.playerParams = { 
        ...this.playerParams,
        tenant_id: this.tenantId,
        adminId: this.adminId,
      }

      this.isSubAgentModuleAllowed = (localStorage.getItem('allowedModules')?.includes('subAgent') ? true : false);


      this.subAdminParams = {
        ...this.subAdminParams,
        tenant_id: this.tenantId,
        adminId: this.adminId,
      }

      this.settingForm = this.formBuilder.group({
        admin_user_id: ['', Validators.required],
        key: ['commission_percentage', [ Validators.required ]],
        value: ['', [ Validators.required, Validators.min(0), Validators.max(100) ]]
      });

      this.f.admin_user_id.setValue(this.adminId);

    }

  ngOnInit(): void {
    this.adminAuthService.adminUser.subscribe((user: any) => {
      if(user && user.id) {
        this.loggedInUserId = user.id
      }
    });
    this.getAgent();
    const roles = JSON.parse(this.roles);
    this.showDepositBtn = !(roles && roles.findIndex( (role: any) => role === Role.Admin ) == -1 && roles.findIndex( (role: any) => role === Role.WithdrawalAdmin ) > -1);
    this.showWithdrawalBtn = !(roles && roles.findIndex( (role: any) => role === Role.Admin ) == -1 && roles.findIndex( (role: any) => role === Role.DepositAdmin ) > -1);
    this.showEditBtn= !(roles && roles.findIndex( (role: any) => role === Role.Admin ) == -1 || (roles.findIndex( (role: any) => role === Role.DepositAdmin ) > -1 && roles.findIndex( (role: any) => role === Role.WithdrawalAdmin ) > -1));
    this.showTreebtn= !(roles && roles.findIndex( (role: any) => role === Role.Admin ) == -1 || (roles.findIndex( (role: any) => role === Role.DepositAdmin ) > -1 && roles.findIndex( (role: any) => role === Role.WithdrawalAdmin ) > -1));
  }

  setAdminId(adminId: number = this.adminId) {
    this.adminId = adminId;

    this.playerParams = { 
      ...this.playerParams,
      adminId: this.adminId,
    }

    this.subAdminParams = {
      ...this.subAdminParams,
      adminId: this.adminId,
    }

    this.getAgent();
  }

  getAgent() {
    this.getTenantAdmin();
    this.getTenant(); 
    this.adminAuthService.adminPermissions.subscribe(res => {
      if (this.permissionService.checkPermission('agents','R')){
        this.getSubAdmins();
      }
      if(this.permissionService.checkPermission('players','R')){
        this.getPlayers();
      }
    })
  }

  get f() {
    return this.settingForm.controls;
  }

  setSetting(setting: any) {
    this.setting = setting;
    // this.f.value.setValue(setting.value);
    this.settingForm.patchValue({
      admin_user_id: this.adminId,
      key: 'commission_percentage',
      value: setting.value
    })
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.settingForm.invalid) return;
    
    this.settingLoader = true;
    this.agentService.updateAdminAgentSetting(this.settingForm.value)
      .subscribe((res: any) => {
        toastr.success(res.message || 'Setting updaed successfully.');
        this.settingLoader = false;
        $('#modal-setting').modal('hide');
        
        if(res?.record?.id) {

          const setgInx = this.admin.setting.findIndex((f: any) => f.id === res.record.id);

          if(setgInx > -1) {
            this.admin.setting = this.admin.setting.map((m: any)=> {
              if(this.setting && this.setting.id === m.id) {
                m.value = this.settingForm.value.value;
              }
              return m;
            });
          } else {
            this.admin.setting.push(res.record);
          }

        } else {
          this.getTenantAdmin();
        }

        
      }, (err: any) => {
        this.settingLoader = false; 
      });
  }

  getTenant() {
    if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.tenantService.getTenant(this.tenantId).subscribe((res: any) => {
        this.tenant = res.record.tenants;
        this.currencies = res.record.configurations;
        this.breadcrumbs = [
          { title: 'Home', path: '/super-admin' },
          { title: truncateString(this.tenant.name, 10), path: `/super-admin/tenants/details/${this.tenantId}` },
          { title: 'Edit', path: `/super-admin/tenants/admin/${this.adminId}` },
          { title: 'Tenants', path: '/super-admin/tenants' }
        ];

      });
    } else {
      this.agentService.getAdminTenant().subscribe((res: any) => {
        this.currencies = res.record.configurations;
        this.tenant = res.record.tenants;
      });
    }
  }

  getTenantAdmin() {
    if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.agentService.getSuperAdminAgent(this.adminId).subscribe((res: any) => {
        this.admin = res.record;
        if(this.admin.ip_whitelist){
            this.admin.ip_whitelist = JSON.parse(this.admin.ip_whitelist);
        }
        if(this.admin.permissions){
          this.admin.permissions = JSON.parse(this.admin.permissions);          
        }
      });
    } else {
      this.agentService.getAdminAgent(this.adminId).subscribe((res: any) => {
        this.admin = res.record;
        if(this.admin.ip_whitelist){
          this.admin.ip_whitelist = JSON.parse(this.admin.ip_whitelist);
        }
        if(this.admin.permissions){
          this.admin.permissions = JSON.parse(this.admin.permissions);
        }
      });
    }

  }

  getRoles(roleIds: any) {
    if(roleIds) {
      if(roleIds && roleIds.length > 1) {
        return roleIds.split(',');
      } else {
        return roleIds.split('');
      }
    }
  }

  getRole(role: any) {
    return getRole(role);
  }

  subAdminPageChanged(page: number) {
    this.subAdminParams = { ...this.subAdminParams, page };
    this.getSubAdmins();
  }

  subAdminResetFilter() {
    this.subAdminP = 1;
    this.subAdminParams = {
      size: 10,
      page: 1,
      search: '',
      tenant_id: this.tenantId,
      adminId: this.adminId,
    };
    this.getSubAdmins();
  }

  filterSubAdmins(evt: any) {
    this.subAdminP = 1;
    this.subAdminParams = { ...this.subAdminParams, page: this.subAdminP };
    this.getSubAdmins();
  }

  filterPlayers(evt: any) {
    this.playerP = 1;
    this.playerParams = { ...this.playerParams, page: this.playerP };
    this.getPlayers();
  }

  getSubAdmins() {
    if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.agentService.getSuperSubAdmins(this.subAdminParams).subscribe((res: any) => {
        this.subAdminParams.subadminUser=1;
        this.subAdmins = res.record.data;
        this.subAdminTotal = res.record.count;
      });
    } else {
      this.subAdminParams.subadminUser=1;
      this.agentService.getAdminAgents(this.subAdminParams).subscribe((res: any) => {

        this.subAdmins = res.record.data;
        this.subAdminTotal = res.record.count;
      });
    }
  }

  playerPageChanged(page: number) {
    this.playerParams = { ...this.playerParams, page };
    this.getPlayers();
  }

  getPlayers() {
    if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.playerService.getSuperAdminPlayer(this.playerParams).subscribe((res: any) => {
        // console.log(res);
        this.players = res.record.data;
        this.playerTotal = res.record.count;
      });
    } else {
      this.playerService.getAdminPlayer(this.playerParams).subscribe((res: any) => {
        // console.log(res);
        this.players = res.record.data;
        this.playerTotal = res.record.count;
      });
    }
  }

  playerResetFilter() {
    this.playerP = 1;
    this.playerParams = {
      size: 10,
      page: 1,
      search: '',
      status: '',
      tenant_id: this.tenantId,
      adminId: this.adminId,
    };
    this.getPlayers();
  }

  getCurrency(id: number) {
    const currency = this.currencies.find((f: Currency) =>f.id === id);
    return currency;
  }

  createSetting() {
    this.settingForm.patchValue({
      admin_user_id: this.adminId,
      key: 'commission_percentage',
      value: ''
    })
  }

  deleteSetting(setting: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to delete ${setting.key}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {

        this.agentService.deleteAdminAgentSetting(setting.id).subscribe((res: any) => {
          toastr.success(res.message || 'Setting deleted successfully' );
          this.admin.setting = this.admin.setting.filter((f: any) => setting.id != f.id);
        });
        
      }
    });

  }

  updatePlayerStatus(player: User, status: number) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to ${player.active ? 'Deactive' : 'Active'} ${player.user_name}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, ${player.active ? 'Deactive' : 'Active'} it!`
    }).then((result) => {
      if (result.isConfirmed) {

        this.playerService.updateAdminPlayerStatus(player.id, status).subscribe((res: any) => {
          toastr.success(res.message || 'Player updated successfully' );
          this.players = this.players.map((f: User) => {
            if(f.id == player.id) {
              f.active = player.active ? false : true;
            }
            return f;
          });
        });
        
      }
    });

  }

  activeAgentStatus(admin: Admin) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to Active ${admin.agent_name}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Active it!'
    }).then((result) => {
      if (result.isConfirmed) {

        this.agentService.activeAdminAgentStatus(admin.id).subscribe((res: any) => {
          toastr.success(res.message || 'Admin updated successfully' );
          this.subAdmins = this.subAdmins.map((f: Admin) => {
            if(f.id == admin.id) {
              f.active =  true;
            }
            return f;
          });
        });
        
      }
    });

  }

  deactiveAgentStatus(admin: Admin) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to Deactive ${admin.agent_name}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Deactive it!'
    }).then((result) => {
      if (result.isConfirmed) {

        this.agentService.deactiveAdminAgentStatus(admin.id).subscribe((res: any) => {
        toastr.success(res.message || 'Admin updated successfully' );
        this.subAdmins = this.subAdmins.map((f: Admin) => {
          if(f.id == admin.id) {
            f.active = false;
          }
          return f;
        });
      });
        
      }
    });
    
  }

  copyMessage(val: string){
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    toastr.options = {
      "progressBar": true,
      "preventDuplicates": true,
      "onclick": null,
    };
    toastr.info('Copied');
    document.body.removeChild(selBox);
  }

  exportAgentsAsXLSX() {
    this.agentService.downloadAgents(this.subAdminParams).subscribe(
      (res: any) => {
        window.location.href = res.record.url;
    });
  }

  exportPlayersAsXLSX() {
    this.playerService.downloadPlayers(this.playerParams).subscribe(
      (res: any) => {
        window.location.href = res.record.url;
    });
  }

  amountCheck(wallets: any) {
    if (wallets && wallets.length) {
      const total =  wallets.map((m: any) => m.amount).reduce((a: number, b: number) => a + b);
      return total > 0 ? false : true;
    } else {
      return true;
    }
  }

  getLabel(value: any): string {
    return this.permissionLabels.find(v => v.label == value)?.value || value;
  }

  getPermissionsFromValue(value: any) {
    const order = ['R', 'C', 'U', 'D', 'T'];

    return value.sort((a:any, b:any) => order.indexOf(a) - order.indexOf(b));
  }

  objectToArray(value:any){
    
    return Object.keys(value).map(key => ({
      key,
      value: value[key]
    }));
  }

}
