import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SuperAdminAuthService } from '../super-admin/services/super-admin-auth.service';

@Injectable({ providedIn: 'root' })

export class AgentService {

  constructor(private http: HttpClient,
    private superAdminAuthService: SuperAdminAuthService) { }

  // ====================== Admin url ==============================
  getAdminAgents(params: any) {
    return this.http.post(`admin/usersadmin/agent`, params);
  }

  addAdminAgent(data: any) {
    return this.http.post(`admin/usersadmin/add`, data);
  }

  getAdminAgent(id: number) {
    return this.http.get(`admin/users/${id}`);
  }

  getAdminAgentTree(id: number = 0) {
    return this.http.get(`admin/agent/tree/${id}`);
  }
  
  updateAdminAgent(data: any) {
    return this.http.post(`admin/usersadmin/edit/${data.id}`, data);
  }

  bulkEdit(data: any) {
    return this.http.post(`admin/usersadmin/bulkEdit`, data);
  }

  // getAdminPlayer(params: any) {
  //   return this.http.post(`admin/usersadmin/player`, params);
  // }

  updateAdminAgentSetting(data: any) {
    return this.http.post(`admin/users/setting`, data);
  }

  deleteAdminAgentSetting(id: number) {
    return this.http.delete(`admin/users/setting/${id}`);    
  }

  getAdminTenant() {
    return this.http.get(`admin/tenants`);
  }

  activeAdminAgentStatus(id: number) {
    return this.http.post(`admin/users/active/${id}`, {});    
  }

  deactiveAdminAgentStatus(id: number) {
    return this.http.post(`admin/users/deactive/${id}`, {});    
  }

    // ====================== Super Admin url ==============================

  addSuperAdminAgent(data: any) {
    return this.http.post(`super/admin/usersadmin/add`, data);
  }

  getSuperAdminAgent(id: number) {
    return this.http.get(`super/admin/users/${id}`);
  }
  
  updateSuperAdminAgent(data: any) {
    return this.http.post(`super/admin/usersadmin/edit/${data.id}`, data);
  }
  
  getSuperSubAdmins(params: any) {
    return this.http.post(`super/admin/usersadmin/agent`, params);
  }

  // getSuperAdminPlayer(params: any) {
  //   return this.http.post(`super/admin/usersadmin/player`, params);
  // }


  downloadAgents(params: any) {
    if(this.superAdminAuthService && this.superAdminAuthService.superAdminTokenValue) {
      // ====================== Super Admin url ==============================
      return this.http.post(`super/admin/usersadmin/download`, params) ;
    } else {
      // ====================== Admin url ==============================
      return this.http.post(`admin/usersadmin/download`, params);
    }
  }

  getPermissionRoles(params:any){
    return this.http.get(`admin/permissionRole/get?role_type=${params.role_type}`);
  }

}
