import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { TreeComponent } from './tree/tree.component';
import { EditAdminComponent } from './edit-admin/edit-admin.component';
import { AdminDetailsComponent } from './admin-details/admin-details.component';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { AdminAuthGuard, CommonAdminAuthGuard, SuperAdminCommonAuthGuard, AdminAgentCommonAuthGuard } from 'src/app/guards';
import { PipeModule } from 'src/app/pipes/pipes.module';

const agentRoutes: Routes = [
  { path: '', component: ListComponent, canActivate: [ AdminAgentCommonAuthGuard ] },
  { path: 'tree/:tenantId/:adminId', component: TreeComponent, canActivate: [ CommonAdminAuthGuard ] },
  { path: ':tenantId/:adminId', component: EditAdminComponent, canActivate: [ CommonAdminAuthGuard ] },
  { path: 'details/:tenantId/:adminId', component: AdminDetailsComponent, canActivate: [ CommonAdminAuthGuard ] },
];

@NgModule({
  declarations: [ ListComponent, TreeComponent, EditAdminComponent, AdminDetailsComponent ],
  imports: [
    CommonModule,
    RouterModule.forChild(agentRoutes),
    SharedModule,
    ComponentsModule,
    DirectivesModule,
    PipeModule
  ]
})

export class AgentModule { }
