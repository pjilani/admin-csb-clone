import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Currency, Language, Layout, Tenant, Theme, User } from 'src/app/models';
import { encryptPassword } from 'src/app/services/utils.service';
import { Countries, Constants } from 'src/app/shared/constants';
import { CustomValidators } from 'src/app/shared/validators';
import { PlayerService } from '../player.service';
import { AdminAuthService } from '../../admin/services/admin-auth.service';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
import { CurrenciesService } from '../../super-admin/super-admin-modules/currencies/currencies.service';
import { Role } from 'src/app/models';

declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})

export class EditComponent implements OnInit {
  
  phoneMinLen: number = 5;
  phoneMaxLen: number = 10;
  zipcodeMinLen: number = 5;

  hideShowPass: boolean = false;
  
  format: string = "YYYY-MM-DD";
  initDate = new Date().toISOString().split('T')[0];

  imgURL: any;
  id: number = 0;
  currencies: Currency[] = [];
  languages: Language[] = [];
  layouts: Layout[] = [];
  themes: Theme[] = [];

  tenant!: Tenant;
  tenantRecords: any;

  playerForm: FormGroup | any;
  submitted: boolean = false;
  playerLoader: boolean = false;
  selectLogo!: File;
  roleStored: string = localStorage.getItem('roles') || '';

  theme: any = { ...Constants.INIT_THEME };
  title: string = 'Create'

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Player', path: '/players' }
  ];

  player!: User;
  minDate = new Date(2017, 0, 1);
  countries = Countries;
  sameUsernameAndNicknameErrorMessage = 'Username and Nickname must not be the same.';

  constructor(private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private playerService: PlayerService,
    public adminAuthService: AdminAuthService,
    public superAdminAuthService: SuperAdminAuthService,
    private currenciesService: CurrenciesService
    ) {
     this.id = this.route.snapshot.params['id'];
    
    this.playerForm = this.formBuilder.group(
      {
      user_name: ['', [ Validators.required, Validators.minLength(3), Validators.pattern("^(?![0-9]+$)[A-Za-z0-9]*$"), Validators.maxLength(15) ]],
      nick_name: ['', [ Validators.minLength(3), Validators.maxLength(15), Validators.pattern(/^\S*$/)]],
      first_name: ['', [ Validators.required, Validators.minLength(3), Validators.maxLength(15) ]],
      last_name: ['', [ Validators.required ,  Validators.minLength(3), Validators.maxLength(15)]],
      email: ['', [ Validators.required, Validators.pattern(CustomValidators.emailRegEx) ]],
      password: ['', [ Validators.pattern(CustomValidators.passwordRegex) ]],
      phone: ['', [ Validators.required, Validators.minLength(this.phoneMinLen),Validators.maxLength(this.phoneMaxLen), Validators.pattern(CustomValidators.phoneRegex) ]],
      zip_code: ['', [ Validators.required, Validators.minLength(this.zipcodeMinLen), Validators.pattern(CustomValidators.phoneRegex) ]],
      date_of_birth: ['', [ Validators.required ]],
      country_code: ['', [ Validators.required ]],
      currency_id: ['', [ Validators.required ]],
      vip_level: [0, [ Validators.required ]],
      city: ['', [ Validators.required,  Validators.minLength(3), Validators.maxLength(15) ]],
      email_verified: [false],
      phone_verified: [false],
      active: [true],
      demo: [false],
    },
    // {
    //   validator: this.notSameUsernameAndNickname() // Add the custom validator here
    // }
    );

    if(this.id > 0) {
      this.title = 'Edit';

      this.getPlayer();
        
    } else {

      this.playerForm.patchValue({
        date_of_birth: this.initDate,
        country_code: this.countries[0].value,
      });

      setTimeout(() => {
        $("#date_of_birth").data('daterangepicker').setStartDate(this.initDate); 
      }, 10);

      const passControl = this.playerForm.get('password');
      passControl.setValidators([Validators.required, Validators.pattern(CustomValidators.passwordRegex)]); 
      passControl.updateValueAndValidity();
    }

  this.breadcrumbs.push({ title: this.title, path: `/players/${this.id}` });

  }
  // notSameUsernameAndNickname(): ValidatorFn {
  //   return (control: AbstractControl): { [key: string]: any } | null => {
  //     const user_name = control.get('user_name')?.value;
  //     const nick_name = control.get('nick_name')?.value;
  
  //     if (user_name !== null && nick_name !== null && user_name === nick_name) {
  //       return { sameUsernameAndNickname: true };
  //     }
  
  //     return null;
  //   };
  // }

  ngOnInit(): void {
    
    if (this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      
      this.currenciesService.getAdminCurrencies().subscribe((res: any) => {
        this.currencies = res.record;

        this.currencies = this.currencies.map((m: any) => {
          m.name = m.code;
          return m;
        });
        
        if (this.id > 0) {
          setTimeout(() => {
            $("#currency_id").val(this.player.currency_id).trigger('change');
          }, 100);
        } else {
          this.f.currency_id.setValue(this.currencies[0].id);
        }
      });
    } else {

      // const roleStored = JSON.parse(this.roleStored); console.log(roleStored);
      // if((roleStored && roleStored.findIndex( (role: any) => role === Role.Admin ) == -1 &&  (roleStored.findIndex( (role: any) => role === Role.DepositAdmin ) > -1 || roleStored.findIndex( (role: any) => role === Role.WithdrawalAdmin ) > -1))){
      //   toastr.error('You are not authorized!');
      //   this.router.navigate(['/players']);
      // }
      
      this.adminAuthService?.tenantData.subscribe((tenantData: any) => {
        if(Object.keys(tenantData).length){
          const tenant_base_currency = tenantData?.tenant_base_currency
          const allowed_currencies = tenantData?.allowed_currencies          
          if(!tenant_base_currency || tenant_base_currency == 'EUR'){
            this.currencies = allowed_currencies || []
          }else{
            this.currencies = allowed_currencies?.filter((v:any) => v.code == tenant_base_currency)
          }
          if (this.id > 0 && this.player?.currency_id) {
            setTimeout(() => {
              $('#currency_id')
                .val(this.player.currency_id)
                .trigger('change');
            }, 100);
          } else {
            this.f.currency_id.setValue(this.currencies[0].id);
          }
        }
        
      });
      
    }

    this.playerForm.get('country_code').valueChanges.subscribe((value:any) => {
      if(value && value == "IN"){
        this.playerForm.get('phone').setValidators([Validators.required, Validators.pattern(CustomValidators.phoneRegex),this.tenDigitNumberValidator()])
      }else{
        this.playerForm.get('phone').setValidators([Validators.required, Validators.minLength(this.phoneMinLen), Validators.pattern(CustomValidators.phoneRegex)])
      }

      this.playerForm.get('phone').updateValueAndValidity();
    })

  }

  getNumArr(length: number) {
   return Array.from({ length }).map((_, i) => i);
  }

  getPlayer() {
    this.playerService.getAdminPlayerById(this.id).subscribe((res: any) => {
      this.player = res.record;
      this.playerForm.patchValue({
        user_name: this.player.user_name,
        nick_name: this.player.nick_name,
        first_name: this.player.first_name,
        last_name: this.player.last_name,
        email: this.player.email,
        password: '',
        phone: this.player.phone,
        zip_code: this.player.zip_code,
        date_of_birth: this.player.date_of_birth,
        country_code: this.player.country_code,
        currency_id: this.player.currency_id,
        vip_level: this.player.vip_level,
        email_verified: this.player.email_verified,
        phone_verified: this.player.phone_verified,
        active: this.player.active,
        demo: this.player.demo,
        city: this.player.city,
      });

      setTimeout(() => {
        $("#currency_id").val(this.player.currency_id).trigger('change');
        $("#country_code").val(this.player.country_code).trigger('change');
        $("#vip_level").val(this.player.vip_level).trigger('change');
      }, 100);

    });
  }
  
  selectDate(date: string) {
    if(!this.underAgeValidate(date)){
      this.playerForm.get('date_of_birth').setErrors({
        'ageValidate': true
      });
      return;
    }
    
    this.f.date_of_birth.setValue(date);
  }

  removeSpace(event:any){
    var k;  
    k = event.charCode;
    return(((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 45 || k== 95) && event.keyCode != 32 ? event:event.preventDefault());

  }

  underAgeValidate(date:any){
    var optimizedBirthday = date.replace(/-/g, "/");
    var myBirthday:any = new Date(optimizedBirthday);
    var myAge = ~~((Date.now() - myBirthday) / (31557600000));
    if(myAge < 18) {
        return false;
    }else{
      return true;
    }
  
  } 

  selectCountryCode(country_code: any) {
    this.f.country_code.setValue(country_code);
  }

  selectCurrency(currency_id: any) {
    this.f.currency_id.setValue(currency_id);
  }

  selectVipLevel(vip_level: any) {
    this.f.vip_level.setValue(vip_level);
  }

  get f() {
    return this.playerForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // console.log(this.playerForm.value);
    if (this.playerForm.invalid) return;
    
    this.playerLoader = true;

    const data = this.playerForm.value;

    if(this.f.password.value) {
      data.encrypted_password = encryptPassword(this.f.password.value);
    }

    // if(this.f.date_of_birth.value) {
    //   data.date_of_birth = this.f.date_of_birth.value;
    // }
    
    if(this.id > 0) {

      data.id = this.id;
      delete data.password;
      this.playerService.updateAdminPlayer(data)
      .subscribe((response: any) => {
        toastr.success(response?.message || 'Player Updated Successfully!');
        this.playerLoader = false; 
        // this.router.navigate(['/players']);
      }, (err: any) => {
        this.playerLoader = false; 
      });      

    } else {
      delete data.password;
      this.playerService.createAdminPlayer(data)
      .subscribe((response: any) => {
        toastr.success(response?.message || 'Player Created Successfully!');
        // this.router.navigate(['/players']);
        this.playerLoader = false; 
      }, (err: any) => {
        this.playerLoader = false; 
      });

    }
  }

  resetPass() {
    this.playerService.AdminPlayerResetPassword({ id: this.player.id })
      .subscribe((response: any) => {
        toastr.success(response?.message || 'Email is sent for reset password!');
      });
  }

  tenDigitNumberValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const value = control.value;
  
      if (value === null || value === undefined || value === '') {
        return null; // Return null if the value is empty
      }
  
      // Check if the input value is a 10-digit number
      const isValid = /^\d{10}$/.test(value);
  
      return isValid ? null : { tenDigitNumber: { value: control.value } };
    };
  }

}
