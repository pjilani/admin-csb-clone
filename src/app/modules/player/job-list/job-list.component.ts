import { AfterViewInit, Component, OnInit } from '@angular/core';
import { PlayerService } from '../player.service';
import * as moment from 'moment-timezone';
import { TIMEZONE } from 'src/app/shared/constants';
import { AdminAuthService } from '../../admin/services/admin-auth.service';
import { DatePipe } from '@angular/common';
declare const toastr: any;
declare const $: any;

@Component({
  selector: 'app-job-list',
  templateUrl: './job-list.component.html',
  styleUrls: ['./job-list.component.scss']
})

export class JobListComponent implements OnInit,AfterViewInit{

    
    constructor(private playerService: PlayerService,
    public adminAuthService: AdminAuthService,
    private datePipe: DatePipe

      ) { }
  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Players', path: '/players' },
    { title: 'Jobs', path: '/jobs' },

  ];
  zone: any = '(GMT +00:00) UTC';
  selectedTimeZone: any = 'UTC +00:00';
  job_id:any;
  params: any = {
    size: 10,
    page: 1,
    order: 'desc',
    sort_by: 'id',
    datetime: {},
    search:'',
    time_period: {},
    status:'',
    type:'',
    player_commission: false
  };
  isLoader:boolean = false;
  format: string = "YYYY-MM-DD HH:mm:ss";
  TIMEZONE: any[] = TIMEZONE;
  retry = false;
  paramsUser: any = {
    size: 10,
    page: 1,
    order: 'desc',
    sort_by: 'id',
    search:'',
    time_period: {},
    status:'',
  };
  total: number = 0;
  notStartedTotal: number = 0;
  inProgressTotal: number = 0;
  failedTotal: number = 0;
  completedTotal: number = 0;
  partiallyFailedTotal: number = 0;

  totalUser:number = 0
  p: number = 1;
  puser: number = 1;

  jobs: any;
  userDetails: any;
  ngOnInit(): void {
    this.adminAuthService.adminUser.subscribe((user: any) => {
      if(user && user.id) {
        // this.params = {...this.params, time_zone: user.timezone}
        setTimeout(() => {
          const _zone = TIMEZONE.find(t => t.zonename == user.timezone);
          this.selectedTimeZone = _zone?.zonename
          $(`#time_zone`).val(_zone?.zonename).trigger('change');
        }, 100);
      }
    });
    // this.getJobs();

  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      $('#time_period_log').val('');
      $('#time_period_log_user').val('');  
    }, 100);
  }

  convertToUTC(inputDate: string, inputTimezone: string): string {
    const date = moment.tz(inputDate, inputTimezone);
    const utcDate = date.utc().format('YYYY-MM-DD HH:mm:ss');
    return utcDate;
  }
  convertToJSON(data:any) {    
    return JSON.parse(data);
  }
  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getJobs();
  }
  pageChangedUser(id:any, page: number) {
    this.paramsUser = { ...this.paramsUser, page };
    this.getUserDetails(id);
  }
  filterSelectTimeZone(zonename: any) {
    // this.p = 1;

    if(zonename) {
      const zone = TIMEZONE.find(t => t.zonename === zonename);
      this.zone = zone?.name;
      this.selectedTimeZone = zone?.zonename
      this.params = {...this.params, time_zone: zone?.value, time_zone_name: zone?.zonename};
    }else{
      this.zone='';
      this.params = {...this.params, time_zone: this.zone};
    }

    // this.getJobs();
}
formatDateInSelectedTimeZone(date: string): string {
  const formattedDate = this.datePipe.transform(date, 'dd-MM-yyyy HH:mm:ss', this.selectedTimeZone);
  return formattedDate || '-'; // Return '-' if formatting fails
}
  getJobs() {
    this.isLoader = true
    if(this.params.time_period.start_date && this.params.time_period.end_date && this.params.time_zone_name && this.params.time_zone_name !== 'UTC +00:00'){
      this.params = {...this.params, datetime: {  start_date: this.params.time_period.start_date, end_date: this.params.time_period.end_date}}
    } else{
      this.params = {...this.params, datetime: {  start_date: this.params.time_period.start_date, end_date:this.params.time_period.end_date}}
    }
    this.playerService.listJobs(this.params).subscribe((res: any) => {
      this.jobs = res.record.data.data;
      this.notStartedTotal = res.record.total_not_started;
      this.inProgressTotal = res.record.total_in_progress;
      this.failedTotal = res.record.total_failed;
      this.completedTotal = res.record.total_completed;
      this.partiallyFailedTotal = res.record.total_partially_failed;
      this.total = res.record.data.total;
      this.isLoader = false
      },
      (error:any) => {
        this.isLoader = false

      });
  }
  getUserDetails(id:any){
    this.isLoader = true
    // this.job_id = id;
    this.playerService.getUserbyJobId(this.paramsUser, id).subscribe((res: any) => {
      this.userDetails = res.record.data;
        this.totalUser = res.record.total;
        this.isLoader = false
      },
      (error:any) => {
        this.isLoader = false

      });
  }
  setOrder(sort: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p, order: sort.order, sort_by: sort.column };
    this.getJobs();
  }
  setOrderUser(id:any, sort: any) {
    this.puser = 1;
    this.paramsUser = { ...this.paramsUser, page: this.puser, order: sort.order, sort_by: sort.column };
    this.getUserDetails(id);
  }

  resetFilter() {
    this.p = 1;
    $('#time_period_log').val('');
    this.params = {      
      size: 10,
      page: 1,
      search: '',
      datetime:{},
      order: 'desc',
      sort_by: 'id',
      time_period: {},
      status:'',
      type:'',
      player_commission: false
    };

    this.getJobs();
  }
  resetUserFilter(id:any) {
    this.puser = 1;
    $('#time_period_log_user').val('');
    this.paramsUser = {      
      size: 10,
      page: 1,
      search: '',
      order: 'desc',
      sort_by: 'id',
      time_period: {},
      status:'',
    };

    this.getUserDetails(id);
  }


  filter(evt: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getJobs();
  }

  filterUser(evt: any, id:any) {
    this.puser = 1;
    this.paramsUser = { ...this.paramsUser, page: this.puser };
    this.getUserDetails(id);
  }
  selectDateRange(time_period: any) {
    this.params = { ...this.params, time_period}
    // this.getJobs();
  }
  selectDateRangeUser(time_period: any, id:any) {
    this.paramsUser = { ...this.paramsUser, time_period}
    // this.getUserDetails(id);
  }

  retryJob(job_id:any){
    this.retry = true
    this.playerService.retryJob(job_id).subscribe((res:any)=>{
      toastr.success(res.message || 'Retry Successfully');
      this.retry = false
      this.getJobs()
    }, err =>{
      this.retry = false
    })
  }
}
