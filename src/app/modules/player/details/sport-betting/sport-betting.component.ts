import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ReportService } from 'src/app/modules/reports/reports.service';
import { SportService } from 'src/app/modules/sports/sport.service';
import { PageSizes , TIMEZONE} from 'src/app/shared/constants';
import { SuperAdminAuthService } from 'src/app/modules/super-admin/services/super-admin-auth.service';
import { TenantService } from 'src/app/modules/tenant/tenant.service';
// import { ExcelService } from 'src/app/services/excel.service';
import { environment } from 'src/environments/environment';
import { PermissionService } from 'src/app/services/permission.service';
import { generateApiUrl } from 'src/app/services/utils.service';

declare const $: any;

@Component({
  selector: 'app-sport-betting',
  templateUrl: './sport-betting.component.html',
  styleUrls: ['./sport-betting.component.scss']
})
  
export class SportBettingComponent implements OnInit, OnDestroy {

  @ViewChild('f') form!: any;
  
  searchLeaguesUrl: string = '';

  @Input() playerId: number = 0;

  selectedCoulumn: any;
  
  sports: any[] = [];
  countries: any[] = [];
  leagues: any[] = [];
  matches: any[] = [];

  allTenants: any[] = [];
  TIMEZONE: any[] = TIMEZONE;
  zone: any = '(GMT +00:00) UTC';
  allStatus = [
    // { value: "pending", name: "pending" },
    // { value: "won", name: "won" },
    // { value: "lost", name: "lost" },
    // { value: "half_lost", name: "half_lost" },
    // { value: "half_won", name: "half_won" },
    // { value: "refunded", name: "refunded" },
    // { value: "cashed_out", name: "cashed_out" },
    // { value: "cancelled", name: "cancelled" }

    // { value: -2, name: "In Game" },
    // { value: -1, name: "Cancelled" },
    // { value: 1, name: "Lost" },
    // { value: 2, name: "Won" },
    // { value: 3, name: "Refund" },
    // { value: 4, name: "Halflost" },
    // { value: 5, name: "Halfwon" }

    // { value: -2, name: "In Game" },
    // { value: -1, name: "Cancelled" },
    { value: 'in_game', name: "In Game" },
    // { value: 'won', name: "Won" },
    // { value: 'PlaceMatchedBet', name: "Place Matched Bet" },
    // { value: 'CancelBet', name: "Cancel Bet" },
    { value: 'settledmarket', name: "Settled Market" },
    { value: 'cancelmarket', name: "Cancel Market" },
    { value: 'cancelsettledmarket', name: "CancelSettledMarket" }
  ]
  
  pageSizes = PageSizes;
  
  reports: any[] = [];

  report: any;
  

  p: number = 1;

  format: string = "YYYY-MM-DD HH:mm";

  totals: number = 0;

  params: any = {
    size: PageSizes[0].name,
    page: 1,
    search: '',
    user_id: 0,
    start_date: '',
    end_date: '',
    bet_type: 'not_combo',
    sport_id: '',
    country: '',
    tournament_id: '',
    match_id: '',
    stake_min_amt: '',
    stake_max_amt: '',
    winning_min_amt: '',
    winning_max_amt: '',
    tenant_id: '',
    status: '',
    sort_by: 'created_at',
    time_zone: 'UTC +00:00',
    time_zone_name: 'UTC +00:00',
    order: 'asc'
  };

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Bet Report', path: '/reports/bet-report' },
  ];

  isLoader:boolean = false;
  firstTimeApiCall = false;

  constructor(public reportService: ReportService,
    // private excelService: ExcelService,
    private sportService: SportService,
    private tenantService: TenantService,
    private route: ActivatedRoute,
    public superAdminAuthService: SuperAdminAuthService,
    public permissionService: PermissionService) { }

  ngOnInit(): void {
    const bet_report = this.route.snapshot.queryParams['bet_report'];
    if (bet_report) {
      this.params = { ...this.params, ...this.reportService.betReportParams, user_id: this.playerId };
    } else {
      this.params = { ...this.params, ...this.reportService.userBetReportParams, user_id: this.playerId };
    }

    // this.reportService.userBetReportParams = { ...this.reportService.userBetReportParams, user_id: this.playerId };
    this.getAllSport();
    this.getAllCountries();
    // this.getAllLeagues();
    
    if (this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.getTenantsAll();
    } else {
      // this.getReport();
      this.searchLeaguesUrl = generateApiUrl() + `admin/sports/leagues/search`;
    }
    
  }

  ngAfterViewInit(): void {
    const that = this;

    setTimeout(() => {

      // $("#tenant_id").val(this.reportService.userBetReportParams.tenant_id).trigger('change');
      // $("#sport_id").val(this.reportService.userBetReportParams.sport_id).trigger('change');
      // $("#country").val(this.reportService.userBetReportParams.country).trigger('change');
      // $("#tournament_id").val(this.reportService.userBetReportParams.tournament_id).trigger('change');
      // $("#match_id").val(this.reportService.userBetReportParams.match_id).trigger('change');

      if (this.params.start_date && this.params.end_date) {
        $('#filter_date').daterangepicker({
         startDate: new Date(this.params.start_date),
          endDate: new Date(this.params.end_date),
          locale: {
            format: this.format
          }
        }, function(start: any, end: any) {
          const start_date = start.format(that.format);
          const end_date = end.format(that.format);
          that.selectDateRange({ start_date, end_date });
        });
        // $('#filter_date').val({ startDate: this.params.start_date, endDate: this.params.end_date });
      } else {
        $('#filter_date').val('');
      }

      $('#time_period').val('');
      
    }, 100);
  }

  getTenantsAll() {
    this.tenantService.getTenantsAll().subscribe((res: any) => {
      this.allTenants = res.record;
      if (this.params.tenant_id == 0 && this.allTenants && this.allTenants.length > 0) {
        this.tenantFilter(this.allTenants[0].id);
        this.searchLeaguesUrl = generateApiUrl() + `super/admin/sports/leagues/search?tenant_id=${this.allTenants[0].id}`;
      } else {
        this.tenantFilter(this.params.tenant_id);
        this.searchLeaguesUrl = generateApiUrl() + `super/admin/sports/leagues/search?tenant_id=${this.params.tenant_id}`;
      }
    });
  }

  selectReport(report: any) {
    this.report = report;
  }

  setOrder(sort: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p, order: sort.order, sort_by: sort.column };
    this.getReport();
  }

  getReport() {
    this.isLoader = true
    this.firstTimeApiCall = true;
    if (this.permissionService.checkPermission('bet_report','R')){
      this.reportService.getBetReports(this.params).subscribe((res: any) => {
        this.reports = res.record.data;
        this.totals = res.record.total;
        this.isLoader = false
      },
      (error:any) => {
        this.isLoader = false

      } );
    }
  }

  getAllSport() {
    this.sportService.getAdminAllSports().subscribe((res: any) => {
      this.sports = res.record;
    });  
  }

  getAllCountries() {
    this.sportService.getAdminAllCountries().subscribe((res: any) => {
      this.countries = res.record;
    });
  }

  // getAllLeagues(tenant_id?: any) {
  //   this.sportService.getAdminAllLeagues({ tenant_id: tenant_id ? tenant_id : '' }).subscribe((res: any) => {
  //     this.leagues = res.record;
  //   });
  // }

  filter(evt: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getReport();
  }
  
  formValidFilter(evt: any) {
    // if (this.form.valid) {
    //   this.getReport();
    // }
  }
  
  sportFilter(sport_id: any) {
    // this.p = 1;
    this.params = { ...this.params, sport_id };
    // if (sport_id) { 
    //   this.getReport(); 
    // }
  }

  selectCountry(country: any) {
    // this.p = 1;
    this.params = { ...this.params, country };
    // if (country) {
    //   this.getReport();
    // }
  }

  selectTournament(league_id: any) {
    if(league_id) {
      this.params = { ...this.params, tournament_id: league_id };
      this.sportService.getAdminAllMatches({ league_id, tenant_id: this.params.tenant_id }).subscribe((res: any) => {
        this.matches = res.record;
      });
    }
  }

  selectMatch(match_id: any) {
    // this.p = 1;
    this.params = { ...this.params, match_id };
    // if (match_id) { 
    //   this.getReport();
    // }
  }

  selectDateRange(dates: any) {
    // this.p = 1;
    this.params = { ...this.params, start_date: dates.start_date, end_date: dates.end_date };
    // if (dates.start_date && dates.end_date) { 
    //   this.getReport();
    // }
  }

  tenantFilter(tenant_id: any) {
    // this.p = 1;
    this.params = { ...this.params, tenant_id };
    // if (tenant_id) { 
    //   // this.getAllLeagues(tenant_id);
    //   this.getReport();
    // }
  }

  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getReport();
  }

  resetFilter() {
    
    setTimeout(() => {
      $("#tenant_id").val(this.allTenants.length > 0 ? this.allTenants[0].id : '').trigger('change');
      $("#sport_id").val("").trigger('change');
      $("#country").val("").trigger('change');
      $("#tournament_id").val("").trigger('change');
      $("#match_id").val("").trigger('change');
      $('#time_period').val('');
      $( "#time_zone" ).val('UTC +00:00').trigger('change');
    }, 100);

    this.matches = [];

    this.p = 1;
    this.params = {
      size: 10,
      page: 1,

      search: '',
      start_date: '',
      end_date: '',
      bet_type: 'not_combo',
      sport_id: '',
      country: '',
      tournament_id: '',
      match_id: '',
      stake_min_amt: '',
      stake_max_amt: '',
      winning_min_amt: '',
      winning_max_amt: '',
      status: '',
      user_id: this.playerId,
      time_zone: 'UTC +00:00',
      time_zone_name: 'UTC +00:00',
      tenant_id: this.allTenants.length > 0 ? this.allTenants[0].id : '',
      sort_by: 'created_at',
      order: 'asc'
    };

    this.getReport();
  }

  exportAsXLSX() {
    this.reportService.getAllBetReports(this.params).subscribe(
      (res: any) => {
        window.location.href = res.record.url;
      // this.excelService.exportAsExcelFile(res.record.data, 'report');
    });
  }

  filterSelectTimeZone(evt: any) {
    this.p = 1;
    let timeZoneValue='';
    if(evt){
      timeZoneValue=TIMEZONE.filter(element => element.value === evt)[0].zonename;
    }
    if(timeZoneValue) {
      this.zone=TIMEZONE.filter(element => element.value === evt)[0].name;
      this.params = {...this.params, page: this.p, time_zone:evt, time_zone_name: timeZoneValue};
    }else{
      this.zone='';
      this.params = {...this.params, page: this.p, time_zone:evt};
    }

    if(evt)
    this.getReport();
  }

  winningPrice(price: string, stake: string) {
    if(price && stake) {
      return (parseFloat(price) * parseFloat(stake)).toFixed(2);
    } else {
      return 'NA';
    }
  }

  setBetType(bet_type: string) {
    // this.p = 1;
    this.reports = []
    this.totals = 0;
    this.firstTimeApiCall = false;
    this.params = { ...this.params, bet_type };
    // this.getReport();
  }

  ngOnDestroy(): void {
    this.reportService.userBetReportParams = this.params;
  }


}
