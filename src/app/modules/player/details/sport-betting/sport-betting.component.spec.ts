import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SportBettingComponent } from './sport-betting.component';

describe('SportBettingComponent', () => {
  let component: SportBettingComponent;
  let fixture: ComponentFixture<SportBettingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SportBettingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SportBettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
