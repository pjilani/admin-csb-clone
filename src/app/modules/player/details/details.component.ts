import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { User, Admin } from 'src/app/models';
import { PlayerService } from '../player.service';
import Swal from 'sweetalert2';
import { SuperAdminAuthService } from 'src/app/modules/super-admin/services/super-admin-auth.service';
import { AdminAuthService } from 'src/app/modules/admin/services/admin-auth.service';
// import { SocketIOService } from 'src/app/services/socket-io.service';
import { Role } from 'src/app/models';
import { DomainForDisableModule } from 'src/app/shared/constants';
import { PermissionService } from 'src/app/services/permission.service';

declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
  // providers: [SocketIOService]
})

export class DetailsComponent implements OnInit, AfterViewInit {

  kyc_done: boolean = false;
  isAgent:boolean = false;
  isAllowedModule:any = '';

  DomainForDisableModule:any = DomainForDisableModule;
  setting: any;
  checkSuperAdmin:boolean = false;
  checkAdmin:boolean = false;
  sportMenuCheck:boolean = false;

  settingForm: FormGroup | any;
  submitted: boolean = false;
  settingLoader: boolean = false;

  bankLoader: boolean = false;

  ncbForm: FormGroup | any;
  ncbSubmitted: boolean = false;
  ncbLoader: boolean = false;
  ncbType: string = 'Deposit';

  bank:any;

  bankForm: FormGroup | any;

  document: any;
  rejForm: FormGroup | any;
  rejSubmitted: boolean = false;
  rejLoader: boolean = false;

  player!: User;
  non_cash_amountCheck:boolean = false;
  amountCheck:boolean = false;
  playerId: number = 0;

  userLoginHistoryParams: any = {
    size: 10,
    page: 1,
  };

  bankDetailsParams: any = {
    size: 10,
    page: 1,
  }

  playerSettingKeys: Array<any> = [
    { name: 'Daily Betting limit', key: 'dailyBettingLimit', value: '', updatedAt:'', id: 0 },
    { name: 'Weekly Betting limit', key: 'weeklyBettingLimit', value: '', updatedAt: '', id: 0 },
    { name: 'Monthly Betting limit', key: 'monthlyBettingLimit', value: '', updatedAt: '', id: 0 },
    { name: 'Daily Deposit limit', key: 'dailyDepositLimit', value: '', updatedAt: '', id: 0 },
    { name: 'Weekly Deposit limit', key: 'weeklyDepositLimit', value: '', updatedAt: '', id: 0 },
    { name: 'Monthly Deposit limit', key: 'monthlyDepositLimit', value: '', updatedAt: '', id: 0 },
    // { name: 'Self Exclusion', key: 'selfExclusion', value: 0, updatedAt: '', id: 0 }
  ];

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Players', path: '/players' },
    { title: 'Player Details', path: '/players' }
  ];

  roles: string = localStorage.getItem('roles') || '';
  showWithdrawalBtn:boolean=true;
  showEditBtn:boolean=true;
  showDepositBtn:boolean=true;
  showSettingTab:boolean=true;
  userName: string | undefined;

  commissionForm: FormGroup|any;
  initialCommissionValue: number | null | undefined;
  commissionLoader: boolean=false;
  commissionId: any = 0;
  maxCommissionValue: any;
  isLoader:boolean = false;
  playerLoginHistory: any;
  playerLoginHistoryTotal: number = 0;
  playerP: number = 1;
  playerTotal: number = 0;

  bankDetails: any;
  bankDetailsTotal: number = 0;
  bankDetailsP: number = 1;

  constructor(private route: ActivatedRoute,
    private playerService: PlayerService,
    private formBuilder: FormBuilder,
    public superAdminAuthService: SuperAdminAuthService,public adminAuthService: AdminAuthService,
    public permissionService: PermissionService
    // private socketIOService: SocketIOService
    ) {

      this.playerId = this.route.snapshot.params['id'];

      // this.settingForm = this.formBuilder.group({
      //   user_id: ['', Validators.required],
      //   key: ['bettingLimit', [ Validators.required ]],
      //   value: ['', [ Validators.required, Validators.min(1) ]],
      //   duration: ['', [ Validators.required, Validators.min(1) ]],
      //   description: ['', [ Validators.required ]]
      // });

      this.ncbForm = this.formBuilder.group({
        wallet_id: ['', Validators.required],
        non_cash_amount: ['', [Validators.required, Validators.min(1)]]
      });

      this.rejForm = this.formBuilder.group({
        reason: ['', Validators.required],
        user_id: ['', Validators.required],
        document_id: ['1', Validators.required],
        status: ['rejected', Validators.required]
      });

      this.bankForm = this.formBuilder.group({
        bank_name: ['', [ Validators.required,Validators.pattern('^[a-zA-Z ]+$')]],
        ifsc_code: ['', [ Validators.required,Validators.pattern('^[A-Za-z]{4}[a-zA-Z0-9]{7}$')]],
        account_number: ['', [ Validators.required,Validators.pattern('^[0-9]{9,18}$') ]],
        account_holder_name: ['', [ Validators.required,Validators.pattern('^[a-zA-Z_ ]+$') ]],
        active: [''],
      });

      this.rejf.user_id.setValue(this.playerId);

      this.setCommissionForm();
      this.getLoginHistory();
      this.getBankDetails();

      // this.f.user_id.setValue(this.playerId);

  }


  ngOnInit(): void {
    this.getPlayer();
    this.menuItems();
    const roles = JSON.parse(this.roles);
    this.showDepositBtn = !(roles && roles.findIndex( (role: any) => role === Role.Admin ) == -1 && roles.findIndex( (role: any) => role === Role.WithdrawalAdmin ) > -1);
    this.showWithdrawalBtn = !(roles && roles.findIndex( (role: any) => role === Role.Admin ) == -1 && roles.findIndex( (role: any) => role === Role.DepositAdmin ) > -1);
    this.showEditBtn= !(roles && roles.findIndex( (role: any) => role === Role.Admin ) == -1 || (roles.findIndex( (role: any) => role === Role.DepositAdmin ) > -1 && roles.findIndex( (role: any) => role === Role.WithdrawalAdmin ) > -1));
    this.showSettingTab= !(roles && roles.findIndex( (role: any) => role === Role.Admin ) == -1 || (roles.findIndex( (role: any) => role === Role.DepositAdmin ) > -1 && roles.findIndex( (role: any) => role === Role.WithdrawalAdmin ) > -1)|| (roles.findIndex( (role: any) => role === Role.DepositAdmin ) > -1 && roles.findIndex( (role: any) => role === Role.Subadmin ) > -1));


    this.isAllowedModule = (localStorage.getItem('domain') ? localStorage.getItem('domain') : '');
    
  
    if (this.adminAuthService.adminTokenValue && this.adminAuthService.adminTokenValue.length > 0) {

      if(this.roles) {
        const roles = JSON.parse(this.roles);
        if(roles && roles.findIndex( (role: any) => role === Role.Admin ) == -1 && roles.findIndex( (role: any) => role === Role.Agent ) > -1) {
          this.isAgent = true;
        }

      }

    }
  }

  menuItems(){
    this.checkAdmin = this.adminAuthService.adminTokenValue && this.adminAuthService.adminTokenValue.length > 0;
    this.checkSuperAdmin = this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0;
    if(!this.checkSuperAdmin){
      this.adminAuthService.tenantData.subscribe((tenantData:any) => {
        if(tenantData){
          let sport  = tenantData?.menuList.filter((element:string) => {
            if(element === "Exchange" || element === 'Live Bets'){return 1;}
            return 0;
          });
          if(sport != ""){
            this.sportMenuCheck = true;
          }
        }
      });
    }
  }

  playerPageChanged(page: number) {
    this.userLoginHistoryParams = { ...this.userLoginHistoryParams, page };
    this.getLoginHistory();
  }

  bankDetailsPageChanged(page: number) {
    this.bankDetailsParams = { ...this.bankDetailsParams, page };
    this.getBankDetails();
  }


  ngAfterViewInit(): void {
    const tab = this.route.snapshot.queryParams['tab'];

    if (tab) {
      $('.nav-link').removeClass("active");
      $('.tab-pane').removeClass("active");
      $(`#${tab}`).addClass("active");
      $(`#${tab}l`).addClass("active");
    }
  }

  getPlayer() {
    this.playerService.getAdminPlayerById(this.playerId).subscribe((res: any) => {
      this.player = res.record;
      if (this.player) {        
        this.userName = this.player.user_name;
        this.kyc_done = this.player.kyc_done;
        if(this.player.sign_in_ip){
          
          this.player.sign_in_ip = JSON.parse(this.player.sign_in_ip || '{}'); 
        }

        let r = parseFloat(this.player.non_cash_amount);
        if (r<=0) {
          this.non_cash_amountCheck = true;
        }else{
          this.non_cash_amountCheck = false;
        }

        let t = parseFloat(this.player.amount);
        if (t<=0) {
          this.amountCheck = true;
        }else{
          this.amountCheck = false;
        }
      }
      
      for (let i = 0; i < this.player.setting.length; i++) {
        for (let j = 0; j < this.playerSettingKeys.length; j++) {
            
          if(this.playerSettingKeys[j].key == this.player.setting[i].key){
            this.playerSettingKeys[j].value = this.player.setting[i].value;
            this.playerSettingKeys[j].updatedAt = this.player.setting[i].updated_at;
            this.playerSettingKeys[j].id = this.player.setting[i].id;
          }
          
        }
        
      }

      this.maxCommissionValue = this.player.player_commission_max_percentage;
      this.initialCommissionValue = this.findCommissionPercentage(this.player.setting);

      let initialRadioValue = 'none'

      if (this.initialCommissionValue === 0) {
        initialRadioValue = 'none';
      } else if (this.initialCommissionValue === 5) {
        initialRadioValue = '5';
      } else if (this.initialCommissionValue === 10) {
        initialRadioValue = '10';
      } else {
        initialRadioValue = 'custom';
        this.commissionForm.get('customValue').setValue(this.initialCommissionValue)
      }

      this.commissionForm.get('option').setValue(initialRadioValue)

      // this.playerSettingKeys[6].value = this.player.self_exclusion;
      

      this.ncb.wallet_id.setValue(this.player.wallet_id);
    });
  }

  getLoginHistory(){
    this.playerService.getPlayerLoginHistoryById(this.playerId,this.userLoginHistoryParams).subscribe((res: any) => {
        this.playerLoginHistory = res.record.data;
        this.playerLoginHistoryTotal = res.record.total;
        // this.loading=false;
    });
  }

  getBankDetails(){
    this.playerService.getBankDetailsById(this.playerId,this.bankDetailsParams).subscribe((res: any) => {
        this.bankDetails = res.record.data;
        this.bankDetailsTotal = res.record.total;
        // this.loading=false;
    });
  }

  // get f() {
  //   return this.settingForm.controls;
  // }

  get rejf() {
    return this.rejForm.controls;
  }

  // setSetting(setting: any) {
  //   this.setting = setting;
  //   this.settingForm.patchValue({
  //     user_id: this.playerId,
  //     key: setting.key,
  //     value: setting.value,
  //     duration: setting.duration?setting.duration:0,
  //     description: setting.description
  //   });
  // }

  // createNew() {
  //   this.setting = null;
  //   this.submitted = false;
  //   this.settingForm.patchValue({
  //     user_id: this.playerId,
  //     key: 'bettingLimit',
  //     value: '',
  //     duration: '',
  //     description: ''
  //   });
  // }

  get ncb() { return this.ncbForm.controls; }

  get b() { return this.bankForm.controls; }

  deposit() {
    this.ncbType = 'Deposit';
    this.ncb.non_cash_amount.setValue('');
  }

  withdraw() {
    this.ncbType = 'Withdraw';
    this.ncb.non_cash_amount.setValue('');
  }

  onNCBSubmit() {
    this.ncbSubmitted = true;
    if(this.ncbForm.invalid) return;

    this.ncbLoader = true;

    if(this.ncbType === 'Deposit') {
      this.playerService.nonCashDeposit(this.ncbForm.value).subscribe((res: any) => {
          toastr.success(res.message || 'Amount deposit successfully');
          this.player.non_cash_amount = `${parseFloat(this.player.non_cash_amount) + this.ncb.non_cash_amount.value}`;
          $('#modal-ncb').modal('hide');
          this.ncbSubmitted = false;
          this.ncbLoader = false;
      }, err => {
        this.ncbSubmitted = false;
        this.ncbLoader = false;
      });
    } else {
      this.playerService.nonCashWithdraw(this.ncbForm.value).subscribe((res: any) => {
          toastr.success(res.message || 'Amount withdraw successfully');
          this.player.non_cash_amount = `${parseFloat(this.player.non_cash_amount) - this.ncb.non_cash_amount.value}`;
          $('#modal-ncb').modal('hide');
          this.ncbSubmitted = false;
          this.ncbLoader = false;

        let r = parseFloat(this.player.non_cash_amount);
        if (r<=0) {
          this.non_cash_amountCheck = true;
        }else{
          this.non_cash_amountCheck = false;
        }


      }, err => {
        this.ncbSubmitted = false;
        this.ncbLoader = false;
        });
    }

  }

  // onSubmit() {
  //   this.f.user_id.setValue(this.playerId);
  //   this.submitted = true;
  //   // stop here if form is invalid
  //   if (this.settingForm.invalid) return;

  //   this.settingLoader = true;
  //   if(this.setting && this.setting.id > 0) {

  //     const data = this.settingForm.value;
  //     data.id = this.setting.id;

  //   this.playerService.updateAdminPlayerSetting(this.playerId, data)
  //     .subscribe((res: any) => {
  //       toastr.success(res.message || 'Setting updated successfully.');

  //       if(res?.record?.id) {

  //           this.player.setting = this.player.setting.map((m: any)=> {
  //             if(this.setting && this.setting.id === m.id) {
  //               m.key = this.f.key.value;
  //               m.value = this.f.value.value;
  //               m.duration = this.f.duration.value;
  //               m.description = this.f.description.value;
  //             }
  //             return m;
  //           });

  //       } else {
  //         this.getPlayer();
  //       }

  //       this.resetSettingForm();

  //     }, (err: any) => {
  //       this.settingLoader = false;
  //     });

  //   } else {

  //     this.playerService.createAdminPlayerSetting(this.settingForm.value)
  //     .subscribe((res: any) => {
  //       toastr.success(res.message || 'Setting created successfully.');
  //       this.settingLoader = false;
  //       $('#modal-setting').modal('hide');
  //       this.resetSettingForm();

  //       if(res?.record?.id) {

  //           this.player.setting.push(res.record);

  //       } else {
  //         this.getPlayer();
  //       }

  //     }, (err: any) => {
  //       this.settingLoader = false;
  //     });

  //   }
  // }


  updatePlayerSetting(key: any, status: string,value:any,type:any,id:any) {
    let html = '';
    if(type =='update'){
        html = `<h4>You want to ${status} it!</h4><br>` +
        '<label>Limit<span class="text-red">*</span></label>' +
        '<input style="margin-left:31px;margin-top: 2px;" value="'+value+'" autocomplete="new-limit" type="text" autocomplete="off" id="limit" placeholder="Enter limit*" class="swal2-input">';
    }
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to ${status} it!`,
      html:html,
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, ${status} it!`,
      preConfirm: function () {
        return new Promise(function (resolve) {
          // Validate input
         if($('#limit').val() == '' || $('#limit').val() == 0){
            Swal.showValidationMessage("Limit can't be 0 or empty"); // Show error when validation fails.
            Swal.enableButtons(); // Enable the confirm button again.
          } else {
              Swal.resetValidationMessage(); // Reset the validation message.
              resolve([
                  $('#limit').val(),
              ]);
          }
      })
      }
    }).then((result) => {
      if (result.isConfirmed) {
        console.log(result.value);
        
        this.settingLoader = true;
        this.playerService.createAdminPlayerSetting({ id, type , key, value: ($('#limit').val() ? $('#limit').val() : ''), user_id: this.playerId }).subscribe((res: any) => {
          toastr.success(res.message || 'Limit has been updated' );
          this.settingLoader = false;
          this.getPlayer();
        });
        
      }

    });
  }

  // resetSettingForm() {
  //   this.settingForm.reset();
  //   this.submitted = false;
  //   this.settingLoader = false;
  //   $('#modal-setting').modal('hide');
  // }

  // deleteSetting(setting: any) {
  //   Swal.fire({
  //     title: 'Are you sure?',
  //     text: `You want to delete ${setting.key}!`,
  //     icon: 'warning',
  //     showCancelButton: true,
  //     confirmButtonColor: '#3085d6',
  //     cancelButtonColor: '#d33',
  //     confirmButtonText: 'Yes, delete it!'
  //   }).then((result) => {
  //     if (result.isConfirmed) {

  //       this.playerService.deleteAdminPlayerSetting(setting.id).subscribe((res: any) => {
  //         toastr.success(res.message || 'Setting deleted successfully' );
  //         this.player.setting = this.player.setting.filter((f: any) => setting.id != f.id);
  //       });

  //     }
  //   });

  // }


  updatePlayerDocStatus(docs: any, status: string) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to ${status} it!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, ${status} it!`
    }).then((result) => {
      if (result.isConfirmed) {

        const params = {
          user_id: this.playerId,
          document_id: docs.id,
          status
        };

        this.playerService.updatePlayerDocStatus(params).subscribe((res: any) => {
          // socket
          // this.socketIOService.sendMessage(this.playerId, 'Player Document Approved By Admin.');

          toastr.success(res.message || 'Player Document updated successfully');
          this.player.user_documents = this.player.user_documents.map((f: any) => {
            if(f.id == docs.id) {
              f.status = status;
            }
            if(status=='approved'){
              f.is_verified=true;
            }
            return f;
          });
        });

      }
    });

  }

  setPlayerDoc(doc: any) {
    this.document = doc;
    this.rejSubmitted = false;

    this.rejForm.patchValue({
      reason: '',
      user_id: this.playerId,
      document_id: doc.id,
      status: 'rejected'
    });
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.bankForm.invalid) return;
      this.bankLoader = true;
    
      if(this.bank && this.bank.id > 0) {

        this.playerService.updateBankDetail(this.bank.id ,this.playerId, this.bankForm.value)
        .subscribe((res: any) => {
          $('#modal-page').modal('hide');
          toastr.success(res.message || 'bank detail updated successfully.');
          this.bankForm.reset();
          this.getBankDetails();
          // this.resetbankForm();
          // this.getSocialMedia();
          
          this.bankLoader = false; 
        }, (err: any) => {
          this.bankLoader = false; 
        });

      }else{
        this.playerService.createBankDetail(this.playerId, this.bankForm.value)
        .subscribe((res: any) => {
          $('#modal-page').modal('hide');
          toastr.success(res.message || 'bank detail created successfully.');
          
          this.bankForm.reset();
          this.getBankDetails();
          this.bankLoader = false; 
        }, (err: any) => {
          this.bankLoader = false; 
        });

      }
    
  }

  onSubmitPlayerDoc() {
    this.rejSubmitted = true;

    // console.log(this.rejForm.value);

    if (this.rejForm.invalid) return;
    this.rejLoader = true;

      this.playerService.updatePlayerDocStatus(this.rejForm.value).subscribe((res: any) => {
        // socket
        // this.socketIOService.sendMessage(this.playerId, 'Player Document Rejected By Admin.');

        this.rejLoader = false;
        this.rejSubmitted = false;
        $('#modal-rej').modal('hide');

        toastr.success(res.message || 'Player Document updated successfully' );
        this.player.user_documents = this.player.user_documents.map((f: any) => {
          if(f.id == this.rejf.document_id.value) {
            f.status = 'rejected';
          }
          return f;
        });
        this.getPlayer();
      }, err => { this.rejLoader = false; });

  }

  changeKyc() {

    const docs = this.player.user_documents.filter((f: any) => f.status != 'approved');

    if (this.kyc_done && docs && docs.length > 0) {
      Swal.fire({
        title: 'Are you sure?',
        text: `Are you sure for KYC ${ this.kyc_done ? 'Done' : 'Not Done' } it!`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: `Yes, ${ this.kyc_done ? 'Done' : 'Not Done' } it!`
      }).then((result) => {
        if (result.isConfirmed) {

          this.updateKYC();

        }
      });
    } else {
      this.updateKYC();
    }

  }

  updateKYC() {
    this.playerService.updateKyc({ id: this.playerId, kyc_done: this.kyc_done }).subscribe((res: any) => {
      toastr.success( res.message || 'Player Kyc updated Successfully');
    });
  }

  playerSelfExclusion() {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to reset self exclusion!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, update it!'
    }).then((result) => {
      if (result.isConfirmed) {

        this.playerService.playerSelfExclusion({id: this.playerId}).subscribe((res: any) => {
          toastr.success(res.message || 'Setting updated successfully' );
          this.getPlayer();
        });

      }
    });

  }


  setCommissionForm(){
    // console.log(this.player.setting);

    let initialRadioValue = 'none'; 

    

    this.commissionForm = this.formBuilder.group({
      option: [initialRadioValue, Validators.required],
      customValue: [{ value: this.initialCommissionValue, disabled: initialRadioValue !== 'custom' }]
    });

    this.commissionForm.get('option').valueChanges.subscribe((value:any) => {
      if (value !== 'custom') {
        this.commissionForm.get('customValue').reset();
        this.commissionForm.get('customValue').disable();
        this.commissionForm.get('customValue').clearValidators();
      } else {
        this.commissionForm.get('customValue').enable();
        this.commissionForm.get('customValue').setValidators([Validators.required, Validators.min(0),Validators.max(this.maxCommissionValue ?? 20)]);
        if(!this.commissionForm.get('customValue').value){
          this.commissionForm.get('customValue').setValue(this.maxCommissionValue ?? 20)
        }
      }
      this.commissionForm.get('customValue').updateValueAndValidity();
    });

  }


  submitCommissionForm() {
    let submittedValue: number | undefined;
    this.commissionLoader = true;

    if(this.commissionForm.invalid){
      return
    }

    const selectedOption = this.commissionForm.get('option').value;

    if (selectedOption === 'custom') {
      submittedValue = this.commissionForm.get('customValue').value;
    } else if (selectedOption === 'none') {
      submittedValue = 0;
    } else {
      submittedValue = parseInt(selectedOption, 10);
    }

    this.playerService.createAdminPlayerSetting({ id: this.commissionId, type:'update' , key:'commissionPercentage', value: submittedValue, user_id: this.playerId }).subscribe((res: any) => {
      toastr.success(res.message || 'Commission Percentage has been updated' );
      this.commissionLoader = false;
      this.getPlayer();
    });
  }


  findCommissionPercentage(data:any) {
    const commissionObj = data.find((obj:any) => obj.key === "commissionPercentage");

    if (commissionObj) {
        this.commissionId = commissionObj.id
        return parseFloat(commissionObj.value); // Assuming the value is a number
    } else {
        return 0;
    }
}

selectBank(bank: any){
  this.submitted = false;
  this.bank = bank;
  this.bankForm.patchValue({
    bank_name: bank.bank_name,
    ifsc_code: bank.bank_ifsc_code,
    account_number: bank.account_number,
    account_holder_name: bank.name,
    active: bank.status== 'active'? true: false
  });
}

createBank(){
  this.submitted = false;
  this.bank='';
  this.bankForm.patchValue({
    bank_name: '',
    ifsc_code: '',
    account_number: '',
    account_holder_name: '',
    active: ''
  });
}


}
