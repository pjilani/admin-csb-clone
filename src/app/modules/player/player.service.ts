import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SuperAdminAuthService } from '../super-admin/services/super-admin-auth.service';

@Injectable({ providedIn: 'root' })

export class PlayerService {

  constructor(private http: HttpClient,
  private superAdminAuthService: SuperAdminAuthService) { }

  // ====================== Admin url ==============================
  
  getAdminPlayer(params: any) {
    return this.http.post(`admin/usersadmin/player`, params);
  }

  createAdminPlayer(data: any) {
    if (this.superAdminAuthService?.superAdminTokenValue?.length > 0) {
      return this.http.post(`super/admin/user/player/add`, data);
    } else {
      return this.http.post(`admin/user/player/add`, data);
    }
  }

  AdminPlayerResetPassword(data: any) {
    if (this.superAdminAuthService?.superAdminTokenValue?.length > 0) {
      return this.http.post(`super/admin/user/player/reset-password`, data);
    } else {
      return this.http.post(`admin/user/player/reset-password`, data);
    }
  }

  getAdminPlayerById(id: number) {
    if (this.superAdminAuthService?.superAdminTokenValue?.length > 0) {
      return this.http.get(`super/admin/user/player/${id}`);
    } else {
      return this.http.get(`admin/user/player/${id}`);
    }
  }

  updateAdminPlayer(data: any) {
    if (this.superAdminAuthService?.superAdminTokenValue?.length > 0) {
      return this.http.post(`super/admin/user/player/update`, data);
    } else {
      return this.http.post(`admin/user/player/update`, data);
    }
  }
  
  updateAdminPlayerStatus(id: number, status: number) {
   
    if (this.superAdminAuthService?.superAdminTokenValue?.length > 0) {
      return this.http.put(`super/admin/user/player/status/${id}/${status}`, {});
    } else {
      return this.http.put(`admin/user/player/status/${id}/${status}`, {});
    }
  }

  createAdminPlayerSetting(data: any) {
    return this.http.post(`admin/player/setting`, data);
  }
  
  playerSelfExclusion(data: any) {
    return this.http.post(`admin/player/setting/self-exclusion`, data);
  }

  updateAdminPlayerSetting(id: number, data: any) {
    return this.http.post(`admin/player/setting/edit/${id}`, data);
  }

  deleteAdminPlayerSetting(id: number) {
    return this.http.delete(`admin/player/setting/${id}`);
  }

  updatePlayerDocStatus(params: any) {
    return this.http.post(`admin/player/setting/document/status`, params);
  }

  nonCashDeposit(params: any) {
    if (this.superAdminAuthService?.superAdminTokenValue?.length > 0) {
      return this.http.post(`super/admin/transactions/no-cash/deposit`, params);
    } else {
      return this.http.post(`admin/transactions/no-cash/deposit`, params);
    }
    
  }

  nonCashWithdraw(params: any) {
    if (this.superAdminAuthService?.superAdminTokenValue?.length > 0) {
      return this.http.post(`super/admin/transactions/no-cash/withdraw`, params);
    } else {
      return this.http.post(`admin/transactions/no-cash/withdraw`, params);
    }
  }
  
  updateKyc(data: any) {
    if (this.superAdminAuthService?.superAdminTokenValue?.length > 0) {
      return this.http.put(`super/admin/user/player/update-kyc`, data);
    } else {
      return this.http.put(`admin/user/player/update-kyc`, data);
    }
  }

  downloadPlayers(params: any) {
    if(this.superAdminAuthService && this.superAdminAuthService.superAdminTokenValue) {
      // ====================== Super Admin url ==============================
      return this.http.post(`super/admin/players/download`, params) ;
    } else {
      // ====================== Admin url ==============================
      return this.http.post(`admin/players/download`, params);
    }
  }

  downloadUserPlayers(params: any) {
    return this.http.post(`admin/usersadmin/player/download`, params);
  }

    // ====================== Super Admin url ==============================

  getSuperAdminPlayer(params: any) {
    return this.http.post(`super/admin/usersadmin/player`, params);
  }

  getSuperAdminPlayerById(id: number) {
    return this.http.get(`super/admin/user/player/${id}`);
  }

  downloadSuperAdminUserPlayers(params: any) {
    return this.http.post(`admin/usersadmin/player/download`, params);
  }

  bulkFinance(params:any){
    return this.http.post(`admin/bulk/bulk-finance`, params)
  }

  bulkFinanceJob(params:any){
    return this.http.post(`admin/bulk/bulk-finance-job`, params)
  }
  listJobs(params:any){
    return this.http.post(`admin/bulk/get-jobs`, params)
  }
  getUserbyJobId(params:any, id:any){
    return this.http.post(`admin/bulk/jobs/${id}`, params)
  }
  retryJob(job_id:any){
    return this.http.post(`admin/bulk/retry/${job_id}`, { id: job_id})
  }
  getPlayerLoginHistoryById(id: number, params: any){
    if (this.superAdminAuthService?.superAdminTokenValue?.length > 0) {
      return this.http.post(`super/admin/user/player/login-history/${id}`,params);
    } else {
      return this.http.post(`admin/user/player/login-history/${id}`,params);
    }
  }

  bulkPlayerEdit(params:any){
    return this.http.post(`admin/user/player/bulk-player-edit`, params)
  }

  getBankDetailsById(id: number, params: any){
    if (this.superAdminAuthService?.superAdminTokenValue?.length > 0) {
      return this.http.post(`super/admin/user/player/bank-details/${id}`,params);
    } else {
      return this.http.post(`admin/user/player/bank-details/${id}`,params);
    }
  }

  updateBankDetail(bankId: number, playerId: number, params: any){
    if (this.superAdminAuthService?.superAdminTokenValue?.length > 0) {
      return this.http.post(`super/admin/user/player/bank-details/update/${bankId}/${playerId}`,params);
    } else {
      return this.http.post(`admin/user/player/bank-details/update/${bankId}/${playerId}`,params);
    }
  }

  createBankDetail(id: number,params: any){
    if (this.superAdminAuthService?.superAdminTokenValue?.length > 0) {
      return this.http.post(`super/admin/user/player/bank-details/create/${id}`,params);
    } else {
      return this.http.post(`admin/user/player/bank-details/create/${id}`,params);
    }
  }
}
