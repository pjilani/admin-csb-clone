import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ListComponent } from './list/list.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { EditComponent } from './edit/edit.component';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { DetailsComponent } from './details/details.component';
import { AdminAuthGuard, CommonAdminAuthGuard, AdminAgentCommonAuthGuard } from 'src/app/guards';
import { SportBettingComponent } from './details/sport-betting/sport-betting.component';
import { PipeModule } from 'src/app/pipes/pipes.module';
import { ListCloneComponent } from './details/list-clone/list-clone.component';
import { JobListComponent } from './job-list/job-list.component';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
const playerRoutes: Routes = [
  { path: 'jobs', component: JobListComponent, canActivate: [CommonAdminAuthGuard] },
  { path: '', component: ListComponent, canActivate: [AdminAgentCommonAuthGuard] },
  { path: ':id', component: EditComponent, canActivate: [CommonAdminAuthGuard] },
  { path: 'details/:id', component: DetailsComponent, canActivate: [CommonAdminAuthGuard] },

];

@NgModule({
  declarations: [ ListComponent, DetailsComponent, EditComponent, SportBettingComponent, ListCloneComponent, JobListComponent ],
  imports: [
    CommonModule,
    RouterModule.forChild(playerRoutes),
    SharedModule,
    ComponentsModule,
    PipeModule,
    DirectivesModule,
    NgxJsonViewerModule,
  ],
  providers: [
    DatePipe, 
  ],
})

export class PlayerModule { }
