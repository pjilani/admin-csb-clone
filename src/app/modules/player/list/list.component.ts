import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/models';
import { AdminAuthService } from '../../admin/services/admin-auth.service';
import { PlayerService } from '../player.service';
import { FINANCE_TYPES, PageSizes, WALLET_TYPES } from 'src/app/shared/constants';
import { Role } from 'src/app/models';
import { DomainForDisableModule } from 'src/app/shared/constants';
import Swal from 'sweetalert2';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { invalid } from 'moment';
import { CurrenciesService } from '../../super-admin/super-admin-modules/currencies/currencies.service';
import { ReportService } from '../../reports/reports.service';
import { PermissionService } from 'src/app/services/permission.service';
declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit, AfterViewInit {
  adminId: number = 0;
  agentAffiliateTokenUtl: any = '';
  isAgent: boolean = false;
  isAllowedModule: any = '';
  players: User[] = [];
  DomainForDisableModule: any = DomainForDisableModule;
  selectedUsers: Map<number, Array<number>>|any = new Map();
  userNames: Map<number , any> = new Map()
  userList :any[]=[];
  pageSizes = PageSizes;
  submitDisable:boolean = true;

  playerParams: any = {
    size: 10,
    page: 1,
    search: '',
    sort_by: 'id',
    order: 'desc',
    status: '',
    adminId: 0,
    phone_verified: '',
    email_verified: '',
    kyc_done: '',
    date_range: {},
    min_amount: null,
    max_amount: null,
    vip_levels: ''
  };

  playerP: number = 1;
  playerTotal: number = 0;
  vipLevelSearch: any;

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Players', path: '/players' },
    { title: 'Players Listing', path: '/players' },
  ];

  roles: string = localStorage.getItem('roles') || '';
  showEditBtn: boolean = true;
  showAffiliateToken: boolean = false;
  format: string = "YYYY-MM-DD HH:mm";
  currencies: any[] = [];
  targetWallete: any | null = null;
  financeForm: FormGroup;
  editForm: FormGroup;
  csvFinanceForm: FormGroup;
  walletTypes = WALLET_TYPES;
  financeTypes = FINANCE_TYPES
  isModalOpen: boolean = false;
  submitted: boolean = false;
  isSubAgentModuleAllowed: any = '';

  source_wallete_id=  new FormControl('', [Validators.required])
  target_wallete_id=  new FormControl('', [Validators.required])
  isBulkDepositEnable: boolean = false;
  isBulkWithdrawEnable: boolean = false;
  isBulkEditEnable: boolean = false;
  isSubAdmin : boolean = false;
  isDepositAdmin : boolean = false;
  isWithdrawalAdmin : boolean = false;

  isLoader:boolean = false;
  isCsvModalOpen: boolean = false;
  csvSubmitted: boolean = false;
  uploadedFileName: any;
  selectFile:any;
  firstTimeApiCall = false;

  @ViewChild('minInput') minInputField!: ElementRef;
  @ViewChild('maxInput') maxInputField!: ElementRef;

  constructor(
    private route: ActivatedRoute,
    private adminAuthService: AdminAuthService,
    public superAdminAuthService: SuperAdminAuthService,
    private playerService: PlayerService,
    private formBuilder: FormBuilder,
    private currenciesService: CurrenciesService,
    private ReportService: ReportService,
    public permissionService: PermissionService
  ) {
    this.playerParams = {
      ...this.playerParams,
      adminId: this.adminId,
    };

    this.financeForm = this.formBuilder.group({
      type: [null, [ Validators.required ]],
      amount: ['', [ Validators.required, Validators.min(1)]],
      wallet_type: [null,[Validators.required]],
      ids: []
    });

    this.editForm = this.formBuilder.group({
      vip_level: ['', [ ]],
      status: ['',[ ]],
      ids: []
    });

    this.csvFinanceForm = this.formBuilder.group({
      type: [1, [ Validators.required ]],
      wallet_type: [null,[Validators.required]],
      csvFile : [null,[Validators.required]],
      isCsv : [true]
    });
  }
  ngAfterViewInit(): void {
    $('[data-toggle="tooltip"]').tooltip()

    $('#time_period').val('');
  }

  ngOnInit(): void {
    this.adminAuthService.adminUser.subscribe((user: any) => {
      if (user && user.id) {
        this.adminId = user.id;
        // this.getPlayers();
        this.getAgentUserList();
      }
    });

    const roles = JSON.parse(this.roles);
    this.showEditBtn = !(
      (roles &&
        roles.findIndex((role: any) => role === Role.Admin) == -1 &&
        roles.findIndex((role: any) => role === Role.Agent) == -1) ||
      (roles.findIndex((role: any) => role === Role.DepositAdmin) > -1 &&
        roles.findIndex((role: any) => role === Role.WithdrawalAdmin) > -1)
    );

    if(roles && roles.findIndex( (role: any) => role === Role.Admin ) == -1 && roles.findIndex( (role: any) => role === Role.Subadmin ) > -1) {
      this.isSubAdmin = true;
    }
    if(roles && roles.findIndex( (role: any) => role === Role.Admin ) == -1 && roles.findIndex( (role: any) => role === Role.DepositAdmin ) > -1) {
      this.isDepositAdmin = true;
    }
    if(roles && roles.findIndex( (role: any) => role === Role.Admin ) == -1 && roles.findIndex( (role: any) => role === Role.WithdrawalAdmin ) > -1) {
      this.isWithdrawalAdmin = true;
    }

    this.showAffiliateToken = !(
      roles && roles.findIndex((role: any) => role === Role.Agent) == -1
    );

    this.isAllowedModule = localStorage.getItem('domain')
      ? localStorage.getItem('domain')
      : '';

    this.isSubAgentModuleAllowed = (localStorage.getItem('allowedModules')?.includes('subAgent') ? true : false);

    if (
      this.adminAuthService.adminTokenValue &&
      this.adminAuthService.adminTokenValue.length > 0
    ) {
      if (this.roles) {
        const roles = JSON.parse(this.roles);
        if (
          roles &&
          roles.findIndex((role: any) => role === Role.Admin) == -1 &&
          roles.findIndex((role: any) => role === Role.Agent) > -1
        ) {
          this.isAgent = true;
        }
      }
    }

    this.financeForm.controls.wallet_type.valueChanges.subscribe(value => {
      if(value == 1){

        if(this.f.type.value == 1){
          
          !this.financeForm.contains('source_wallete_id') && this.financeForm.addControl('source_wallete_id',this.source_wallete_id)
          this.f.amount.setValidators([Validators.required, Validators.min(1), this.amountValidator()])
          this.f.amount.updateValueAndValidity();
        }
        else{
          !this.financeForm.contains('target_wallete_id') && this.financeForm.addControl('target_wallete_id',this.target_wallete_id)
          this.f.amount.setValidators([Validators.required, Validators.min(1)])
          this.f.amount.updateValueAndValidity();
        }
      }else{
        this.financeForm.contains('source_wallete_id') && this.financeForm.removeControl('source_wallete_id')
        this.financeForm.contains('target_wallete_id') && this.financeForm.removeControl('target_wallete_id')
        this.f.amount.setValidators([Validators.required, Validators.min(1)])
        this.f.amount.updateValueAndValidity();
      }

      this.financeForm.updateValueAndValidity();
    })

    //for csv
    this.csvFinanceForm.controls.wallet_type.valueChanges.subscribe(value => {      
      if(value == 1){

        if(this.csvForm.type.value == 1){
          
          !this.csvFinanceForm.contains('source_wallete_id') && this.csvFinanceForm.addControl('source_wallete_id',this.source_wallete_id)
          this.csvFinanceForm.contains('target_wallete_id') && this.csvFinanceForm.removeControl('target_wallete_id')
        }
        else{
          !this.csvFinanceForm.contains('target_wallete_id') && this.csvFinanceForm.addControl('target_wallete_id',this.target_wallete_id)
          this.csvFinanceForm.contains('source_wallete_id') && this.csvFinanceForm.removeControl('source_wallete_id')
        }
      }else{
        this.csvFinanceForm.contains('source_wallete_id') && this.csvFinanceForm.removeControl('source_wallete_id')
        this.csvFinanceForm.contains('target_wallete_id') && this.csvFinanceForm.removeControl('target_wallete_id')
      }

      this.csvFinanceForm.updateValueAndValidity();
    })

    this.csvFinanceForm.controls.type.valueChanges.subscribe(value => {
      if(value){
        this.csvFinanceForm.get('wallet_type')?.updateValueAndValidity();
      }
    })

    this.isBulkDepositEnable = (localStorage.getItem('allowedModules')?.includes('bulkDeposit') ? true : false);
    this.isBulkWithdrawEnable = (localStorage.getItem('allowedModules')?.includes('bulkWithdraw') ? true : false);
    this.isBulkEditEnable = (localStorage.getItem('allowedModules')?.includes('bulkEdit') ? true : false);

    this.getCurrencies()
  }

  playerPageChanged(page: number) {
    this.playerParams = { ...this.playerParams, page };
    this.getPlayers();
  }

  filterPlayers() {
    this.playerP = 1;
    this.vipLevelSearch = this.playerParams.vip_levels.toString()
    this.playerParams = { ...this.playerParams, page: this.playerP, vip_levels: this.vipLevelSearch };
    this.selectedUsers.clear();
    this.userNames.clear()
    this.getPlayers();
  }

  filterSelectAgent(evt: any) {
    // this.playerP = 1;
    this.playerParams = {...this.playerParams, adminId:evt};
    // this.selectedUsers.clear();
    // this.userNames.clear()
    // if(evt)
    // this.getPlayers();    
  }

  setTimePeriod(date_range:any){
    this.playerP = 1
    this.playerParams = { ...this.playerParams, date_range, page:this.playerP}
    this.selectedUsers.clear();
    this.userNames.clear()
    // this.getPlayers();
  }

  getPlayers() {
    this.isLoader = true
    this.firstTimeApiCall = true;
    if(this.playerParams.adminId == 0){
      this.playerParams = {
        ...this.playerParams,
        adminId: this.adminId,
      };
    }
    this.playerService
      .getAdminPlayer(this.playerParams)
      .subscribe((res: any) => {
        this.agentAffiliateTokenUtl = res.record.affiliate_token;
        this.players = res.record.data;
        this.playerTotal = res.record.count;
        this.isLoader = false;
      },(error:any) => {
        this.isLoader = false

      });
  }

  selectVipLevel(vip_level: any) {
    this.submitDisable = false;
    this.e.vip_level.setValue(vip_level);
  }

  selectVipLevelSearch(vip_level: any) {
    this.playerParams= {
      ...this.playerParams,
      vip_levels: vip_level
    }
  }

  playerResetFilter() {
    this.playerP = 1;
    this.playerParams = {
      size: 10,
      page: 1,
      search: '',
      status: '',
      adminId: 0,
      phone_verified: '',
      sort_by: 'id',
      order: 'desc',
      email_verified: '',
      kyc_done: '',
      date_range:{},
      min_amount: null,
      max_amount: null,
      vip_levels: ''
    };
    this.selectedUsers.clear();
    this.userNames.clear()
    this.getPlayers();
    $('#time_period').val('');
    $( "#agent_id" ).val(0).trigger('change');
    $('#vip_levels').val('').trigger('change');

    this.minInputField.nativeElement.value = '';
    this.maxInputField.nativeElement.value = '';
  }

  updatePlayerStatus(player: User, status: number) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to ${player.active ? 'Deactive' : 'Active'} ${
        player.user_name
      }!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, ${player.active ? 'Deactive' : 'Active'} it!`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.playerService
          .updateAdminPlayerStatus(player.id, status)
          .subscribe((res: any) => {
            toastr.success(res.message || 'Player updated successfully');
            this.players = this.players.map((f: User) => {
              if (f.id == player.id) {
                f.active = player.active ? false : true;
              }
              return f;
            });
          });
      }
    });
  }

  copyMessage(val: string) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    toastr.options = {
      progressBar: true,
      preventDuplicates: true,
      onclick: null,
    };
    toastr.info('Copied');
  }

  exportAsXLSX() {
    this.playerService
      .downloadUserPlayers(this.playerParams)
      .subscribe((res: any) => {
        window.location.href = res.record.url;
      });
  }

  selectUser(user: User, evt: any){
    if(evt.target.checked)
    {
      this.selectedUsers.set(this.playerP, (this.selectedUsers.has(this.playerP)) ? [ ...this.selectedUsers.get(this.playerP) , user.id] : [user.id]);
      this.userNames.set(user.id, user);
    }
    else{
      this.selectedUsers.has(this.playerP)?
      this.selectedUsers.set(this.playerP, this.selectedUsers.get(this.playerP).filter((v:any)=> v!== user.id)):
      this.selectedUsers.delete(this.playerP);
      this.userNames.delete(user.id);
    }
  }

  toggleAll(evt:any){
    if(evt.target.checked) {
      const ids = this.players.map(value=> {
      this.userNames.set(value.id, value)
      return value.id});
      this.selectedUsers.set(this.playerP,ids); 
    } else {
      this.selectedUsers.get(this.playerP).forEach((id: number) => this.userNames.delete(id))
      this.selectedUsers.delete(this.playerP)
    };
  }

  get size(){
    let arr: string | any[] = [];
    this.selectedUsers.forEach((d:any)=>{
      arr = [...arr,...d];
    });
    return arr.length;
  }

  selectAdminWallete(event: any) {
    if(this.f.type.value == 1)
      this.targetWallete = this.currencies.find(f => f.id == event.target.value);
  }

  get f() {
    return this.financeForm.controls;
  }

  get e() {
    return this.editForm.controls;
  }

  getNumArr(length: number) {
    return Array.from({ length }).map((_, i) => i);
   }

  openModal(type:number){
    this.f.type.setValue(type);
    this.f.type.updateValueAndValidity();
    this.f.wallet_type.setValue(1);
    this.f.wallet_type.updateValueAndValidity();
    if(type == 1){
      !this.financeForm.contains('source_wallete_id') && this.financeForm.addControl('source_wallete_id',this.source_wallete_id)
      this.financeForm.contains('target_wallete_id') && this.financeForm.removeControl('target_wallete_id')
      this.f.amount.setValidators([Validators.required, Validators.min(1), this.amountValidator()])
      this.f.amount.updateValueAndValidity();
      this.financeForm.updateValueAndValidity();
    }
    else{
      !this.financeForm.contains('target_wallete_id') && this.financeForm.addControl('target_wallete_id',this.target_wallete_id)
      this.financeForm.contains('source_wallete_id') && this.financeForm.removeControl('source_wallete_id')
      this.f.amount.setValidators([Validators.required, Validators.min(1)])
      this.f.amount.updateValueAndValidity();
      this.financeForm.updateValueAndValidity();
    }

    let arr: any[] = [];
    this.selectedUsers.forEach((d:any)=>{
      arr = [...arr,...d];
    });
    this.f.ids.setValue(arr);
    this.f.ids.updateValueAndValidity();
    
    $('#modal-bulk-finance').modal('show');
    this.isModalOpen = true
  }


  openEditModal(){
    // this.f.type.setValue(type);
    // this.f.type.updateValueAndValidity();
    // this.f.wallet_type.setValue(1);
    // this.f.wallet_type.updateValueAndValidity();
    // if(type == 1){
    //   !this.financeForm.contains('source_wallete_id') && this.financeForm.addControl('source_wallete_id',this.source_wallete_id)
    //   this.financeForm.contains('target_wallete_id') && this.financeForm.removeControl('target_wallete_id')
    //   this.f.amount.setValidators([Validators.required, Validators.min(1), this.amountValidator()])
    //   this.f.amount.updateValueAndValidity();
    //   this.financeForm.updateValueAndValidity();
    // }
    // else{
    //   !this.financeForm.contains('target_wallete_id') && this.financeForm.addControl('target_wallete_id',this.target_wallete_id)
    //   this.financeForm.contains('source_wallete_id') && this.financeForm.removeControl('source_wallete_id')
    //   this.f.amount.setValidators([Validators.required, Validators.min(1)])
    //   this.f.amount.updateValueAndValidity();
    //   this.financeForm.updateValueAndValidity();
    // }

    let arr: any[] = [];
    this.selectedUsers.forEach((d:any)=>{
      arr = [...arr,...d];
    });
    this.e.ids.setValue(arr);
    this.e.ids.updateValueAndValidity();
    
    $('#modal-bulk-edit').modal('show');
  }

  closeEditModal(){
    $('#modal-bulk-edit').modal('hide');
    this.submitted =false
  }

  submitEditForm(){
    this.submitted = true
  
    if(this.editForm.invalid){
      return
    }

    if(this.size > 100){
      toastr.error('Selected players should be less than 100');
      return;
    }


    // this.f.amount.setErrors(this.amountValidator(this.f.amount.value))
    this.playerService.bulkPlayerEdit(this.editForm.value).subscribe((res:any)=>{
      toastr.success(res.message || 'Players edited Successfully');
      this.selectedUsers.clear();
      this.userNames.clear();
      $('#vip_level').val([]).trigger('change');
      this.editForm.reset();
      this.getPlayers();
      this.closeEditModal();
    }, err =>{
      this.selectedUsers.clear();
      this.userNames.clear();
      this.closeEditModal();
      $('#vip_level').val([]).trigger('change');
      this.editForm.reset();

    })
  }

  closeModal(){
    $('#modal-bulk-finance').modal('hide');
    this.isModalOpen = false
    this.submitted =false
    this.resetFinanceForm();
  }

  resetFinanceForm(){
    this.financeForm.reset({
      type:null,
      wallet_type:null,
      amount: ''
    });
  }

  resetEditForm(){
    this.editForm.reset({
      status:true,
      vip_level:0,
    });
  }

  submitFinanceForm(){
    this.submitted = true
    if(this.financeForm.invalid){
      return
    }

    if(this.size > 100){
      toastr.error('Selected players should be less than 100');
      return;
    }


    // this.f.amount.setErrors(this.amountValidator(this.f.amount.value))
    this.playerService.bulkFinanceJob(this.financeForm.value).subscribe((res:any)=>{
     
      toastr.success(res.message || 'Job Created Successfully');
      this.selectedUsers.clear();
      this.userNames.clear();
      this.closeModal();
    }, err =>{
      this.selectedUsers.clear();
      this.userNames.clear();
      this.closeModal();
    })
  }

  getCurrencies() {
    if (this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.currenciesService.getCurrencies().subscribe((res: any) => {
        this.currencies = res.record;
      });
    } else {
      this.adminAuthService?.walletData.subscribe((wallets: any) => {
        this.currencies = wallets;
      });
    }
  }

  amountValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const amount = control.value;
      if(this.size && this.targetWallete){
        if (amount * this.size < this.targetWallete.amount) {
          return null; // Validation passes
        } else {
          return { invalidAmount: true }; // Validation fails
        };
      }
      else{
        return null
      }
    };
  }

  getAgentUserList(){
      this.ReportService.getAdminUserList().subscribe((res: any) => {
        this.userList = res.record;
      });
  }
  removeplayer(id:any)
  {
    this.userNames.delete(id);
    this.selectedUsers.has(this.playerP)?
    this.selectedUsers.set(this.playerP, this.selectedUsers.get(this.playerP).filter((v:any)=> v!== id)):
    this.selectedUsers.delete(this.playerP);
  }

  validateMinInput(event: any): void {
    const input = event.target.value;
    const numberValue = parseFloat(input);
    
    const isNumber = /^\d+(\.\d{1,2})?$/;

    if (isNumber.test(input) && numberValue <= parseFloat(this.playerParams.max_amount ?? 10000000)) {
       this.playerParams = {...this.playerParams, min_amount:input}
      //  this.filterPlayers(event)
    }
  }

  validateMaxInput(event: any): void {
    const input = event.target.value;
    const numberValue = parseFloat(input);
    
    const isNumber = /^\d+(\.\d{1,2})?$/;

    if (isNumber.test(input) && numberValue >=  parseFloat(this.playerParams.min_amount ?? 0)) {
      this.playerParams = {...this.playerParams, max_amount:input}
      // this.filterPlayers(event)
    }
  }

  openCsvModal(){
      this.csvForm.type.setValue(1);
      this.csvForm.type.updateValueAndValidity();
      this.csvForm.wallet_type.setValue(1);
      this.csvForm.wallet_type.updateValueAndValidity();
      if(this.csvForm.type.value == 1){
        !this.csvFinanceForm.contains('source_wallete_id') && this.csvFinanceForm.addControl('source_wallete_id',this.source_wallete_id)
        this.csvFinanceForm.contains('target_wallete_id') && this.csvFinanceForm.removeControl('target_wallete_id')
        this.csvFinanceForm.updateValueAndValidity();
      }
      else{
        !this.csvFinanceForm.contains('target_wallete_id') && this.csvFinanceForm.addControl('target_wallete_id',this.target_wallete_id)
        this.csvFinanceForm.contains('source_wallete_id') && this.csvFinanceForm.removeControl('source_wallete_id')
        this.csvFinanceForm.updateValueAndValidity();
      }
      
      $('#modal-bulk-csv-finance').modal('show');
      this.isCsvModalOpen = true
    }

  closeCsvModal(){
    $('#modal-bulk-csv-finance').modal('hide');
    this.isCsvModalOpen = false
    this.csvSubmitted =false
    this.resetCsvFinanceForm();
  }

  resetCsvFinanceForm(){
    this.csvFinanceForm.reset({
      type:1,
      wallet_type:null,
      csvFile:null,
      isCsv:true,
    });

    this.selectFile = undefined
    this.uploadedFileName = undefined
  }

  get csvForm() {
    return this.csvFinanceForm.controls;
  }

  submitCsvFinanceForm(){
    this.csvSubmitted = true
    if(this.csvFinanceForm.invalid){    
      return
    }

    const data = this.csvFinanceForm.value;

    const fd = new FormData();
    
    delete data.csvFile;
    
    if(this.selectFile && this.selectFile.name) {
      fd.append('csvFile', this.selectFile, this.selectFile.name);
    }

    for(let key in data) {
      fd.append(key, data[key]);
    }


    this.playerService.bulkFinanceJob(fd).subscribe((res:any)=>{
      toastr.success(res.message || 'Job Created Successfully');
      this.closeCsvModal();
    }, err =>{
      this.closeCsvModal();
    })
  }

  selectCsv(files: any) {
    if (files.length === 0)
    return;
    const extension = files[0].type.split('/')[1].toLowerCase();
    const imgt = ['csv'].includes(extension);

    if(imgt) {
      // this.selectLogo = files[0];
      this.uploadedFileName = files[0].name;
        const reader = new FileReader();
        // this.imagePath = files;
        reader.readAsDataURL(files[0]); 
        reader.onload = (_event) => { 

          // this.csvForm.csvFile.setValue(csvContent);

          // this.imgURL = reader.result; 
        }
        this.selectFile = files[0];
      } else {
        // this.imgURL = '';
        // this.csvForm.csvFile.setValue(null);
        this.uploadedFileName = undefined;
      }

  }

  sampleCsv(){
    let data = {...this.playerParams, sampleCsv: true}
    this.playerService
      .downloadUserPlayers(data)
      .subscribe((res: any) => {
        window.location.href = res.record.url;
      });
  }

  handleChange(evt:any) {
    var target = evt.target;
    if (target.checked) {
      this.submitDisable = false;
    } 
  }

  setOrder(sort: any) {
    this.playerP =1;
    this.playerParams = { ...this.playerParams, page: this.playerP, order: sort.order, sort_by: sort.column };
    this.selectedUsers.clear();
    this.userNames.clear()
    this.getPlayers();
  }
}
