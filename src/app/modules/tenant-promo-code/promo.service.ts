import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })

export class PromoService {

 

  constructor(private http: HttpClient) { }

  
  // ====================== Admin url ==============================

  
  getAllPromoCodes(params: any = []) {
    return this.http.get(`admin/promo-code/list`, {params: params});
  }

  createPromoCode(data: any) {
    return this.http.post(`admin/promo-code/add`, data);
  }
 
  getPromoDetails(data: any) {
    return this.http.post(`admin/promo-code/detail`, data);
  }

  updatePromoCodeStatus(data:any){
    return this.http.post(`admin/promo-code/status`, data);
      
  }


}
