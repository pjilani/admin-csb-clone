import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Currency, Language, Layout, Admin, Theme } from 'src/app/models';
import { Constants } from 'src/app/shared/constants';
import { CustomValidators } from 'src/app/shared/validators';
import { PromoService } from '../promo.service';
import { DatePipe } from '@angular/common';
import { PermissionService } from 'src/app/services/permission.service';
declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
  providers: [DatePipe]
})

export class DetailsComponent implements OnInit {
  
  id: number = 0;
  pageId: number = 1;
  promo:any = [];
  total:any = 0;
  params: any = {
    size: 10,
    page: 1
  };
  constructor(private route: ActivatedRoute,
    public datepipe: DatePipe,
    private router: Router,
    private promoService: PromoService,
    public permissionService: PermissionService
  ) {
  
    this.id = this.route.snapshot.params['id'];
    this.params = { 
      ...this.params,
      id: this.id,
    }
  }


  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Promo-code', path: '/promo-code/list' },
    { title: 'Details', path: '/' }
  ];

  ngOnInit(): void { 
    if(this.id > 0)
    this.getPromoById();
    
  }

  onPageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getPromoById();
  }


  getPromoById() {
    this.promoService.getPromoDetails(this.params).subscribe((res: any) => {
      this.promo = res.record.list;
      this.total = res.record.total;
      
    });
  }


}
