import { Component, OnInit } from '@angular/core';
import { PromoService } from '../promo.service';
import { PageSizes } from 'src/app/shared/constants';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
declare const toastr: any;
import Swal from 'sweetalert2';
import { PermissionService } from 'src/app/services/permission.service';
@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {

  total: number = 0;
  list: any[] = [];

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Promo-code', path: '/promo-code/list' },
  ];

  constructor(private promoService: PromoService,public superAdminAuthService: SuperAdminAuthService, public permissionService: PermissionService) { }

  ngOnInit(): void {
    this.getList();
  }



  getList() {
    this.promoService.getAllPromoCodes().subscribe((res: any) => {
      this.list = res.record.list;
      this.total = res.record.total;
    });
  }


  updatePromoStatus(id: any, status: boolean) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to `+ (status == true ? 'Activate ' : 'Deactivate') +` it!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, `+ (status == true ? 'Activate ' : 'Deactivate') +` it!`
    }).then((result) => {
      if (result.isConfirmed) {

        this.promoService.updatePromoCodeStatus({ id: id, status :status }).subscribe((res: any) => {
          toastr.success(res.message || 'Promo Code updated successfully' );
          this.getList();
          ;
        });
        
      }

    });
  }
 

}
