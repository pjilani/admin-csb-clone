import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { RouterModule, Routes } from '@angular/router';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { EditComponent } from './edit/edit.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { PipeModule } from 'src/app/pipes/pipes.module';
import { AdminAgentCommonAuthGuard } from 'src/app/guards';
import { DetailsComponent } from './details/details.component';
const tenantsRoutes: Routes = [
  { path: '', component: ListComponent,canActivate: [ AdminAgentCommonAuthGuard ] },
  { path: 'list', component: ListComponent,canActivate: [ AdminAgentCommonAuthGuard ] },
  { path: 'details/:id', component: DetailsComponent,canActivate: [ AdminAgentCommonAuthGuard ] },
  { path: ':id', component: EditComponent,canActivate: [ AdminAgentCommonAuthGuard ] }
]

@NgModule({
  declarations: [
    ListComponent,
    EditComponent,
    DetailsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(tenantsRoutes),
    SharedModule,
    ComponentsModule,
    PipeModule,
    DirectivesModule,
  ]
})

export class PromoModule { }
