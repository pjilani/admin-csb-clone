import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Currency, Language, Layout, Admin, Theme } from 'src/app/models';
import { Constants } from 'src/app/shared/constants';
import { CustomValidators } from 'src/app/shared/validators';
import { PromoService } from '../promo.service';
import { DatePipe } from '@angular/common';
declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  providers: [DatePipe]
})

export class EditComponent implements OnInit {
  
  id: number = 0;
  promo:any = [];
  promoForm: FormGroup | any;
  submitted: boolean = false;
  promoLoader: boolean = false;
  title:any = '';
  minDate:any = '';
  maxDate:any = '';
  
  constructor(private route: ActivatedRoute,
    public datepipe: DatePipe,
    private router: Router,
    private formBuilder: FormBuilder,
    private promoService: PromoService,
  ) {
  
    this.minDate = new Date();
    this.maxDate = new Date();
    this.id = this.route.snapshot.params['id'];
    this.promoForm = this.formBuilder.group({
      promo_name: ['', [ Validators.required ]],
      code: ['', [ Validators.required, Validators.maxLength(8), Validators.minLength(8) ]],
      promo_code_used_in: ['', [ Validators.required ]],
      description: ['', [ Validators.required ]],
      valid_from: ['', [ Validators.required, ]],
      valid_till: ['', [ Validators.required ]],
      promo_code_bonus_type: ['', [ Validators.required ]],
      wallet_type: ['', [ Validators.required ]],
      bonus_amount: ['', [ Validators.required, Validators.min(1) ]],
      status: [''],
      id: ['']
    });

    this.title = 'Add New Promo-code';
    
    if(this.id > 0) {
      this.title = "Edit Promo-code Details";
    }
    let page_title = this.title; 

    this.breadcrumbs.push({ title :'Manage' , path: `/promo/${this.id}` });

  }


  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Promo-code', path: '/promo-code/list' }
  ];

  ngOnInit(): void { 
    if(this.id > 0)
    this.getPromoById(this.id);
    
  }


  get f() {
    return this.promoForm.controls;
  }

  updateValidTill(date:any){
      this.maxDate = new Date(date.target.value);
      $('#valid_till').val('');
  }
 

  getPromoById(id:any) {
    this.promoService.getAllPromoCodes({'id':id}).subscribe((res: any) => {
      this.promo = res.record.list[0];
      
      this.promoForm.patchValue({
        promo_name: this.promo.promo_name,
        code: this.promo.code,
        promo_code_used_in: this.promo.promo_code_used_in,
        description: this.promo.description,
        valid_from: this.datepipe.transform(new Date(this.promo.valid_from), 'yyyy-MM-dd'),
        valid_till: this.datepipe.transform(new Date(this.promo.valid_till), 'yyyy-MM-dd'),
        promo_code_bonus_type: this.promo.promo_code_bonus_type?.toString(),
        wallet_type: this.promo.wallet_type?.toString(),
        bonus_amount: this.promo.bonus_amount,
        status: this.promo.status,
        id: this.promo.id
      });
      
    });
  }

  removeSpace(event:any){
    var k;  
    k = event.charCode;
    return(((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 45 || k== 95) && event.keyCode != 32 ? event:event.preventDefault());

  }
 
  onSubmit() {
    this.submitted = true;

    if (this.promoForm.invalid){
      return;
    } 
    this.promoLoader = true;
    
    const data = this.promoForm.value;
    
      this.promoService.createPromoCode(data)
      .subscribe((response: any) => {
        if(this.id > 0) {
          toastr.success(response?.message || 'Promo code Detail Updated Successfully!');
        }else{
          toastr.success(response?.message || 'New Promo code Created Successfully!');
        }
        this.router.navigate(['/promo-code/list']);
        this.promoLoader = false; 
      }, (err: any) => {
        this.promoLoader = false; 
      });
  
  }

}
