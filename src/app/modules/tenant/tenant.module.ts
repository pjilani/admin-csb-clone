import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { TenantCredentialsComponent } from './tenant-credentials/tenant-credentials.component';
import { ThemeSettingComponent } from './theme-setting/theme-setting.component';
import { TenantConfigurationsComponent } from './tenant-configurations/tenant-configurations.component';
import { BannerComponent } from './banner/banner.component';
import { MenuSettingComponent } from './menu-setting/menu-setting.component';
import { EditCredentialsComponent } from './edit-credentials/edit-credentials.component';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { EditBannerComponent } from './edit-banner/edit-banner.component';
import { AdminAgentCommonAuthGuard, CommonAdminAuthGuard, SuperAdminCommonAuthGuard } from 'src/app/guards';
import { EditSportSettingComponent } from './edit-sport-setting/edit-sport-setting.component';
import { EditPaymentProvidersComponent } from './edit-payment-providers/edit-payment-providers.component';
import { TenantPaymentProvidersComponent } from './tenant-payment-providers/tenant-payment-providers.component';
import { PipeModule } from 'src/app/pipes/pipes.module';
import { TenantRegistrationFieldComponent } from './tenant-registration-fields/tenant-registration-fields.component';
import { SocialMediaSettingComponent } from './social-media-setting/social-media-setting.component';
import { FaqComponent } from './faq/faq.component';
import { EditFaqComponent } from './faq/edit-faq/edit-faq.component';
import { CKEditorModule } from 'ckeditor4-angular';
import { FaqCategoriesComponent } from './faq-categories/faq-categories.component';
import { EditFaqCategoriesComponent } from './faq-categories/edit-faq-categories/edit-faq-categories.component';

const agentRoutes: Routes = [
  { path: '', redirectTo: 'credentials', pathMatch: 'full' },
  { path: 'credentials', component: TenantCredentialsComponent, canActivate: [ AdminAgentCommonAuthGuard ] },
  { path: 'theme-settings', component: ThemeSettingComponent, canActivate: [ AdminAgentCommonAuthGuard ] },
  { path: 'configurations', component: TenantConfigurationsComponent, canActivate: [ AdminAgentCommonAuthGuard ] },
  { path: 'payment-providers', component: TenantPaymentProvidersComponent, canActivate: [ AdminAgentCommonAuthGuard ] },
  { path: 'registration-fields', component: TenantRegistrationFieldComponent, canActivate: [ AdminAgentCommonAuthGuard ] },
  { path: 'banners', component: BannerComponent, canActivate: [ AdminAgentCommonAuthGuard ] },
  { path: 'menu-setting', component: MenuSettingComponent, canActivate: [ AdminAgentCommonAuthGuard ] },
  { path: 'banners/:id', component: EditBannerComponent, canActivate: [ AdminAgentCommonAuthGuard ] },
  { path: 'credentials/:credentialId', component: EditCredentialsComponent, canActivate: [ CommonAdminAuthGuard ] },
  { path: 'payment-providers/:providerId', component: EditPaymentProvidersComponent, canActivate: [ CommonAdminAuthGuard ] },
  { path: 'sport-settings/:tenantId/:id', component: EditSportSettingComponent, canActivate: [ CommonAdminAuthGuard ] },
  { path: 'social-media-settings', component: SocialMediaSettingComponent, canActivate: [ AdminAgentCommonAuthGuard ] },
  { path: 'faqs', component: FaqComponent, canActivate: [ AdminAgentCommonAuthGuard ] },
  { path: 'faqs/:id', component: EditFaqComponent, canActivate: [ AdminAgentCommonAuthGuard ] },
  { path: 'faq/categories', component: FaqCategoriesComponent, canActivate: [ AdminAgentCommonAuthGuard ] },
  { path: 'faq/categories/:id', component: EditFaqCategoriesComponent, canActivate: [ AdminAgentCommonAuthGuard ] },
];

@NgModule({

  declarations: [
    TenantCredentialsComponent, ThemeSettingComponent, TenantConfigurationsComponent, BannerComponent, MenuSettingComponent,
    EditCredentialsComponent, EditBannerComponent, EditSportSettingComponent, EditPaymentProvidersComponent, TenantPaymentProvidersComponent,TenantRegistrationFieldComponent, SocialMediaSettingComponent, FaqComponent, EditFaqComponent, FaqCategoriesComponent, EditFaqCategoriesComponent
  ],

  imports: [
    CommonModule,
    CKEditorModule,
    RouterModule.forChild(agentRoutes),
    SharedModule,
    ComponentsModule,
    PipeModule,
    DirectivesModule
  ]

})

export class TenantModule { }
