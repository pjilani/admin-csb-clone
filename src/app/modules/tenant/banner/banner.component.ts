import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Banner } from 'src/app/models';
import { TenantService } from '../tenant.service';

import Swal from 'sweetalert2';
import { PermissionService } from 'src/app/services/permission.service';

declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})

export class BannerComponent implements OnInit, AfterViewInit {

  banners: Banner[] = [];
  banner!: Banner;

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Banners', path: '/tenants/banners' }
  ];

  constructor(
    public tenantService: TenantService,
    public permissionService: PermissionService) {  }
    
  ngOnInit(): void {
    this.getBanners();
  }
  
  ngAfterViewInit(): void {
  // jQuery UI sortable for the todo list
  if(!this.isTouchDevice && this.permissionService.checkPermission('tenant_banners','U')){
    const that = this;
    
    $('table tbody').sortable({
      helper: '.handle',
      zIndex: 999999,
      // items : "> [data-order]",
      update : function() {

        const ids = $(this).children().get().map(function (el: any) {
          return el.id;
        });

        that.updateOrder(ids);
        
      }
    }).disableSelection();
  }

  }

  updateOrder(idsArr: Array<string>) {
    this.tenantService.updateTenantBannerOrder({ orders: idsArr }).subscribe((res: any) => {
      toastr.success(res.message || 'Banner orders updated successfully.');
    });
  }

  getBanners() {
    this.tenantService.getAdminBanners().subscribe((res: any) => {
      this.banners = res.record;
    });
  }

  deleteBanner(banner: Banner) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to delete ${banner.name}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, delete it!`
    }).then((result) => {
      if (result.isConfirmed) {

        this.tenantService.deleteAdminBanner(banner).subscribe((res: any) => {
          this.banners = this.banners.filter((f: Banner) => f.id != banner.id);
        });
        
      }
    });
  
  }

  selectBanner(banner: Banner) {
    this.banner = banner;
    // console.log(this.banner);
  }

  get isTouchDevice() {
    try{ document.createEvent("TouchEvent"); return true; }
    catch(e){ return false; }
  }

}

