import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminAuthService } from '../../admin/services/admin-auth.service';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
import { TenantService } from 'src/app/modules/tenant/tenant.service';
import { PermissionService } from 'src/app/services/permission.service';

declare const $: any;
declare const toastr: any;

@Component({
  selector: 'app-edit-sport-setting',
  // template: `dd`,
  templateUrl: './edit-sport-setting.component.html',
  styleUrls: ['./edit-sport-setting.component.scss']
})
  
export class EditSportSettingComponent implements OnInit {

  setting: any;

  tenantId: number = 0;
  settingId: number = 0;
    
  adminForm: FormGroup | any;
  submitted: boolean = false;
  adminLoader: boolean = false;

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' }
  ];

  constructor(private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private tenantService: TenantService,
    public superAdminAuthService: SuperAdminAuthService,
    public adminAuthService: AdminAuthService,
    public permissionService: PermissionService
    ) {
     this.tenantId = this.route.snapshot.params['tenantId'];
     this.settingId = this.route.snapshot.params['id'];
     
    this.adminForm = this.formBuilder.group({
      id: [ '', Validators.required ],
      event_liability: [ '', Validators.required ],
      max_single_bet: ['', [ Validators.required ]],
      min_bet: ['', [ Validators.required ]],
      max_multiple_bet: ['', [ Validators.required ]],
      // max_bet_on_event: ['', [ Validators.required ]],
      deposit_limit: ['', [ Validators.required ]],
      max_win_amount: ['', [ Validators.required ]],
      max_odd: ['', [ Validators.required ]],
      cashout_percentage: ['', [Validators.required]],
      bet_disabled: ['']
    });

  }

  ngOnInit(): void {

    if (this.superAdminAuthService?.superAdminTokenValue?.length > 0) {
      this.breadcrumbs.push({ title: 'Tenants', path: '/super-admin/tenants' });
      this.breadcrumbs.push({ title: 'Tenants Settings', path: '/super-admin/tenants' });
    } else {
      this.breadcrumbs.push({ title: 'Tenants Settings', path: `/tenants/sport-settings/${this.tenantId}/${this.settingId}` });
    }
    
    this.tenantService.getTenantSportSettings(this.settingId).subscribe((res: any) => {

      this.setting = res.record;

      this.adminForm.patchValue(this.setting);

    });
  }

  get f() {
    return this.adminForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid

    // console.log(this.adminForm.value);

    if (this.adminForm.invalid) return;    

    this.adminLoader = true;
    
    this.tenantService.updateTenantSportSettings(this.adminForm.value).subscribe((res: any) => {
      toastr.success(res.message || 'Tenant Setting update successfully!');
      this.adminLoader = false;
    }, err => this.adminLoader = false );
    
  }

  onKeyPress(event: any) {
    const regexpNumber = /[0-9\+\-\ ]/;
    let inputCharacter = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !regexpNumber.test(inputCharacter)) {
      event.preventDefault();
    }
  }
}
