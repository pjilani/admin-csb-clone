import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSportSettingComponent } from './edit-sport-setting.component';

describe('EditSportSettingComponent', () => {
  let component: EditSportSettingComponent;
  let fixture: ComponentFixture<EditSportSettingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditSportSettingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSportSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
