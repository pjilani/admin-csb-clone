import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Banner } from 'src/app/models';
import { CustomValidators } from 'src/app/shared/validators';
import { TenantService } from '../tenant.service';

declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './edit-banner.component.html',
  styleUrls: ['./edit-banner.component.scss']
})

export class EditBannerComponent implements OnInit {
  @ViewChild('file') logo_file!: ElementRef;

  tenantForm: FormGroup | any;
  submitted: boolean = false;
  tenantLoader: boolean = false;
  selectLogo!: File | null;
  imageURL: any = '';
  title: string = 'Create';

  banner!: Banner;
  bannerId: number = 0;

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Banners', path: `/tenants/banners` },
    { title: 'Banner', path: `/tenants/banners/0` }
  ];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private tenantService: TenantService
    ) {

    this.bannerId = this.route.snapshot.params['id'];

    this.tenantForm = this.formBuilder.group({
      name: ['', [ Validators.required ]],
      redirect_url: ['', [ Validators.pattern(CustomValidators.urlRegex) ]],
      banner_type: ['image', [ Validators.required ]],
      logo: ['', [ CustomValidators.requiredFileType(CustomValidators.imgType)] ],
      enabled: [false, Validators.required],
      game_name: [''],
      open_table: [''],
      provider_name: ['']
      // order: [1, Validators.required]
    });

    
    if (this.bannerId > 0) {
      
      this.title = 'Edit';

      this.banner = history.state.banner;

      if(this.banner && this.banner.id) {
            this.tenantForm.patchValue({
              name: this.banner.name,
              redirect_url: this.banner.redirect_url,
              banner_type: this.banner.banner_type,
              enabled: this.banner.enabled,
              game_name: (this.banner.game_name)?this.banner.game_name:'',
              open_table: (this.banner.open_table)?this.banner.open_table:'',
              provider_name: (this.banner.provider_name)?this.banner.provider_name:''
              // order: this.banner.order
            });
        
        this.imageURL = this.banner.image_url;
      
      } else {
        this.router.navigate(['/tenants/banners']);
      }

      this.selectType({ target: { value: this.banner.banner_type } });
      
    } else {
      this.selectType({ target: { value: 'image' } });
    }

  }

  ngOnInit(): void {  }

  get f() {
    return this.tenantForm.controls;
  }

  selectImage(files: any) {
    const that = this;

    if (files.length === 0) return;

    const extension = files[0].name.split('.')[1].toLowerCase();

    let check = CustomValidators.imgType.includes(extension);

    if (this.f.banner_type.value === 'image') {
      check = CustomValidators.imgType.includes(extension);
    } else if(this.f.banner_type.value === 'video') {
      check = CustomValidators.videoTypes.includes(extension);
    }

    if(check) {
      
      if(files && files[0]['size']) {
        const fileSize = files[0]['size'] / 1024 / 1024;

        if (this.f.banner_type.value === 'image') {

          if (fileSize && fileSize > 1) {            
            this.imageURL = '';
            this.f.logo.setValue('');
            this.selectLogo = null;
            toastr.error(`File must be less than 1000KB`);
            return;
          }

          const reader = new FileReader();
          reader.readAsDataURL(files[0]);
          reader.onload = (_event) => {

            const src: any = reader.result;
            const img = new Image();

            img.onload = function (this: any) {
              if (this.width > 2000) {
                that.selectLogo = null;
                that.imageURL = '';
                that.f.logo.setValue('');
                toastr.error(`File width pixel must be less than 2000PX`);
              } else {
                that.imageURL = src;
              }
            };

            img.src = src;

          }

        } else if (this.f.banner_type.value === 'video') {
          if (fileSize && fileSize > 10) {
            this.f.logo.setValue('');
            this.selectLogo = null;
            toastr.error(`File is bigger than 10MB`);
            return;
          }

          const reader = new FileReader();
          reader.readAsDataURL(files[0]);
          reader.onload = (_event) => {
            that.imageURL = reader.result;
          }

        }

      }

      this.selectLogo = files[0];
      
    } else {
      toastr.error(`Please select ${this.f.banner_type.value}`);
    }

  }



  selectType(event: any) {
    const logoControl = this.tenantForm.get('logo');

    if (event.target.value === 'image') {
      if (this.bannerId > 0) {
        logoControl.setValidators([CustomValidators.requiredFileType(CustomValidators.imgType)]); 
      } else {
        this.imageURL = null;
        logoControl.setValidators([Validators.required, CustomValidators.requiredFileType(CustomValidators.imgType)]); 
      }
    } else if (event.target.value === 'video') {
      if (this.bannerId > 0) {
        logoControl.setValidators([CustomValidators.requiredFileType(CustomValidators.videoTypes)]); 
      } else {
        this.imageURL = null;
        logoControl.setValidators([Validators.required, CustomValidators.requiredFileType(CustomValidators.videoTypes)]); 
      }
    }

    logoControl.updateValueAndValidity();

    this.f.logo.setValue('');
    this.selectLogo = null;
    
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid

    if (this.tenantForm.invalid) return;
    
    this.tenantLoader = true;

    const data = this.tenantForm.value;
    const fd = new FormData();
    
    delete data.logo;

    if (this.selectLogo && this.selectLogo.name) {
      fd.append('image', this.selectLogo, this.selectLogo.name);
    }

    for (let key in data) {
      fd.append(key, data[key]);
    }

    // debugger;
    
    if(this.bannerId > 0) {
      fd.append('id', `${this.bannerId}`);

      this.tenantService.updateAdminBanner(fd)
      .subscribe((response: any) => {
        toastr.success(response?.message || 'Tenant Banner Updated Successfully!');
        this.router.navigate(['/tenants/banners']);
        this.tenantLoader = false; 
      }, (err: any) => {
        this.tenantLoader = false; 
      });
    } else {
      this.tenantService.createAdminBanner(fd)
      .subscribe((response: any) => {
        toastr.success(response?.message || 'Tenant Banner Updated Successfully!');
        this.router.navigate(['/tenants/banners']);
        this.tenantLoader = false; 
      }, (err: any) => {
        this.tenantLoader = false; 
      });
    }
      
  }

}
