import {Component, OnInit, ElementRef, ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TenantService } from '../tenant.service';
import Swal from 'sweetalert2';
import {socialMedia} from "../../../models";
import { CustomValidators } from 'src/app/shared/validators';
declare const toastr: any;
declare const $: any;

@Component({
  selector: 'app-social-media-setting',
  templateUrl: './social-media-setting.component.html',
  styleUrls: ['./social-media-setting.component.scss']
})
export class SocialMediaSettingComponent implements OnInit {

  @ViewChild('logo_file', { static: false }) logo_file!: ElementRef;
  p: number = 1;
  menuId: any;
  menuName:any;
  socialMedia:any;
  socialMedias: socialMedia[] = [];
  imageForm: FormGroup | any;
  submitted: boolean = false;
  tenantLoader: boolean = false;
  socialMediaForm: FormGroup | any;
  imgURL: any;
  iconForm: FormGroup | any;
  selectLogo: File | null = null;
  breadcrumbs: Array<any> = [
      {title: 'Home', path: '/super-admin'},
      {title: 'Menu', path: '/super-admin/menu'},
  ];

  constructor(private formBuilder: FormBuilder,private tenantService: TenantService) {
        this.socialMediaForm = this.formBuilder.group({
          name: ['', [ Validators.required ]],
          image: ['', ],
          redirect_url: ['', [ Validators.required, Validators.pattern(CustomValidators.urlRegex) ]],
        });
  }

  ngOnInit(): void {
      this.getSocialMedia();
  }
  get f() {
      return this.socialMediaForm.controls;
    }

    getSocialMedia() {
      this.tenantService.getSocialMedia().subscribe((res: any) => {
          this.socialMedias = res.record;
      });
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.socialMediaForm.invalid) return;
    if(this.socialMedia && this.socialMedia.id > 0) {
      const data = this.socialMediaForm.value;
      const fd = new FormData();
      fd.append('id', `${this.socialMedia.id}`);
      fd.append('name', `${data.name}`);
      fd.append('redirect_url', `${data.redirect_url}`);
      if (this.selectLogo && this.selectLogo.name) {
        fd.append('image', this.selectLogo, this.selectLogo.name);
      }
  
      for (let key in data) {
        fd.append(key, data[key]);
      }
      this.tenantLoader = true;
    this.tenantService.updateSocialMedia(fd)
      .subscribe((res: any) => {
        toastr.success(res.message || 'Page updated successfully.');
        

        this.resetSocialForm();
        this.socialMediaForm.reset();
        this.getSocialMedia();
        this.imgURL = '';
          
      }, (err: any) => {
        this.tenantLoader = false; 
      });

    } else {

      const data = this.socialMediaForm.value;
      const fd = new FormData();
      delete data.image;
      delete data.top_menu_id;
      // fd.append('top_menu_id', `${this.topPageId}`);
      if (this.selectLogo && this.selectLogo.name) {
        fd.append('image', this.selectLogo, this.selectLogo.name);
      }
  
      for (let key in data) {
        fd.append(key, data[key]);
      }
      this.tenantLoader = true;

      this.tenantService.createSocialMedia(fd)
      .subscribe((res: any) => {

        toastr.success(res.message || 'Page created successfully.');
        
        this.resetSocialForm();
        this.socialMediaForm.reset();
        this.getSocialMedia();
        
        // if(res?.record?.id) {
        //     this.pages.push(res.record);
        // } else {
          // this.getPages(this.topPageId);
          this.imgURL = '';
        // }
        
      }, (err: any) => {
        this.tenantLoader = false; 
      });
      
    }
  }

    selectImage(files: any) {
      this.imgURL = '';
      if (files.length === 0)
        return;
  
      if ((files[0].size / 1000000) > 5) {
  
        this.f.image.setErrors({ 'required': 'File is required' });
        toastr.error( 'Max file size limit reached');
        // toastr.error(`Max file size is 5MB`);
        return;
      }
  
      const extension = files[0].name.split('.')[1].toLowerCase();
      let check = CustomValidators.imgType.includes(extension);

      if (check) {
  
        const reader = new FileReader();
        reader.readAsDataURL(files[0]);
        const img = new Image();
        img.src = window.URL.createObjectURL( files[0] );
        reader.onload = (_event) => {
        //   const width = img.naturalWidth;
        //    const height = img.naturalHeight;
        //    console.log(width, height);
        //    if( width > 30 || height > 30 ) {
        //       toastr.error("Icon should be less then 30 x 30 size");
        //     this.f.logo.setErrors({ 'dimension': 'Icon should be less then 30 x 30 size' });
        //     this.imgURL = '';
        //  }
  
          this.imgURL = reader.result;
        }
        this.selectLogo = files[0];
      } else {
        this.imgURL = '';
        toastr.error( 'Image extension should be svg or png');
        // toastr.error(`Please select ${this.f.banner_type.value}`);
      }
  
    }

  // delete(menu: MenuMaster) {
  //     Swal.fire({
  //         title: 'Are you sure?',
  //         text: `You want to delete ${menu.name}!`,
  //         icon: 'warning',
  //         showCancelButton: true,
  //         confirmButtonColor: '#3085d6',
  //         cancelButtonColor: '#d33',
  //         confirmButtonText: `Yes, delete it!`
  //     }).then((result) => {
  //         if (result.isConfirmed) {

  //             this.menuService.deleteMenu(menu).subscribe((res: any) => {
  //                 toastr.success(res.message || 'Currency Deleted Successfully.');
  //                 this.getMenus();
  //             });

  //         }
  //     });

  // }
  createPageForm() {
    // this.socialMedia = null;
    this.socialMedia.id=null;
    this.imgURL = null;
    this.submitted = false;
    this.socialMediaForm.controls["image"].setValidators(Validators.required);
    this.socialMediaForm.controls["image"].updateValueAndValidity();
    this.socialMediaForm.patchValue({
      name: '',
      redirect_url: '',
      image: ''
    });
  }

  resetSocialForm() {
    $('#modal-page').modal('hide');
    this.socialMediaForm.reset();
    this.tenantLoader = false;
    this.submitted = false;
  }

  selectSocialMedia(socialMedia: any) {
    this.submitted = false;
    this.socialMedia = socialMedia;
    this.imgURL = this.socialMedia.image;
    this.logo_file.nativeElement.value = '';
    this.selectLogo = null
    this.socialMediaForm.patchValue({
      name: socialMedia.name,
      redirect_url: socialMedia.redirect_url,
    });
    this.socialMediaForm.controls["image"].clearValidators();
    this.socialMediaForm.controls["image"].updateValueAndValidity();

  }

  activeSocialMediaStatus(socialMedia: socialMedia) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to Active ${socialMedia.name}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Active it!'
    }).then((result) => {
      if (result.isConfirmed) {

        this.tenantService.activeSocialMediaStatus(socialMedia.id).subscribe((res: any) => {
          toastr.success(res.message || 'Player updated successfully' );
          this.socialMedias = this.socialMedias.map((f: socialMedia) => {
            if(f.id == socialMedia.id) {
              f.status =  true;
            }
            return f;
          });
        });
        
      }
    });

  }

  deactiveSocialMediaStatus(socialMedia: socialMedia) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to Deactive ${socialMedia.name}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Deactive it!'
    }).then((result) => {
      if (result.isConfirmed) {

        this.tenantService.deactiveSocialMediaStatus(socialMedia.id).subscribe((res: any) => {
          toastr.success(res.message || 'Player updated successfully' );
          this.socialMedias = this.socialMedias.map((f: socialMedia) => {
            if(f.id == socialMedia.id) {
              f.status = false;
            }
            return f;
          });
        });
        
      }
    });
    
  }




}
