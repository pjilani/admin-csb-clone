import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TenantService } from '../tenant.service';
import { AdminAuthService } from 'src/app/modules/admin/services/admin-auth.service';
import { CustomValidators } from 'src/app/shared/validators';
import { ComparePassword } from 'src/app/shared/validators/confirm-password.validator';
declare const toastr: any;
declare const $: any;


@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  constructor(
    private route: ActivatedRoute, 
    private adminAuthService: AdminAuthService,
    private tenantService: TenantService,
    private router: Router,
    private formBuilder: FormBuilder) { }

  breadcrumbs: Array<any> = [];
  hideShowOldPass: boolean = false;
  hideShowNewPass: boolean = false;
  hideShowConfirmPass: boolean = false;
  id:number =0;
  submitted: boolean = false;
  title: string = 'ACTION_BUTTON.CREATE';
  
  changePasswordForm: FormGroup | any;
  userRoleLoader: boolean = false;
  userRoleDetails:any;
  translations = {
    HOME: '',
    USERS: '',
    PLAYERS: '',
  };

  ngOnInit(): void {

    this.changePasswordForm = this.formBuilder.group({
      old_password: ['', [Validators.required]],
      new_password: ['', [Validators.required, Validators.pattern(CustomValidators.passwordRegex)]],
      confirm_password: ['', [Validators.required, Validators.pattern(CustomValidators.passwordRegex)]]
    }, { 
      validator: ComparePassword('new_password', 'confirm_password')
    });
  }

  get f() {
    return this.changePasswordForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    // debugger
    if (this.changePasswordForm.invalid) return ;

    this.userRoleLoader = true;

    const data = this.changePasswordForm.value;
    data.confirm_password = btoa(data.confirm_password); 
    data.old_password = btoa(data.old_password);
    delete data.new_password;

      this.tenantService
        .changeAdminPassword(data)
        .subscribe(
          (response: any) => {
            if (response?.message) {
              toastr.success(response?.message || 'Password changed successfully.');
              this.logout();
              this.router.navigate(['/']);
            }
          },
          (err:any) => {
            console.log(err);
            this.userRoleLoader = false;
          }
        );
    
  }

  logout() {
    $('#preloader').css('height', '');
    $('#preloader img').show();

      this.adminAuthService.logout().subscribe(() => {
        $('#preloader').css('height', 0);
        $('#preloader img').hide();
      });
  

  }



}
