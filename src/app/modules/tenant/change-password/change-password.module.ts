import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { ChangePasswordComponent } from '../change-password/change-password.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonAdminAuthGuard } from 'src/app/guards';

const changePasswordRoutes: Routes = [
  {
    path: '', component: ChangePasswordComponent,
    canActivate: [CommonAdminAuthGuard],
  },
];

@NgModule({
  declarations: [ChangePasswordComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(changePasswordRoutes),
    SharedModule,
    ComponentsModule,
    DirectivesModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ChangePasswordModule { }
