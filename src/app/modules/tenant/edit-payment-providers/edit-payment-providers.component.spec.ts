import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPaymentProvidersComponent } from './edit-payment-providers.component';

describe('EditPaymentProvidersComponent', () => {
  let component: EditPaymentProvidersComponent;
  let fixture: ComponentFixture<EditPaymentProvidersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditPaymentProvidersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPaymentProvidersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
