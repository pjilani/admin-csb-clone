import { Component, OnInit } from '@angular/core';
import {  AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {  Tenant } from 'src/app/models';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
import { TenantService } from './../tenant.service';
declare const $: any;
declare const toastr: any;
@Component({
  selector: 'app-edit-payment-providers',
  templateUrl: './edit-payment-providers.component.html',
  styleUrls: ['./edit-payment-providers.component.scss']
})
export class EditPaymentProvidersComponent implements OnInit {



  providerId: number = 0;
  providerList:any[] = [];
  paymentProvider!: any;
  removeControl:any[] = [];
  selectedValue:any = [];
  tenant!: Tenant;
  title: string = 'Create';
  remainingProvider: any[] = [];

  paymentForm: FormGroup | any;
  submitted: boolean = false;
  providerLoader: boolean = false;

  breadcrumbs: Array<any> = [];
  paymentGatewayCategoriesEnable: boolean = false;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private tenantService: TenantService,
    public superAdminAuthService: SuperAdminAuthService
  ) {
    this.providerId = this.route.snapshot.params['providerId'];
    this.paymentForm = this.formBuilder.group({
      paymentProvider: ['', [Validators.required]],
      description: ['', [Validators.required]],
      status: [true],
      vip_level: [null],
      enable_limits: [false],
      min_deposit_limit:[null],
      max_deposit_limit:[null]
    });

    this.remainingProvider = history.state.remprods ? history.state.remprods : [];
    this.tenant = history.state.tenant;
    if (history.state.paymentProviders)
      this.paymentProvider = history.state.paymentProviders;

    
    if (this.providerId > 0) {
      this.title = 'Edit';

      if (this.paymentProvider?.provider_id && this.paymentProvider?.provider_name) {
        
        this.paymentForm.patchValue({
          paymentProvider: this.paymentProvider.provider_id,
          status: (this.paymentProvider.active),
          description: this.paymentProvider.description,
          vip_level:(this.paymentProvider?.vip_level) ? JSON.parse(this.paymentProvider?.vip_level) : null,
          max_deposit_limit : this.paymentProvider.max_deposit_limit,
          min_deposit_limit : this.paymentProvider.min_deposit_limit,
          enable_limits : (this.paymentProvider.min_deposit_limit && this.paymentProvider.max_deposit_limit)
        });

        this.remainingProvider.push({
          id: this.paymentProvider.provider_id,
          provider_keys: this.paymentProvider.provider_key_values,
          created_at: '',
          updated_at: '',
          provider_name: this.paymentProvider.provider_name,
          provider_type: this.paymentProvider.provider_type
        });

        setTimeout(() => {
          $("#paymentProvider").val(this.paymentProvider.provider_id).trigger('change');
          var vipLevels = (this.paymentForm.get('vip_level').value)? this.paymentForm.get('vip_level').value.map(Number):null;
          
          $("#vip_level").val(vipLevels).trigger('change');
        }, 100);
        this.selectedValue = this.paymentProvider.provider_key_values;
        this.changeKey(this.paymentProvider.provider_id);

      } else {
        console.log('sdfhgfsdf');
        
        this.redirectTo();
      }
    } else {
      this.changeKey(this.remainingProvider[0].id);
    }


    if (this.tenant?.id && this.remainingProvider.length > 0) {
      if (this.superAdminAuthService.superAdminTokenValue?.length > 0) {
        this.breadcrumbs = [
          { title: 'Home', path: '/super-admin' },
          { title: 'Tenants', path: '/super-admin/tenants' },
          { title: "Setup Payment Configurations", path: "" }
        ]
      } else {
        this.breadcrumbs = [
          { title: 'Home', path: '/' },
          { title: 'Tenant Payment Configurations', path: `/tenants/credentials` },
          { title: this.title, path: `/tenants/credentials/${this.tenant.id}` }
        ]
      }
    } else {
      console.log('ss');
      
      this.redirectTo();
    }

    if (this.superAdminAuthService.superAdminTokenValue?.length > 0){
      
      this.tenantService.getTenant(this.tenant?.id).subscribe((res: any) => {
        this.paymentGatewayCategoriesEnable = (res.record?.setting?.allowed_modules?.includes('paymentGatewayCategoriesEnable')) ? true : false;
        
        if(this.paymentGatewayCategoriesEnable){      
          this.paymentForm.get('vip_level').setValidators([Validators.required])
        }else{
          this.paymentForm.get('vip_level').clearValidators();
        }

        setTimeout(() => {
          var vipLevels = (this.paymentForm.get('vip_level').value)? this.paymentForm.get('vip_level').value.map(Number):null;
          $("#vip_level").val(vipLevels).trigger('change');
        }, 100);
      });
    }else{
      this.paymentGatewayCategoriesEnable = (localStorage.getItem('allowedModules')?.includes('paymentGatewayCategoriesEnable') ? true : false);
      if(this.paymentGatewayCategoriesEnable){      
        this.paymentForm.get('vip_level').setValidators([Validators.required])
      }else{
        this.paymentForm.get('vip_level').clearValidators();
      }

      setTimeout(() => {
        var vipLevels = (this.paymentForm.get('vip_level').value)? this.paymentForm.get('vip_level').value.map(Number):null;
        $("#vip_level").val(vipLevels).trigger('change');
      }, 100);
    }

  }

  ngOnInit(): void {
    this.paymentForm.get('enable_limits').valueChanges.subscribe((value:any) => {      
      if(value){
        this.paymentForm.get('min_deposit_limit').setValidators([Validators.required, Validators.min(0)]);
        this.paymentForm.get('max_deposit_limit').setValidators([Validators.required, Validators.min(0), this.minValueValidator(this.paymentForm.get('min_deposit_limit'))]);
      }else{
        this.paymentForm.get('min_deposit_limit').setValue(null);
        this.paymentForm.get('max_deposit_limit').setValue(null);
        this.paymentForm.get('min_deposit_limit').clearValidators();
        this.paymentForm.get('max_deposit_limit').clearValidators();     
      }

      this.paymentForm.get('min_deposit_limit').updateValueAndValidity();
      this.paymentForm.get('max_deposit_limit').updateValueAndValidity();
    })

    this.paymentForm.get('max_deposit_limit').valueChanges.subscribe((value:any) => {
      if(value){
        this.paymentForm.get('min_deposit_limit').updateValueAndValidity();
      }
    })
  }


 

  redirectTo() {
    if (this.superAdminAuthService.superAdminTokenValue?.length > 0) {
      if (this.tenant?.id) {
        this.router.navigate([`/super-admin/tenants/details`, this.tenant.id]);
      } else {
        this.router.navigate([`/super-admin/tenants`]);
      }
    } else {
      this.router.navigate([`/tenants/payment-providers`]);
    }
  }

  changeKey(key: any) {
    this.createDynamicForm(key);
    this.f.paymentProvider.setValue(key);
  }

 

  createDynamicForm(id:any){
    this.tenantService.paymentProviderById(id)
        .subscribe((response: any) => {
          for(var i=this.removeControl.length;i-->0;){
            this.paymentForm.removeControl(this.removeControl[i]); 
          }

            this.providerList = JSON.parse(response.record.provider_keys);
            let providerListValues:any = [];
             
            
            if(this.selectedValue != '')
            providerListValues = JSON.parse(this.selectedValue);

            for(var i=this.removeControl.length;i-->0;){
              this.paymentForm.removeControl(this.removeControl[i]); 
            }

            JSON.parse(response.record.provider_keys).forEach((x:any, index:any) =>{
              this.removeControl.push(x);
              if(id == response.record.id && this.selectedValue != ''){
                this.paymentForm.addControl(x,new FormControl((providerListValues[x] != '' ? providerListValues[x] : '')));
              }else{
                this.paymentForm.addControl(x,new FormControl(''));
              }

             })
          
        }, (err: any) => {
          
        });
  }

  get f() {
    return this.paymentForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid

     console.log(this.paymentForm);
     

    if (this.paymentForm.invalid) return;

    if(this.paymentForm.value.enable_limits && this.paymentForm.value.min_deposit_limit > this.paymentForm.value.max_deposit_limit){
      toastr.error("Min value should be not be greater than Max value");
      return
    }

    this.providerLoader = true;
    const data = this.paymentForm.value;

    delete data.enable_limits;
    

    if(this.superAdminAuthService.superAdminTokenValue?.length > 0) {

      data.tenant_id = this.tenant.id;

      if(this.providerId > 0) {
        data.id = this.providerId;

        this.tenantService.updateSuperAdminPaymentProvider(data)
        .subscribe((response: any) => {
          toastr.success(response?.message || 'Payment Provider Updated Successfully!');
          this.providerLoader = false;
        }, (err: any) => {
          this.providerLoader = false;
        });

      } else {
        this.tenantService.createSuperAdminPaymentProvider(data)
        .subscribe((response: any) => {
          toastr.success(response?.message || 'Payment Provider Created Successfully!');
          this.router.navigate(['/super-admin/tenants/details', this.tenant.id]);
          this.providerLoader = false;
        }, (err: any) => {
          this.providerLoader = false;
        });

      }

    } else {

      if(this.providerId > 0) {
        data.id = this.providerId;

        this.tenantService.updateAdminPaymentProvider(data)
        .subscribe((response: any) => {
          toastr.success(response?.message || 'Payment Provider Updated Successfully!');
          this.providerLoader = false;
        }, (err: any) => {
          this.providerLoader = false;
        });

      } else {
        this.tenantService.createAdminPaymentProvider(data)
        .subscribe((response: any) => {
          toastr.success(response?.message || 'Payment Provider Created Successfully!');
          this.router.navigate(['/tenants/payment-providers']);
          this.providerLoader = false;
        }, (err: any) => {
          this.providerLoader = false;
        });

      }

    }

  }

  selectVipLevel(vip_level: any) {    
    this.f.vip_level.setValue(vip_level);
  }

  getNumArr(length: number) {
    return Array.from({ length }).map((_, i) => i);
  }

  minValueValidator(minControl: AbstractControl): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if (!minControl || !minControl.value || !control.value) {
        return null;
      }
  
      const minValue = parseFloat(minControl.value);
      const value = parseFloat(control.value);
  
      if (!isNaN(minValue) && !isNaN(value) && value < minValue) {
        return { 'minValueOvershoot': true };
      }
  
      return null;
    };
  }



}
