import { Component, OnInit } from '@angular/core';
import { Currency } from 'src/app/models';
import { TenantService } from '../tenant.service';
import { PermissionService } from 'src/app/services/permission.service';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

declare const toastr: any;

@Component({
  templateUrl: './tenant-configurations.component.html',
  styleUrls: ['./tenant-configurations.component.scss']
})

export class TenantConfigurationsComponent implements OnInit {

  configurationsCurrencies: Currency[] = [];
  configurationsCasinoProvider: any = [];

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Tenant Configurations', path: '/tenants/credentials' }
  ];
  playerCommissionInterval: any;

  intervalForm: FormGroup | any;
  configurationsForm:FormGroup | any;
  submitted: { [key: string]: boolean } = {};
  googleSettings:any

  otpEnableValues = [
    {label: 'Bank Details', value: 'bankDetails'},
    {label: 'Change Password', value: 'changePassword'},
    {label: 'Update Profile', value: 'updateProfile'},
  ]

  constructor(
    public tenantService: TenantService,
    public permissionService: PermissionService,
    public formBuilder : FormBuilder) { 
      this.intervalForm = this.formBuilder.group({
        interval: new FormControl('daily')
      });

      this.configurationsForm = this.formBuilder.group({
        google_analytics_scripts_code: [''],
        chatbot_token: [''],
        google_tag_manager_code: [''],
        google_search_console_code: [''],
        google_recaptcha_keys: this.formBuilder.group({
          site_key: [''],
          secret_key: ['']
        }),
        otp_enable: this.formBuilder.array([]),
        max_attempts: ['',[Validators.min(1),Validators.max(10),Validators.pattern(/^-?\d+$/)]],
        attempt_expiry_time:['',[Validators.min(1),Validators.max(60),Validators.pattern(/^-?\d+$/)]],
        max_bank_account_limit: ['',[Validators.min(1),Validators.max(10),Validators.pattern(/^-?\d+$/)]],
      });

      this.addOtpEnableCheckboxes();
     }

  ngOnInit(): void {
    this.getTenant();
    this.getCasinoProviders();
    this.getTenantCommissionInterval();
  }

  getTenant() {
    this.tenantService.getTenantCurrencies().subscribe((res: any) => {
      this.configurationsCurrencies = res.record
    });
  }
  
  getCasinoProviders() {
    this.tenantService.getTenantThemeSetting().subscribe((res: any) => {
      this.configurationsCasinoProvider = res.record
      const {google_analytics_scripts_code , chatbot_token, google_tag_manager_code, google_search_console_code, google_recaptcha_keys,max_attempts,attempt_expiry_time,max_bank_account_limit} = res.record.setting
      this.googleSettings = {google_analytics_scripts_code , chatbot_token, google_tag_manager_code, google_search_console_code,max_attempts,attempt_expiry_time,max_bank_account_limit}
      this.googleSettings.google_recaptcha_keys = (google_recaptcha_keys) ? JSON.parse(google_recaptcha_keys) : null;

      this.configurationsForm.patchValue(this.googleSettings);

      const {otp_enable} = res.record.setting

      if(otp_enable){
        this.setCheckboxValues(otp_enable);
      }

      
    });
  }

  getTenantCommissionInterval(){
    this.tenantService.getTenantCommissionInterval().subscribe((res:any) => {
      this.playerCommissionInterval = res.record.player_commission_interval
      this.intervalForm.get('interval').setValue(this.playerCommissionInterval);
    })
  }

  updatePlayerCommissionInterval(interval:any){
    this.intervalForm.get('interval').setValue(interval);
    if(this.intervalForm.invalid){
      return
    }

    this.tenantService.updateTenantPlayerCommissionInterval(this.intervalForm.value).subscribe((res: any) => {
    });
  }

  onSubmit(controlName: string) {
    this.submitted[controlName] = true;
    
    if (!this.configurationsForm.get(controlName).valid) return;

    let data = { [controlName] : this.configurationsForm.get(controlName).value}

    this.tenantService.updateTenantGoogleSettings(data).subscribe((res: any) => {
      toastr.success(res.message || 'Tenant Google Setting updated successfully!');
      this.submitted[controlName] = false;
    })

  }

  isControlInvalid(controlName: string): boolean {
    return (
      this.configurationsForm.get(controlName).invalid &&
      this.configurationsForm.get(controlName).touched &&
      this.submitted[controlName]
    );
  }

  get otpEnable() {
    return this.configurationsForm.get('otp_enable') as FormArray;
  }

  addOtpEnableCheckboxes() {
    this.otpEnableValues.forEach(() => {
      this.otpEnable.push(this.formBuilder.control(false));
    });
  }

  setCheckboxValues(data: string) {
    const selectedOtpEnableValues = data.split(',');
    const formControls = this.otpEnable.controls;

    selectedOtpEnableValues.forEach(value => {
      const index = this.otpEnableValues.findIndex(item => item.value === value);
      if (index !== -1) {
        formControls[index].setValue(true);
      }
    });
  }

  submitOtpField() {
    const selectedOtpEnableValues = this.configurationsForm.value.otp_enable
      .map((checked:any, i:any) => checked ? this.otpEnableValues[i].value : null)
      .filter((value:any) => value !== null);

    let data = { 'otp_enable' : selectedOtpEnableValues.join(',')}

    this.tenantService.updateTenantGoogleSettings(data).subscribe((res: any) => {
      toastr.success(res.message || 'Tenant Otp Enable Setting updated successfully!');
    })
  }

}
