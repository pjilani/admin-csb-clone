import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TenantConfigurationsComponent } from './tenant-configurations.component';

describe('TenantConfigurationsComponent', () => {
  let component: TenantConfigurationsComponent;
  let fixture: ComponentFixture<TenantConfigurationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TenantConfigurationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TenantConfigurationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
