import { Component, OnInit } from '@angular/core';
import { Credentials, Tenant } from 'src/app/models';
import { AdminAuthService } from '../../admin/services/admin-auth.service';
import { TenantService } from '../tenant.service';
import Swal from 'sweetalert2';
import { PermissionService } from 'src/app/services/permission.service';
declare const $: any;
declare const toastr: any;
@Component({
  selector: 'app-tenant-payment-providers',
  templateUrl: './tenant-payment-providers.component.html',
  styleUrls: ['./tenant-payment-providers.component.scss']
})
export class TenantPaymentProvidersComponent implements OnInit {
  paymentProviders: any[] = [];
  remainingPaymentProvider: any[] = [];
  selectedPaymentProvider: any[] = [];
  disabledProviders:any;
  tenant!: Tenant;

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Tenant Credentials', path: '/tenants/credentials' },
  ];
  paymentGatewayCategoriesEnable: boolean = false;

  constructor(public tenantService: TenantService,
  public adminAuthService: AdminAuthService,
  public permissionService: PermissionService) { }

  ngOnInit(): void {

    this.adminAuthService.tenantData.subscribe((tenantData: any) => {
      if (tenantData) {
        this.tenant = tenantData;
        }
    })
    this.getSelectedProviders();
    this.getPaymentProviders();

    this.paymentGatewayCategoriesEnable = (localStorage.getItem('allowedModules')?.includes('paymentGatewayCategoriesEnable') ? true : false);
  }

  

  getPaymentProviders() {
    this.tenantService.getAdminTenantRemPaymentProvider().subscribe((res: any) => {
      this.remainingPaymentProvider = res.record;
        
      
    });
  }
 
 
  getSelectedProviders() {
    this.tenantService.getAdminTenantPaymentProvider().subscribe((res: any) => {
      this.selectedPaymentProvider = res.record.paymentProvider;
      this.disabledProviders = res.record.providers_disabled;
      let result = this.disabledProviders.map((i:any)=>Number(i));
      this.selectedPaymentProvider.map(provider=>{
        result.includes(provider.provider_id) == true ? provider.disabledByAdmin=false: provider.disabledByAdmin=true;
      })
    });
  }


  activeProviderStatus(data: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to Active ${data.provider_name}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Active it!'
    }).then((result) => {
      if (result.isConfirmed) {

        let providerData = {
          "id" : data.id,
          "status": true
        };

        this.tenantService.tenantAdminProviderStatus(providerData).subscribe((res: any) => {
          toastr.success(res.message || 'Payment Provider Activate successfully' );
          this.selectedPaymentProvider = this.selectedPaymentProvider.map((f: any) => {
            if(f.id == data.id) {
              f.active =  true;
            }
            return f;
          });
        });
        
      }
    });

  }

  deactiveProviderStatus(data: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to Deactive ${data.provider_name}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Deactivate it!'
    }).then((result) => {
      if (result.isConfirmed) {
        let providerData = {
          "id" : data.id,
          "status": false
        };
        this.tenantService.tenantAdminProviderStatus(providerData).subscribe((res: any) => {
          toastr.success(res.message || 'Payment Provider Deactivated successfully' );
          this.selectedPaymentProvider = this.selectedPaymentProvider.map((f: any) => {
            if(f.id == data.id) {
              f.active = false;
            }
            return f;
          });
        });
        
      }
    });
    
  }

  jsonParse(value:any){   
    if(value != "null"){
      return JSON.parse(value)
    }
    else{
      return [];
    }
  }


 

}
