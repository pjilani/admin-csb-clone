import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TenantPaymentProvidersComponent } from './tenant-payment-providers.component';

describe('TenantPaymentProvidersComponent', () => {
  let component: TenantPaymentProvidersComponent;
  let fixture: ComponentFixture<TenantPaymentProvidersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TenantPaymentProvidersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TenantPaymentProvidersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
