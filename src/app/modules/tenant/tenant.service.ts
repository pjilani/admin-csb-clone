import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Banner, Credentials } from 'src/app/models';
import { SuperAdminAuthService } from '../super-admin/services/super-admin-auth.service';

@Injectable({ providedIn: 'root' })

export class TenantService {

  public tenantDetials: any;

  public remCredentials: Credentials[] = [];
  public remainingPaymentProvider: any[] = [];
  public getAdminTenantRegistrationField: any[] = [];
  public remPaymentProvider: any[] = [];

  constructor(private http: HttpClient,
    private superAdminAuthService: SuperAdminAuthService) { }

  // ====================== Admin url ==============================
  getAdminTenant() {
    return this.http.get(`admin/tenants`).pipe(map((m: any) => {
      this.tenantDetials = m.record;
      return m;
    }));
  }


  updateTenantRegistrationField(data: any) {
    return this.http.post(`admin/tenants/registration-field`, data);
  }

  updateTenantPermission(data: any) {
    return this.http.post(`admin/tenants/update-permission`, data);
  }

  updateTenantPlayerCommissionInterval(data: any) {
    if (this.superAdminAuthService?.superAdminTokenValue?.length > 0) {
      return this.http.post(`super/admin/tenants/update-player-commission-interval`, data);
    } else {
      return this.http.post(`admin/tenants/update-player-commission-interval`, data);
    }
  }

  updateTenantGoogleSettings(data: any) {
    if (this.superAdminAuthService?.superAdminTokenValue?.length > 0) {
      return this.http.post(`super/admin/tenants/theme/setting/google/update`, data);
    } else {
      return this.http.post(`admin/tenants/theme/setting/google/update`, data);
    }
  }

  getAdminTenantRemCred() {
    return this.http.get(`admin/tenants/credentials`).pipe(map((m: any) => {
      this.remCredentials = m.record;
      return m;
    }));
  }


  getAdminTenantRemPaymentProvider() {
    return this.http.get(`admin/tenants/paymentProvider`).pipe(map((m: any) => {
      this.remainingPaymentProvider = m.record;
      return m;
    }));
  }
  getAdminTenantPaymentProvider() {
    return this.http.get(`admin/tenants/selectedPaymentProvider`).pipe(map((m: any) => {
      this.remainingPaymentProvider = m.record;
      return m;
    }));
  }

  getAdminTenantRegistrationFields() {
    return this.http.get(`admin/tenants/getregistrationFields`).pipe(map((m: any) => {
      this.getAdminTenantRegistrationField = m.record;
      return m;
    }));
  }

  tenantAdminProviderStatus(data: any) {
    return this.http.post(`admin/tenants/tenantPaymentProviderStatus`, data);
  }

  createAdminPaymentProvider(data: any) {
    return this.http.post(`admin/tenants/paymentProvider/create/new`, data);
  }

  updateAdminPaymentProvider(data: any) {
    return this.http.post(`admin/tenants/paymentProvider/edit/${data.id}`, data);
  }

  createAdminTenCred(credential: Credential) {
    return this.http.post(`admin/tenants/credentials`, credential);
  }

  updateAdminTenCred(credential: Credential) {
    return this.http.post(`admin/tenants/credentials/edit/${credential.id}`, credential);
  }

  getAdminLayouts() {
    return this.http.get(`admin/layout`);
  }

  getAdminLanguages() {
    return this.http.get(`admin/languages`);
  }

  updateAdminTheme(tenant: any) {
    return this.http.post(`admin/tenants/theme/update`, tenant);
  }

  getAdminBanners() {
    return this.http.get(`admin/tenants/banners`);
  }

  getAdminMenuSetting() {
    return this.http.get(`admin/tenants/menu/setting`);
  }

  deleteAdminBanner(banner: Banner) {
    return this.http.delete(`admin/tenants/banner/${banner.id}`);
  }

  createAdminBanner(banner: any) {
    return this.http.post(`admin/tenants/banner`, banner);
  }

  updateAdminBanner(banner: any) {
    return this.http.post(`admin/tenants/banner/update`, banner);
  }

  getTenantSportSettings(id: number) {
    if (this.superAdminAuthService?.superAdminTokenValue?.length > 0) {
      return this.http.get(`super/admin/tenants/sports/setting?id=${id}`);
    } else {
      return this.http.get(`admin/tenants/sport-setting?id=${id}`);
    }
  }

  updateTenantSportSettings(data: any) {
    if (this.superAdminAuthService?.superAdminTokenValue?.length > 0) {
      return this.http.put(`super/admin/tenants/sport-setting/update`, data);
    } else {
      return this.http.put(`admin/tenants/sport-setting/update`, data);
    }

  }

  // ====================== Super Admin url ==============================


  updateRegistrationField(data: any) {
    return this.http.post(`super/admin/tenants/registration-field`, data);
  }

  createTenants(tenant: any) {
    return this.http.post(`super/admin/tenants`, tenant);
  }

  getTenants(params: any) {
    return this.http.post(`super/admin/tenants/lists`, params);
  }

  getTenant(id: number) {
    return this.http.get(`super/admin/tenants/${id}`);
  }

  getLanguages() {
    return this.http.get(`super/admin/languages`);
  }

  updateTenants(id: number, tenant: any) {
    return this.http.post(`super/admin/tenants/${id}`, tenant);
  }

  getSuperAdminTenant(id: number) {
    return this.http.get(`super/admin/tenants/${id}`).pipe(map((m: any) => {
      this.tenantDetials = m.record;
      return m;
    }));
  }

  getLayouts() {
    return this.http.get(`super/admin/layout`);
  }

  getAssignedProviders() {
    return this.http.get(`super/admin/casino/providers`);
  }
  getSuperAdminTenantRemCred(id: number) {
    return this.http.get(`super/admin/tenants/credentials/${id}`).pipe(map((m: any) => {
      this.remCredentials = m.record;
      return m;
    }));
  }

  getSuperAdminTenantRemPaymentProvider(id: number) {
    return this.http.get(`super/admin/tenants/paymentProvider/${id}`).pipe(map((m: any) => {
      this.remPaymentProvider = m.record;
      return m;
    }));
  }

  paymentProviderById(id: any) {

    if (this.superAdminAuthService.superAdminTokenValue?.length > 0) {
      return this.http.get(`super/admin/tenants/paymentProvidersById/${id}`);

    } else {

      return this.http.get(`admin/tenants/paymentProvidersById/${id}`);

    }
  }

  tenantProviderStatus(data: any) {
    return this.http.post(`super/admin/tenants/tenantPaymentProviderStatus`, data);
  }




  getSuperAdminTenantMenuSetting(MenuSetting: any) {
    return this.http.post(`super/admin/tenants/menu/setting`, MenuSetting);
  }

  createSuperAdminTenCred(credential: Credential) {
    return this.http.post(`super/admin/tenants/credentials`, credential);
  }


  createSuperAdminPaymentProvider(data: any) {
    return this.http.post(`super/admin/tenants/paymentProvider/create/new`, data);
  }


  updateSuperAdminTenCred(credential: Credential) {
    return this.http.post(`super/admin/tenants/credentials/edit/${credential.id}`, credential);
  }
  updateSuperAdminPaymentProvider(data: any) {
    return this.http.post(`super/admin/tenants/paymentProvider/edit/${data.id}`, data);
  }


  getTenantsAll() {
    return this.http.get(`super/admin/tenants`);
  }

  getTenantsCredentialsList() {
    return this.http.get(`admin/tenants/credentialsList`);
  }

  getTenantCurrencies() {
    return this.http.get(`admin/tenants/currencies`);
  }

  getTenantThemeSetting() {
    return this.http.get(`admin/tenants/theme/setting`);
  }

  getTenantCommissionInterval() {
    return this.http.get(`admin/tenants/player-commission-interval`);
  }

  updateReportStatus(params: any) {
    return this.http.post(`super/admin/tenants/report/status`, params);
  }

  updateTenantBannerOrder(data: any) {
    return this.http.put(`admin/tenants/banner/update/order`, data);
  }
  updateTenantMenuOrder(data: any) {
    return this.http.put(`admin/tenants/menu/update/order`, data);
  }

  // updateTenantsConfigurations(data: any) {
  //   return this.http.post(`super/tenants`, data);
  // }

  // getTenantCredentials(id: number) {
  //   return this.http.get(`super/admin/tenants/credentials/${id}`);
  // }

  // createTenantCredentials(credentials: any) {
  //   return this.http.post(`super/admin/tenants/credentials`, credentials);
  // }

  // updateTenantCredentials(credentials: any) {
  //   return this.http.post(`super/tenants/credentials/edit/${credentials.id}`, credentials);
  // }

  // addAdmin(data: any) {
  //   return this.http.post(`super/admin/usersadmin/add`, data);
  // }

  // getAdmin(id: number) {
  //   return this.http.get(`super/admin/users/${id}`);
  // }

  // updateAdmin(data: any) {
  //   return this.http.post(`super/admin/usersadmin/edit/${data.id}`, data);
  // }

  // getSubAdmins(params: any) {
  //   return this.http.post(`super/admin/usersadmin/agent`, params);
  // }

  // getAdminPlayer(params: any) {
  //   return this.http.post(`super/admin/usersadmin/player`, params);
  // }

  changeAdminPassword(data: any) {
    return this.http.post(`admin/tenants/change-password`, data);
  }

  getSocialMedia(){
    return this.http.get(`admin/tenants/social-media`);
  }

  createSocialMedia(data:any){
    return this.http.post(`admin/tenants/social-media/create`, data);
  }

  updateSocialMedia(data:any){
    return this.http.post(`admin/tenants/social-media/update`, data);
  }

  activeSocialMediaStatus(id: number) {
    return this.http.post(`admin/tenants/social-media/active/${id}`, {});    
  }

  deactiveSocialMediaStatus(id: number) {
    return this.http.post(`admin/tenants/social-media/deactive/${id}`, {});    
  }

  getPaymentProviders(){
    return this.http.get(`admin/tenants/payment-providers`);
  }

  getFaqs(params:any){
    return this.http.post(`admin/tenants/faqs`,params);
  }

  getFaqCategories(params?:any){
    return this.http.post(`admin/tenants/faqs/categories`,params);
  }

  createFaq(data:any){
    return this.http.post(`admin/tenants/faq/create`, data);
  }

  updateFaq(data:any, id:any){
    return this.http.post(`admin/tenants/faq/update/${id}`, data);
  }


  getFaqById(id: number) {
    return this.http.get(`admin/tenants/faqs/${id}`);
  }

  updateFaqStatus(data:any){
    return this.http.post(`admin/tenants/faq/status`, data);
      
  }

  deleteFaq(data:any){
    return this.http.post(`admin/tenants/faq/delete`, data);
  }

  createFaqCategory(data:any){
    return this.http.post(`admin/tenants/faqs/categories/create`, data);
  }

  getFaqByCategoryId(id: number){
    return this.http.get(`admin/tenants/faqs/categories/${id}`);
  }

  updateFaqCategory(data:any){
    return this.http.post(`admin/tenants/faqs/categories/update`, data);
  }

  updateFaqCategoryStatus(data:any) {
    return this.http.post(`admin/tenants/faqs/categories/status`, data);
      
  }

  deleteFaqCategory(data:any){
    return this.http.post(`admin/tenants/faqs/categories/delete`, data);  
  }

}
