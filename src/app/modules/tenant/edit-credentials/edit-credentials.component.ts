import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Credentials, Tenant } from 'src/app/models';
import { truncateString } from 'src/app/services/utils.service';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
import { TenantService } from './../tenant.service';

declare const $: any;
declare const toastr: any;

@Component({
  selector: 'app-edit-credentials',
  templateUrl: './edit-credentials.component.html',
  styleUrls: ['./edit-credentials.component.scss']
})

export class EditCredentialsComponent implements OnInit {

  credentialId: number = 0;

  credential!: Credentials;

  tenant!: Tenant;
  title: string = 'Create';
  remainingCredentials: Credentials[] = [];

  credentialForm: FormGroup | any;
  submitted: boolean = false;
  credentialLoader: boolean = false;

  breadcrumbs: Array<any> = [];

  constructor(private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private tenantService: TenantService,
    public superAdminAuthService: SuperAdminAuthService
    ) {

     this.credentialId = this.route.snapshot.params['credentialId'];

    this.credentialForm = this.formBuilder.group({
      key: ['', [ Validators.required ]],
      keyvalue: ['', [ Validators.required ]],
      description: ['', [ Validators.required ]],
    });

    this.remainingCredentials = history.state.remcreds ? history.state.remcreds : [];
    this.tenant = history.state.tenant;
    this.credential = history.state.credential;

    if(this.credentialId > 0) {
      this.title = 'Edit';
    
      if(this.credential?.id && this.credential?.key) {
        this.credentialForm.patchValue({
          key: this.credential.key,
          keyvalue: this.credential.value,
          description: this.credential.description
        });

        this.remainingCredentials.push({ 
            id: 0,
            value: '',
            keyvalue: '',
            key: this.credential.key,
            created_at: '',
            updated_at: '',
            description: '',
            name: this.credential.key 
          });
              
          setTimeout(() => {
            $("#key").val(this.credential.key).trigger('change');
          }, 100);

      } else {
        this.redirectTo();
      }
    } else {
      this.changeKey(this.remainingCredentials[0].name);
    }


    this.credentialForm.get('key').valueChanges.subscribe((value:any)=> {
      if(value){
        if(value == 'PLAYER_COMMISSION_MAX_PERCENTAGE'){
          this.credentialForm.get('keyvalue').setValidators([Validators.required,Validators.min(10),Validators.max(100)])
        }
        else{
          this.credentialForm.get('keyvalue').setValidators([Validators.required])
        }
        this.credentialForm.get('keyvalue').updateValueAndValidity();
      }
    } )


    if(this.tenant?.id && this.remainingCredentials.length > 0) {
      if(this.superAdminAuthService.superAdminTokenValue?.length > 0) {
        this.breadcrumbs = [
          { title: 'Home', path: '/super-admin' },
          { title: 'Tenants', path: '/super-admin/tenants' },
          { title: truncateString(this.tenant.name, 10), path: `/super-admin/tenants/details/${this.tenant.id}` },
          { title: this.title, path: `/super-admin/tenants/details/${this.tenant.id}` }
        ]
      } else {
        this.breadcrumbs = [
          { title: 'Home', path: '/' },
          { title: 'Tenant Credentials', path: `/tenants/credentials` },
          { title: this.title, path: `/tenants/credentials/${this.tenant.id}` }
        ]
      }
    } else {
      this.redirectTo();
    }

  }

  ngOnInit(): void {  }

  redirectTo() {
    if(this.superAdminAuthService.superAdminTokenValue?.length > 0) {
      if(this.tenant?.id) {
        this.router.navigate([`/super-admin/tenants/details`, this.tenant.id]);
      } else {
        this.router.navigate([`/super-admin/tenants`]);
      }
    } else {
      this.router.navigate([`/tenants/credentials`]);
    }
  }

  changeKey(key: any) {
    this.f.key.setValue(key);
  }

  get f() {
    return this.credentialForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid

    // console.log(this.credentialForm.value);

    if (this.credentialForm.invalid) return;

    this.credentialLoader = true;
    const data = this.credentialForm.value;

    if(this.superAdminAuthService.superAdminTokenValue?.length > 0) {

      data.tenant_id = this.tenant.id;

      if(this.credentialId > 0) {
        data.id = this.credentialId;

        this.tenantService.updateSuperAdminTenCred(data)
        .subscribe((response: any) => {
          toastr.success(response?.message || 'Tenant Creadentials Updated Successfully!');
          this.credentialLoader = false;
        }, (err: any) => {
          this.credentialLoader = false;
        });

      } else {
        this.tenantService.createSuperAdminTenCred(data)
        .subscribe((response: any) => {
          toastr.success(response?.message || 'Tenant Creadentials Created Successfully!');
          this.router.navigate(['/super-admin/tenants/details', this.tenant.id]);
          this.credentialLoader = false;
        }, (err: any) => {
          this.credentialLoader = false;
        });

      }

    } else {

      if(this.credentialId > 0) {
        data.id = this.credentialId;

        this.tenantService.updateAdminTenCred(data)
        .subscribe((response: any) => {
          toastr.success(response?.message || 'Tenant Creadentials Updated Successfully!');
          this.credentialLoader = false;
        }, (err: any) => {
          this.credentialLoader = false;
        });

      } else {
        this.tenantService.createAdminTenCred(data)
        .subscribe((response: any) => {
          toastr.success(response?.message || 'Tenant Creadentials Created Successfully!');
          this.router.navigate(['/tenants/credentials']);
          this.credentialLoader = false;
        }, (err: any) => {
          this.credentialLoader = false;
        });

      }

    }

  }

}
