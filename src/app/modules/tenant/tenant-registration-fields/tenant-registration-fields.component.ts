import { Component, OnInit } from '@angular/core';
import { Credentials, Tenant } from 'src/app/models';
import { AdminAuthService } from '../../admin/services/admin-auth.service';
import { TenantService } from '../tenant.service';
import Swal from 'sweetalert2';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PermissionService } from 'src/app/services/permission.service';

declare const $: any;
declare const toastr: any;
@Component({
  selector: 'app-tenant-registration-fields',
  templateUrl: './tenant-registration-fields.component.html',
  styleUrls: ['./tenant-registration-fields.component.scss']
})
export class TenantRegistrationFieldComponent implements OnInit {
  tenantRegistrationFieldsconfiguration: any[] = [];
  submitted: boolean = false;
  adminLoader: boolean = false;
  tenantRegistrationFields: FormGroup | any;
  tenant!: Tenant;

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Dashboard', path: '/' },
  ];

  constructor(public tenantService: TenantService,  private formBuilder: FormBuilder,
  public adminAuthService: AdminAuthService,
  public permissionService: PermissionService) {
  
    this.tenantRegistrationFields = this.formBuilder.group({
      phaseExecutions: this.formBuilder.group({
        PRE: this.formBuilder.array([])
      }),
    });

    this.adminAuthService.tenantData.subscribe((tenantData: any) => {
      if (tenantData) {
        this.tenant = tenantData;
      }
    })
    

   }

  ngOnInit(): void {
    this.getTenantFormSetting();

    this.adminAuthService.adminPermissions.subscribe((res=>{
      if(res && Object.keys(res).length){
        if(!this.permissionService.checkPermission('tenant_registration_fields','U')){
          this.tenantRegistrationFields.disable();
        }
      }
    }))
  }

  getTenantFormSetting() {
      this.tenantService.getAdminTenantRegistrationFields().subscribe((res: any) => {
        this.tenantRegistrationFieldsconfiguration = res.record.tenantRegistrationFields;
       for (let x in this.tenantRegistrationFieldsconfiguration) { 
         this.configuration_keys.push(this.newProviderKeys(x,this.tenantRegistrationFieldsconfiguration[x],res.record.setting.user_login_type));
       }
     });
   
  }

  get configuration_keys() {
    const control = <FormArray>(<FormGroup>this.tenantRegistrationFields.get('phaseExecutions')).get('PRE');
    return control;
  }

  get phaseExecutions(): FormArray {
    return this.tenantRegistrationFields.get('phaseExecutions').get('PRE') as FormArray;
  }

  getProviderFormArr(index: any): FormGroup {
    const formGroup = this.phaseExecutions.controls[index] as FormGroup;
    return formGroup;
  }

  newProviderKeys(key:any = '',val: any = '',loginType:any = 0): any {
    let disable = false;
    if(key === 'username' || key === 'password' || key === 'currency'){
        disable = true
    }
    if(loginType ==  1 && key == 'phone'){
      // Mobile Login Enable
      val = 2;
      disable = true;
    }else if(loginType ==  0 && key == 'email'){
      // Email Base Login
      val = 2;
      disable = true;
    }
    return this.formBuilder.group({
      [key]: [{value:(val !== '' ? val : ''),disabled:disable}, [Validators.required]],
    })
  }


  getControlByName(index: any): any {
    const formGroup = this.phaseExecutions.controls[index] as FormGroup;
    let finalObj = Object.keys(formGroup.value);
    return finalObj[0];
  }



  onSubmit() {

    this.submitted = true;

    if (this.tenantRegistrationFields.invalid) {
      return;
    }
  
    this.adminLoader = true;

    let data = this.tenantRegistrationFields.getRawValue();
    data.tenantId = this.tenant.id;
    if (!data.phaseExecutions.PRE.length) {
      toastr.error('Please Select All the field action');
      this.adminLoader = false;
      return
    }
    
    this.tenantService.updateTenantRegistrationField(data)
    .subscribe((response: any) => {
      toastr.success(response?.message || 'Tenant Registration Field updated!');
        this.adminLoader = false; 
    }, (err: any) => {
      this.adminLoader = false; 
    });


    
  }


}
