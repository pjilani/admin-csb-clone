import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TenantRegistrationFieldComponent } from './tenant-registration-fields.component';

describe('TenantPaymentProvidersComponent', () => {
  let component: TenantRegistrationFieldComponent;
  let fixture: ComponentFixture<TenantRegistrationFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TenantRegistrationFieldComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TenantRegistrationFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
