import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Currency, Language, Layout, Tenant, Theme } from 'src/app/models';
import { Constants } from 'src/app/shared/constants';
import { CustomValidators } from 'src/app/shared/validators';
import { ThemesService } from '../../super-admin/super-admin-modules/themes/themes.service';
import { TenantService } from '../tenant.service';
import { environment } from 'src/environments/environment';
import { PermissionService } from 'src/app/services/permission.service';
import { AdminAuthService } from '../../admin/services/admin-auth.service';

declare const $: any;
declare const toastr: any;

@Component({
  selector: 'app-theme-setting',
  templateUrl: './theme-setting.component.html',
  styleUrls: ['./theme-setting.component.scss']
})

export class ThemeSettingComponent implements OnInit {
  @ViewChild('file') logo_file!: ElementRef;
  @ViewChild('fabFile') logo_fab_icon_file!: ElementRef;
  @ViewChild('signupPopupFile') logo_signup_popup_file!: ElementRef;

  fabImgURL: any;
  signUpPopURL: any;
  imgURL: any;
  currencies: Currency[] = [];
  languages: Language[] = [];
  layouts: Layout[] = [];
  themes: Theme[] = [];
  constTheme:any;
  // tenant!: Tenant;
  tenantRecords: any;

  tenantForm: FormGroup | any;
  submitted: boolean = false;
  tenantLoader: boolean = false;
  selectLogo!: File;
  selectFabIcon!: File;
  selectSignupImg!: File;
  // selectLogoBool: boolean = false;

  theme: any = { ...Constants.INIT_THEME };
  selectedLayout: any = Constants.defaultLayout ;

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Theme Settings', path: `/tenants/theme-settings` }
  ];

  constructor(
    private formBuilder: FormBuilder,
    private tenantService: TenantService,
    private themesService: ThemesService,
    public permissionService: PermissionService,
    public adminAuthService: AdminAuthService,
    ) {

    this.tenantForm = this.formBuilder.group({
      language: ['', [ Validators.required ]],
      layout: ['', [ Validators.required ]],
      whatsappNumber: ['',[Validators.min(1000000000), Validators.max(999999999999)]],
      theme: ['', [ Validators.required ]],
      logo: ['', [ CustomValidators.requiredFileType(CustomValidators.imgType)] ],
      fabLogo: ['', [ CustomValidators.requiredFileType(CustomValidators.imgType)] ],
      signupPopup: ['', [ CustomValidators.requiredFileType(CustomValidators.imgType)] ]

    });


      this.tenantService.getTenantThemeSetting().subscribe((res: any) => {
        // this.tenant = res.record.tenants;
        this.tenantRecords = res.record;

        const theme = JSON.parse(res.record.setting.theme);
        const language = res.record.setting.language_id;
        const layout = res.record.setting.layout_id;
        const whatsappNumber = res.record.setting.whatsapp_number;

        this.tenantForm.patchValue({
          language: language,
          layout: layout,
          whatsappNumber: whatsappNumber,
          theme: theme.name
        });
        this.imgURL = res.record.setting.logo_url;
        this.fabImgURL = res.record.setting.fab_icon_url;
        this.signUpPopURL = res.record.setting.signup_popup_image_url;
        this.theme = theme;
        this.constTheme = theme;

        setTimeout(() => {
          $("#theme").val(this.theme.name).trigger('change');
          $("#layout").val(layout).trigger('change');
          $("#language").val(language).trigger('change');
        }, 100);
        
      });

  }

  ngOnInit(): void {

    this.tenantService.getAdminLanguages().subscribe((res: any) => {
      this.languages = res.record;
      
      if(this.tenantRecords?.setting?.language_id) {
        setTimeout(() => {
          $("#language").val(this.tenantRecords.setting.language_id).trigger('change');
        }, 100);
      }

    });

    this.tenantService.getAdminLayouts().subscribe((res: any) => {
      this.layouts = res.record;

      if(this.tenantRecords?.setting?.layout_id) {
        setTimeout(() => {
          $("#layout").val(this.tenantRecords.setting.layout_id).trigger('change');
        }, 100);
      }

    });

    this.themesService.getAdminThemes().subscribe((res: any) => {
      this.themes = res.record;

      setTimeout(() => {
        $("#theme").val(this.theme.name).trigger('change');
      }, 100);

    });

    this.adminAuthService.adminPermissions.subscribe((res=>{
      if(res && Object.keys(res).length){
        if(!this.permissionService.checkPermission('tenant_theme_settings','U')){     
          this.tenantForm.disable();
          setTimeout(() => {
            $("#theme").prop('disabled',true).trigger('change');
            $("#layout").prop('disabled',true).trigger('change');
          },300)
        }
      }
    }))

  }

  numberOnly(event:any): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  ngAfterViewInit(): void {  }

  selectCurrencies(currencies: any) {
    this.f.currencies.setValue(currencies);
  }

  selectLanguage(language: any) {
    this.f.language.setValue(language);
  }

  selectLayout(layout: any) {
    this.selectedLayout = layout;
    this.f.layout.setValue(layout);
  }

  selectTheme(theme: any) {
    let findTheme: any = {};
    if(theme != 'Custom') {
      const data: any = this.themes.find(f => f.name === theme);
      findTheme = JSON.parse(data.options);
    } else {
      findTheme = (this.constTheme.name == 'Custom' ? this.constTheme : { ...Constants.INIT_THEME });
    }

    this.f.theme.setValue(theme);
    findTheme.name = theme;
    this.theme = findTheme;
  }

  get f() {
    return this.tenantForm.controls;
  }

  selectImage(files: any,flag:any = '') {
    if (files.length === 0)
    return;
    const extension = files[0].name.split('.')[1].toLowerCase();
    const imgt = CustomValidators.imgType.includes(extension);

    if(imgt) {
      // this.selectLogo = files[0];
        const reader = new FileReader();
        // this.imagePath = files;
        reader.readAsDataURL(files[0]); 
        reader.onload = (_event) => { 
          if(flag == 'logo'){
            this.imgURL = reader.result; 
            this.selectLogo = files[0];
          } else if(flag == 'fab') {
            this.fabImgURL = reader.result; 
            this.selectFabIcon = files[0];
          }else{
            this.signUpPopURL = reader.result; 
            this.selectSignupImg = files[0];
          }
        }
        // this.selectLogoBool = true;
      } else {
        if(flag == 'logo') this.imgURL = ''; else this.fabImgURL = ''; 

      }

  }

  changeTheme(theme: any) {
    this.theme = theme;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid

    this.theme.name = this.f.theme.value;

    // console.log(this.tenantForm.value);
    // console.log('theme ', this.theme);

    if (this.tenantForm.invalid) return;
    
    this.tenantLoader = true;

    const data = this.tenantForm.value;
    const fd = new FormData();
    
    delete data.logo;
    delete data.theme;

    if(this.selectLogo && this.selectLogo.name) {
      fd.append('logo', this.selectLogo, this.selectLogo.name);
    }

    if(this.selectFabIcon && this.selectFabIcon.name) {
      fd.append('fab_icon_url', this.selectFabIcon, this.selectFabIcon.name);
    }
   
    if(this.selectSignupImg && this.selectSignupImg.name) {
      fd.append('signup_popup_image_url', this.selectSignupImg, this.selectSignupImg.name);
    }

    for(let key in data) {
      fd.append(key, data[key]);
    }

    fd.append('theme', JSON.stringify(this.theme));
    
      this.tenantService.updateAdminTheme(fd)
      .subscribe((response: any) => {
        toastr.success(response?.message || 'Tenant Theme Updated Successfully!');
        this.tenantLoader = false; 
      }, (err: any) => {
        this.tenantLoader = false; 
      });
      
  }

}
