import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditFaqCategoriesComponent } from './edit-faq-categories.component';

describe('EditFaqCategoriesComponent', () => {
  let component: EditFaqCategoriesComponent;
  let fixture: ComponentFixture<EditFaqCategoriesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditFaqCategoriesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditFaqCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
