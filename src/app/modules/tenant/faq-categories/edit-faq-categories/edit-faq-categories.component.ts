import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Currency, Language, Layout, Admin, Theme } from 'src/app/models';
import { Constants } from 'src/app/shared/constants';
import { CustomValidators } from 'src/app/shared/validators';
import { TenantService } from '../../tenant.service';
import { PermissionService } from 'src/app/services/permission.service';
import Swal from 'sweetalert2';
declare const $: any;
declare const toastr: any;

@Component({
  selector: 'app-edit-faq-categories',
  templateUrl: './edit-faq-categories.component.html',
  styleUrls: ['./edit-faq-categories.component.scss']
})
export class EditFaqCategoriesComponent implements OnInit {

  id: number = 0;
  answer:any = [];
  faqCategoryForm: FormGroup | any;
  faqCategory:any = [];;
  submitted: boolean = false;
  faqLoader: boolean = false;
  imgURL: any;
  isEdit:boolean = false;
  selectLogo!: File;

 

  constructor(private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private tenantService: TenantService,
    public permissionService : PermissionService
  ) {
    
    this.id = this.route.snapshot.params['id'];
    this.faqCategoryForm = this.formBuilder.group({
      name: ['', [ Validators.required, Validators.pattern('^[a-zA-Z_ ]+$')]],
      image: ['', [ Validators.required ]],
      slug: ['', [ Validators.required ]],
      active: [false],
      
    });

    let title: string = 'Create';
    
    if(this.id > 0) {
      title = "Edit";
      this.isEdit = true
    }

    this.breadcrumbs.push({ title, path: `/cms/${this.id}` });

  }


  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'FAQ categories', path: '/faq/categories' }
  ];

  ngOnInit(): void { 
    if(this.id > 0) {
      this.getFaqCategoryById(this.id);
    }
  }


  get f() {
    return this.faqCategoryForm.controls;
  }
 

  getFaqCategoryById(id:any) {
    this.tenantService.getFaqByCategoryId(id).subscribe((res: any) => {
      this.faqCategory = res.record[0];

      
      // console.log(this.content);
      // this.content.forEach((data: { value: string; type: any; }) => {
      //   (this.cmsForm.get("content") as FormGroup).addControl(
      //     data.type,
      //     new FormControl(data.value, Validators.required)
      //   );
        
      // });
      this.imgURL = this.faqCategory.image;
      this.faqCategoryForm.patchValue({
        name: this.faqCategory.name,
        slug: this.faqCategory.slug,
        active: this.faqCategory.active,
        id: this.faqCategory.id,
      });
      this.faqCategoryForm.controls["image"].clearValidators();
      this.faqCategoryForm.controls["image"].updateValueAndValidity();
      
    });
  }

  // userWarning(event:any){
  //   let pMessage = '';
  //   if(event.target.checked){
  //     pMessage = `You want to Enable it!, If you Enable this flag, if any of flag is already active, then it will be disabled. And this page is visible to all the player's`;
  //   }else{
  //     pMessage = "You want to Disable it! If you Disable this flag, No page will be displayed on the player's side registration.";
  //   }
  //   Swal.fire({
  //     title: 'Are you sure?',
  //     text: pMessage,
  //     icon: 'warning',
  //     showCancelButton: true,
  //     confirmButtonColor: '#3085d6',
  //     cancelButtonColor: '#d33',
  //     confirmButtonText: `Yes, `+ (event.target.checked ? 'Activate ' : 'Deactivate') +` it!`
  //   }).then((result) => {
  //     if (result.isConfirmed) {

  //       this.faqForm.patchValue({
  //         enable_flag: (event.target.checked ? true : false),
  //       });
        
  //     }else{
  //       this.faqForm.patchValue({
  //         enable_flag: (event.target.checked ? false : true),
  //       });
  //     }

  //   });
  // }

  removeSpace(event:any){
    var k;  
    k = event.charCode;
    return(((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 45 || k== 95) && event.keyCode != 32 ? event:event.preventDefault());

  }
 
  selectImage(files: any) {
    this.imgURL = '';
    if (files.length === 0)
      return;

    if ((files[0].size / 1000000) > 5) {

      this.f.image.setErrors({ 'required': 'File is required' });
      toastr.error( 'Max file size limit reached');
      // toastr.error(`Max file size is 5MB`);
      return;
    }

    const extension = files[0].name.split('.')[1].toLowerCase();
    let check = CustomValidators.imgType.includes(extension);

    if (check) {

      const reader = new FileReader();
      reader.readAsDataURL(files[0]);
      const img = new Image();
      img.src = window.URL.createObjectURL( files[0] );
      reader.onload = (_event) => {
      //   const width = img.naturalWidth;
      //    const height = img.naturalHeight;
      //    console.log(width, height);
      //    if( width > 30 || height > 30 ) {
      //       toastr.error("Icon should be less then 30 x 30 size");
      //     this.f.logo.setErrors({ 'dimension': 'Icon should be less then 30 x 30 size' });
      //     this.imgURL = '';
      //  }

        this.imgURL = reader.result;
      }
      this.selectLogo = files[0];
    } else {
      this.imgURL = '';
      toastr.error( 'Image extension should be svg or png');
      // toastr.error(`Please select ${this.f.banner_type.value}`);
    }

  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.faqCategoryForm.invalid) return;
    if(this.faqCategoryForm && this.id > 0) {
      const data = this.faqCategoryForm.value;
      delete data.image;
      const fd = new FormData();
      fd.append('id', `${this.id}`);

      if (this.selectLogo && this.selectLogo.name) {
        fd.append('image', this.selectLogo, this.selectLogo.name);
      }
  
      for (let key in data) {
        fd.append(key, data[key]);
      }
      this.faqLoader = true;
    this.tenantService.updateFaqCategory(fd)
      .subscribe((res: any) => {
        toastr.success(res.message || 'Page updated successfully.');
        
        this.faqLoader = false; 
        this.router.navigate(['/tenants/faq/categories']);
        // this.resetSocialForm();
        // this.getSocialMedia();
        this.imgURL = '';
          
      }, (err: any) => {
        this.faqLoader = false; 
      });

    } else {

      const data = this.faqCategoryForm.value;
      const fd = new FormData();
      delete data.image;
      // fd.append('top_menu_id', `${this.topPageId}`);
      if (this.selectLogo && this.selectLogo.name) {
        fd.append('image', this.selectLogo, this.selectLogo.name);
      }
  
      for (let key in data) {
        fd.append(key, data[key]);
      }
      this.faqLoader = true;

      this.tenantService.createFaqCategory(fd)
      .subscribe((res: any) => {

        toastr.success(res.message || 'Page created successfully.');
        this.faqLoader = false;
        this.router.navigate(['/tenants/faq/categories']);
        // this.resetSocialForm();
        // this.getSocialMedia();
        
        // if(res?.record?.id) {
        //     this.pages.push(res.record);
        // } else {
          // this.getPages(this.topPageId);
          this.imgURL = '';
        // }
        
      }, (err: any) => {
        this.faqLoader = false; 
      });
      
    }
  }
}
