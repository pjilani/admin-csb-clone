import { Component, OnInit } from '@angular/core';
import { PageSizes } from 'src/app/shared/constants';
import { TenantService } from '../tenant.service';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
declare const toastr: any;
import Swal from 'sweetalert2';
import { PermissionService } from 'src/app/services/permission.service';
declare const $: any;
@Component({
  selector: 'app-faq-categories',
  templateUrl: './faq-categories.component.html',
  styleUrls: ['./faq-categories.component.scss']
})
export class FaqCategoriesComponent implements OnInit {

  total: number = 0;
  faqCategories: any[] = [];
  pageSizes = PageSizes;

  bankDetails: any;
  isLoader:boolean = false;
  format: string = "YYYY-MM-DD HH:mm";
  faqP: number = 1;

  faqParams: any = {
    size: 10,
    page: 1,
    search: '',
    date_range: {},
  };

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Faqs', path: '/tenant/faqs' },
  ];

  constructor(public superAdminAuthService: SuperAdminAuthService, public permissionService : PermissionService,public tenantService:TenantService ) { }

  ngOnInit(): void {
    this.getList();
  }

  faqResetFilter() {
    this.faqP =1;
    this.faqParams = {
      size: 10,
      page: 1,
      search: '',
      date_range: {}
    };

    this.getList();
    $('#time_period').val('');
  }

  ngAfterViewInit(): void {

    $('#time_period').val('');
  }

  filterFaqs() {
    this.faqP = 1;
    this.faqParams = { ...this.faqParams, page: this.faqP };
    this.getList();
  }

  setTimePeriod(date_range:any){
    this.faqP = 1
    this.faqParams = { ...this.faqParams, date_range, page:this.faqP}
  }



  getList() {
    this.tenantService.getFaqCategories(this.faqParams).subscribe((res: any) => {
      this.faqCategories = res.record.data;
      this.total = res.record.total;
    });
  }

  faqPageChanged(page: number) {
    this.faqParams = { ...this.faqParams, page };
    this.getList();
  }

  updateFaqCategoryStatus(id: any, status: boolean) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to `+ (status == true ? 'Activate ' : 'Deactivate') +` it!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, `+ (status == true ? 'Activate ' : 'Deactivate') +` it!`
    }).then((result) => {
      if (result.isConfirmed) {

        this.tenantService.updateFaqCategoryStatus({ id: id, status :status }).subscribe((res: any) => {
          toastr.success(res.message || 'Faq updated successfully' );
          this.getList();
          ;
        });
        
      }

    });
  }

  deleteFaqCategory(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to delete category along with its Faqs!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, delete it!`
    }).then((result) => {
      if (result.isConfirmed) {

        this.tenantService.deleteFaqCategory({ id: id }).subscribe((res: any) => {
          toastr.success(res.message || 'Faq deleted successfully' );
          this.getList();
          ;
        });
        
      }

    });
  }

}
