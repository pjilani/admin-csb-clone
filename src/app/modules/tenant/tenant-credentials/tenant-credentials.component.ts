import { Component, OnInit } from '@angular/core';
import { Credentials, Tenant } from 'src/app/models';
import { AdminAuthService } from '../../admin/services/admin-auth.service';
import { TenantService } from '../tenant.service';
import { PermissionService } from 'src/app/services/permission.service';

@Component({
  templateUrl: './tenant-credentials.component.html',
  styleUrls: ['./tenant-credentials.component.scss']
})

export class TenantCredentialsComponent implements OnInit {
  
  remainingCredentials: any[] = [];
  credentials: Credentials[] = [];
  tenant!: Tenant;

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Tenant Credentials', path: '/tenants/credentials' },
  ];

  constructor(public tenantService: TenantService,
  public adminAuthService: AdminAuthService,
  public permissionService: PermissionService) { }

  ngOnInit(): void {

    this.adminAuthService.tenantData.subscribe((tenantData: any) => {
      if (tenantData) {
        this.tenant = tenantData;
        this.tenant.id = tenantData.id;
      }
    })

    this.getTenantsCredentialsList();
    this.getRemTenCred();
  }

  getTenantsCredentialsList() {
    this.tenantService.getTenantsCredentialsList().subscribe((res: any) => {
      this.credentials = res.record.credentials
    });
  }

  getRemTenCred() {
    this.tenantService.getAdminTenantRemCred().subscribe();
    this.tenantService.getAdminTenantRemCred().subscribe((res: any) => {
      this.remainingCredentials = res.record;
    });
    
  }

}
