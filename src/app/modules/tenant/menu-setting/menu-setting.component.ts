import { AfterViewInit, Component, OnInit } from '@angular/core';
import { TenantService } from '../tenant.service';
import { MenuTenantSetting } from 'src/app/models';
import Swal from 'sweetalert2';
import { PermissionService } from 'src/app/services/permission.service';

declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './menu-setting.component.html',
  styleUrls: ['./menu-setting.component.scss']
})

export class MenuSettingComponent implements OnInit, AfterViewInit {

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Menu Setting', path: '/tenants/menu-setting' }
  ];

  menuList:MenuTenantSetting[] = [];
  constructor(
    public tenantService: TenantService,
    public permissionService: PermissionService) {  }
    
  ngOnInit(): void {
    this.getMenu();
  }
  
  ngAfterViewInit(): void {
  // jQuery UI sortable for the todo list
  if(!this.isTouchDevice && this.permissionService.checkPermission('tenant_menu_setting','U')){
    const that = this;
    
    $('table tbody').sortable({
      helper: '.handle',
      zIndex: 999999,
      // items : "> [data-order]",
      update : function() {

        const ids = $(this).children().get().map(function (el: any) {
          return el.id;
        });

        that.updateOrder(ids);
        
      }
    }).disableSelection();
  }
  }

  updateOrder(idsArr: Array<string>) {
    this.tenantService.updateTenantMenuOrder({ orders: idsArr }).subscribe((res: any) => {
      toastr.success(res.message || 'Menu orders updated successfully.');
    });
  }

  getMenu() {

    this.tenantService.getAdminMenuSetting().subscribe((res: any) => {
      this.menuList = res.record;
    });
  }

  get isTouchDevice() {
    try{ document.createEvent("TouchEvent"); return true; }
    catch(e){ return false; }
  }
}

