import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BannerComponent } from './menu-setting.component';

describe('MenuSettingComponent', () => {
  let component: MenuSettingComponent;
  let fixture: ComponentFixture<MenuSettingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BannerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
