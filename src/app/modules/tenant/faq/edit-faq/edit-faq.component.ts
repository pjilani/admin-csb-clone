import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Currency, Language, Layout, Admin, Theme } from 'src/app/models';
import { Constants } from 'src/app/shared/constants';
import { CustomValidators } from 'src/app/shared/validators';
import { TenantService } from '../../tenant.service';
import { PermissionService } from 'src/app/services/permission.service';
import Swal from 'sweetalert2';
declare const $: any;
declare const toastr: any;

@Component({
  selector: 'app-edit-faq',
  templateUrl: './edit-faq.component.html',
  styleUrls: ['./edit-faq.component.scss']
})
export class EditFaqComponent implements OnInit {

  id: number = 0;
  faq:any = [];
  answer:any = [];
  faqForm: FormGroup | any;
  faqCategories:any;
  faqLoader: boolean = false;
  submitted: boolean = false;

 

  constructor(private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private tenantService: TenantService,
    public permissionService : PermissionService
  ) {
    
    this.id = this.route.snapshot.params['id'];
    this.faqForm = this.formBuilder.group({
      question: ['', [ Validators.required ]],
      answer: this.formBuilder.group({}),
      active: [false],
      featured: [false],
      category_id: ['', [ Validators.required ]],
      
    });

    let title: string = 'Create';
    
    if(this.id > 0) {
      title = "Edit";
    }

    this.breadcrumbs.push({ title, path: `/cms/${this.id}` });

  }


  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'FAQ', path: '/faq' }
  ];

  ngOnInit(): void { 
    this.getCategoryList();
    if(this.id > 0) {
      this.getFaqById(this.id);
    }else{
      this.answer = [
        {
          "type" : "EN",
          "value" : ""
        }
      ]
      this.answer.forEach((data: { value: string; type: any; }) => {
        (this.faqForm.get("answer") as FormGroup).addControl(
          data.type,
          new FormControl(data.value, Validators.required)
        );
        
      });
    } 
  }


  get f() {
    return this.faqForm.controls;
  }
 

  getFaqById(id:any) {
    this.tenantService.getFaqById(id).subscribe((res: any) => {
      this.faq = res.record[0];
      this.answer = JSON.parse(this.faq.answer);
      this.answer = Object.keys(this.answer).map(key => ({type: key, value: this.answer[key]}));;
      
      
      this.answer.forEach((data: { value: string; type: any; }) => {
        (this.faqForm.get("answer") as FormGroup).addControl(
          data.type,
          new FormControl(data.value, Validators.required)
        );
        
      });
      
      // console.log(this.content);
      // this.content.forEach((data: { value: string; type: any; }) => {
      //   (this.cmsForm.get("content") as FormGroup).addControl(
      //     data.type,
      //     new FormControl(data.value, Validators.required)
      //   );
        
      // });

      this.faqForm.patchValue({
        question: this.faq.question,
        active: this.faq.active,
        featured: this.faq.featured,
        id: this.faq.id,
        category_id: this.faq.category_id
      });

      setTimeout(() => {

        $("#category").val(this.faq.category_id).trigger('change');
       
      }, 100);
      
    });
  }

  userWarning(event:any){
    let pMessage = '';
    if(event.target.checked){
      pMessage = `You want to Enable it!, If you Enable this flag, if any of flag is already active, then it will be disabled. And this page is visible to all the player's`;
    }else{
      pMessage = "You want to Disable it! If you Disable this flag, No page will be displayed on the player's side registration.";
    }
    Swal.fire({
      title: 'Are you sure?',
      text: pMessage,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, `+ (event.target.checked ? 'Activate ' : 'Deactivate') +` it!`
    }).then((result) => {
      if (result.isConfirmed) {

        this.faqForm.patchValue({
          enable_flag: (event.target.checked ? true : false),
        });
        
      }else{
        this.faqForm.patchValue({
          enable_flag: (event.target.checked ? false : true),
        });
      }

    });
  }

  removeSpace(event:any){
    var k;  
    k = event.charCode;
    return(((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 45 || k== 95) && event.keyCode != 32 ? event:event.preventDefault());

  }
 
  // onSubmit() {
  //   this.submitted = true;

  //   if (this.faqForm.invalid){
  //     return;
  //   } 
  //   this.cmsLoader = true;
    
  //   if(this.id > 0) {
  //   const data = this.faqForm.value;
  //     this.tenantService.createFaq(data)
  //     .subscribe((response: any) => {
  //         toastr.success(response?.message || 'CMS Page Updated Successfully!');
  //       }else{
  //         toastr.success(response?.message || 'CMS Page Created Successfully!');
  //       }
  //       this.router.navigate(['/tenants/faqs']);
  //       this.cmsLoader = false; 
  //     }, (err: any) => {
  //       this.cmsLoader = false; 
  //     });
  
  // }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.faqForm.invalid) return;

    if( this.id > 0) {
      const data = this.faqForm.value;
    this.tenantService.updateFaq(data,this.id)
      .subscribe((res: any) => {
        toastr.success(res.message || 'Faq updated successfully.');
        this.router.navigate(['/tenants/faqs']);

        // this.resetFaqForm();
          
      }, (err: any) => {
        this.faqLoader = false; 
      });

    } else {

      const data = this.faqForm.value;
      // fd.append('top_menu_id', `${this.topPageId}`);

      this.faqLoader = true;

      this.tenantService.createFaq(data)
      .subscribe((res: any) => {

        toastr.success(res.message || 'Faq created successfully.');
        this.router.navigate(['/tenants/faqs']);
        // this.resetFaqForm();
        
        // if(res?.record?.id) {
        //     this.pages.push(res.record);
        // } else {
          // this.getPages(this.topPageId);
        // }
        
      }, (err: any) => {
        this.faqLoader = false; 
      });
      
    }
  }

  getCategoryList() {
    this.tenantService.getFaqCategories().subscribe((res: any) => {
      this.faqCategories = res.record.data;
    });
  }

  changeCategory(category: any) {
    this.f.category_id.setValue(category);
  }
}
