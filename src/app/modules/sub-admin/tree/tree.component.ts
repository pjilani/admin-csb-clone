import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { AdminAuthService } from '../../admin/services/admin-auth.service';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
import { generateApiUrl } from 'src/app/services/utils.service';

declare const $: any;

@Component({
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.scss']
})

export class TreeComponent implements OnInit, AfterViewInit{

  adminId: number = 0;
  tenantId: number = 0;

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Agents', path: '/agents' },
    { title: 'Agent Tree', path: '/agents/tree/1/1' },
  ];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private superAdminAuthService: SuperAdminAuthService,
      private adminAuthService: AdminAuthService) {
      this.adminId = this.route.snapshot.params['adminId'];
      this.tenantId = this.route.snapshot.params['tenantId'];
    }

  ngAfterViewInit(): void {

    const that = this;

    let url = generateApiUrl()+`admin/agent/tree`;
    let Authorization = 'Bearer ' + this.adminAuthService.adminTokenValue;
    
    if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      url = generateApiUrl()+`super/admin/agent/tree`;
      Authorization = 'Bearer ' + this.superAdminAuthService.superAdminTokenValue;  
    }

    $('#agent_tree').jstree({
      core : {
        animation : 0,
        check_callback : true,
        themes : { "stripes" : false },
        data : {
          headers: { Authorization },
          url,
          data : function (node: any) {
            return { id : that.adminId, tenant_id: that.tenantId };
          }
        }
      },
      types: {
        default : {
          icon : "fa fa-plus"
        }
      },
      contextmenu: {
        items: {
          seePlayers: {
            label: "See Agent",
            action: function (node: any) {
              const id = node.reference[0].parentNode.id;
              window.location.href = `/agents/details/${that.tenantId}/${id}`;
            }
          }
        }
      },
      plugins: ['themes', 'types', 'contextmenu']
    })
    
    $('#agent_tree').on('loaded.jstree', function() {
      if($('#agent_tree li').length == 0){
        $('#agent_tree').html('<h4>No subagents present for this agent</h4>')
        return;
      }
      $('#agent_tree').jstree('select_node', 'ul > li:first');
    })
    
    $('#agent_tree').on("select_node.jstree", function(e: any, data: any) {

      const id = data.node.original.id;
      that.adminId = id ? id : that.adminId;

      if(data.node.state.opened){
        $('#agent_tree').jstree().close_node(id);
        $('#agent_tree').jstree().set_icon(id, 'fa fa-plus')
        return;
      }
      
      if(data.node.children != 0){
        $('#agent_tree').jstree().open_node(id);
        $('#agent_tree').jstree().set_icon(id, 'fa fa-minus')
        return;
      }

      $.ajax({
          url: url+`?id=${that.adminId}&tenant_id=${that.tenantId}&type=child`,
          type: 'GET',
          headers: { Authorization },
          success: function(data: any) {
            data.forEach(function(ele: any) {
              $('#agent_tree').jstree().create_node(id, ele);
            })
            $('#agent_tree').jstree().open_node(id);
            $('#agent_tree').jstree().set_icon(id, 'fa fa-minus')
          }
      });

    });

  }

  ngOnInit(): void {  }

}
