import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Admin } from 'src/app/models';
import { getRole } from 'src/app/services/utils.service';
import { AdminAuthService } from '../../admin/services/admin-auth.service';
import { SubAdminService } from '../sub-admin.service';
import { PageSizes } from 'src/app/shared/constants';
import Swal from 'sweetalert2';
import { PermissionService } from 'src/app/services/permission.service';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {

  admin!: Admin;

  pageSizes = PageSizes;

  subAdmins: Admin[] = [];
  
  subAdminParams: any = {
    size: 10,
    page: 1,
    search: '',
    tenant_id: 0,
    adminId: 0,
    order: 'desc',
    sort_by: 'id',
  };

  subAdminP: number = 1;
  subAdminTotal: number = 0;
  subAdminParentID: number = 0;

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Agents', path: '/agents' },
  ];

  isLoader:boolean = false;

  selectedSubAdmins: Map<number, Array<any>>|any = new Map();
  bulkEditForm: FormGroup|any;
  isBulkModalOpen: boolean = false;
  formSubmitted: boolean = false;
  globalIpAddress: any;
  firstTimeApiCall = false;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private subAdminService: SubAdminService,
    private adminAuthService: AdminAuthService,
    public permissionService: PermissionService,
    private formBuilder: FormBuilder) {  }

  ngOnInit(): void {
    this.adminAuthService.adminUser.subscribe((user: any) => {
      if(user && user.id) {
        this.admin = user;
        this.subAdminParams = { ...this.subAdminParams, tenant_id: this.admin.tenant_id, adminId: this.admin.id }
        // this.getSubAdmins();
      }
    });

    this.adminAuthService.tenantData.subscribe((tenantData:any) => {
      if(tenantData){
        this.globalIpAddress= tenantData?.global_ip_address
      }
    })

    this.bulkEditForm = this.formBuilder.group({
      isActive: [null],
      whitelistOption: [null],
      ips: this.formBuilder.array([]),
      ids: null,
      admin_type: 'sub_admin'
    });

    this.bulkEditForm.get('whitelistOption').valueChanges.subscribe((option:any) => {
      if (option === 'manual') {
        this.bulkEditForm.setControl('ips', this.formBuilder.array([this.createIPControl()]));
      } else {
        this.bulkEditForm.setControl('ips', this.formBuilder.array([]));
      }
    });
  }

  subAdminPageChanged(page: number) {
    this.subAdminParams = { ...this.subAdminParams, page };
    this.getSubAdmins();
  }

  subAdminResetFilter() {
    this.subAdminP =1;
    this.subAdminParams = {
      size: 10,
      page: 1,
      search: '',
      tenant_id: this.admin.tenant_id, 
      adminId: this.admin.id,
      order: 'desc',
      sort_by: 'id',
    };
    this.selectedSubAdmins.clear();
    this.getSubAdmins();
  }

  filterSubAdmins() {
    this.subAdminP = 1;
    this.subAdminParams = { ...this.subAdminParams, page: this.subAdminP };
    this.selectedSubAdmins.clear();
    this.getSubAdmins();
  }

  getSubAdmins() {
    this.isLoader = true
    this.firstTimeApiCall = true;
    this.subAdminService.getSubAdmins(this.subAdminParams).subscribe((res: any) => {
      this.subAdmins = res.record.data;
      this.subAdminTotal = res.record.count;
      this.subAdminParentID = res.record.parent_id;
      this.isLoader = false
    },(error:any) => {
      this.isLoader = false

    });
  }

  getRoles(roleIds: any) {
    if(roleIds) {
      if(roleIds && roleIds.length > 1) {
        return roleIds.split(',');
      } else {
        return roleIds.split('');
      }
    }
  }

  getRole(role: any) {
    return getRole(role);
  }

  activeSubadminStatus(admin: Admin) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to Active ${admin.agent_name}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Active it!'
    }).then((result) => {
      if (result.isConfirmed) {

        this.subAdminService.activeSubAdminStatus(admin.id).subscribe((res: any) => {
          toastr.success(res.message || 'Player updated successfully' );
          this.subAdmins = this.subAdmins.map((f: Admin) => {
            if(f.id == admin.id) {
              f.active =  true;
            }
            return f;
          });
        });
        
      }
    });

  }

  deactiveSubadminStatus(admin: Admin) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to Deactive ${admin.agent_name}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Deactive it!'
    }).then((result) => {
      if (result.isConfirmed) {

        this.subAdminService.deactiveSubAdminStatus(admin.id).subscribe((res: any) => {
          toastr.success(res.message || 'Player updated successfully' );
          this.subAdmins = this.subAdmins.map((f: Admin) => {
            if(f.id == admin.id) {
              f.active = false;
            }
            return f;
          });
        });
        
      }
    });
    
  }

  exportAsXLSX() {
    this.subAdminService.downloadAgents(this.subAdminParams).subscribe(
      (res: any) => {
        window.location.href = res.record.url;
    });
  }

  selectSubAdmin(subAdmin: Admin, evt: any){
    if(evt.target.checked)
    {
      this.selectedSubAdmins.set(this.subAdminP, (this.selectedSubAdmins.has(this.subAdminP)) ? [ ...this.selectedSubAdmins.get(this.subAdminP) , subAdmin] : [subAdmin]);
    }
    else{
      this.selectedSubAdmins.has(this.subAdminP)?
      this.selectedSubAdmins.set(this.subAdminP, this.selectedSubAdmins.get(this.subAdminP).filter((v:any)=> v.id !== subAdmin.id)):
      this.selectedSubAdmins.delete(this.subAdminP);
    }
  }

  toggleAll(evt:any){
    if(evt.target.checked) {
      this.selectedSubAdmins.set(this.subAdminP,this.subAdmins); 
    } else {
      this.selectedSubAdmins.delete(this.subAdminP)
    };
  }

  get allSelectedSubAdmins(){
    let arr: string | any[] = [];
    this.selectedSubAdmins.forEach((d:any)=>{
      arr = [...arr,...d];
    });
    return arr;
  }

  removeplayer(id:any)
  {
    this.selectedSubAdmins.has(this.subAdminP)?
    this.selectedSubAdmins.set(this.subAdminP, this.selectedSubAdmins.get(this.subAdminP).filter((v:any)=> v.id !== id)):
    this.selectedSubAdmins.delete(this.subAdminP);
  }

  isSubAdminChecked(temp:any){    
    return this.selectedSubAdmins.has(this.subAdminP) && this.selectedSubAdmins.get(this.subAdminP).find((u:any) => u.id == temp.id)
  }

  createIPControl(): FormControl {
    return this.formBuilder.control('', Validators.required);
  }

  get ips(){
    return this.bulkEditForm.get('ips') as FormArray;
  }

  getIpFormControl(index: number): FormControl {
    return this.ips.at(index) as FormControl;
  }

  addIP() {
    this.ips.push(this.createIPControl());
  }

  removeIP(index: number) {
    this.ips.removeAt(index);
  }

  openBulkEditModal(){
    let arr:any = [];
    for (const value of this.selectedSubAdmins.values()) {
      const ids = value.map((agent:any) => agent.id);
      arr = arr.concat(ids);
    }
    this.bulkFormControls.ids.setValue(arr);
    this.bulkFormControls.ids.updateValueAndValidity();

    $('#bulkEditModal').modal('show');
    this.isBulkModalOpen = true
  }

closeBulkEditModal(){
  $('#bulkEditModal').modal('hide');
  this.isBulkModalOpen = false
  this.formSubmitted =false
  this.resetBulkEditForm();
  this.selectedSubAdmins.clear();
}

resetBulkEditForm(){
  this.bulkEditForm.reset({
    isActive: null,
    whitelistOption: null,
    ips: [],
    ids: null,
    admin_type: 'sub_admin'
  });
}

get bulkFormControls() {
  return this.bulkEditForm.controls;
}

onSubmit(){
  this.formSubmitted = true
  if(this.bulkEditForm.invalid){
    return
  }

  if(!this.bulkEditForm.value.whitelistOption  && !this.bulkEditForm.value.isActive){
    toastr.error('Please select any one field to move further.')
    return
  }

  if(this.bulkEditForm.value.whitelistOption == 'global' && !this.globalIpAddress){
    toastr.error('Firstly, please set global ip address or ask your superior to do.')
    return
  }

  if(this.bulkEditForm.value.whitelistOption == 'manual' && this.bulkEditForm.value.ips.length < 1){
    toastr.error('Firstly, please set atleast one IP.')
    return
  }

  this.subAdminService.bulkEdit(this.bulkEditForm.value).subscribe((res:any)=>{
    toastr.success(res.message || 'Bulk Edited Successfully');
    this.closeBulkEditModal();
    this.getSubAdmins();
  }, (err:any) =>{
    this.closeBulkEditModal();
  })
}


openTenantCredentials(){
  this.closeBulkEditModal();
  this.router.navigate(['/tenants/credentials']);
}

setOrder(sort: any) {
  this.subAdminP =1;
  this.subAdminParams = { ...this.subAdminParams, page: this.subAdminP, order: sort.order, sort_by: sort.column };
  this.selectedSubAdmins.clear();
  this.getSubAdmins();
}

}
