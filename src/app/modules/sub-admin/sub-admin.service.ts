import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SuperAdminAuthService } from '../super-admin/services/super-admin-auth.service';

@Injectable({ providedIn: 'root' })

export class SubAdminService {

  constructor(private http: HttpClient,
    private superAdminAuthService: SuperAdminAuthService) { }

  // ====================== Admin url ==============================
  getSubAdmins(params: any) {
    return this.http.post(`admin/usersadmin/sub-admin`, params);
  }

  addSubAdmin(data: any) {
    return this.http.post(`admin/usersadmin/add`, data);
  }

  getSubAdmin(id: number) {
    return this.http.get(`admin/users/${id}`);
  }

  getSubAdminTree(id: number = 0) {
    return this.http.get(`admin/agent/tree/${id}`);
  }
  
  updateSubAdmin(data: any) {
    return this.http.post(`admin/usersadmin/edit/${data.id}`, data);
  }

  bulkEdit(data: any) {
    return this.http.post(`admin/usersadmin/bulkEdit`, data);
  }

  // getAdminPlayer(params: any) {
  //   return this.http.post(`admin/usersadmin/player`, params);
  // }

  updateSubAdminSetting(data: any) {
    return this.http.post(`admin/users/setting`, data);
  }

  deleteSubAdminSetting(id: number) {
    return this.http.delete(`admin/users/setting/${id}`);    
  }

  getAdminTenant() {
    return this.http.get(`admin/tenants`);
  }

  activeSubAdminStatus(id: number) {
    return this.http.post(`admin/users/active/${id}`, {});    
  }

  deactiveSubAdminStatus(id: number) {
    return this.http.post(`admin/users/deactive/${id}`, {});    
  }

    // ====================== Super Admin url ==============================

  addSuperSubAdmin(data: any) {
    return this.http.post(`super/admin/usersadmin/add`, data);
  }

  getSuperAdminSubAdmin(id: number) {
    return this.http.get(`super/admin/users/${id}`);
  }
  
  updateSuperSubAdmin(data: any) {
    return this.http.post(`super/admin/usersadmin/edit/${data.id}`, data);
  }
  
  getSuperAdminSubAdmins(params: any) {
    return this.http.post(`super/admin/usersadmin/agent`, params);
  }

  // getSuperAdminPlayer(params: any) {
  //   return this.http.post(`super/admin/usersadmin/player`, params);
  // }


  downloadAgents(params: any) {
    if(this.superAdminAuthService && this.superAdminAuthService.superAdminTokenValue) {
      // ====================== Super Admin url ==============================
      return this.http.post(`super/admin/usersadmin/download`, params) ;
    } else {
      // ====================== Admin url ==============================
      return this.http.post(`admin/usersadmin/download`, params);
    }
  }

  getPermissionRoles(params:any){
    return this.http.get(`admin/permissionRole/get?role_type=${params.role_type}`);
  }

}
