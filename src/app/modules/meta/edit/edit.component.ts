import { TenantService } from './../../tenant/tenant.service';
import { Component,OnInit} from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Menu } from 'src/app/models';
import { MetaService } from '../meta.service';
import Swal from 'sweetalert2';
declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})

export class EditComponent implements OnInit {
  
  
  id: number = 0;
  meta:any = [];
  metaForm: FormGroup | any;
  submitted: boolean = false;
  metaLoader: boolean = false;
  menu: Menu | null = null;
  drpMasterMenus: Menu[] = [];

  constructor(private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private MetaService: MetaService,
    private TenantService: TenantService,
  ) {
    
    this.id = this.route.snapshot.params['id'];
    this.metaForm = this.formBuilder.group({
      meta_title: ['', [ Validators.required ]],
      meta_description: ['', [ Validators.required ]],
      meta_keyword: ['', [ Validators.required ]],
      menu_id: ['', [ Validators.required ]],
      active: [''],
      id: [''],
    
    });

    let title: string = 'Create';
    
    if(this.id > 0) {
      title = "Edit";
    }

    this.breadcrumbs.push({ title, path: `/meta/${this.id}` });
  }


  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'SEO', path: '/meta' }
  ];

  ngOnInit(): void { 
    if(this.id > 0) {
      this.getMetaById(this.id);
    }
    this.getMenu();
  }

  get f() {
    return this.metaForm.controls;
  }

  getMetaById(id:any) {
      this.MetaService.getMetaPages({'id':id}).subscribe((res: any) => {
      this.meta = res.record.filter(function(getData:any) {
        return getData.id==id;
    })[0];

        this.metaForm.patchValue({
        meta_title: this.meta.meta_title,
        meta_description: this.meta.meta_description,
        meta_keyword: this.meta.meta_keyword,
        menu_id: this.meta.menu_id,
        active: this.meta.active,
        id: this.meta.id,
        enable_flag: this.meta.enable_meta_for_register,
      });
    });
  }
//   getMenu() {
//     this.MetaService.getAdminMenuMasterList().subscribe((res: any) => {
//        this.drpMasterMenus = res.record;

//     });
// }

  getMenu() {
    this.TenantService.getAdminMenuSetting().subscribe((res: any) => {
      this.drpMasterMenus = res.record;
    });
  }

  onSubmit() {
    this.submitted = true;

    if (this.metaForm.invalid){
      return;
    } 
    this.metaLoader = true;
    
    const data = this.metaForm.value;
      this.MetaService.createMeta(data)
      .subscribe((response: any) => {
        if(this.id > 0) {
          toastr.success(response?.message || 'META Page Updated Successfully!');
        }else{
          toastr.success(response?.message || 'META Page Created Successfully!');
        }
        this.router.navigate(['/meta']);
        this.metaLoader = false; 
      }, (err: any) => {
        this.metaLoader = false; 
      });
  }

}
