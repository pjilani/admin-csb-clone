import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })

export class MetaService {


  constructor(private http: HttpClient) { }

  // ====================== Admin url ==============================

  getMetaPages(params: any = []) {
    return this.http.get(`admin/meta/list`, {params: params});
  }

  createMeta(data: any) {
    return this.http.post(`admin/meta/add`, data);
  }

  updateMetaStatus(data:any){
    return this.http.post(`admin/meta/status`, data);
      
  }

  getAdminMenuMasterList() {
    return this.http.get(`admin/tenants/menu/list`);
  }


}
