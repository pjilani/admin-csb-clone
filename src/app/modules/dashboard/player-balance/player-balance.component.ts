import { Component, OnDestroy, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
import { DashboardService } from '../dashboard.service';
import { DatePipe } from '@angular/common';
import { TIMEZONE } from 'src/app/shared/constants';
import { PermissionService } from 'src/app/services/permission.service';
import * as moment from 'moment-timezone';
import { Subscription } from 'rxjs';

declare const $: any;
declare const toastr: any;

@Component({
  selector: 'app-player-balance',
  templateUrl: './player-balance.component.html',
  styleUrls: ['./player-balance.component.scss'],
  providers: [DatePipe],
})
export class PlayerBalanceComponent implements OnInit , OnDestroy{


  params: any = {};

  user_balance: any;

  loadDataBool: boolean = true;

  isSubAgentModuleAllowed: boolean = true;
  isPrimaryCurrencyDataEnable: boolean = true;
  isAllowedModule: any = '';
  financeData: any = [];

  casinoMenuCheck: boolean = false;
  sportMenuCheck: boolean = false;
  tenantsList: any[] = [];

  dataSubscription: Subscription;

  constructor(
    public superAdminAuthService: SuperAdminAuthService,
    public service: DashboardService,
    public datepipe: DatePipe,
    public permissionService: PermissionService
  ) {
    this.service._playerBalanceParams$.subscribe((res:any) => {      
      if(res){
        this.params = res
        this.setFields(res);
      }else{
        if (
          this.superAdminAuthService.superAdminTokenValue &&
          this.superAdminAuthService.superAdminTokenValue.length > 0
        ) {
          this.params = {...this.params,tenant_id : '0'}
        }else{
          this.params = {...this.params,tenant_id : ''}
          this.searchButton();
        }
      }
    })

    this.dataSubscription = this.service.playerBalance$.subscribe((res: any) => {
      if(res){
        this.user_balance = res.record.user_balance;

      }  
    });

    this.isSubAgentModuleAllowed = localStorage
      .getItem('allowedModules')
      ?.includes('subAgent')
      ? true
      : false;
    this.isPrimaryCurrencyDataEnable = localStorage
      .getItem('allowedModules')
      ?.includes('primaryCurrencyDataEnable')
      ? true
      : false;
    this.isAllowedModule = localStorage.getItem('domain')
      ? localStorage.getItem('domain')
      : '';
   }
  ngOnDestroy(): void {
    this.dataSubscription.unsubscribe()
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.service._loadDataBool$.subscribe((res:any) => {
      this.loadDataBool = res;      
    })

    this.service._casinoMenuCheck$.subscribe((res:any) => {
      this.casinoMenuCheck = res;      
    })

    this.service._sportMenuCheck$.subscribe((res:any) => {
      this.sportMenuCheck = res;      
    })

    this.service._tenantsList$.subscribe((res:any) => {
      if(res && Object.keys(res).length){
        this.tenantsList = res;      
      }
    })
  }

  resetFilter() {
    if (
      this.superAdminAuthService.superAdminTokenValue &&
      this.superAdminAuthService.superAdminTokenValue.length > 0
    ) {
      this.params = {
        ...this.params,
        tenant_id: '0',
      };
    } else {
      this.params = {
      };
    }

    this.searchButton();
  }

  searchButton() {

    this.service.setPlayerBalanceParams(this.params);
  }

  setFields(params:any){
    if(params.tenant_id){
      this.params = {...this.params, tenant_id:+params.tenant_id}
    }
  }

}
