import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { RouterModule, Routes } from '@angular/router';
import { HighchartsChartModule } from 'highcharts-angular';
import { FormsModule } from '@angular/forms';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { PipeModule } from 'src/app/pipes/pipes.module';
import { ActivePlayersWageredComponent } from './active-players-wagered/active-players-wagered.component';
import { CommonAdminAuthGuard } from 'src/app/guards';
import { NgxPaginationModule } from 'ngx-pagination';
import { DashboardOldComponent } from './dashboard-old.component';
import { FinancialActivityComponent } from './financial-activity/financial-activity.component';
import { PlayerActivityComponent } from './player-activity/player-activity.component';
import { CasinoComponent } from './casino/casino.component';
import { SportsBettingComponent } from './sports-betting/sports-betting.component';
import { PlayerBalanceComponent } from './player-balance/player-balance.component';

const dashboardRoutes: Routes = [
  { 
    path: '',
    component: DashboardComponent,
    // component: DashboardOldComponent
  },
  {
    path: 'wagered',
    component: ActivePlayersWageredComponent,
    canActivate: [CommonAdminAuthGuard]
  },
  
]

@NgModule({
  declarations: [
    DashboardComponent,
    DashboardOldComponent,
    ActivePlayersWageredComponent,
    FinancialActivityComponent,
    PlayerActivityComponent,
    CasinoComponent,
    SportsBettingComponent,
    PlayerBalanceComponent
  ],
  imports: [
    CommonModule,
    HighchartsChartModule,
    RouterModule.forChild(dashboardRoutes),
    FormsModule,
    PipeModule,
    DirectivesModule,
    ComponentsModule,
    NgxPaginationModule
  ]
})
export class DashboardModule { }
