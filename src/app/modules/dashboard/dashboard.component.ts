import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { SuperAdminAuthService } from '../super-admin/services/super-admin-auth.service';
import { AdminAuthService } from 'src/app/modules/admin/services/admin-auth.service';
import { Role } from 'src/app/models';
import { DomainForDisableModule } from 'src/app/shared/constants';
import { DashboardService } from './dashboard.service';
import { DatePipe } from '@angular/common';
import { ReportService } from '../reports/reports.service';
import {  TIMEZONE } from 'src/app/shared/constants';
import { PermissionService } from 'src/app/services/permission.service';
declare const $: any;
declare const toastr: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [DatePipe],
})
export class DashboardComponent implements OnInit, AfterViewInit, OnDestroy {
  isSubAgentModuleAllowed: boolean = true;
  isPrimaryCurrencyDataEnable: boolean = true;
  isAllowedModule: any = '';
  roles: string = localStorage.getItem('roles') || '';
  isAgent: boolean = false;
  tenantsList: any[] = [];
  userList: any[] = [];
  checkSuperAdmin: boolean = false;
  checkAdmin: boolean = false;
  casinoMenuCheck: boolean = false;
  sportMenuCheck: boolean = false;
  customDate = false;
  format: string = 'YYYY-MM-DD HH:mm:ss';
  timePeriodCategories: any[] = [];
  TIMEZONE: any[] = TIMEZONE;
  zone: any = '(GMT +00:00) UTC';

  DomainForDisableModule: any = DomainForDisableModule;

  dates: any = {
    start_date: new Date().toISOString().split('T')[0] + ' 00:00:00',
    end_date: new Date().toISOString().split('T')[0] + ' 23:59:59',
  };

  params: any = {
    // agent_id: '',
    // player_type: 'all',
    time_period: 'today',
    time_zone: 'UTC +00:00',
    time_zone_name: 'UTC +00:00',
    // tenant_id: '',
    // start_date: new Date().toISOString().split('T')[0]+' 00:00:00',
    // end_date: new Date().toISOString().split('T')[0]+' 23:59:59',
    page: 1,
    size:10
  };

  breadcrumbs: Array<any> = [{ title: 'Home', path: '/' }];

  loadDataBool: boolean = true;

  //optimisation
  selectedTab: string = 'financial_activity';
  isFirstTimeApiCallDone = false;

  constructor(
    public superAdminAuthService: SuperAdminAuthService,
    public adminAuthService: AdminAuthService,
    private dashBoardService: DashboardService,
    private reportService: ReportService,
    public datepipe: DatePipe,
    public permissionService: PermissionService
  ) {
    this.isSubAgentModuleAllowed = localStorage
      .getItem('allowedModules')
      ?.includes('subAgent')
      ? true
      : false;
    this.isPrimaryCurrencyDataEnable = localStorage
      .getItem('allowedModules')
      ?.includes('primaryCurrencyDataEnable')
      ? true
      : false;
    this.isAllowedModule = localStorage.getItem('domain')
      ? localStorage.getItem('domain')
      : '';

  }

  ngOnInit(): void {

    if (
      this.adminAuthService.adminTokenValue &&
      this.adminAuthService.adminTokenValue.length > 0
    ) {

      if (this.roles) {
        const roles = JSON.parse(this.roles);
        if (
          roles &&
          roles.findIndex((role: any) => role === Role.Admin) == -1 &&
          roles.findIndex((role: any) => role === Role.Agent) > -1
        ) {
          this.isAgent = true;
        }
      }
    }

    this.checkAdmin =
      this.adminAuthService.adminTokenValue &&
      this.adminAuthService.adminTokenValue.length > 0;
    this.checkSuperAdmin =
      this.superAdminAuthService.superAdminTokenValue &&
      this.superAdminAuthService.superAdminTokenValue.length > 0;

    this.dashBoardService._refreshToken$.next(true);
  }

  ngAfterViewInit(): void {
    this.dashBoardService._loadDataBool$.subscribe((res:any) => {
      this.loadDataBool = res;
    })

    this.dashBoardService._casinoMenuCheck$.subscribe((res: any) => {
      this.casinoMenuCheck = res;
    });

    this.dashBoardService._sportMenuCheck$.subscribe((res: any) => {
      this.sportMenuCheck = res;
    });
  }

  //optimisation
  changeTab(tab: string) {
    this.selectedTab = tab;
    this.dashBoardService._selectedTab$.next(tab);
  }

  ngOnDestroy(){
    this.dashBoardService.resetAll()
  }
}
