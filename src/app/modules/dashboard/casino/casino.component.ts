import { Component, OnDestroy, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
import { DashboardService } from '../dashboard.service';
import { DatePipe } from '@angular/common';
import { TIMEZONE } from 'src/app/shared/constants';
import { PermissionService } from 'src/app/services/permission.service';
import * as moment from 'moment-timezone';
import { Subscription } from 'rxjs';

declare const $: any;
declare const toastr: any;

@Component({
  selector: 'app-casino',
  templateUrl: './casino.component.html',
  styleUrls: ['./casino.component.scss'],
  providers: [DatePipe],
})
export class CasinoComponent implements OnInit,OnDestroy{

  loadDataBool: boolean = true;

  ggrData: any[] = [];
  winAmountData: any;
  stakeData: any[] = [];

  dates: any = {
    start_date: new Date().toISOString().split('T')[0] + ' 00:00:00',
    end_date: new Date().toISOString().split('T')[0] + ' 23:59:59',
  };

  params: any = {
    // agent_id: '',
    // player_type: 'all',
    time_period: 'today',
    time_zone: '',
    time_zone_name: '',
    // tenant_id: '',
    // start_date: new Date().toISOString().split('T')[0]+' 00:00:00',
    // end_date: new Date().toISOString().split('T')[0]+' 23:59:59',
    page: 1,
  };

  isHighcharts = typeof Highcharts === 'object';
  Highcharts: typeof Highcharts = Highcharts; // required
  chartConstructor: string = 'chart'; // optional string, defaults to 'chart'
  chartCallback: Highcharts.ChartCallbackFunction = function (chart) {  } // optional function, defaults to null
  updateFlag: boolean = false; // optional boolean
  oneToOneFlag: boolean = true; // optional boolean, defaults to false
  runOutsideAngular: boolean = false; // optional boolean, defaults to false

  totalStakeChartOptionsLine: Highcharts.Options = {
    title: {
      text: 'Total Stake'
    },
    legend: {
      layout: 'horizontal',
      align: 'center',
      verticalAlign: 'bottom',
    },
    xAxis: {
      categories: [],
    },
    yAxis: {
      title: {
        text: 'Total Stake'
      }
    },
    tooltip: {
      shared: true,
    //   valueSuffix: ' Sum'
    },
    credits: {
      enabled: false
    },
    plotOptions: {
      areaspline: {
        fillOpacity: 0.5
      }
    },
    series: []
  }

  ggrChartOptionsLine: Highcharts.Options = {
    title: {
      text: 'GGR'
    },
    legend: {
      layout: 'horizontal',
      align: 'center',
      verticalAlign: 'bottom',
    },
    xAxis: {
      categories: [],
    },
    yAxis: {
      title: {
        text: 'GGR'
      }
    },
    tooltip: {
      shared: true,
    //   valueSuffix: ' Sum'
    },
    credits: {
      enabled: false
    },
    plotOptions: {
      areaspline: {
        fillOpacity: 0.5
      }
    },
    series: []
  }

  isSubAgentModuleAllowed: boolean = true;
  isPrimaryCurrencyDataEnable: boolean = true;
  isAllowedModule: any = '';
  financeData: any = [];

  casinoMenuCheck: boolean = false;
  sportMenuCheck: boolean = false;
  tenantsList: any[] = [];
  userList: any[] = [];

  customDate = false;
  format: string = 'YYYY-MM-DD HH:mm:ss';
  timePeriodCategories: any[] = [];
  TIMEZONE: any[] = TIMEZONE;
  zone: any = '(GMT +00:00) UTC';

  dataSubscription: Subscription;

  constructor(
    public superAdminAuthService: SuperAdminAuthService,
    public service: DashboardService,
    public datepipe: DatePipe,
    public permissionService: PermissionService
  ) {
    this.service._casinoParams$.subscribe((res:any) => {      
      if(res){
        this.setFields(res);
      }else{
        if (
          this.superAdminAuthService.superAdminTokenValue &&
          this.superAdminAuthService.superAdminTokenValue.length > 0
        ) {
          this.setTimePeriod('today', '0');
        } else {
          this.params = { ...this.params, agent_id: '', player_type: 'all' };
          this.setTimePeriod('today');
        }
      }
    })

    this.dataSubscription = this.service.casino$.subscribe((res: any) => {
      if(res){        
        this.ggrData = res.record.ggr;

        this.ggrChartOptionsLine.series = res.record.ggr.length-1 ? res.record.ggr?.slice(0,-1) : res.record.ggr;
        this.totalStakeChartOptionsLine.series = res.record.stake.length-1 ? res.record.stake?.slice(0,-1) : res.record.stake;

        this.winAmountData = res.record.win_amount;
        this.stakeData = res.record.stake;
      }  
    });

    this.isSubAgentModuleAllowed = localStorage
      .getItem('allowedModules')
      ?.includes('subAgent')
      ? true
      : false;
    this.isPrimaryCurrencyDataEnable = localStorage
      .getItem('allowedModules')
      ?.includes('primaryCurrencyDataEnable')
      ? true
      : false;
    this.isAllowedModule = localStorage.getItem('domain')
      ? localStorage.getItem('domain')
      : '';
  }

  ngOnDestroy(): void {
    this.dataSubscription.unsubscribe();
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.service._loadDataBool$.subscribe((res:any) => {
      this.loadDataBool = res;      
    })

    this.service._casinoMenuCheck$.subscribe((res:any) => {
      this.casinoMenuCheck = res;      
    })

    this.service._sportMenuCheck$.subscribe((res:any) => {
      this.sportMenuCheck = res;      
    })

    this.service._tenantsList$.subscribe((res:any) => {
      if(res && Object.keys(res).length){
        this.tenantsList = res;      
      }
    })

    this.service._userList$.subscribe((res:any) => {
      if(res && Object.keys(res).length){
        this.userList = res;
      }      
    })
  }

  getMonday() {
    const d = new Date();
    const day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
    return new Date(d.setDate(diff));
  }

  getYesterday() {
    const d = new Date();
    return new Date(d.setDate(d.getDate() - 1));
  }

  nextweek(d: any) {
    const today = new Date(d);
    return new Date(today.getFullYear(), today.getMonth(), today.getDate() + 7);
  }

  getMonthStartDate() {
    const date = new Date();
    return new Date(date.getFullYear(), date.getMonth(), 2);
  }

  getMonthLastDate() {
    const date = new Date();
    return new Date(date.getFullYear(), date.getMonth() + 1, 1);
  }

  getLastMonthStartDate() {
    const date = new Date();
    return new Date(date.getFullYear(), date.getMonth() - 1, 2);
  }

  getLastMonthLastDate() {
    const date = new Date();
    return new Date(date.getFullYear(), date.getMonth() - 0);
  }

  resetFilter() {
    $('#time_zone').val('UTC +00:00').trigger('change');
    if (
      this.superAdminAuthService.superAdminTokenValue &&
      this.superAdminAuthService.superAdminTokenValue.length > 0
    ) {
      this.params = {
        ...this.params,
        tenant_id: '0',
        time_zone: 'UTC +00:00',
        time_zone_name: 'UTC +00:00',
      };
    } else {
      this.params = {
        ...this.params,
        player_type: 'all',
        agent_id: '',
        time_zone: 'UTC +00:00',
        time_zone_name: 'UTC +00:00',
      };
      $('#agent_id').val('').trigger('change');
    }

    this.params = { ...this.params, page: 1 ,size:10};

    // this.setTimePeriod('today');

    //code to set time period
    this.dates = {
      ...this.dates,
      start_date: new Date().toISOString().split('T')[0] + ' 00:00:00',
      end_date: new Date().toISOString().split('T')[0] + ' 23:59:59',
    };
    this.customDate = false;
    this.timePeriodCategories = [
      0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
      20, 21, 22, 23,
    ];

    this.searchButton();
  }
  filterSelectTimeZone(zonename: any) {
    if (zonename) {
      const zone = TIMEZONE.find((t) => t.zonename === zonename);
      this.zone = zone?.name;
      this.params = {
        ...this.params,
        time_zone: zone?.value,
        time_zone_name: zone?.zonename,
      };
    } else {
      this.zone = '';
      this.params = { ...this.params, time_zone: this.zone };
    }

    // if (!this.isFirstTimeApiCallDone) {
    //   this.isFirstTimeApiCallDone = true;
    //   this.searchButton();
    // }

    // this.getData();
    // this.getActivePlayersData();
  }

  setTimePeriod(time_period: string, tenant_id = '', date: any = '') {
    this.params = { ...this.params, time_period };
    if (tenant_id != '') {
      this.params = { ...this.params, tenant_id: tenant_id };
    }
    switch (time_period) {
      case 'today':
        this.dates = {
          ...this.dates,
          start_date: new Date().toISOString().split('T')[0] + ' 00:00:00',
          end_date: new Date().toISOString().split('T')[0] + ' 23:59:59',
        };
        this.customDate = false;
        this.timePeriodCategories = [
          0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
          20, 21, 22, 23,
        ];

        break;

      case 'yesterday':
        this.dates = {
          ...this.dates,
          start_date:
            this.getYesterday().toISOString().split('T')[0] + ' 00:00:00',
          end_date:
            this.getYesterday().toISOString().split('T')[0] + ' 23:59:59',
        };

        this.customDate = false;
        this.timePeriodCategories = [
          0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
          20, 21, 22, 23,
        ];

        break;

      case 'weekly':
        this.dates = {
          ...this.dates,
          start_date:
            this.getMonday().toISOString().split('T')[0] + ' 00:00:00',
          end_date:
            this.nextweek(this.getMonday()).toISOString().split('T')[0] +
            ' 23:59:59',
        };
        this.customDate = false;
        this.timePeriodCategories = [
          'Monday',
          'Tuesday',
          'Wednesday',
          'Thursday',
          'Friday',
          'Saturday',
          'Sunday',
        ];

        break;

      case 'lastMonth':
        this.dates = {
          ...this.dates,

          start_date:
            this.getLastMonthStartDate().toISOString().split('T')[0] +
            ' 00:00:00',
          end_date:
            this.getLastMonthLastDate().toISOString().split('T')[0] +
            ' 23:59:59',
        };
        this.customDate = false;

        // const firstDate = this.datepipe.transform(this.dates.start_date, 'yyyy-MM-dd');
        const lastDate = this.datepipe.transform(
          this.dates.end_date,
          'yyyy-MM-dd'
        );

        let array = [];
        let dates = new Date(this.dates.start_date);
        let loops = true;

        while (loops) {
          const firstDate = this.datepipe.transform(dates, 'yyyy-MM-dd');

          array.push(`${this.twoNum(dates.getDate())}`);

          dates.setDate(dates.getDate() + 1);

          if (firstDate == lastDate) {
            loops = false;
          }
        }

        this.timePeriodCategories = array;

        break;

      case 'monthly':
        this.dates = {
          ...this.dates,
          start_date:
            this.getMonthStartDate().toISOString().split('T')[0] + ' 00:00:00',
          end_date:
            this.getMonthLastDate().toISOString().split('T')[0] + ' 23:59:59',
        };
        this.customDate = false;
        const startDate = this.datepipe.transform(
          this.dates.start_date,
          'yyyy-MM-dd'
        );
        const endDate = this.datepipe.transform(
          this.dates.end_date,
          'yyyy-MM-dd'
        );

        let arr = [];
        let d = new Date(this.dates.start_date);
        let loop = true;

        while (loop) {
          const startDate = this.datepipe.transform(d, 'yyyy-MM-dd');

          arr.push(`${this.twoNum(d.getDate())}`);

          d.setDate(d.getDate() + 1);

          if (startDate == endDate) {
            loop = false;
          }
        }

        this.timePeriodCategories = arr;

        break;

      case 'yearly':
        this.dates = {
          ...this.dates,
          start_date: new Date().getFullYear() + '-01-01 00:00:00',
          end_date: new Date().getFullYear() + '-12-31 23:59:59',
        };
        this.customDate = false;
        this.timePeriodCategories = [
          'January',
          'February',
          'March',
          'April',
          'May',
          'June',
          'July',
          'August',
          'September',
          'October',
          'November',
          'December',
        ];

        break;

      case 'custom':
        if (time_period == 'custom' && date != '') {
          this.dates = {
            ...this.dates,
            start_date: date.start_date,
            end_date: date.end_date,
          };
          const startDate = this.datepipe.transform(
            this.dates.start_date,
            'yyyy-MM-dd'
          );
          const endDate = this.datepipe.transform(
            this.dates.end_date,
            'yyyy-MM-dd'
          );

          let arr = [];
          let d = new Date(this.dates.start_date);
          let loop = true;
          while (loop) {
            const startDate = this.datepipe.transform(d, 'yyyy-MM-dd');
            arr.push(`${this.convert(new Date(d))}`);
            d.setDate(d.getDate() + 1);
            if (startDate == endDate) {
              loop = false;
            }
          }

          this.timePeriodCategories = arr;
        } else {
          this.customDate = true;
        }

        break;

      default:
        this.dates = {
          ...this.dates,
          start_date: new Date().toISOString().split('T')[0] + ' 00:00:00',
          end_date: new Date().toISOString().split('T')[0] + ' 23:59:59',
        };

        this.timePeriodCategories = [
          0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
          20, 21, 22, 23,
        ];

        break;
    }

    // console.log(this.timePeriodCategories);

    
    
    // if((time_period != 'custom') || (time_period == 'custom' && date != ''))
    // this.getData();
    // this.getActivePlayersData();
  }

  convert(str: any) {
    var date = new Date(str),
      mnth = ('0' + (date.getMonth() + 1)).slice(-2),
      day = ('0' + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join('-');
  }

  isDateRangeWithinOneMonth(dateRange: any) {
    const { start_date, end_date } = dateRange;
    const startDate = new Date(start_date.replace(' ', 'T'));
    const endDate = new Date(end_date.replace(' ', 'T'));
    const differenceInMs = endDate.getTime() - startDate.getTime();
    const oneMonthInMs = 45 * 24 * 60 * 60 * 1000; // Assuming a month as 30 days
    return differenceInMs <= oneMonthInMs;
  }

  twoNum(num: number) {
    return num > 9 ? num : `0${num}`;
  }

  filterSelectAgent(agent_id: any) {
    this.params = { ...this.params, agent_id };
    // this.getData();
    // this.getActivePlayersData();
  }

  convertToUTC(inputDate: string, inputTimezone: string): string {
    const date = moment.tz(inputDate, inputTimezone);
    const utcDate = date.utc().format('YYYY-MM-DD HH:mm:ss');
    return utcDate;
  }

  searchButton() {
    if (
      this.params.time_period == 'custom' &&
      this.dates != '' &&
      !this.isDateRangeWithinOneMonth(this.dates)
    ) {
      toastr.error('Custom Date should be in range of 45 days');
      return;
    }

    this.params = { ...this.params, ...this.dates, page: 1};

    if (
      this.params.start_date &&
      this.params.end_date &&
      this.params.time_zone_name &&
      this.params.time_zone_name !== 'UTC +00:00'
    ) {
      this.params = {
        ...this.params,
        start_date: this.convertToUTC(
          this.params.start_date,
          this.params.time_zone_name
        ),
        end_date: this.convertToUTC(
          this.params.end_date,
          this.params.time_zone_name
        ),
      };
    }

    this.service.setCasinoParams(this.params);
  }

  setFields(params:any){
    this.params = {...this.params, tenant_id:+params.tenant_id}
    if(params.time_period == 'custom'){
      this.customDate = true
      const data = {
        start_date: moment.utc(params.start_date).tz((params?.time_zone_name) ? params?.time_zone_name : 'UTC +00:00').format('YYYY-MM-DD HH:mm:ss'),
        end_date : moment.utc(params.end_date).tz((params?.time_zone_name) ? params?.time_zone_name : 'UTC +00:00').format('YYYY-MM-DD HH:mm:ss')
      }
      this.setTimePeriod('custom','',data)
      setTimeout(() => {
        const datepickerInstance = $('#time_period').data('daterangepicker')
        datepickerInstance.setStartDate(data.start_date ?? this.dates.start_date);
        datepickerInstance.setEndDate(data.end_date ?? this.dates.end_date);
      }, 10);
      
    }else{
      this.customDate = false
      this.setTimePeriod(params.time_period);
    }

    setTimeout(() => {
      $('#time_zone').val(params?.time_zone_name ?? '').trigger('change');
      $('#agent_id').val(params?.agent_id ?? '').trigger('change');
    }, 10);
  }

}
