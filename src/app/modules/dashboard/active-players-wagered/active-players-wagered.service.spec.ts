import { TestBed } from '@angular/core/testing';

import { ActivePlayersWageredService } from './active-players-wagered.service';

describe('ActivePlayersWageredService', () => {
  let service: ActivePlayersWageredService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ActivePlayersWageredService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
