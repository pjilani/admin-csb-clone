import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivePlayersWageredComponent } from './active-players-wagered.component';

describe('ActivePlayersWageredComponent', () => {
  let component: ActivePlayersWageredComponent;
  let fixture: ComponentFixture<ActivePlayersWageredComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActivePlayersWageredComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivePlayersWageredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
