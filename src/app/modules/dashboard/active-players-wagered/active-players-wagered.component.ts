
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
import { AdminAuthService } from 'src/app/modules/admin/services/admin-auth.service';
import { Options } from 'highcharts';
import {PageSizes, TIMEZONE} from "src/app/shared/constants";
import * as Highcharts from 'highcharts';
import { DashboardService } from './../dashboard.service';
import { DatePipe } from '@angular/common';
import { ReportService } from '../../reports/reports.service';
import { ActivatedRoute } from '@angular/router';
import { PermissionService } from 'src/app/services/permission.service';
declare const $: any;

@Component({
  selector: 'app-active-players-wagered',
  templateUrl: './active-players-wagered.component.html',
  styleUrls: ['./active-players-wagered.component.scss'],
  providers: [DatePipe]
})
export class ActivePlayersWageredComponent implements OnInit, AfterViewInit {

  tenantsList: any[] = [];
  userList: any[] = [];
  betCount: number = 0;
  betList: any[] = [];
  checkSuperAdmin: boolean = false;
  checkAdmin: boolean = false;
  casinoMenuCheck: boolean = false;
  sportMenuCheck: boolean = false;
  p: any = 1;
  total: number = 0;
  pageSizes: any[] = PageSizes;
  reg_player: number = 0;
  isAgent:boolean = false;
  isSubAgentModuleAllowed:boolean = true;

  dates: any = {
    start_date: new Date().toISOString().split('T')[0] + ' 00:00:00',
    end_date: new Date().toISOString().split('T')[0] + ' 23:59:59',
  }

  params: any = {
    // agent_id: '',
    // player_type: 'all',
    time_period: 'today',
    page: 1
    // tenant_id: '',
    // start_date: new Date().toISOString().split('T')[0]+' 00:00:00',
    // end_date: new Date().toISOString().split('T')[0]+' 23:59:59',
  }

  breadcrumbs: Array<any> = [
    { title: 'Dashboard', path: '/' }
  ];

  loadDataBool: boolean = false;

  updateFlag: boolean = false; // optional boolean
  oneToOneFlag: boolean = true; // optional boolean, defaults to false
  runOutsideAngular: boolean = false; // optional boolean, defaults to false


  constructor(public superAdminAuthService: SuperAdminAuthService,
    public adminAuthService: AdminAuthService,
    private dashBoardService: DashboardService,
    private reportService: ReportService,
    private route: ActivatedRoute,
    public datepipe: DatePipe,
    public permissionService: PermissionService) { }

  ngOnInit(): void {

    let time_period = 'today';
    let agent_id = "0";
    let tenant_id = '0';
    let player_type = 'all';

    this.route.queryParams
      .subscribe(Qparams => {
        time_period = (Qparams.time ? Qparams.time : 'today');
        agent_id = (Qparams.agent_id ? Qparams.agent_id : 0);
        tenant_id = (Qparams.tenant_id ? Qparams.tenant_id  : '0');
        player_type = (Qparams.player_type ? Qparams.player_type:  'all');
        
      }
    );

    if (this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.getTenantList(time_period,tenant_id);
    } else {
      this.params = { ...this.params, agent_id: agent_id, player_type: player_type }
      this.adminAuthService.adminPermissions.subscribe(res => {
        if(this.permissionService.checkPermission('agents','R')){
          this.getAgents();
        }
      });
      this.setTimePeriod(time_period);
    }


    this.checkAdmin = this.adminAuthService.adminTokenValue && this.adminAuthService.adminTokenValue.length > 0;
    this.checkSuperAdmin = this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0;
    if (!this.checkSuperAdmin) {
      this.adminAuthService.tenantData.subscribe((tenantData:any) => {
        if(tenantData){
          let casino = tenantData?.menuList.filter((element: string) => {
            if (element === "Casino" || element === 'Live Casino') { return 1; }
            return 0;
          });
          if (casino != "") { this.casinoMenuCheck = true; }
          let sport = tenantData?.menuList.filter((element: string) => {
            if (element === "Exchange" || element === 'Live Bets') { return 1; }
            return 0;
          });
          if (sport != "") { this.sportMenuCheck = true; }
        }
      });
    }

  }

  ngAfterViewInit(): void {




  }

  getTenantList(time_period: string,query_tenant_id:any) {
    this.reportService.getSuperTenantsList().subscribe((res: any) => {
      let tenant_id = 0;
      this.tenantsList = res.record;
      tenant_id = (query_tenant_id ? query_tenant_id : (this.tenantsList ? this.tenantsList[0].id : 0));
      this.setTimePeriod(time_period, tenant_id);
    });
  }

  getAgents() {
    this.reportService.getAdminUserList().subscribe((res: any) => {
      this.userList = res.record;
    });
  }

    
  pageChanged(page: any) {
    // this.p = page;
    // console.log(this.p);
    
    this.params = { ...this.params, page };
    this.getData();
  }

  resetFilter() {
    this.p = 1;
    
    this.params = {
      time_zone: '',
      page: 1
    };
    if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
    
      this.params = { ...this.params, tenant_id:'0'   };
    }else{
      this.params = { ...this.params, player_type:'all' , agent_id:''  };
      $("#agent_id").val('').trigger('change');
  
    }
    this.setTimePeriod('today')
    this.getData();
    // this.getActivePlayerCount();

  }

  getMonday() {
    const d = new Date();
    const day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
    return new Date(d.setDate(diff));
  }

  nextweek(d: any) {
    const today = new Date(d);
    return new Date(today.getFullYear(), today.getMonth(), today.getDate() + 7);
  }

  getMonthStartDate() {
    const date = new Date();
    return new Date(date.getFullYear(), date.getMonth(), 2);
  }

  getMonthLastDate() {
    const date = new Date();
    return new Date(date.getFullYear(), date.getMonth() + 1, 1);
  }

  setTimePeriod(time_period: string, tenant_id:any = '') {
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.params = { ...this.params, time_period };
    if (tenant_id != '') {
      this.params = { ...this.params, tenant_id: tenant_id };
    }

    this.getData();
    // this.getActivePlayerCount();
  }

  filter(evt: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getData();
  }

  twoNum(num: number) {
    return num > 9 ? num : `0${num}`;
  }

  filterSelectAgent(agent_id: any) {
    this.params = { ...this.params, agent_id };
    this.getData();
  }

  getData() {

    this.loadDataBool = false;
    this.params = { ...this.params, ...this.dates };
    this.dashBoardService.getActivePlayer(this.params).subscribe((res: any) => {

      this.loadDataBool = true;
      this.betCount = res.record.totalBetCount;
      this.betList = res.record.betList.data;
      this.total = res.record.total;
     
      
      // console.log(this.betList);

    }, err => {
      this.loadDataBool = true;
    });

  }



}
