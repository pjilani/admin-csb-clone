import { AfterViewInit, Component, OnInit } from '@angular/core';
import { SuperAdminAuthService } from '../super-admin/services/super-admin-auth.service';
import { AdminAuthService } from 'src/app/modules/admin/services/admin-auth.service';
import { Options } from 'highcharts';
import { Role } from 'src/app/models';
import { DomainForDisableModule } from 'src/app/shared/constants';
import * as Highcharts from 'highcharts';
import { DashboardService } from './dashboard.service';
import { DatePipe } from '@angular/common';
import { ReportService } from '../reports/reports.service';
import { PageSizes , TIMEZONE} from 'src/app/shared/constants';
import * as moment from 'moment-timezone';
import { PermissionService } from 'src/app/services/permission.service';
declare const $: any;
declare const toastr: any; 

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard-old.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers:[DatePipe]
})

export class DashboardOldComponent implements OnInit, AfterViewInit {
  isSubAgentModuleAllowed:boolean = true;
  isPrimaryCurrencyDataEnable:boolean = true;
  isAllowedModule:any = '';
  roles: string = localStorage.getItem('roles') || '';
  isAgent:boolean = false;
  tenantsList: any[] = [];
  userList: any[] = [];
  checkSuperAdmin:boolean = false;
  checkAdmin:boolean = false;
  casinoMenuCheck:boolean = false;
  sportMenuCheck:boolean = false;
  customDate=false;
  format: string = "YYYY-MM-DD HH:mm:ss";
  timePeriodCategories: any[] = [];
  ggrData: any[] = [];
  betCount: number = 0;
  depositData: any[] = [];
  depositAmount: any[] = [];
  withdrawAutoCashUser: any[] = [];
  withdrawCashByAdmin: any[] = [];
  withdrawNonCashByAdmin: any[] = [];
  withdrawManualCashUser: any[] = [];
  TIMEZONE: any[] = TIMEZONE;
  zone: any = '(GMT +00:00) UTC';
  depositAutoCashUser: any[] = [];
  depositCashByAdmin: any[] = [];
  depositManualCashUser: any[] = [];
  depositNonCashByAdmin: any[] = [];

  balanceReport: any[] = [];
  balanceReportEUR: any;
  stakeData: any[] = [];
  betslipData: any;
  DomainForDisableModule:any = DomainForDisableModule;
  casinoProfitData: any[] = [];
  casinoProfitGrandTotalInEur: number = 0;

  casinoEsportProfitData: any[] = [];
  casinoEsportProfitGrandTotalInEur: number = 0;

  sportProfitData: any[] = [];
  sportProfitGrandTotalInEur: number = 0;

  user_balance: any;
  winAmountData: any;
  withdraw: any;

  reg_player: number = 0;

  dates: any = {
    start_date: new Date().toISOString().split('T')[0]+' 00:00:00',
    end_date: new Date().toISOString().split('T')[0]+' 23:59:59',
  }

  params: any = {
    // agent_id: '',
    // player_type: 'all',
    time_period: 'today',
    time_zone: 'UTC +00:00',
    time_zone_name: 'UTC +00:00',
    // tenant_id: '',
    // start_date: new Date().toISOString().split('T')[0]+' 00:00:00',
    // end_date: new Date().toISOString().split('T')[0]+' 23:59:59',
    page: 1
  }

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' }
  ];

  loadDataBool: boolean = false;

  isHighcharts = typeof Highcharts === 'object';
  Highcharts: typeof Highcharts = Highcharts; // required
  chartConstructor: string = 'chart'; // optional string, defaults to 'chart'
  chartCallback: Highcharts.ChartCallbackFunction = function (chart) {  } // optional function, defaults to null
  updateFlag: boolean = false; // optional boolean
  oneToOneFlag: boolean = true; // optional boolean, defaults to false
  runOutsideAngular: boolean = false; // optional boolean, defaults to false

  ggrChartOptionsLine: Highcharts.Options = {
    title: {
      text: 'GGR'
    },
    legend: {
      layout: 'horizontal',
      align: 'center',
      verticalAlign: 'bottom',
    },
    xAxis: {
      categories: [],
    },
    yAxis: {
      title: {
        text: 'GGR'
      }
    },
    tooltip: {
      shared: true,
    //   valueSuffix: ' Sum'
    },
    credits: {
      enabled: false
    },
    plotOptions: {
      areaspline: {
        fillOpacity: 0.5
      }
    },
    series: []
  }

  totalStakeChartOptionsLine: Highcharts.Options = {
    title: {
      text: 'Total Stake'
    },
    legend: {
      layout: 'horizontal',
      align: 'center',
      verticalAlign: 'bottom',
    },
    xAxis: {
      categories: [],
    },
    yAxis: {
      title: {
        text: 'Total Stake'
      }
    },
    tooltip: {
      shared: true,
    //   valueSuffix: ' Sum'
    },
    credits: {
      enabled: false
    },
    plotOptions: {
      areaspline: {
        fillOpacity: 0.5
      }
    },
    series: []
  }

  depositChartOptionsLine: Highcharts.Options = {
    title: {
      text: 'Deposits'
    },
    legend: {
      layout: 'horizontal',
      align: 'center',
      verticalAlign: 'bottom',
    },
    xAxis: {
      categories: [],
    },
    yAxis: {
      title: {
        text: 'Deposit'
      }
    },
    tooltip: {
      shared: true,
    //   valueSuffix: ' Sum'
    },
    credits: {
      enabled: false
    },
    plotOptions: {
      areaspline: {
        fillOpacity: 0.5
      }
    },
    series: []
  }

  activePlayerChartOptionsLine: Highcharts.Options = {
    title: {
      text: 'Total Unique Players'
    },
    legend: {
      layout: 'horizontal',
      align: 'center',
      verticalAlign: 'bottom',
    },
    xAxis: {
      categories: [],
    },
    yAxis: {
      title: {
        text: 'Total Unique Players'
      }
    },
    tooltip: {
      shared: true,
    //   valueSuffix: ' Sum'
    },
    credits: {
      enabled: false
    },
    plotOptions: {
      areaspline: {
        fillOpacity: 0.5
      }
    },
    series: []
  }

  betslipChartOptionsLine: Highcharts.Options = {
    title: {
      text: 'Betslips'
    },
    legend: {
      layout: 'horizontal',
      align: 'center',
      verticalAlign: 'bottom',
    },
    xAxis: {
      categories: [],
    },
    yAxis: {
      title: {
        text: 'Betslips amount'
      }
    },
    tooltip: {
      shared: true,
    },
    credits: {
      enabled: false
    },
    plotOptions: {
      areaspline: {
        fillOpacity: 0.5
      }
    },
    series: []
  }
  total: any;
  betList: any;
  p: number = 1;

  constructor(public superAdminAuthService: SuperAdminAuthService,
    public adminAuthService: AdminAuthService,
    private dashBoardService: DashboardService,
    private reportService: ReportService,
  public datepipe: DatePipe,
  public permissionService : PermissionService) { 
    this.isSubAgentModuleAllowed = (localStorage.getItem('allowedModules')?.includes('subAgent') ? true : false);
    this.isPrimaryCurrencyDataEnable = (localStorage.getItem('allowedModules')?.includes('primaryCurrencyDataEnable') ? true : false);
    this.isAllowedModule = (localStorage.getItem('domain') ? localStorage.getItem('domain') : '');

  }

  ngOnInit(): void {

    if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.getTenantList();
    } else {
      this.params = { ...this.params, agent_id: '', player_type: 'all' }
      this.adminAuthService.adminPermissions.subscribe(res => {
        if(this.permissionService.checkPermission('agents','R')){
          this.getAgents();
        }
      });
      
      this.setTimePeriod('today');
    }
   
    
    if (this.adminAuthService.adminTokenValue && this.adminAuthService.adminTokenValue.length > 0) {
     
      this.adminAuthService.adminUser.subscribe((user: any) => {
        if(user && user.id) {
          // this.params = {...this.params, time_zone: user.timezone}
          setTimeout(() => {
            const _zone = TIMEZONE.find(t => t.zonename == user.timezone);
            $(`#time_zone`).val(_zone?.zonename).trigger('change');
          }, 100);
        }
      });

      if(this.roles) {
        const roles = JSON.parse(this.roles);
        if(roles && roles.findIndex( (role: any) => role === Role.Admin ) == -1 && roles.findIndex( (role: any) => role === Role.Agent ) > -1) {
          this.isAgent = true;
        }

      }

    }

    this.checkAdmin = this.adminAuthService.adminTokenValue && this.adminAuthService.adminTokenValue.length > 0;
    this.checkSuperAdmin = this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0;
    if(!this.checkSuperAdmin){
      this.adminAuthService.getPersonalDetails().toPromise().then((response) => {
        let casino  = response?.record?.tenant?.menuList.filter((element:string) => {
          if(element === "Casino" || element === 'Live Casino'){return 1;}
          return 0;
        });
        if(casino != ""){this.casinoMenuCheck  = true; }
        let sport  = response?.record?.tenant?.menuList.filter((element:string) => {
          if(element === "Exchange" || element === 'Live Bets'){return 1;}
          return 0;
        });
        if(sport != ""){this.sportMenuCheck  = true; }
      });
    }
   
  }

  ngAfterViewInit(): void {




   }

  getTenantList() {
    this.reportService.getSuperTenantsList().subscribe((res: any) => {
      this.tenantsList = res.record;
      let tenant_id = (this.tenantsList ? this.tenantsList[0].id : 0)
        // this.setTimePeriod('today',tenant_id);
        this.setTimePeriod('today','0');
      });
  }

  getAgents() {
    this.reportService.getAdminUserList().subscribe((res: any) => {
      this.userList = res.record;
    });
  }

 getMonday() {
   const d = new Date();
    const day = d.getDay(),
        diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
    return new Date(d.setDate(diff));
  }

getYesterday() {
  const d = new Date();
    return new Date(d.setDate(d.getDate() - 1));
}
 
nextweek(d: any) {
    const today = new Date(d);
    return new Date(today.getFullYear(), today.getMonth(), today.getDate()+7);
}

getMonthStartDate() {
  const date = new Date();
  return new Date(date.getFullYear(), date.getMonth(), 2);
}

getMonthLastDate() {
  const date = new Date();
  return new Date(date.getFullYear(), date.getMonth() + 1, 1);
}

getLastMonthStartDate() {
  const date = new Date();
  return new Date(date.getFullYear(), date.getMonth() - 1, 2);
}

getLastMonthLastDate() {
  const date = new Date();
  return new Date(date.getFullYear(), date.getMonth() - 0);
}

resetFilter(){
  $( "#time_zone" ).val('UTC +00:00').trigger('change');
  if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
    
    this.params = { ...this.params, tenant_id:'0',      time_zone: 'UTC +00:00',
      time_zone_name: 'UTC +00:00',   };
  }else{
    this.params = { ...this.params, player_type:'all' , agent_id:'',      time_zone: 'UTC +00:00',
      time_zone_name: 'UTC +00:00',  };
    $("#agent_id").val('').trigger('change');


  }

  this.params = {...this.params, page: 1}

  this.setTimePeriod('today')
  
}
filterSelectTimeZone(zonename: any) {
  if(zonename) {
    const zone = TIMEZONE.find(t => t.zonename === zonename);
    this.zone = zone?.name;
    this.params = {...this.params, time_zone: zone?.value, time_zone_name: zone?.zonename};
  }else{
    this.zone='';
    this.params = {...this.params,  time_zone: this.zone};
  }

  this.getData();
  this.getActivePlayersData();
}

  setTimePeriod(time_period: string,tenant_id = '',date:any= '') {
    this.params = { ...this.params, time_period };
    if(tenant_id != ''){
      this.params = { ...this.params, tenant_id:tenant_id };
    }
    switch(time_period) {
      case 'today':
        this.dates = {
          ...this.dates,
          start_date: new Date().toISOString().split('T')[0]+' 00:00:00',
          end_date: new Date().toISOString().split('T')[0]+' 23:59:59'
        };
        this.customDate = false;
        this.timePeriodCategories = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];

        break;

      case 'yesterday':
        this.dates = {
          ...this.dates,
          start_date: this.getYesterday().toISOString().split('T')[0]+' 00:00:00',
          end_date: this.getYesterday().toISOString().split('T')[0]+' 23:59:59'
        };
        
        this.customDate = false;
        this.timePeriodCategories = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];

        break;

      case 'weekly':
        this.dates = {
          ...this.dates,
          start_date: this.getMonday().toISOString().split('T')[0]+' 00:00:00',
          end_date: this.nextweek(this.getMonday()).toISOString().split('T')[0]+' 23:59:59'
        };
        this.customDate = false;
        this.timePeriodCategories = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

        break;

        case 'lastMonth':
          this.dates = {
            ...this.dates,

            start_date: this.getLastMonthStartDate().toISOString().split('T')[0]+' 00:00:00',
            end_date: this.getLastMonthLastDate().toISOString().split('T')[0]+' 23:59:59'

          };
          this.customDate = false;

          // const firstDate = this.datepipe.transform(this.dates.start_date, 'yyyy-MM-dd');
          const lastDate = this.datepipe.transform(this.dates.end_date, 'yyyy-MM-dd');
  
          let array = [];
          let dates = new Date(this.dates.start_date);
          let loops = true;
  
          while(loops) {
            const firstDate = this.datepipe.transform(dates, 'yyyy-MM-dd');
  
            array.push(`${this.twoNum(dates.getDate())}`);
  
            dates.setDate(dates.getDate() + 1);
  
            if(firstDate == lastDate) {
              loops = false;
            }
          };
  
          this.timePeriodCategories = array;
          
          break;
          
      case 'monthly':
        this.dates = {
          ...this.dates,
          start_date: this.getMonthStartDate().toISOString().split('T')[0]+' 00:00:00',
          end_date: this.getMonthLastDate().toISOString().split('T')[0]+' 23:59:59'
        };
        this.customDate = false;
        const startDate = this.datepipe.transform(this.dates.start_date, 'yyyy-MM-dd');
        const endDate = this.datepipe.transform(this.dates.end_date, 'yyyy-MM-dd');

        let arr = [];
        let d = new Date(this.dates.start_date);
        let loop = true;

        while(loop) {
          const startDate = this.datepipe.transform(d, 'yyyy-MM-dd');

          arr.push(`${this.twoNum(d.getDate())}`);

          d.setDate(d.getDate() + 1);

          if(startDate == endDate) {
            loop = false;
          }
        };

        this.timePeriodCategories = arr;

        break;

      case 'yearly':
        this.dates = {
          ...this.dates,
          start_date: new Date().getFullYear()+'-01-01 00:00:00',
          end_date: new Date().getFullYear()+'-12-31 23:59:59'
        };
        this.customDate = false;
        this.timePeriodCategories = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

        break;


        case 'custom':
        if(time_period == 'custom' && date != '')
        {
          this.dates = {
            ...this.dates,
            start_date: date.start_date,
            end_date: date.end_date
          };
          const startDate = this.datepipe.transform(this.dates.start_date, 'yyyy-MM-dd');
          const endDate = this.datepipe.transform(this.dates.end_date, 'yyyy-MM-dd');
  
          let arr = [];
          let d = new Date(this.dates.start_date);
          let loop = true;
          while(loop) {
            const startDate = this.datepipe.transform(d, 'yyyy-MM-dd');
            arr.push(`${this.convert(new Date(d))}`);
            d.setDate(d.getDate() + 1);
            if(startDate == endDate) {
              loop = false;
            }
          };
  
          this.timePeriodCategories = arr;
      }else{
          this.customDate = true;
        }

        break;

      default:
        this.dates = {
            ...this.dates,
            start_date: new Date().toISOString().split('T')[0]+' 00:00:00',
            end_date: new Date().toISOString().split('T')[0]+' 23:59:59'
        };

        this.timePeriodCategories = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];

        break;
    }

    // console.log(this.timePeriodCategories);

    this.ggrChartOptionsLine.xAxis = { categories: this.timePeriodCategories };
    this.depositChartOptionsLine.xAxis = { categories: this.timePeriodCategories };
    this.totalStakeChartOptionsLine.xAxis = { categories: this.timePeriodCategories };
    this.activePlayerChartOptionsLine.xAxis = { categories: this.timePeriodCategories };
    this.betslipChartOptionsLine.xAxis = { categories: this.timePeriodCategories };
    if(time_period == 'custom' && date != '' && !this.isDateRangeWithinOneMonth(date)){
      toastr.error('Custom Date should be in range of 45 days');
      return
    }
    if((time_period != 'custom') || (time_period == 'custom' && date != ''))
    this.getData();
    this.getActivePlayersData();
  }


  convert(str:any) {
    var date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
  }

  isDateRangeWithinOneMonth(dateRange:any) {
    const { start_date, end_date } = dateRange;
    const startDate = new Date(start_date.replace(" ", "T"));
    const endDate = new Date(end_date.replace(" ", "T"));
    const differenceInMs = endDate.getTime() - startDate.getTime();
    const oneMonthInMs = 45 * 24 * 60 * 60 * 1000; // Assuming a month as 30 days
    return differenceInMs <= oneMonthInMs;
  }


  filter(evt: any) {
    this.getData();
    this.getActivePlayersData();
  }

  twoNum(num: number) {
    return num > 9 ? num : `0${num}`;
  }

  filterSelectAgent(agent_id: any) {
      this.params = {...this.params, agent_id };
      this.getData();
      this.getActivePlayersData();
  }

  convertToUTC(inputDate: string, inputTimezone: string): string {
    const date = moment.tz(inputDate, inputTimezone);
    const utcDate = date.utc().format('YYYY-MM-DD HH:mm:ss');
    return utcDate;
  }
  getData() {

    // $('#preloader').css('height', '');
    // $('#preloader img').show();

    this.loadDataBool = false;

    // time_period => today/monthly/weekly/yearly

    this.params = { ...this.params, ...this.dates };

    if(this.params.start_date && this.params.end_date && this.params.time_zone_name && this.params.time_zone_name !== 'UTC +00:00'){
      this.params = {...this.params, start_date: this.convertToUTC(this.params.start_date, this.params.time_zone_name), end_date:this.convertToUTC(this.params.end_date, this.params.time_zone_name)}
    }
    this.dashBoardService.getDashboard(this.params).subscribe((res: any) => {

      this.loadDataBool = true;
      // $('#preloader').css('height', 0);
      // $('#preloader img').hide();

      this.ggrChartOptionsLine.series = res.record.ggr;
      this.depositChartOptionsLine.series = res.record.deposit;
      this.totalStakeChartOptionsLine.series = res.record.stake;
      this.activePlayerChartOptionsLine.series = res.record.active_players;
      this.betslipChartOptionsLine.series = res.record.betslip.data;

      this.ggrData = res.record.ggr;
      this.depositData = res.record.deposit;      
      this.depositAmount = res.record.deposit_amount.data;
      this.stakeData = res.record.stake;
      this.winAmountData = res.record.win_amount;
      this.withdraw = res.record.withdraw;

      this.withdrawAutoCashUser = res.record.withdraw_auto_cash_user.data;
      this.withdrawCashByAdmin = res.record.withdraw_cash_by_admin.data;
      this.withdrawManualCashUser = res.record.withdraw_manual_cash_user.data;
      this.withdrawNonCashByAdmin = res.record.withdraw_non_cash_by_admin.data;

      this.depositAutoCashUser = res.record.deposit_auto_cash_user.data;
      this.depositCashByAdmin = res.record.deposit_cash_admin.data;
      this.depositManualCashUser = res.record.deposit_manual_cash_user.data;
      this.depositNonCashByAdmin = res.record.deposit_non_cash_admin.data;

      // Total Balance Report
      this.balanceReport = [];
      this.balanceReportEUR = 0;
      this.depositAmount.forEach((value, key) => {
        if(key < this.depositAmount.length){

          let revenue_total = value.total - this.withdraw.data[key]?.total;
          let eur_revenue_total = value.eur_total - this.withdraw.data[key]?.eur_total;
          let name = value.name;
          this.balanceReport.push({revenue_total,name,eur_revenue_total})
        }else{
          let total_eur = value.total - this.withdraw.eur_total;
          this.balanceReportEUR = total_eur;
        }
      });



      this.reg_player = res.record.reg_player;
      this.user_balance = res.record.user_balance;

      this.betslipData = res.record.betslip;

      this.casinoProfitData = res.record.casino_profit.data;
      this.casinoProfitGrandTotalInEur = res.record.casino_profit.grand_total_in_eur;

      this.casinoEsportProfitData = res.record.casino_esport_profit.data;
      this.casinoEsportProfitGrandTotalInEur = res.record.casino_esport_profit.grand_total_in_eur;

      this.sportProfitData = res.record.sport_profit.data;
      this.sportProfitGrandTotalInEur = res.record.sport_profit.grand_total_in_eur;

      // console.log(res.record);

    }, err => {
      this.loadDataBool = true;
      // $('#preloader').css('height', 0);
      // $('#preloader img').hide();
    });

    // this.dashBoardService.getActiveUniquePlayersCount({...this.params,is_dashboard:true}).subscribe((res: any) =>{
    //   this.betCount = (res.hits?.total?.value || 0);
    // },err =>{
    //
    // });

  }

  getActivePlayersData() {

    this.loadDataBool = false;
    this.params = { ...this.params, ...this.dates };
    this.dashBoardService.getActivePlayer(this.params).subscribe((res: any) => {

      this.loadDataBool = true;
      this.betCount = res.record.totalBetCount;
      this.betList = res.record.betList.data;
      this.total = res.record.total;
     
      
      // console.log(this.betList);

    }, err => {
      this.loadDataBool = true;
    });

  }

  filterActivePlayers(evt: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getActivePlayersData();
  }

  pageChanged(page: any) {
    // this.p = page;
    // console.log(this.p);
    
    this.params = { ...this.params, page };
    this.getActivePlayersData();
  }

}
