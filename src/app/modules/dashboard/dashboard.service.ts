import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SuperAdminAuthService } from '../super-admin/services/super-admin-auth.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilChanged, filter, switchMap, tap } from 'rxjs/operators';
import { AdminAuthService } from '../admin/services/admin-auth.service';
import { ReportService } from '../reports/reports.service';
import { PermissionService } from 'src/app/services/permission.service';

@Injectable({ providedIn: 'root' })
export class DashboardService {
  _financeParams$ = new BehaviorSubject(null);
  _casinoParams$ = new BehaviorSubject(null);
  _sportParams$ = new BehaviorSubject(null);
  _playerParams$ = new BehaviorSubject(null);
  _playerBalanceParams$ = new BehaviorSubject(null);

  _loadDataBool$ = new BehaviorSubject(true);
  _selectedTab$ = new BehaviorSubject('financial_activity');
  _casinoMenuCheck$ = new BehaviorSubject(false);
  _sportMenuCheck$ = new BehaviorSubject(false);
  _tenantsList$ = new BehaviorSubject([]);
  _userList$ = new BehaviorSubject([]);

  // Finance
  _finance$ = new BehaviorSubject(null);

  // Casino
  _casino$ = new BehaviorSubject(null);

  // Sport Betting
  _sport$ = new BehaviorSubject(null);

  // player Activity
  _player$ = new BehaviorSubject(null);

  // Player balance
  _playerBalance$ = new BehaviorSubject(null);

  _refreshToken$:any = new BehaviorSubject(null);

  constructor(
    private http: HttpClient,
    public superAdminAuthService: SuperAdminAuthService,
    public adminAuthService: AdminAuthService,
    public reportService: ReportService,
    public permissionService:PermissionService
  ) {
    this._refreshToken$.subscribe((res:any) => {
      if(res){
        this.fetchGameChecks()

        if (
          this.superAdminAuthService.superAdminTokenValue &&
          this.superAdminAuthService.superAdminTokenValue.length > 0
        ) {
          this.getTenantList()
        }else{
          this.getAgents();
        }
      }
    })
  }

  getDashboard(params: any) {
    if (
      this.superAdminAuthService.superAdminTokenValue &&
      this.superAdminAuthService.superAdminTokenValue.length > 0
    ) {
      return this.http.post(`super/admin/dashboard`, params);
    } else {
      return this.http.post(`admin/dashboard`, params);
    }
  }

  getActivePlayer(params: any) {
    if (
      this.superAdminAuthService.superAdminTokenValue &&
      this.superAdminAuthService.superAdminTokenValue.length > 0
    ) {
      return this.http.post(`super/admin/active-players`, params);
    } else {
      return this.http.post(`admin/active-players`, params);
    }
  }

  getActiveUniquePlayersCount(params: any) {
    if (
      this.superAdminAuthService.superAdminTokenValue &&
      this.superAdminAuthService.superAdminTokenValue.length > 0
    ) {
      return this.http.post(`super/admin/active-unique-players-count`, params);
    } else {
      return this.http.post(`admin/active-unique-players-count`, params);
    }
  }

  setFinanceParams(values:any){
    this._finance$.next(null);
    this._financeParams$.next(values);
  }

  fetchFinanceData() {
    this._loadDataBool$.next(false);
    if (
      this.superAdminAuthService.superAdminTokenValue &&
      this.superAdminAuthService.superAdminTokenValue.length > 0
    ) {
      this.http.post(`super/admin/dashboard/financial-activity`, this._financeParams$.value).
      subscribe((response: any) => {
          this._loadDataBool$.next(true);
          return this._finance$.next(response);
        });
    } else {
      this.http
        .post(`admin/dashboard/financial-activity`, this._financeParams$.value)
        .subscribe((response: any) => {
          this._loadDataBool$.next(true);
          return this._finance$.next(response);
        },(err:any) => {          
          this._loadDataBool$.next(true);
        });
    }
  }

  get finance$(): Observable<any> {
    return this._financeParams$.pipe(filter(v => !!v && this._selectedTab$.value == 'financial_activity'),
      distinctUntilChanged(),
      tap((params) => {
        if (!this._finance$.value) {          
          this.fetchFinanceData();
        }
      }),
      switchMap(() => this._finance$.asObservable())
    );
  }

  setCasinoParams(values:any){    
    this._casino$.next(null);
    this._casinoParams$.next(values);
  }

  fetchCasinoData() {
    this._loadDataBool$.next(false);
    if (
      this.superAdminAuthService.superAdminTokenValue &&
      this.superAdminAuthService.superAdminTokenValue.length > 0
    ) {
      this.http.post(`super/admin/dashboard/casino`, this._casinoParams$.value).
      subscribe((response: any) => {
          this._loadDataBool$.next(true);
          return this._casino$.next(response);
        });
    } else {
      this.http
        .post(`admin/dashboard/casino`, this._casinoParams$.value)
        .subscribe((response: any) => {
          this._loadDataBool$.next(true);
          return this._casino$.next(response);
        },(err:any) => {          
          this._loadDataBool$.next(true);
        });
    }
  }

  get casino$(): Observable<any> {
    return this._casinoParams$.pipe(filter(v => !!v && this._selectedTab$.value == 'casino'),
      distinctUntilChanged(),
      tap((params) => {
        if (!this._casino$.value) {          
          this.fetchCasinoData();
        }
      }),
      switchMap(() => this._casino$.asObservable())
    );
  }

  setSportParams(values:any){
    this._sport$.next(null);
    this._sportParams$.next(values);
  }

  fetchSportsBettingData() {
    this._loadDataBool$.next(false);
    if (
      this.superAdminAuthService.superAdminTokenValue &&
      this.superAdminAuthService.superAdminTokenValue.length > 0
    ) {
      this.http.post(`super/admin/dashboard/sport`, this._sportParams$.value).
      subscribe((response: any) => {
          this._loadDataBool$.next(true);
          return this._sport$.next(response);
        });
    } else {
      this.http
        .post(`admin/dashboard/sport`, this._sportParams$.value)
        .subscribe((response: any) => {
          this._loadDataBool$.next(true);
          return this._sport$.next(response);
        },(err:any) => {          
          this._loadDataBool$.next(true);
        });
    }
  }

  get sport$(): Observable<any> {
    return this._sportParams$.pipe(filter(v => !!v && this._selectedTab$.value == 'sport'),
      distinctUntilChanged(),
      tap((params) => {
        if (!this._sport$.value) {          
          this.fetchSportsBettingData();
        }
      }),
      switchMap(() => this._sport$.asObservable())
    );
  }

  setPlayerParams(values:any){
    this._player$.next(null);
    this._playerParams$.next(values);
  }

  fetchPlayerData() {
    this._loadDataBool$.next(false);
    if (
      this.superAdminAuthService.superAdminTokenValue &&
      this.superAdminAuthService.superAdminTokenValue.length > 0
    ) {
      this.http.post(`super/admin/dashboard/player-activity`, this._playerParams$.value).
      subscribe((response: any) => {
          this._loadDataBool$.next(true);
          return this._player$.next(response);
        });
    } else {
      this.http
        .post(`admin/dashboard/player-activity`, this._playerParams$.value)
        .subscribe((response: any) => {
          this._loadDataBool$.next(true);
          return this._player$.next(response);
        },(err:any) => {          
          this._loadDataBool$.next(true);
        });
    }
  }

  get player$(): Observable<any> {
    return this._playerParams$.pipe(filter(v => !!v && this._selectedTab$.value == 'player_activity'),
      distinctUntilChanged(),
      tap((params) => {
        if (!this._player$.value) {          
          this.fetchPlayerData();
        }
      }),
      switchMap(() => this._player$.asObservable())
    );
  }

  setPlayerBalanceParams(values:any){
    this._playerBalance$.next(null);
    this._playerBalanceParams$.next(values);
  }

  fetchPlayerBalanceData() {
    this._loadDataBool$.next(false);
    if (
      this.superAdminAuthService.superAdminTokenValue &&
      this.superAdminAuthService.superAdminTokenValue.length > 0
    ) {
      this.http.post(`super/admin/dashboard/player-balance-info`, this._playerBalanceParams$.value).
      subscribe((response: any) => {
          this._loadDataBool$.next(true);
          return this._playerBalance$.next(response);
        });
    } else {
      this.http
        .post(`admin/dashboard/player-balance-info`, this._playerBalanceParams$.value)
        .subscribe((response: any) => {
          this._loadDataBool$.next(true);
          return this._playerBalance$.next(response);
        },(err:any) => {          
          this._loadDataBool$.next(true);
        });
    }
  }

  get playerBalance$(): Observable<any> {
    return this._playerBalanceParams$.pipe(filter(v => !!v && this._selectedTab$.value == 'player_balance'),
      distinctUntilChanged(),
      tap((params) => {
        if (!this._playerBalance$.value) {          
          this.fetchPlayerBalanceData();
        }
      }),
      switchMap(() => this._playerBalance$.asObservable())
    );
  }

  fetchGameChecks(){
    let checkSuperAdmin =
      this.superAdminAuthService.superAdminTokenValue &&
      this.superAdminAuthService.superAdminTokenValue.length > 0;
    if (!checkSuperAdmin) {
      this.adminAuthService
        .getPersonalDetails()
        .toPromise()
        .then((response) => {
          let casino = response?.record?.tenant?.menuList.filter(
            (element: string) => {
              if (element === 'Casino' || element === 'Live Casino') {
                return 1;
              }
              return 0;
            }
          );
          if (casino != '') {
            this._casinoMenuCheck$.next(true);
          }
          let sport = response?.record?.tenant?.menuList.filter(
            (element: string) => {
              if (element === 'Exchange' || element === 'Live Bets') {
                return 1;
              }
              return 0;
            }
          );
          if (sport != '') {
            this._sportMenuCheck$.next(true);
          }
        });
    }
  }

  getTenantList() {
    this.reportService.getSuperTenantsList().subscribe((res: any) => {
      if(res && res.record){
        this._tenantsList$.next(res.record);
      }
    });
  }

  getAgents() {
    this.reportService.getAdminUserList().subscribe((res: any) => {
      this._userList$.next(res.record);
    });
  }

  resetAll(){
  this._financeParams$.next(null);
  this._casinoParams$.next(null);
  this._sportParams$.next(null);
  this._playerParams$.next(null);
  this._playerBalanceParams$.next(null);

  this._loadDataBool$.next(true);
  this._selectedTab$.next('financial_activity');
  this._casinoMenuCheck$.next(false);
  this._sportMenuCheck$.next(false);
  this._tenantsList$.next([]);
  this._userList$.next([]);

  this._finance$.next(null);

  this._casino$.next(null);

  this._sport$.next(null);

  this._player$.next(null);

  this._playerBalance$.next(null);
  }
}
