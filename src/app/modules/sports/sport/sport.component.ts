import { Component, OnInit } from '@angular/core';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
import { SportService } from '../sport.service';
import { PageSizes } from 'src/app/shared/constants';
import { TenantService } from '../../tenant/tenant.service';
import { AdminAuthService } from '../../admin/services/admin-auth.service';

import Swal from 'sweetalert2';
declare const $: any;
declare const toastr: any;
@Component({
  selector: 'app-sport',
  templateUrl: './sport.component.html',
  styleUrls: ['./sport.component.scss']
})

export class SportComponent implements OnInit {

  pageSizes: any[] = PageSizes;
  sports: any[] = [];

  allTenants: any[] = [];
  
  sport: any;

  p: number = 1;
  total: number = 0;

  params: any = {
    size: 10,
    page: 1,
    search: '',
    kind: '',
    enabled: '',
    tenant_id: ''
  };

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Sports', path: '/sports' },
  ];

  isLoader:boolean = false;
  firstTimeApiCall = false;

  constructor(private sportService: SportService,
    private tenantService: TenantService,
    public superAdminAuthService: SuperAdminAuthService,
  public adminAuthService: AdminAuthService) { }

  ngOnInit(): void {
    if (this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.getTenantsAll();
    } else {
      // this.getSports();
    }
  }

  getTenantsAll() {
    this.tenantService.getTenantsAll().subscribe((res: any) => {
      this.allTenants = res.record;
      this.tenantFilter(this.allTenants[0].id);
    });
  }

  getSports() {
    // console.log(this.params);
    this.isLoader = true
    this.firstTimeApiCall = true;
    this.sportService.getAdminSports(this.params).subscribe((res: any) => {
      this.sports = res.record.data.data;
      this.total = res.record.total;
      this.isLoader = false
    },(err:any)=>{
      this.isLoader = false
    });  
  }

  filter(evt: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getSports();
  }

  tenantFilter(tenant_id: any) {
    // this.p = 1;
    this.params = { ...this.params, tenant_id };
    // if (tenant_id) {
    //   this.getSports();
    // }
  }

  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getSports();
  }

  resetFilter() {
    setTimeout(() => {
      $("#tenant_id").val(this.allTenants.length > 0 ? this.allTenants[0].id : '').trigger('change');
    }, 100);

    this.p = 1;
    this.params = {
      size: 10,
      page: 1,
      search: '',
      kind: '',
      enabled: '',
      tenant_id: this.allTenants.length > 0 ? this.allTenants[0].id : ''
    };

    this.getSports();
  }

  sportDetails(sport: any) {
    this.sport = sport;
  }

  updateSportStatus(sport: any, status: string) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to ${!sport.is_deleted ? 'Disabled' : 'Enabled'} ${sport.name_en}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, ${!sport.is_deleted ? 'Disabled' : 'Enabled'} it!`
    }).then((result) => {
      if (result.isConfirmed) {

        this.sportService.updateAdminSportEnabled({ id : sport.id, status }).subscribe((res: any) => {
          toastr.success(res.message || 'Sport updated successfully' );
          this.sports = this.sports.map((f: any) => {
            if(f.id == sport.id) {
              f.is_deleted = sport.is_deleted ? false : true;
            }
            return f;
          });
        });
        
      }
    });
  }

}
