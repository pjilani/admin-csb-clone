import { Component, OnInit } from '@angular/core';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
import { SportService } from '../sport.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PageSizes } from 'src/app/shared/constants';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { AdminAuthService } from '../../admin/services/admin-auth.service';
import { TenantService } from '../../tenant/tenant.service';

declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './tournament.component.html',
  styleUrls: ['./tournament.component.scss']
})

export class TournamentComponent implements OnInit {

  pageSizes: any[] = PageSizes;
  countries: any[] = [];

  tournaments: any[] = [];
  sports: any[] = [];

  allTenants: any[] = [];
  
  tournament: any;

  p: number = 1;
  total: number = 0;

  params: any = {
    size: 10,
    page: 1,
    sport_id: '',
    search: '',
    country: '',
    enabled: '',
    tenant_id: ''
  };

  queryParams: any = {
    sport_id: '',
    tenant_id: ''
  };

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Tournaments', path: '/sports/tournaments' },
  ];

  tournamentForm: FormGroup | any;
  submitted: boolean = false;
  tournamentLoader: boolean = false;
  isLoader:boolean = false;
  firstTimeApiCall = false;


  constructor(private sportService: SportService,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
    public superAdminAuthService: SuperAdminAuthService,
    private tenantService: TenantService,
  public adminAuthService: AdminAuthService,) {

      this.tournamentForm = this.formBuilder.group({
        tournament_id: ['', [ Validators.required ]],
        display_name: ['', [ Validators.required ]],
        rank: ['', [ Validators.required, Validators.min(1) ]]
      });

      const sport_id = this.route.snapshot.queryParams['sport_id'];
      const tenant_id = this.route.snapshot.queryParams['tenant_id'];
      
      this.queryParams = { sport_id: sport_id || '', tenant_id: tenant_id || '' };

      this.params = { ...this.params, ...this.queryParams };
  }

  ngOnInit(): void {
    // this.getTournament();
    this.getAllCountries();
    
    if (this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.getTenantsAll();
    } else {
      this.getAllSport(this.queryParams.tenant_id);
      // this.getTournament();
    }

  }

  get f() {
    return this.tournamentForm.controls;
  }

  getTenantsAll() {
    this.tenantService.getTenantsAll().subscribe((res: any) => {
      this.allTenants = res.record;

      if (this.queryParams.tenant_id) {

        setTimeout(() => {
          $("#tenant_id").val(this.queryParams.tenant_id).trigger('change');
        }, 100);

        this.tenantFilter(this.queryParams.tenant_id);
      } else {
        this.tenantFilter(this.allTenants[0].id);
      }

    });
  }

  tenantFilter(tenant_id: any) {
    // this.p = 1;
    this.params = { ...this.params, tenant_id };
    if (tenant_id) {
      this.getAllSport(tenant_id);
      // this.getTournament();
    }
  }

  getAllSport(tenant_id: any) {
    const params = { ...this.params, page: this.p, tenant_id, league_id: '', sport_id: '' };
    this.sportService.getTenantAllSports(params).subscribe((res: any) => {
      this.sports = res.record;

      if(this.params.sport_id > 0) {
        setTimeout(() => {
          $("#sport_id").val(this.params.sport_id).trigger('change');
        }, 100);
      }

    });  
  }

  getTournament() {
    this.isLoader = true
    this.firstTimeApiCall = true;
    this.sportService.getAdminLeagues(this.params).subscribe((res: any) => {
      this.tournaments = res.record.data;
      this.total = res.record.total;
      this.isLoader = false
    },(error:any) => {
      this.isLoader = false

    });  
  }

  getAllCountries() {
    this.sportService.getAdminAllCountries().subscribe((res: any) => {
      this.countries = res.record;
    });
  }

  selectCountry(country: any) {
    // this.p = 1;
    this.params = { ...this.params, country };
    // if (country) { 
    //   this.getTournament();
    // }
  }

  filter(evt: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getTournament();
  }

  sportFilter(sport_id: any) {
    // this.p = 1;
    this.params = { ...this.params, sport_id };
    // if (sport_id) { 
    //   this.getTournament(); 
    // }
  }
  
  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getTournament();
  }

  resetFilter() {

    this.p = 1;

    setTimeout(() => {
      $("#sport_id").val(this.queryParams.sport_id).trigger('change');
      $("#country").val("").trigger('change');
      $("#tenant_id").val((this.queryParams.tenant_id)?this.queryParams.tenant_id :this.allTenants[0].id ).trigger('change');
    }, 100);

    const sport_id = this.route.snapshot.queryParams['sport_id'];

    this.params = {
      size: 10,
      page: 1,
      search: '',
      country: '',
      enabled: '',
      sport_id: this.queryParams.sport_id,
      tenant_id: (this.queryParams.tenant_id)?this.queryParams.tenant_id :this.allTenants[0].id
    };

    this.getTournament();
  }

  updateTournamentStatus(tournament: any, status: number) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You wont to ${status === 0 ? 'Disabled' : 'Enabled'} ${tournament.name_en}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, ${status === 0 ? 'Disabled' : 'Enabled'} it!`
    }).then((result) => {
      if (result.isConfirmed) {

        if (status === 0) {

          this.sportService.updateAdminTournamentDisabled({ id: tournament.id }).subscribe((res: any) => {
            toastr.success(res.message || 'Tournament Disabled successfully');
            this.tournaments = this.tournaments.map((f: any) => {
              if (f.id == tournament.id) {
                f.blocked = res.record;
              }
              return f;
            });
          });
          
        } else {

          this.sportService.updateAdminTournamentEnabled({ id: tournament.blocked.id }).subscribe((res: any) => {
            toastr.success(res.message || 'Tournament Enabled successfully');
            this.tournaments = this.tournaments.map((f: any) => {
              if (f.id == tournament.id) {
                f.blocked = null;
              }
              return f;
            });
          });

        }
        
      }
    });
  }

  updateTournamentTopLeague(tournament: any, status: number) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You want to ${tournament.enabled ? 'No' : 'Yes'} ${tournament.name_en}!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, ${tournament.enabled ? 'No' : 'Yes'} it!`
    }).then((result) => {
      if (result.isConfirmed) {

        this.sportService.updateAdminTournamentEnabled({id: tournament.id, status}).subscribe((res: any) => {
          toastr.success(res.message || 'Tournament updated successfully' );
          this.tournaments = this.tournaments.map((f: any) => {
            if(f.id == tournament.id) {
              f.enabled = tournament.enabled ? false : true;
            }
            return f;
          });
        });
        
      }
    });
    
  }


  tournamentDetails(tournament: any) {
    this.tournament = tournament;
    // console.log(this.tournament);
  }

  editTournament(tournament: any) {
    this.submitted = false;
    // console.log(tournament);
    this.tournamentForm.patchValue({
      tournament_id: tournament.id,
      display_name: tournament.display_name,
      rank: tournament.rank,
    });
  }

    onSubmit() {
      this.submitted = true;
      // stop here if form is invalid
      if (this.tournamentForm.invalid) return;

      // console.log(this.tournamentForm.value);
      
      this.tournamentLoader = true;
      this.tournamentLoader = false;

      $('#modal-tournament').modal('hide');
    }

}
