import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SuperAdminAuthService } from './../super-admin/services/super-admin-auth.service';

@Injectable({
  providedIn: 'root'
})

export class SportService {

  constructor(private http: HttpClient,
   public superAdminAuthService: SuperAdminAuthService) { }

  // Admin

  // Sport
  getAdminSports(params: any) {
    if(this.superAdminAuthService && this.superAdminAuthService.superAdminTokenValue) {
      return this.http.get(`super/admin/sports?page=${params.page}&size=${params.size}&search=${params.search}&kind=${params.kind}&enabled=${params.enabled}&tenant_id=${params.tenant_id}`);
    } else {
      return this.http.get(`admin/sports?page=${params.page}&size=${params.size}&search=${params.search}&kind=${params.kind}&enabled=${params.enabled}&tenant_id=${params.tenant_id}`);
    }
  }

  getAdminAllSports() {
    if(this.superAdminAuthService && this.superAdminAuthService.superAdminTokenValue) {
      return this.http.get(`super/admin/sports/all-sports`);
    } else { 
      return this.http.get(`admin/sports/all-sports`);
    }
  }

  getTenantAllSports(params: any) {
    if(this.superAdminAuthService && this.superAdminAuthService.superAdminTokenValue) {
      return this.http.get(`super/admin/sports/all-sports?tenant_id=${params.tenant_id}`);
    } else { 
      return this.http.get(`admin/sports/all-sports?tenant_id=${params.tenant_id}`);
    }
  }

  updateAdminSportEnabled(params: any) {
    if(this.superAdminAuthService && this.superAdminAuthService.superAdminTokenValue) {
      return this.http.put(`super/admin/sports/update/enabled`, params);    
    } else {
      return this.http.put(`admin/sports/update/enabled`, params);    
    }
  }

  // Location

  getAdminCountries(params: any) {
    if(this.superAdminAuthService && this.superAdminAuthService.superAdminTokenValue) {
      // return this.http.get(`super/admin/sports/countries?page=${params.page}&size=${params.size}&sport_id=${params.sport_id}`);
      return this.http.post(`super/admin/sports/countries`,params);
    } else {
      // return this.http.get(`admin/sports/countries?page=${params.page}&size=${params.size}&sport_id=${params.sport_id}`);
      return this.http.post(`admin/sports/countries`,params);
    }
  }

  getAdminAllCountries(params: any=[]) {
    if(this.superAdminAuthService && this.superAdminAuthService.superAdminTokenValue) {
      return this.http.post(`super/admin/sports/all-countries`,params);
    } else {
      return this.http.post(`admin/sports/all-countries`,params);
    }
  }

  getAdminSportsCountries() {
    if(this.superAdminAuthService && this.superAdminAuthService.superAdminTokenValue) {
      return this.http.get(`super/admin/sports/countries/location`);
    } else {
      return this.http.get(`admin/sports/countries/location`);
    }
  }

  // League

  getAdminLeagues(params: any) {
    if(this.superAdminAuthService && this.superAdminAuthService.superAdminTokenValue) {
      return this.http.get(`super/admin/sports/leagues?page=${params.page}&size=${params.size}&sport_id=${params.sport_id}&search=${params.search}&country=${params.country}&enabled=${params.enabled}&tenant_id=${params.tenant_id}`);
    } else {
      return this.http.get(`admin/sports/leagues?page=${params.page}&size=${params.size}&sport_id=${params.sport_id}&search=${params.search}&country=${params.country}&enabled=${params.enabled}&tenant_id=${params.tenant_id}`);
    }
  }

  // getAdminAllLeagues(params: any) {
  //   if(this.superAdminAuthService && this.superAdminAuthService.superAdminTokenValue) {
  //     return this.http.get(`super/admin/sports/all-leagues?tenant_id=${params.tenant_id}`);
  //   } else {
  //     return this.http.get(`admin/sports/all-leagues`);
  //   }
  // }

  updateAdminTournamentEnabled(params: any) {
    return this.http.put(`admin/sports/leagues/update/enabled`, params);    
  }

  updateAdminTournamentDisabled(params: any) {
    return this.http.put(`admin/sports/leagues/update/disabled`, params);    
  }

  // Match

  getAdminMatches(params: any) {
    if(this.superAdminAuthService && this.superAdminAuthService.superAdminTokenValue) {
      return this.http.get(`super/admin/sports/matches?page=${params.page}&size=${params.size}&league_id=${params.league_id}&sport_id=${params.sport_id}&search=${params.search}&country=${params.country}&enabled=${params.enabled}&status=${params.status}&tenant_id=${params.tenant_id}`);
    } else {
      return this.http.get(`admin/sports/matches?page=${params.page}&size=${params.size}&league_id=${params.league_id}&sport_id=${params.sport_id}&search=${params.search}&country=${params.country}&enabled=${params.enabled}&status=${params.status}&tenant_id=${params.tenant_id}`);
    }
  }

  getAdminAllMatches(params: any) {
    if(this.superAdminAuthService && this.superAdminAuthService.superAdminTokenValue) {
      return this.http.get(`super/admin/sports/all-matches?league_id=${params.league_id}&tenant_id=${params.tenant_id}`);
    } else {
      return this.http.get(`admin/sports/all-matches?league_id=${params.league_id}`);
    }
  }

  updateAdminMatchEnabled(data: any) {
    return this.http.put(`admin/sports/match/update/enabled`, data);
  }

  updateAdminMatchDisabled(data: any) {
    return this.http.put(`admin/sports/match/update/disabled`, data);
  }

  // Market

  getAdminMarkets(params: any) {
    if(this.superAdminAuthService && this.superAdminAuthService.superAdminTokenValue) {
      return this.http.get(`super/admin/sports/markets?page=${params.page}&size=${params.size}&sport_id=${params.sport_id}&sport_kind=${params.sport_kind}&search=${params.search}`);
    } else {
      return this.http.get(`admin/sports/markets?page=${params.page}&size=${params.size}&sport_id=${params.sport_id}&sport_kind=${params.sport_kind}&search=${params.search}`);
    }
  }

  getBets(params: any) {
      return this.http.get(`super/admin/sports/bets?page=${params.page}&size=${params.size}&search=${params.search}&time_zone${params.time_zone}&time_zone_name=${params.time_zone_name}`);
  }


  setSettlementOtp() {
    // if(this.superAdminAuthService && this.superAdminAuthService.superAdminTokenValue) {
      return this.http.get(`super/admin/sports/settlementOtp`);
    // }
  }
  setSettlement(params: any=[]) {
    // if(this.superAdminAuthService && this.superAdminAuthService.superAdminTokenValue) {
    return this.http.post(`super/admin/sports/settlement`,params);
    // }
  }
}
