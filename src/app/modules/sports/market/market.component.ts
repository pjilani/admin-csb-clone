import { Component, OnInit } from '@angular/core';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
import { SportService } from '../sport.service';
import { PageSizes } from 'src/app/shared/constants';
import { ActivatedRoute } from '@angular/router';

declare const $: any;

@Component({
  selector: 'app-market',
  templateUrl: './market.component.html',
  styleUrls: ['./market.component.scss']
})
export class MarketComponent implements OnInit {

  pageSizes: any[] = PageSizes;

  markets: any[] = [];
  sports: any[] = [];

  p: number = 1;
  total: number = 0;

  params: any = {
    size: 10,
    page: 1,
    sport_id: '',
    sport_kind: '0',
    search: '',
  };

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Markets', path: '/sports/markets' },
  ];

  isLoader:boolean = false;
  firstTimeApiCall = false;

  constructor(private sportService: SportService,
            private route: ActivatedRoute,
    public superAdminAuthService: SuperAdminAuthService) {

      const sport_id = this.route.snapshot.queryParams['sport_id'];

      this.params = { ...this.params, sport_id: sport_id ? sport_id : '' };
  }

  ngOnInit(): void {
    this.getAllSport();
  }

  getAllSport() {
    this.sportService.getAdminAllSports().subscribe((res: any) => {
      this.sports = res.record;

      if(this.params.sport_id > 0) {
        setTimeout(() => {
          $("#sport_id").val(this.params.sport_id).trigger('change');
        }, 100);
      } else {
        this.params = { ...this.params, sport_id: this.sports[0].id };
      }

      this.getMarkets();
    });  
  }

  getMarkets() {
    // console.log(this.params);
    this.isLoader = true
    this.firstTimeApiCall = true;
    this.sportService.getAdminMarkets(this.params).subscribe((res: any) => {
      this.markets = res.record.data;
      this.total = res.record.total;
      this.isLoader = false
    },(error:any) => {
      this.isLoader = false

    });  
  }

  filter(evt: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getMarkets();
  }
  
  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getMarkets();
  }

  sportFilter(sport_id: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p, sport_id };
    this.getMarkets();
  }

  resetFilter() {
    const sport_id = this.route.snapshot.queryParams['sport_id'];

    setTimeout(() => {
      $("#sport_id").val(this.sports[0].id).trigger('change');
    }, 100);

    this.p = 1;

    this.params = {
      size: 10,
      page: 1,
      sport_kind: '0',
      search: '',
      sport_id: sport_id ? sport_id : this.sports[0].id,
    };

    this.getMarkets();
  }

}
