import {Component, OnInit} from "@angular/core";
import {ThirdPartyAPIService} from "src/app/services/third-party.service";
import {PageSizes, TIMEZONE} from "src/app/shared/constants";
import {AdminAuthService} from "../../admin/services/admin-auth.service";
import {SuperAdminAuthService} from "../../super-admin/services/super-admin-auth.service";
import {SportService} from "../sport.service";

declare const $: any;
declare const toastr: any;

@Component({
  selector: 'app-bet-settlement',
  templateUrl: './bet-settlement.component.html',
  styleUrls: ['./bet-settlement.component.scss'],
  providers: [ ThirdPartyAPIService ]
})
  
export class BetSettlementComponent implements OnInit {
  
  settlement: number = -1;
  
  pageSizes: any[] = PageSizes;
  TIMEZONE: any[] = TIMEZONE;

  zone: any = '(GMT +00:00) UTC';

  settlements: any[] = [
    { name: 'Cancelled', value: -1 },
    { name: 'Lost', value: 1 },
    { name: 'Won', value: 2 },
    { name: 'Refund', value: 3 },
    { name: 'Halflost', value: 4 },
    { name: 'Halfwon', value: 5 }
  ];

  bets: any[] = [];

  selectedBets: any[] = [];

  loader: boolean = false;
  loaderResetOtp: boolean = false;
  loaderOtpSubmit: boolean = false;
  data : any = {
    bets:{},
    otp:'',
    otp_token:''
  };
  isOtp : boolean = false;
  IsSaveSettlement : boolean = true;
  IsSaveSettlementtable : boolean = true;
  p: number = 1;
  total: number = 0;

  params: any = {
    search: '',
    time_zone: '',
    time_zone_name: '',
    size: 10,
    page: 1
  };

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/' },
    { title: 'Bets Settlement', path: '/sports/bet-settlement' },
  ];

  constructor(private sportService: SportService,
    public superAdminAuthService: SuperAdminAuthService,
    public adminAuthService: AdminAuthService,
  private thirdPartyAPIService: ThirdPartyAPIService) {
    
  }

  ngOnInit(): void {
    // this.getBets();
    this.filterSelectTimeZone(TIMEZONE[0].value);
  }

  getBets() {
    // console.log(this.params);
    this.sportService.getBets(this.params).subscribe((res: any) => {
      this.bets = res.record.data;
      this.total = res.record.total;
    });  
  }

  filter(evt: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getBets();
  }
  
  pageChanged(page: number) {
    $('.cst-check').prop('checked', false);
    
    this.params = { ...this.params, page };
    this.getBets();
  }

  resetFilter() {
    this.p = 1;

    setTimeout(() => {
      $("#time_zone").val("").trigger('change');
    }, 100);

    this.params = {
      time_zone: '',
      time_zone_name: '',
      search: '',
      size: 10,
      page: 1
    };

    this.getBets();
  }

  selectAllBets(event: any) {
    if (event.checked) {
      $('.cst-check').prop('checked', true);
      this.selectedBets = [...this.selectedBets, ...this.bets];
    } else {
      $('.cst-check').prop('checked', false);
      this.selectedBets = [];
    }
 }
  
  selectBet(event: any, bet: any) {

    if (event.checked) {
      this.selectedBets.push(bet);
    } else {
      this.selectedBets = this.selectedBets.filter(f => f.id != bet.id );
    }
    
  }
  settlementCancel(){
    this.selectedBets = [];
    $('.cst-check').prop('checked', false);
    this.data.otp_token = '';
    this.isOtp =false;
    this.loader = false;
    this.IsSaveSettlement =true;
    this.IsSaveSettlementtable =true;
  }


  settlementOtp() {


    const bets = this.selectedBets.map(b => {
      return {
        Id: b.bet_id,
        couponId: b.betslip.coupon_id,
        Settlement: this.settlement
      };
    });
    this.data.bets = bets;



    this.loader = true;
    this.sportService.setSettlementOtp().subscribe((res: any) => {
      toastr.success(res.message);
      this.data.otp_token = res.record.otp_token;
      this.loader = false;
      this.IsSaveSettlement =false;
      this.IsSaveSettlementtable =false;
      this.isOtp =true;
     });

  }



  saveSettlement() {

    if(!this.data.otp){
      toastr.error("OTP can not blank");
      return ;
    }else {
      this.loaderOtpSubmit = true;
      const bets = this.selectedBets.map(b => {
        return {
          Id: b.bet_id,
          couponId: b.betslip.coupon_id,
          Settlement: this.settlement
        };
      });
      this.data.bets = bets;


      // this.thirdPartyAPIService.setSettlement(bets).subscribe((res: any) => {

      this.sportService.setSettlement(this.data).subscribe((res: any) => {

        this.loaderOtpSubmit = false;
        this.isOtp = false;
        this.IsSaveSettlement = true;
        this.IsSaveSettlementtable = true;
        this.data.otp = '';
        this.selectedBets = [];
        toastr.success('Settlement updated successfully.');
        $('.cst-check').prop('checked', false);
        this.getBets();
      }, err => {
        this.loaderOtpSubmit = false;
      });
    }
  }

  filterSelectTimeZone(evt: any) {
        this.p = 1;
        
        let timeZoneValue = '';
      
        if(evt){
            timeZoneValue=TIMEZONE.filter(element => element.value === evt)[0].zonename;
        }
        if(timeZoneValue) {
            this.zone = TIMEZONE.filter(element => element.value === evt)[0].name;
            this.params = {...this.params, page: this.p, time_zone:evt, time_zone_name: timeZoneValue};
        }else{
            this.zone = '';
            this.params = {...this.params, page: this.p, time_zone:evt};
        }

        if(evt)
        this.getBets();
    }

}
