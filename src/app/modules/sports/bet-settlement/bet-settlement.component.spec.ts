import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BetSettlementComponent } from './bet-settlement.component';

describe('BetSettlementComponent', () => {
  let component: BetSettlementComponent;
  let fixture: ComponentFixture<BetSettlementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BetSettlementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BetSettlementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
