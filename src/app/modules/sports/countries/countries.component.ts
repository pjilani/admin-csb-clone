import { Component, OnInit } from '@angular/core';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
import { SportService } from '../sport.service';
import { PageSizes } from 'src/app/shared/constants';

declare const $: any;

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.scss']
})

export class CountriesComponent implements OnInit {

  pageSizes: any[] = PageSizes;
  countries: any[] = [];
  countriesAllList: any[] = [];

  sports: any[] = [];

  p: number = 1;
  total: number = 0;

  params: any = {
    size: 10,
    page: 1,
    sport_id: '',
    location_ids:'',
    location_id:''
  };

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Countries', path: '/sports/countries' },
  ];

  isLoader:boolean = false;
  firstTimeApiCall = false;

  constructor(private sportService: SportService,
    public superAdminAuthService: SuperAdminAuthService) { }

  ngOnInit(): void {
    this.getCountries();
    this.getAllSports();
    this.getAllCountries();
  }

  getCountries() {
    this.isLoader = true
    this.firstTimeApiCall = true;
    this.sportService.getAdminCountries(this.params).subscribe((res: any) => {
      this.countries = res.record.data;
      this.total = res.record.total;
      this.isLoader = false;
    },(error:any) => {
      this.isLoader = false

    });
  }

  getAllSports() {
    this.sportService.getAdminSportsCountries().subscribe((res: any) => {
      this.sports = res.record;
    });
  }

  getAllCountries(location_id='') {
    this.p = 1;
    if(location_id!=''){
      this.params = { ...this.params, page: this.p, location_id:location_id };
    }
    this.sportService.getAdminAllCountries( this.params).subscribe((res: any) => {
      this.countriesAllList = res.record;
    });
  }

  filter(evt: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getCountries();
  }

  sportFilter(sport_id: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p, location_ids:sport_id };
    this.getAllCountries();
    this.getCountries();
  }

  CountriesFilter(location_id: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p, location_id:location_id };

    this.sportService.getAdminCountries(this.params).subscribe((res: any) => {
      this.countries = res.record.data;
      this.total = res.record.total;
    });
  }

  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getCountries();
  }

  resetFilter() {
    setTimeout(() => {
      $("#sport_id").val("").trigger('change');
    }, 100);

    this.p = 1;
    this.params = {
      size: 10,
      page: 1,
      sport_id: ''
    };

    this.getCountries();
  }

}
