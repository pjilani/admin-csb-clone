import { Component, OnInit } from '@angular/core';
import { SuperAdminAuthService } from '../../super-admin/services/super-admin-auth.service';
import { SportService } from '../sport.service';
import { PageSizes } from 'src/app/shared/constants';
// import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { AdminAuthService } from '../../admin/services/admin-auth.service';
import { TenantService } from '../../tenant/tenant.service';
import { environment } from 'src/environments/environment';
import { generateApiUrl } from 'src/app/services/utils.service';

declare const $: any;
declare const toastr: any;

@Component({
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.scss']
})

export class MatchComponent implements OnInit {

  searchLeaguesUrl: string = '';
  
  pageSizes: any[] = PageSizes;
  countries: any[] = [];
  leagues: any[] = [];
  
  matches: any[] = [];
  markets: any[] = [];

  sports: any[] = [];

  allTenants: any[] = [];
  
  match: any;

  p: number = 1;
  total: number = 0;

  params: any = {
    size: 10,
    page: 1,
    sport_id: '',
    status: '',
    search: '',
    country: '',
    enabled: '',
    league_id: '',
    tenant_id:''
  };

  breadcrumbs: Array<any> = [
    { title: 'Home', path: '/super-admin' },
    { title: 'Matches', path: '/sports/matches' },
  ];

  isLoader:boolean = false;
  firstTimeApiCall = false;

  constructor(private sportService: SportService,
    // private route: ActivatedRoute,
    public superAdminAuthService: SuperAdminAuthService,
    private tenantService: TenantService,
    public adminAuthService: AdminAuthService,) {

    // const league_id = this.route.snapshot.queryParams['league_id'];
    // this.params = { ...this.params, league_id: league_id ? league_id : '' };
  }

  ngOnInit(): void {
    
    this.getAllCountries();
    
    if (this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.getTenantsAll();
    } else {

      const state = history.state;

      if (state && state.league_id && state.league_name) { 
        this.setStateData();
      } else {
        // this.getMatches();
      }
      
      
      this.getAllSport(this.params.tenant_id);
      // this.getAllLeagues();
      this.searchLeaguesUrl = generateApiUrl() + `admin/sports/leagues/search`;
    }
  }

  setStateData() {
    const state = history.state;
        
      if (state && state.league_id && state.league_name) {
        
        setTimeout(() => {
          const newOption = $("<option selected='selected'></option>").val(state.league_id).text(state.league_name); 
          $("#tournament").append(newOption).trigger('change');
        }, 100);

      }
  }

  getTenantsAll() {
    this.tenantService.getTenantsAll().subscribe((res: any) => {
      this.allTenants = res.record;
      // this.tenantFilter(this.allTenants[0].id);

      const state = history.state;
      if (state && state.league_id && state.league_name) {
        this.setStateData();
        
        // setTimeout(() => {
        //   $("#tenant_id").val(state.tenant_id).trigger('change');
        // }, 100);

        this.params = { ...this.params, tenant_id: state.tenant_id, sport_id: '' };

      } else {
        this.params = { ...this.params, tenant_id: this.allTenants[0].id, league_id: '', sport_id: '' };
        // this.getMatches();
      }
      
      this.getAllSport(this.allTenants[0].id);

      this.searchLeaguesUrl = generateApiUrl() + `super/admin/sports/leagues/search?tenant_id=${this.allTenants[0].id}`;
    });
  }

  tenantFilter(tenant_id: any) {
    // this.p = 1;

    if (tenant_id) {
      this.params = { ...this.params, tenant_id, league_id: '', sport_id: '' };
      
      $("#tournament").val("").trigger('change');
      $("#sport_id").val("").trigger('change');

      // this.getAllLeagues(tenant_id);
      this.getAllSport(tenant_id);
      // this.getMatches();
    }
  }
  
  getAllSport(tenant_id: any) {
    const params = { ...this.params, page: this.p, tenant_id, league_id: '', sport_id: '' };
    this.sportService.getTenantAllSports(params).subscribe((res: any) => {
      this.sports = res.record;
    });  
  }

  getAllCountries() {
    this.sportService.getAdminAllCountries().subscribe((res: any) => {
      this.countries = res.record;
    });
  }

  // getAllLeagues(tenant_id?: any) {
  //   this.sportService.getAdminAllLeagues({ tenant_id: this.allTenants.length > 0 ? tenant_id : '' }).subscribe((res: any) => {
  //     this.leagues = res.record;
  //   });
  // }

  sportFilter(sport_id: any) {
    // this.p = 1;
    this.params = { ...this.params, sport_id };
    // if (sport_id) { 
    //   this.getMatches(); 
    // }
  }

  getMatches() {
    this.isLoader = true
    this.firstTimeApiCall = true;
    this.sportService.getAdminMatches(this.params).subscribe((res: any) => {      
      this.matches = res.record.data;      
      this.total = res.record.total;
      this.isLoader = false
    },(error:any) => {
      this.isLoader = false

    });  
  }
  getMarket(id:any){
    this.markets = this.matches.filter((el: any) => el?.id == id);
    this.markets = JSON.parse(this.markets[0]?.market)
         
  }

  selectCountry(country: any) {
    // this.p = 1;
    this.params = { ...this.params, country};
    // if (country) { 
    //   this.getMatches();
    // }
  }

  selectTournament(league_id: any) {
    // this.p = 1;
    this.params = { ...this.params, league_id };
    // if (league_id) { 
    //   this.getMatches();
    // }
  }

  filter(evt: any) {
    this.p = 1;
    this.params = { ...this.params, page: this.p };
    this.getMatches();
  }
  
  pageChanged(page: number) {
    this.params = { ...this.params, page };
    this.getMatches();
  }

  resetFilter() {
    this.p = 1;
    
    setTimeout(() => {
      $("#country").val("").trigger('change');
      $("#tournament").val("").trigger('change');
      $("#sport_id").val("").trigger('change');
      $("#tenant_id").val(this.allTenants.length > 0 ? this.allTenants[0].id : '').trigger('change');
    }, 100);

    // const league_id = this.route.snapshot.queryParams['league_id'];

    this.params = {
      size: 10,
      page: 1,
      sport_id: '',
      status: '',
      search: '',
      country: '',
      enabled: '',
      league_id: '',
      tenant_id: this.allTenants.length > 0 ? this.allTenants[0].id : ''
    };

    this.getMatches();
  }

  matchDetails(match: any) {
    this.match = match;
  }

  updateMatchStatus(match: any, status: number) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You wont to ${status === 0 ? 'Disabled' : 'Enabled'} it!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, ${status === 0 ? 'Disabled' : 'Enabled'} it!`
    }).then((result) => {
      if (result.isConfirmed) {

         if (status === 0) {

          this.sportService.updateAdminMatchDisabled({ id: match.fixture_id }).subscribe((res: any) => {
            toastr.success(res.message || 'Match Disabled successfully');
            this.matches = this.matches.map((f: any) => {
              if (f.id == match.id) {
                f.blocked = res.record;
              }
              return f;
            });
          });
          
        } else {

          this.sportService.updateAdminMatchEnabled({ id: match.blocked.id }).subscribe((res: any) => {
            toastr.success(res.message || 'Match Enabled successfully');
            this.matches = this.matches.map((f: any) => {
              if (f.id == match.id) {
                f.blocked = null;
              }
              return f;
            });
          });

        }
        
      }
    });
    
  }

}
