import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { ComponentsModule } from 'src/app/shared/components/components.module';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { SportComponent } from './sport/sport.component';
import { CountriesComponent } from './countries/countries.component';
import { TournamentComponent } from './tournament/tournament.component';
import { MatchComponent } from './match/match.component';
import { MarketComponent } from './market/market.component';
import { PipeModule } from 'src/app/pipes/pipes.module';
import { BetSettlementComponent } from './bet-settlement/bet-settlement.component';
import { SuperAdminAuthGuard } from 'src/app/guards';

const sportsRoutes: Routes = [
  { path: '', component: SportComponent },
  { path: 'countries', component: CountriesComponent },
  { path: 'tournaments', component: TournamentComponent },
  { path: 'markets', component: MarketComponent },
  { path: 'matches', component: MatchComponent },
  { path: 'bet-settlement', component: BetSettlementComponent, canActivate: [ SuperAdminAuthGuard ] },
];
@NgModule({
  declarations: [
    SportComponent,
    CountriesComponent,
    TournamentComponent,
    MatchComponent,
    MarketComponent,
    BetSettlementComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(sportsRoutes),
    ComponentsModule,
    DirectivesModule,
    PipeModule,
    SharedModule,
  ]
})

export class SportsModule { }
