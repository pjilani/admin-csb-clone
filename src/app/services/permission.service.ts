import { Injectable } from '@angular/core';
import { SuperAdminAuthService } from '../modules/super-admin/services/super-admin-auth.service';
import { AdminAuthService } from '../modules/admin/services/admin-auth.service';

@Injectable({
  providedIn: 'root'
})
export class PermissionService {

  permissions:any={};

  constructor(
    private superAdminAuthService: SuperAdminAuthService,
    private adminAuthService: AdminAuthService) {
      this.adminAuthService.adminPermissions.subscribe(res => {
        if(res){
          this.permissions = res;
        }  
      })
     }

  checkPermission(moduleName: string, permission: string): boolean {

    if((this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0)){
      return true
    }

    // if (Object.keys(this.permissions).length === 0) {
    //   return true; 
    // }
    
    if (moduleName in this.permissions) {
      return this.permissions[moduleName].includes(permission);
    } else {
      return false;
    }
  }
}
