import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable()

export class ThirdPartyAPIService {

  constructor(private http: HttpClient) { }

  setSettlement(bets: any[]) {
    // console.log('bets in ', bets);
    // console.log(environment.NODE_API+`betting/admin/settle-bet/`);
    const betsParam = {
      "bets" : bets
    }
    return this.http.post(environment.NODE_API+`betting/admin/settle-bet/`, betsParam);
    // return this.http.post(`http://54.89.78.143:8006/api/admin/settle-bet/`, betsParam);
  }

  resetEvent() {
    return this.http.patch(environment.NODE_API_TWO + `l-sports/reset-events`, {});
  }

}
