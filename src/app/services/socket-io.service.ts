import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AdminAuthService } from '../modules/admin/services/admin-auth.service';
import { NotificationsService } from '../modules/admin/admin-modules/notifications/notifications.service';
import Echo from 'laravel-echo';
// import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
@Injectable()

export class SocketIOService {

  echo: any;
  echoRun: boolean = false;

  constructor(
    // private router: Router,
    private adminAuthService: AdminAuthService,
    private AppComponent: AppComponent,
    private notificationsService: NotificationsService
  ) {
    
    // socket initialize start
    
    try {
      this.echo = new Echo({
          broadcaster: 'socket.io',
          host: environment.SOCKET_URL
      });
      
      this.echo.connector.socket.on('connect', () => {
          console.log('CONNECTED');
      });

      this.echo.connector.socket.on('reconnecting', () => {
          console.log('CONNECTING');
      });

      this.echo.connector.socket.on('disconnect', () => {
          console.log('DISCONNECTED');
      });

      this.echoRun = true;
        
    } catch (e) {
        console.log('err ', e);
    }

    // socket initialize end

    if (this.adminAuthService.adminTokenValue && this.adminAuthService.adminTokenValue.length > 0) {

      this.adminAuthService.adminUser.subscribe((admin: any) => {

        const listen = `AdminEvent_${admin.id}`;

          if (admin && admin.id && this.echoRun) {

            // this.echo.connector.socket.on('admin-channel.AdminEvent5', (data: any) => {
            //     console.log('1', data);
            // });

            // this.echo.connector.socket.on('.AdminEvent5', (data: any) => {
            //     console.log('2', data);
            // });

            this.echo.connector.socket.on(listen, (channel: any) => {
              //console.log('channel name ', channel);
            });

            // this.echo.join('admin-channel').joining((data: any) => {
            //     console.log('joining', data);
            // }).leaving((data: any) => {
            //     console.log('leaving', data);
            // });

            this.echo.channel('admin-channel').listen(`.${listen}`, (data: any) => {
              //console.log('From laravel echo: ', data);
              this.AppComponent.hideLoader = true;
              this.notificationsService.getUnReadNotification().subscribe();
              // if (router.url === '/notifications') {
                // this.notificationsService.getNotification().subscribe();
              // }
            });

            // console.log(this.echo);

        }

      });

    }

  }

}
