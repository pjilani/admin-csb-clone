import { environment } from "src/environments/environment";
import { Roles } from "../shared/constants";

export function encryptPassword(password: string): string {
  return btoa(password);
}

export function truncateString(txt: string, len: number): string {
  return txt.length > len ? txt.substring(0, len)+'...' : txt;
}

export function getRole(role: number) {
  return Roles.find(f => f.value == role )?.name;
}

export function generateApiUrl(){
    const hostNameArray = ["localhost"]
    const currentHostname: string = window.location.hostname;
    // const currentHostname: string = 'iadmin.lionplay.co';

    if (!hostNameArray.includes(currentHostname)) {
        if (currentHostname == "staging.8dexsuperadmin.com") {    //check for super-admin staging
            const hostnameParts: string[] = currentHostname.split('.');
            hostnameParts[0] = 'stage-adminapi';
            const modifiedHostname: string = hostnameParts.join('.');
            const modifiedURL: string = window.location.protocol + '//' + modifiedHostname + '/' + environment.API_URL;
            return modifiedURL
        }
      const hostnameParts: string[] = currentHostname.split('.');

      if(hostnameParts.length > 2){
        hostnameParts[0] = 'adminapi';
      }
      else{
        hostnameParts.unshift('adminapi');
      }
      const modifiedHostname: string = hostnameParts.join('.');
      const modifiedURL: string = window.location.protocol + '//' + modifiedHostname +'/'+ environment.API_URL;
      return modifiedURL
    }else{
      return environment.API_URL
    } 
  
}
