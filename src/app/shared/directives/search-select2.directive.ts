import { Directive, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { AdminAuthService } from 'src/app/modules/admin/services/admin-auth.service';
import { SuperAdminAuthService } from 'src/app/modules/super-admin/services/super-admin-auth.service';

declare const $: any;

@Directive({ selector: '[search_select2]' })

export class SearchSelect2Directive implements OnInit, OnChanges {

  @Input() url: string = '';
  @Input() placeholder: string = '';
  @Output() onSelect: EventEmitter<string> = new EventEmitter();

  constructor(private el: ElementRef,
      private adminAuthService: AdminAuthService,
    public superAdminAuthService: SuperAdminAuthService) { }

    ngOnInit(): void {  }
  
  ngOnChanges(changes: SimpleChanges): void {
    if (changes?.url?.currentValue) {
      this.url = changes.url.currentValue;

      const that = this;

      let Authorization = 'Bearer ' + this.adminAuthService.adminTokenValue;

      if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
        Authorization = 'Bearer ' + this.superAdminAuthService.superAdminTokenValue;
      } else {
        Authorization = 'Bearer ' + this.adminAuthService.adminTokenValue;
      }
        
      $(this.el.nativeElement).select2({
        minimumInputLength: 3,
        allowClear: false,
        placeholder: this.placeholder || 'Search',
        ajax: {
          url: this.url, //URL for searching companies
          dataType: "json",
          delay: 200,
          headers: { Authorization },
          data: function (params: any) {
              return {
                search: params.term, //params send to companies controller
              };
          },
          processResults: function (data: any) {
              return {
                results: $.map(data.record, function (obj: any, index: any) {
                  return { id: obj.id, text: obj.text };
                })
              };
          },
          cache: true
      },

      });

      $(this.el.nativeElement).on("change", function (e: any) { 
        that.onSelect.emit($(e.currentTarget).val());
      });
      
    }
  }

}
