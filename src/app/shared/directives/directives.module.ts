import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DaterangePickerDirective, OutsideClickDirective, RemoveHost, Select2Directive, 
  DatePickerDirective, 
  DaterangeTimePickerDirective} from './index';
import { SearchSelect2Directive } from './search-select2.directive';
import { DatatableDirective } from './datatable.directive';
import { DigitOnlyDirective } from './digit-only.directive';
import { DaterangeTimePicker2Directive } from './daterangetime-picker2.directive';

@NgModule({
  
  imports: [
    CommonModule,
  ],

  declarations: [
    RemoveHost, OutsideClickDirective, Select2Directive, DaterangePickerDirective, DatePickerDirective, SearchSelect2Directive,
    DatatableDirective, DaterangeTimePickerDirective, DigitOnlyDirective,
    DaterangeTimePicker2Directive

  ],

  exports: [ 
    RemoveHost, OutsideClickDirective, Select2Directive, DaterangePickerDirective, DatePickerDirective, SearchSelect2Directive,
    DatatableDirective, DaterangeTimePickerDirective,
    DaterangeTimePicker2Directive,
    DigitOnlyDirective
  ]
  
})

export class DirectivesModule { }
