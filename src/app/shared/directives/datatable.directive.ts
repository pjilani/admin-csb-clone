import { AfterViewInit, Directive, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

declare const $: any;

@Directive({ selector: '[datatable]' })

export class DatatableDirective implements OnChanges {

  @Input() params: any = {
          paging: false,
          searching: false,
          ordering: true,
          info: false,
          responsive: false,
          lengthChange: false,
          autoWidth: false,
          buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"]
      };

  constructor(private el: ElementRef) { }
  
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.params.currentValue) {
      setTimeout(() => {
        $(this.el.nativeElement).DataTable(changes.params.currentValue).buttons().container().appendTo('#dbuttons');
      }, 500);  
    }
  }
 
}
