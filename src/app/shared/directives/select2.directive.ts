import { Directive, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

declare const $: any;

@Directive({ selector: '[select2]' })

export class Select2Directive implements OnInit, OnChanges {

  @Input() value: any;
  @Output() onSelect: EventEmitter<string> = new EventEmitter();

  constructor(private el: ElementRef) {  }

    ngOnInit(): void {

      const that = this;
      $(this.el.nativeElement).select2();

      $(this.el.nativeElement).on("change", function (e: any) { 
        that.onSelect.emit($(e.currentTarget).val());
      });

    }

    ngOnChanges(changes: SimpleChanges): void {
      // if(changes?.value?.currentValue) {
      //   $(this.el.nativeElement).val(changes.value.currentValue).trigger('change');
      // }
    }

}
