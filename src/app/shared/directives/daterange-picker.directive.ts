import { Directive, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { dateFormat } from '../constants';

declare const $: any;

@Directive({ selector: '[daterange_picker]' })

export class DaterangePickerDirective implements OnInit {

  @Input() timePicker: boolean = false;
  @Input() format: string = dateFormat;
  @Output() onSelect: EventEmitter<any> = new EventEmitter();

  constructor(private el: ElementRef) {  }

    // ngOnInit(): void {

    //   const that = this;

    //   $(this.el.nativeElement).daterangepicker({
    //     timePicker: that.timePicker,
    //     // timePickerIncrement: 30,
    //     locale: {
    //       format: that.format
    //     },
    //   }, function(start: any, end: any) {
    //       const start_date = start.format(that.format);
    //       const end_date = end.format(that.format);
    //       that.onSelect.emit({ start_date, end_date });
    //   });

    // }

    ngOnInit(): void {
      const that = this;
     
      $(this.el.nativeElement).daterangepicker(
      {
      timePicker: that.timePicker,
      locale: {
      format: that.format,
      },
      },
      function (start: any, end: any) {
     
      }
      );
     
      $('#time_period').on(
      'apply.daterangepicker',
      function (ev: any, picker: any) {
      const start_date = picker.startDate.format(that.format);
      const end_date = picker.endDate.format(that.format);
      that.onSelect.emit({ start_date, end_date });
      }
      );
      }

}
