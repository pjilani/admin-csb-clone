import { Directive, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { dateFormat } from '../constants';

declare const $: any;

@Directive({ selector: '[date_picker]' })

export class DatePickerDirective implements OnInit {

  @Input() format: string = dateFormat;
  @Output() onSelect: EventEmitter<any> = new EventEmitter();

  constructor(private el: ElementRef) { 
    
   }

    ngOnInit(): void {

      const that = this;

      $(this.el.nativeElement).daterangepicker({
        locale: {
            format: that.format,
            maxDate: new Date
        },
        singleDatePicker: true,
        showDropdowns: true
      }, function(start: any) {
          that.onSelect.emit(start.format(that.format));
      });

    }

}
