import { Directive, HostListener, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[digitOnly]'
})
export class DigitOnlyDirective {
  @Input() decimalPlaces: number = 2; // Number of decimal places allowed

  constructor(private el: ElementRef) {}

  @HostListener('input', ['$event']) onInput(event: InputEvent): void {
    const inputElement = this.el.nativeElement as HTMLInputElement;
    let inputValue: string = inputElement.value;

    // Remove leading zeroes
    inputValue = inputValue.replace(/^0+/, '');

    // Allow only digits, decimal point, and a single decimal point
    inputValue = inputValue.replace(/[^0-9.]/g, '');

    // Allow only one decimal point
    const decimalCount = (inputValue.match(/\./g) || []).length;
    if (decimalCount > 1) {
      inputValue = inputValue.replace(/\.+/, '.');
    }

    // Check if the input contains a decimal point
    const decimalIndex = inputValue.indexOf('.');
    if (decimalIndex !== -1) {
      const integerPart = inputValue.substring(0, decimalIndex);
      const decimalPart = inputValue.substring(decimalIndex + 1);

      // Allow only up to 2 digits after the decimal point
      if (decimalPart.length > 2) {
        inputValue = `${integerPart}.${decimalPart.substring(0, 2)}`;
      }
    }

    // Restrict the maximum length to 7 characters
    if (inputValue.length > 7) {
      inputValue = inputValue.substring(0, 7);
    }

    // Set the sanitized input value back to the input element
    inputElement.value = inputValue;
  }
}
