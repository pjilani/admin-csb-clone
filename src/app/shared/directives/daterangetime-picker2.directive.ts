import { Directive, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { dateFormat } from '../constants';
import * as moment from 'moment';

declare const $: any;

@Directive({ selector: '[daterangetime_picker2]' })

export class DaterangeTimePicker2Directive implements OnInit {

  @Input() timePicker: boolean = false;
  @Input() dateLimit: number|null = null;
  @Input() format: string = dateFormat;
  @Input() maxDate: string | null = null; 
  @Input() minDate: string | null = null; 
  @Output() onSelect: EventEmitter<any> = new EventEmitter();

  constructor(private el: ElementRef) {  }

  ngOnInit(): void {
    const that = this;

    const options:any = {
      timePicker: true,
      timePickerSeconds: true,
      locale: {
        format: that.format,
      },
      timePickerIncrement: 1,
      timePicker24Hour: true,
    }

    if (this.dateLimit !== null) {
      options.dateLimit = {
        days: this.dateLimit
      };
    }
    if (this.maxDate !== null) {
      options.maxDate = this.maxDate;
      options.startDate = moment.min(moment(), moment(new Date(this.maxDate).setHours(0,0,0,0)));
    }

    if (this.minDate !== null) {
      options.minDate = this.minDate;
      options.startDate = moment.min(moment(), moment(new Date(this.minDate).setHours(0,0,0,0)));
    }
  
    $(this.el.nativeElement).daterangepicker(
      options, function (start: any, end: any) {
        const start_date = start.format(that.format);
        const end_date = end.format(that.format);
        that.onSelect.emit({ start_date, end_date });
      });
  
    $('#time_period_actioned_at').on(
      'apply.daterangepicker',
      function (ev: any, picker: any) {
        const start_date = picker.startDate.format(that.format);
        const end_date = picker.endDate.format(that.format);
        that.onSelect.emit({ start_date, end_date });
      }
    );
  }

}