import { Component, Input, OnInit } from '@angular/core';
import { Location } from '@angular/common';
@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styles: [`
    .back { margin-top: -3px; }
  `]
})

export class BreadcrumbComponent implements OnInit {

 @Input() title: string = '';
 @Input() breadcrumbs: Array<any> = [];

  constructor(private location: Location,) { }
  
  ngOnInit(): void { }
  
  goBack() {
    this.location.back();
  }

}
