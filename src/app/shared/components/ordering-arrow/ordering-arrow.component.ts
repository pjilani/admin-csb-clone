import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-ordering-arrow',
  templateUrl: './ordering-arrow.component.html'
})

export class OrderingArrowComponent implements OnInit {

 @Input() column: string = '';
 @Input() params: any;
 @Output() changeOrder: EventEmitter<any> = new EventEmitter();
  
  constructor() { }
  
  ngOnInit(): void { }
  
  setOrder(order: string) {
    this.changeOrder.emit({ column: this.column, order });
  }

}
