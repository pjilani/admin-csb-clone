import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { RouterModule } from '@angular/router';
import { ThemeComponent } from './theme/theme.component';
import { FormsModule } from '@angular/forms';
import { DirectivesModule } from '../directives/directives.module';
import { OrderingArrowComponent } from './ordering-arrow/ordering-arrow.component';

@NgModule({
  
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    DirectivesModule
  ],

  declarations: [
    BreadcrumbComponent, ThemeComponent, OrderingArrowComponent
  ],

  exports: [ 
    BreadcrumbComponent, ThemeComponent, OrderingArrowComponent
  ]
  
})

export class ComponentsModule { }
