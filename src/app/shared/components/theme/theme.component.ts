import { AfterViewInit, Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Constants, ThemeFonts } from '../../constants';

declare const $: any;

@Component({
  selector: 'app-theme',
  templateUrl: './theme.component.html',
  styleUrls: ['./theme.component.scss']
})

export class ThemeComponent implements OnInit, OnDestroy, OnChanges {

 @ViewChild('form', { static: true }) ngForm!: NgForm;

 themeFonts = ThemeFonts;
 disable_theme:string = 'Custom';
 formChangesSubscription: any;

//  @Input() theme: any = { ...Constants.INIT_THEME };
  setTheme:any;
  firstChange:any = 1
  @Input() theme: any = {};
  @Input() selectedLayout: any = Constants.defaultLayout;

 @Output() changeTheme : EventEmitter<any> = new EventEmitter();

  constructor() { }
  
  ngOnInit(): void {
    this.formChangesSubscription = this.ngForm.form.valueChanges.subscribe(() => {      
      this.changeTheme.emit(this.theme);
    });
  }

  ngOnChanges(changes: SimpleChanges): void {

    if(this.firstChange < 3){
      let type = changes?.theme?.currentValue.palette.type;
      this.setTheme = {...this.theme};
      if(changes?.theme?.currentValue.name == 'Custom')
      this.setTheme = {...this.setTheme,type};
      this.firstChange++;
    }  


    if(changes?.theme?.currentValue) {
      const ntheme = changes?.theme?.currentValue;
      // console.log(ntheme);
      $(`#font_family`).val(ntheme.typography.font_family).trigger('change');
    }
  }

  changeThemeType(theme_type:string){
    if(theme_type == 'light'){
      if(this.setTheme.type === 'light'){
        this.theme = {...this.setTheme};
      }else{
        this.theme = { ...Constants.INIT_THEME };
      }
    }else{
      
      if(this.setTheme.type === 'dark'){
        this.theme = {...this.setTheme};
      }else{
        this.theme = { ...Constants.INIT_DARK_THEME };
      }
    }
    this.theme.palette.type = theme_type;
    this.changeTheme.emit({...this.theme});
  
}


  selectFont(font_family: string) {
    this.theme.typography.font_family = font_family;
    this.changeTheme.emit({...this.theme});

  }

  ngOnDestroy() {
    this.formChangesSubscription.unsubscribe();
  }

}
