import { Component, OnInit } from '@angular/core';
// import { Router } from '@angular/router';
import { NotificationsService } from 'src/app/modules/admin/admin-modules/notifications/notifications.service';
import { AdminAuthService } from 'src/app/modules/admin/services/admin-auth.service';
import { SuperAdminAuthService } from 'src/app/modules/super-admin/services/super-admin-auth.service';
import { LayoutService } from './../layout.service';
import { PermissionService } from 'src/app/services/permission.service';
import { CurrenciesService } from 'src/app/modules/super-admin/super-admin-modules/currencies/currencies.service';

declare const $: any;
 declare const introJs: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})

export class HeaderComponent implements OnInit {

  currencies:any = []
  currentDate!:Date

  constructor(
    // private router: Router,
    private layoutService: LayoutService,
    public adminAuthService: AdminAuthService,
    public superAdminAuthService: SuperAdminAuthService,
    public notificationsService: NotificationsService,
    public permissionService: PermissionService,
    public currencyService: CurrenciesService
    ) {  }

  ngOnInit() {
    if(this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0) {
      this.currentDate = new Date();
      // this.superAdminAuthService.getPersonalDetails().subscribe();
      this.currencyService.getCurrencies().subscribe((res: any)=>{
        this.currencies = res.record;
      })
    } else {
      if(this.permissionService.checkPermission('notifications', 'R')){
        this.notificationsService.getUnReadNotification().subscribe();
      }
      this.adminAuthService.adminUser.subscribe((user: any) => {
      
         const intro = localStorage.getItem('intro');

         if (user && user.login_count === 1 && !intro) {
           setTimeout(() => {
             localStorage.setItem('intro', 'Complated');
             introJs().start();
           }, 1000);
         }
        
      });
    }
  }

  toggleSidebar() {
    if($('body').hasClass('sidebar-collapse')) {
      $('body').removeClass('sidebar-collapse');
      this.layoutService.changeSidebarToggle(true);
    } else {
      this.layoutService.changeSidebarToggle(false);
      $('body').addClass('sidebar-collapse');
    }
  }

  logout() {
    $('#preloader').css('height', '');
    $('#preloader img').show();

    const superAdminToken = this.superAdminAuthService.superAdminTokenValue;

    if (superAdminToken && superAdminToken.length > 0) {
      this.superAdminAuthService.logout().subscribe(() => {
        $('#preloader').css('height', 0);
        $('#preloader img').hide();
      });  
    } else {
      this.adminAuthService.logout().subscribe(() => {
        $('#preloader').css('height', 0);
        $('#preloader img').hide();
      });
    }

  }

  readAll() {
    if (this.notificationsService.unreadNotifications?.count > 0) { 
      this.notificationsService.readAllNotifications().subscribe();
    }
  }

}
