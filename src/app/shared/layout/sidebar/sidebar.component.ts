import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminAuthService } from 'src/app/modules/admin/services/admin-auth.service';
import { SuperAdminAuthService } from 'src/app/modules/super-admin/services/super-admin-auth.service';
import { LayoutService } from './../layout.service';
import { Role } from 'src/app/models';
import { TenantService } from 'src/app/modules/tenant/tenant.service';
import { DomainForDisableModule } from 'src/app/shared/constants';
import { PermissionService } from 'src/app/services/permission.service';

declare const $: any;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})

export class SidebarComponent implements OnInit, AfterViewInit {
  checkSuperAdmin:boolean = false;
  checkAdmin:boolean = false;
  isSubAgentModuleAllowed:boolean = true;
  casinoMenuCheck:boolean = false;
  sportMenuCheck:boolean = false;
  roles: string = localStorage.getItem('roles') || '';
  isAgent: boolean = false;
  isSubAdmin: boolean = false;
  isDepositAdmin: boolean = false;
  isCurrencyHide:boolean = false;
  isWithdrawalAdmin: boolean = false;
  isAllowedModule:any = '';
  DomainForDisableModule:any = DomainForDisableModule;
  wallets: any = [];
  constructor(public router: Router,
    public layoutService: LayoutService,
    public adminAuthService: AdminAuthService,
    public superAdminAuthService: SuperAdminAuthService,
    public tenantService: TenantService,
    public permissionService: PermissionService
  ) { 
    this.isSubAgentModuleAllowed = (localStorage.getItem('allowedModules')?.includes('subAgent') ? true : false);
  }

  ngOnInit() {
    if (this.adminAuthService.adminTokenValue && this.adminAuthService.adminTokenValue.length > 0) {

      if(this.roles) {
        const roles = JSON.parse(this.roles);
        if(roles && roles.findIndex( (role: any) => role === Role.Admin ) == -1 && roles.findIndex( (role: any) => role === Role.Agent ) > -1) {
          this.isAgent = true;
        }
        if(roles && roles.findIndex( (role: any) => role === Role.Admin ) == -1 && roles.findIndex( (role: any) => role === Role.Subadmin ) > -1) {
          this.isSubAdmin = true;
        }
        if(roles && roles.findIndex( (role: any) => role === Role.Admin ) == -1 && roles.findIndex( (role: any) => role === Role.DepositAdmin ) > -1) {
          this.isDepositAdmin = true;
        }
        if(roles && roles.findIndex( (role: any) => role === Role.Admin ) == -1 && roles.findIndex( (role: any) => role === Role.WithdrawalAdmin ) > -1) {
          this.isWithdrawalAdmin = true;
        } 
        // else {
        //   this.tenantService.getAdminTenant().subscribe();
        // }
      this.isAllowedModule = (localStorage.getItem('domain') ? localStorage.getItem('domain') : '');


      } else {
        this.router.navigate(['/login'], { queryParams: { returnUrl: this.router.url } });
      }

    
      

    }

    this.menuList();
  }

  menuList(){
    this.checkAdmin = this.adminAuthService.adminTokenValue && this.adminAuthService.adminTokenValue.length > 0;
    this.checkSuperAdmin = this.superAdminAuthService.superAdminTokenValue && this.superAdminAuthService.superAdminTokenValue.length > 0;
    if(!this.checkSuperAdmin){
      this.adminAuthService.tenantData.subscribe((tenantData:any) => {
        if(tenantData){
          let casino  = tenantData?.menuList.filter((element:string) => {
            if(element === "Casino" || element === 'Live Casino'){return 1;}
            return 0;
          });
          if(casino != ""){
            this.casinoMenuCheck  = true;
          }
          let sport  = tenantData?.menuList.filter((element:string) => {
            if(element === "Exchange" || element === 'Live Bets'){return 1;}
            return 0;
          });
          if(sport != ""){
            this.sportMenuCheck  = true;
          }
        }
      });

      this.adminAuthService.getWalletDetails().subscribe((res:any) => {
        this.wallets = res.record
        for( let i = 0, len = res?.record.length; i < len; i++ ) {
          if( res?.record[i]['currency_name'] === 'chips' ) {
              this.isCurrencyHide = true;
              break;
          }
      }
      })
    }else{
      this.superAdminAuthService.getWalletDetails().subscribe((res:any) => {
        this.wallets = res.record
      })
    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
      $(".nav-main-item").on('click', function(this: any) {
        if($(this).closest('.nav-item').hasClass( "menu-is-opening menu-open" )) {
          $(this).closest('.nav-item').removeClass('menu-is-opening menu-open');
        } else {
          $(this).closest('.nav-item').addClass('menu-is-opening menu-open');
        }
      });
    } , 3000);

    $('.sidebar').overlayScrollbars({
      className: 'os-theme-light',
      sizeAutoCapable: true,
      scrollbars: {
        autoHide: 'l',
        clickScrolling: true
      }
    });

    if(!this.checkSuperAdmin){
      $("#appIcon").attr("href", this.adminAuthService?.tenantDataValue?.logo_url || "favicon.ico");  
    }
    else{
      $("#appIcon").attr("href", './assets/dist/img/s_logo.ico');  
    }
    
  }

  numDigit(num: string, digit: number) {
    return parseFloat(num || '0').toFixed(digit);
  }

  adminReportPermission(report_abbr: string) {
    if (report_abbr && this.adminAuthService.adminReportsValue.includes(report_abbr)) {
      return true;
    } else {
      return false;
    }
  }

  //function to check permissions of module
  checkPermission(moduleName:string, permission:string){
    return this.permissionService.checkPermission(moduleName,permission)
  }

}
