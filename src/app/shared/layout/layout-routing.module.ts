import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonAdminAuthGuard, SuperAdminAuthGuard, SuperAdminCommonAuthGuard, ReportAuthGuard } from 'src/app/guards';
import { LayoutComponent } from './layout.component';
import { BankModule } from '../../modules/tenant-bank/bank.module';

const layoutRoutes: Routes = [
  {
    path: '', 
    component: LayoutComponent,
    children: [
      { 
        path: '', 
        loadChildren: () => import('./../../modules/admin/admin.module').then(m => m.AdminModule),
      },
      { 
        path: 'super-admin', 
        loadChildren: () => import('./../../modules/super-admin/super-admin.module').then(m => m.SuperAdminModule),
        canActivate: [SuperAdminAuthGuard],
        canActivateChild: [ SuperAdminAuthGuard ],
      },
      {
        path: 'agents',
        loadChildren: () => import('./../../modules/agent/agent.module').then(m => m.AgentModule),
      },
      {
        path: 'sub-admins',
        loadChildren: () => import('./../../modules/sub-admin/sub-admin.module').then(m => m.SubAdminModule),
      },
      {
        path: 'cms',
        canActivate: [CommonAdminAuthGuard],
        loadChildren: () => import('./../../modules/cms/cms.module').then(m => m.CmsModule),
      },
      {
        path: 'players',
        loadChildren: () => import('./../../modules/player/player.module').then(m => m.PlayerModule)
      },
      {
        path: 'players-commission',
        loadChildren: () => import('./../../modules/player-commission/player-commission.module').then(m => m.PlayerCommissionModule)
      },
      {
        path: 'tenants',
        loadChildren: () => import('./../../modules/tenant/tenant.module').then(m => m.TenantModule),
      },
      {
        path: 'transactions',
        loadChildren: () => import('./../../modules/transactions/transactions.module').then(m => m.TransactionsModule),
        canActivate: [CommonAdminAuthGuard]
      },
      {
        path: 'bank',
        loadChildren: () => import('./../../modules/tenant-bank/bank.module').then(m => m.BankModule),
        canActivate: [CommonAdminAuthGuard]
      },
      {
        path: 'promo-code',
        loadChildren: () => import('./../../modules/tenant-promo-code/promo.module').then(m => m.PromoModule),
        canActivate: [CommonAdminAuthGuard]
      },
      {
        path: 'reports',
        loadChildren: () => import('./../../modules/reports/reports.module').then(m => m.ReportsModule)
      },
      {
        path: 'active-players',
        canActivate: [CommonAdminAuthGuard],
        loadChildren: () => import('./../../modules/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'sports',
        loadChildren: () => import('./../../modules/sports/sports.module').then(m => m.SportsModule),
        canActivate: [CommonAdminAuthGuard]
      },
      {
        path: 'meta',
        canActivate: [CommonAdminAuthGuard],
        loadChildren: () => import('./../../modules/meta/meta.module').then(m => m.MetaModule),
      },
      {
        path: 'change-password',
        loadChildren: () =>
          import('./../../modules/tenant/change-password/change-password.module').then(
            m => m.ChangePasswordModule
          ),
        canActivate: [CommonAdminAuthGuard],
      },

    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(layoutRoutes)],
  exports: [RouterModule]
})

export class LayoutRoutingModule { }
