export class Constants {
  static defaultLayout: any = "1";

  static get INIT_THEME() {
    return {
      name: "Custom",
      palette: {
        type: "light",
        primary: {
          main: "#3f51b5",
          contrast_text: "#ffffff"
        },
        secondary: {
          main: "#ff4081",
          contrast_text: "#ffffff"
        },
        background: {
          default: "#fafafa",
          paper: "#ffffff",
          sub_navbar: "#ffffff"
        },
        text: {
          primary: "#0d0d0d",
          secondary: "#1a1a1a",
          sub_navbar: "#0d0d0d",
          disabled: "#666666"
        },
        action: {
          selected: "#7a7575"
        },
        alert: {
          text: {
            default: '#ffffff',
            success: '#ffffff',
            error: '#ffffff'
          }
        }
      },
      typography: {
        font_family: "Droid serif"
      }
    }
  };

  static get INIT_DARK_THEME() {
    return {
      name: "Custom",
      palette: {
        type: "dark",
        primary: {
          main: "#00000",
          contrast_text: "#ffffff"
        },
        secondary: {
          main: "#5200e700",
          contrast_text: "#ffffff"
        },
        background: {
          default: "#940001",
          paper: "#634849",
          sub_navbar: "#634849"
        },
        text: {
          primary: "#ffffff",
          secondary: "#1a1a1a",
          sub_navbar: "#ffffff",
          disabled: "#666666"
        },
        action: {
          selected: "#7a7575"
        },
        alert: {
          text: {
            default: '#000000',
            success: '#ffffff',
            error: '#ffffff'
          }
        }
      },
      typography: {
        font_family: "Droid serif"
      }
    }
  };


  static get REPORTS() {
    return [
      { path: '/reports/player-report', report_abbr: 'player_report' },
      { path: '/reports/player-financial-report', report_abbr: 'player_financial_report' },
      { path: '/reports/game-transaction-report', report_abbr: 'game_transaction_report' },
      { path: '/reports/player-ncb-report', report_abbr: 'player_ncb_report' },
      { path: '/reports/tip-report', report_abbr: 'tip_report' },
      { path: '/reports/player-revenue-report', report_abbr: 'player_revenue_report' },
      { path: '/reports/sport-player-revenue-report', report_abbr: 'sport_player_revenue_report' },
      { path: '/reports/agent-revenue-report', report_abbr: 'agent_revenue_report' },
      { path: '/reports/sport-agent-revenue-report', report_abbr: 'sport_agent_revenue_report' },
      { path: '/reports/unified-report', report_abbr: 'unified_transaction_report' },
      { path: '/reports/bonus-status-per-player-report', report_abbr: 'bonus_status_per_player_report' }
    ];
  }

}

export const dateFormat: string = "YYYY-MM-DD HH:mm:ss";

export const ThemeFonts = [
  { value: "'Roboto', 'Helvetica', 'Arial', sans-serif", name: "'Roboto', 'Helvetica', 'Arial', sans-serif" },
  { value: "Droid Sans", name: "Droid Sans" },
  { value: "Droid Serif", name: "Droid Serif" },
  { value: "Lato", name: "Lato" },
  { value: "Lora", name: "Lora" },
  { value: "Montserrat", name: "Montserrat" },
  { value: "Open Sans", name: "Open Sans" },
  { value: "Oswald", name: "Oswald" },
  { value: "PT Sans", name: "PT Sans" },
  { value: "Raleway", name: "Raleway" },
  { value: "Roboto", name: "Roboto" },
  { value: "Slabo 27px", name: "Slabo 27px" },
  { value: "Source Sans Pro", name: "Source Sans Pro" }
];

export const PageSizes = [
  { name: 10 },
  { name: 25 },
  { name: 50 },
  { name: 100 },
  { name: 200 },
  { name: 500 },
];


export const DomainForDisableModule = [
  'www.zovi24.com',
  'www.lionplay.co',
  //Tenant 1 Royal24
  'wiin24.com',
  'www.wiin24.com',
  'royal24.com',
  'www.royal24.com',
  //Tenant 2
  'strikes247.com',
  'www.strikes247.com',
  'crown246.com',
  'www.crown246.com',

];

export const NoEditOptionForTenantCredentails = ['APP_CHIPS_CURRENCY_CODE'];

export const CKEDITOR_CONFIG = {
  toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'blockQuote', 'undo', 'redo' ]
};

export const Roles = [
  { name: 'Owner', value: 1 },
  { name: 'Agent', value: 2 },
  { name: 'Sub Admin', value: 3 },
  { name: 'Deposit Admin', value: 4 },
  { name: 'Withdrawal Admin', value: 5 }
];

export const TransactionTypes = [
  { name :'Bet', title: 'bet', value: 0 },
  { name :'Bet non cash', title: 'bet_non_cash', value: 8 },
  { name :'Deposit', title: 'deposit', value: 3 },
  { name :'Deposit bonus claim', title: 'deposit_bonus_claim', value: 12 },
  { name :'Joining Bonus Claimed', title: 'joining_bonus_claimed', value: 15 },
  { name :'Non cash granted by admin', title: 'non_cash_granted_by_admin', value: 5 },
  { name :'Non cash withdraw by admin', title: 'non_cash_withdraw_by_admin', value: 6 },
  { name :'Non cash bonus claim', title: 'non_cash_bonus_claim', value: 11 },
  { name :'Refund', title: 'refund', value: 2 },
  { name :'Refund non cash', title: 'refund_non_cash', value: 10 },
  { name :'Tip non cash', title: 'tip_non_cash', value: 13 },
  { name :'Tip', title: 'non_cash_grtipanted_by_admin', value: 7 },
  { name :'Win', title: 'win', value: 1 },
  { name :'Withdraw', title: 'withdraw', value: 4 },
  { name :'Win non cash', title: 'win_non_cash', value: 9 },
  { name :'Withdraw cancel', title: 'withdraw_cancel', value: 14 },
  { name :'Failed transaction', title: 'failed', value: 16 },
  { name :'Promo Code Bonus Claimed', title: 'promo_code_bonus_claimed', value: 17 },
  { name :'Commission Received', title: 'commission_received', value: 19 }
]
export const GetAllTransactionTypes = [
  { name :'Bet', title: 'bet', value: 0 },
  { name :'Bet non cash', title: 'bet_non_cash', value: 8 },
  { name :'Deposit', title: 'deposit', value: 3 },
  { name :'Deposit bonus claim', title: 'deposit_bonus_claim', value: 12 },
  { name :'Joining Bonus Claimed', title: 'joining_bonus_claimed', value: 15 },
  { name :'Non cash granted by admin', title: 'non_cash_granted_by_admin', value: 5 },
  { name :'Non cash withdraw by admin', title: 'non_cash_withdraw_by_admin', value: 6 },
  { name :'Non cash bonus claim', title: 'non_cash_bonus_claim', value: 11 },
  { name :'Refund', title: 'refund', value: 2 },
  { name :'Refund non cash', title: 'refund_non_cash', value: 10 },
  { name :'Tip non cash', title: 'tip_non_cash', value: 13 },
  { name :'Tip', title: 'non_cash_grtipanted_by_admin', value: 7 },
  { name :'Win', title: 'win', value: 1 },
  { name :'Withdraw', title: 'withdraw', value: 4 },
  { name :'Win non cash', title: 'win_non_cash', value: 9 },
  { name :'Withdraw cancel', title: 'withdraw_cancel', value: 14 },
  { name :'Failed transaction', title: 'failed', value: 16 },
  { name :'Promo Code Bonus Claimed', title: 'promo_code_bonus_claimed', value: 17 },
  { name :'Commission Received', title: 'commission_received', value: 19 },
  { name :'Exchange place bet non cash debit', title: 'exchange_place_bet_non_cash_debit', value: 20 },
  { name :'Exchange place bet cash debit', title: 'exchange_place_bet_cash_debit', value: 21 },
  { name :'Exchange place bet cash credit', title: 'exchange_place_bet_cash_credit', value: 22 },
  { name :'Exchange refund cancel bet non cash debit', title: 'exchange_refund_cancel_bet_non_cash_debit', value: 23 },
  { name :'Exchange refund cancel bet cash debit', title: 'exchange_refund_cancel_bet_cash_debit', value: 24 },
  { name :'Exchange refund cancel bet non cash credit', title: 'exchange_refund_cancel_bet_non_cash_credit', value: 25 },
  { name :'Exchange refund cancel bet cash credit', title: 'exchange_refund_cancel_bet_cash_credit', value: 26 },
  { name :'Exchange refund market cancel non cash debit', title: 'exchange_refund_market_cancel_non_cash_debit', value: 27 },
  { name :'Exchange refund market cancel cash debit', title: 'exchange_refund_market_cancel_cash_debit', value: 28 },
  { name :'Exchange refund market cancel non cash credit', title: 'exchange_refund_market_cancel_non_cash_credit', value: 29 },
  { name :'Exchange refund market cancel cash credit', title: 'exchange_refund_market_cancel_cash_credit', value: 30 },
  { name :'Exchange settle market cash credit', title: 'exchange_settle_market_cash_credit', value: 31 },
  { name :'Exchange settle market cash debit', title: 'exchange_settle_market_cash_debit', value: 32 },
  { name :'Exchange resettle market cash credit', title: 'exchange_resettle_market_cash_credit', value: 33 },
  { name :'Exchange resettle market cash debit', title: 'exchange_resettle_market_cash_debit', value: 34 },
  { name :'Exchange cancel settled market cash credit', title: 'exchange_cancel_settled_market_cash_credit', value: 35 },
  { name :'Exchange cancel settled market cash debit', title: 'exchange_cancel_settled_market_cash_debit', value: 36 },
  { name :'Exchange deposit bonus claim', title: 'exchange_deposit_bonus_claim', value: 37 }
]
export const LogServices = [
  { name :'spribe' },
  { name :'ezugi' }
]

export const Uuids = [
  { value: "51", name: "Unlimited Blackjack (51)" },
  { value: "blackjack", name: "Lobby Blackback (blackjack)" },
  { value: "roulette", name: "Lobby Roulette (roulette)" },
  { value: "poker", name: "Lobby Poker (poker)" },
  { value: "baccarat", name: "Lobby Baccarat (baccarat)" },
  { value: "andar_bahar", name: "Lobby Andar Bahar (andar_bahar)" },
  { value: "lottery", name: "Lobby Lottery (lottery)" },
  { value: "lucky7", name: "Lobby Lucky7 (lucky7)" },
  { value: "32cards", name: "Lobby 32Cards (32cards)" },
  { value: "sicbo", name: "Lobby SicBo (sicbo)" },
  { value: "1", name: "Blackjack Gold 5 (1)" },
  { value: "4", name: "Blackjack Gold 4 (4)" },
  { value: "5", name: "Blackjack Platinum 1 (5)" },
  { value: "100", name: "Baccarat (100)" },
  { value: "101", name: "Speed Cricket Baccarat (101)" },
  { value: "120", name: "Baccarat Knock Out (120)" },
  { value: "130", name: "Baccarat Super 6 (130)" },
  { value: "150", name: "Dragon Tiger (150)" },
  { value: "170", name: "Baccarat No Commission (170)" },
  { value: "201", name: "Blackjack 1 (201)" },
  { value: "203", name: "Blackjack Gold 3 (203)" },
  { value: "204", name: "Blackjack Gold 1 (204)" },
  { value: "221", name: "Blackjack 7 (221)" },
  { value: "223", name: "Blackjack Gold 6 (223)" },
  { value: "224", name: "VIP Blackjack with Surrender (224)" },
  { value: "225", name: "Diamond VIP Blackjack (225)" },
  { value: "226", name: "Italian Blackjack (226)" },
  { value: "411", name: "Rumba Blackjack 1 (411)" },
  { value: "412", name: "Rumba Blackjack 2 (412)" },
  { value: "413", name: "Rumba Blackjack 3 (413)" },
  { value: "421", name: "Türkçe Blackjack 1 (421)" },
  { value: "422", name: "Türkçe Blackjack 2 (422)" },
  { value: "425", name: "White Russian Blackjack (425)" },
  { value: "426", name: "Black Russian Blackjack (426)" },
  { value: "501", name: "Turkish Blackjack (501)" },
  { value: "1000", name: "Italian Roulette (1000)" },
  { value: "4151", name: "Mambo Unlimited Blackjack (4151)" },
  { value: "4351", name: "Fiesta BlackJack Unlimited (4351)" },
  { value: "5001", name: "Auto Roulette (5001)" },
  { value: "5051", name: "Unlimited Turkish Blackjack (5051)" },
  { value: "26100", name: "Baccarat Pro 1 (26100)" },
  { value: "26101", name: "Baccarat Pro 2 (26101)" },
  { value: "32100", name: "Casino Marina Baccarat 1 (32100)" },
  { value: "32101", name: "Casino Marina Baccarat 2 (32101)" },
  { value: "32102", name: "Casino Marina Baccarat 3 (32102)" },
  { value: "32103", name: "Casino Marina Baccarat 4 (32103)" },
  { value: "41100", name: "Salsa Baccarat 1 (41100)" },
  { value: "41101", name: "Salsa Baccarat 2 (41101)" },
  { value: "41103", name: "Salsa Baccarat 4 (41103)" },
  { value: "43100", name: "Fiesta Baccarat (43100)" },
  { value: "221000", name: "Speed Roulette (221000)" },
  { value: "221002", name: "Speed Auto Roulette (221002)" },
  { value: "221003", name: "Diamond Roulette (221003)" },
  { value: "221004", name: "Prestige Auto Roulette (221004)" },
  { value: "221005", name: "Namaste Roulette (221005)" },
  { value: "224000", name: "Sic Bo (224000)" },
  { value: "227100", name: "Teen Patti 3 Card (227100)" },
  { value: "227101", name: "20-20 Teen Patti (227101)" },
  { value: "228000", name: "Andar Bahar (228000)" },
  { value: "228001", name: "Lucky 7 (228001)" },
  { value: "228002", name: "32 Cards (228002)" },
  { value: "321000", name: "Casino Marina Roulette (321000)" },
  { value: "321001", name: "Casino Marina Roulette 2 (321001)" },
  { value: "328000", name: "OTT Andar Bahar (328000)" },
  { value: "411000", name: "Cumbia Ruleta 1 (411000)" },
  { value: "421000", name: "Türkçe Rulet (421000)" },
  { value: "421002", name: "Slavyanka Roulette (421002)" },
  { value: "431000", name: "Ruleta Clasica (431000)" },
  { value: "431001", name: "Fiesta Roulette (431001)" },
  { value: "501000", name: "Turkish Roulette (501000)" },
  { value: "507000", name: "Casino Holdem 1 (507000)" },
  { value: "601000", name: "Ruletka Russia (601000)" },
  { value: "602000", name: "Bet On Numbers 1 (602000)" },
  { value: "611000", name: "Portomaso Casino Roulette (611000)" },
  { value: "611001", name: "Oracle Casino Roulette (611001)" },
  { value: "611003", name: "Oracle Casino Roulette 360 (611003)" },
  { value: "611004", name: "Portomaso Real Casino Roulette (611004)" },
  { value: "611005", name: "Lumia Auto Roulette (611005)" },
  { value: "611006", name: "Oracle Blaze (611006)" },
  { value: "1000012", name: "Baccarat A (1000012)" },
  { value: "1000013", name: "Baccarat B (1000013)" },
  { value: "1000014", name: "Baccarat C (1000014)" },
  { value: "1000015", name: "Baccarat Control Squeeze (1000015)" },
  { value: "1000016", name: "Baccarat Squeeze (1000016)" },
  { value: "1000017", name: "No Comm Speed Baccarat (1000017)" },
  { value: "1000018", name: "No Commission Baccarat (1000018)" },
  { value: "1000020", name: "Salon Privé Baccarat A (1000020)" },
  { value: "1000021", name: "Speed Baccarat A (1000021)" },
  { value: "1000022", name: "Speed Baccarat B (1000022)" },
  { value: "1000023", name: "Speed Baccarat C (1000023)" },
  { value: "1000024", name: "Speed Baccarat D (1000024)" },
  { value: "1000025", name: "Speed Baccarat E (1000025)" },
  { value: "1000026", name: "Speed Baccarat F (1000026)" },
  { value: "1000027", name: "Speed Baccarat G (1000027)" },
  { value: "1000028", name: "Speed Baccarat H (1000028)" },
  { value: "1000029", name: "Speed Baccarat I (1000029)" },
  { value: "1000030", name: "Blackjack A (1000030)" },
  { value: "1000031", name: "Blackjack B (1000031)" },
  { value: "1000032", name: "Blackjack C (1000032)" },
  { value: "1000033", name: "Blackjack D (1000033)" },
  { value: "1000034", name: "Blackjack E (1000034)" },
  { value: "1000035", name: "Blackjack F (1000035)" },
  { value: "1000036", name: "Blackjack G (1000036)" },
  { value: "1000037", name: "Blackjack H (1000037)" },
  { value: "1000038", name: "Blackjack I (1000038)" },
  { value: "1000039", name: "Blackjack J (1000039)" },
  { value: "1000040", name: "Blackjack Party (1000040)" },
  { value: "1000041", name: "Blackjack Silver A (1000041)" },
  { value: "1000042", name: "Blackjack Silver B (1000042)" },
  { value: "1000043", name: "Blackjack Silver C (1000043)" },
  { value: "1000044", name: "Blackjack Silver D (1000044)" },
  { value: "1000045", name: "Blackjack Silver E (1000045)" },
  { value: "1000046", name: "Blackjack Silver F (1000046)" },
  { value: "1000047", name: "Blackjack Silver G (1000047)" },
  { value: "1000048", name: "Blackjack VIP A (1000048)" },
  { value: "1000049", name: "Blackjack VIP B (1000049)" },
  { value: "1000050", name: "Blackjack VIP C (1000050)" },
  { value: "1000051", name: "Blackjack VIP D (1000051)" },
  { value: "1000052", name: "Blackjack VIP E (1000052)" },
  { value: "1000053", name: "Blackjack VIP G (1000053)" },
  { value: "1000054", name: "Blackjack VIP H (1000054)" },
  { value: "1000055", name: "Blackjack VIP I (1000055)" },
  { value: "1000056", name: "Blackjack VIP L (1000056)" },
  { value: "1000057", name: "Blackjack VIP M (1000057)" },
  { value: "1000058", name: "Blackjack VIP O (1000058)" },
  { value: "1000059", name: "Blackjack VIP P (1000059)" },
  { value: "1000060", name: "Blackjack VIP R (1000060)" },
  { value: "1000061", name: "Blackjack VIP S (1000061)" },
  { value: "1000062", name: "Blackjack VIP T (1000062)" },
  { value: "1000063", name: "Blackjack White A (1000063)" },
  { value: "1000064", name: "Blackjack White B (1000064)" },
  { value: "1000065", name: "Blackjack White C (1000065)" },
  { value: "1000068", name: "Salon Privé Blackjack A (1000068)" },
  { value: "1000069", name: "Salon Privé Blackjack B (1000069)" },
  { value: "1000070", name: "Salon Privé Blackjack C (1000070)" },
  { value: "1000073", name: "2 Hand Casino Holdem (1000073)" },
  { value: "1000074", name: "Dragon Tiger (1000074)" },
  { value: "1000076", name: "Lightning Dice (1000076)" },
  { value: "1000077", name: "Dream Catcher (1000077)" },
  { value: "1000084", name: "Double Ball Roulette (1000084)" },
  { value: "1000092", name: "Lightning Roulette (1000092)" },
  { value: "1000094", name: "Roulette (1000094)" },
  { value: "1000102", name: "Salon Privé Roulette (1000102)" },
  { value: "1000103", name: "Speed Auto Roulette (1000103)" },
  { value: "1000104", name: "Speed Roulette (1000104)" },
  { value: "1000110", name: "Super Sic Bo (1000110)" },
  { value: "1000111", name: "Texas Hold'em Bonus Poker (1000111)" },
  { value: "1000112", name: "Football studio (1000112)" },
  { value: "1000115", name: "Side Bet City (1000115)" },
  { value: "1000118", name: "First Person Roulette (1000118)" },
  { value: "1000119", name: "First Person Blackjack (1000119)" },
  { value: "1000120", name: "First Person Lightning Roulette (1000120)" },
  { value: "1000121", name: "First Person Dream Catcher (1000121)" },
  { value: "1000122", name: "Immersive Roulette (1000122)" },
  { value: "1000123", name: "Auto-Roulette (1000123)" },
  { value: "1000124", name: "VIP Roulette (1000124)" },
  { value: "1000126", name: "Auto-Roulette VIP (1000126)" },
  { value: "1000127", name: "Salon Privé Baccarat B (1000127)" },
  { value: "1000129", name: "Speed Baccarat J (1000129)" },
  { value: "1000130", name: "Blackjack VIP Q (1000130)" },
  { value: "1000131", name: "Blackjack VIP Z (1000131)" },
  { value: "1000132", name: "Speed Blackjack A (1000132)" },
  { value: "1000133", name: "Speed Blackjack B (1000133)" },
  { value: "1000134", name: "Speed Blackjack C (1000134)" },
  { value: "1000135", name: "Speed Blackjack D (1000135)" },
  { value: "1000142", name: "First Person Megaball (1000142)" },
  { value: "1000143", name: "First Person DragonTiger (1000143)" },
  { value: "1000144", name: "First Person TopCard (1000144)" },
  { value: "1000145", name: "First Person Baccarat (1000145)" },
  { value: "1000146", name: "Instant Roulette (1000146)" },
  { value: "1000148", name: "Crazy Time (1000148)" },
  { value: "1000149", name: "Lightning Baccarat (1000149)" },
  { value: "1000151", name: "Ultimate Texas Hold'em (1000151)" },
  { value: "1000152", name: "Three Card Poker (1000152)" },
  { value: "1000156", name: "Salon Privé Baccarat C (1000156)" },
  { value: "1000158", name: "Salon Privé Blackjack D (1000158)" },
  { value: "1000159", name: "Salon Privé Blackjack E (1000159)" },
  { value: "1000160", name: "Salon Privé Blackjack F (1000160)" },
  { value: "1000162", name: "Blackjack VIP Alpha (1000162)" },
  { value: "1000163", name: "Blackjack VIP Beta (1000163)" },
  { value: "1000164", name: "Blackjack VIP Gamma (1000164)" },
  { value: "1000141", name: "Megaball (1000141)" },
  { value: "1000248", name: "Craps (1000248)" },
  { value: "1000000", name: "Lobby Game Shows (1000000)" },
  { value: "1000001", name: "Lobby Blackjack (1000001)" },
  { value: "1000002", name: "Lobby Baccarat Sicbo (1000002)" },
  { value: "1000003", name: "Lobby Roulette (1000003)" },
  { value: "1000004", name: "Lobby Top Games (1000004)" },
  { value: "1000005", name: "Lobby Poker (1000005)" },
  { value: "1000249", name: "RNG Craps (1000249)" }
];

export const Countries = [
  { value: 'AD', name: 'AD - Andorra' },
  { value: 'AE', name: 'AE - United Arab Emirates' },
  { value: 'AF', name: 'AF - Afghanistan' },
  { value: 'AG', name: 'AG - Antigua and Barbuda' },
  { value: 'AI', name: 'AI - Anguilla' },
  { value: 'AL', name: 'AL - Albania' },
  { value: 'AM', name: 'AM - Armenia' },
  { value: 'AO', name: 'AO - Angola' },
  { value: 'AQ', name: 'AQ - Antarctica' },
  { value: 'AR', name: 'AR - Argentina' },
  { value: 'AS', name: 'AS - American Samoa' },
  { value: "AT", name: "AT - Austria" },
  { value: "AU", name: "AU - Australia" },
  { value: "AW", name: "AW - Aruba" },
  { value: "AX", name: "AX - Åland" },
  { value: "AZ", name: "AZ - Azerbaijan" },
  { value: "BA", name: "BA - Bosnia and Herzegovina" },
  { value: "BB", name: "BB - Barbados" },
  { value: "BD", name: "BD - Bangladesh" },
  { value: "BE", name: "BE - Belgium" },
  { value: "BF", name: "BF - Burkina Faso" },
  { value: "BG", name: "BG - Bulgaria" },
  { value: "BH", name: "BH - Bahrain" },
  { value: "BI", name: "BI - Burundi" },
  { value: "BJ", name: "BJ - Benin" },
  { value: "BL", name: "BL - Saint-Barthélemy" },
  { value: "BM", name: "BM - Bermuda" },
  { value: "BN", name: "BN - Brunei" },
  { value: "BO", name: "BO - Bolivia" },
  { value: "BQ", name: "BQ - Bonaire" },
  { value: "BR", name: "BR - Brazil" },
  { value: "BS", name: "BS - Bahamas" },
  { value: "BT", name: "BT - Bhutan" },
  { value: "BW", name: "BW - Botswana" },
  { value: "BY", name: "BY - Belarus" },
  { value: "BZ", name: "BZ - Belize" },
  { value: "CA", name: "CA - Canada" },
  { value: "CC", name: "CC - Cocos [Keeling] Islands" },
  { value: "CD", name: "CD - Congo" },
  { value: "CF", name: "CF - Central African Republic" },
  { value: "CG", name: "CG - Republic of the Congo" },
  { value: "CH", name: "CH - Switzerland" },
  { value: "CI", name: "CI - Ivory Coast" },
  { value: "CK", name: "CK - Cook Islands" },
  { value: "CL", name: "CL - Chile" },
  { value: "CM", name: "CM - Cameroon" },
  { value: "CN", name: "CN - China" },
  { value: "CO", name: "CO - Colombia" },
  { value: "COUNTRY_ISO_CODE", name:  "COUNTRY_ISO_CODE - country_name" },
  { value: "CR", name: "CR - Costa Rica" },
  { value: "CU", name: "CU - Cuba" },
  { value: "CV", name: "CV - Cape Verde" },
  { value: "CW", name: "CW - Curaçao" },
  { value: "CX", name: "CX - Christmas Island" },
  { value: "CY", name: "CY - Cyprus" },
  { value: "CZ", name: "CZ - Czech Republic" },
  { value: "DE", name: "DE - Germany" },
  { value: "DJ", name: "DJ - Djibouti" },
  { value: "DK", name: "DK - Denmark" },
  { value: "DM", name: "DM - Dominica" },
  { value: "DO", name: "DO - Dominican Republic" },
  { value: "DZ", name: "DZ - Algeria" },
  { value: "EC", name: "EC - Ecuador" },
  { value: "EE", name: "EE - Estonia" },
  { value: "EG", name: "EG - Egypt" },
  { value: "ER", name: "ER - Eritrea" },
  { value: "ES", name: "ES - Spain" },
  { value: "ET", name: "ET - Ethiopia" },
  { value: "FI", name: "FI - Finland" },
  { value: "FJ", name: "FJ - Fiji" },
  { value: "FK", name: "FK - Falkland Islands" },
  { value: "FM", name: "FM - Federated States of Micronesia" },
  { value: "FO", name: "FO - Faroe Islands" },
  { value: "FR", name: "FR - France" },
  { value: "GA", name: "GA - Gabon" },
  { value: "GB", name: "GB - United Kingdom" },
  { value: "GD", name: "GD - Grenada" },
  { value: "GE", name: "GE - Georgia" },
  { value: "GF", name: "GF - French Guiana" },
  { value: "GG", name: "GG - Guernsey" },
  { value: "GH", name: "GH - Ghana" },
  { value: "GI", name: "GI - Gibraltar" },
  { value: "GL", name: "GL - Greenland" },
  { value: "GM", name: "GM - Gambia" },
  { value: "GN", name: "GN - Guinea" },
  { value: "GP", name: "GP - Guadeloupe" },
  { value: "GQ", name: "GQ - Equatorial Guinea" },
  { value: "GR", name: "GR - Greece" },
  { value: "GS", name: "GS - South Georgia and the South Sandwich Islands" },
  { value: "GT", name: "GT - Guatemala" },
  { value: "GU", name: "GU - Guam" },
  { value: "GW", name: "GW - Guinea-Bissau" },
  { value: "GY", name: "GY - Guyana" },
  { value: "HK", name: "HK - Hong Kong" },
  { value: "HN", name: "HN - Honduras" },
  { value: "HR", name: "HR - Croatia" },
  { value: "HT", name: "HT - Haiti" },
  { value: "HU", name: "HU - Hungary" },
  { value: "ID", name: "ID - Indonesia" },
  { value: "IE", name: "IE - Ireland" },
  { value: "IL", name: "IL - Israel" },
  { value: "IM", name: "IM - Isle of Man" },
  { value: "IN", name: "IN - India" },
  { value: "IO", name: "IO - British Indian Ocean Territory" },
  { value: "IQ", name: "IQ - Iraq" },
  { value: "IR", name: "IR - Iran" },
  { value: "IS", name: "IS - Iceland" },
  { value: "IT", name: "IT - Italy" },
  { value: "JE", name: "JE - Jersey" },
  { value: "JM", name: "JM - Jamaica" },
  { value: "JO", name: "JO - Hashemite Kingdom of Jordan" },
  { value: "JP", name: "JP - Japan" },
  { value: "KE", name: "KE - Kenya" },
  { value: "KG", name: "KG - Kyrgyzstan" },
  { value: "KH", name: "KH - Cambodia" },
  { value: "KI", name: "KI - Kiribati" },
  { value: "KM", name: "KM - Comoros" },
  { value: "KN", name: "KN - Saint Kitts and Nevis" },
  { value: "KP", name: "KP - North Korea" },
  { value: "KR", name: "KR - Republic of Korea" },
  { value: "KW", name: "KW - Kuwait" },
  { value: "KY", name: "KY - Cayman Islands" },
  { value: "KZ", name: "KZ - Kazakhstan" },
  { value: "LA", name: "LA - Laos" },
  { value: "LB", name: "LB - Lebanon" },
  { value: "LC", name: "LC - Saint Lucia" },
  { value: "LI", name: "LI - Liechtenstein" },
  { value: "LK", name: "LK - Sri Lanka" },
  { value: "LR", name: "LR - Liberia" },
  { value: "LS", name: "LS - Lesotho" },
  { value: "LT", name: "LT - Republic of Lithuania" },
  { value: "LU", name: "LU - Luxembourg" },
  { value: "LV", name: "LV - Latvia" },
  { value: "LY", name: "LY - Libya" },
  { value: "MA", name: "MA - Morocco" },
  { value: "MC", name: "MC - Monaco" },
  { value: "MD", name: "MD - Republic of Moldova" },
  { value: "ME", name: "ME - Montenegro" },
  { value: "MF", name: "MF - Saint Martin" },
  { value: "MG", name: "MG - Madagascar" },
  { value: "MH", name: "MH - Marshall Islands" },
  { value: "MK", name: "MK - Macedonia" },
  { value: "ML", name: "ML - Mali" },
  { value: "MM", name: "MM - Myanmar [Burma]" },
  { value: "MN", name: "MN - Mongolia" },
  { value: "MO", name: "MO - Macao" },
  { value: "MP", name: "MP - Northern Mariana Islands" },
  { value: "MQ", name: "MQ - Martinique" },
  { value: "MR", name: "MR - Mauritania" },
  { value: "MS", name: "MS - Montserrat" },
  { value: "MT", name: "MT - Malta" },
  { value: "MU", name: "MU - Mauritius" },
  { value: "MV", name: "MV - Maldives" },
  { value: "MW", name: "MW - Malawi" },
  { value: "MX", name: "MX - Mexico" },
  { value: "MY", name: "MY - Malaysia" },
  { value: "MZ", name: "MZ - Mozambique" },
  { value: "NA", name: "NA - Namibia" },
  { value: "NC", name: "NC - New Caledonia" },
  { value: "NE", name: "NE - Niger" },
  { value: "NF", name: "NF - Norfolk Island" },
  { value: "NG", name: "NG - Nigeria" },
  { value: "NI", name: "NI - Nicaragua" },
  { value: "NL", name: "NL - Netherlands" },
  { value: "NO", name: "NO - Norway" },
  { value: "NP", name: "NP - Nepal" },
  { value: "NR", name: "NR - Nauru" },
  { value: "NU", name: "NU - Niue" },
  { value: "NZ", name: "NZ - New Zealand" },
  { value: "OM", name: "OM - Oman" },
  { value: "PA", name: "PA - Panama" },
  { value: "PE", name: "PE - Peru" },
  { value: "PF", name: "PF - French Polynesia" },
  { value: "PG", name: "PG - Papua New Guinea" },
  { value: "PH", name: "PH - Philippines" },
  { value: "PK", name: "PK - Pakistan" },
  { value: "PL", name: "PL - Poland" },
  { value: "PM", name: "PM - Saint Pierre and Miquelon" },
  { value: "PN", name: "PN - Pitcairn Islands" },
  { value: "PR", name: "PR - Puerto Rico" },
  { value: "PS", name: "PS - Palestine" },
  { value: "PT", name: "PT - Portugal" },
  { value: "PW", name: "PW - Palau" },
  { value: "PY", name: "PY - Paraguay" },
  { value: "QA", name: "QA - Qatar" },
  { value: "RE", name: "RE - Réunion" },
  { value: "RO", name: "RO - Romania" },
  { value: "RS", name: "RS - Serbia" },
  { value: "RU", name: "RU - Russia" },
  { value: "RW", name: "RW - Rwanda" },
  { value: "SA", name: "SA - Saudi Arabia" },
  { value: "SB", name: "SB - Solomon Islands" },
  { value: "SC", name: "SC - Seychelles" },
  { value: "SD", name: "SD - Sudan" },
  { value: "SE", name: "SE - Sweden" },
  { value: "SG", name: "SG - Singapore" },
  { value: "SH", name: "SH - Saint Helena" },
  { value: "SI", name: "SI - Slovenia" },
  { value: "SJ", name: "SJ - Svalbard and Jan Mayen" },
  { value: "SK", name: "SK - Slovakia" },
  { value: "SL", name: "SL - Sierra Leone" },
  { value: "SM", name: "SM - San Marino" },
  { value: "SN", name: "SN - Senegal" },
  { value: "SO", name: "SO - Somalia" },
  { value: "SR", name: "SR - Suriname" },
  { value: "SS", name: "SS - South Sudan" },
  { value: "ST", name: "ST - São Tomé and Príncipe" },
  { value: "SV", name: "SV - El Salvador" },
  { value: "SX", name: "SX - Sint Maarten" },
  { value: "SY", name: "SY - Syria" },
  { value: "SZ", name: "SZ - Swaziland" },
  { value: "TC", name: "TC - Turks and Caicos Islands" },
  { value: "TD", name: "TD - Chad" },
  { value: "TF", name: "TF - French Southern Territories" },
  { value: "TG", name: "TG - Togo" },
  { value: "TH", name: "TH - Thailand" },
  { value: "TJ", name: "TJ - Tajikistan" },
  { value: "TK", name: "TK - Tokelau" },
  { value: "TL", name: "TL - East Timor" },
  { value: "TM", name: "TM - Turkmenistan" },
  { value: "TN", name: "TN - Tunisia" },
  { value: "TO", name: "TO - Tonga" },
  { value: "TR", name: "TR - Turkey" },
  { value: "TT", name: "TT - Trinidad and Tobago" },
  { value: "TV", name: "TV - Tuvalu" },
  { value: "TW", name: "TW - Taiwan" },
  { value: "TZ", name: "TZ - Tanzania" },
  { value: "UA", name: "UA - Ukraine" },
  { value: "UG", name: "UG - Uganda" },
  { value: "UM", name: "UM - U.S. Minor Outlying Islands" },
  { value: "US", name: "US - United States" },
  { value: "UY", name: "UY - Uruguay" },
  { value: "UZ", name: "UZ - Uzbekistan" },
  { value: "VA", name: "VA - Vatican City" },
  { value: "VC", name: "VC - Saint Vincent and the Grenadines" },
  { value: "VE", name: "VE - Venezuela" },
  { value: "VG", name: "VG - British Virgin Islands" },
  { value: "VI", name: "VI - U.S. Virgin Islands" },
  { value: "VN", name: "VN - Vietnam" },
  { value: "VU", name: "VU - Vanuatu" },
  { value: "WF", name: "WF - Wallis and Futuna" },
  { value: "WS", name: "WS - Samoa" },
  { value: "XK", name: "XK - Kosovo" },
  { value: "YE", name: "YE - Yemen" },
  { value: "YT", name: "YT - Mayotte" },
  { value: "ZA", name: "ZA - South Africa" },
  { value: "ZM", name: "ZM - Zambia" },
  { value: "ZW", name: "ZW - Zimbabwe" }
]

export const TIMEZONE = [
  { value: 'UTC +00:00',name: '(GMT +00:00) UTC', zonename: 'UTC +00:00' },
  { value: 'UTC -11:00',name: '(GMT-11:00) American Samoa', zonename: 'US/Samoa' },
  { value: 'UTC -11:00', name: '(GMT-11:00) Midway Island', zonename: 'Pacific/Midway' },
  { value: 'UTC -12:00', name: '(GMT-12:00) International Date Line West', zonename: 'Pacific/Kwajalein' },
  { value: 'UTC -10:00', name: '(GMT-10:00) Hawaii', zonename: 'US/Hawaii' },
  { value: 'UTC -09:00', name: '(GMT-09:00) Alaska', zonename: 'US/Alaska' },
  { value: 'UTC -08:00', name: '(GMT-08:00) Pacific Time (US & Canada)', zonename: 'US/Pacific' },
  { value: 'UTC -08:00', name: '(GMT-08:00) Tijuana', zonename: 'America/Tijuana' },
  { value: 'UTC -07:00', name : '(GMT-07:00) Arizona', zonename: 'US/Arizona' },
  { value: 'UTC -07:00', name : '(GMT-07:00) Chihuahua', zonename: 'America/Chihuahua' },
  { value: 'UTC -07:00', name : '(GMT-07:00) Mazatlan', zonename: 'America/Mazatlan'  },
  { value: 'UTC -07:00', name : '(GMT-07:00) Mountain Time (US & Canada)', zonename: 'US/Mountain' },
  { value: 'UTC -06:00', name : '(GMT-06:00) Central America', zonename: 'America/Managua' },
  { value: 'UTC -06:00', name : '(GMT-06:00) Central Time (US & Canada)', zonename: 'US/Central' },
  { value: 'UTC -06:00', name : '(GMT-06:00) Guadalajara', zonename: '' },
  { value: 'UTC -06:00', name : '(GMT-06:00) Mexico City', zonename: 'America/Mexico_City' },
  { value: 'UTC -06:00', name : '(GMT-06:00) Monterrey', zonename: 'America/Monterrey' },
  { value: 'UTC -06:00', name : '(GMT-06:00) Saskatchewan', zonename: 'Canada/Saskatchewan' },
  { value: 'UTC -05:00', name : '(GMT-05:00) Bogota', zonename: 'America/Bogota' },
  { value: 'UTC -05:00', name : '(GMT-05:00) Eastern Time (US & Canada)', zonename: 'US/Eastern' },
  { value: 'UTC -05:00', name : '(GMT-05:00) Indiana (East)', zonename: 'US/East-Indiana' },
  { value: 'UTC -05:00', name : '(GMT-05:00) Lima', zonename: 'America/Lima' },
  { value: 'UTC -05:00', name : '(GMT-05:00) Quito', zonename: ' 	America/Guayaquil' },
  { value: 'UTC -04:00', name : '(GMT-04:00) Atlantic Time (Canada)', zonename: 'Canada/Atlantic' },
  { value: 'UTC -04:00', name : '(GMT-04:00) Caracas', zonename: 'America/Caracas' },
  { value: 'UTC -04:00', name : '(GMT-04:00) Georgetown', zonename: 'America/Argentina/Buenos_Aires' },
  { value: 'UTC -04:00', name : '(GMT-04:00) La Paz', zonename: 'America/La_Paz' },
  { value: 'UTC -04:00', name : '(GMT-04:00) Puerto Rico', zonename: 'America/Puerto_Rico' },
  { value: 'UTC -04:00', name : '(GMT-04:00) Santiago', zonename: 'America/Santiago' },
  { value: 'UTC -03:30', name : '(GMT-03:30) Newfoundland', zonename: 'Canada/Newfoundland' },
  { value: 'UTC -03:00', name : '(GMT-03:00) Brasilia', zonename: 'America/Sao_Paulo' },
  { value: 'UTC -03:00', name : '(GMT-03:00) Buenos Aires', zonename: 'America/Buenos_Aires' },
  { value: 'UTC -03:00', name : '(GMT-03:00) Greenland', zonename: 'America/Godthab' },
  { value: 'UTC -03:00', name : '(GMT-03:00) Montevideo', zonename: 'America/Montevideo' },
  { value: 'UTC -02:00', name : '(GMT-02:00) Mid-Atlantic', zonename: 'Atlantic/South_Georgia' },
  { value: 'UTC -01:00', name : '(GMT-01:00) Azores', zonename: 'Atlantic/Azores' },
  { value: 'UTC -01:00', name : '(GMT-01:00) Cape Verde Is.', zonename: 'Atlantic/Cape_Verde' },
  { value: 'UTC +01:00', name : '(GMT+01:00) Amsterdam', zonename: 'Europe/Amsterdam' },
  { value: 'UTC +01:00', name : '(GMT+01:00) Belgrade', zonename: 'Europe/Belgrade' },
  { value: 'UTC +01:00', name : '(GMT+01:00) Berlin', zonename: 'Europe/Berlin'   },
  { value: 'UTC +01:00', name : '(GMT+01:00) Bern', zonename: 'Europe/Berlin' },
  { value: 'UTC +01:00', name : '(GMT+01:00) Bratislava', zonename: 'Europe/Bratislava' },
  { value: 'UTC +01:00', name : '(GMT+01:00) Brussels', zonename: 'Europe/Brussels' },
  { value: 'UTC +01:00', name : '(GMT+01:00) Budapest', zonename: 'Europe/Budapest' },
  { value: 'UTC +01:00', name : '(GMT+01:00) Casablanca', zonename: 'Africa/Casablanca' },
  { value: 'UTC +01:00', name : '(GMT+01:00) Copenhagen', zonename: 'Europe/Copenhagen' },
  { value: 'UTC +01:00', name : '(GMT+01:00) Dublin', zonename: 'Europe/Dublin' },
  { value: 'UTC +01:00', name : '(GMT+01:00) Ljubljana', zonename: 'Europe/Ljubljana' },
  { value: 'UTC +01:00', name : '(GMT+01:00) Madrid', zonename: 'Europe/Madrid' },
  { value: 'UTC +01:00', name : '(GMT+01:00) Paris', zonename: 'Europe/Paris'  },
  { value: 'UTC +01:00', name : '(GMT+01:00) Prague', zonename: 'Europe/Prague' },
  { value: 'UTC +01:00', name : '(GMT+01:00) Rome', zonename: 'Europe/Rome'   },
  { value: 'UTC +01:00', name : '(GMT+01:00) Sarajevo', zonename: 'Europe/Sarajevo' },
  { value: 'UTC +01:00', name : '(GMT+01:00) Skopje', zonename: 'Europe/Skopje'   },
  { value: 'UTC +01:00', name : '(GMT+01:00) Stockholm', zonename: 'Europe/Stockholm' },
  { value: 'UTC +01:00', name : '(GMT+01:00) Vienna', zonename: 'Europe/Vienna' },
  { value: 'UTC +01:00', name : '(GMT+01:00) Warsaw', zonename: 'Europe/Warsaw' },
  { value: 'UTC +01:00', name : '(GMT+01:00) West Central Africa', zonename: 'Africa/Algiers' },
  { value: 'UTC +01:00', name : '(GMT+01:00) Zagreb', zonename: 'Europe/Zagreb' },
  { value: 'UTC +01:00', name : '(GMT+01:00) Zurich', zonename: 'Europe/Zurich' },
  { value: 'UTC +02:00', name : '(GMT+02:00) Athens', zonename: 'Europe/Athens' },
  { value: 'UTC +02:00', name : '(GMT+02:00) Bucharest', zonename: 'Europe/Bucharest' },
  { value: 'UTC +02:00', name : '(GMT+02:00) Cairo', zonename: 'Africa/Cairo'},
  { value: 'UTC +02:00', name : '(GMT+02:00) Harare', zonename: 'Africa/Harare' },
  { value: 'UTC +02:00', name : '(GMT+02:00) Helsinki', zonename: 'Europe/Helsinki' },
  { value: 'UTC +02:00', name : '(GMT+02:00) Jerusalem', zonename: 'Asia/Jerusalem' },
  { value: 'UTC +02:00', name : '(GMT+02:00) Kaliningrad', zonename: 'Europe/Kaliningrad' },
  { value: 'UTC +02:00', name : '(GMT+02:00) Kyiv', zonename: 'Europe/Kiev' },
  { value: 'UTC +02:00', name : '(GMT+02:00) Pretoria', zonename: 'Africa/Johannesburg' },
  { value: 'UTC +02:00', name : '(GMT+02:00) Riga', zonename: 'Europe/Riga' },
  { value: 'UTC +02:00', name : '(GMT+02:00) Sofia', zonename: 'Europe/Sofia' },
  { value: 'UTC +02:00', name : '(GMT+02:00) Tallinn', zonename: 'Europe/Tallinn' },
  { value: 'UTC +02:00', name : '(GMT+02:00) Vilnius', zonename: 'Europe/Vilnius' },
  { value: 'UTC +03:00', name : '(GMT+03:00) Baghdad', zonename: 'Asia/Baghdad' },
  { value: 'UTC +03:00', name : '(GMT+03:00) Istanbul', zonename: 'Europe/Istanbul' },
  { value: 'UTC +03:00', name : '(GMT+03:00) Kuwait', zonename: 'Asia/Kuwait' },
  { value: 'UTC +03:00', name : '(GMT+03:00) Minsk', zonename: 'Europe/Minsk' },
  { value: 'UTC +03:00', name : '(GMT+03:00) Moscow', zonename: 'Europe/Moscow' },
  { value: 'UTC +03:00', name : '(GMT+03:00) Nairobi', zonename: 'Africa/Nairobi' },
  { value: 'UTC +03:00', name : '(GMT+03:00) Riyadh', zonename: 'Asia/Riyadh' },
  { value: 'UTC +03:00', name : '(GMT+03:00) St. Petersburg', zonename: 'Europe/Moscow' },
  { value: 'UTC +03:00', name : '(GMT+03:00) Volgograd', zonename: 'Europe/Volgograd' },
  { value: 'UTC +03:30', name : '(GMT+03:30) Tehran', zonename: 'Asia/Tehran' },
  { value: 'UTC +04:00', name : '(GMT+04:00) Abu Dhabi', zonename: 'Asia/Dubai' },
  { value: 'UTC +04:00', name : '(GMT+04:00) Baku', zonename: 'Asia/Baku' },
  { value: 'UTC +04:00', name : '(GMT+04:00) Muscat', zonename: 'Asia/Muscat' },
  { value: 'UTC +04:00', name : '(GMT+04:00) Samara', zonename: 'Europe/Samara' },
  { value: 'UTC +04:00', name : '(GMT+04:00) Tbilisi', zonename: 'Asia/Tbilisi' },
  { value: 'UTC +04:00', name : '(GMT+04:00) Yerevan', zonename: 'Asia/Yerevan' },
  { value: 'UTC +04:30', name : '(GMT+04:30) Kabul', zonename: 'Asia/Kabul' },
  { value: 'UTC +05:00', name : '(GMT+05:00) Ekaterinburg', zonename: 'Asia/Yekaterinburg' },
  { value: 'UTC +05:00', name : '(GMT+05:00) Islamabad', zonename: 'Asia/Karachi' },
  { value: 'UTC +05:00', name : '(GMT+05:00) Karachi', zonename: 'Asia/Karachi' },
  { value: 'UTC +05:00', name : '(GMT+05:00) Tashkent', zonename: 'Asia/Tashkent' },
  { value: 'UTC +05:30', name : '(GMT+05:30) New Delhi', zonename: 'Asia/Kolkata' },
  { value: 'UTC +05:30', name : '(GMT+05:30) Sri Jayawardenepura', zonename: 'Asia/Colombo' },
  { value: 'UTC +05:45', name : '(GMT+05:45) Kathmandu', zonename: 'Asia/Kathmandu' },
  { value: 'UTC +06:00', name : '(GMT+06:00) Almaty', zonename: 'Asia/Almaty' },
  { value: 'UTC +06:00', name : '(GMT+06:00) Astana', zonename: 'Asia/Almaty' },
  { value: 'UTC +06:00', name : '(GMT+06:00) Dhaka', zonename: 'Asia/Dhaka' },
  { value: 'UTC +06:00', name : '(GMT+06:00) Urumqi', zonename: 'Asia/Urumqi' },
  { value: 'UTC +06:30', name : '(GMT+06:30) Rangoon', zonename: 'Asia/Rangoon' },
  { value: 'UTC +07:00', name : '(GMT+07:00) Bangkok', zonename: 'Asia/Bangkok' },
  { value: 'UTC +07:00', name : '(GMT+07:00) Hanoi', zonename: 'Asia/Ho_Chi_Minh' },
  { value: 'UTC +07:00', name : '(GMT+07:00) Jakarta', zonename: 'Asia/Jakarta' },
  { value: 'UTC +07:00', name : '(GMT+07:00) Krasnoyarsk', zonename: 'Asia/Krasnoyarsk' },
  { value: 'UTC +07:00', name : '(GMT+07:00) Novosibirsk', zonename: 'Asia/Novosibirsk' },
  { value: 'UTC +08:00', name : '(GMT+08:00) Beijing', zonename: 'Asia/Brunei' },
  { value: 'UTC +08:00', name : '(GMT+08:00) Chongqing', zonename: 'Asia/Chongqing' },
  { value: 'UTC +08:00', name : '(GMT+08:00) Hong Kong', zonename: 'Asia/Hong_Kong' },
  { value: 'UTC +08:00', name : '(GMT+08:00) Irkutsk', zonename: 'Asia/Irkutsk' },
  { value: 'UTC +08:00', name : '(GMT+08:00) Kuala Lumpur', zonename: 'Asia/Kuala_Lumpur' },
  { value: 'UTC +08:00', name : '(GMT+08:00) Perth', zonename: 'Australia/Perth' },
  { value: 'UTC +08:00', name : '(GMT+08:00) Singapore', zonename: 'Asia/Singapore' },
  { value: 'UTC +08:00', name : '(GMT+08:00) Taipei', zonename: 'Asia/Taipei'},
  { value: 'UTC +08:00', name : '(GMT+08:00) Ulaanbaatar', zonename: 'Asia/Ulaanbaatar' },
  { value: 'UTC +09:00', name : '(GMT+09:00) Osaka', zonename: 'Asia/Tokyo' },
  { value: 'UTC +09:00', name : '(GMT+09:00) Sapporo', zonename: 'Asia/Tokyo' },
  { value: 'UTC +09:00', name : '(GMT+09:00) Seoul', zonename: 'Asia/Seoul' },
  { value: 'UTC +09:00', name : '(GMT+09:00) Tokyo', zonename: 'Asia/Tokyo' },
  { value: 'UTC +09:00', name : '(GMT+09:00) Yakutsk', zonename: 'Asia/Yakutsk' },
  { value: 'UTC +09:30', name : '(GMT+09:30) Adelaide', zonename: 'Australia/Adelaide' },
  { value: 'UTC +09:30', name : '(GMT+09:30) Darwin', zonename: 'Australia/Darwin' },
  { value: 'UTC +10:00', name : '(GMT+10:00) Brisbane', zonename: 'Australia/Brisbane' },
  { value: 'UTC +10:00', name : '(GMT+10:00) Canberra', zonename: 'Australia/Canberra' },
  { value: 'UTC +10:00', name : '(GMT+10:00) Guam', zonename: 'Pacific/Guam' },
  { value: 'UTC +10:00', name : '(GMT+10:00) Hobart', zonename: 'Australia/Hobart' },
  { value: 'UTC +10:00', name : '(GMT+10:00) Melbourne', zonename: 'Australia/Melbourne' },
  { value: 'UTC +10:00', name : '(GMT+10:00) Port Moresby', zonename: 'Pacific/Port_Moresby' },
  { value: 'UTC +10:00', name : '(GMT+10:00) Sydney', zonename: 'Australia/Sydney' },
  { value: 'UTC +10:00', name : '(GMT+10:00) Vladivostok', zonename: 'Asia/Vladivostok' },
  { value: 'UTC +11:00', name : '(GMT+11:00) Magadan', zonename: 'Asia/Magadan' },
  { value: 'UTC +11:00', name : '(GMT+11:00) New Caledonia', zonename: 'Pacific/Noumea' },
  { value: 'UTC +11:00', name : '(GMT+11:00) Solomon Is.', zonename: 'Pacific/Guadalcanal' },
  { value: 'UTC +11:00', name : '(GMT+11:00) Srednekolymsk', zonename: 'Asia/Srednekolymsk' },
  { value: 'UTC +12:00', name : '(GMT+12:00) Auckland', zonename: 'Pacific/Auckland' },
  { value: 'UTC +12:00', name : '(GMT+12:00) Fiji', zonename: 'Pacific/Fiji' },
  { value: 'UTC +12:00', name : '(GMT+12:00) Kamchatka', zonename: 'Asia/Kamchatka' },
  { value: 'UTC +12:00', name : '(GMT+12:00) Marshall Is.', zonename: 'Pacific/Majuro' },
  { value: 'UTC +12:00', name : '(GMT+12:00) Wellington', zonename: 'Pacific/Auckland' },
  { value: 'UTC +12:45', name : '(GMT+12:45) Chatham Is.', zonename: 'Pacific/Chatham' },
  { value: 'UTC +13:00', name : "(GMT+13:00) Nuku'alofa", zonename: 'Pacific/Tongatapu' },
  { value: 'UTC +13:00', name : '(GMT+13:00) Samoa', zonename: 'US/Samoa' },
  { value: 'UTC +13:00', name : '(GMT+13:00) Tokelau Is.', zonename: 'Pacific/Fakaofo' },
];


export const WALLET_TYPES = [
  {label: 'Cash', value:1},
  {label: 'Non-Cash', value:2}
]

export const FINANCE_TYPES = [
  {label: 'Deposit', value:1},
  {label: 'Withdraw', value:2}
]

export function getTransactionType(type:any) {
  let str = '';
  switch (type) {
    case 'bet':
      str = 'Bet';
      break;
    case 'win':
      str = 'Win';
      break;
    case 'refund':
      str = 'Refund';
      break;
    case 'deposit':
      str = 'Deposit';
      break;
    case 'withdraw':
      str = 'Withdraw';
      break;
    case 'non_cash_granted_by_admin':
      str = 'Non cash granted by admin';
      break;
    case 'non_cash_withdraw_by_admin':
      str = 'Non cash withdraw by admin';
      break;
    case 'tip':
      str = 'Tip';
      break;
    case 'bet_non_cash':
      str = 'Bet non cash';
      break;
    case 'win_non_cash':
      str = 'Win non cash';
      break;
    case 'refund_non_cash':
      str = 'Refund non cash';
      break;
    case 'non_cash_bonus_claim':
      str = 'Non cash bonus claim';
      break;
    case 'deposit_bonus_claim':
      str = 'Deposit bonus claim';
      break;
    case 'tip_non_cash':
      str = 'Tip non cash';
      break;
    case 'withdraw_cancel':
      str = 'Withdraw cancel';
      break;
    case 'joining_bonus_claimed':
      str = 'Joining bonus claimed';
      break;
    case 'failed':
      str = 'Failed';
      break;
    case 'promo_code_bonus_claimed':
      str = 'Promo code bonus claimed';
      break;
    case 'commission_received':
      str = 'Commission Received';
      break;
    case 'exchange_place_bet_non_cash_debit':
      str = 'Exchange place bet non cash debit';
      break;
    case 'exchange_place_bet_cash_debit':
      str = 'Exchange place bet cash debit';
      break;   
    case 'exchange_place_bet_cash_credit':
      str = 'Exchange place bet cash credit';
      break;
    case 'exchange_refund_cancel_bet_non_cash_debit':
      str = 'Exchange refund cancel bet non cash debit';
      break;
    case 'exchange_refund_cancel_bet_cash_debit':
      str = 'Exchange refund cancel bet cash debit';
      break;
    case 'exchange_refund_cancel_bet_non_cash_credit':
      str = 'Exchange refund cancel bet non cash credit';
      break;
    case 'exchange_refund_cancel_bet_cash_credit':
      str = 'Exchange refund cancel bet cash credit';
      break;
    case 'exchange_refund_market_cancel_non_cash_debit':
      str = 'Exchange refund market cancel non cash debit';
      break; 
    case 'exchange_refund_market_cancel_cash_debit':
      str = 'Exchange refund market cancel cash debit';
      break;     
    case 'exchange_refund_market_cancel_non_cash_credit':
      str = 'Exchange refund market cancel non cash credit';
      break;
    case 'exchange_refund_market_cancel_cash_credit':
      str = 'Exchange refund market cancel cash credit';
      break; 
    case 'exchange_settle_market_cash_credit':
      str = 'Exchange settle market cash credit';
      break; 
    case 'exchange_settle_market_cash_debit':
      str = 'Exchange settle market cash debit';
      break;
    case 'exchange_resettle_market_cash_credit':
      str = 'Exchange resettle market cash credit';
      break; 
    case 'exchange_resettle_market_cash_debit':
      str = 'Exchange resettle market cash debit';
      break; 
    case 'exchange_cancel_settled_market_cash_credit':
      str = 'Exchange cancel settled market cash credit';
      break;
    case 'exchange_cancel_settled_market_cash_debit':
      str = 'Exchange cancel settled market cash debit';
      break;
    case 'exchange_deposit_bonus_claim':
      str = 'Exchange deposit bonus claim';
      break;                    
    default:
      str = 'na';
      break;
  }
  return str;
}

export const TENENT_PERMISSION = {
  dashboard: ['R', 'financial_activity', 'gaming_activity'],
  notifications: ['R'],
  sub_admin: ['R', 'C', 'U', 'T'],
  agents: ['R', 'C', 'U', 'T'],
  players: ['R', 'C', 'U', 'T','export'],
  players_commission: ['R','T','export'],
  tenant_credentials: ['R', 'C', 'U'],
  casino_management: ['R', 'C', 'U', 'D'],
  casino_transactions: ['R','export'],
  sport_transactions: ['R','export'],
  cms: ['R', 'C', 'U', 'T'],
  tenant_payment_providers: ['R', 'C', 'U', 'T'],
  tenant_theme_settings: ['R', 'U'],
  tenant_registration_fields: ['R', 'U'],
  tenant_configurations: ['R','U','T'],
  tenant_banners: ['R', 'C', 'U', 'D'],
  tenant_menu_setting: ['R', 'U'],
  tenant_sport_setting: ['R', 'U'],
  bank_details: ['R', 'C', 'U', 'T'],
  deposit_requests: ['R', 'T', 'export'],
  withdrawal_request: ['R', 'U', 'T','export'],
  fund_transfer_deposit: ['R', 'C'],
  fund_transfer_withdraw: ['R', 'C'],
  site_maintenance: ['R','U'],
  permission_role: ['R','C','U'],
  site_seo_management: ['R','C','U','T'],
  losing_bonus: ['R', 'C', 'U', 'D', 'T'],
  joining_bonus: ['R', 'C', 'U', 'D', 'T'],
  deposit_bonus: ['R', 'C', 'U', 'D', 'T'],
  promo_code: ['R', 'C', 'U', 'T'],
  casino_agent_revenue_report: ['R','export'],
  // sport_agent_revenue_report: ['R','export'],
  bonus_status_per_player_report: ['R','export'],
  game_transaction_report: ['R','export'],
  player_financial_report: ['R','export'],
  player_ncb_report: ['R','export'],
  player_report: ['R','export'],
  casino_player_revenue_report: ['R','export'],
  // sport_player_revenue_report: ['R','export'],
  tip_report: ['R','export'],
  unified_transaction_report: ['R','export'],
  ggr_report: ['R', 'export'],
  sports_book: ['R'],
  bet_report: ['R','export'],
  faq_management: ['R', 'C', 'U', 'D', 'T'],
  player_bank_details: ['R','C','U'],
  faq_categories: ['R', 'C', 'U', 'D', 'T']
}


export const QUEUE_LOG_TYPES = [
  {
    label: 'Casino Transaction',
    value: 'casino_transaction'
  },
  {
    label: 'Bet Transaction',
    value: 'bet_transaction'
  },
  {
    label: 'User Transaction',
    value: 'user_transaction'
  },
  {
    label: 'Bulk Deposit Withdraw',
    value: 'bulk_deposit_withdraw'
  },
  {
    label: 'Player Commission',
    value: 'player_commission'
  }
]

export const QUEUE_LOG_STATUSES = [
  {
    label: 'Ready',
    value: 0
  },
  {
    label: 'Done',
    value: 1
  },
  {
    label: 'InProgress',
    value: 2
  },
  {
    label: 'Failed',
    value: 3
  },
]



