export const environment = {
  production: true,
  API_URL: 'backend/api/',
  NODE_API: 'https://api.ezugitenantone.tk/',
  NODE_API_TWO: 'http://54.89.78.143:8007/api/',
  SOCKET_URL: '3.234.145.139:8085'
};
