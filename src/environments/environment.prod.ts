export const environment = {
  production: true,
  API_URL: 'backend/api/',
  //API_URL: 'http://localhost:9050/api/',
  NODE_API: 'https://api.ezugis.com/',
  NODE_API_TWO: 'https://api.ezugis.com/api/',
  SOCKET_URL: 'admin.ezugi.com:6001'
};
