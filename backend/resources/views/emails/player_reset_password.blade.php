<p> Hello {{ @$user_name }} </p>

<p> A link to change your password. You can do this through the link below. </p>

<a href="<?php echo $host . 'reset-password/' . $id .'/' . $randomKey ?>"> Change my password </a>

<p> If you didn't request this, please ignore this email. </p>

<p> Your password won't change until you access the link above and create a new one. </p>
