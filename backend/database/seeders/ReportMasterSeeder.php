<?php

namespace Database\Seeders;

use App\Models\Reports\ReportMaster;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ReportMasterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('tenant_report_configurations')->delete();
        \DB::table('report_master')->delete();

        $inputs = [
            [
                'name' => "Player Report",
                'report_abbr' => 'player_report',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()
            ],
            [
                'name' => "Player Financial Report",
                'report_abbr' => 'player_financial_report',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()
            ],
            [
                'name' => "Game Transaction Report",
                'report_abbr' => 'game_transaction_report',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()
            ],
            [
                'name' => "Player NCB Report",
                'report_abbr' => 'player_ncb_report',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()
            ],
            [
                'name' => "Tip Report",
                'report_abbr' => 'tip_report',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()
            ],
            [
                'name' => "Casino Player Revenue Report",
                'report_abbr' => 'player_revenue_report',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()
            ],
            [
                'name' => "Sport Player Revenue Report",
                'report_abbr' => 'sport_player_revenue_report',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()
            ],
            [
                'name' => "Casino Agent Revenue Report",
                'report_abbr' => 'agent_revenue_report',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()
            ],
            [
                'name' => "Sport Agent Revenue Report",
                'report_abbr' => 'sport_agent_revenue_report',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()
            ],
            [
                'name' => "Unified Transaction Report",
                'report_abbr' => 'unified_transaction_report',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()
            ],
            [
                'name' => "Bonus Status Per Player Report",
                'report_abbr' => 'bonus_status_per_player_report',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()
            ],
        ];
        ReportMaster::insert($inputs);
    }
}

