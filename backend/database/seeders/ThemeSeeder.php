<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Themes;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ThemeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $inputs[] = [
            'name' => 'Light',
            'options' => '{"palette":{"type":"light","primary":{"main":"#d3d3d3","contrast_text":"#ffffff"},"secondary":{"main":"#f8c765","contrast_text":"#ffffff"},"background":{"default":"#fafafa","paper":"#ffffff"},"text":{"primary":"#0d0d0d","secondary":"#1a1a1a","disabled":"#666666"},"action":{"selected":"#f8c765"},"alert":{"text":{"default":"#000000","success":"#ffffff","error":"#ffffff"}}},"typography":{"font_family":"Droid Sans"}}',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ];

        $inputs[] = [
            'name' => 'Dark',
            'options' => '{"name":"Dark","palette":{"type":"dark","primary":{"main":"#6f7280","contrast_text":"#ffffff"},"secondary":{"main":"#960837","contrast_text":"#ffffff"},"background":{"default":"#ffc7c7","paper":"#ffffff"},"text":{"primary":"#0d0d0d","secondary":"#1a1a1a","disabled":"#df8686"},"action":{"selected":"#7a7575"},"alert":{"text":{"default":"#000000","success":"#ffffff","error":"#ffffff"}}},"typography":{"font_family":"Source Sans Pro"}}',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ];

        $inputs[] = [
            'name' => 'Orange',
            'options' => '{"palette":{"type":"dark","primary":{"main":"#ffffff","contrast_text":"#e57b30"},"secondary":{"main":"#d23520","contrast_text":"#ffffff"},"background":{"default":"#d06518","paper":"#e57b30"},"text":{"primary":"#ffffff","secondary":"#f2f2f2","disabled":"#e6e6e6"},"action":{"selected":"#d23520"},"alert":{"text":{"default":"#000000","success":"#ffffff","error":"#ffffff"}}},"typography":{"font_family":"Droid Sans"}}',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ];

        $inputs[] = [
            'name' => 'Green',
            'options' => '{"palette":{"type":"dark","primary":{"main":"#357b71","contrast_text":"#ffffff"},"secondary":{"main":"#f8c765","contrast_text":"#000000"},"background":{"default":"#1e4f48","paper":"#275a53"},"text":{"primary":"#ffffff","secondary":"#f2f2f2","disabled":"#e6e6e6"},"action":{"selected":"#f8c765"},"alert":{"text":{"default":"#000000","success":"#ffffff","error":"#ffffff"}}},"typography":{"font_family":"Droid Sans"}}',
            'updated_at' => Carbon::now()->toDateTimeString(),
            'created_at' => Carbon::now()->toDateTimeString()
        ];

        $inputs[] = [
            'name' => 'Red',
            'options' => '{"name":"Red","palette":{"type":"light","primary":{"main":"#3f51b5","contrast_text":"#ffffff"},"secondary":{"main":"#ff4081","contrast_text":"#ffffff"},"background":{"default":"#ff6161","paper":"#ce3b3b"},"text":{"primary":"#0d0d0d","secondary":"#1a1a1a","disabled":"#666666"},"action":{"selected":"#322f2f"},"alert":{"text":{"default":"#000000","success":"#ffffff","error":"#ffffff"}}},"typography":{"font_family":"Droid Sans"}}',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ];

        $record = DB::table('themes')->get(['id'])->count();

        if (!$record) {
            Themes::insert($inputs);
        }

    }
}
