<?php

namespace Database\Seeders;

use App\Models\SuperMailConfiguration;
use Illuminate\Database\Seeder;

class SuperAdminMailConfigurationTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $record = SuperMailConfiguration::get(['id'])->count();

        if (!$record) {
            $data = [
                [
                  'display_key' => 'APP SENDGRID HOST',
                  'key_provided_by_account' => 'APP_SENDGRID_HOST',
                  'value' => '',
                  'parent_type' => 'SuperAdminUser',
                  'last_updated_parent_id'=> null,
                  'created_at' => date('Y-m-d H:i:s'), 
                  'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                  'display_key' => 'APP SENDGRID USERNAME',
                  'key_provided_by_account' => 'APP_SENDGRID_USERNAME',
                  'value' => '',
                  'parent_type' => 'SuperAdminUser',
                  'last_updated_parent_id'=> null,
                  'created_at' => date('Y-m-d H:i:s'), 
                  'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                  'display_key' => 'APP_SENDGRID_FRONTEND_HOST',
                  'key_provided_by_account' => 'APP_SENDGRID_FRONTEND_HOST',
                  'value' => '',
                  'parent_type' => 'SuperAdminUser',
                  'last_updated_parent_id'=> null,
                  'created_at' => date('Y-m-d H:i:s'), 
                  'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                  'display_key' => 'APP SENDGRID EMAIL',
                  'key_provided_by_account' => 'APP_SENDGRID_EMAIL',
                  'value' => '',
                  'parent_type' => 'SuperAdminUser',
                  'last_updated_parent_id'=> null,
                  'created_at' => date('Y-m-d H:i:s'), 
                  'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                  'display_key' => 'APP SENDGRID PORT',
                  'key_provided_by_account' => 'APP_SENDGRID_PORT',
                  'value' => '',
                  'parent_type' => 'SuperAdminUser',
                  'last_updated_parent_id'=> null,
                  'created_at' => date('Y-m-d H:i:s'), 
                  'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                  'display_key' => 'APP SENDGRID RELAY KEY',
                  'key_provided_by_account' => 'APP_SENDGRID_RELAY_KEY',
                  'value' => '',
                  'parent_type' => 'SuperAdminUser',
                  'last_updated_parent_id'=> null,
                  'created_at' => date('Y-m-d H:i:s'), 
                  'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                  'display_key' => 'APP SENDGRID SENDER EMAIL',
                  'key_provided_by_account' => 'APP_SENDGRID_SENDER_EMAIL',
                  'value' => '',
                  'parent_type' => 'SuperAdminUser',
                  'last_updated_parent_id'=> null,
                  'created_at' => date('Y-m-d H:i:s'), 
                  'updated_at' => date('Y-m-d H:i:s')
                ]
            ];
            SuperMailConfiguration::insert($data);
        }
    }
}
