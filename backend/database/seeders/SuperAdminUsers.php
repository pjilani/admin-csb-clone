<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SuperAdminUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $record = DB::table('super_admin_users')->get(['id'])->count();

        if (!$record) {
            $data = [
                [
                    'first_name' => 'Super',
                    'last_name' => 'Admin',
                    'email' => 'ezugi2021@gmail.com',
                    'encrypted_password' => bcrypt('123456789'),
                    'parent_type' => 'SuperAdminUser',
                    'parent_id' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')],
            ];
            DB::table('super_admin_users')->insert($data);
        }
    }
}
