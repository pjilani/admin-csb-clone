<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Currencies extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $record = DB::table('currencies')->get(['id'])->count();

        if (!$record) {
            $data = [
                ['name' => 'Euro', 'code' => 'EUR', 'primary' => true, 'exchange_rate' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
                ['name' => 'Rupee', 'code' => 'INR', 'primary' => false, 'exchange_rate' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
                ['name' => 'United Sates Dollar', 'primary' => false, 'exchange_rate' => 1, 'code' => 'USD', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
            ];
            DB::table('currencies')->insert($data);
        }
    }
}
