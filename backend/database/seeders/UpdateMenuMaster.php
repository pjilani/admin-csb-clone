<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Menu\MenuMasterModel;

class UpdateMenuMaster extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $inputs = [
            [
                'component_name' => 'sports',
                'component' => 'Dashboard',
                'path' => '/sports',
                'id' => 1
            ],
            [
                'path' => '/live-betting',
                'component_name' => 'liveBets',
                'component' => 'Inplay',
                'id' => 2
            ],
            [
                'path' => '/casino',
                'component_name' => 'casino',
                'component' => 'Casino',
                'id' => 3
            ],
            [

                'path' => '/live-casino',
                'component_name' => 'liveCasino',
                'component' => 'Casino',
                'id' => 4
            ],
            [

                'path' => '/promotion',
                'component_name' => 'promotion',
                'component' => 'Promotion',
                'id' => 5
            ]
        ];

        foreach ($inputs as $value) {
            $id = $value['id'];
            unset($value['id']);
            MenuMasterModel::where('id', $id)->update($value);
        }
    }
}
