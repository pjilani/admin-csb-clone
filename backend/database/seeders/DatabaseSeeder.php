<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            MenuMaster::class,
            ReportMasterSeeder::class,
            AdminRoles::class,
            ThemeSeeder::class,
            UpdateMenuMaster::class,
            AdminRoles::class,
            SuperRoles::class,
            Languages::class,
            Currencies::class,
            Layouts::class,
            SuperAdminUsers::class,
            TenantCredentialsKeys::class,
        ]);
    }
}
