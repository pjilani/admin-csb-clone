<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class updateNewItemToTenantCredentailsWithdrawalLimit extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = [
          [
            'name' => 'USER_MIN_WITHDRAWAL_LIMIT',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
          ],
          [
            'name' => 'USER_MIN_DEPOSIT_WITHDRAWAL_CHECK',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
          ],
          [
            'name' => 'APP_FAST2SMS_SENDER_ID',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
          ],
          [
            'name' => 'APP_FAST2SMS_MESSAGE_ID',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
          ]
          
      ];
      DB::table('tenant_credentials_keys')->insert($data);
    }
}
