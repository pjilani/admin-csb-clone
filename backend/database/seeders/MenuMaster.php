<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Menu\MenuMasterModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class MenuMaster extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $record = DB::table('menu_master')->get(['id'])->count();

        if (!$record) {
            $inputs = [
                [
                    'name' => "Sports",
                    'path' => '/',
                    'active' => false,
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString()
                ],
                [
                    'name' => "Live Bets",
                    'path' => 'live-betting',
                    'active' => false,
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString()
                ],
                [
                    'name' => "Casino",
                    'path' => 'casino',
                    'active' => true,
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString()
                ],
                [
                    'name' => "Live Casino",
                    'path' => 'live-casino',
                    'active' => true,
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString()
                ],
                [
                    'name' => "Promotion",
                    'path' => 'promotion',
                    'active' => true,
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString()
                ]
            ];
            \DB::table('menu_master')->delete();
            MenuMasterModel::insert($inputs);
        }
    }
}

