<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class updateNewFieldsTenantConfigurations extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = [
          [
            'name' => 'GLOBAL_IP_ADDRESS_FOR_WHITELIST',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
          ]
      ];
      \DB::table('tenant_credentials_keys')->insert($data);
    }
}


