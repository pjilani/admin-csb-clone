<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Layouts extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $record = DB::table('layouts')->get(['id'])->count();

        if (!$record) {
            $data = [
                ['name' => 'Layout 1', 'value' => 'layout-1', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
                ['name' => 'Layout 2', 'value' => 'layout-2', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]
            ];
            DB::table('layouts')->insert($data);
        }
    }
}
