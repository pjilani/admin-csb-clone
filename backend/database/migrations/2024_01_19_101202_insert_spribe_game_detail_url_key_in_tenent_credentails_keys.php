<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class InsertSpribeGameDetailUrlKeyInTenentCredentailsKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('tenant_credentials_keys')->insert([
            [
                'name' => 'APP_SPRIBE_GAME_DETAILS_URL',
                'description' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'APP_EZUGI_GAME_DETAILS_URL',
                'description' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('tenant_credentials_keys')->whereIn('name', ['APP_SPRIBE_GAME_DETAILS_URL', 'APP_EZUGI_GAME_DETAILS_URL'])->delete();
    }
}
