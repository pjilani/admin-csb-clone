<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTenantUserRegistrationFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_user_registration_fields', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('tenant_id')->comment('Tenant user id'); 
            $table->integer('first_name')->comment('0-> Not visible, 1-> visible on page with optional, 2-> visible with required')->default(1); 
            $table->integer('last_name')->comment('0-> Not visible, 1-> visible on page with optional, 2-> visible with required')->default(1); 
            $table->integer('email')->comment('0-> Not visible, 1-> visible on page with optional, 2-> visible with required')->default(1); 
            $table->integer('username')->comment('2-> visible with required')->default(2); 
            $table->integer('nick_name')->comment('0-> Not visible, 1-> visible on page with optional, 2-> visible with required')->default(1); 
            $table->integer('dob')->comment('0-> Not visible, 1-> visible on page with optional, 2-> visible with required')->default(1); 
            $table->integer('currency')->comment('2-> visible with required')->default(2); 
            $table->integer('phone')->comment('0-> Not visible, 1-> visible on page with optional, 2-> visible with required')->default(1); 
            $table->integer('city')->comment('0-> Not visible, 1-> visible on page with optional, 2-> visible with required')->default(1); 
            $table->integer('zip_code')->comment('0-> Not visible, 1-> visible on page with optional, 2-> visible with required')->default(1); 
            $table->integer('password')->comment('2-> visible with required')->default(2); 
            $table->integer('confirm_password')->comment('0-> Not visible, 1-> visible on page with optional, 2-> visible with required')->default(1); 
            $table->integer('promo_code')->comment('0-> Not visible, 1-> visible on page with optional, 2-> visible with required')->default(1); 
            $table->integer('eighteen_year_check')->comment('0-> Not visible, 2-> visible with required')->default(1); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_user_registration_fields');
    }
}
