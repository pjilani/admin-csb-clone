<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBonusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('bonus', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->string('code')->nullable();
            $table->decimal('percentage', 10, 0)->nullable();
            $table->boolean('enabled')->nullable()->default(0);
            $table->timestamp('valid_from')->nullable();
            $table->timestamp('valid_upto')->nullable();
            $table->string('kind')->nullable();
            $table->integer('currency_id')->nullable();
            $table->string('promotion_title')->nullable();
            $table->string('image')->nullable();
            $table->text('terms_and_conditions')->nullable();
            $table->text('vip_levels')->nullable()->default('{}');
            $table->integer('usage_count')->nullable()->default(1);
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bonus');
    }
}
