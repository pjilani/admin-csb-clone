<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class InsertTenantCredentialsKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('tenant_credentials_keys')->insert([
            [
                'name' => 'ST8_SITE_KEY',
                'description' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'ST8_SITE_LOBBY',
                'description' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('tenant_credentials_keys')->whereIn('name', ['ST8_SITE_KEY', 'ST8_SITE_LOBBY'])->delete();
    }
}

