<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOtherCurrencyNonCashAmountToBetsTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bets_transactions', function (Blueprint $table) {
            //$table->text('other_currency_non_cash_amount')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bets_transactions', function (Blueprint $table) {
            //$table->dropColumn('other_currency_non_cash_amount');
        });
    }
}
