<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterBonusAddTenantId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bonus', function (Blueprint $table) {

            if (!Schema::hasColumn('bonus', 'tenant_id'))
            {
                $table->bigInteger('tenant_id')->nullable()->index('index_bonus_on_tenant_id');
            }

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('bonus', 'tenant_id'))
        {
            Schema::hasTable('bonus', function (Blueprint $table) {
                $table->dropColumn('tenant_id');
            });
        }

    }
}
