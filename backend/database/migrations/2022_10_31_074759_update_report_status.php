<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateReportStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('report_master')->where('name','Sport Player Revenue Report')->update(['active'=>false]);
        DB::table('report_master')->where('name','Sport Agent Revenue Report')->update(['active'=>false]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::table('report_master')->where('name','Sport Player Revenue Report')->update(['active'=>true]);
        DB::table('report_master')->where('name','Sport Agent Revenue Report')->update(['active'=>true]);
    }
}
