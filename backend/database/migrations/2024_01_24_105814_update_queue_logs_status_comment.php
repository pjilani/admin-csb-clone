<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UpdateQueueLogsStatusComment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {        
        DB::statement('COMMENT ON COLUMN queue_logs.status IS NULL');

        DB::statement("COMMENT ON COLUMN queue_logs.status IS '0=>Ready, 1=>Done, 2=>In progress, 3=>Failed'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('COMMENT ON COLUMN queue_logs.status IS NULL');
    }
}

