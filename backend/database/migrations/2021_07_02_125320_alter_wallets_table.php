<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterWalletsTable extends Migration
{
    protected $table="wallets";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::hasTable($this->table, function (Blueprint $table) {
            if (Schema::hasColumn($this->table, 'amount')) {
                $table->decimal('amount', 15, 4)->default(0.0)->change();
            } else {
                $table->decimal('amount', 15, 4)->default(0.0);
            }
            if (Schema::hasColumn($this->table, 'non_cash_amount')) {
                $table->decimal('non_cash_amount', 15, 4)->default(0.0)->change();
            } else {
                $table->decimal('non_cash_amount', 15, 4)->default(0.0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn($this->table, 'amount')) {
            Schema::hasTable($this->table, function (Blueprint $table) {
                $table->dropColumn('amount');
            });
        }
        if (!Schema::hasColumn($this->table, 'non_cash_amount')) {
            Schema::hasTable($this->table, function (Blueprint $table) {
                $table->dropColumn('non_cash_amount');
            });
        }

    }
}
