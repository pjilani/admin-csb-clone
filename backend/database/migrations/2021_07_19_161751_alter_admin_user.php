<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AlterAdminUser extends Migration
{
    protected $table='admin_users';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::hasTable($this->table, function (Blueprint $table){

            $sqlCheck="SELECT conname
                        FROM pg_constraint
                        WHERE conrelid =
                            (SELECT oid 
                            FROM pg_class
                            WHERE relname LIKE 'admin_users') 
                        and conname= 'admin_users_email_and_tenant_id';";

            $result=DB::statement($sqlCheck);
            if(!$result) {
                $sql = "
                    ALTER TABLE admin_users ADD COLUMN new_email VARCHAR NULL;
                    UPDATE admin_users SET new_email= email;
                    ALTER TABLE \"admin_users\" DROP \"email\"; COMMENT ON TABLE \"admin_users\" IS '';
                    ALTER TABLE \"admin_users\" 
                    ALTER \"new_email\" TYPE character varying,
                    ALTER \"new_email\" DROP DEFAULT,
                    ALTER \"new_email\" DROP NOT NULL;
                    ALTER TABLE \"admin_users\" RENAME \"new_email\" TO \"email\";
                    COMMENT ON COLUMN \"admin_users\".\"email\" IS '';
                    COMMENT ON TABLE \"admin_users\" IS '';
                    ";
                DB::statement($sql);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
