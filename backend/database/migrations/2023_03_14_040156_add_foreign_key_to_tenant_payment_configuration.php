<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeyToTenantPaymentConfiguration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      
        Schema::table('tenant_payment_configurations', function (Blueprint $table) {
          $table->foreign('provider_id', 'fk_payment_24e6b25e44')->references('id')->on('payment_providers')->onUpdate('RESTRICT')->onDelete('RESTRICT');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('tenant_payment_configurations', function (Blueprint $table) {
        $table->dropForeign('fk_payment_24e6b25e44');
    });
    }
}
