<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenantSportsBetSettingTable extends Migration
{
    protected $table = 'tenant_sports_bet_setting';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->table)) {
            Schema::create($this->table, function (Blueprint $table) {
                $table->bigInteger('id', true);
                $table->bigInteger('tenant_id')->nullable()->index('index_tenant_sports_bet_setting_on_tenant_id');
                $table->decimal('event_liability', 10, 0)->nullable();
                $table->decimal('max_single_bet', 10, 0)->nullable();
                $table->decimal('min_bet', 10, 0)->nullable();
                $table->decimal('max_multiple_bet', 10, 0)->nullable();
                $table->decimal('max_bet_on_event', 10, 0)->nullable();
                $table->decimal('deposit_limit', 10, 0)->nullable();
                $table->decimal('max_win_amount', 10, 0)->nullable();
                $table->decimal('max_odd', 10, 0)->nullable();
                $table->timestamp('created_at', 6);
                $table->timestamp('updated_at', 6);
                $table->integer('cashout_percentage')->nullable();
                $table->boolean('bet_disabled')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
