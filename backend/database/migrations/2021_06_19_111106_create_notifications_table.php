<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->string('sender_type');
            $table->bigInteger('sender_id');
            $table->string('reference_type');
            $table->bigInteger('reference_id');
            $table->text('message')->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
            $table->index(['reference_type', 'reference_id'], 'index_notifications_on_reference_type_and_reference_id');
            $table->index(['sender_type', 'sender_id'], 'index_notifications_on_sender_type_and_sender_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
