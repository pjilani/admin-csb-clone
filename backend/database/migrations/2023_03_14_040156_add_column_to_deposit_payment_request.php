<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToDepositPaymentRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      
        Schema::table('deposit_requests', function (Blueprint $table) {
          $table->integer('payment_provider_id')->nullable()->after('updated_at');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('deposit_requests', function (Blueprint $table) {
        $table->dropColumn('payment_provider_id');
    });
    }
}
