<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewColumnToTenantPromoCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_promo_codes', function (Blueprint $table) {
            $table->integer('wallet_type')->nullable()->default(1)->comment('0-> cash, 1-> non-cash');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_promo_codes', function (Blueprint $table) {
            $table->dropColumn('wallet_type');
        });
    }
}
