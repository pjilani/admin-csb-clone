<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMaxBankAccountLimitToTenantThemeSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_theme_settings', function (Blueprint $table) {
            $table->integer('max_bank_account_limit')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_theme_settings', function (Blueprint $table) {
            $table->dropColumn('max_bank_account_limit');
        });
    }
}
