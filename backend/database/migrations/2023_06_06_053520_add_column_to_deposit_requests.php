<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToDepositRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deposit_requests', function (Blueprint $table) {
            //
          $table->string('utr_number')->nullable()->comment('Used for manual deposit case, user will enter this number')->after('order_id');
          $table->string('transaction_receipt')->nullable()->comment('Used for manual deposit case, user will upload paid receipt')->after('order_id');
          $table->float('amount')->nullable()->default(0.0)->after('order_id');
          $table->string('deposit_type')->default('payment_gateway')->comment('payment_gateway / manual')->after('order_id');
          $table->jsonb('bank_details')->nullable()->comment('tenant bank details in json formate')->after('order_id');
          $table->string('remark')->nullable()->comment('Admin can put any remark for the user')->after('order_id');
          $table->bigInteger('tenant_id')->nullable()->after('order_id');
          $table->bigInteger('action_id')->nullable()->after('order_id');
          $table->string('action_type')->nullable()->after('order_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deposit_requests', function (Blueprint $table) {
            //
            $table->dropColumn('utr_number');
            $table->dropColumn('transaction_receipt');
            $table->dropColumn('amount');
            $table->dropColumn('amount');
            $table->dropColumn('deposit_type');
            $table->dropColumn('bank_details');
            $table->dropColumn('remark');
        });
    }
}
