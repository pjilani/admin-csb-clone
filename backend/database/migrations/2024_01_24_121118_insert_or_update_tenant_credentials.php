<?php

use App\Models\TenantCredentials;
use App\Models\Tenants;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\App;

class InsertOrUpdateTenantCredentials extends Migration
{
    public function up()
    {
        foreach (Tenants::all() as $tenant) {
            TenantCredentials::updateOrCreate(
                ['key' => 'APP_EZUGI_GAME_DETAILS_URL', 'tenant_id' => $tenant->id],
                ['value' => $this->valueBasedOnEnvironment(), 'description' => 'Ezugi Game Details Url']
            );
        }
    }

    private function valueBasedOnEnvironment()
    {
        if (App::environment(['production'])) {
            return 'https://sbo.ezugi.com/api/get/';
        } else {
            return  'https://boint.tableslive.com/api/get/';
        }
    }

    public function down()
    {
        TenantCredentials::where('key', 'APP_EZUGI_GAME_DETAILS_URL')->delete();
    }
}

