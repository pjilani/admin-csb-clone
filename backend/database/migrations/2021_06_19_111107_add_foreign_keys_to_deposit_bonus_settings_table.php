<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToDepositBonusSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deposit_bonus_settings', function (Blueprint $table) {
            $table->foreign('bonus_id', 'fk_rails_4f2e4cbb18')->references('id')->on('bonus')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deposit_bonus_settings', function (Blueprint $table) {
            $table->dropForeign('fk_rails_4f2e4cbb18');
        });
    }
}
