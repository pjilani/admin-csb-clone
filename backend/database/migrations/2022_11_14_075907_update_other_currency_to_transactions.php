<?php

use App\Models\Currencies;
use App\Models\TenantConfigurations;
use App\Models\Transactions;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UpdateOtherCurrencyToTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      $transactionInfo = DB::table("transactions as t")
      ->leftjoin('wallets AS sw', 'sw.owner_id', '=', 't.source_wallet_id')
      ->leftjoin('wallets AS tw', 'tw.owner_id', '=', 't.target_wallet_id')
      ->leftJoin('currencies AS sc', 'sc.id', '=', 't.source_currency_id')
      ->leftJoin('currencies AS tc', 'tc.id', '=', 't.target_currency_id')
      ->select(['t.id', 't.tenant_id', 't.actionee_id', 't.actionee_type', 't.source_wallet_id', 't.target_wallet_id', 't.target_currency_id',
          't.source_currency_id', 't.transaction_type', 't.transaction_id', 't.amount', 't.conversion_rate', 't.status', 't.comments', 't.created_at',
          't.source_after_balance',
          't.source_before_balance',
          't.target_after_balance',
          't.other_currency_amount',
          't.target_before_balance',
          't.table_id', 't.round_id', 't.error_code', 't.error_description', 't.payment_method','t.game_id',
          'sw.owner_type as source_owner_type', 'sw.owner_id as source_owner_id',
          'tw.owner_type as target_owner_type', 'tw.owner_id as target_owner_id',
          'sc.code as source_currency_code',
          'tc.code as target_currency_code'])
      ->whereNull('t.other_currency_amount')
      ->get();

      foreach ($transactionInfo as $key => $value) {
          if($value->other_currency_amount == null){
              //  =======================================Add Other Currency Amount ======================================= 
            //  =======================================Add Other Currency Amount ======================================= 
            $configurations = TenantConfigurations::select('allowed_currencies')->where('tenant_id', $value->tenant_id)->get();
            $configurations = $configurations->toArray();
            $CurrenciesValue=[];
            if ($configurations) {

                $configurations = $configurations[0];
                if (strlen($configurations['allowed_currencies']) > 1) {
                    $configurations = str_replace('{', '', $configurations['allowed_currencies']);
                    $configurations = str_replace('}', '', $configurations);
                    $configurations = explode(',', $configurations);

                    if (@$configurations[0] && count($configurations) > 0) {
                        $CurrenciesValue = Currencies::select('code')->whereIn('id', $configurations)->pluck('code');
                        $CurrenciesValue = $CurrenciesValue->toArray();
                    }
                }
            }
            
            $currenciesTempArray = $other_currency_amount = [];

            $exMainTransactionCurrecy = '';
            if(isset($value->source_currency_id)){
                $exMainTransactionCurrecy = Currencies::select('code')->where('id',   $value->source_currency_id)->pluck('code');
                $exMainTransactionCurrecy =$exMainTransactionCurrecy->toArray();;
            }else{
                $exMainTransactionCurrecy = Currencies::select('code')->where('id',   $value->target_currency_id)->pluck('code');
                $exMainTransactionCurrecy =$exMainTransactionCurrecy->toArray();;
            }

            $amount_currency_ex = Currencies:: getExchangeRateCurrency($exMainTransactionCurrecy);
            // dd($amount_currency_ex);
            foreach ($CurrenciesValue as $key => $value1) {
                if($exMainTransactionCurrecy != $value1){
                    $getExchangeRateCurrency = Currencies:: getExchangeRateCurrency($value1);
                    $currenciesTempArray[$value1] = (float)number_format((float)$value->amount * ($amount_currency_ex / $getExchangeRateCurrency),4,".","");
                }else{
                    $currenciesTempArray[$value1] = (float)$value->amount;
                }

            }
            $other_currency_amount = json_encode($currenciesTempArray);
            DB::table('transactions')->where('id',$value->id)->update(['other_currency_amount'=>$other_currency_amount]);
          }
      }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::table('transactions')->update(['other_currency_amount'=>null]);
    }
}
