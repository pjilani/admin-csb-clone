<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToDepositRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deposit_requests', function (Blueprint $table) {
            //
        });

        Schema::table('deposit_requests', function (Blueprint $table) {
            if (!Schema::hasColumn('deposit_requests', 'session_id')) {
                $table->string('session_id')->nullable();
            }

            if (!Schema::hasColumn('deposit_requests', 'tracking_id')) {
                $table->string('tracking_id')->nullable();
            }

            if (!Schema::hasColumn('deposit_requests', 'payment_initiate')) {
                $table->boolean('payment_initiate')->default(false)->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deposit_requests', function (Blueprint $table) {
            $table->dropColumn('deposit_requests');
            $table->dropColumn('tracking_id');
            $table->dropColumn('payment_initiate');
            
        });


      
    }
}
