<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepositWithdrawJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasTable('deposit_withdraw_job')) {
            Schema::create('deposit_withdraw_job', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('type')->comment('1-bulk deposit, 2-bulk withdraw');
                $table->jsonb('request_object');
                $table->integer('status')->default(0)->comment('0-not started, 1-in progress, 2-failed, 3-completed, 4-partially failed');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit_withdraw_job');
    }
}
