<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNonCashColumnToBetsTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bets_transactions', function (Blueprint $table) {
            $table->decimal('source_non_cash_before_balance', 15, 4)->nullable();
            $table->decimal('source_non_cash_after_balance', 15, 4)->nullable();
            $table->decimal('target_non_cash_before_balance', 15, 4)->nullable();
            $table->decimal('target_non_cash_after_balance', 15, 4)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bets_transactions', function (Blueprint $table) {
            $table->dropColumn('source_non_cash_before_balance');
            $table->dropColumn('source_non_cash_after_balance');
            $table->dropColumn('target_non_cash_before_balance');
            $table->dropColumn('target_non_cash_after_balance');
        });
    }
}
