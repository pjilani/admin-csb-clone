<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepositBonusSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposit_bonus_settings', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->float('min_deposit')->nullable()->default(0.0);
            $table->float('max_bonus')->nullable();
            $table->integer('rollover_multiplier')->nullable();
            $table->float('max_rollover_per_bet')->nullable();
            $table->integer('valid_for_days')->nullable();
            $table->bigInteger('bonus_id')->index('index_deposit_bonus_settings_on_bonus_id');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit_bonus_settings');
    }
}
