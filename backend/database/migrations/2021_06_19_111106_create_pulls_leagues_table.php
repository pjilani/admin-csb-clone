<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePullsLeaguesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pulls_leagues', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->boolean('is_deleted');
            $table->integer('league_id');
            $table->string('name_de')->nullable();
            $table->string('season')->nullable();
            $table->bigInteger('location_id')->index('index_pulls_leagues_on_location_id');
            $table->bigInteger('sport_id')->index('index_pulls_leagues_on_sport_id');
            $table->string('name_en')->nullable();
            $table->string('name_fr')->nullable();
            $table->string('name_ru')->nullable();
            $table->string('name_tr')->nullable();
            $table->string('name_nl')->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pulls_leagues');
    }
}
