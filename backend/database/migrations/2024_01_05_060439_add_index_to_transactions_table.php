<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexToTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->index(['transaction_id', 'tenant_id'], 'index_transactions_on_transaction_id_and_tenant_id');
            $table->index(['debit_transaction_id', 'tenant_id'], 'index_transactions_on_debit_transaction_id_and_tenant_id');
            $table->index(['cancel_transaction_id', 'tenant_id'], 'index_transactions_on_cancel_transaction_id_and_tenant_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropIndex('index_transactions_on_transaction_id_and_tenant_id');
            $table->dropIndex('index_transactions_on_debit_transaction_id_and_tenant_id');
            $table->dropIndex('index_transactions_on_cancel_transaction_id_and_tenant_id');
        });
    }
}
