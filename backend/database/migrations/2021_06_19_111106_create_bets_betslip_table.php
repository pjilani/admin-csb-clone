<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBetsBetslipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('bets_betslip', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->boolean('is_deleted');
            $table->integer('bettype');
            $table->decimal('stake', 15);
            $table->bigInteger('user_id')->index('index_bets_betslip_on_user_id');
            $table->decimal('multi_price', 15);
            $table->string('betslip_status');
            $table->bigInteger('coupon_id')->nullable()->index('index_bets_betslip_on_coupon_id');
            $table->decimal('possible_win_amount', 15,4);
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bets_betslip');
    }
}
