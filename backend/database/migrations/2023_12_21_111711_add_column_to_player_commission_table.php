<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToPlayerCommissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('player_commission', function (Blueprint $table) {
            $table->bigInteger('week_id')->nullable();
            $table->bigInteger('month_id')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('player_commission', function (Blueprint $table) {
            $table->dropColumn('week_id');
            $table->dropColumn('month_id');
            
        });
    }
}