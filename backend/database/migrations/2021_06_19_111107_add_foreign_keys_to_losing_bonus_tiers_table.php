<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToLosingBonusTiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('losing_bonus_tiers', function (Blueprint $table) {
            $table->foreign('losing_bonus_setting_id', 'fk_rails_31972c0dfd')->references('id')->on('losing_bonus_settings')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('losing_bonus_tiers', function (Blueprint $table) {
            $table->dropForeign('fk_rails_31972c0dfd');
        });
    }
}
