<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallets', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->float('amount')->nullable()->default(0.0);
            $table->boolean('primary')->nullable()->default(0);
            $table->bigInteger('currency_id')->nullable()->index('index_wallets_on_currency_id');
            $table->string('owner_type')->nullable();
            $table->bigInteger('owner_id')->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
            $table->float('non_cash_amount')->nullable()->default(0.0);
            $table->index(['owner_type', 'owner_id'], 'index_wallets_on_owner_type_and_owner_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallets');
    }
}
