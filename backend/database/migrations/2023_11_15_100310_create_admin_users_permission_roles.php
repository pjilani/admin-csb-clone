<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminUsersPermissionRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_users_permission_roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('admin_user_id')->nullable()->index('index_admin_users_permission_roles_on_admin_user_id');
            $table->bigInteger('permission_role_id')->nullable()->index('index_admin_users_permission_roles_on_permission_role_id');
            $table->index(['admin_user_id', 'permission_role_id'], 'index_admin_users_permission_roles_on_admin_users_permission_roles');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_users_permission_roles');
    }
}
