<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToTransaction extends Migration
{
    
  protected $table = 'transactions';
  protected $columnsName = 'other_currency_amount';

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
      Schema::table($this->table, function (Blueprint $table) {
          if (!Schema::hasColumn($this->table, $this->columnsName)
          ) {
              $table->text($this->columnsName, 500)->nullable();
          }
      });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      if (
      Schema::hasColumn($this->table, $this->columnsName)
      ) {
          Schema::table($this->table, function (Blueprint $table) {
              $table->dropColumn($this->columnsName);

          });
      }
  }
}
