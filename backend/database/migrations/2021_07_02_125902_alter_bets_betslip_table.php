<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterBetsBetslipTable extends Migration
{
    protected $table="bets_betslip";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::hasTable($this->table, function (Blueprint $table) {
            if (Schema::hasColumn($this->table, 'stake')) {
                $table->decimal('stake', 15, 4)->change();
            } else {
                $table->decimal('stake', 15, 4);
            }
            if (Schema::hasColumn($this->table, 'multi_price')) {
                $table->decimal('multi_price', 15, 4)->change();
            } else {
                $table->decimal('multi_price', 15, 4);
            }
            if (Schema::hasColumn($this->table, 'possible_win_amount')) {
                $table->decimal('possible_win_amount', 15, 4)->change();
            } else {
                $table->decimal('possible_win_amount', 15, 4);
            }
            if (!Schema::hasColumn($this->table, 'settlement_status')) {
                $table->string('settlement_status')->nullable();
            }

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn($this->table, 'possible_win_amount')) {
            Schema::hasTable($this->table, function (Blueprint $table) {
                $table->dropColumn('possible_win_amount');
            });
        }

        if (!Schema::hasColumn($this->table, 'stake')) {
            Schema::hasTable($this->table, function (Blueprint $table) {
                $table->dropColumn('stake');
            });
        }

        if (!Schema::hasColumn($this->table, 'multiprice')) {
            Schema::hasTable($this->table, function (Blueprint $table) {
                $table->dropColumn('multiprice');
            });
        }

        if (!Schema::hasColumn($this->table, 'settlement_status')) {
            Schema::hasTable($this->table, function (Blueprint $table) {
                $table->dropColumn('settlement_status');
            });
        }

    }
}
