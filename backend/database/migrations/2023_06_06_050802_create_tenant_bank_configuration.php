<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenantBankConfiguration extends Migration
{
  protected $table = 'tenant_bank_configuration';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->table)) {
          Schema::create($this->table, function (Blueprint $table) {
              $table->bigInteger('id', true);
              $table->bigInteger('tenant_id'); 
              $table->string('used_for')->nullable(); // deposit
              $table->string('bank_name')->nullable(); // Bank Name
              $table->string('account_holder_name')->nullable(); // Account_holder Name
              $table->string('bank_ifsc_code')->nullable(); // Bank Name
              $table->string('account_number')->nullable(); // Bank Name
              $table->boolean('status')->default(true); // Bank Name
              $table->bigInteger('created_by')->comment('Create by tenant owner ID'); // Super_admin
              $table->bigInteger('last_updated_owner_id')->nullable(); // id
              $table->timestamp('created_at');
              $table->timestamp('updated_at');
          });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
