<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpribeAuthTokenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spribe_auth_token', function (Blueprint $table) {
            $table->id();
            $table->integer('tenant_id');
            $table->bigInteger('user_id');
            $table->string('session_token');
            $table->string('auth_token');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spribe_auth_token');
    }
}
