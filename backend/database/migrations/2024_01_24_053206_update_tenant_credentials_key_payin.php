<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UpdateTenantCredentialsKeyPayin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('tenant_credentials_keys')->insert([
        [
            'name' => 'PAYIN_IDS',
            'description' => null,
            'created_at' => now(),
            'updated_at' => now(),
        ]
    ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::table('tenant_credentials_keys')->where('name', ['PAYIN_IDS'])->delete();

    }
}
