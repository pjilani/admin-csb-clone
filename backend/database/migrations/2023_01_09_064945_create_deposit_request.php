<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateDepositRequest extends Migration
{
  protected $table = 'deposit_requests';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
              

        if (!Schema::hasTable($this->table)) {
          Schema::create($this->table, function (Blueprint $table) {
              $table->bigInteger('id', true);
              $table->bigInteger('user_id')->nullable();
              $table->string('order_id')->nullable();
              $table->string('status')->nullable()->default('opened');
              $table->integer('ledger_id')->nullable();
              $table->string('data')->nullable();
              $table->timestamp('created_at');
              $table->timestamp('updated_at');
          });

          DB::statement("ALTER TABLE deposit_requests ALTER data TYPE hstore USING (data::hstore);");
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
