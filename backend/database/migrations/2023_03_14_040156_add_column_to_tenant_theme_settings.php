<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToTenantThemeSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      
        Schema::table('tenant_theme_settings', function (Blueprint $table) {
          $table->integer('user_login_type')->nullable()->comment('1->mobile otp login, 2-> email with password login')->after('language_id');
          $table->string('sms_gateway')->nullable()->comment('Twilio, fast2SMS')->after('theme');
          $table->string('fab_icon_url')->nullable()->after('logo_url');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('tenant_theme_settings', function (Blueprint $table) {
        $table->dropColumn('user_login_type');
        $table->dropColumn('sms_gateway');
        $table->dropColumn('fab_icon_url');
    });
    }
}
