<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class InsertEzugiSocketUrlInTenantCredentialKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('tenant_credentials_keys')->insert([
            [
                'name' => 'APP_EZUGI_SOCKET_URL',
                'description' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('tenant_credentials_keys')->whereIn('name', ['APP_EZUGI_SOCKET_URL'])->delete();
    }
}
