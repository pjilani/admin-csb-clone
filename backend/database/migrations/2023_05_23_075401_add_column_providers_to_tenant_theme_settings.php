<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnProvidersToTenantThemeSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_theme_settings', function (Blueprint $table) {
            //
            $table->string('assigned_providers')->nullable()->after('language_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_theme_settings', function (Blueprint $table) {
            //
            $table->dropColumn('assigned_providers');
        });
    }
}
