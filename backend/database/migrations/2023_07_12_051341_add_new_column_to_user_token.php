<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewColumnToUserToken extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_tokens', function (Blueprint $table) {
            //
            $table->string('phone')->nullable()->comment('Only use on registration process')->after('token');
            $table->string('phone_code')->nullable()->comment('Only use on registration process')->after('token');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_tokens', function (Blueprint $table) {
            //
            $table->dropColumn('phone');
            $table->dropColumn('phone_code');

        });
    }
}
