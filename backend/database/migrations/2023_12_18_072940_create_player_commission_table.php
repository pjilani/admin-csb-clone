<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlayerCommissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_commission', function (Blueprint $table) {
            $table->id();
            $table->decimal('commission_amount', 4, 2);
            $table->decimal('commission_percentage', 4, 2);
            $table->decimal('ngr', 10, 2);
            $table->integer('user_id');
            $table->integer('status')->default(0)->comment('0=>Pending, 1=>Approved, 2=>Rejected');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player_commission');
    }
}