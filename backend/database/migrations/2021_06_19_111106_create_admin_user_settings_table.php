<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminUserSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */


    public function up()
    {
        Schema::create('admin_user_settings', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->string('key')->nullable();
            $table->string('value')->nullable();
            $table->bigInteger('admin_user_id')->nullable()->index('index_admin_user_settings_on_admin_user_id');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_user_settings');
    }
}
