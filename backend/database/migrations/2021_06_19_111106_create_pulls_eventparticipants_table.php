<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePullsEventparticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pulls_eventparticipants', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->boolean('is_deleted');
            $table->string('position')->nullable();
            $table->integer('rotation_id')->nullable();
            $table->boolean('is_active');
            $table->string('extra_data')->nullable();
            $table->bigInteger('event_id')->index('index_pulls_eventparticipants_on_event_id');
            $table->bigInteger('participant_id')->index('index_pulls_eventparticipants_on_participant_id');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pulls_eventparticipants');
    }
}
