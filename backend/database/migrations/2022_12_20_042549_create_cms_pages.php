<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCmsPages extends Migration
{
    protected $table = 'cms_pages';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if (!Schema::hasTable($this->table)) {
        Schema::create($this->table, function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('tenant_id')->nullable();
            $table->bigInteger('admin_user_id')->nullable();
            $table->text('title')->nullable();
            $table->string('slug')->nullable();
            $table->jsonb('content')->nullable();
            $table->boolean('active')->default(true)->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_pages');
    }
}
