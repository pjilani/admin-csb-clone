<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnToAddBankDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('user_bank_details', function (Blueprint $table) {
        if (!Schema::hasColumn('user_bank_details', 'phone_code')
        ) {
            Schema::table('user_bank_details', function (Blueprint $table) {
                $table->string('phone_code')->nullable(true);
            });
        }
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('user_bank_details', function (Blueprint $table) {
        $table->dropColumn('phone_code');
      });
    }
}
