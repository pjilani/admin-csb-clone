<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_items', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('page_menu_id')->nullable()->index('index_menu_items_on_page_menu_id');
            $table->bigInteger('casino_item_id')->nullable()->index('index_menu_items_on_casino_item_id');
            $table->string('name')->nullable();
            $table->integer('order')->nullable();
            $table->boolean('active')->nullable()->default(1);
            $table->boolean('featured')->nullable()->default(0);
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_items');
    }
}
