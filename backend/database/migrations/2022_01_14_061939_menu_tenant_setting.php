<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MenuTenantSetting extends Migration
{
    protected $table = 'menu_tenant_setting';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->table)) {
            Schema::create($this->table, function (Blueprint $table) {
                $table->id();
                $table->integer('ordering')->nullable();
                $table->bigInteger('tenant_id');
                $table->bigInteger('menu_id');
                $table->foreign('menu_id')->references('id')
                    ->on('menu_master')
                    ->onUpdate('RESTRICT')
                    ->onDelete('RESTRICT');
                $table->foreign('tenant_id')->references('id')
                    ->on('tenants')
                    ->onUpdate('RESTRICT')
                    ->onDelete('RESTRICT');
                $table->timestamps();
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
