<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTenantColumnToDepositWithdrawJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deposit_withdraw_job', function (Blueprint $table) {
            if (!Schema::hasColumn('deposit_withdraw_job', 'tenant_id')) {
                $table->bigInteger('tenant_id')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('deposit_withdraw_job', 'tenant_id')) {
            Schema::table('deposit_withdraw_job', function (Blueprint $table) {
                $table->dropColumn('tenant_id');
  
            });
        }
    }
}
