<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToCasinoGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('casino_games', function (Blueprint $table) {
            $table->foreign('casino_provider_id', 'fk_rails_13020b6759')->references('id')->on('casino_providers')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('casino_games', function (Blueprint $table) {
            $table->dropForeign('fk_rails_13020b6759');
        });
    }
}
