<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterBetsTransactionTable extends Migration
{
    protected $table="bets_transactions";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->table, function (Blueprint $table) {
            if (!Schema::hasColumn($this->table, 'status')) {
                $table->string('status')->nullable();
            }
            if (Schema::hasColumn($this->table, 'source_before_balance')) {
                $table->decimal('source_before_balance', 15, 4)->nullable()->change();
            }else{
                $table->decimal('source_before_balance', 15, 4)->nullable();
            }
            if (Schema::hasColumn($this->table, 'source_after_balance')) {
                $table->decimal('source_after_balance', 15, 4)->nullable()->change();
            }else{
                $table->decimal('source_after_balance', 15, 4)->nullable();
            }
            if (Schema::hasColumn($this->table, 'target_after_balance')) {
                $table->decimal('target_after_balance', 15, 4)->nullable()->change();
            }else{
                $table->decimal('target_after_balance', 15, 4)->nullable();
            }
            if (Schema::hasColumn($this->table, 'target_before_balance')) {
                $table->decimal('target_before_balance', 15, 4)->nullable()->change();
            }else{
                $table->decimal('target_before_balance', 15, 4)->nullable();
            }

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn($this->table, 'status')) {
            Schema::hasTable($this->table, function (Blueprint $table) {
                $table->dropColumn('status');
            });
        }

        if (!Schema::hasColumn($this->table, 'source_before_balance')) {
            Schema::hasTable($this->table, function (Blueprint $table) {
                $table->dropColumn('source_before_balance');
            });
        }

        if (!Schema::hasColumn($this->table, 'source_after_balance')) {
            Schema::hasTable($this->table, function (Blueprint $table) {
                $table->dropColumn('source_after_balance');
            });
        }

        if (!Schema::hasColumn($this->table, 'target_after_balance')) {
            Schema::hasTable($this->table, function (Blueprint $table) {
                $table->dropColumn('target_after_balance');
            });
        }
        if (!Schema::hasColumn($this->table, 'target_before_balance')) {
            Schema::hasTable($this->table, function (Blueprint $table) {
                $table->dropColumn('target_before_balance');
            });
        }
    }
}