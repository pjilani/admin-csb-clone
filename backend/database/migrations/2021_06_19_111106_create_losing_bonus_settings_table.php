<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLosingBonusSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('losing_bonus_settings', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('bonus_id')->index('index_losing_bonus_settings_on_bonus_id');
            $table->integer('claim_days')->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('losing_bonus_settings');
    }
}
