<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMaxAndMinDepositLimitToTenantPaymentConfigurationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_payment_configurations', function (Blueprint $table) {
            $table->integer('max_deposit_limit')->nullable();
            $table->integer('min_deposit_limit')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_payment_configurations', function (Blueprint $table) {
            $table->dropColumn('max_deposit_limit');
            $table->dropColumn('min_deposit_limit');
        });
    }
}
