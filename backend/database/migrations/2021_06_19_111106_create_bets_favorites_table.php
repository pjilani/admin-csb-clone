<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBetsFavoritesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('bets_favorites', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->boolean('is_deleted');
            $table->bigInteger('event_id')->index('index_bets_favorites_on_event_id');
            $table->bigInteger('user_id')->index('index_bets_favorites_on_user_id');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bets_favorites');
    }
}
