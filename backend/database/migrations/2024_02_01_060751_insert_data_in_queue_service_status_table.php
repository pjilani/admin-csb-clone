<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class InsertDataInQueueServiceStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('queue_service_status')->insert([
            [
                'service' => 'push_in_queue',
                'status' => 1,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'service' => 'push_in_queue_cron',
                'status' => 1,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'service' => 'req_res_log_cron',
                'status' => 1,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'service' => 'update_deposit_request_cron',
                'status' => 1,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'service' => 'delete_spribe_token_cron',
                'status' => 1,
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('queue_service_status')->whereIn('service', ['push_in_queue', 'push_in_queue_cron', 'req_res_log_cron', 'update_deposit_request_cron', 'delete_spribe_token_cron'])->delete();
    }
}
