<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMenuMaster extends Migration
{
    protected $table = 'menu_master';
    protected $columnsComponent = 'component';
    protected $columnsComponentName = 'component_name';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->table, function (Blueprint $table) {
            if (!Schema::hasColumn($this->table, $this->columnsComponent) &&
                !Schema::hasColumn($this->table, $this->columnsComponentName)
            ) {
                $table->string($this->columnsComponent,50)->nullable();
                $table->string($this->columnsComponentName,50)->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (
            Schema::hasColumn($this->table, $this->columnsComponent) &&
            Schema::hasColumn($this->table, $this->columnsComponentName)
        ) {
            Schema::table($this->table, function (Blueprint $table) {
                $table->dropColumn($this->columnsComponent);
                $table->dropColumn($this->columnsComponentName);

            });
        }
    }
}
