<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;

class CreateWeekDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('week_details', function (Blueprint $table) {
            $table->id();
            $table->integer('week_count');
            $table->timestamp('start_date');
            $table->timestamp('end_date');
            $table->timestamps();
        });

        $currentYear = now()->year;
        $weekDetails = [];
        for ($week = 1; $week <= 52; $week++) {
            $startDate = Carbon::now()->setISODate($currentYear, $week)->startOfWeek();
            $endDate = $startDate->copy()->endOfWeek();
            $weekDetails[] = [
                'week_count' => $week,
                'start_date' => $startDate->toDateTimeString(),
                'end_date' => $endDate->toDateTimeString(),
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }  
        DB::table('week_details')->insert($weekDetails);  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('week_details');
    }
}