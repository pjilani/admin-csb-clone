<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePageMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_menus', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('page_id')->nullable()->index('index_page_menus_on_page_id');
            $table->bigInteger('casino_menu_id')->nullable()->index('index_page_menus_on_casino_menu_id');
            $table->string('name')->nullable();
            $table->integer('menu_order')->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_menus');
    }
}
