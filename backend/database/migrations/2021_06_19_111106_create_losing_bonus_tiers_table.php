<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLosingBonusTiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('losing_bonus_tiers', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('losing_bonus_setting_id')->index('index_losing_bonus_tiers_on_losing_bonus_setting_id');
            $table->float('min_losing_amount')->nullable();
            $table->float('percentage')->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('losing_bonus_tiers');
    }
}
