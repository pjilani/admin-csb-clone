<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPromoCodeBonus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_promo_code_bonus', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->string('status')->nullable();
            $table->float('bonus_amount')->nullable()->default(0.0);
            $table->bigInteger('user_id')->index('index_user_promo_code_bonus_on_user_id');
            $table->bigInteger('promo_code_id')->index('index_user_promo_code_bonus_on_promo_code_id');
            $table->string('kind')->nullable();
            $table->timestamp('expires_at')->nullable();
            $table->timestamp('claimed_at')->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
            $table->bigInteger('transaction_id')->nullable()->index('index_user_promo_code_bonus_on_transaction_id');
      
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_promo_code_bonus');
    }
}
