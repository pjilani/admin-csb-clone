<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepositWithdrawUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasTable('deposit_withdraw_users')) {
            Schema::create('deposit_withdraw_users', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('job_id');
                $table->bigInteger('user_id');
                $table->integer('status')->default(0)->comment('0-not started, 1-failed, 2-completed');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit_withdraw_users');
    }
}
