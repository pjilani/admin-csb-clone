<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserBonusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_bonus', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->string('status')->nullable();
            $table->float('bonus_amount')->nullable()->default(0.0);
            $table->float('rollover_balance')->nullable()->default(0.0);
            $table->bigInteger('user_id')->index('index_user_bonus_on_user_id');
            $table->bigInteger('bonus_id')->index('index_user_bonus_on_bonus_id');
            $table->string('kind')->nullable();
            $table->timestamp('expires_at')->nullable();
            $table->timestamp('claimed_at')->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
            $table->bigInteger('transaction_id')->nullable()->index('index_user_bonus_on_transaction_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_bonus');
    }
}
