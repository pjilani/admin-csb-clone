<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenantBlockedLeaguesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_blocked_leagues', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('tenant_id')->nullable()->index('index_tenant_blocked_leagues_on_tenant_id');
            $table->bigInteger('admin_user_id')->nullable()->index('index_tenant_blocked_leagues_on_admin_user_id');
            $table->bigInteger('pulls_league_id')->nullable()->index('index_tenant_blocked_leagues_on_pulls_league_id');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_blocked_leagues');
    }
}
