<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransactionColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('tenant_configurations', function (Blueprint $table) {
          $table->text('allowed_currencies')->default('')->change(); // Change Datatype length
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('tenant_configurations', function (Blueprint $table) {
        $table->text('allowed_currencies',[])->change(); // Change Datatype length
      });
    }
}
