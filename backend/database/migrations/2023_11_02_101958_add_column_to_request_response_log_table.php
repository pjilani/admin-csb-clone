<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToRequestResponseLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_response_log', function (Blueprint $table) {
            if (!Schema::hasColumn('request_response_log', 'response_code')) {
                $table->string('response_code')->nullable();
            }

            if (!Schema::hasColumn('request_response_log', 'response_status')) {
                $table->string('response_status')->nullable();
            }

            if (!Schema::hasColumn('request_response_log', 'error_code')) {
                $table->string('error_code')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_response_log', function (Blueprint $table) {
            if (Schema::hasColumn('request_response_log', 'response_code')) {
                Schema::table('request_response_log', function (Blueprint $table) {
                    $table->dropColumn('response_code');
                });
            }

            if (Schema::hasColumn('request_response_log', 'response_status')) {
                Schema::table('request_response_log', function (Blueprint $table) {
                    $table->dropColumn('response_status');
                });
            }

            if (Schema::hasColumn('request_response_log', 'error_code')) {
                Schema::table('request_response_log', function (Blueprint $table) {
                    $table->dropColumn('error_code');
                });
            }
        });
    }
}
