<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeDecimalRangeInPlayerCommissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('player_commission', function (Blueprint $table) {
            $table->decimal('commission_amount', 10, 2)->change();
            $table->decimal('commission_percentage', 10, 2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('player_commission', function (Blueprint $table) {
            $table->decimal('commission_amount', 4, 2)->change();
            $table->decimal('commission_percentage', 4, 2)->change();
        });
    }
}
