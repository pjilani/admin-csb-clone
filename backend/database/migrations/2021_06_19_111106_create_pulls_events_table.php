<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePullsEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pulls_events', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->boolean('is_deleted');
            $table->integer('fixture_id');
            $table->integer('fixture_status');
            $table->timestamp('start_date')->nullable();
            $table->timestamp('last_update')->nullable();
            $table->bigInteger('league_id')->index('index_pulls_events_on_league_id');
            $table->bigInteger('location_id')->index('index_pulls_events_on_location_id');
            $table->bigInteger('sport_id')->index('index_pulls_events_on_sport_id');
            $table->jsonb('livescore')->nullable();
            $table->jsonb('market')->nullable();
            $table->string('league_name_de')->nullable();
            $table->string('league_name_en')->nullable();
            $table->string('league_name_fr')->nullable();
            $table->string('league_name_ru')->nullable();
            $table->string('league_name_tr')->nullable();
            $table->string('location_name_de')->nullable();
            $table->string('location_name_en')->nullable();
            $table->string('location_name_fr')->nullable();
            $table->string('location_name_ru')->nullable();
            $table->string('location_name_tr')->nullable();
            $table->string('sport_name_de')->nullable();
            $table->string('sport_name_en')->nullable();
            $table->string('sport_name_fr')->nullable();
            $table->string('sport_name_ru')->nullable();
            $table->string('sport_name_tr')->nullable();
            $table->boolean('is_event_blacklisted');
            $table->string('league_name_nl')->nullable();
            $table->string('location_name_nl')->nullable();
            $table->string('sport_name_nl')->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pulls_events');
    }
}
