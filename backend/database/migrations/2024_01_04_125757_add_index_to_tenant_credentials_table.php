<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexToTenantCredentialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_credentials', function (Blueprint $table) {
            $table->index(['key', 'tenant_id'], 'tenant_credentials_key_tenant_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_credentials', function (Blueprint $table) {
            $table->dropIndex('tenant_credentials_key_tenant_id');
        });
    }
}
