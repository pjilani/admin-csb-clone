<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePullsLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pulls_locations', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->boolean('is_deleted');
            $table->integer('location_id');
            $table->string('name_de')->nullable();
            $table->string('name_en')->nullable();
            $table->string('name_fr')->nullable();
            $table->string('name_ru')->nullable();
            $table->string('name_tr')->nullable();
            $table->string('name_nl')->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pulls_locations');
    }
}
