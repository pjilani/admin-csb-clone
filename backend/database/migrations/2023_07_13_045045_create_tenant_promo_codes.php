<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenantPromoCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_promo_codes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('tenant_id')->comment('Tenant admin id'); 
            $table->string('promo_name')->comment('name of promo code')->nullable();
            $table->string('code')->comment('promo code')->nullable();
            $table->string('promo_code_used_in')->comment('Where will this promo code show: like Instagram, facebook and etc')->nullable();
            $table->text('description')->comment('promo code description')->nullable();
            $table->timestamp('valid_from')->comment('Valid start date')->nullable();
            $table->timestamp('valid_till')->comment('Valid End Date')->nullable();
            $table->integer('promo_code_bonus_type')->comment('0-> flat, 1-> upto')->default('0')->nullable();
            $table->integer('bonus_amount')->comment('This amount user will get in non cash chips')->default('0')->nullable();
            $table->boolean('status')->comment('0-> deactivate 1-> active')->default(1)->nullable();
            $table->integer('total_usage')->comment('Number of usage count')->default(0)->nullable();
            $table->integer('total_number_of_bonus_till_now')->comment('Total Number of bonus user get till now')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_promo_codes');
    }
}
