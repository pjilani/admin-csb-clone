<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConversionHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conversion_history', function (Blueprint $table) {
            $table->id();
            $table->integer('currency_id');
            $table->decimal('old_exchange_rate',12,5);
            $table->decimal('new_exchange_rate',12,5);
            $table->integer('old_currency_response_id');
            $table->integer('new_currency_response_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conversion_history');
    }
}
