<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenantPaymentConfigurations extends Migration
{
  protected $table = 'tenant_payment_configurations';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if (!Schema::hasTable($this->table)) {
        Schema::create($this->table, function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('tenant_id')->nullable();
            $table->bigInteger('provider_id')->nullable();
            $table->jsonb('provider_key_values')->nullable();
            $table->string('parent_type')->nullable(); // superAdminUser, adminUser
            $table->bigInteger('parent_id')->nullable(); // id
            $table->boolean('active')->default(true)->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_payment_configurations');
    }
}
