<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenantConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_configurations', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('tenant_id')->nullable()->index('index_tenant_configurations_on_tenant_id');
            $table->text('allowed_currencies')->nullable()->default('{}');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
            $table->text('report_settings')->nullable();
            $table->boolean('is_bet_disabled')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_configurations');
    }
}
