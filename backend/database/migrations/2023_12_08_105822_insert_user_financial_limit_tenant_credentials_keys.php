<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class InsertUserFinancialLimitTenantCredentialsKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('tenant_credentials_keys')->insert([
            [
                'name' => 'USER_MAX_WITHDRAWAL_LIMIT',
                'description' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'USER_MIN_DEPOSIT_AMOUNT',
                'description' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'USER_MAX_DEPOSIT_AMOUNT',
                'description' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('tenant_credentials_keys')->whereIn('name', ['USER_MAX_WITHDRAWAL_LIMIT', 'USER_MIN_DEPOSIT_AMOUNT', 'USER_MAX_DEPOSIT_AMOUNT'])->delete();
    }
}
