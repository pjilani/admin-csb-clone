<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenantBlockedEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_blocked_events', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('tenant_id')->nullable()->index('index_tenant_blocked_events_on_tenant_id');
            $table->bigInteger('admin_user_id')->nullable()->index('index_tenant_blocked_events_on_admin_user_id');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
            $table->bigInteger('pulls_event_fixture_id')->nullable()->index('index_tenant_blocked_events_on_pulls_event_fixture_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_blocked_events');
    }
}
