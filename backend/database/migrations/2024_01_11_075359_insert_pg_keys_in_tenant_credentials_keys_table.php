<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class InsertPgKeysInTenantCredentialsKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('tenant_credentials_keys')->insert([
            [
                'name' => 'PG_LAUNCH_URL',
                'description' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'PG_OPERATOR_KEY',
                'description' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'PG_LOBBY_URL',
                'description' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('tenant_credentials_keys')->whereIn('name', ['PG_LAUNCH_URL', 'PG_OPERATOR_KEY', 'PG_LOBBY_URL'])->delete();
    }
}
