<?php

use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;
use App\Models\TenantCredentialsKeys;
use App\Models\TenantCredentials;

class AddAppTwilioPhoneDataToTenantCredentialsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (TenantCredentialsKeys::where('name', 'APP_TWILIO_PHONE')->count() == 0) {
            TenantCredentialsKeys::insert(
                array(
                    'name' => 'APP_TWILIO_PHONE',
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString()
                )
            );
        }

        if (TenantCredentials::where(['key'=> 'APP_TWILIO_PHONE', 'tenant_id' => 1])->count() == 0) {
            TenantCredentials::insert(
                array(
                    'key' => 'APP_TWILIO_PHONE',
                    'tenant_id' => 1,
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString()
                )
            );
        }

        if (TenantCredentials::where(['key'=> 'APP_TWILIO_PHONE', 'tenant_id' => 2])->count() == 0) {
            TenantCredentials::insert(
                array(
                    'key' => 'APP_TWILIO_PHONE',
                    'tenant_id' => 2,
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString()
                )
            );
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {  }

}
