<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMonthWiseGgrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('month_wise_ggr', function (Blueprint $table) {
            $table->id();
            $table->decimal('total_bet', 10, 4);
            $table->decimal('total_bet_eur', 10, 4);
            $table->decimal('total_win', 10, 4);
            $table->decimal('total_win_eur', 10, 4);
            $table->decimal('total_ggr', 10, 4);
            $table->decimal('total_ggr_eur', 10, 4);
            $table->string('currency');
            $table->decimal('exchange_rate', 12, 5);
            $table->bigInteger('tenant_id');
            $table->bigInteger('agent_id')->nullable();
            $table->string('month');
            $table->string('year');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('month_wise_ggr');
    }
}