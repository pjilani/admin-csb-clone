<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnSport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pulls_leagues', function (Blueprint $table) {
            if (!Schema::hasColumn('pulls_leagues', 'tenant_id')
            ) {
                Schema::table('pulls_leagues', function (Blueprint $table) {
                    $table->integer('tenant_id')->nullable(false);
                });
            }
        });
        Schema::table('pulls_sports', function (Blueprint $table) {
            if (!Schema::hasColumn('pulls_sports', 'tenant_id')
            ) {
                Schema::table('pulls_sports', function (Blueprint $table) {
                    $table->integer('tenant_id')->nullable(false);
                });
            }
        });
        Schema::table('pulls_events', function (Blueprint $table) {
            if (!Schema::hasColumn('pulls_events', 'tenant_id')
            ) {
                Schema::table('pulls_events', function (Blueprint $table) {
                    $table->integer('tenant_id')->nullable(false);
                });
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pulls_leagues', function (Blueprint $table) {
            $table->dropColumn('tenant_id');
        });
        Schema::table('pulls_sports', function (Blueprint $table) {
            $table->dropColumn('tenant_id');
        });
        Schema::table('pulls_events', function (Blueprint $table) {
            $table->dropColumn('tenant_id');
        });
    }
}
