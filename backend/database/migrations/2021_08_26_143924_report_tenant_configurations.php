<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReportTenantConfigurations extends Migration
{
    protected $table='tenant_report_configurations';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

            Schema::create($this->table, function (Blueprint $table) {
                $table->id();
                $table->bigInteger('tenant_id')->nullable();
                $table->bigInteger('report_id')->nullable();
                $table->foreign('tenant_id', 'fk_tenant_id_report')
                    ->references('id')->on('tenants')->onUpdate('RESTRICT')->onDelete('RESTRICT');
                $table->foreign('report_id', 'fk_tenant_id_report_id')
                    ->references('id')->on('report_master')->onUpdate('RESTRICT')->onDelete('RESTRICT');
                $table->timestamps();
            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
