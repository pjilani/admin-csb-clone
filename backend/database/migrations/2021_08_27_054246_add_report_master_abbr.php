<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReportMasterAbbr extends Migration
{
    protected $table='report_master';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->table, function (Blueprint $table) {
            if (!Schema::hasColumn($this->table, 'report_abbr')) {
                $table->string('report_abbr')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn($this->table,'report_abbr'))
        {
            Schema::hasTable($this->table, function (Blueprint $table) {
                $table->dropColumn('report_abbr');

            });
        }
    }
}
