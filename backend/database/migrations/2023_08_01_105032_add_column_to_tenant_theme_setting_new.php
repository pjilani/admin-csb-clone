<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToTenantThemeSettingNew extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_theme_settings', function (Blueprint $table) {
            //
        $table->string('google_analytics_scripts_code')->nullable()->after('domain');
        $table->string('chatbot_token')->nullable()->after('domain');
        $table->json('google_recaptcha_keys')->nullable()->after('domain');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_theme_settings', function (Blueprint $table) {
            //
            $table->dropColumn('google_analytics_scripts_code');
            $table->dropColumn('chatbot_token');
            $table->dropColumn('google_recaptcha_keys');

        });
    }
}
