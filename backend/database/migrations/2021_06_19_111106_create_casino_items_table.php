<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCasinoItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('casino_items', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->string('uuid');
            $table->string('name')->nullable();
            $table->string('image')->nullable();
            $table->string('item_type')->nullable();
            $table->integer('item_order')->nullable();
            $table->string('provider')->nullable();
            $table->string('technology')->nullable();
            $table->boolean('has_lobby')->nullable();
            $table->boolean('is_mobile')->nullable();
            $table->boolean('has_free_spins')->nullable();
            $table->boolean('active')->nullable()->default(1);
            $table->boolean('featured')->nullable()->default(1);
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
            $table->bigInteger('tenant_id')->nullable()->index('index_casino_items_on_tenant_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('casino_items');
    }
}
