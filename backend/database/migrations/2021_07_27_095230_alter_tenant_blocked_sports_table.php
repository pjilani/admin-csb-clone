<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTenantBlockedSportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

     protected $table = "tenant_blocked_sports";
    public function up()
    {
        
        Schema::table($this->table, function (Blueprint $table) {

            if (!Schema::hasColumn($this->table,'is_deleted'))
            {
                $table->boolean('is_deleted')->nullable();
               
            }

        });

        Schema::table($this->table, function (Blueprint $table) { 
            $table->bigInteger('admin_user_id')->nullable()
                ->comment('It stores Super Admin User id from super_admin_users')
                ->change();
          });

          Schema::table($this->table, function (Blueprint $table) {
            $table->foreign('admin_user_id')->references('id')->on('super_admin_users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('tenant_id')->references('id')->on('tenants')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('pulls_sport_id')->references('id')->on('pulls_sports')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn($this->table,'is_deleted'))
        {
            Schema::hasTable($this->table, function (Blueprint $table) {
                $table->dropColumn('is_deleted');
            
        });
        }

        Schema::table($this->table, function (Blueprint $table) {
            $table->dropForeign('admin_user_id');
        });
    }
}
