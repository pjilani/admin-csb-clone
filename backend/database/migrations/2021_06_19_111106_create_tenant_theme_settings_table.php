<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenantThemeSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_theme_settings', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('tenant_id')->index('index_tenant_theme_settings_on_tenant_id');
            $table->bigInteger('layout_id')->nullable()->index('index_tenant_theme_settings_on_layout_id');
            $table->bigInteger('language_id')->nullable()->index('index_tenant_theme_settings_on_language_id');
            $table->json('theme')->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
            $table->string('logo_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_theme_settings');
    }
}
