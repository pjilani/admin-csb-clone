<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToTenantThemeSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_theme_settings', function (Blueprint $table) {
            $table->integer('max_attempts')->nullable();
            $table->integer('attempt_expiry_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_theme_settings', function (Blueprint $table) {
            $table->dropColumn('max_attempts');
            $table->dropColumn('attempt_expiry_time');
        });
    }
}
