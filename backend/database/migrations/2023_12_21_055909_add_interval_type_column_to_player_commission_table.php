<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIntervalTypeColumnToPlayerCommissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('player_commission', function (Blueprint $table) {
            $table->string('interval_type')->nullable();
            // $table->string('range')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('player_commission', function (Blueprint $table) {
            $table->dropColumn('interval_type');
        });
    }
}