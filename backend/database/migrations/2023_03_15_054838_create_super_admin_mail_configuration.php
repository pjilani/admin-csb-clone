<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuperAdminMailConfiguration extends Migration
{
  protected $table = 'super_admin_mail_configuration';
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    if (!Schema::hasTable($this->table)) {
      Schema::create($this->table, function (Blueprint $table) {
          $table->bigInteger('id', true);
          $table->string('display_key')->nullable(); // use for label in form
          $table->string('key_provided_by_account')->nullable(); // use form send mail and configuration
          $table->string('value')->nullable(); // updated by admin 
          $table->string('parent_type')->nullable(); // Super_admin
          $table->bigInteger('last_updated_parent_id')->nullable(); // id
          $table->timestamp('created_at');
          $table->timestamp('updated_at');
      });
  }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('super_admin_mail_configuration');
  }
}
