<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddErrorColumnToDepositWithdrawUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deposit_withdraw_users', function (Blueprint $table) {
            if (!Schema::hasColumn('deposit_withdraw_users', 'error')) {
                $table->jsonb('error')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('deposit_withdraw_users', 'error')) {
            Schema::table('deposit_withdraw_users', function (Blueprint $table) {
                $table->dropColumn('error');
  
            });
        }
    }
}
