<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReportMaster extends Migration
{
    protected $table='report_master';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table($this->table, function (Blueprint $table) {
            Schema::create($this->table, function (Blueprint $table) {
                $table->id();
                $table->string('name')->nullable();
                $table->boolean('active')->nullable()->default(true);
                $table->timestamps();
            });

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::dropIfExists($this->table);
    }
}
