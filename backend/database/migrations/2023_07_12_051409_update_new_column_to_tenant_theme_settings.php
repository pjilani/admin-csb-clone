<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateNewColumnToTenantThemeSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_theme_settings', function (Blueprint $table) {
            //
            $table->string('signup_popup_image_url')->nullable()->comment('Registration Popup side image')->after('fab_icon_url');
            $table->string('whatsapp_number')->nullable()->comment('Used for Registration Popup')->after('fab_icon_url');
            $table->string('powered_by')->nullable()->after('fab_icon_url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_theme_settings', function (Blueprint $table) {
            //
            $table->dropColumn('signup_popup_image_url');
            $table->dropColumn('whatsapp_number');
            $table->dropColumn('powered_by');
        });
    }
}
