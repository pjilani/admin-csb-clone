<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUsersAddCityZipCodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            if (!Schema::hasColumn('users', 'city')) {
                $table->string('city')->nullable();
            }
            if (!Schema::hasColumn('users', 'zip_code')) {
                $table->string('zip_code')->nullable();
            }
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('users', 'city')) {
            Schema::hasTable('users', function (Blueprint $table) {
                $table->dropColumn('city');

            });
        }

        if (!Schema::hasColumn('users', 'zip_code')) {
            Schema::hasTable('users', function (Blueprint $table) {
                $table->dropColumn('zip_code');

            });
        }
    }
}
