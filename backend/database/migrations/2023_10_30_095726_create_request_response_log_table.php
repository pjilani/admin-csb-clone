<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestResponseLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('request_response_log')) {
            Schema::create('request_response_log', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->jsonb('request_json')->nullable(true);
                $table->jsonb('response_json')->nullable(true);
                $table->string('service')->nullable(true);
                $table->string('url')->nullable(true);
               $table->bigInteger('tenant_id')->nullable(true);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_response_log');
    }
}
