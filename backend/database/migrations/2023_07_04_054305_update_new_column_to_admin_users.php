<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateNewColumnToAdminUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_users', function (Blueprint $table) {
            //
          $table->boolean('is_applied_ip_whitelist')->nullable()->comment('Yes-> 1 , No-> 0')->default(false)->after('parent_id');
          $table->string('ip_whitelist_type')->nullable()->comment('global,manual')->after('parent_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admin_users', function (Blueprint $table) {
            //
            $table->dropColumn('is_applied_ip_whitelist');
            $table->dropColumn('ip_whitelist_type');

        });
    }
}
