<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenantSiteMaintenance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_site_maintenance', function (Blueprint $table) {
          $table->bigInteger('id', true);
          $table->bigInteger('tenant_id'); 
          $table->string('type')->nullable(); // maintenance
          $table->string('title')->nullable(); 
          $table->string('announcement_title')->nullable();
          $table->boolean('is_announcement_active')->default(false)->nullable(); 
          $table->boolean('site_down')->default(false); 
          $table->timestamp('created_at');
          $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_site_maintenance');
    }
}
