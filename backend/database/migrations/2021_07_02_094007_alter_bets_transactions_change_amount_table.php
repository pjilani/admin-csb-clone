<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterBetsTransactionsChangeAmountTable extends Migration
{
    protected $table="bets_transactions";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->table, function (Blueprint $table) {
            if (!Schema::hasColumn($this->table, 'amount')) {
                $table->double('amount', 15, 4)->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn($this->table, 'amount')) {
            Schema::hasTable($this->table, function (Blueprint $table) {
                $table->dropColumn('amount');

            });
        }
    }
}
