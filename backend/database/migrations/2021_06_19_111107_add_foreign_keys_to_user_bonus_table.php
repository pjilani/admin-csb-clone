<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToUserBonusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_bonus', function (Blueprint $table) {
            $table->foreign('bonus_id', 'fk_rails_24e6b25e44')->references('id')->on('bonus')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('user_id', 'fk_rails_b571113cee')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_bonus', function (Blueprint $table) {
            $table->dropForeign('fk_rails_24e6b25e44');
            $table->dropForeign('fk_rails_b571113cee');
        });
    }
}
