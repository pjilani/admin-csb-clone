<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableUserBankDetails extends Migration
{
  protected $table = 'user_bank_details';
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    if (!Schema::hasTable($this->table)) {
      Schema::create($this->table, function (Blueprint $table) {
          $table->bigInteger('id', true);
          $table->string('bank_name')->nullable(); 
          $table->string('bank_ifsc_code')->nullable(); 
          $table->string('account_number')->nullable();  
          $table->string('name')->nullable(); 
          $table->string('phone_number')->nullable(); 
          $table->string('status')->default('active')->nullable(); 
          $table->bigInteger('user_id')->nullable(); 
          $table->bigInteger('tenant_id')->nullable();
          $table->timestamp('created_at');
          $table->timestamp('updated_at');
      });
  }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('user_bank_details');
  }
}
