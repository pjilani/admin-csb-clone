<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationReceiversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_receivers', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->string('receiver_type');
            $table->bigInteger('receiver_id');
            $table->bigInteger('notification_id')->index('index_notification_receivers_on_notification_id');
            $table->boolean('is_read')->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
            $table->index(['receiver_type', 'receiver_id'], 'index_notification_receivers_on_receiver_type_and_receiver_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_receivers');
    }
}
