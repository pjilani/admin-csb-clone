<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
            $table->boolean('email_verified')->nullable()->default(0);
            $table->string('encrypted_password')->nullable();
            $table->string('phone')->nullable();
            $table->boolean('phone_verified')->nullable()->default(0);
            $table->timestamp('date_of_birth')->nullable();
            $table->string('gender')->nullable();
            $table->string('locale')->nullable();
            $table->integer('sign_in_count')->default(0);
            $table->ipAddress('sign_in_ip')->nullable();
            $table->string('parent_type')->nullable();
            $table->bigInteger('parent_id')->nullable();
            $table->timestamp('created_at', 6);
            $table->timestamp('updated_at', 6);
            $table->string('user_name')->nullable();
            $table->string('country_code')->nullable();
            $table->bigInteger('tenant_id')->nullable()->index('index_users_on_tenant_id');
            $table->boolean('active')->nullable()->default(1);
            $table->timestamp('last_login_date')->nullable();
            $table->string('self_exclusion')->nullable();
            $table->softDeletes();
            $table->integer('vip_level')->nullable()->default(0);
            $table->string('nick_name')->nullable();
            $table->timestamp('disabled_at', 6)->nullable();
            $table->string('disabled_by_type')->nullable();
            $table->bigInteger('disabled_by_id')->nullable();
            $table->string('phone_code')->nullable();
            $table->boolean('demo')->nullable()->default(0);
            $table->string('city')->nullable();
            $table->string('zip_code')->nullable();
            $table->boolean('kyc_done')->nullable();
            $table->index(['parent_type', 'parent_id'], 'index_users_on_parent_type_and_parent_id');
            $table->unique(['tenant_id', 'email'], 'index_users_on_tenant_id_and_email');
            $table->index(['disabled_by_type', 'disabled_by_id'], 'index_users_on_disabled_by_type_and_disabled_by_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
