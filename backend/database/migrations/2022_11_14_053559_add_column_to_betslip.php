<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToBetslip extends Migration
{
  protected $table = 'bets_betslip';
  protected $columnsName = 'settlement_status';

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
      Schema::table($this->table, function (Blueprint $table) {
          if (!Schema::hasColumn($this->table, $this->columnsName)
          ) {
              $table->string($this->columnsName, 255)->nullable();
          }
      });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      if (
      Schema::hasColumn($this->table, $this->columnsName)
      ) {
          Schema::table($this->table, function (Blueprint $table) {
              $table->dropColumn($this->columnsName);

          });
      }
  }
}
