<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBetsBetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bets_bets', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->boolean('is_deleted');
            $table->string('bet_id');
            $table->integer('fixture_id');
            $table->integer('provider_id');
            $table->string('champ');
            $table->string('match');
            $table->string('market');
            $table->string('name');
            $table->decimal('price', 15,4);
            $table->timestamp('start_date')->nullable();
            $table->bigInteger('betslip_id')->index('index_bets_bets_on_betslip_id');
            $table->integer('market_id');
            $table->integer('bet_status');
            $table->bigInteger('event_id')->nullable()->index('index_bets_bets_on_event_id');
            $table->integer('settlement_status')->nullable();
            $table->jsonb('livescore')->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bets_bets');
    }
}
