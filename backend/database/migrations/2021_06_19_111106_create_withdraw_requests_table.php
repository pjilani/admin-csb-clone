<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWithdrawRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdraw_requests', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('user_id')->nullable()->index('index_withdraw_requests_on_user_id');
            $table->string('status')->nullable()->default('pending');
            $table->string('name')->nullable();
            $table->string('account_number')->nullable();
            $table->string('ifsc_code')->nullable();
            $table->float('amount')->nullable();
            $table->string('phone_number')->nullable();
            $table->integer('transaction_id')->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
            $table->string('actionable_type')->nullable();
            $table->bigInteger('actionable_id')->nullable();
            $table->timestamp('actioned_at')->nullable();
            $table->bigInteger('tenant_id')->index('index_withdraw_requests_on_tenant_id');
            $table->index(['actionable_type', 'actionable_id'], 'index_withdraw_requests_on_actionable_type_and_actionable_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdraw_requests');
    }
}
