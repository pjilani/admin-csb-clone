<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToPageBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('page_banners', function (Blueprint $table) {
            //
        });
        Schema::table('page_banners', function (Blueprint $table) {
            if (!Schema::hasColumn('page_banners', 'game_name')) {
                $table->string('game_name')->nullable();
            }

            if (!Schema::hasColumn('page_banners', 'open_table')) {
                $table->string('open_table')->nullable();
            }

            if (!Schema::hasColumn('page_banners', 'provider_name')) {
                $table->string('provider_name')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('page_banners', function (Blueprint $table) {
            $table->dropColumn('game_name');
            $table->dropColumn('open_table');
            $table->dropColumn('provider_name');
        });
    }
}
