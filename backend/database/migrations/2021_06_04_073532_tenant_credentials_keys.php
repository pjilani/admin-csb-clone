<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\TenantCredentialsKeys as TenantCredentialsKeysModel;

class TenantCredentialsKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_credentials_keys', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->string('name')->unique();
            $table->string('description')->nullable();
            $table->timestamps();
        });
        TenantCredentialsKeysModel::insert(
            [
            ['name' => 'APP_TWILIO_ACCOUNT_SID'],
            ['name' => 'APP_TWILIO_SERVICE_ID'],
            ['name' => 'APP_TWILIO_AUTH_TOKEN'],
            ['name' => 'APP_SENDGRID_RELAY_KEY'],
            ['name' => 'APP_SENDGRID_HOST'],
            ['name' => 'APP_SENDGRID_PORT'],
            ['name' => 'APP_SENDGRID_USERNAME'],
            ['name' => 'APP_SENDGRID_EMAIL'],
            ['name' => 'APP_EZUGI_OPERATOR_ID'],
            ['name' => 'APP_EZUGI_HASH_KEY'],
            ['name' => 'APP_EZUGI_API_USERNAME'],
            ['name' => 'APP_EZUGI_API_ID'],
            ['name' => 'APP_EZUGI_API_ACCESS_TOKEN']
            ]
        );
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_credentials_keys');
    }
}
