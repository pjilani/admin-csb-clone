<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterBetsBetsChangePriceTable extends Migration
{
    protected $table='bets_bets';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::hasTable($this->table, function (Blueprint $table) {
            if (Schema::hasColumn($this->table, 'price')) {
                $table->decimal('price', 15, 4)->change();
            } else {
                $table->decimal('price', 15, 4);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn($this->table, 'price')) {
            Schema::hasTable($this->table, function (Blueprint $table) {
                $table->dropColumn('price');
            });
        }
    }
}
