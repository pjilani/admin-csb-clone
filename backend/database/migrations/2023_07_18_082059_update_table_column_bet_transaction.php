<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableColumnBetTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('bets_transactions', function (Blueprint $table) {
          //
          $table->string('transaction_id')->nullable()->after('tenant_id');
          $table->string('reverse_transaction_id')->nullable()->after('tenant_id');
          $table->string('transaction_code')->nullable()->after('tenant_id');
          $table->string('runner_name')->nullable()->after('tenant_id');
          $table->string('market_id')->nullable()->after('tenant_id');
          $table->double('net_pl')->nullable()->after('tenant_id');
          $table->double('commission_per')->nullable()->after('tenant_id');
          $table->double('commission_amount')->nullable()->after('tenant_id');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bets_transactions', function (Blueprint $table) {
            //
            $table->dropColumn('transaction_id');
            $table->dropColumn('reverse_transaction_id');
            $table->dropColumn('transaction_code');
            $table->dropColumn('runner_name');
            $table->dropColumn('market_id');
            $table->dropColumn('net_pl');
            $table->dropColumn('commission_per');
            $table->dropColumn('commission_amount');
        });
    }
}
