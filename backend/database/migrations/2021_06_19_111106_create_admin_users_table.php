<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_users', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('phone')->nullable();
            $table->boolean('phone_verified')->nullable()->default(0);
            $table->string('parent_type')->nullable();
            $table->bigInteger('parent_id')->nullable();
            $table->string('email')->default('')->unique('index_admin_users_on_email');
            $table->string('encrypted_password')->default('');
            $table->string('reset_password_token')->nullable()->unique('index_admin_users_on_reset_password_token');
            $table->timestamp('reset_password_sent_at')->nullable();
            $table->timestamp('remember_created_at')->nullable();
            $table->string('confirmation_token')->nullable()->unique('index_admin_users_on_confirmation_token');
            $table->timestamp('confirmed_at')->nullable();
            $table->timestamp('confirmation_sent_at')->nullable();
            $table->string('unconfirmed_email')->nullable();
            $table->timestamp('created_at', 6);
            $table->timestamp('updated_at', 6);
            $table->bigInteger('tenant_id')->nullable()->index('index_admin_users_on_tenant_id');
            $table->string('agent_name')->nullable();
            $table->boolean('active')->nullable()->default(1);
            $table->integer('deactivated_by_id')->nullable();
            $table->string('deactivated_by_type')->nullable();
            $table->timestamp('deactivated_at')->nullable();
            $table->text('allowed_currencies')->nullable()->default('{}');
            $table->string('remember_token')->nullable();
            $table->boolean('kyc_regulated')->nullable()->default(0);
            $table->integer('login_count')->default(0);
            $table->index(['parent_type', 'parent_id'], 'index_admin_users_on_parent_type_and_parent_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        Schema::dropIfExists('admin_users');
    }
}
