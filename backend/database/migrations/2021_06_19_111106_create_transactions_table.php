<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->string('actionee_type')->nullable();
            $table->bigInteger('actionee_id')->nullable();
            $table->bigInteger('source_wallet_id')->nullable()->index('index_transactions_on_source_wallet_id');
            $table->bigInteger('target_wallet_id')->nullable()->index('index_transactions_on_target_wallet_id');
            $table->bigInteger('source_currency_id')->nullable()->index('index_transactions_on_source_currency_id');
            $table->bigInteger('target_currency_id')->nullable()->index('index_transactions_on_target_currency_id');
            $table->decimal('conversion_rate', 10, 0)->nullable();
            $table->float('amount')->nullable()->default(0.0);
            $table->float('source_before_balance')->nullable()->default(0.0);
            $table->float('source_after_balance')->nullable()->default(0.0);
            $table->float('target_before_balance')->nullable()->default(0.0);
            $table->float('target_after_balance')->nullable()->default(0.0);
            $table->string('status')->nullable();
            $table->string('comments')->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
            $table->bigInteger('tenant_id')->nullable()->index('index_transactions_on_tenant_id');
            $table->string('transaction_id')->nullable();
            $table->string('timestamp')->nullable();
            $table->integer('transaction_type')->nullable();
            $table->boolean('success')->nullable();
            $table->integer('server_id')->nullable();
            $table->bigInteger('round_id')->nullable();
            $table->integer('game_id')->nullable();
            $table->integer('table_id')->nullable();
            $table->integer('bet_type_id')->nullable();
            $table->string('seat_id', 10)->nullable();
            $table->integer('platform_id')->nullable();
            $table->integer('error_code')->nullable();
            $table->string('error_description', 100)->nullable();
            $table->integer('return_reason')->nullable();
            $table->boolean('is_end_round')->nullable();
            $table->string('credit_index', 10)->nullable();
            $table->string('debit_transaction_id')->nullable();
            $table->string('payment_method')->nullable();
            $table->index(['actionee_type', 'actionee_id'], 'index_transactions_on_actionee_type_and_actionee_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
