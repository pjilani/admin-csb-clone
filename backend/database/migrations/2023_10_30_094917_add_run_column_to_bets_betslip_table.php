<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRunColumnToBetsBetslipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bets_betslip', function (Blueprint $table) {
            if (!Schema::hasColumn('bets_betslip', 'run')) {
                    $table->decimal('run', 15, 4)->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('bets_betslip', 'run')) {
            Schema::table('bets_betslip', function (Blueprint $table) {
                $table->dropColumn('run');
            });
        }
    }
}
