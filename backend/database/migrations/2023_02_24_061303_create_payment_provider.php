<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentProvider extends Migration
{
  protected $table = 'payment_providers';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if (!Schema::hasTable($this->table)) {
        Schema::create($this->table, function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->string('provider_name')->nullable();
            $table->jsonb('provider_keys')->nullable();
            $table->string('logo')->nullable();
            $table->boolean('active')->default(true)->nullable();
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }
  }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
