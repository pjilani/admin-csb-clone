<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestResponseLogsTableForBackupDb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('request_response_logs', function (Blueprint $table) {
            $table->id();
            $table->jsonb('request_json')->nullable();
            $table->jsonb('response_json')->nullable();
            $table->string('service', 255)->nullable();
            $table->string('url', 255)->nullable();
            $table->bigInteger('tenant_id')->nullable();
            $table->string('response_code', 255)->nullable();
            $table->string('response_status', 255)->nullable();
            $table->string('error_code', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_response_logs');
    }
}
