<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnToAdminUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_users', function (Blueprint $table) {
            //
          $table->string('affiliate_token')->nullable()->comment('Agent Affiliate Token for user onboard')->after('parent_id');
          $table->jsonb('ip_whitelist')->nullable()->comment('Agent Ip WhiteLIst')->after('parent_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admin_users', function (Blueprint $table) {
            //
            $table->dropColumn('affiliate_token');
            $table->dropColumn('ip_whitelist');

        });
    }
}
