<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFlatColumnsInBonus extends Migration
{
    protected $table = 'bonus';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->table, function (Blueprint $table) {
            if (!Schema::hasColumn($this->table, 'flat')) {
                $table->bigInteger('flat')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn($this->table, 'flat')) {
            Schema::table($this->table, function (Blueprint $table) {
                $table->dropColumn('flat');

            });
        }
    }
}
