<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTwoColumnsInTenantThemeSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant_theme_settings', function (Blueprint $table) {
            $table->string('google_tag_manager_code')->nullable()->after('google_recaptcha_keys');
            $table->string('google_search_console_code')->nullable()->after('google_recaptcha_keys');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_theme_settings', function (Blueprint $table) {
            $table->dropColumn('google_tag_manager_code');
            $table->dropColumn('google_search_console_code');
        });
    }
}
