<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableColumnBetBets extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    //
    Schema::table('bets_bets', function (Blueprint $table) {
      //
      $table->integer('fixture_id')->nullable(true)->change();
      $table->integer('provider_id')->nullable(true)->change();
      $table->string('champ')->nullable(true)->change();
      $table->string('match')->nullable(true)->change();
      $table->string('name')->nullable(true)->change();
      $table->string('market')->nullable(true)->change();
      $table->string('price')->nullable(true)->change();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    //
    Schema::table('bets_bets', function (Blueprint $table) {

      $table->string('fixture_id')->nullable(false)->change();
      $table->string('provider_id')->nullable(false)->change();
      $table->string('champ')->nullable(false)->change();
      $table->string('match')->nullable(false)->change();
      $table->string('market')->nullable(false)->change();
      $table->string('name')->nullable(false)->change();
      $table->string('price')->nullable(false)->change();
    });
  }
}
