<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class InsertPlayerCommissionMaxPercentageInTenantCredentialKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('tenant_credentials_keys')->insert([
            [
                'name' => 'PLAYER_COMMISSION_MAX_PERCENTAGE',
                'description' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('tenant_credentials_keys')->whereIn('name', ['PLAYER_COMMISSION_MAX_PERCENTAGE'])->delete();
    }
}
