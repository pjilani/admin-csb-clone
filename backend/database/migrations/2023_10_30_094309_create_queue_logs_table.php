<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQueueLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('queue_logs')) {
            Schema::create('queue_logs', function (Blueprint $table) {
                $table->id();
                $table->string('type', 50);
                $table->integer('status')->default(0)->comment('0=>Ready, 1=>In progress, 2=>Done, 3=>Failed');
                $table->jsonb('ids');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('queue_logs');
    }
}
