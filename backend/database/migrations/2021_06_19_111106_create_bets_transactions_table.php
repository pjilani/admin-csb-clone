<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBetsTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('bets_transactions', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->boolean('is_deleted');
            $table->decimal('amount', 10, 4)->nullable();
            $table->string('journal_entry');
            $table->string('status')->nullable();
            $table->string('reference');
            $table->string('description');
            $table->bigInteger('user_id')->index('index_bets_transactions_on_user_id');
            $table->bigInteger('betslip_id')->nullable()->index('index_bets_transactions_on_betslip_id');
            $table->timestamp('created_at', 6);
            $table->timestamp('updated_at', 6);
            $table->bigInteger('tenant_id')->nullable()->index('index_bets_transactions_on_tenant_id');
            $table->string('actionee_type')->nullable();
            $table->bigInteger('source_currency_id')->nullable()->index('index_bets_transactions_on_source_currency_id');
            $table->bigInteger('target_currency_id')->nullable()->index('index_bets_transactions_on_target_currency_id');
            $table->bigInteger('source_wallet_id')->nullable()->index('index_bets_transactions_on_source_wallet_id');
            $table->bigInteger('target_wallet_id')->nullable()->index('index_bets_transactions_on_target_wallet_id');
            $table->decimal('conversion_rate', 10, 0)->nullable();
            $table->integer('payment_for')->nullable()->comment('1-bet placement, 2- won, 3- cashout, 4-refund');
            $table->decimal('non_cash_amount', 10, 0)->nullable();
            $table->decimal('current_balance', 10, 0)->nullable();
            $table->decimal('source_before_balance', 15, 4)->nullable();
            $table->decimal('source_after_balance', 15, 4)->nullable();
            $table->decimal('target_after_balance', 15, 4)->nullable();
            $table->decimal('target_before_balance', 15, 4)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bets_transactions');
    }
}
