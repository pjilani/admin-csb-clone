<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnDepositWithdrawJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deposit_withdraw_job', function (Blueprint $table) {
            if (!Schema::hasColumn('deposit_withdraw_job', 'created_by')) {
                $table->bigInteger('created_by');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('deposit_withdraw_job', 'created_by')) {
                Schema::table('deposit_withdraw_job', function (Blueprint $table) {
                    $table->dropColumn('created_by');
      
                });
            }
    }
}
