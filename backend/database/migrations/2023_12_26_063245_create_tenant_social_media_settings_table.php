<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenantSocialMediaSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_social_media_settings', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('tenant_id');
            $table->string('name');
            $table->string('image');
            $table->string('redirect_url');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_social_media_settings');
    }
}
