<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CreateMonthDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('month_details', function (Blueprint $table) {
            $table->id();
            $table->integer('month_count');
            $table->timestamp('start_date');
            $table->timestamp('end_date');
            $table->timestamps();
        });

        $monthDetails = []; 
        for ($month = 1; $month <= 12; $month++) {
            $startDate = Carbon::create(date('Y'), $month, 1)->startOfMonth();
            $endDate = Carbon::create(date('Y'), $month, 1)->endOfMonth();
        
            $monthDetails[] = [
                'month_count' => $month,
                'start_date' => $startDate->toDateTimeString(),
                'end_date' => $endDate->toDateTimeString(),
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }  
        DB::table('month_details')->insert($monthDetails); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('month_details');
    }
}