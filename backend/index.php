<?php

$errorArray=[];
if (version_compare(PHP_VERSION,"7.4","=")) {
    $errorArray[] = "PHP 7.4  is required. Your Current php version ".PHP_VERSION;
}
if (!extension_loaded('json')) {
    $errorArray[] = "PHP does not have the JSON extension installed.";
}
if (!extension_loaded("pgsql") && !extension_loaded("pgsql")) {
    $errorArray[]  = "PHP does not have the pgsql extension installed.";
}

if(!extension_loaded('gd')){
    $errorArray[]= "'GD\|ImageMagick' PHP extensions not found";
}
if(!extension_loaded('curl')){
    $errorArray[]= "'CURL' PHP extensions not found";
}
if(!extension_loaded('zip')){
    $errorArray[]= "'ZIP' PHP extensions not found";
}

if(!extension_loaded('simplexml')){
    $errorArray[]= "'xml' PHP extensions not found";
}
if(!extension_loaded('mbstring')){
    $errorArray[]= "'mbstring' PHP extensions not found";
}

if(!is_dir('storage/')){
    $errorArray[]= "storage directory not exists";
}elseif (!is_writable(getcwd()."/storage/")) {
    $errorArray[] = "'storage' directory is not writable.";
}

if(!is_dir('bootstrap/cache')){
    $errorArray[]= "bootstrap/cache directory not exists";
}elseif (!is_writable(getcwd()."/bootstrap/cache")) {
    $errorArray[] = "'bootstrap/cache' directory is not writable.";
}

if(!is_dir('storage/app/csv')){
    $errorArray[]= "storage/app/csv directory not exists";
}elseif (!is_writable(getcwd()."/storage/app/csv/")) {
    $errorArray[] = "'storage/app/csv' directory is not writable.";
}

/*$tmp_dir = ini_get('upload_tmp_dir') ? ini_get('upload_tmp_dir') : sys_get_temp_dir();
if(!is_dir($tmp_dir)){
    $errorArray[]= "$tmp_dir  directory not exists ";
}elseif (!is_writable(getcwd().$tmp_dir)) {
    $errorArray[] = "$tmp_dir directory is not writable.";
}*/

/*
if (intval(ini_get('upload_max_filesize')) < 10240) {
    $errorArray[] = "Max upload filesize (upload_max_filesize in php.ini) is currently less than 10240MB.";
}
if (intval(ini_get('post_max_size')) < 10240) {
    $errorArray[] = "Max POST size (post_max_size in php.ini) is currently less than 10240MB. 10240MB or higher is recommended.";
}
if (intval(ini_get("memory_limit")) < 128) {
    $errorArray[] = "PHP's memory limit is currently under 128MB. BigTree recommends at least 128MB of memory be available to PHP.";
}*/
// mod_rewrite check
if (function_exists("apache_get_modules")) {
    $apache_modules = apache_get_modules();
    if (in_array('mod_rewrite', $apache_modules) === false) {
        $errorArray[] = "Apache's mod_rewrite is not installed. Only basic routing is available without mod_rewrite.";
    }
    if (in_array('mod_headers', $apache_modules) === false) {
        $errorArray[] = "Apache's headers is not Enable. Only basic API is available without headers. ";
    }
}

if (count($errorArray) > 0) {
    ?>
    <div style="color: red;text-align: left;">
    <strong>Error !</strong><br> <?= implode('<br>', $errorArray) ?>
    </div><?php
    exit(); // EXIT_ERROR
}

/**
 * Laravel - A PHP Framework For Web Artisans
 *
 * @package  Laravel
 * @author   Taylor Otwell <taylor@laravel.com>
 */

$uri = urldecode(
    parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)
);

// This file allows us to emulate Apache's "mod_rewrite" functionality from the
// built-in PHP web server. This provides a convenient way to test a Laravel
// application without having installed a "real" web server software here.
if ($uri !== '/' && file_exists(__DIR__.'/public'.$uri)) {
    return false;
}

require_once __DIR__.'/public/index.php';
