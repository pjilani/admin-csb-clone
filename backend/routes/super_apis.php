<?php
/* currencies CRUD API*/
require_once "super/currencies/currenciesRoutes.php";

/* currencies CRUD API*/
require_once "super/users/index.php";


require_once "super/admins/adminsRoutes.php";


// Payment Provider List
require_once "super/paymentProviders/paymentProvidersRoutes.php";

/* Transactions CRUD API*/
require_once "super/transactions/transactionRoute.php";

/* PermissionRolesController CRUD API*/
require_once "super/permissionRole/permissionRoleRoutes.php";



/* Layout CRUD API*/
Route::get('/layout','LayoutController@index');
Route::get('/layout/{id}','LayoutController@show');

// bet report
Route::post('/bet-reports', 'SportsController@'.'getBetReports');
Route::post('/betreport', 'SportsController@' . 'getBetReportsByES');
Route::post('/bet-reports/download', 'SportsController@'.'getAllBetReports');

/* ThemesController CRUD API*/
require_once "super/themes/themesRoutes.php";

/*Reports*/
require_once "super/super_report_apis.php";

// sport
require_once "super/sports/sportsRoutes.php";

/* Languages CRUD API*/
require_once "super/languages/languagesRoutes.php";


/* Tenants CRUD API*/

require_once "super/tenants/tenantsRoute.php";


require_once "super/casino/index.php";

require_once "super/menu/menuRoute.php";

Route::post('/aws/get/url','AdminController@getAWSUrl');

// Dashboard Routes
Route::post('dashboard', 'DashboardController@index');


Route::post('active-players', 'DashboardController@activePLayers');
Route::post('active-unique-players-count', 'DashboardController@activeUniquePlayersCount');

Route::post('dashboard/financial-activity', 'DashboardController@financialDashboard');
Route::post('dashboard/casino', 'DashboardController@gamingDashboard');
Route::post('dashboard/sport', 'DashboardController@sportsDashboard');
Route::post('dashboard/player-activity', 'DashboardController@userDashboard');
Route::post('dashboard/player-balance-info', 'DashboardController@userDashboardBalanceInfo');


