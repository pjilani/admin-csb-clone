<?php
// casino/Tables
Route::group(['prefix' => 'casino/pages','namespace' => 'Casino'], function(){
//    Route::post('/','CasinoPagesController@index');
    Route::put('/update-order','CasinoPagesController@updatePageOrder');
    Route::put('/update-menu-order','CasinoPagesController@updatePageMenuOrder');
    Route::get('/{id?}','CasinoPagesController@show');
    Route::get('/byId/{id?}','CasinoPagesController@pagesById');
    Route::get('/{id}/{page_menu_id}','CasinoPagesController@showPagesMenus');

    Route::get('/record/page_menu/{id}/{page_menu_id}','CasinoPagesController@showPageMenu');
    Route::post('/record/page_menu_items','CasinoPagesController@showPageMenuItems');
    Route::post('/record/page_menu/add','CasinoPagesController@createPagesMenus');
    Route::post('/record/page_menu/update/{page_manu_id}','CasinoPagesController@updatePagesMenus');
    Route::delete('/record/page_menu/{manu_id}','CasinoPagesController@deletePagesMenus');

    Route::post('/record/add','CasinoPagesController@store');
    Route::post('/edit','CasinoPagesController@update');
    // Route::delete('/{id}','CasinoPagesController@destroy');

    Route::post('menu-item/add','CasinoPagesController@createPageMenuItem');
    Route::put('menu-item/update/{id}','CasinoPagesController@updatePageMenuItem');
    Route::put('menu-item/update-menu-item-order','CasinoPagesController@updatePageMenuItemOrder');
    Route::delete('menu-item/delete/{id}','CasinoPagesController@deletePageMenuItem');

});
