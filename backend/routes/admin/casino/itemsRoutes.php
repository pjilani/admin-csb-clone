<?php
// items
Route::group(['prefix' => 'casino/items','namespace' => 'Casino'], function(){
    Route::get('/tables','CasinoItemsController@getTablesWithId');
    Route::get('/{id?}','CasinoItemsController@show');
    Route::post('/get', 'CasinoItemsController@getItems');
    Route::post('/add','CasinoItemsController@store');
    Route::post('/edit/{id}','CasinoItemsController@update');
//    Route::delete('/{id}','CasinoItemsController@destroy');

});