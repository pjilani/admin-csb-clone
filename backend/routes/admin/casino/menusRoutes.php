<?php
// Menus
Route::group(['prefix' => 'casino/menus','namespace' => 'Casino'], function(){
//    Route::get('/','CasinoMenusController@index');
    Route::get('/{id?}','CasinoMenusController@show');
    Route::get('/list/all','CasinoMenusController@lists')->name('menus_list');
    Route::post('/add','CasinoMenusController@store');
    Route::post('/update/{id}','CasinoMenusController@update');
//    Route::delete('/{id}','CasinoMenusController@destroy');

});