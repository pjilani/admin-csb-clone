<?php

Route::group(['prefix' => 'user/player'], function(){
    Route::put('/status/{id}/{status}','UserController@statusUserPlayer');
    Route::post('/add','UserController@store');
    Route::post('/login-history/{id}','UserController@loginHistory');
    Route::post('/bank-details/{id}','UserController@bankDetails');
    Route::post('/bank-details/update/{bankId}/{playerId}','UserController@updateBankDetails');
    Route::post('/bank-details/create/{id}','UserController@createBankDetails');
    Route::post('/update','UserController@update');
    Route::get('/{id}','UserController@getPlayer');
    Route::post('/reset-password','UserController@resetPassword');
    Route::post('/bulk-player-edit','UserController@editPlayers');

    Route::put('/update-kyc','UserController@updateKyc');
});

/* Player User setting CRUD API*/
Route::group(['prefix' => 'player/setting'], function(){
    Route::get('/','UserSettingsController@index');
    Route::get('/{id}','UserSettingsController@show');
    Route::post('/','UserSettingsController@store');
    Route::post('/self-exclusion','UserController@selfExclusionUpdate');
    Route::post('/document/status','UserSettingsController@documentsStatus');
    Route::post('/edit/{id}','UserSettingsController@update');
    Route::delete('/{id}','UserSettingsController@destroy');
});



