<?php

/* Users CRUD API*/
Route::post('/users','AdminController@index');
Route::get('/users/{id}','AdminController@show');
Route::post('/users/setting/','AdminController@setting');
Route::delete('/users/setting/{id}','AdminController@deleteSetting');
Route::post('/users/deactive/{id}','AdminController@destroy');

Route::post('/users/active/{id}','AdminController@activeUsers');

Route::post('/usersadmin/add/','AdminController@store');
Route::post('/usersadmin/edit/{id}','AdminController@update');
Route::post('/usersadmin/agent','AdminController@getAdminAgents');
Route::post('/usersadmin/sub-admin','AdminController@getSubadmins');
Route::post('/usersadmin/player','AdminController@getUserPlayer');
Route::post('/usersadmin/player-commission','AdminController@getPlayerCommission');
Route::post('/usersadmin/player-commission/download','AdminController@downloadPlayerCommission');
Route::post('/usersadmin/player/download','AdminController@downloadUserPlayer');
Route::post('/usersadmin/bulkfinance','AdminController@bulkFinance');
Route::post('/usersadmin/bulkEdit','AdminController@bulkEdit');


Route::get('/agent/tree','AdminController@getAdminUserTree');
