<?php


Route::group(['prefix' => 'sports'], function(){
    $controller='SportsController@';

   Route::get('/', $controller.'sports');
  
    Route::get('/all-sports', $controller.'allSports');
    
    Route::post('/countries', $controller.'countries');
    Route::post('/all-countries', $controller.'allCountries');

    Route::get('/leagues', $controller.'leagues');
    Route::get('/leagues/search', $controller.'allLeaguesSearch');

    Route::get('/matches', $controller.'matches');
    Route::get('/all-matches', $controller.'allMatches');
    
    Route::get('/markets', $controller.'markets');

    Route::get('/bets', $controller.'bets');
    
    Route::put('/update/enabled', $controller.'updateSportEnabled');
    
    Route::put('/leagues/update/disabled', $controller.'updateLeagueDisabled');
    Route::put('/leagues/update/enabled', $controller.'updateLeagueEnabled');

    Route::get('/countries/location', $controller.'getSportsCountriesList');

    Route::put('/match/update/enabled', $controller.'updateMatchEnabled');
    Route::put('/match/update/disabled', $controller.'updateMatchDisabled');

});
