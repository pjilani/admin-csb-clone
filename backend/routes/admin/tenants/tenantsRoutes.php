<?php


Route::group(['prefix' => 'tenants'], function(){

    Route::get('/','TenantsController@index');
    Route::get('/credentialsList','TenantsController@getListCredentials');
    Route::post('/registration-field', 'TenantsController@registrationFieldUpdate');
    Route::post('/update-permission', 'TenantsController@updatePermission');
    Route::post('/update-player-commission-interval', 'TenantsController@updatePlayerCommissionInterval');
    Route::get('/credentials','TenantsController@getCredentials');
    Route::post('/credentials','TenantsController@setCredentials');
    Route::post('/credentials/edit/{id}','TenantsController@updateCredentials');
    Route::delete('/credentials/{id}','TenantsController@deleteCredentials');
    Route::post('/theme/update','TenantsController@updateThemeSetting');
    Route::get('/theme/setting','TenantsController@getThemeSetting');
    Route::post('/theme/setting/google/update', 'TenantsController@updateGoogleSetting');


    Route::post('/paymentProvider/edit/{id}', 'TenantsController@updatePaymentProvider');
    Route::get('/paymentProvider', 'TenantsController@getPaymentProvider');
    Route::get('/getregistrationFields', 'TenantsController@getregistrationFields');
    Route::get('/selectedPaymentProvider', 'TenantsController@selectedPaymentProvider');
    Route::post('/paymentProvider/create/new', 'TenantsController@addPaymentProvider');
    Route::post('/tenantPaymentProviderStatus', 'TenantsController@tenantPaymentProviderStatus');
    Route::get('/paymentProvidersById/{id}', 'TenantsController@getPaymentProvidersById');


    Route::get('/banners','TenantsController@getBanners');
    Route::post('/banner','TenantsController@addBanners');
    Route::post('/banner/update','TenantsController@updateBanners');
    Route::put('/banner/update/order','TenantsController@updateBannersOrders');
    Route::delete('/banner/{id}','TenantsController@deleteBanners');
    
    Route::get('/currencies','CurrenciesController@getTenantsCurrencies');
    Route::get('/player-commission-interval','TenantsController@getPlayerCommissionInterval');

    Route::get('/sport-setting','TenantsController@getTenantSportsBetSetting');
    Route::put('/sport-setting/update','TenantsController@updateTenantSportsBetSetting');
    Route::get('/menu/setting','TenantsController@getTenantMenuSetting');
    Route::put('/menu/update/order','TenantsController@updateMenuOrders');
    Route::post('change-password', 'TenantsController@changePassword');
    Route::get('/social-media','TenantsController@getSocialmedia');
    Route::post('/social-media/create','TenantsController@createSocialmedia');
    Route::post('/social-media/update','TenantsController@updateSocialmedia');
    Route::post('/social-media/active/{id}','TenantsController@activateSocialMedia');
    Route::post('/social-media/deactive/{id}','TenantsController@deactivateSocialMedia');
    Route::get('/payment-providers','TenantsController@getPaymentPorviders');

    Route::post('/faqs','TenantsController@getfaqs');
    Route::post('/faq/create','TenantsController@createFaq');
    Route::get('/faqs/{id}','TenantsController@getfaq');
    Route::post('/faq/update/{id}','TenantsController@updateFaq');
    Route::post('/faq/status','TenantsController@updateFaqStatus');
    Route::post('/faq/delete','TenantsController@deleteFaq');

    Route::post('/faqs/categories','TenantsController@getFaqsCategories');
    Route::post('/faqs/categories/create','TenantsController@createFaqsCategories');
    Route::post('/faqs/categories/update','TenantsController@updateFaqsCategories');
    Route::post('/faqs/categories/status','TenantsController@updateFaqsCategoriesStatus');
    Route::get('/faqs/categories/{id}','TenantsController@getFaqsCategory');
    Route::post('/faqs/categories/delete','TenantsController@deleteFaqCategory');

});
