<?php


Route::group(['prefix' => 'cms'], function(){
    Route::get('/list','CmsController@getList');
    Route::post('/add','CmsController@addCmsPages');
    Route::post('/status','CmsController@updateStatus');
});
