<?php


Route::group(['prefix' => 'bank'], function(){
    Route::get('/list','TenantBankController@getList');
    Route::post('/add','TenantBankController@addNewBank');
    Route::post('/status','TenantBankController@updateStatus');
});
