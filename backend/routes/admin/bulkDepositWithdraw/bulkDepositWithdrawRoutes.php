<?php

Route::group(['prefix' => 'bulk'], function(){

    // Route::post('/bulk-finance','BulkDepositWithdrawController@bulkFinance');
    Route::post('/bulk-finance-job','BulkDepositWithdrawController@storeBulkJobs');
    Route::post('/get-jobs','BulkDepositWithdrawController@listJobs');
    Route::post('/jobs/{id}','BulkDepositWithdrawController@getJobById');
    Route::post('/retry/{id}','BulkDepositWithdrawController@retryJob');
    // Route::post('/player-commission/status','BulkDepositWithdrawController@updateBulkStatus');
    Route::post('/player-commission/all','BulkDepositWithdrawController@updateBulkStatusAll');
    // Route::post('/process-bulk-deposit-withdraw','BulkDepositWithdrawController@processBulkDepositWithdraw');
    Route::post('/get-dates','BulkDepositWithdrawController@getDates');
});
