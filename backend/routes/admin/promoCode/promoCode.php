<?php


Route::group(['prefix' => 'promo-code'], function(){
    Route::get('/list','TenantPromoCodeController@getList');
    Route::post('/add','TenantPromoCodeController@addNewPromoCode');
    Route::post('/detail','TenantPromoCodeController@getDetails');
    Route::post('/status','TenantPromoCodeController@updateStatus');
});
