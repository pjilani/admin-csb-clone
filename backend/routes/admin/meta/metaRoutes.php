<?php


Route::group(['prefix' => 'meta'], function(){
    Route::get('/list','MetaController@index');
    Route::post('/add','MetaController@addEdit');
    Route::post('/status','MetaController@delete');
});

