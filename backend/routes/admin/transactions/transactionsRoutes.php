<?php

Route::group(['prefix' => 'transactions'], function(){

    Route::post('/','TransactionsController@index');
    Route::post('record','TransactionsController@indexByES');
    Route::post('/download','TransactionsController@download');
    Route::post('/downloadByES','TransactionsController@downloadByES');
    Route::post('/list','TransactionsController@betList');
    Route::post('/listdownload/','TransactionsController@betListDownload');
    Route::get('/{id}','TransactionsController@show');
    Route::get('/users/new','UserController@searchUsers');
    Route::post('/withdrawal/lists','TransactionsController@getWithdrawal');
    Route::post('/withdrawal/download','TransactionsController@downloadWithdrawal');
    Route::post('/deposit/request/lists','TransactionsController@getDepositRequest');
    Route::post('/deposit/request/download','TransactionsController@downloadDepositRequest');
    Route::put('/deposit/update/status','TransactionsController@updateDepositRequest');
    
    Route::get('/withdrawal/payment/lists','TransactionsController@getWithdrawalPaymentList');
    Route::put('/withdrawal/update/status','TransactionsController@updateWithdrawalStatus');
    Route::put('/withdrawal/update/verify/status','TransactionsController@updateVerifyWithdrawalStatus');
    Route::put('/withdrawal/update/auto/verify/status','TransactionsController@updateAutoVerifyWithdrawalStatus');

    Route::get('/users/wallets/{id}','UserController@getwalletsDetails');

    Route::post('/transfer-fund/new','TransactionsController@store');

    Route::post('/no-cash/deposit/','TransactionsController@walletDeposit'); 
    Route::post('/no-cash/withdraw/','TransactionsController@walletWithdraw'); 
});
