<?php

Route::group(['prefix' => 'bones'], function() {

    Route::post('/', 'BonusController@getAllDeposit'); 
    Route::delete('/{id}', 'BonusController@destroy'); 

    Route::group(['prefix' => 'deposit'], function() {

        $controller='BonusController@';
        Route::get('/{id?}',$controller.'getDeposit'); 
        Route::post('/add',$controller.'store'); 
        Route::post('/edit/{id}',$controller.'update'); 
        Route::delete('/{id}',$controller.'destroy'); 
        Route::post('/update-status', $controller.'updateDepositAndLosingStatus'); 
    });

    Route::group(['prefix' => 'losing'], function() {

        $controller='BonusController@';
        Route::get('/{id?}',$controller.'getLosing'); 
        Route::post('/add',$controller.'store'); 
        Route::post('/edit/{id}',$controller.'update'); 
        Route::delete('/{id}',$controller.'destroy'); 
        Route::post('/update-status', $controller.'updateDepositAndLosingStatus'); 
    });

    Route::group(['prefix' => 'joining'], function() {
        
        $controller='BonusController@';
        Route::post('/add', $controller.'storeJoining'); 
        Route::post('/update-status', $controller.'updateJoiningStatus'); 
        Route::get('/{id?}',$controller.'getJoining'); 
        Route::post('/edit/{id}', $controller.'updateJoining'); 

    });

});
