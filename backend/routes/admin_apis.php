<?php
/* Player CRUD API*/
require_once "admin/users/index.php";

/* Tenants CRUD API*/
require_once "admin/tenants/tenantsRoutes.php";

// casino
require_once "admin/casino/index.php";

// bones
require_once "admin/bones/bonesRoutes.php";

// sport
require_once "admin/sports/sportsRoutes.php";

//cms
require_once "admin/cms/cmsRoutes.php";

//Meta service
require_once "admin/meta/metaRoutes.php";

// tenant Bank
require_once "admin/bank/bankRoutes.php";


// tenant Bank
require_once "admin/promoCode/promoCode.php";


// tenant Site Maintenance
require_once "admin/maintenance/index.php";

/* PermissionRolesController CRUD API*/
require_once "admin/permissionRole/permissionRoleRoutes.php";

// bet report
Route::post('/bet-reports', 'SportsController@' . 'getBetReports');
Route::post('/betreport', 'SportsController@' . 'getBetReportsByES');
Route::post('/bet-reports/download', 'SportsController@' . 'getAllBetReports');

/* Transactions CRUD API*/
require_once "admin/transactions/transactionsRoutes.php";

// bulk routes
require_once "admin/bulkDepositWithdraw/bulkDepositWithdrawRoutes.php";


//Reports
require_once "admin/report_apis.php";


Route::get('/themes', 'ThemesController@index');
Route::get('/layout', 'LayoutController@index');
Route::get('/languages', 'LanguagesController@index');


/* Notification */
/* Player User setting CRUD API*/

Route::group(['prefix' => 'notification'], function() {
  Route::get('/', 'NotificationsController@index');
  Route::get('/unread', 'NotificationsController@unread');
  Route::put('/all-read', 'NotificationsController@allRead');
  Route::get('/{id}', 'NotificationsController@show');
  Route::get('/read/{id}', 'NotificationsController@read');
});


Route::post('/aws/get/url', 'AdminController@getAWSUrl');

// Dashboard Routes
Route::post('dashboard', 'DashboardController@index');
Route::post('active-players', 'DashboardController@activePLayers');
Route::post('active-unique-players-count', 'DashboardController@activeUniquePlayersCount');



Route::post('dashboard/financial-activity', 'DashboardController@financialDashboard');
Route::post('dashboard/casino', 'DashboardController@gamingDashboard');
Route::post('dashboard/sport', 'DashboardController@sportsDashboard');
Route::post('dashboard/player-activity', 'DashboardController@userDashboard');
Route::post('dashboard/player-balance-info', 'DashboardController@userDashboardBalanceInfo');

