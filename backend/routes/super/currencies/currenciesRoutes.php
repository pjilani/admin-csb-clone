<?php
/* currencies CRUD API*/
Route::get('/currencies','CurrenciesController@index');
Route::get('/currencies/{id}','CurrenciesController@show');
Route::post('/currency','CurrenciesController@store');
Route::post('/currency/{id}','CurrenciesController@update');
Route::delete('/currency/{id}','CurrenciesController@destroy');
Route::post('/list-conversion-history','CurrenciesController@listConversionHistory');
Route::get('/currency-response/{id}','CurrenciesController@currencyResponse');
