<?php

Route::group(['prefix' => 'paymentProviders'], function(){

  Route::post('/lists', 'PaymentProvidersController@paymentProvidersList');
  Route::post('/{id}', 'PaymentProvidersController@update');
  Route::post('/', 'PaymentProvidersController@store');
  Route::post('/update/status', 'PaymentProvidersController@destroy');

 

});
