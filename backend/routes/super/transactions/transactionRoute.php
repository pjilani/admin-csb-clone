<?php

Route::group(['prefix' => 'transactions'], function(){

    Route::post('/','TransactionsController@index');
    Route::post('record','TransactionsController@indexByES');
    Route::post('/list/','TransactionsController@betList');
    Route::post('/listdownload/','TransactionsController@betListDownload');
    Route::post('/download','TransactionsController@download');
    Route::post('/downloadByES','TransactionsController@downloadByES');
    Route::get('/{id}','TransactionsController@show');
    Route::get('/users/new','UserController@searchUsers');
    Route::post('/withdrawal/lists','TransactionsController@getWithdrawal');
    Route::get('/users/wallets/{id}','UserController@getwalletsDetails');
    Route::post('/transfer-fund/new','TransactionsController@adminTransaction');
    Route::post('/no-cash/deposit/','TransactionsController@walletDeposit');
    Route::post('/no-cash/withdraw/','TransactionsController@walletWithdraw');



});