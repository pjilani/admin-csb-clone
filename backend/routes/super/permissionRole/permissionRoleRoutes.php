<?php

Route::get('/permissionRole','PermissionRoleController@index');
Route::get('/permissionRole/get','PermissionRoleController@getPermissionRoles');
Route::get('/permissionRole/{id}','PermissionRoleController@show');
Route::post('/permissionRole','PermissionRoleController@store');
Route::post('/permissionRole/{id}','PermissionRoleController@update');
Route::delete('/permissionRole/{id}','PermissionRoleController@destroy');
Route::get('/get-permissions-tenant-and-role-wise','PermissionRoleController@getPermissionsTenantAndRoleWise');
