<?php
Route::group(['prefix' => 'menu','namespace' => 'Menu'], function(){
    Route::get('/','MenuController@index');
    Route::get('/{id?}','MenuController@show');
    Route::post('/add','MenuController@store');
    Route::post('/update-icon','MenuController@updateIcon');
    Route::post('/edit/{id}','MenuController@update');
    Route::delete('/{id}','MenuController@destroy');
});