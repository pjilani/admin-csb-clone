<?php

Route::group(['prefix' => 'reports/'], function(){

    $controller = 'Reports\PlayerReportController@';
    $gameTransactionController = 'Reports\GameTransactionReportController@';
    $playerFinancialController = 'Reports\PlayerFinancialReportController@';
    $playerNCBController = 'Reports\PlayerNCBReportController@';
    $playerBonusStatusController = 'Reports\PlayerBonusStatusController@';
    $playerRevenueController = 'Reports\PlayerRevenueReportController@';
    $playerSportRevenueController = 'Reports\PlayerSportRevenueReportController@';
    $unifiedTransactionController = 'Reports\UnifiedTransactionReportController@';
    $tipController = 'Reports\TipReportController@';
    $agentRevenueController = 'Reports\AgentRevenueReportController@';
    $agentSportRevenueController = 'Reports\AgentSportRevenueReportController@';

    Route::get('tenant/admin_users/{id}',$controller.'TenantAdminUsers');
    Route::get('tenants', $controller.'TenantList');
    Route::get('owners', $controller.'TenantOwnerList');
    Route::get('users/hierarchy', $controller.'getAdminUserHierarchy');
    Route::get('agent/all', $controller.'getAdminBetHierarchy');
    Route::post('player', $controller.'getPlayerReport');
    Route::post('player/download', $controller.'downloadPlayerReport');

    Route::post('game/transaction', $gameTransactionController . 'getGameTransactionReport');
    Route::post('player/financial', $playerFinancialController . 'getplayerFinancialReport');
    Route::post('player/ncb', $playerNCBController . 'getplayerNCBReport');
    Route::post('player/bonusstatus', $playerBonusStatusController . 'getPlayerBonusStatusReport');
    Route::post('player/revenue', $playerRevenueController . 'getPlayerRevenueReport');
    Route::post('playersport/revenue', $playerSportRevenueController . 'getPlayerRevenueReport');
    Route::post('unified/transaction', $unifiedTransactionController . 'getUnifiedTransactionReport');
    Route::post('ggr', 'Reports\GgrReportController@getGgrReport');
    Route::post('tip', $tipController . 'getTipReport');
    Route::post('agent/revenue', $agentRevenueController . 'getAgentRevenueReport');
    Route::post('agentsport/revenue', $agentSportRevenueController . 'getAgentRevenueReport');

    /**
     * Download Csv section
     */
    
    Route::post('game/transaction/details', $gameTransactionController . 'show');
    Route::post('game/transaction/download', $gameTransactionController . 'downloadGameTransactionReport');
    Route::post('player/financial/download', $playerFinancialController . 'downloadPlayerFinancialReport');
    Route::post('player/ncb/download', $playerNCBController . 'downloadPlayerNCBReport');
    Route::post('unified/transaction/download', $unifiedTransactionController . 'downloadUnifiedTransactionReport');
    Route::post('tip/download', $tipController . 'downloadTipReport');
    Route::post('agent/revenue/download', $agentRevenueController . 'downloadAgentRevenueReport');
    Route::post('player/revenue/download', $agentRevenueController . 'downloadAgentRevenueReport');
    Route::post('player/bonusstatus/download', $playerBonusStatusController . 'downloadBonusStatusReport');
    Route::post('ggr/download', 'Reports\GgrReportController@downloadGgrReport');
});