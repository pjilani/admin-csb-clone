<?php



/* casino/providers */
Route::group(['prefix' => 'casino/providers','namespace' => 'Casino'], function(){
    Route::get('/','ProvidersController@index');
    Route::get('/{id}','ProvidersController@show');
    Route::post('/record/add','ProvidersController@store');
    Route::put('/edit/{id}','ProvidersController@update');
    Route::delete('/{id}','ProvidersController@destroy');

});

// casino/ games
Route::group(['prefix' => 'casino/games','namespace' => 'Casino'], function(){
    Route::post('/','CasinoGamesController@index');
    Route::get('/{id}','CasinoGamesController@show');
    Route::get('/list/all/{id?}','CasinoGamesController@all');
    Route::post('/add','CasinoGamesController@store');
    Route::put('/edit/{id}','CasinoGamesController@update');
    Route::delete('/{id}','CasinoGamesController@destroy');
});

// casino/Tables
Route::group(['prefix' => 'casino/tables','namespace' => 'Casino'], function(){
    Route::post('/','CasinoTablesController@index');
    Route::get('/{id}','CasinoTablesController@show');
    Route::post('/add','CasinoTablesController@store');
    Route::put('/edit/{id}','CasinoTablesController@update');
    Route::delete('/{id}','CasinoTablesController@destroy');

});