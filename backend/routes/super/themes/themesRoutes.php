<?php

Route::get('/themes','ThemesController@index');
Route::get('/themes/{id}','ThemesController@show');
Route::post('/themes','ThemesController@store');
Route::post('/themes/{id}','ThemesController@update');
Route::delete('/themes/{id}','ThemesController@destroy');