<?php


Route::post('/users','AdminController@index'); //for admin Users
Route::get('/users/{id}','AdminController@show');

Route::get('/user/wallet/details/{id}','AdminController@getSuperAdminWalletDetails');
Route::post('/transaction/details','AdminController@getSuperAdminTransactionDetails');
Route::post('/wallet/deposit','AdminController@superAdminWalletDeposit');

Route::post('/usersadmin/add/','AdminController@store');
Route::post('/usersadmin/edit/{id}','AdminController@update');
Route::delete('/users/{id}','AdminController@destroy');
Route::post('/usersadmin/agent','AdminController@getAdminAgents');
Route::post('/usersadmin/player','AdminController@getUserPlayer');
Route::post('/usersadmin/player/download','AdminController@downloadUserPlayer');

Route::get('agent/tree','AdminController@getSuperAdminUserTree');


Route::group(['prefix' => 'user/player'], function() {
    Route::put('/status/{id}/{status}','UserController@statusUserPlayer');
    Route::post('/add','UserController@store');
    Route::post('/update','UserController@update');
    Route::get('/{id}','UserController@getPlayer');
    Route::post('/reset-password','UserController@resetPassword');
    Route::put('/update-kyc','UserController@updateKyc');
    Route::post('/login-history/{id}','UserController@loginHistory');
    Route::post('/bank-details/{id}','UserController@bankDetails');
});
