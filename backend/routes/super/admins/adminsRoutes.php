<?php

Route::group(['prefix' => 'admins'], function(){

  Route::post('/lists', 'SuperAdminController@listAdmins');
  Route::post('/{id}', 'SuperAdminController@update');
  Route::post('/update/status', 'SuperAdminController@destroy');
  Route::post('/', 'SuperAdminController@store');
  Route::post('/roles/list', 'SuperAdminController@roleList');



    //Route::delete('/tenants/{id}','TenantsController@destroy'); not use full

 

});

Route::get('/settings/mail_configuration', 'SuperAdminController@getMailConfiguration');
Route::post('/settings/update_mail_configuration', 'SuperAdminController@updateMailConfiguration');
Route::post('log-list', 'SuperAdminController@logList');
Route::post('queue-logs', 'SuperAdminController@queueLogs');
Route::post('queue-logs/create', 'SuperAdminController@createQueueLogs');
Route::post('old-log-list', 'SuperAdminController@oldLogList');
Route::post('log-download', 'SuperAdminController@logDownload');
Route::post('old-log-download', 'SuperAdminController@oldLogDownload');
