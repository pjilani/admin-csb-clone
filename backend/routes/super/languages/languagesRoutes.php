<?php

Route::get('/languages','LanguagesController@index');
Route::get('/languages/{id}','LanguagesController@show');
Route::post('/languages','LanguagesController@store');
Route::post('/languages/{id}','LanguagesController@update');
Route::delete('/languages/{id}','LanguagesController@destroy');