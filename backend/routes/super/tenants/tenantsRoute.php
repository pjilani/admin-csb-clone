<?php

Route::group(['prefix' => 'tenants'], function(){
    Route::get('/', 'TenantsController@index');
    Route::post('/registration-field', 'TenantsController@registrationFieldUpdate');
    Route::post('/update-permission', 'TenantsController@updatePermission');
    Route::post('/update-player-commission-interval', 'TenantsController@updatePlayerCommissionInterval');
    Route::post('/lists', 'TenantsController@listTenants');
    Route::get('/{id}', 'TenantsController@show');
    Route::get('/credentials/{id}', 'TenantsController@getCredentials');
    Route::post('/credentials', 'TenantsController@setCredentials');
    Route::post('/credentials/edit/{id}', 'TenantsController@updateCredentials');
    
    Route::post('/paymentProvider/edit/{id}', 'TenantsController@updatePaymentProvider');
    Route::get('/paymentProvider/{id}', 'TenantsController@getPaymentProvider');
    Route::post('/paymentProvider/create/new', 'TenantsController@addPaymentProvider');
    Route::post('/tenantPaymentProviderStatus', 'TenantsController@tenantPaymentProviderStatus');
    Route::get('/paymentProvidersById/{id}', 'TenantsController@getPaymentProvidersById');
    Route::post('/credentials', 'TenantsController@setCredentials');
    Route::post('/credentials/edit/{id}', 'TenantsController@updateCredentials');
    Route::post('/', 'TenantsController@store');
    Route::post('/{id}', 'TenantsController@update');
    //Route::delete('/tenants/{id}','TenantsController@destroy'); not use full

    Route::get('/currencies/list', 'CurrenciesController@getTenantsCurrencies');

    Route::post('/sports/status', 'TenantsController@sportsStatus');
    Route::post('/report/status', 'TenantsController@reportsStatus');

    Route::get('/sports/setting','TenantsController@getTenantSportsBetSetting');
    Route::put('/sport-setting/update','TenantsController@updateTenantSportsBetSetting');
    Route::post('/menu/setting','TenantsController@updateTenantMenuSetting');

});
