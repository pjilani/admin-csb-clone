<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/', function (Request $request) {
    return response()->json(['success'=>1,'message'=>'Welcome to Backend API '.env('APP_NAME'),'record'=>[]]);
});
Route::get('/health-check', function (Request $request) {
    if (env('APP_ENV') != 'local' && env('APP_DEBUG')) {
        return response()->json(['error' => 'Debug is enabled'], 500);
    }

    return response()->json(['success'=>1,'message'=>'Welcome to Backend API '.env('APP_NAME'),'record'=>[]],200);
});

Route::get('/reindex-user/{id?}', 'ReindexESUsersController@reIndexUsers');
Route::get('/reindex-transaction/{id?}', 'ReindexESTransactionsController@reIndexTransactions');
Route::get('/reindex-bet-transaction/{id?}', 'ReindexESTransactionsController@reIndexBetTransactions');

Route::get('/possible-win-amount/{betType}', 'TestController@updatePossibleWinAmount');

Route::get('/create-table/{type}', 'TestController@createReindexTableData');
Route::get('/reindex-admin-dummy-transaction/{id?}', 'ReindexESTransactionsController@reIndexAdminDummyTransactions');