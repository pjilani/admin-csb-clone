<?php


\Route::group(['namespace' => 'Payout'], function () {
  \Route::post('/payout/status', 'PayoutCallbacksController@payoutStatus');
  \Route::middleware(['validateIndigritCallback'])->post('/payout/status/indigrit', 'PayoutCallbacksController@payoutStatusIndigrit');
  \Route::middleware(['validateSkyCallback'])->post('/payout/status/sky', 'PayoutCallbacksController@payoutStatusSky');
});
