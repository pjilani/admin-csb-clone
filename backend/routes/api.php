<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Welcome Message
 */
Route::get('/', function (Request $request) {
    return response()->json(['success' => 1, 'message' => 'Welcome to Backend API ' . env('APP_NAME'), 'record' => []],200);
});


Route::get('/fire/{adminId}', function ($adminId) {
    event(new \App\Events\SendMessage(['message' => 'my message', 'id' => $adminId]));
    return 'ok';
});

Route::group(['middleware' => ['api', 'cors'], 'prefix' => '/login/', 'namespace' => 'Api'], function () {
    Route::post('super', 'LoginController@superAdminlogin')->name('superAdminlogin');
    Route::post('admin', 'LoginController@login')->name('login');
});


//Login Routes
//Route::middleware('auth:api')->get('/users/{id?}','Api\LoginController@show');
//Route::middleware('auth:api')->post('/user','Api\LoginController@profileUpdate');


//Logout Routes
Route::middleware(['auth:superapi', 'cors'])->post('/logout/admin/super', 'Api\LoginController@adminSuperLogout');
Route::middleware(['auth:adminapi', 'cors'])->post('/logout/admin', 'Api\LoginController@adminLogout');


//Super Admin Route
Route::group(['prefix' => 'super/admin/', 'middleware' => ['auth:superapi', 'superadmin', 'cors'], 'namespace' => 'Api'], function () {
    Route::get('/', function (Request $request) {
        return response()->json(['success' => 1, 'message' => 'Welcome to Backend Super Admin API ' . env('APP_NAME'), 'record' => []]);
    });

    Route::get('/user', function () {
      $user = request()->user();
      $role_details = DB::table('super_admin_users_super_roles as sr')->select("sr.super_role_id","r.name")->leftJoin('super_roles as r','r.id' , '=' , 'sr.super_role_id')->where("sr.super_admin_user_id",$user->id)->first();

      if($role_details){
        // $user->wallets = getWalletDetails($user->id, 'SuperAdminUser');
       $user->role_id = $role_details->super_role_id;
       $user->role = $role_details->name;
      }else{
       return returnResponse(false, 'unauthorized', [], 403);
      }
      return response()->json(['success' => 1, 'message' => '', 'record' => $user]);
    });

    Route::get('/user/wallet', function () {
        try {
            $id = request()->user()->id;
            $wallets = getWalletDetails($id, 'SuperAdminUser');
            return response()->json(['success' => 1, 'message' => '', 'record' => $wallets]);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) { //, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, [
                    'Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    });

    include_once('super_apis.php');
});


//Admin Route
Route::group(['prefix' => 'admin', 'middleware' => ['auth:adminapi', 'admin', 'cors','blockIP'], 'namespace' => 'Api'], function () {
    Route::get('/', function (Request $request) {
        return response()->json(['success' => 1, 'message' => 'Welcome to Backend Admin API ' . env('APP_NAME'), 'record' => []]);
    });
    Route::get('/user', function () {
        try {
        $data = request()->user();
        $data->wallets = getWalletDetails($data->id, 'AdminUser');
        $data->roles = getRolesDetails($data->id);
        // $data->tenant = getTenantDetails($data->tenant_id);

        // $data->notification = getNotification($data->id, 'AdminUser');
        // $data->permissions = getPermissionsForAdminUser($data->id, $data->tenant_id);
        return response()->json(['success' => 1, 'message' => '', 'record' => $data]);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    });
    Route::get('/user/wallet', function () {
        try {
        $id = request()->user()->id;
        $wallets = getWalletDetails($id, 'AdminUser');
        return response()->json(['success' => 1, 'message' => '', 'record' => $wallets]);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    });

    Route::get('/user/permissions', function () {
        try {
        $id = request()->user()->id;
        $tenant_id = request()->user()->tenant_id;
        $permissions = getPermissionsForAdminUser($id, $tenant_id);
        return response()->json(['success' => 1, 'message' => '', 'record' => $permissions]);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    });

    Route::get('/user/tenant', function () {
        try {
        $tenant_id = request()->user()->tenant_id;
        $tenantData = getTenantDetails($tenant_id);
        return response()->json(['success' => 1, 'message' => '', 'record' => $tenantData]);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    });

    include_once('admin_apis.php');
});

Route::post('/process-bulk-deposit-withdraw','Api\BulkDepositWithdrawController@processBulkDepositWithdraw');


Route::get('/create-player', 'TestController@createPlayer');
Route::get('/agents-hierarchy', 'TestController@agentsHierarchy');

Route::get('/cache-clear', function() {
    Artisan::call('cache:clear');

    dd("cache clear All");
});

Route::group(['prefix' => 'callback', 'namespace' => 'Api'], function () {
include_once('payment_callback/payout/index.php');
});

//Tenant Details
Route::get('admin/tenantDetailsFromDomain', 'Api\LoginController@getTenantsDetailsFromDomain');
