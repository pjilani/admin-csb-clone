 cd /var/www/html
mkdir -p storage/framework/sessions
mkdir -p storage/framework/views
mkdir -p storage/framework/cache
mkdir -p storage/framework/cache/data
mkdir -p storage/app/csv
mkdir -p docker_volumes_data
mkdir -p logs
chmod -R 777 storage/
chown -R www-data:www-data storage/
chown -R www-data:www-data bootstrap/cache/

composer update
php artisan key:generate
php artisan vendor:publish --tag="cors"
php artisan passport:install
php artisan passport:keys
chown www-data:www-data storage/oauth-*.key
chmod 777 storage/oauth-*.key
composer dump-autoload -o
php artisan config:clear
php artisan route:clear
php artisan config:cache
php artisan cache:clear
php artisan config:cache
php artisan config:clear
php artisan migrate


npm install
npm install -g laravel-echo-server
