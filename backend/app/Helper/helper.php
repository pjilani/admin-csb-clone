<?php

use App\Mail\CommonEmail;
use App\Models\Admin;
use App\Models\AdminUsersAdminRoles;
use App\Models\Currencies;
use App\Models\Menu\MenuTenantSettingModel;
use App\Models\TenantConfigurations;
use App\Models\TenantPermission;
use App\Models\User;
use App\Services\NotificationService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Elasticsearch\ClientBuilder;
use Illuminate\Support\Facades\App;

//Load models


/**
 *
 * @description errorArrayCreate
 */
if (!function_exists('errorArrayCreate')) {
    function errorArrayCreate($obj)
    {
        try {
            $obj = $obj->toArray();
            $errors = array();
            if (is_array($obj) && !empty($obj)) {
                foreach ($obj as $k => $v) {
                    if (count($v) > 1) {
                        $err = '';
                        foreach ($v as $value) {
                            $err .= $value . ' && ';
                        }
                        trim($err, '&&');
                        $errors[$k] = $err;
                    } else {
                        $errors[$k] = $v[0];
                    }
                }
            }
            return $errors;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}


if (!function_exists('du')) {
    function du($array)
    {
        echo "<pre>";
        print_r($array);
        die;
    }

}

if (!function_exists('datetimest')) {
    function datetimest()
    {
        date_default_timezone_set(env('timezone'));
        return $currentDate = Date('Y-m-d H:i:s');
    }
}

if (!function_exists('nextDate')) {
    function nextDate($days = 1)
    {
        $startDate = time();
        date_default_timezone_set(env('timezone'));
        return $currentDate = date('Y-m-d H:i:s', strtotime('+' . $days . ' day', $startDate));
    }
}


if (!function_exists('sendEmail')) {
    function sendEmail($view, $email, $data)
    {
        if (view()->exists('email_template.' . $view)) {
            $data['view'] = $view;
            return Mail::to($email)
                ->send(new CommonEmail($data));
        } else {
            return false;
        }
    }
}

if (!function_exists('returnResponse')) {
    function returnResponse($result = true, $message = "Record fetch successfully", $record = [], $resCode = 200, $count = false)
    {
        try {
            $responseArray = array();
            if ($result === true) {
                $responseArray['success'] = 1;
            } else {
                $responseArray['success'] = 0;
            }
            $responseArray['message'] = $message;
            $responseArray['record'] = $record;

            if (!$count)
                $responseArray['count'] = count($record);

        } catch (Exception $e) {
//            $responseArray['success']=0;
//            $responseArray['message']=$message .' '.$e->getMessage();
        }
        return response()->json($responseArray, $resCode);

    }
}

/**
 * @param $url
 * @param string $method
 * @param array $post_parameters
 */
function background_curl_request($url, $method = 'get', $post_parameters = [], $storeId = '')
{
    if (is_array($post_parameters)) {
        $params = "";
        foreach ($post_parameters as $key => $value) {
            $params .= $key . "=" . urlencode($value) . '&';
        }
        $params = rtrim($params, "&");
    } else {
        $params = $post_parameters;
    }
    $path = "/var/log/app/store_" . $storeId . "_" . date('y-m-d_h:i:s') . ".json";
    $command = "/usr/bin/curl -X '" . $method . "' -d '" . $params . "' --url '" . $url . "' >> $path 2> /dev/null &";
    exec($command);
}


function removeSpecialChar($string)
{
    $string = str_replace(' ', '', $string);

    $string = preg_replace('/[^A-Za-z0-9\-]/', '_', $string);
    return $string;
}


if (!function_exists('getWalletDetails')) {
    function getWalletDetails($id, $type = 'User')
    {
        if ($id == '') {
            return;
        }
        return DB::table('wallets')
            ->select(DB::raw('wallets.*, (SELECT code FROM "currencies" where id=wallets.currency_id) as currency_name'))
            ->where('owner_type', $type)
            ->where('owner_id', $id)->get();

    }
}


function payoutCall($endpoint,$data,$headers){
  $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $endpoint,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS =>$data,
      CURLOPT_HTTPHEADER => $headers,
      CURLINFO_HEADER_OUT => true,

    ));

    $response = curl_exec($curl);

    curl_close($curl);

    return $response;
}


if (!function_exists('getNodeUrl')) {
    function getNodeURl($domain)
    {
      $BASE_API = env('REACT_APP_GRAPHQL_URL');
      $PORT = env('REACT_APP_BACKEND_LOCAL_PORT');

      if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||  isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
        $protocol = 'https://';
      }
      else {
        $protocol = 'http://';
      }

      $locationInfo = $protocol;
      $currentHostname =  $domain;

      $url = '';

      if (str_contains($currentHostname, 'www.')) {
        $newHostname =  str_replace("www.","api.",$currentHostname);
       $url = "$locationInfo"."$newHostname".(isset($PORT) ? ':'.$PORT : '');
      } else {
        if (in_array($currentHostname, EZUGI_DOMAINS) && $BASE_API != "") {
          $url = "$locationInfo"."$BASE_API";
        } else {
          // $url = `${locationInfo?.protocol}//api.${currentHostname}${PORT ? `:${PORT}` : ''}`;
          $url = "$locationInfo".'api.'."$currentHostname".(isset($PORT) ? ':'.$PORT : '');

        }
      }
       return $url;
    }
}


if (!function_exists('getAffiliateUrl')) {
  function getAffiliateUrl($domain)
  {
    if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||  isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
      $protocol = 'https://';
    }
    else {
      $protocol = 'http://';
    }
    $url = $protocol.$domain;

    $affiliateUrl = $url.'/invite/';
    return $affiliateUrl;
  }
}

if (!function_exists('getWallet')) {
    function getWallet($id)
    {
        if ($id == '') {
            return;
        }
        return DB::table('wallets')
            ->select(DB::raw('wallets.*, (SELECT code FROM "currencies" where id=wallets.currency_id) as currency_name'))
            ->where('id', $id)->get();

    }
}

if (!function_exists('getRolesDetails')) {

    function getRolesDetails($id = '')
    {
      $super = false;
      if ($id == '') {
        $id = Auth::user()->id;
        $super = true;
        }
        if((isset(Auth::user()->parent_type) && (Auth::user()->parent_type == 'SuperAdminUser' || Auth::user()->parent_type == 'Manager')) && $super == true){
          $roles = DB::connection('read_db')->table('super_admin_users_super_roles')->where('super_admin_user_id', $id)->select('super_role_id')->get();
          $rolesArray = [];

          if ($roles && $roles->toArray()) {
              foreach ($roles->toArray() as $key => $value) {
                  // $rolesArray[] = $value['admin_role_id'];
                  if ($value->super_role_id == 1)
                      // $rolesArray['owner'] = $value['admin_role_id'];
                      $rolesArray[] = 'owner';

                  if ($value->super_role_id == 2)
                      // $rolesArray['agent'] = $value['admin_role_id'];
                      $rolesArray[] = 'owner';
              }
          }
        }else{
          $roles = AdminUsersAdminRoles::on('read_db')->where('admin_user_id', $id)->select('admin_role_id')->get();
          $rolesArray = [];

          if ($roles && $roles->toArray()) {
              foreach ($roles->toArray() as $key => $value) {
                  // $rolesArray[] = $value['admin_role_id'];
                  if ($value['admin_role_id'] == 1)
                      // $rolesArray['owner'] = $value['admin_role_id'];
                      $rolesArray[] = 'owner';

                  if ($value['admin_role_id'] == 2)
                      // $rolesArray['agent'] = $value['admin_role_id'];
                      $rolesArray[] = 'agent';
                

                  if ($value['admin_role_id'] == 3)
                    // $rolesArray['agent'] = $value['admin_role_id'];
                    $rolesArray[] = 'sub-admin';

                  if ($value['admin_role_id'] == 4)
                    // $rolesArray['agent'] = $value['admin_role_id'];
                    $rolesArray[] = 'deposit-admin';

                  if ($value['admin_role_id'] == 5)
                    // $rolesArray['agent'] = $value['admin_role_id'];
                    $rolesArray[] = 'withdrawal-admin';
              }
          }
        }
        

        return $rolesArray;
    }
}


function getUserStatusString($status)
{
    $str = '';
    switch ($status) {
        case 0:
            $str = 'InActive';
            break;
        case 1:
            $str = 'Active';
            break;
        default:
            $str = 'NA';
    }
    return $str;
}


function getTransactionTypeString($transactionType)
{
    $str = '';
    switch ($transactionType) {
        case 0:
            $str = 'bet';
            break;
        case 1:
            $str = 'win';
            break;
        case 2:
            $str = 'refund';
            break;
        case 3:
            $str = 'deposit';
            break;
        case 4:
            $str = 'withdraw';
            break;
        case 5:
            $str = 'non_cash_granted_by_admin';
            break;
        case 6:
            $str = 'non_cash_withdraw_by_admin';
            break;
        case 7:
            $str = 'tip';
            break;
        case 8:
            $str = 'bet_non_cash';
            break;
        case 9:
            $str = 'win_non_cash';
            break;
        case 10:
            $str = 'refund_non_cash';
            break;
        case 11:
            $str = 'non_cash_bonus_claim';
            break;
        case 12:
            $str = 'deposit_bonus_claim';
            break;
        case 13:
            $str = 'tip_non_cash';
            break;
        case 14:
            $str = 'withdraw_cancel';
            break;
        case 15:
            $str = 'joining_bonus_claimed';
            break;
        case 16:
          $str = 'failed';
          break;
        case 17:
          $str = 'promo_code_bonus_claimed';
          break;
        case 19:
          $str = 'commission_received';
          break;
        case 20:
          $str = 'exchange_place_bet_non_cash_debit';
          break;
        case 21:
          $str = 'exchange_place_bet_cash_debit';
          break;
        case 22:
          $str = 'exchange_place_bet_cash_credit';
          break;
        case 23:
          $str = 'exchange_refund_cancel_bet_non_cash_debit';
          break;
        case 24:
          $str = 'exchange_refund_cancel_bet_cash_debit';
          break;
        case 25:
          $str = 'exchange_refund_cancel_bet_non_cash_credit';
          break;
        case 26:
          $str = 'exchange_refund_cancel_bet_cash_credit';
          break;
        case 27:
          $str = 'exchange_refund_market_cancel_non_cash_debit';
          break;
        case 28:
          $str = 'exchange_refund_market_cancel_cash_debit';
          break;
        case 29:
          $str = 'exchange_refund_market_cancel_non_cash_credit';
          break;
        case 30:
          $str = 'exchange_refund_market_cancel_cash_credit';
          break;
        case 31:
          $str = 'exchange_settle_market_cash_credit';
          break;
        case 32:
          $str = 'exchange_settle_market_cash_debit';
          break;
        case 33:
          $str = 'exchange_resettle_market_cash_credit';
          break;
        case 34:
          $str = 'exchange_resettle_market_cash_debit';
          break;
        case 35:
          $str = 'exchange_cancel_settled_market_cash_credit';
          break;
        case 36:
          $str = 'exchange_cancel_settled_market_cash_debit';
          break;
        case 37:
          $str = 'exchange_deposit_bonus_claim';
          break;  
        default:
            $str = 'na';
            break;
    }
    return $str;

}

function getUUIDTransactionId(){
    $str=(string) Str::uuid();
    $sql = "SELECT id FROM transactions WHERE transaction_id = '".$str."'";
    $sqlCheckUUID = DB::select($sql);
    if (@count($sqlCheckUUID[0])>0) {
        getUUIDTransactionId();
    }
    return $str;
}

function checkEmailForTenant($email, $tenant_id, $type)
{
    if ($type == "player") {
        $result = User::where('tenant_id', $tenant_id)->where('email', $email)->select('id')->get();
    } else {
        $result = Admin::where('tenant_id', $tenant_id)->where('email', $email)->select('id')->get();
    }
    return $result->count();
}

function checkPhoneForTenant($phone, $tenant_id, $type)
{
    if ($type == "player") {
        $result = User::where('tenant_id', $tenant_id)->where('phone', $phone)->select('id')->get();
    } else {
        $result = Admin::where('tenant_id', $tenant_id)->where('phone', $phone)->select('id')->get();
    }
    
    return $result->count();
}


function getTimeZoneWiseTime($creation_date, $timeZone)
{
    return \Carbon\Carbon::parse($creation_date)->setTimezone($timeZone)->toDateTimeString();
}

function getTimeZoneWiseTimeISO($creation_date)
{
    return \Carbon\Carbon::parse($creation_date)->toISOString();
}

function dateConversion($timePeriod)
{
    $fromDate = '';
    $endDate = '';

    if ($timePeriod == "yesterday") {
        $yesterday = Carbon::yesterday();
        $fromDate = $yesterday->toDateString() . "T00:00:00.000Z";

        $endDate = $yesterday->toDateString() . "T23:59:59.999Z";
    }
    if ($timePeriod == "daily" || $timePeriod == "today") {
        $today = Carbon::today();
        $fromDate = $today->toDateString() . "T00:00:00.000Z";

        $endDate = $today->toDateString() . "T23:59:59.999Z";
    }
    if ($timePeriod == "weekly") {
        $now = Carbon::now();
        $startweek = $now->startOfWeek();
        $fromDate = $startweek->toDateString() . "T00:00:00.000Z";

        $now = Carbon::now();
        $endweek = $now->endOfWeek();
        $endDate = $endweek->toDateString() . "T23:59:59.999Z";
    }
    if ($timePeriod == "lastMonth") {

      $firstdate = Carbon::now()->startOfMonth()->subMonthsNoOverflow();
      $lastdate = Carbon::now()->subMonthsNoOverflow()->endOfMonth();
      $fromDate = $firstdate->toDateString() . "T00:00:00.000Z";
      $endDate = $lastdate->toDateString() . "T23:59:59.999Z";
  }
    if ($timePeriod == "monthly") {
        $now = Carbon::now();
        $mydate = $now->toDateString();
        $firstdate = Carbon::createFromFormat('Y-m-01', $now->toDateString())->firstOfMonth();
        $lastdate = Carbon::createFromFormat('Y-m-d', $now->toDateString())->lastOfMonth();

        $fromDate = $firstdate->toDateString() . "T00:00:00.000Z";

        $endDate = $lastdate->toDateString() . "T23:59:59.999Z";
    }

    if ($timePeriod == "yearly") {
        $year = Carbon::now()->format("Y");
        $startyear = Carbon::createFromDate($year, "01", "01");
        $endyear = Carbon::createFromDate($year, "12", "31");


        $fromDate = $startyear->toDateString() . "T00:00:00.000Z";

        $endDate = $endyear->toDateString() . "T23:59:59.999Z";
    }
    return ['fromdate' => $fromDate, "enddate" => $endDate];

}

function convertToUTC($inputDate, $inputTimezone) {
    $date = Carbon::createFromFormat('Y-m-d H:i:s', $inputDate, $inputTimezone);
    $utcDate = $date->setTimezone('UTC');
    return $utcDate;
}

function dateConversionWithTimeZone($timePeriod)
{
    $fromDate = '';
    $endDate = '';

    if ($timePeriod == "yesterday") {
        $yesterday = Carbon::yesterday();
        $fromDate = $yesterday->toDateString() . " 00:00:00";

        $endDate = $yesterday->toDateString() . " 23:59:59";
    }
    if ($timePeriod == "daily" || $timePeriod == "today") {
        $today = Carbon::today();
        $fromDate = $today->toDateString() . " 00:00:00";

        $endDate = $today->toDateString() . " 23:59:59";
    }
    if ($timePeriod == "weekly") {
        $now = Carbon::now();
        $startweek = $now->startOfWeek();
        $fromDate = $startweek->toDateString() . " 00:00:00";

        $now = Carbon::now();
        $endweek = $now->endOfWeek();
        $endDate = $endweek->toDateString() . " 23:59:59";


    }
    if ($timePeriod == "lastMonth") {

        $firstdate = Carbon::now()->startOfMonth()->subMonthsNoOverflow();
        $lastdate = Carbon::now()->subMonthsNoOverflow()->endOfMonth();
        $fromDate = $firstdate->toDateString() . " 00:00:00";
        $endDate = $lastdate->toDateString() . " 23:59:59";
    }

    if ($timePeriod == "monthly") {
        $now = Carbon::now();
        $mydate = $now->toDateString();
        $firstdate = Carbon::createFromFormat('Y-m-d', $now->toDateString())->firstOfMonth();
        $lastdate = Carbon::createFromFormat('Y-m-d', $now->toDateString())->lastOfMonth();

        $fromDate = $firstdate->toDateString() . " 00:00:00";

        $endDate = $lastdate->toDateString() . " 23:59:59";
    }

    return ['fromdate' => $fromDate, "enddate" => $endDate];

}

function getNotification($id, $type)
{
    NotificationService::getNotification($id, $type);
}

if (!function_exists('getRolesDetails')) {

    function getRolesDetails($id = '')
    {

        if ($id == '') {
            $id = Auth::user()->id;
        }

        $roles = AdminUsersAdminRoles::on('read_db')->where('admin_user_id', $id)->select('admin_role_id')->get();
        $rolesArray = [];

        if ($roles && $roles->toArray()) {
            foreach ($roles->toArray() as $key => $value) {
                // $rolesArray[] = $value['admin_role_id'];
                if ($value['admin_role_id'] == 1)
                    // $rolesArray['owner'] = $value['admin_role_id'];
                    $rolesArray[] = 'owner';

                if ($value['admin_role_id'] == 2)
                    // $rolesArray['agent'] = $value['admin_role_id'];
                    $rolesArray[] = 'agent';

                if ($value['admin_role_id'] == 3)
                // $rolesArray['agent'] = $value['admin_role_id'];
                $rolesArray[] = 'sub-admin';

                if ($value['admin_role_id'] == 4)
                // $rolesArray['agent'] = $value['admin_role_id'];
                $rolesArray[] = 'deposit-admin';

                if ($value['admin_role_id'] == 5)
                // $rolesArray['agent'] = $value['admin_role_id'];
                $rolesArray[] = 'withdrawal-admin';
            }
        }

        return $rolesArray;
    }
}

if (!function_exists('getTenantDetails')) {

    function getTenantDetails($id = '')
    {

        $tenant = \App\Models\Tenants::select('tenants.id','tenants.domain','tenants.name', 'tenant_theme_settings.logo_url','tenant_theme_settings.allowed_modules',
            'TSS.id as tenant_sports_bet_setting_id')
            ->leftJoin('tenant_theme_settings', 'tenant_theme_settings.tenant_id', '=', 'tenants.id')
            ->leftJoin('tenant_sports_bet_setting as TSS', 'TSS.tenant_id', '=', 'tenants.id')
            ->where('tenants.id', '=', $id)
            ->get();


        // Menu List for the Tenant Admin

        $tenant[0]['menuList'] = MenuTenantSettingModel::select(
            [
              DB::raw('mm.name  as menu_name')
            ])
            ->leftjoin('menu_master as mm','mm.id','menu_tenant_setting.menu_id')
            ->where(['tenant_id' =>$id,"active"=>true])
            ->pluck('menu_name');

        //tenant credentials
        $tenant_credentials_values = getTenantredentialValuesByKeys($id, ['TENANT_BASE_CURRENCY','GLOBAL_IP_ADDRESS_FOR_WHITELIST']);
        $tenant[0]['tenant_base_currency'] = $tenant_credentials_values[0] ?? null;
        $tenant[0]['global_ip_address'] = $tenant_credentials_values[1] ?? null;

        //tenant allowed currencies
        $configurations = TenantConfigurations::on('read_db')->select('allowed_currencies')->where('tenant_id', $id)->get();
            $configurations = $configurations->toArray();
            if ($configurations) {

                $configurations = $configurations[0];
                if (strlen($configurations['allowed_currencies']) > 1) {

                    $configurations = str_replace('{', '', $configurations['allowed_currencies']);
                    $configurations = str_replace('}', '', $configurations);
                    $configurations = explode(',', $configurations);
                    if (@$configurations[0] && count($configurations) > 0) {
                        $CurrenciesValue = Currencies::on('read_db')->orderBy('code', 'ASC')->select('id', 'name', 'code', 'exchange_rate')->whereIn('id', $configurations)->get();
                        $CurrenciesValue = $CurrenciesValue->toArray();
                        $code = array_column($CurrenciesValue, 'code');
                        $index = array_search('chips', $code);
                        if($index !== false){
                          $CurrenciesValue = [$CurrenciesValue[$index]];
                        }
                        $tenant[0]['allowed_currencies'] = $CurrenciesValue;
                    }
                }
            }

        return @$tenant[0];
    }
}

/**
 * @description getTenantredentialValuesByKeys
 * @param $idAgent
 * @return mixed
 */
if (!function_exists('getTenantredentialValuesByKeys')) {
    function getTenantredentialValuesByKeys($tenantId, $keys)
    {
        $results = DB::connection('read_db')->table('tenant_credentials')
            ->select('key','value')
            ->where('tenant_id', $tenantId)
            ->whereIn('key', $keys)
            ->get();

        $values = [];
        foreach ($keys as $key) {
            $matchingRecord = $results->where('key', $key)->first();
            $values[] = $matchingRecord ? $matchingRecord->value : null;
        }

        return $values;
    }
}

/**
 * @description getTopParentAgent
 * @param $idAgent
 * @return mixed
 */
function getTopParentAgent($idAgent)
{
    $sql = "select parent_id from admin_users where id = " . (int)$idAgent;
    $idTopParent = DB::select($sql);
    if (@$idTopParent[0]) {
        $idTopParent = $idTopParent[0]->parent_id;
    }

    if ($idTopParent == $idAgent) {
        return $idTopParent;
    } else {
        getTopParentAgent($idTopParent);
    }
}

/**
 * @description getAdminHierarchy
 * @param $idAgent
 * @param bool $topLevel
 * @return array
 */
function getAdminHierarchy($idAgent, $topLevel = true)
{
    $structure = array();

    if ($topLevel) {

        $sql = "select id from admin_users where id = " . (int)$idAgent;
        $idTopParent = DB::connection('read_db')->select($sql);
        if (@$idTopParent[0]) {
            $structure[$idAgent] = array('id' => $idTopParent[0]->id, 'child_id' => getAdminHierarchy($idTopParent[0]->id, false));
        }
    } else {

        $sql = "select id from admin_users where parent_id = " . (int)$idAgent;
        $idResult = DB::connection('read_db')->select($sql);
        foreach ($idResult as $dummy => $record) {
            $structure[$record->id]['id'] = $record->id;

            if($record->id != $idAgent) {
                $structure[$record->id]['child_id'] = getAdminHierarchy($record->id, false);
            }
        }
    } 
    return $structure;
}


function getAgentAdminHierarchy($idAgent, $topLevel = true,$email)
{
    $structure = array();
    if ($topLevel) {

        $sql = "select id,email from admin_users where id = " . (int)$idAgent."AND email like '%".$email."%'";
        $idTopParent = DB::select($sql);
        if (@$idTopParent[0]) {
            $structure[$idAgent] = array('id' => $idTopParent[0]->id, 'child_id' => getAgentAdminHierarchy($idTopParent[0]->id, false,$email));
        }
    } else {

        $sql = "select id,email from admin_users where parent_id = " . (int)$idAgent . "AND email like '%".$email."%'";
        $idResult = DB::select($sql);
        foreach ($idResult as $dummy => $record) {
            $structure[$record->id]['id'] = $record->id;
            $structure[$record->id]['email'] = $record->email;

            if($record->id != $idAgent) {
                $structure[$record->id]['child_id'] = getAgentAdminHierarchy($record->id, false,$email);
            }
        }
    }
    return $structure;
}

/**
 * @description flatten
 * @param array $array
 * @return array
 */
function flatten(array $array) {
    $return = array();
    array_walk_recursive($array, function($a) use (&$return) { $return[] = $a; });
    return $return;
}

/**
 * @description getActionTypeName
 * @param $id
 * @return string
 */
function getActionTypeName($id)
{
    $str = '-';
    if ($id == 1) {
        $str = "bet_placement";
    } else if ($id == 2) {
        $str = "won";
    } else if ($id == 3) {
        $str = "cashout";
    } else if ($id == 4) {
        $str = "refund";
    } else if ($id == 5) {
        $str = "lost_by_resettlement";
    } else if ($id == 6) {
        $str = "deposit_bonus_claim";
    } else if ($id == 7) {
        $str = "deposit_bonus_pending";
    }else if ($id == 8) {
        $str = "deposit_bonus_cancel";
    }
    return $str;
}


/*function getEsClientObj(){

   $myCert = storage_path(env('ELASTICSEARCH_PEM', 'mycert.pem'));
    $client = ClientBuilder::create()->setHosts([env('ELASTICSEARCH_HOST', 'localhost')]);
   if (App::environment(['production'])) {
     $client = $client->setSSLVerification($myCert);
   }
     $client = $client->build();

   return $client;
}*/

 function getEsClientObj(){
     if (App::environment() === 'production') {
         $myCert = storage_path(env('ELASTICSEARCH_PEM', 'mycert.pem'));
         $client = ClientBuilder::create()->setHosts([env('ELASTICSEARCH_HOST', 'localhost')]);
         if (App::environment(['production'])) {
             $client = $client->setSSLVerification($myCert);
         }
     } elseif (App::environment() === 'stage') {
         $myCert = storage_path(env('ELASTICSEARCH_PEM', 'mycert.pem'));
         $client = ClientBuilder::create()->setHosts([env('ELASTICSEARCH_HOST', 'localhost')]);
         $client = $client->setSSLVerification($myCert);
     } else {
         $hosts = [
             [
                 'host' => env('ELASTICSEARCH_HOST', 'localhost'),
                 'port' => env('ELASTICSEARCH_PORT', ''),
                 'user' => env('ELASTICSEARCH_USER', ''),
                 'pass' => env('ELASTICSEARCH_PASS', '')
             ]
         ];
         $client = ClientBuilder::create()->setHosts($hosts);
     }
     $client = $client->build();

   return $client;
}

// function getEsClientObj(){

//     $myCert = storage_path(env('ELASTICSEARCH_PEM', 'mycert.pem'));
//     if (1) {
//         $client = ClientBuilder::create()->setHosts([env('ELASTICSEARCH_HOST', 'localhost')]);
//         $client = $client->setSSLVerification($myCert);
//     }elseif(App::environment(['stage'])){
//         $hosts = [
//             [
//                 'host' => env('ELASTICSEARCH_HOST', 'localhost'),
//                 'port' => env('ELASTICSEARCH_PORT', ''),
//                 'user' => env('ELASTICSEARCH_USER', ''),
//                 'pass' => env('ELASTICSEARCH_PASS', '')
//             ]
//         ];
//         $client = ClientBuilder::create()->setHosts($hosts);
//     }else{
//         $hosts = [
//           [
//               'host' => env('ELASTICSEARCH_HOST', 'localhost'),
//               'port' => env('ELASTICSEARCH_PORT', ''),
//               'user' => env('ELASTICSEARCH_USER', ''),
//               'pass' => env('ELASTICSEARCH_PASS', '')
//           ]
//         ];
//         $client = ClientBuilder::create()->setHosts($hosts);
//     }
//     $client = $client->build();

//     return $client;

// }


function getRoleByAdminId($id){
    return AdminUsersAdminRoles::on('read_db')->where('admin_user_id', $id)->select('admin_role_id')->first();
}


/**
 * @description getParentAgent
 * @param $idAgent
 * @return mixed
 */
function getParentAgent($idAgent)
{
    $sql = "select parent_id from admin_users where id = " . (int)$idAgent;
    $idTopParent = DB::connection('read_db')->select($sql);
    if (@$idTopParent[0]) {
        $idTopParent = $idTopParent[0]->parent_id;
    }

    return $idTopParent;
}

if (!function_exists('getParentOwner')) {
    function getParentOwner($tenantId)
    {
        $sql = "select id from admin_users  where tenant_id = " . (int)$tenantId. " order by id asc limit 1";
        $idTopParent = DB::connection('read_db')->select($sql);
        if (@$idTopParent[0]) {
            $idTopParent = $idTopParent[0]->id;
        }

        return $idTopParent;
    }
}
function setUnlimited($memory = "6G")
{
    ini_set('max_execution_time', 0);
    ini_set("memory_limit", $memory);
}
/*  function getBetSettermentStatus($id){
      $str = "In Game";

      switch ($id) {
          case -2:
              $str = 'In Game';
              break;
          case -1:
              $str = 'Cancelled';
              break;
          case 1:
              $str = 'Lost';
              break;
          case 2:
              $str = 'Won';
              break;
          case 3:
              $str = 'Refund';
              break;
          case 4:
              $str = 'Halflost';
              break;
          case 5:
              $str = 'Halfwon';
              break;
          default:
              $str = "In Game";
              break;
      }

      return $str;
  }*/


  function isCurrencyHide($id = '', $type = '')
{
    if ($id == '') {
        return false;
    }

    $wallets = DB::table('wallets')
        ->select(DB::raw('wallets.*, (SELECT code FROM "currencies" where id=wallets.currency_id) as currency_name'))
        ->where('owner_type', $type)
        ->where('owner_id', $id)
        ->get();

    foreach ($wallets as $wallet) {
        if ($wallet->currency_name === 'chips') {
            return true;
        }
    }

    return false;
}


function isSubAgentAllowed($id = '')
{
    $tenant = \App\Models\Tenants::select('tenant_theme_settings.allowed_modules')
        ->leftJoin('tenant_theme_settings', 'tenant_theme_settings.tenant_id', '=', 'tenants.id')
        ->where('tenants.id', '=', $id)
        ->first();

    if ($tenant) {
        $allowedModules = $tenant->allowed_modules;
        return strpos($allowedModules, 'subAgent') !== false;
    }

    return false;
}


function getRoles($id) {
    $roles = AdminUsersAdminRoles::where('admin_user_id', $id)->select('admin_role_id')->get();
    $rolesArray = [
        'IsAgent' => false,
        'IsOwner' => false,
        'IsSubAdmin' => false,
        'IsDepositAdmin' => false,
        'IsWithdrawalAdmin' => false,
    ];

    if ($roles && $roles->toArray()) {
        foreach ($roles->toArray() as $key => $value) {
            $roleId = $value['admin_role_id'];

            if ($roleId == 1) {
                $rolesArray['IsOwner'] = true;
            } elseif ($roleId == 2) {
                $rolesArray['IsAgent'] = true;
            } elseif ($roleId == 3) {
                $rolesArray['IsSubAdmin'] = true;
            } elseif ($roleId == 4) {
                $rolesArray['IsDepositAdmin'] = true;
            } elseif ($roleId == 5) {
                $rolesArray['IsWithdrawalAdmin'] = true;
            }
        }
    }

    return (object)$rolesArray;
}

function checkPermissions($params) {
    $getPermissions = getPermissionsForAdminUser($params->admin_user_id, $params->tenant_id);
    if ($getPermissions) {
        $data = $getPermissions;
        if (isset($params->module) && array_key_exists($params->module, $data)) {
            // $moduleActions = $data[$params->module];
            $moduleActions = $data->{$params->module};
            if (in_array($params->action, $moduleActions)) {
                return true;
            }
        }
    }

    return false;
}

function checkTenantPermissions($params){
    $tenantPermission = TenantPermission::on('read_db')->where('tenant_id',$params->tenant_id)->select('permission')->first(); 
                if ($tenantPermission) {
                    if (array_key_exists($params->module, json_decode($tenantPermission->permission))) {
                        $moduleActions = json_decode($tenantPermission->permission)->{$params->module};
                        if (in_array($params->action, $moduleActions)) {
                            return true;
                        }
                    }
                }
                return false;
}

function getPermissionsForAdminUser($adminUserId,$tenantId)
{
    $ownerRole = DB::connection('read_db')->table('admin_users_admin_roles')
    ->where('admin_user_id', $adminUserId)
    ->where('admin_role_id', 1)
        ->select('admin_user_id')
    ->exists();

    if ($ownerRole) {
        /*$tenantId = DB::connection('read_db')->table('admin_users')
        ->where('id', $adminUserId)
        ->value('tenant_id');*/
        $permissions = DB::connection('read_db')->table('tenant_permissions')
        ->where('tenant_id', $tenantId)
        ->value('permission');
        return json_decode($permissions);
    }

    $permissionRoleId = DB::connection('read_db')->table('admin_users_permission_roles')
    ->where('admin_user_id', $adminUserId)
    ->value('permission_role_id');

    if ($permissionRoleId) {
        $permissions = DB::connection('read_db')->table('permission_role')
        ->where('id', $permissionRoleId)
        ->value('permission');

        return json_decode($permissions);
    }

    return [];
}

function getDomainNameFromHost(){
    if(request()->headers->get('host') == 'localhost'){
        return 'localhost';
    }
    else{   
        $host = request()->headers->get('host');
        $keyword = explode('.', explode(':', $host)[0]);
        $keyword = implode('.', array_slice($keyword, 1));
        return $keyword;
    }
}

function st8Data() {
    $data = '{
        "data": {
            "categories": [
                {
                    "name": "Video Slots",
                    "type": "rng"
                },
                {
                    "name": "Live Roulette",
                    "type": "live_dealer"
                },
                {
                    "name": "Live Poker",
                    "type": "live_dealer"
                },
                {
                    "name": "Live Blackjack",
                    "type": "live_dealer"
                },
                {
                    "name": "Game Show",
                    "type": "live_dealer"
                },
                {
                    "name": "Live Dealer",
                    "type": "live_dealer"
                },
                {
                    "name": "Live Baccarat",
                    "type": "live_dealer"
                },
                {
                    "name": "Live Dragon Tiger",
                    "type": "live_dealer"
                },
                {
                    "name": "Live Sic Bo",
                    "type": "live_dealer"
                },
                {
                    "name": "Table Games",
                    "type": "rng"
                },
                {
                    "name": "Unknown",
                    "type": "rng"
                },
                {
                    "name": "Blackjack",
                    "type": "rng"
                },
                {
                    "name": "Roulette",
                    "type": "rng"
                },
                {
                    "name": "Live Lobby",
                    "type": "live_dealer"
                },
                {
                    "name": "Crash Games",
                    "type": "rng"
                },
                {
                    "name": "Fishing Games",
                    "type": "rng"
                },
                {
                    "name": "Virtual Sports",
                    "type": "rng"
                }
            ],
            "developers": [
                {
                    "code": "rtg",
                    "name": "Red Tiger",
                    "restricted_territories": []
                },
                {
                    "code": "ntn",
                    "name": "NetEnt",
                    "restricted_territories": []
                },
                {
                    "code": "evo",
                    "name": "Evolution Gaming",
                    "restricted_territories": []
                },
                {
                    "code": "png",
                    "name": "Play\'n Go",
                    "restricted_territories": []
                },
                {
                    "code": "pgp",
                    "name": "Pragmatic Play",
                    "restricted_territories": []
                },
                {
                    "code": "reg",
                    "name": "Relax Gaming",
                    "restricted_territories": []
                },
                {
                    "code": "gmz",
                    "name": "Gamzix",
                    "restricted_territories": []
                },
                {
                    "code": "plt",
                    "name": "Playtech",
                    "restricted_territories": []
                },
                {
                    "code": "nlc",
                    "name": "Nolimit City",
                    "restricted_territories": []
                },
                {
                    "code": "avx",
                    "name": "Aviatrix",
                    "restricted_territories": []
                },
                {
                    "code": "jil",
                    "name": "Jili",
                    "restricted_territories": []
                },
                {
                    "code": "oak",
                    "name": "3 Oaks Gaming",
                    "restricted_territories": []
                },
                {
                    "code": "spn",
                    "name": "Spinomenal",
                    "restricted_territories": []
                },
                {
                    "code": "btg",
                    "name": "Big Time Gaming",
                    "restricted_territories": []
                },
                {
                    "code": "wzd",
                    "name": "Wazdan",
                    "restricted_territories": []
                },
                {
                    "code": "gmb",
                    "name": "GameBeat",
                    "restricted_territories": []
                },
                {
                    "code": "rbp",
                    "name": "RubyPlay",
                    "restricted_territories": []
                },
                {
                    "code": "psh",
                    "name": "Push Gaming",
                    "restricted_territories": []
                }
            ],
            "games": [
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_10001_nights",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "10.001 Nights",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_24_hour_grand_prix",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "24 Hour Grand Prix",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_4squad",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "4Squad",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_5_families",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "5 Families",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_777_strike",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "777 Strike",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_age_of_akkadia",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Age of Akkadia",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_agent_royale",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Agent Royale",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_ancient_script",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Ancient Script",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_ancients_blessing",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Ancients Blessing",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_arcade_bomb",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Arcade Bomb",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_atlantis",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Atlantis",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_aurum_codex",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Aurum Codex",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_aztec_spins",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Aztec Spins",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_betty_boris_and_boo",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Betty Boris and Boo",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_blue_diamond",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Blue Diamond",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_bombuster",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Bombuster",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_bounty_raid",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Bounty Raid",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_cash_ultimate",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Cash Ultimate",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_cash_volt",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Cash volt",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_chinese_treasures",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Chinese Treasures",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_cinderellas_ball",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Cinderella\'s Ball",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_cirque_de_la_fortune",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Cirque de la Fortune",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_clash_of_the_beasts",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Clash of the Beasts",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "rtg_classic_blackjack",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Classic Blackjack",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_crazy_genie",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Crazy Genie",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_crystal_mirror",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Crystal Mirror",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_da_vincis_mystery",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Da Vinci\'s Mystery",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_devils_number",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Devil\'s Number",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_diamond_blitz",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Diamond Blitz",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_dice_dice_dice",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Dice Dice Dice",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_divine_ways",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Divine Ways",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_dragon_king_legend_of_the_seas",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Dragon King: Legend of the Seas",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_dragons_fire",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Dragon\'s Fire",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_dragons_fire_infinireels",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Dragon\'s Fire Infinireels",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_dragons_fire_megaways",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Dragon\'s Fire Megaways",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_dragons_luck",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Dragon\'s Luck",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_dragons_luck_deluxe",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Dragon\'s Luck Deluxe",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_dragons_luck_megaways",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Dragon\'s Luck Megaways",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_dragons_luck_power_reels",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Dragon\'s Luck Power Reels",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_dynamite_riches",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Dynamite Riches",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_dynamite_riches_megaways",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Dynamite Riches Megaways",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_eagle_riches",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Eagle Riches",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_elven_magic",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Elven Magic",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_emerald_diamond",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Emerald Diamond",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_epic_journey",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Epic Journey",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_esqueleto_mariachi",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Esqueleto Mariachi",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "rtg_european_roulette",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "European Roulette",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_five_star",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Five Star",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_five_star_power_reels",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Five Star Power Reels",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_flaming_fox",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Flaming Fox",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_forever_7s",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Forever 7’s",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_fortune_charm",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Fortune Charm",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_fortune_fest",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Fortune Fest",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_fortune_house",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Fortune House",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_fruit_blox",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Fruit Blox",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_fruit_snap",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Fruit Snap",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_gems_gone_wild",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Gems Gone Wild",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_gems_gone_wild_power_reels",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Gems Gone Wild Power Reels",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_gemtastic",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Gemtastic",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_god_of_wealth",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "God of Wealth",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_gold_star",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Gold Star",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_golden_cryptex",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Golden Cryptex",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_golden_leprechaun_megaways",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Golden Leprechaun Megaways",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_golden_lotus",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Golden Lotus",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_golden_offer",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Golden Offer",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_golden_temple",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Golden Temple",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_golden_tsar",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Golden Tsar",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_gonzos_quest_megaways",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Gonzo\'s Quest Megaways",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_grand_wheel",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Grand Wheel",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_harle_coin",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Harle Coin",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_hoard_of_poseidon",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Hoard of Poseidon",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_imperial_palace",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Imperial Palace",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_jack_in_a_pot",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Jack in a Pot",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_jackpot_quest",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Jackpot Quest",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_jade_charms",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Jade Charms",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_jester_spins",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Jester Spins",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_jewel_scarabs",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Jewel Scarabs",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_jingle_bells",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Jingle Bells",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_jingle_bells_power_reels",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Jingle Bells Power Reels",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_joe_exotic",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Joe Exotic",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_laser_fruit",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Laser Fruit",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_legend_of_athena",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Legend of Athena",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_legendary_excalibur",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Legendary Excalibur",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_lion_dance",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Lion Dance",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_lucky_easter",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Lucky Easter",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_lucky_fortune_cat",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Lucky Fortune Cat",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_lucky_fridays",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Lucky Fridays",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_lucky_halloween",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Lucky Halloween",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_lucky_little_devil",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Lucky Little Devil",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_lucky_oktoberfest",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Lucky Oktoberfest",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_lucky_valentine",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Lucky Valentine",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_lucky_wizard",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Lucky Wizard",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_magic_gate",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Magic Gate",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_masquerade",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Masquerade",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_mayan_gods",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Mayan Gods",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_mega_dragon",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Mega Dragon",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_mega_jade",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Mega Jade",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_mega_pyramid",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Mega Pyramid",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_mega_rise",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Mega Rise",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_midas_gold",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Midas Gold",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_multiplier_riches",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Multiplier Riches",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_mystery_reels",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Mystery Reels",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_mystery_reels_megaways",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Mystery Reels Megaways",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_mystery_reels_power_reels",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Mystery Reels Power Reels",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_mystic_staxx",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Mystic Staxx",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_mystic_wheel",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Mystic Wheel",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_neon_links",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Neon Links",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_nft_megaways",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "NFT Megaways",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_night_roller",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Night roller",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_ninja_ways",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Ninja Ways",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_ocean_fortune",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Ocean Fortune",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_path_of_destiny",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Path of Destiny",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_persian_fortune",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Persian Fortune",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_phoenix",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Phoenix",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_phoenix_fire_power_reels",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Phoenix Fire Power Reels",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_piggy_pirates",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Piggy Pirates",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_piggy_riches_megaways",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Piggy Riches Megaways",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_pirates_plenty",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Pirates\' Plenty",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_pirates_plenty_battle_for_gold",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Pirates\' Plenty Battle for Gold",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_pirates_plenty_megaways",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Pirates\' Plenty MegaWays",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_primate_king",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Primate King",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "rtg_punto_banco",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Punto Banco",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_puss_n_boots",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Puss \'N Boots",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_ras_legend",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "RA\'s Legend",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_rainbow_jackpots",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Rainbow Jackpots",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_rainbow_jackpots_power_lines",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Rainbow Jackpots Power Lines",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_reactor",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Reactor",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_red_diamond",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Red Diamond",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_reel_heist",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Reel Heist",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_reel_keeper",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Reel Keeper",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_reel_king_mega",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Reel King Mega",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_regal_beasts",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Regal Beasts",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_regal_streak",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Regal Streak",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_reptizillions_power_reels",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Reptizillions Power Reels",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_riddle_of_the_sphinx",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Riddle Of The Sphinx",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_rio_stars",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Rio Stars",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_robin_hoods_wild_forest",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Robin Hoods Wild Forest",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_rocket_men",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Rocket Men",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_royal_gems",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Royal Gems",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_shah_mat",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Shah Mat",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_snow_wild_and_the_7_features",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Snow Wild And The 7 Features",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_spin_town",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Spin Town",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_stage_888",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Stage 888",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_sumo_spins",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Sumo Spins",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_sylvan_spirits",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Sylvan Spirits",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_ten_elements",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Ten Elements",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_the_equalizer",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "The Equalizer",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_the_greatest_train_robbery",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "The Greatest Train Robbery",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_the_wild_hatter",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "The Wild Hatter",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_the_wisecracker_lightning",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "The Wisecracker Lightning",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_thors_lightning",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Thor\'s Lightning",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_thors_vengeance",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Thor\'s vengeance",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_three_kingdoms",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Three Kingdoms",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_three_musketeers",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Three Musketeers",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_tiki_fruits",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Tiki Fruits",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_tiki_fruits_totem_frenzy",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Tiki Fruits Totem Frenzy",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_totem_lightning",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Totem Lightning",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_totem_lightning_power_reels",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Totem Lightning Power Reels",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_treasure_mine",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Treasure Mine",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_treasure_mine_power_reels",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Treasure Mine Power Reels",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_trillionaire",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Trillionaire",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_vault_cracker",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Vault Cracker",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_vault_of_anubis",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Vault of Anubis",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_vicky_ventura",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Vicky Ventura",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_war_of_gods",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "War Of Gods",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_well_of_wilds_megaways",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Well of Wilds Megaways",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_well_of_wishes",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Well Of Wishes",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_what_the_fox_megaways",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "What the Fox MegaWays",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_wild_cats_multiline",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Wild Cats Multiline",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_wild_circus",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Wild Circus",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_wild_elements",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Wild Elements",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_wild_fight",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Wild Fight",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_wild_hot_chilli_reels",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Wild Hot Chilli Reels",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_wild_nords",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Wild Nords",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_wild_oclock",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Wild O\'Clock",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_wild_spartans",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Wild Spartans",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_wild_wild_chest",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Wild Wild Chest",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_win_escalator",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Win Escalator",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_wings_of_ra",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Wings of Ra",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_winter_wonders",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Winter Wonders",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_yucatans_mystery",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Yucatan\'s Mystery",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "rtg_zeus_lightning_power_reels",
                    "deprecation_date": null,
                    "developer": "Red Tiger",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Zeus Lightning Power Reels",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_aloha_christmas",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Aloha! Christmas",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_aloha_cluster_pays",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Aloha! Cluster Pays",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "ntn_american_roulette",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "American Roulette",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_asgardian_stones",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Asgardian Stones",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "ntn_baccarat",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Baccarat",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "ntn_blackjack",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Blackjack",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_blood_suckers",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Blood Suckers",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_blood_suckers_ii",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Blood Suckers II",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_butterfly_staxx",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Butterfly Staxx",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_butterfly_staxx_2",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Butterfly Staxx 2",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_codex_of_fortune",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Codex of Fortune",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_dark_king_forbidden_riches",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Dark King: Forbidden Riches",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_dazzle_me",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Dazzle Me",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_dazzle_me_megaways",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Dazzle Me Megaways",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_dead_or_alive",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Dead or Alive",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_dead_or_alive_2",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Dead or Alive 2",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_dead_or_alive_2_feature_buy",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Dead or Alive 2 Feature Buy",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_divine_fortune_megaways",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Divine Fortune Megaways",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_double_stacks",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Double Stacks",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_drive_multiplier_mayhem",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Drive: Multiplier Mayhem",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_elements_the_awakening",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Elements: The Awakening",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "ntn_european_roulette",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "European Roulette",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_fairytale_legends_red_riding_hood",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Fairytale Legends: Red Riding Hood",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_finn_and_the_swirly_spin",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Finn and the Swirly Spin",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_finns_golden_tavern",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Finn\'s Golden Tavern",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_flowers",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Flowers",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "ntn_french_roulette",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "French Roulette",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_fruit_blaze",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Fruit Blaze",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_fruit_shop",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Fruit Shop",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_fruit_shop_christmas_edition",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Fruit Shop Christmas Edition",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_fruit_shop_megaways",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Fruit Shop Megaways",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_fruit_spin",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Fruit Spin",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_gonzos_quest",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Gonzo\'s Quest",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_gordon_ramsay_hells_kitchen",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Gordon Ramsay Hell’s Kitchen",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_gorilla_kingdom",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Gorilla Kingdom",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_guns_n_roses_video_slots",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Guns N\' Roses video Slots",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_hotline",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Hotline",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_hotline_2",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Hotline 2",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_irish_pot_luck",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Irish Pot Luck",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_jack_and_the_beanstalk",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Jack and the Beanstalk",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_jack_hammer",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Jack Hammer",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_jack_hammer_2_fishy_business",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Jack Hammer 2: Fishy Business",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_jimi_hendrix_online_slot",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Jimi Hendrix Online Slot",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_jingle_spin",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Jingle Spin",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_jumanji",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Jumanji",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_jungle_spirit_call_of_the_wild",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Jungle Spirit: Call of the Wild",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_koi_princess",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Koi Princess",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_lost_relics",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Lost Relics",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_motorhead_video_slot",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Motorhead video Slot",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_narcos",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Narcos",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_ozzy_osbourne_video_slots",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Ozzy Osbourne video Slots",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_parthenon_quest_for_immortality",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Parthenon: Quest for Immortality",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_piggy_riches",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Piggy Riches",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_pyramid_quest_for_immortality",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Pyramid: Quest for Immortality",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_rage_of_the_seas",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Rage of the Seas",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_reef_raider",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Reef Raider",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_reel_rush",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Reel Rush",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_riches_of_midgard_land_and_expand",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Riches of Midgard: Land and Expand",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_rome_the_golden_age",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Rome: The Golden Age",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_scruffy_duck",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Scruffy Duck",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_secret_of_the_stones",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Secret of the Stones",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_secrets_of_atlantis",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Secrets of Atlantis",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_secrets_of_christmas",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Secrets of Christmas",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_serengeti_kings",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Serengeti Kings",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_space_wars",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Space Wars",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_starburst",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Starburst",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_starburst_xxxtreme",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Starburst XXXtreme",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_steam_tower",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Steam Tower",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_street_fighter_ii_the_world_warrior_slot",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Street Fighter II: The World Warrior Slot",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_the_invisible_man",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "The Invisible Man",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_turn_your_fortune",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Turn Your Fortune",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_twin_spin",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Twin Spin",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_twin_spin_megaways",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Twin Spin Megaways",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_victorious",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Victorious",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_vikings_video_slot",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Vikings Video Slot",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_warlords_crystals_of_power",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Warlords: Crystals of Power",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_wild_water",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Wild Water",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_wild_wild_west_the_great_train_heist",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Wild Wild West: The Great Train Heist",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_wild_worlds",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Wild Worlds",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_willys_hot_chillies",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Willy\'s Hot Chillies",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Roulette",
                    "code": "evo_roulette",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Roulette",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Poker",
                    "code": "evo_three_card_poker",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Three Card Poker",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Blackjack",
                    "code": "evo_blackjack_a",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Blackjack A",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Game Show",
                    "code": "evo_dream_catcher",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Dream Catcher",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets",
                        "free_money"
                    ],
                    "category": "Game Show",
                    "code": "evo_crazy_time",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Crazy Time",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Dealer",
                    "code": "evo_side_bet_city",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Side Bet City",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Poker",
                    "code": "evo_caribbean_stud_poker",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Caribbean Stud Poker",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Baccarat",
                    "code": "evo_baccarat_squeeze",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Baccarat Squeeze",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Blackjack",
                    "code": "evo_free_bet_blackjack",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Free Bet Blackjack",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Dragon Tiger",
                    "code": "evo_dragon_tiger",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Dragon Tiger",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Roulette",
                    "code": "evo_french_roulette_gold",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "French Roulette Gold",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Poker",
                    "code": "evo_2_hand_casino_holdem",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "2 Hand Casino Hold\'em",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Baccarat",
                    "code": "evo_no_commission_baccarat",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "No Commission Baccarat",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Roulette",
                    "code": "evo_immersive_roulette",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Immersive Roulette",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_money"
                    ],
                    "category": "Game Show",
                    "code": "evo_monopoly_live",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "MONOPOLY Live",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Roulette",
                    "code": "evo_lightning_roulette",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Lightning Roulette",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Baccarat",
                    "code": "evo_baccarat_control_squeeze",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Baccarat Control Squeeze",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Baccarat",
                    "code": "evo_lightning_baccarat",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Lightning Baccarat",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Poker",
                    "code": "evo_triple_card_poker",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Triple Card Poker",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Roulette",
                    "code": "evo_double_ball_roulette",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Double Ball Roulette",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Roulette",
                    "code": "evo_autoroulette",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Auto-Roulette",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Poker",
                    "code": "evo_casino_holdem",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Casino Hold\'em",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Roulette",
                    "code": "evo_instant_roulette",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Instant Roulette",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Game Show",
                    "code": "evo_deal_or_no_deal",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Deal or No Deal",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Game Show",
                    "code": "evo_mega_ball",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Mega Ball",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Game Show",
                    "code": "evo_lightning_dice",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Lightning Dice",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Blackjack",
                    "code": "evo_blackjack_b",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Blackjack B",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Game Show",
                    "code": "evo_gonzos_treasure_hunt",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Gonzo\'s Treasure Hunt",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Poker",
                    "code": "evo_texas_holdem_bonus_poker",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Texas Hold\'em Bonus Poker",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Blackjack",
                    "code": "evo_blackjack_c",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Blackjack C",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Baccarat",
                    "code": "evo_baccarat_a",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [
                        ""
                    ],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Baccarat A",
                    "release_date": null,
                    "rtp": null,
                    "themes": [
                        ""
                    ],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Poker",
                    "code": "evo_extreme_texas_holdem",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Extreme Texas Hold\'em",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Blackjack",
                    "code": "evo_infinite_blackjack",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Infinite Blackjack",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Poker",
                    "code": "evo_ultimate_texas_holdem",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Ultimate Texas Hold\'em",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Game Show",
                    "code": "evo_football_studio",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Football Studio",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Sic Bo",
                    "code": "evo_super_sic_bo",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Super Sic Bo",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Blackjack",
                    "code": "evo_power_blackjack",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Power Blackjack",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Roulette",
                    "code": "evo_american_roulette",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "American Roulette",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Game Show",
                    "code": "evo_first_person_dream_catcher",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "First Person Dream Catcher",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Blackjack",
                    "code": "evo_blackjack_vip_a",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Blackjack VIP A",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Roulette",
                    "code": "evo_vip_roulette",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "VIP Roulette",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_snakebite",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Snakebite",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_dr_toonz",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Dr Toonz",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "pgp_star_pirates_code",
                    "deprecation_date": null,
                    "developer": "Pragmatic Play",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Star Pirates Code",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_pearl_lagoon",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Pearl Lagoon",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_24k_dragon",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "24k Dragon",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_3_clown_monty",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "3 Clown Monty",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Table Games",
                    "code": "png_3hand_casino_holdem",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "3-Hand Casino Hold\'em",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_5x_magic",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "5x Magic",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_7_sins",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "7 Sins",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_ace_of_spades",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Ace of Spades",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_agent_destiny",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Agent Destiny",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Unknown",
                    "code": "png_agent_of_hearts",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Agent of Hearts",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_alice_cooper_and_the_tome_of_madness",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Alice Cooper and the Tome of Madness",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_ankh_of_anubis",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Ankh of Anubis",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_annihilator",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Annihilator",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_aztec_idols",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Aztec Idols",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_aztec_warrior_princess",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Aztec Warrior Princess",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_bakers_treat",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Baker\'s Treat",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_banana_rock",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Banana Rock",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_battle_royal",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Battle Royal",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_beast_of_wealth",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Beast of Wealth",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_beasts_of_fire",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Beasts of Fire",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "png_bell_of_fortune",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Bell of Fortune",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_big_win_777",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Big Win 777",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_big_win_cat",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Big Win Cat",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_black_mamba",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Black Mamba",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Blackjack",
                    "code": "png_blackjack_mh",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "BlackJack MH",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_blazin_bullfrog",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Blazin\' Bullfrog",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_blinged",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Blinged",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_book_of_dead",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Book of Dead",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Unknown",
                    "code": "png_bugs_party",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Bugs Party",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_bull_in_a_china_shop",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Bull in a China Shop",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_candy_island_princess",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Candy Island Princess",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_cash_pump",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Cash Pump",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_cash_vandal",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Cash Vandal",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Table Games",
                    "code": "png_casino_holdem",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Casino Hold\'em",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Table Games",
                    "code": "png_casino_stud_poker",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Casino Stud Poker",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_cat_wilde_and_the_doom_of_dead",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Cat Wilde and the Doom of Dead",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_cat_wilde_in_the_eclipse_of_the_sun_god",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Cat Wilde in the Eclipse of the Sun God",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_cats_and_cash",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Cats and Cash",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_celebration_of_wealth",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Celebration of Wealth",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_charlie_chance",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Charlie Chance",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_charlie_chance_and_the_curse_of_cleopatra",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Charlie Chance and the Curse of Cleopatra",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_charlie_chance_in_hell_to_pay",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Charlie Chance in Hell to Pay",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_chinese_new_year",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Chinese New Year",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_chronos_joker",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Chronos Joker",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_cloud_quest",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Cloud Quest",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Unknown",
                    "code": "png_coils_of_cash",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Coils of Cash",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_contact",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Contact",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_copsnrobbers",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Cops\'n\'Robbers",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_coywolf_cash",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Coywolf Cash",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_crazy_cows",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Crazy Cows",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_crystal_sun",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Crystal Sun",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_dawn_of_egypt",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Dawn of Egypt",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_demon",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Demon",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Unknown",
                    "code": "png_deuces_wild_mh",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Deuces Wild MH",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_diamonds_of_the_realm",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Diamonds of the Realm",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_diamond_vortex",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Diamond Vortex",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_disco_diamonds",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Disco Diamonds",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_divine_showdown",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Divine Showdown",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_doom_of_egypt",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Doom of Egypt",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Blackjack",
                    "code": "png_double_exposure_blackjack_mh",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Double Exposure BlackJack MH",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_dragon_maiden",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Dragon Maiden",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_dragon_ship",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Dragon Ship",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_easter_eggs",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Easter Eggs",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_enchanted_crystals",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Enchanted Crystals",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_enchanted_meadow",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Enchanted Meadow",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_energoonz",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Energoonz",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Blackjack",
                    "code": "png_european_blackjack_mh",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "European BlackJack MH",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Roulette",
                    "code": "png_european_roulette_pro",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "European Roulette Pro",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_eye_of_the_kraken",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Eye of the Kraken",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_feline_fury",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Feline Fury",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_fire_joker",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Fire Joker",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_fire_joker_freeze",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Fire Joker Freeze",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_fire_toad",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Fire Toad",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_firefly_frenzy",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Firefly Frenzy",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Unknown",
                    "code": "png_flying_pigs",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Flying Pigs",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_fortune_teller",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Fortune Teller",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_fortunes_of_ali_baba",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Fortunes of Ali Baba",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_free_reelin_joker",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Free Reelin\' Joker",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_frozen_gems",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Frozen Gems",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_fruit_bonanza",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Fruit Bonanza",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_fu_er_dai",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "FU ER DAI",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_game_of_gladiators",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Game of Gladiators",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_gemix",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Gemix",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_gemix_2",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Gemix 2",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_ghost_of_dead",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Ghost of Dead",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "png_gift_shop",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Gift Shop",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_gold_king",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Gold King",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_gold_trophy_2",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Gold Trophy 2",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Unknown",
                    "code": "png_gold_volcano",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Gold Volcano",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_golden_caravan",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Golden Caravan",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_golden_colts",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Golden Colts",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "png_golden_goal",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Golden Goal",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_golden_legend",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Golden Legend",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_golden_osiris",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Golden Osiris",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_golden_ticket",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Golden Ticket",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_golden_ticket_2",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Golden Ticket 2",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_grim_muerto",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Grim Muerto",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_gunslinger_reloaded",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Gunslinger: Reloaded",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_hammerfall",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "HammerFall",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_happy_halloween",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Happy Halloween",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_helloween",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Helloween",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_holiday_season",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Holiday Season",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_holiday_spirits",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Holiday Spirits",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_honey_rush",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Honey Rush",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_hotel_yetiway",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Hotel Yeti-Way",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_house_of_doom",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "House of Doom",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_house_of_doom_2_the_crypt",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "House of Doom 2: The Crypt",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_hugo",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Hugo",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_hugo_2",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Hugo 2",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_hugo_carts",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Hugo Carts",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_hugo_goal",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Hugo Goal",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_hugos_adventure",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Hugo\'s Adventure",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_ice_joker",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Ice Joker",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_imperial_opera",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Imperial Opera",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_inferno_joker",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Inferno Joker",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_inferno_star",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Inferno Star",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_irish_gold",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Irish Gold",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_iron_girl",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Iron Girl",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Unknown",
                    "code": "png_jackpot_poker",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Jackpot Poker",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Unknown",
                    "code": "png_jacks_or_better_mh",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Jacks or Better MH",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_jade_magician",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Jade Magician",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_jewel_box",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Jewel Box",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Unknown",
                    "code": "png_joker_poker_mh",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Joker Poker MH",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_jolly_roger",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Jolly Roger",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_jolly_roger_2",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Jolly Roger 2",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Unknown",
                    "code": "png_keno",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Keno",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_kiss_reels_of_rock",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "KISS REELS OF ROCK",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_lady_of_fortune",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Lady of Fortune",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_legacy_of_dead",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Legacy of Dead",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_legacy_of_egypt",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Legacy of Egypt",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_legend_of_the_ice_dragon",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Legend of the Ice Dragon",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_leprechaun_goes_egypt",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Leprechaun goes Egypt",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_leprechaun_goes_to_hell",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Leprechaun goes to Hell",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_leprechaun_goes_wild",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Leprechaun Goes Wild",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_lord_merlin_and_the_lady_of_the_lake",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Lord Merlin and The Lady of The Lake",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_lucky_diamonds",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Lucky Diamonds",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_madame_ink",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Madame Ink",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_mahjong_88",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Mahjong 88",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_matsuri",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Matsuri",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_mermaids_diamond",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Mermaid\'s Diamond",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_merry_xmas",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Merry Xmas",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_miner_donkey_trouble",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Miner Donkey Trouble",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Table Games",
                    "code": "png_mini_baccarat",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Mini Baccarat",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_mission_cash",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Mission Cash",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Table Games",
                    "code": "png_money_wheel",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Money Wheel",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_moon_princess",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Moon Princess",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_moon_princess_christmas_kingdom",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Moon Princess: Christmas Kingdom",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_muerto_en_mictlan",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Muerto en Mictlan",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_multifruit_81",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "MULTIFRUIT 81",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_mystery_joker",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Mystery Joker",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_mystery_joker_6000",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Mystery Joker 6000",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_myth",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Myth",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_new_year_riches",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "New Year Riches",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_ninja_fruits",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Ninja Fruits",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_nyjah_huston_skate_for_gold",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Nyjah Huston: Skate for Gold",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_octopus_treasure",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Octopus Treasure",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_odin_protector_of_realms",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Odin: Protector of Realms",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_pack__cash",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Pack & Cash",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_pearls_of_india",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Pearls of India",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Unknown",
                    "code": "png_perfect_gems",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Perfect Gems",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_phoenix_reborn",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Phoenix Reborn",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_photo_safari",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Photo Safari",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_piggy_bank_farm",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Piggy Bank Farm",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_pimped",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Pimped",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_planet_fortune",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Planet Fortune",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_prism_of_gems",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Prism of Gems",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_prissy_princess",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Prissy Princess",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_prosperity_palace",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Prosperity Palace",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_queens_day_tilt",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Queen\'s Day Tilt",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_rabbit_hole_riches",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Rabbit Hole Riches",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_rabbit_hole_riches__court_of_hearts",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Rabbit Hole Riches - Court of Hearts",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_rage_to_riches",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Rage to Riches",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_raging_rex",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Raging Rex",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_rainforest_magic",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Rainforest Magic",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Unknown",
                    "code": "png_rainforest_magic_bingo",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Rainforest Magic Bingo",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_rally_4_riches",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Rally 4 Riches",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_reactoonz",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Reactoonz",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_reactoonz_2",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Reactoonz 2",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_rich_wilde__the_shield_of_athena",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Rich Wilde & The Shield of Athena",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_rich_wilde_and_the_amulet_of_dead",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Rich Wilde and the Amulet of Dead",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_rich_wilde_and_the_wandering_city",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Rich Wilde and the Wandering City",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_riches_of_ra",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Riches of RA",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_riches_of_robin",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Riches of Robin",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_riddle_reels_a_case_of_riches",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Riddle Reels: A Case of Riches",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_ring_of_odin",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Ring of Odin",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_rise_of_athena",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Rise of Athena",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_rise_of_dead",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Rise of Dead",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_rise_of_merlin",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Rise of Merlin",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_rise_of_olympus",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Rise of Olympus",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_royal_masquerade",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Royal Masquerade",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_sabaton",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Sabaton",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_sails_of_gold",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Sails of Gold",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_samba_carnival",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Samba Carnival",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_saxon",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Saxon",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_scroll_of_dead",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Scroll of Dead",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_sea_hunter",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Sea Hunter",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Blackjack",
                    "code": "png_single_deck_blackjack_mh",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Single Deck BlackJack MH",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_sisters_of_the_sun",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Sisters of the Sun",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_sizzling_spins",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Sizzling Spins",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_space_race",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Space Race",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_sparky_shortz",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Sparky&Shortz",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "png_speed_cash",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Speed Cash",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_spin_party",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Spin Party",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_star_joker",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Star Joker",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_sticky_joker",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Sticky Joker",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_street_magic",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Street Magic",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_super_flip",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Super Flip",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Table Games",
                    "code": "png_super_wheel",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Super Wheel",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_sweet_27",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Sweet 27",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_sweet_alchemy",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Sweet Alchemy",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Unknown",
                    "code": "png_sweet_alchemy_bingo",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Sweet Alchemy Bingo",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_tales_of_asgard_lokis_fortune",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Tales of Asgard: Loki\'s Fortune",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_temple_of_wealth",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Temple of Wealth",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_testament",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Testament",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_thats_rich",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "That\'s Rich",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_the_faces_of_freya",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "The Faces of Freya",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_the_green_knight",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "The Green Knight",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_the_last_sundown",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "The Last Sundown",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_the_paying_piano_club",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "The Paying Piano Club",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_the_shimmering_woods",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "The Shimmering Woods",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_the_sword_and_the_grail",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "The Sword and The Grail",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_the_wild_class",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "The Wild Class",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_thunder_screech",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Thunder Screech",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_tome_of_madness",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Tome of Madness",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_tower_quest",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Tower Quest",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_troll_hunters",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Troll Hunters",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_troll_hunters_2",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Troll Hunters 2",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_twisted_sister",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Twisted Sister",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_viking_runecraft",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Viking Runecraft",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Unknown",
                    "code": "png_viking_runecraft_bingo",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Viking Runecraft Bingo",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_wild_blood",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Wild Blood",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_wild_blood_2",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Wild Blood 2",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_wild_falls",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Wild Falls",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_wild_frames",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Wild Frames",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_wild_melon",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Wild Melon",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_wild_north",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Wild North",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_wild_rails",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Wild Rails",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_wildhound_derby",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Wildhound Derby",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_winabeest",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Win-A-Beest",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_wizard_of_gems",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Wizard of Gems",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_xmas_joker",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Xmas Joker",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_xmas_magic",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Xmas Magic",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "png_zz_top_roadside_riches",
                    "deprecation_date": null,
                    "developer": "Play\'n Go",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "ZZ Top Roadside Riches",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_money_train_2",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Money Train 2",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_money_cart_2",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Money Cart 2",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "reg_plunderland",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": false,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Plunderland",
                    "release_date": "2022-01-11",
                    "rtp": "96.43",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_hazakura_ways",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Hazakura Ways",
                    "release_date": "2022-01-25",
                    "rtp": "96.37",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_frequent_flyer",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Frequent Flyer",
                    "release_date": "2020-12-02",
                    "rtp": "96.14",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_lets_get_ready_to_rumble",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Let\'s get ready to Rumble",
                    "release_date": "2019-09-24",
                    "rtp": "96.13",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_the_great_pigsby",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "The Great Pigsby",
                    "release_date": "2019-01-09",
                    "rtp": "96.55",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_epic_joker",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Epic Joker",
                    "release_date": "2018-11-06",
                    "rtp": "97",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_mega_masks",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Mega Masks",
                    "release_date": "2020-03-03",
                    "rtp": "96.41",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_attila_the_hun",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Attila The Hun",
                    "release_date": "2020-02-17",
                    "rtp": "96.38",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_heroes_gathering",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Heroe\'s Gathering ",
                    "release_date": "2020-02-04",
                    "rtp": "96.25",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_dragons_awakening",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Dragons Awakening",
                    "release_date": "2019-03-06",
                    "rtp": "96.96",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_erik_the_red",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Erik the Red",
                    "release_date": "2018-10-02",
                    "rtp": "96.95",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_money_cart",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Money Cart",
                    "release_date": "2021-12-07",
                    "rtp": "98",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_wild_chapo",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Wild Chapo",
                    "release_date": "2021-04-08",
                    "rtp": "96.36",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_la_fiesta",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "La Fiesta",
                    "release_date": "2020-08-05",
                    "rtp": "96.19",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_marching_legions",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Marching Legions",
                    "release_date": "2020-07-02",
                    "rtp": "98.12",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_tnt_tumble",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "TNT Tumble",
                    "release_date": "2020-04-01",
                    "rtp": "96.11",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_tower_tumble",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Tower Tumble",
                    "release_date": "2019-11-07",
                    "rtp": "96.11",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_its_time",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "It\'s Time",
                    "release_date": "2019-10-02",
                    "rtp": "96.74",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_powerspin",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Powerspin",
                    "release_date": "2019-07-17",
                    "rtp": "96.84",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_ignite_the_night",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Ignite The Night",
                    "release_date": "2019-06-04",
                    "rtp": "96.39",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_caveman_bob",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Caveman Bob",
                    "release_date": "2019-05-02",
                    "rtp": "96.5",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_santas_stack",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Santa\'s Stack",
                    "release_date": "2021-11-25",
                    "rtp": "97",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_cluster_tumble",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Cluster Tumble",
                    "release_date": "2021-11-03",
                    "rtp": "96.57",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_mega_mine",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Mega Mine",
                    "release_date": "2021-10-20",
                    "rtp": "96.14",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_helios_fury",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Helios Fury",
                    "release_date": "2021-10-06",
                    "rtp": "96.27",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_volatile_vikings",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Volatile Vikings",
                    "release_date": "2021-09-28",
                    "rtp": "96.23",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_dead_mans_trail",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Dead Man\'s Trail",
                    "release_date": "2021-09-07",
                    "rtp": "96.29",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "reg_trolls_gold",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Troll\'s Gold",
                    "release_date": "2021-08-24",
                    "rtp": "96.36",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_deep_descent",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Deep Descent",
                    "release_date": "2021-08-03",
                    "rtp": "95",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_chip_spin",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Chip Spin",
                    "release_date": "2021-07-06",
                    "rtp": "96.74",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_spirit_of_the_beast",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Spirit of The Beast",
                    "release_date": "2021-06-22",
                    "rtp": "96",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_top_dawg",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Top Dawg$",
                    "release_date": "2021-06-02",
                    "rtp": "96.1",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_sails_of_fortune",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Sails of Fortune",
                    "release_date": "2021-05-19",
                    "rtp": "96.17",
                    "themes": [],
                    "volatility": 4
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_book_of_99",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Book of 99",
                    "release_date": "2021-05-04",
                    "rtp": "99",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_templar_tumble",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Templar Tumble",
                    "release_date": "2021-03-24",
                    "rtp": "96.25",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_multiplier_odyssey",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Multiplier Odyssey",
                    "release_date": "2021-02-04",
                    "rtp": "96.5",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_kluster_krystals_megaclusters",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Kluster Krystals Megaclusters",
                    "release_date": "2021-01-12",
                    "rtp": "96.49",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_iron_bank",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Iron Bank",
                    "release_date": "2020-11-05",
                    "rtp": "96.2",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_ramses_revenge",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Ramses Revenge",
                    "release_date": "2020-10-28",
                    "rtp": "96.15",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_mega_flip",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Mega Flip",
                    "release_date": "2020-06-02",
                    "rtp": "96.2",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_hellcatraz",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Hellcatraz",
                    "release_date": "2020-05-05",
                    "rtp": "96.46",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_snake_arena",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Snake Arena",
                    "release_date": "2020-01-07",
                    "rtp": "96.25",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_money_train",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Money Train",
                    "release_date": "2019-08-06",
                    "rtp": "96.2",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_king_of_kings",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "King of Kings",
                    "release_date": "2019-04-03",
                    "rtp": "96.25",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_temple_tumble",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Temple Tumble",
                    "release_date": "2019-02-06",
                    "rtp": "96.25",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_zombie_circus",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Zombie Circus",
                    "release_date": "2018-10-24",
                    "rtp": "96.92",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "reg_wildchemy",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Wildchemy",
                    "release_date": "2019-12-03",
                    "rtp": "96.02",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "reg_emeralds_infinity_reels",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Emerald\'s Infinity Reels",
                    "release_date": "2021-03-11",
                    "rtp": "96.6",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "reg_blackjack_6_decks",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Blackjack 6 Decks",
                    "release_date": "2021-09-30",
                    "rtp": "99.59",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "reg_blackjack_neo",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Blackjack Neo",
                    "release_date": "2017-09-12",
                    "rtp": "99.6",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "reg_roulette_nouveau",
                    "deprecation_date": null,
                    "developer": "Relax Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Roulette Nouveau",
                    "release_date": "2015-01-01",
                    "rtp": "97.3",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Lobby",
                    "code": "evo_top_games_lobby",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Top Games Lobby",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Baccarat",
                    "code": "evo_golden_wealth_baccarat",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Golden Wealth Baccarat",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Sic Bo",
                    "code": "evo_bac_bo",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Bac Bo",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Game Show",
                    "code": "evo_cash_or_crash",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Cash Or Crash",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Crash Games",
                    "code": "pgp_spaceman",
                    "deprecation_date": null,
                    "developer": "Pragmatic Play",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Spaceman",
                    "release_date": null,
                    "rtp": "96.5",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Fishing Games",
                    "code": "pgp_fishing_king",
                    "deprecation_date": null,
                    "developer": "Pragmatic Play",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Fishing King",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "pgp_7_monkeys_jackpot",
                    "deprecation_date": null,
                    "developer": "Pragmatic Play",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "7 Monkeys Jackpot",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "pgp_gates_of_olympus",
                    "deprecation_date": null,
                    "developer": "Pragmatic Play",
                    "enabled": true,
                    "features": [
                        "Bonus Buy"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Gates of Olympus",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "pgp_sweet_bonanza",
                    "deprecation_date": null,
                    "developer": "Pragmatic Play",
                    "enabled": true,
                    "features": [
                        "Bonus Buy"
                    ],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Sweet Bonanza",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "pgp_7_monkeys",
                    "deprecation_date": null,
                    "developer": "Pragmatic Play",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "7 Monkeys",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "pgp_multihand_blackjack",
                    "deprecation_date": null,
                    "developer": "Pragmatic Play",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Multihand Blackjack",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "pgp_wild_wild_riches",
                    "deprecation_date": null,
                    "developer": "Pragmatic Play",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Wild Wild Riches",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_40_chilli_fruits",
                    "deprecation_date": "2023-05-12",
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "40 Chilli Fruits",
                    "release_date": "2022-04-11",
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_the_lion",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "The Lion",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_joker_splash",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Joker Splash",
                    "release_date": "2020-06-12",
                    "rtp": "96.26",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_book_of_symbols",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Book Of Symbols",
                    "release_date": "2020-06-12",
                    "rtp": "96.43",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_really_hot",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Really Hot",
                    "release_date": "2020-06-12",
                    "rtp": "96.15",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_go_wild",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Go Wild",
                    "release_date": "2020-06-12",
                    "rtp": "96.35",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_dragons_secret",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Dragons Secret",
                    "release_date": "2020-06-12",
                    "rtp": "96.1",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_gold_of_maya",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Gold Of Maya",
                    "release_date": "2020-07-20",
                    "rtp": "96.6",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_banana_bar",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Banana Bar",
                    "release_date": "2020-07-20",
                    "rtp": "96.02",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_the_hottest_game",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "The Hottest Game",
                    "release_date": "2020-11-04",
                    "rtp": "95.9",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_hoonga_boonga",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Hoonga Boonga",
                    "release_date": "2020-11-04",
                    "rtp": "96.04",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_burning_power",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Burning Power",
                    "release_date": "2020-11-20",
                    "rtp": "95.5",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_hot_life",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Hot Life",
                    "release_date": "2020-11-30",
                    "rtp": "95.7",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_really_hot_flaming_edition",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Really Hot Flaming Edition",
                    "release_date": "2021-01-15",
                    "rtp": "96.5",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_make_money",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Make Money",
                    "release_date": "2021-02-23",
                    "rtp": "96.9",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_40_chilli_fruits_flaming_edition",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "40 Chilli Fruits Flaming Edition",
                    "release_date": "2021-03-23",
                    "rtp": "96.2",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_make_money_rich_edition",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Make Money Rich Edition",
                    "release_date": "2021-04-19",
                    "rtp": "96.9",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_hot_life_buy_bonus",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Hot Life Buy Bonus",
                    "release_date": "2021-06-24",
                    "rtp": "96.9",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_sunny_coin_hold_the_spin",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [
                        "In-game Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Sunny Coin: Hold The Spin",
                    "release_date": "2021-07-15",
                    "rtp": "96",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_gg_coin_hold_the_spin",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [
                        "In-game Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "GG Coin: Hold The Spin",
                    "release_date": "2021-01-09",
                    "rtp": "96.6",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "gmz_go_go",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "GO GO",
                    "release_date": "2021-01-11",
                    "rtp": "98",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_snow_coin_hold_the_spin",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [
                        "In-game Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Snow Coin: Hold The Spin",
                    "release_date": "2021-12-01",
                    "rtp": "96",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_gold_mania",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Gold Mania",
                    "release_date": "2022-02-28",
                    "rtp": "97",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_hot_patrick",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Hot Patrick",
                    "release_date": "2022-03-10",
                    "rtp": "96.2",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_really_easter",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Really Easter",
                    "release_date": "2022-04-11",
                    "rtp": "96.2",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Roulette",
                    "code": "evo_xxxtreme_lightning_roulette",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "XXXtreme Lightning Roulette",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "ntn_funk_master",
                    "deprecation_date": null,
                    "developer": "NetEnt",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Funk Master",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_fire_blaze_fire_fighter",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": false,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Fire Blaze: Fire Fighter",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Roulette",
                    "code": "plt_age_of_the_gods_roulette",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": false,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Age of the Gods Roulette",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Table Games",
                    "code": "plt_jacks_or_better",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Jacks or Better",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_cat_queen",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Cat Queen",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_geisha_story",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Geisha Story",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_halloween_fortune_2",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Halloween Fortune 2",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_santa_surprise",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": null,
                    "hit_ratio": null,
                    "name": "Santa Surprise",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_ji_xiang_8",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Ji Xiang 8",
                    "release_date": "2020-07-06",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_jade_emperor_yu_huang_da_di",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Jade Emperor Yu Huang Da Di",
                    "release_date": "2020-07-06",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_the_riches_of_don_quixote",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "The Riches of Don Quixote",
                    "release_date": "2020-04-16",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Roulette",
                    "code": "plt_super_roulette",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Super Roulette",
                    "release_date": "2020-04-16",
                    "rtp": "9.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Roulette",
                    "code": "plt_101_roulette",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "101 Roulette",
                    "release_date": "2020-04-16",
                    "rtp": "9.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_jacks_or_better_multihand",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Jacks or Better Multi-Hand",
                    "release_date": "2020-04-16",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Table Games",
                    "code": "plt_3_card_brag",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "3 Card Brag",
                    "release_date": "2020-04-06",
                    "rtp": "19.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Blackjack",
                    "code": "plt_all_bets_blackjack",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "All Bets Blackjack",
                    "release_date": "2020-04-06",
                    "rtp": "8.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Table Games",
                    "code": "plt_caribbean_stud_poker",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Caribbean Stud Poker",
                    "release_date": "2020-04-06",
                    "rtp": "19.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_cat_in_vegas",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Cat in Vegas",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_gold_rally",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Gold Rally",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_purple_hot",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Purple Hot",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_chests_of_plenty",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Chests of Plenty",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_beach_life",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Beach Life",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_leprechauns_luck",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Leprechaun\'s Luck",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_amazon_wild",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Amazon Wild",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_panther_moon",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Panther Moon",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_funky_fruits_farm",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Funky Fruits Farm",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_cops_n_bandits",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Cops N\' Bandits",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_highway_kings",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Highway Kings",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_golden_tour",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Golden Tour",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_heart_of_the_jungle",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Heart of the Jungle",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_secrets_of_the_amazon",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Secrets of the Amazon",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_bonus_bears",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Bonus Bears",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_white_king",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "White King",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_captain_treasure",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Captain Treasure",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_great_blue",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Great Blue",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_irish_luck",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Irish Luck",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_a_night_out",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "A Night Out",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_zhao_cai_jin_bao",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Zhao Cai Jin Bao",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_chinese_kitchen",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Chinese Kitchen",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_roaring_bears",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Roaring Bears",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_the_glass_slipper",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "The Glass Slipper",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_pixel_samurai",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Pixel Samurai",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_winnings_of_oz",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [
                        "Progressive Jackpot"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Winnings of Oz",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_full_moon_fortunes",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Full Moon Fortunes",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_hot_gems",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Hot Gems",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_fortunes_of_the_fox",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Fortunes of the Fox",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_mr_cashback",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Mr. Cashback",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_wild_gambler",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Wild Gambler",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_miss_fortune",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Miss Fortune",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_desert_treasure",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Desert Treasure",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_easter_surprise",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Easter Surprise",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_safari_heat",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Safari Heat",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_lotto_madness",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Lotto Madness",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_wild_gambler_2_arctic_adventure",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Wild Gambler 2: Arctic Adventure",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_atlantis_queen",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Atlantis Queen",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_heart_of_the_frontier",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Heart of the Frontier",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_fountain_of_youth",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Fountain of Youth",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_thai_paradise",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Thai Paradise",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_legacy_of_the_wild",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Legacy of the Wild",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_ice_cave",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Ice Cave",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_archer",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Archer",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_halloween_fortune",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Halloween Fortune",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_magical_stacks",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Magical Stacks",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_rome_and_glory",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Rome and Glory",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_plenty_ofortune",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Plenty O\'Fortune",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_sinbads_golden_voyage",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Sinbad\'s Golden Voyage",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_stars_awakening",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Stars Awakening",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_sacred_stones",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Sacred Stones",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_wild_wishes",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Wild Wishes",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_nostradamus",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Nostradamus",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_jungle_trouble",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Jungle Trouble",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_jurassic_island",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Jurassic Island",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_jekyll_and_hyde",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": false,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Jekyll and Hyde",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "plt_fields_of_fortune",
                    "deprecation_date": null,
                    "developer": "Playtech",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Fields of Fortune",
                    "release_date": "2020-03-23",
                    "rtp": "16.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_karen_maneater",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Karen Maneater",
                    "release_date": "2022-10-05",
                    "rtp": "94.09",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_remember_gulag",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Remember Gulag",
                    "release_date": "2022-04-19",
                    "rtp": "94.37",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_misery_mining",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Misery Mining",
                    "release_date": "2022-03-15",
                    "rtp": "94.05",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_punk_toilet",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Punk Toilet",
                    "release_date": "2022-08-02",
                    "rtp": "94.13",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_tombstone_rip",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Tombstone RIP",
                    "release_date": "2022-11-01",
                    "rtp": "94.08",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_true_grit_redemption",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "True Grit Redemption",
                    "release_date": "2021-12-14",
                    "rtp": "94.04",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_legion_x",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Legion X",
                    "release_date": "2021-11-16",
                    "rtp": "94.04",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_evil_goblins_xbomb",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Evil Goblins xBomb",
                    "release_date": "2021-10-12",
                    "rtp": "94.28",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_das_xboot",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Das xBoot",
                    "release_date": "2021-09-14",
                    "rtp": "94.04",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_mental",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Mental",
                    "release_date": "2021-08-17",
                    "rtp": "94.08",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_xways_hoarder_xsplit",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "xWays Hoarder xSplit",
                    "release_date": "2021-06-01",
                    "rtp": "94.1",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_infectious_5_xways",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Infectious 5 xWays",
                    "release_date": "2021-06-01",
                    "rtp": "94.27",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_el_paso_gunfight_xnudge",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "El Paso Gunfight xNudge",
                    "release_date": "2021-05-04",
                    "rtp": "94.26",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_bushido_ways_xnudge",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Bushido Ways xNudge",
                    "release_date": "2021-04-13",
                    "rtp": "94.04",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_fire_in_the_hole_xbomb",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Fire In The Hole xBomb",
                    "release_date": "2021-03-02",
                    "rtp": "94.11",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_east_coast_vs_west_coast",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "East Coast Vs West Coast",
                    "release_date": "2021-02-02",
                    "rtp": "94.0",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_san_quentin_xways",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "San Quentin xWays",
                    "release_date": "2021-01-12",
                    "rtp": "94.11",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_tomb_of_akenhaten",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Tomb of Akenhaten",
                    "release_date": "2020-12-15",
                    "rtp": "94.12",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_warrior_graveyard_xnudge",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Warrior Graveyard xNudge",
                    "release_date": "2020-11-03",
                    "rtp": "94.24",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_monkeys_gold_xpays",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Monkey\'s Gold xPays",
                    "release_date": "2020-10-13",
                    "rtp": "94.22",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_buffalo_hunter",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Buffalo Hunter",
                    "release_date": "2020-09-08",
                    "rtp": "94.24",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_book_of_shadows",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Book Of Shadows",
                    "release_date": "2020-08-20",
                    "rtp": "94.09",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_immortal_fruits",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Immortal Fruits",
                    "release_date": "2020-08-04",
                    "rtp": "94.08",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_golden_genie__the_walking_wilds",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Golden Genie & the Walking Wilds",
                    "release_date": "2020-07-07",
                    "rtp": "94.11",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_milky_ways",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Milky Ways",
                    "release_date": "2020-06-24",
                    "rtp": "94.24",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_bonus_bunnies",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Bonus Bunnies",
                    "release_date": "2020-06-09",
                    "rtp": "94.22",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_deadwood_xnudge",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Deadwood xNudge",
                    "release_date": "2020-05-06",
                    "rtp": "94.15",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_harlequin_carnival",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Harlequin Carnival",
                    "release_date": "2020-04-07",
                    "rtp": "94.42",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_gaelic_gold",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Gaelic Gold",
                    "release_date": "2023-05-10",
                    "rtp": "94.42",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_barbarian_fury",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Barbarian Fury",
                    "release_date": "2020-03-03",
                    "rtp": "94.19",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_punk_rocker",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Punk Rocker",
                    "release_date": "2020-02-12",
                    "rtp": "94.1",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_poison_eve",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Poison Eve",
                    "release_date": "2020-01-07",
                    "rtp": "94.35",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_dragon_tribe",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Dragon Tribe",
                    "release_date": "2019-12-12",
                    "rtp": "94.41",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_tomb_of_nefertiti",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Tomb of Nefertiti",
                    "release_date": "2019-11-05",
                    "rtp": "94.12",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_manhattan_goes_wild",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Manhattan Goes Wild",
                    "release_date": "2019-10-09",
                    "rtp": "94.29",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_pixies_vs_pirates",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Pixies vs Pirates",
                    "release_date": "2019-09-03",
                    "rtp": "94.27",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_hot_4_cash",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Hot 4 Cash",
                    "release_date": "2019-08-15",
                    "rtp": "94.23",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_thor_hammer_time",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Thor Hammer Time",
                    "release_date": "2019-08-01",
                    "rtp": "94.34",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_tractor_beam",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Tractor Beam",
                    "release_date": "2019-07-02",
                    "rtp": "94.31",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_mayan_magic_wildfire",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Mayan Magic Wildfire",
                    "release_date": "2019-06-04",
                    "rtp": "94.04",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_tombstone",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Tombstone",
                    "release_date": "2019-05-02",
                    "rtp": "94.2",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_starstruck",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Starstruck",
                    "release_date": "2019-03-27",
                    "rtp": "94.22",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_owls",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Owls",
                    "release_date": "2019-02-27",
                    "rtp": "94.49",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_fruits",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Fruits",
                    "release_date": "2019-01-30",
                    "rtp": "94.14",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_ice_ice_yeti",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Ice Ice Yeti",
                    "release_date": "2018-11-15",
                    "rtp": "94.3",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_dungeon_quest",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Dungeon Quest",
                    "release_date": "2018-09-27",
                    "rtp": "94.22",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_hot_nudge",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Hot Nudge",
                    "release_date": "2018-07-26",
                    "rtp": "94.16",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_coins_of_fortune",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Coins of Fortune",
                    "release_date": "2018-06-28",
                    "rtp": "94.13",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_tesla_jolt",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Tesla Jolt",
                    "release_date": "2018-03-28",
                    "rtp": "94.28",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_casino_win_spin",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Casino Win Spin",
                    "release_date": "2018-01-31",
                    "rtp": "94.26",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_kitchen_drama_bbq_frenzy",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Kitchen Drama BBQ Frenzy",
                    "release_date": "2017-11-29",
                    "rtp": "94.24",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_wixx",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "WiXX",
                    "release_date": "2017-10-31",
                    "rtp": "94.13",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_kitchen_drama_sushi_mania",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Kitchen Drama Sushi Mania",
                    "release_date": "2017-04-12",
                    "rtp": "94.03",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_oktoberfest",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Oktoberfest",
                    "release_date": "2016-11-15",
                    "rtp": "94.37",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "nlc_the_creepy_carnival",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "The Creepy Carnival",
                    "release_date": "2019-07-16",
                    "rtp": "94.07",
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Virtual Sports",
                    "code": "pgp_penalty_shootout",
                    "deprecation_date": null,
                    "developer": "Pragmatic Play",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Penalty Shootout",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Virtual Sports",
                    "code": "pgp_fantastic_league_football",
                    "deprecation_date": null,
                    "developer": "Pragmatic Play",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Fantastic League Football",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Virtual Sports",
                    "code": "pgp_flat_horse_racing",
                    "deprecation_date": null,
                    "developer": "Pragmatic Play",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Flat Horse Racing",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Virtual Sports",
                    "code": "pgp_greyhound_racing",
                    "deprecation_date": null,
                    "developer": "Pragmatic Play",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Greyhound Racing",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Virtual Sports",
                    "code": "pgp_force_1_racing",
                    "deprecation_date": null,
                    "developer": "Pragmatic Play",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Force 1 Racing",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Virtual Sports",
                    "code": "pgp_virtual_sports_lobby",
                    "deprecation_date": null,
                    "developer": "Pragmatic Play",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Virtual Sports Lobby",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Fishing Games",
                    "code": "pgp_blue_ocean_3d",
                    "deprecation_date": null,
                    "developer": "Pragmatic Play",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Blue Ocean 3D",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Fishing Games",
                    "code": "pgp_fortune_fishing",
                    "deprecation_date": null,
                    "developer": "Pragmatic Play",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Fortune Fishing",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Fishing Games",
                    "code": "pgp_pirate_fishing",
                    "deprecation_date": null,
                    "developer": "Pragmatic Play",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Pirate Fishing",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Crash Games",
                    "code": "avx_nft_aviatrix",
                    "deprecation_date": null,
                    "developer": "Aviatrix",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "NFT Aviatrix",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Unknown",
                    "code": "jil_hot_chilli",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Hot Chilli",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Fishing Games",
                    "code": "jil_royal_fishing",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Royal Fishing",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Unknown",
                    "code": "jil_sicbo",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "SicBo",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Unknown",
                    "code": "jil_baccarat",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Baccarat",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Unknown",
                    "code": "jil_lobby",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Lobby",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Unknown",
                    "code": "jil_number_king",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Number King",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Unknown",
                    "code": "jil_ak47",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "AK47",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Unknown",
                    "code": "jil_war_of_dragons",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "War Of Dragons",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Unknown",
                    "code": "jil_rummy",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Rummy",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets",
                        "free_money"
                    ],
                    "category": "Video Slots",
                    "code": "oak_3_coins",
                    "deprecation_date": null,
                    "developer": "3 Oaks Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "3 Coins",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Unknown",
                    "code": "jil_big_small",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Big Small",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "spn_dark_wolf",
                    "deprecation_date": null,
                    "developer": "Spinomenal",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Dark Wolf",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "spn_1_reel_egypt",
                    "deprecation_date": null,
                    "developer": "Spinomenal",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "1 Reel Egypt",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "spn_demigods_4",
                    "deprecation_date": null,
                    "developer": "Spinomenal",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Demi-Gods 4",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Dealer",
                    "code": "evo_craps_dnt",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Craps DNT",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets",
                        "free_money"
                    ],
                    "category": "Game Show",
                    "code": "evo_crazy_coin_flip",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Crazy Coin Flip",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Roulette",
                    "code": "evo_first_person_american_roulette_dnt",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "First Person American Roulette DNT",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Dealer",
                    "code": "evo_first_person_craps_dnt",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "First Person Craps DNT",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Game Show",
                    "code": "evo_first_person_deal_or_no_deal_dnt",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "First Person Deal or No Deal DNT",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Dragon Tiger",
                    "code": "evo_first_person_dragon_tiger_dnt",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "First Person Dragon Tiger DNT",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Baccarat",
                    "code": "evo_first_person_golden_wealth_baccarat_dnt",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "First Person Golden Wealth Baccarat DNT",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Baccarat",
                    "code": "evo_first_person_lightning_baccarat_dnt",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "First Person Lightning Baccarat DNT",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Blackjack",
                    "code": "evo_first_person_lightning_blackjack_dnt",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "First Person Lightning Blackjack DNT",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Game Show",
                    "code": "evo_first_person_mega_ball_dnt",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "First Person Mega Ball DNT",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Dealer",
                    "code": "evo_first_person_top_card_dnt",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "First Person Top Card DNT",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Roulette",
                    "code": "evo_gold_bar_roulette",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": false,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Gold Bar Roulette",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Blackjack",
                    "code": "evo_lightning_blackjack",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Lightning Blackjack",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Game Show",
                    "code": "evo_monopoly_big_baller",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "MONOPOLY Big Baller",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Baccarat",
                    "code": "evo_peek_baccarat",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Peek Baccarat",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Dealer",
                    "code": "evo_super_andar_bahar",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Super Andar Bahar",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Dealer",
                    "code": "evo_teen_patti",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Teen Patti",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "gmz_pilot",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Pilot",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_buffalo_coin",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Buffalo Coin",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_3x3",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "3x3",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "spn_1_reel_xmas",
                    "deprecation_date": null,
                    "developer": "Spinomenal",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "1 Reel Xmas",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "spn_1_reel_patrick",
                    "deprecation_date": null,
                    "developer": "Spinomenal",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "1 Reel Patrick",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "spn_1_reel_panther",
                    "deprecation_date": null,
                    "developer": "Spinomenal",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "1 Reel Panther",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "spn_1_reel_monkey",
                    "deprecation_date": null,
                    "developer": "Spinomenal",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "1 Reel Monkey",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "spn_1_reel_joker",
                    "deprecation_date": null,
                    "developer": "Spinomenal",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "1 Reel Joker",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "spn_1_reel_halloween",
                    "deprecation_date": null,
                    "developer": "Spinomenal",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "1 Reel Halloween",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "spn_1_reel_golden_piggy",
                    "deprecation_date": null,
                    "developer": "Spinomenal",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "1 Reel Golden Piggy",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "spn_1_reel_fruits",
                    "deprecation_date": null,
                    "developer": "Spinomenal",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "1 Reel Fruits",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "spn_1_reel_demi_god_ii",
                    "deprecation_date": null,
                    "developer": "Spinomenal",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "1 Reel Demi God II",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_rich_granny",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Rich Granny",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "gmz_book_of_cairo",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Book of Cairo",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "btg_bonanza",
                    "deprecation_date": null,
                    "developer": "Big Time Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Bonanza",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "btg_extra_chilli",
                    "deprecation_date": null,
                    "developer": "Big Time Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Extra Chilli",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "btg_white_rabbit",
                    "deprecation_date": null,
                    "developer": "Big Time Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "White Rabbit",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "wzd_9_burning_dragons",
                    "deprecation_date": null,
                    "developer": "Wazdan",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "9 Burning Dragons",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "wzd_draculas_castle",
                    "deprecation_date": null,
                    "developer": "Wazdan",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Dracula\'s Castle",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "wzd_fenix_play",
                    "deprecation_date": null,
                    "developer": "Wazdan",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Fenix Play",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "wzd_relic_hunters",
                    "deprecation_date": null,
                    "developer": "Wazdan",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Relic Hunters",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "wzd_power_of_gods",
                    "deprecation_date": null,
                    "developer": "Wazdan",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Power Of Gods",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "wzd_neon_city",
                    "deprecation_date": null,
                    "developer": "Wazdan",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Neon City",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "wzd_larry_the_leprechaun",
                    "deprecation_date": null,
                    "developer": "Wazdan",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Larry The Leprechaun",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "wzd_triple_star",
                    "deprecation_date": null,
                    "developer": "Wazdan",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Triple Star",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "wzd_choco_reels",
                    "deprecation_date": null,
                    "developer": "Wazdan",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Choco Reels",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Blackjack",
                    "code": "wzd_blackjack",
                    "deprecation_date": null,
                    "developer": "Wazdan",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Blackjack",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets",
                        "free_money"
                    ],
                    "category": "Video Slots",
                    "code": "oak_dragon_pearls",
                    "deprecation_date": null,
                    "developer": "3 Oaks Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Dragon Pearls",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets",
                        "free_money"
                    ],
                    "category": "Video Slots",
                    "code": "oak_moon_sisters",
                    "deprecation_date": null,
                    "developer": "3 Oaks Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Moon Sisters",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets",
                        "free_money"
                    ],
                    "category": "Video Slots",
                    "code": "oak_buddha_fortune",
                    "deprecation_date": null,
                    "developer": "3 Oaks Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Buddha Fortune",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets",
                        "free_money"
                    ],
                    "category": "Video Slots",
                    "code": "oak_sun_of_egypt_2",
                    "deprecation_date": null,
                    "developer": "3 Oaks Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Sun of Egypt 2",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Table Games",
                    "code": "jil_jackpot_bingo",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Jackpot Bingo",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "gmz_bonanza_donut",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Bonanza Donut",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "pgp_coffee_wild",
                    "deprecation_date": null,
                    "developer": "Pragmatic Play",
                    "enabled": true,
                    "features": [
                        "Bonus Buy"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Coffee Wild",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "gmz_3x3_hell_spin",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "3X3 Hell Spin",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "gmz_33_27_ways",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "3х3 27 Ways",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Crash Games",
                    "code": "gmz_cup_pilot",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Cup Pilot",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "gmz_dogs_and_tails",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Dogs and Tails",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Crash Games",
                    "code": "gmz_coin_pilot",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Coin Pilot",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "gmz_won_hundred",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Won Hundred",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "gmz_buffalo_ice_hold_the_spin",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Buffalo Ice Hold The Spin",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "gmz_bonanza_donut_xmas",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Bonanza Donut Xmas",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "gmz_dice_hold_the_spin",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Dice: Hold The Spin",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "gmz_fruit_story",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Fruit Story",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "gmz_happy_rabbit_27_ways",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Happy Rabbit: 27 Ways",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "gmz_40_sweet_fruits",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "40 Sweet Fruits",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "gmz_3x3_egypt_hold_the_spin",
                    "deprecation_date": null,
                    "developer": "Gamzix",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "3X3 Egypt: Hold The Spin",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "jil_jili_caishen",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Jili Caishen",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "spn_majestic_king",
                    "deprecation_date": null,
                    "developer": "Spinomenal",
                    "enabled": true,
                    "features": [
                        "Bonus Buy"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Majestic King",
                    "release_date": "2023-05-12",
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "spn_aztec_spell",
                    "deprecation_date": null,
                    "developer": "Spinomenal",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Aztec Spell",
                    "release_date": "2023-05-10",
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "gmb_fortune_three",
                    "deprecation_date": null,
                    "developer": "GameBeat",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Fortune Three",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "gmb_fortune_five",
                    "deprecation_date": null,
                    "developer": "GameBeat",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Fortune Five",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "gmb_buffalo_dale",
                    "deprecation_date": null,
                    "developer": "GameBeat",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Buffalo Dale",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Virtual Sports",
                    "code": "gmb_witch_theasures",
                    "deprecation_date": null,
                    "developer": "GameBeat",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Witch Theasures",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "gmb_conquest_era",
                    "deprecation_date": null,
                    "developer": "GameBeat",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Conquest Era",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "gmb_xiao_treasures",
                    "deprecation_date": null,
                    "developer": "GameBeat",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Xiao Treasures",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "gmb_book_of_hor",
                    "deprecation_date": null,
                    "developer": "GameBeat",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Book Of Hor",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "gmb_dangerous_monster",
                    "deprecation_date": null,
                    "developer": "GameBeat",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Dangerous Monster",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Game Show",
                    "code": "evo_funky_time",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Funky Time",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "spn_lobby",
                    "deprecation_date": null,
                    "developer": "Spinomenal",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Lobby",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Game Show",
                    "code": "evo_football_studio_dice",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Football Studio Dice",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Unknown",
                    "code": "rbp_madame_luck",
                    "deprecation_date": null,
                    "developer": "RubyPlay",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Madame Luck",
                    "release_date": "2023-07-05",
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Unknown",
                    "code": "rbp_piggy_gold",
                    "deprecation_date": null,
                    "developer": "RubyPlay",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Piggy Gold",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Unknown",
                    "code": "rbp_vegas_hotties",
                    "deprecation_date": null,
                    "developer": "RubyPlay",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Vegas Hotties",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Unknown",
                    "code": "rbp_more_dragon_ladies",
                    "deprecation_date": null,
                    "developer": "RubyPlay",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "More Dragon Ladies",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "pgp_fat_panda",
                    "deprecation_date": null,
                    "developer": "Pragmatic Play",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Fat Panda",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "pgp_gates_of_olympus_jackpot_play",
                    "deprecation_date": null,
                    "developer": "Pragmatic Play",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Gates of Olympus Jackpot Play",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets",
                        "free_money"
                    ],
                    "category": "Video Slots",
                    "code": "oak_big_heist",
                    "deprecation_date": null,
                    "developer": "3 Oaks Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Big Heist",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "bonus_game",
                        "free_bets",
                        "free_money"
                    ],
                    "category": "Video Slots",
                    "code": "oak_eye_of_gold",
                    "deprecation_date": null,
                    "developer": "3 Oaks Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Eye Of Gold",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "btg_millionaire_megaways",
                    "deprecation_date": null,
                    "developer": "Big Time Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Millionaire Megaways",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "pgp_pub_kings",
                    "deprecation_date": null,
                    "developer": "Pragmatic Play",
                    "enabled": true,
                    "features": [
                        "Bonus Buy",
                        "In-game Freespins"
                    ],
                    "fun_mode": true,
                    "hit_ratio": "22.9",
                    "name": "Pub Kings",
                    "release_date": "2023-08-22",
                    "rtp": "96.08",
                    "themes": [],
                    "volatility": 5
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Video Slots",
                    "code": "pgp_piggy_bankers",
                    "deprecation_date": null,
                    "developer": "Pragmatic Play",
                    "enabled": true,
                    "features": [
                        "Respin",
                        "In-game Freespins",
                        "Bonus Buy"
                    ],
                    "fun_mode": true,
                    "hit_ratio": "26.9",
                    "name": "Piggy Bankers",
                    "release_date": "2023-08-22",
                    "rtp": "96.05",
                    "themes": [],
                    "volatility": 5
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "nlc_dj_psycho",
                    "deprecation_date": null,
                    "developer": "Nolimit City",
                    "enabled": true,
                    "features": [
                        "Bonus Buy",
                        "In-game Freespins"
                    ],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "DJ Psycho",
                    "release_date": "2023-08-22",
                    "rtp": "94.09",
                    "themes": [],
                    "volatility": 3
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "spn_trouts_treasure__payday",
                    "deprecation_date": null,
                    "developer": "Spinomenal",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Trout\'s Treasure - Payday",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "spn_egyptian_rebirth_ii_mummys_return",
                    "deprecation_date": null,
                    "developer": "Spinomenal",
                    "enabled": false,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Egyptian Rebirth 2: Mummy\'s Return",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "gmb_stolbik_777",
                    "deprecation_date": null,
                    "developer": "GameBeat",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Stolbik 777",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "jil_crazy777",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Crazy777",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "jil_boxing_king",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Boxing King",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "jil_party_night",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Party Night",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "jil_fortune_gems",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Fortune Gems",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "jil_agent_ace",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Agent Ace",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "jil_super_ace",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Super Ace",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "jil_golden_empire",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Golden Empire",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "jil_money_coming",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Money Coming",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "jil_romax",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "RomaX",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "jil_mega_ace",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Mega Ace",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "jil_ali_baba",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Ali Baba",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "jil_charge_buffalo",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Charge Buffalo",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "jil_pharaoh_treasure",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Pharaoh Treasure",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "jil_fengshen",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "FengShen",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "jil_golden_bank",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Golden Bank",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "jil_bubble_beauty",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Bubble Beauty",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "jil_gold_rush",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Gold Rush",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "jil_mayan_empire",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Mayan Empire",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "jil_jungle_king",
                    "deprecation_date": null,
                    "developer": "Jili",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Jungle King",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Unknown",
                    "code": "rbp_rush_fever_7s_deluxe_88",
                    "deprecation_date": null,
                    "developer": "RubyPlay",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Rush Fever 7s Deluxe 88",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [
                        "free_bets"
                    ],
                    "category": "Unknown",
                    "code": "rbp_dragon_prophecy_94",
                    "deprecation_date": null,
                    "developer": "RubyPlay",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Dragon Prophecy 94",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Live Roulette",
                    "code": "evo_red_door_roulette",
                    "deprecation_date": null,
                    "developer": "Evolution Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": false,
                    "hit_ratio": null,
                    "name": "Red Door Roulette",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                },
                {
                    "bonus_types": [],
                    "category": "Video Slots",
                    "code": "psh_razor_shark",
                    "deprecation_date": null,
                    "developer": "Push Gaming",
                    "enabled": true,
                    "features": [],
                    "fun_mode": true,
                    "hit_ratio": null,
                    "name": "Razor Shark\t",
                    "release_date": null,
                    "rtp": null,
                    "themes": [],
                    "volatility": null
                }
            ],
            "status": "ok"
        },
        "status": "ok"
    }';
    return $data;
}

