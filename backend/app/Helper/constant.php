<?php

define('ADMIN_TYPE', 'AdminUser');
define('USER_TYPE', 'User');

define('PLAYER', "player");
define('AGENT', "agent");

define('ADMIN_ROLE_ID', 1);
define('AJENT_ROLE_ID', 2);
define('COMMISSION_PERCENTAGE', 'commission_percentage');

define('SELF_PARENT_ID', 1);


define('TRANSACTION_TYPE_WITHDRAWAL', 4);
define('TRANSACTION_TYPE_DEPOSIT', 3);

define('ALLCOUNT', 100000000);

define('TRANSACTION_TYPE_ARRAY_REPORT', [0, 8]);

define('MESSAGE_DEPOSIT_CLAIM', 12);
define('MESSAGE_WITHDRAW_CLAIM', 11);

define('ERROR_MESSAGE', 'something went wrong');
define('ERROR_service_maintenance', 'Service Under the Maintenance');
define('ERROR_DENIED', 'Permission Denied');
define('PERMISSION_DENIED', 301);

define('ERROR_MESSAGE_EXCEPTION', 'This ID Use in other Place so unable to deleted this');

define('MESSAGE_CREATED', 'Created successfully');
define('MESSAGE_CREATED_FAIL', 'Created unsuccessfully.');

define('MESSAGE_RECORD_NOT_FOUND', 'Record not found');
define('MESSAGE_UPDATED', 'Updated Successfully');
define('MESSAGE_UPDATED_MINIMUM', 'Minimum one menu required');

define('MESSAGE_ALREADY_ENABLE', 'Already ENABLE');

define('MESSAGE_UPDATED_FAIL', 'Updated unsuccessfully.');
define('MESSAGE_NOTHING', 'Nothing Changes');
define('MESSAGE_GET_SUCCESS', 'record get successfully.');


define('MESSAGE_VALIDATION_FAIL', 'Validation error');

define('MESSAGE_WITHDRAW', 'Amount withdraw successfully');
define('MESSAGE_WITHDRAW_NOT_AVAILABLE', 'Amount withdraw not available');
define('MESSAGE_WITHDRAW_FAIL', 'Amount withdraw unsuccessfully.');

define('MESSAGE_DEPOSIT', 'Amount deposit successfully');
define('MESSAGE_DEPOSIT_FAIL', 'Amount deposit unsuccessfully.');

define('MESSAGE_DEACTIVE', 'deactivated successfully.');
define('MESSAGE_ACTIVE', 'Activated successfully.');
define('MESSAGE_DELETE', 'Deleted successfully.');

//define('ELASTICSEARCH_INDEX', ['users' => 'users', 'transactions' => 'transactions']);
define('EZUGI_DOMAINS', ['demo.ezugi.com', 'ezugi-main.tk']);

define('ELASTICSEARCH_INDEX', [
    'users' => 'users_development',
    'transactions' => 'transactions_development',
    'transactions_dashboard' => 'transactions_development',
    'bet_transaction' => 'bet_transaction',
]);


define('PAYOUT_FAILED_TRANSACTION', [
    106,
    107,
    108,
    109,
    111
]);


define('ELASTICSEARCH_TYPE', '_doc');


define('DATE_FORMAT', 'Y-m-d H:i:s');

define('USER_EXIST', "User Already Exist");
define('AGENT_EXIST', "Agent Already Exist");
define('SUB_ADMIN_EXIST', "Sub-Admin Already Exist");
define('PHONE_EXIST', "Phone Already Exist");
define('USER_NICK_CHECK', "Nick name can't be same as username");

define('DEPOSIT_NOTIFICATION', "Amount is deposited");
define('WITHDRAW__NOTIFICATION', "Amount is withdrawn");

define('MESSAGE_NOTIFICATION_READ', 'Notification read successfully');
define('MESSAGE_NOTIFICATION_ALREADY_READ', 'Notification already read');
define("EVENT_LIABILITY", 1000);
define("MAX_SINGLE_BET", 250);
define("MIN_BET", 5);
define("MAX_MULTIPLE_BET", 250);
define("MAX_BET_ON_EVENT", 5000);
define("DEPOSIT_LIMIT", 10000);
define("MAX_WIN_AMOUNT", 15000);
define("MAX_ODD", 3000);
define("CASHOUT_PERCENTSGE", 5);
define("BET_DISABLED", false);
define("PROMOTION", 5);

// Tenant registration form field required action type
define("NOT_VISIBLE", 0);
define("VISIBLE_WITH_OPTIONAL", 1);
define("VISIBLE_WITH_REQUIRED", 2);

define("SPORT_DISABLED_SUCCESSFULLY", "Sport Disabled successfully");
define("SPORT_ENABLED_SUCCESSFULLY", 'Sport Enabled Successfully.');
define("SPORT_ENABLED_ID", '154830,35232,6046,48242,154919,54094');

define('AGENT_NOT_ALLOWED_PERMISSIONS_MODULE', [
    'sports_book',
    'withdrawal_request',
    'tenant_banners',
    'tenant_credentials',
    'tenant_sport_setting',
    'tenant_menu_setting',
    'tenant_registration_fields',
    'tenant_payment_providers',
    'tenant_theme_settings',
    'tenant_configurations',
    'sub_admin',
    'promo_code',
    'joining_bonus',
    'deposit_bonus',
    'losing_bonus',
    'cms',
    'casino_management',
    'bank_details',
    'deposit_requests',
    'site_maintenance',
    'permission_role',
    'site_seo_management'
]);
define('SUB_ADMIN_NOT_ALLOWED_PERMISSIONS_MODULE', [
    'sub_admin',
    'site_maintenance',
    'permission_role',
    'site_seo_management'
]);

define("CASINO_TRANSACTION", 'casino_transaction');
define("BET_TRANSACTION", 'bet_transaction');
define("USER_TRANSACTION", 'user_transaction');
define("WITHDRAW_APPORVED", 'approved');
define("INDIGRIT_SERVICE", 'adminbackend-indigrit');
define("SKY_SERVICE", 'adminbackend-sky-payment');
define("TRANSACTION_FAILED", 'failed');
define("PENDING_BY_GATEWAY",'pending_by_gateway');
define("WITHDRAW_TYPE",'payment_gateway');
define("REJECTED_BY_GATEWAY",'rejected_by_gateway');
define("SUCCESS", 'success');
define("APPROVED",'approved');
define("FAILED",'failed');
define("APPROVED_BY_PAYMENT_GATEWAY",'Approved By Payment gateway');
define("ACTIVE",'active');
define("CANCELLED",'cancelled');
define("REJECTION_COMMENT", 'Rejected By payment Gateway');
define("REJECTED_BY_PAYMENT_GATEWAY",'rejected by Payment Gateway');
define("WITHDRAW_SUCCESS",'Withdraw Request updated successfully');
define("WITHDRAW_NOT_FOUND",'No Withdrawal Request Found For This Transaction ID');
define("WITHDRAW_FAILED",'Transaction failed by payment gateway');
define("WITHDRAW_ALREADY_PROCCESSED",'Withdrawal Request Already Approved For This Transaction ID');
define("TRANSACTION_NOT_FOUND",'Transaction not Found');
define("INVALID_SIGN",'Invalid signature');
define("SIGN_MATCHES", 'Signature Matched');
define("ST8_PROVIDER", 'st8');
define("SKY_WITHDRAW", 'Sky Withdraw Provider');
define("INDIGRIT_WITHDRAW",'Indigrit Withdrawal Provider');
define("PAYOUT_WITHDRAW",'Payout Withdrawal Provider');
define("PAYOUT_SERVICE", 'adminbackend-payout-service');
