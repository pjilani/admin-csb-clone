<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BetTransaction extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = "bets_transactions";
    protected $fillable = [
        'amount',
        'is_deleted',
        'journal_entry',
        'reference',
        'user_id',
        'target_currency_id',
        'conversion_rate',
        'target_wallet_id',
        'target_before_balance',
        'target_after_balance',
        'status',
        'tenant_id',
        'created_at',
        'updated_at',
        'description',
        'payment_for',
        'current_balance'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function betslip()
    {
        return $this->belongsTo(Betslip::class);
    }

}
