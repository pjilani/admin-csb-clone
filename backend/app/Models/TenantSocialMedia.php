<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TenantSocialMedia extends Model
{
    use HasFactory;

    protected $table = "tenant_social_media_settings";

    protected $fillable = ['name', 'image', 'redirect_url', 'tenant_id'];

    public $timestamps = true;
}
