<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QueueLog extends Model
{
    use HasFactory;

      /**
     * @var string
     */
    protected $table="queue_logs";

    /**
     * @var array
     */
    protected $fillable = [
        'type',
        'status',
        'ids',
    ];
}
