<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MonthWiseGgr extends Model
{
    use HasFactory;
    protected $table = "month_wise_ggr";

    protected $fillable = [
        'total_bet',
        'total_bet_eur',
        'total_win',
        'total_win_eur',
        'total_ggr',
        'total_ggr_eur',
        'currency',
        'exchange_rate',
        'tenant_id',
        'agent_id',
        'month',
        'year',
        'created_at',
        'updated_at'
    ];

    public function tenant() {
        return $this->hasOne(Tenants::class, 'id', 'tenant_id');
    }
}