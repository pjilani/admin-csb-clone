<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserToken extends Model
{
    use HasFactory;

    protected $table="user_tokens";

    protected $fillable=[
        'token',
        'user_id',
        'token_type',
        'created_at',
        'updated_at'
    ];
}
