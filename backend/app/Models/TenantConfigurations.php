<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TenantConfigurations extends Model
{
    use HasFactory;
    protected $table="tenant_configurations";

    protected $fillable = [
        'tenant_id',
        'allowed_currencies',
        'created_at',
        'updated_at'
    ];
}
