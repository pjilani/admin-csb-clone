<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConversionHistory extends Model
{
    use HasFactory;

    protected $table ="conversion_history";

    protected $fillable = [ 'currency_id', 'old_exchange_rate', 'new_exchange_rate', 'old_currency_response_id', 'new_currency_response_id' ];

    public function currency() {
        return $this->belongsTo(Currencies::class, 'currency_id', 'id');
    }
}
