<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TenantBlockedEvent extends Model
{
    use HasFactory;
    protected $table="tenant_blocked_events";

    protected $fillable = ['tenant_id', 'admin_user_id', 'pulls_event_fixture_id'];

}

