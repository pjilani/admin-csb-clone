<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;

class Super extends Authenticatable
{
    use HasFactory,HasApiTokens,HasFactory, Notifiable;
    /**
     * @var string
     */
    protected $table="super_admin_users";
    protected $tableAdminRoles="super_admin_users_super_roles";

    /**
     * @var array
     */
    // protected $guarded = ['id'];

    /**
     * @return mixed|string
     */
    public function getAuthPassword()
    {
        return $this->encrypted_password;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'encrypted_password',
        'parent_type',
        'parent_id',
        'remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'encrypted_password',
        'remember_token',
    ];


    public static function getList($limit = 10, $offset = 0, $filter = array(),$id = 0)
    {
        $that = new self();

        $whereStr = '';

        if ($offset == 1) {
            $offset = 0;
        }

        $sqlSelectEmail ="s.*,role.super_role_id, INITCAP( (SELECT ARRAY_TO_STRING(ARRAY_AGG(name), ',')FROM super_roles where id in
          ( SELECT super_role_id FROM {$that->tableAdminRoles} WHERE super_admin_user_id  = s.id )
          )) as role_name";

        $record = DB::table($that->table, 's')
                    ->select(DB::raw($sqlSelectEmail))
                    ->leftJoin("{$that->tableAdminRoles} as role", function($join) {
                      $join->on('role.super_admin_user_id', '=', 's.id');
                    });

        if (@$filter['name'] != '') {
            $whereStr = " s.first_name like '%".$filter['name']."%' or s.last_name like '%".$filter['name']."%'  or s.email like '%".$filter['name']."%' ";
            $record->WhereRaw($whereStr);
        }
        

        if (@$filter['role'] != '') {
          $record->where('role.super_role_id',$filter['role']);
        }
        
        
        if($id != 0){
          $record->where('s.id',$id);
        }else{
          $record->where('s.parent_type',"Manager");
          $record->whereNotNull('s.parent_id');
        }

        $count = $record->count();
        $result = $record->orderByDesc('id')->forPage($offset, $limit)->get();
        // dd($result);
        return ['data' => $result, 'count' => @$count];
    }

}
