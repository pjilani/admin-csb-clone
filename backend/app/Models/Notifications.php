<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    use HasFactory;
    protected $table = "notifications";

    protected $fillable = [
        'sender_type',
        'sender_id',
        'reference_type',
        'reference_id',
        'message',
        'created_at',
        'updated_at',

    ];

}
