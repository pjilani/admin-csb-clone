<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FaqCategory extends Model
{
    use HasFactory;

    protected $table="faq_categories";

    protected $fillable=[
        'name',
        'image',
        'slug',
        'active',
        'tenant_id',
        'created_by',
        'created_at',
        'updated_at'
    ];
}