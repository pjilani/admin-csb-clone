<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BulkReindex extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = "reindex_offset";
    protected $fillable = [
        'offset',
        'status',
        'created_at',
        'updated_at',
        'type'
    ];

   

}
