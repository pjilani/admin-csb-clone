<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class TenantBlockedSports extends Model
{
    use HasFactory;

    protected $table="tenant_blocked_sports";
    protected $tableSports = 'pulls_sports';

    protected $fillable = [
        'tenant_id',
        'admin_user_id',
        'pulls_sport_id',
        'created_at',
        'updated_at',
        'is_deleted'
        
    ];

    public static function getStatus($tenant_id,$pulls_sport_id)
    {
        $that=new self();
        $sportsData = DB::table($that->table)
        ->select('id','is_deleted')
        ->where('tenant_id', $tenant_id)
        ->where('pulls_sport_id' , $pulls_sport_id)
        ->get();
        return($sportsData);
    }
  

}