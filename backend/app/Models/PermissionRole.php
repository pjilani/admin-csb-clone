<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PermissionRole extends Model
{
    use HasFactory;

    protected $table="permission_role";

    protected $fillable=[
        'role_name',
        'permission',
        'tenant_id',
        'created_by',
        'updated_by',
        'created_by_type',
        'role_type'
    ];
}