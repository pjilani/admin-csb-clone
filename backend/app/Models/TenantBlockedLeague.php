<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TenantBlockedLeague extends Model
{
    use HasFactory;
    protected $table="tenant_blocked_leagues";

    protected $fillable = ['tenant_id', 'admin_user_id', 'pulls_league_id'];

}

