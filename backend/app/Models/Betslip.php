<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Betslip extends Model
{
    use HasFactory;

    protected $table="bets_betslip";

    protected $fillable = [];

    public function bet() {
        return $this->hasOne(Bet::class, 'betslip_id');
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function bets() {
        return $this->hasMany(Bet::class, 'betslip_id');
    }

    public $timestamps = false;

}
