<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserBankDeatils extends Model
{
    use HasFactory;

    protected $table ="user_bank_details";

    protected $fillable = ['bank_name','bank_ifsc_code','account_number','name', 'phone_number', 'status', 'phone_code', 'user_id', 'tenant_id'];
}
