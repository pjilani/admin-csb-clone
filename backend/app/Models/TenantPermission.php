<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TenantPermission extends Model
{
    use HasFactory;
    protected $table="tenant_permissions";

    protected $fillable = [
        'tenant_id',
        'permission',
        'created_at',
        'updated_at'
    ];
}
