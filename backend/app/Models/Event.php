<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Event extends Model
{
    use HasFactory;

    protected $table="pulls_events";

    protected $fillable = [
        'is_deleted',
        'fixture_id',
        'fixture_status',
        'start_date',
        'last_update',
        'league_id', 
        'location_id',
        'sport_id', 
        'livescore',
        'market',
        'name_en',
        'league_name_de',
        'league_name_en',
        'league_name_fr',
        'league_name_ru',
        'league_name_tr',

        'location_name_de',
        'location_name_en',
        'location_name_fr',
        'location_name_ru',
        'location_name_tr',

        'sport_name_de',
        'sport_name_en',
        'sport_name_fr',
        'sport_name_ru',
        'sport_name_tr',
        'tenant_id',
        'is_event_blacklisted',
        'league_name_nl',
        'location_name_nl',
        'sport_name_nl'
    ];

    public function blocked() {
        return $this->hasOne(TenantBlockedEvent::class, 'pulls_event_fixture_id', 'fixture_id'); //->where('tenant_id', Auth::User()->tenant_id);
    }

    public function country() {
        return $this->hasOne('App\Models\Location', 'id', 'location_id');
    }

    public function eventParticapnt() {
        return $this->hasOne(EventParticipant::class);
    }

    public function league() {
        return $this->belongsTo(League::class);
    }

    public function sport() {
        return $this->belongsTo(Sport::class);
    }

    public $timestamps = true;

    // protected $hidden = [
    //     'market'
    // ];
}
