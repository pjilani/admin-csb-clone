<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TenantPromoCode extends Model
{
    use HasFactory;
    protected $table="tenant_promo_codes";

    protected $fillable = [
        'tenant_id','promo_name','code','promo_code_used_in', 'wallet_type', 'description','valid_from','valid_till','promo_code_bonus_type','bonus_amount','status','total_usage','total_number_of_bonus_till_now','created_at','updated_at'
    ];

    static function getList($tenant_id,$id = 0){
      $data = TenantPromoCode::where('tenant_id',$tenant_id);
      if($id != 0)
      $data = $data->where('id',$id);

      $data = $data->orderBy('id','DESC')
                   ->get();
      return $data;
    }
    
    static function getDetails($limit, $offset,$tenant_id,$id){
      if ($offset == 1) {
        $offset = 0;
      }
      $data = TenantPromoCode::where('tenant_id',$tenant_id)->where('id',$id)->first();

      // Get User list details 
      $records = UserPromoCodeBonus::where('user_promo_code_bonus.promo_code_id',$id)->where('user_promo_code_bonus.kind','promo_code')->select('user_promo_code_bonus.*','u.user_name')->leftJoin('users as u','u.id','=','user_promo_code_bonus.user_id')->orderBy('user_promo_code_bonus.id','DESC');
      $data['users_count'] = $records->count(); 
      $data['users'] = $records->forPage($offset, $limit)->get(); 

      return $data;
    }
   
}
