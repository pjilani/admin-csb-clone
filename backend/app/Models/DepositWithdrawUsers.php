<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DepositWithdrawUsers extends Model
{
    use HasFactory;

      /**
     * @var string
     */
    protected $table="deposit_withdraw_users";

    /**
     * @var array
     */
    protected $fillable = [
        'job_id',
        'user_id',
        'status',
        'error',
        'amount',
        'created_at',
        'updated_at'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
