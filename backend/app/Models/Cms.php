<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cms extends Model
{
    use HasFactory;
    protected $table="cms_pages";

    protected $fillable = [
        'tenant_id','admin_user_id','title','slug','content','active','enable_cms_for_register','created_at','updated_at'
    ];

    static function getList($tenant_id,$admin_user_id,$id = 0){
      $data = Cms::where('tenant_id',$tenant_id)
                  ->where("admin_user_id",$admin_user_id);
      if($id != 0)
      $data = $data->where('id',$id);

      $data = $data->orderBy('id','DESC')
                   ->get();
      return $data;
    }
}
