<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PageBanners extends Model
{
    use HasFactory;

    protected $table="page_banners";

    protected $fillable=[
        'name',
        'image_url',
        'redirect_url',
        'banner_type',
        'order',
        'enabled',
        'tenant_id',
        'created_at',
        'updated_at',
        'game_name',
        'open_table',
        'provider_name'
    ];
}
