<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DepositRequest extends Model
{
    use HasFactory;

    protected $table ="deposit_requests";

    protected $fillable = [ 
      'id',
      'user_id',
      'order_id',
      'status',
      'ledger_id',
      'data',
      'created_at',
      'updated_at',
      'payment_provider_id',
      'utr_number',
      'transaction_receipt',
      'amount',
      'deposit_type',
      'bank_details',
      'remark',
      'tenant_id',
      'action_id',
      'action_type'
     ];

}
