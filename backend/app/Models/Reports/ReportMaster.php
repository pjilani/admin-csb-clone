<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportMaster extends Model
{
    use HasFactory;
    protected $table="report_master";

    protected $fillable = [
        'name','active','created_at','updated_at'
    ];
}
