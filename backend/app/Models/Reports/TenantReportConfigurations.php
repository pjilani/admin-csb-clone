<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TenantReportConfigurations extends Model
{
    use HasFactory;
    protected $table="tenant_report_configurations";

    protected $fillable = [
        'tenant_id','report_id','created_at','updated_at'
    ];
}
