<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PlayerReport extends Model
{
    use HasFactory;
    private $adminUsers = 'admin_users';
    private $adminRoles = 'admin_roles';
    private $adminUsersAdminRoles = 'admin_users_admin_roles';

    public static function getTenantOwners($tenantID)
    {

        $Obj = new self();

        $record = DB::table("{$Obj->adminUsers} as AU")
            ->join("{$Obj->adminUsersAdminRoles} as AUAR", 'AUAR.admin_user_id', '=', 'AU.id')
            ->join("{$Obj->adminRoles} AS AR", 'AUAR.admin_role_id', '=', 'AR.id')
            ->select('AU.agent_name', 'AU.id')
            ->where('AR.name', 'owner')
            ->where('AU.tenant_id', $tenantID)
            ->orderBy('AU.id','asc')
            ->get();

        return $record->toArray();
    }

    public static function getAdminUserHierarchy($startWithId, $tenantID)
    {
        $sql = 'WITH RECURSIVE cte_query AS
              (
                SELECT id, agent_name as first_name, parent_id, tenant_id
                FROM admin_users
                WHERE parent_id = ' . $startWithId . ' and tenant_id =' . $tenantID . '

                UNION

                SELECT e.id, e.agent_name as first_name,  e.parent_id, e.tenant_id
                FROM admin_users as e
                INNER JOIN cte_query c ON c.id = e.parent_id  and e.tenant_id =' . $tenantID . '
              )
          SELECT * FROM cte_query 
          left join admin_users_admin_roles as ar on cte_query.id = ar.admin_user_id
          where ar.admin_role_id = 2
          order by first_name ASC';
        $admin_users = DB::select($sql);

        return $admin_users;

    }

    /**
     * @description getSuperAdminUserHierarchy
     * @param string $ownerId
     * @param string $tenantId
     * @return array
     */
    public static function getSuperAdminUserHierarchy($ownerId='',$tenantId='')
    {
        $whereStr='';
        if($tenantId){
            $whereStr =" where au.tenant_id=".$tenantId;
        }
        $sql = '
          SELECT au.id, au.agent_name as first_name FROM admin_users au
          JOIN admin_users_admin_roles aur on aur.admin_user_id=au.id and admin_role_id='.AJENT_ROLE_ID.' 
          '.$whereStr ." order by first_name asc";
        $admin_users = DB::select($sql);

        return $admin_users;

    }

    /**
     * @description getSuperAdminAllUserHierarchy
     * @param $tenantId
     * @return array
     */
    public static function getSuperAdminAllUserHierarchy($tenantId)
    {
        $whereStr='';
        if($tenantId){
            $whereStr =" where au.tenant_id=".$tenantId;
        }
        $sql = '
          SELECT au.id, au.agent_name as first_name FROM admin_users au
          JOIN admin_users_admin_roles aur on aur.admin_user_id=au.id  
          '.$whereStr;
        $admin_users = DB::select($sql);

        return $admin_users;

    }

    /**
     * @description getAdminUserHierarchyForOwner
     * @param $user_id
     * @param $tenantID
     * @return array
     */
    public static function getAdminUserHierarchyForOwner($user_id, $tenantID)
    {
        $sql = '
          SELECT id, admin_users.agent_name as first_name
          FROM admin_users WHERE tenant_id =' . $tenantID. " and id != $user_id";
        $admin_users = DB::select($sql);

        return $admin_users;

    }

}
