<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Themes extends Model
{
    use HasFactory;


    protected $fillable = [
        'name',
        'options',
        'created_at',
        'updated_at',
    ];
}
