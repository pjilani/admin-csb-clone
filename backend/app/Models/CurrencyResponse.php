<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CurrencyResponse extends Model
{
    use HasFactory;

    protected $table ="currency_response";

    protected $fillable = [ 'response' ];
}
