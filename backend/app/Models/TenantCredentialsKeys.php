<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TenantCredentialsKeys extends Model
{
    use HasFactory;
    protected $table="tenant_credentials_keys";

    protected $fillable = [
        'name',
        'description',
        'created_at',
        'updated_at'
    ];
}
