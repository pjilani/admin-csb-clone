<?php

namespace App\Models\Casino;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CasinoMenuItem extends Model
{
    use HasFactory;

    protected $table="menu_items";

    protected $fillable = ['page_menu_id', 'casino_item_id', 'name', 'active', 'featured', 'order', 'popular'];

    public function item() {
        return $this->hasOne('App\Models\Casino\CasinoItems', 'id', 'casino_item_id');
    }

}
