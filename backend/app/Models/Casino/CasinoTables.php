<?php

namespace App\Models\Casino;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class CasinoTables extends Model
{
    use HasFactory;

    protected $table="casino_tables";

    protected $fillable=[
        'name','created_at','updated_at','casino_provider_id','game_id', 'table_id', 'is_lobby'
    ];

    /**
     * @param $limit
     * @param $offset
     * @param $filter
     * @return array
     */
    public static function getList($limit,$offset,$filter,$search){
        $that=new self();

        $whereStr='';

        if($offset==1){
            $offset=0;
        }

        $record = DB::table($that->table,'p')
            ->select('p.*','casino_providers.name as casino_providers_name','casino_games.name as game_name')
            ->leftJoin('casino_games','p.game_id','=','casino_games.game_id')
            ->leftJoin('casino_providers','casino_games.casino_provider_id','=','casino_providers.id');


        if(@$filter['game_id']!=''){
            $whereStr ="p.game_id = '".$filter['game_id']."'";
            $record->WhereRaw($whereStr);
        }

        if(@$search!=''){
          $record->where('p.name','ilike', '%' . $search . '%');
        }

        $count=$record->count();
        $result=$record->orderByDesc('p.id')->forPage($offset,$limit)->get();

        return ['data'=>$result,'count'=>@$count];
    }

    public static function getDetailsById($id){
        $that=new self();
        $record = DB::table($that->table,'p')
            ->select('p.*','casino_providers.id as casino_providers_id','casino_providers.name as casino_providers_name','casino_games.name as game_name')
            ->leftJoin('casino_games','p.game_id','=','casino_games.game_id')
            ->leftJoin('casino_providers','casino_games.casino_provider_id','=','casino_providers.id')
            ->Where('p.id',$id);

        //$count=$record->count();
        $result=$record->get();

        return @$result[0];
    }
}
