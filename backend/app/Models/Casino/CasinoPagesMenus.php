<?php

namespace App\Models\Casino;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CasinoPagesMenus extends Model
{
    use HasFactory;

    protected $table="page_menus";

    protected $fillable = ['name', 'casino_menu_id', 'page_id', 'menu_order'];
    
    public function menu() {
        return $this->hasOne('App\Models\Casino\CasinoMenus', 'id', 'casino_menu_id');
    }

    public function items() {
        return $this->hasMany('App\Models\Casino\CasinoMenuItem', 'page_menu_id', 'id')->where('active',true);
    }

}
