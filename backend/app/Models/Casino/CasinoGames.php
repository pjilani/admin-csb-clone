<?php

namespace App\Models\Casino;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class CasinoGames extends Model
{
    use HasFactory;

    protected $table="casino_games";

    protected $fillable=[
        'name','created_at','updated_at','casino_provider_id','game_id'
    ];


    /**
     * @param $limit
     * @param $offset
     * @param $filter
     * @return array
     */
    public static function getList($limit,$offset,$filter,$search){
        $that=new self();

        $whereStr='';

        if($offset==1){
            $offset=0;
        }

        $record = DB::table($that->table,'p')
            ->select('p.*','casino_providers.name as casino_providers_name')
            ->leftJoin('casino_providers','p.casino_provider_id','=','casino_providers.id');


        if(@$filter['provider_id']!=''){
            $whereStr =" p.casino_provider_id = '".$filter['provider_id']."'";
            $record->WhereRaw($whereStr);
        }

        if(@$search!=''){
          $record->where('p.name','ilike', '%' . $search . '%');
        }

        $count=$record->count();
        $result=$record->orderByDesc('id')->forPage($offset,$limit)->get();

        return ['data'=>$result,'count'=>@$count];
    }

    /**
     * @description getGameProviderDetailsByGameId
     * @param $gameId
     * @return \Illuminate\Support\Collection
     */
    public static function getGameProviderDetailsByGameId($gameId,$providerId){
        if(!$gameId){
            return $gameId;
        }
        $that=new self();
        $record = DB::connection('read_db')->table($that->table,'p')
            ->select('p.*','casino_providers.name as casino_providers_name')
            ->leftJoin('casino_providers','p.casino_provider_id','=','casino_providers.id');
        if($gameId!=''){
            $record->Where('p.game_id','=',$gameId);
        }
        if($providerId!=''){
            $record->Where('p.casino_provider_id','=',$providerId);
        }
        $result=$record->orderByDesc('id')->get()->first();
        return $result;
    }
}
