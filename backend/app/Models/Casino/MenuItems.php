<?php

namespace App\Models\Casino;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MenuItems extends Model
{
    use HasFactory;
    protected $table='menu_items';
    protected $fillable=[
        'id',
        'page_menu_id',
        'casino_item_id',
        'name',
        'order',
        'active',
        'featured'.
        'created_at',
        'updated_at'
    ];

    public function items() {
        return $this->hasOne('App\Models\Casino\CasinoItems', 'id', 'casino_item_id');
    }
}
