<?php

namespace App\Models\Casino;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CasinoProviders extends Model
{
    use HasFactory;

    protected $table="casino_providers";

    protected $fillable=[
        'name','created_at','updated_at'
    ];
}
