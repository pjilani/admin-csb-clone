<?php

namespace App\Models\Casino;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CasinoMenus extends Model
{
    use HasFactory;

    protected $table="casino_menus";

    protected $fillable=[
        'name','created_at','updated_at','casino_provider_id','enabled','menu_type','menu_order','tenant_id','image_url'
    ];
}
