<?php

namespace App\Models\Casino;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CasinoItems extends Model
{
    use HasFactory;

    protected $table="casino_items";

    protected $fillable=[
        'uuid',//table_id from casino_tables
        'name',
        'image',
        'item_type',
        'item_order',
        'provider',
        'technology',
        'has_lobby',
        'is_mobile',
        'has_free_spins',
        'active',
        'featured',
        'tenant_id',
        'created_at','updated_at'
    ];
}
