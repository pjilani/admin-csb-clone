<?php

namespace App\Models\Casino;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class CasinoPages extends Model
{
    use HasFactory;

    protected $table="pages";

    protected $fillable=[
        'title','created_at','image','updated_at','enabled','order','tenant_id','top_menu_id','enable_instant_game'
    ];

}
