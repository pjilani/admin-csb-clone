<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminUserSetting extends Model
{
    use HasFactory;
    protected $table="admin_user_settings";

    protected $fillable = [
        'key','value','admin_user_id','created_at','updated_at'
    ];
}
