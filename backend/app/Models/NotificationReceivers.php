<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotificationReceivers extends Model
{
    use HasFactory;

    protected $table="notification_receivers";

    protected $fillable=[
        'receiver_type',
        'receiver_id',
        'notification_id',
        'is_read',
        'created_at',
        'updated_at'
    ];

}
