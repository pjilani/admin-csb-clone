<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPromoCodeBonus extends Model
{
    use HasFactory;

    protected $table ="user_promo_code_bonus";

    protected $fillable = [ 'promo_code_id', 'status', 'transaction_id' ,'bonus_amount', 'updated_at'];

    
}
