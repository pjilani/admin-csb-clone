<?php

namespace App\Models\Menu;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MenuTenantSettingModel extends Model
{
    use HasFactory;

    protected $table="menu_tenant_setting";

    protected $fillable = ['name','menu_id','tenant_id','ordering','created_at','updated_at'];
}
