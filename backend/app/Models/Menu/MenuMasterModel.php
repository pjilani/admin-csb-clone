<?php

namespace App\Models\Menu;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MenuMasterModel extends Model
{
    use HasFactory;
    protected $table="menu_master";

    protected $fillable = ['name','path','active','created_at','updated_at'];
}
