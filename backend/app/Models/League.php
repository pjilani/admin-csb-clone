<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class League extends Model
{
    use HasFactory;

    protected $table="pulls_leagues";

    protected $fillable = ['is_deleted', 'league_id', 'season', 'location_id', 'sport_id', 'name_de', 'name_en', 'name_fr', 'name_ru', 'name_tr', 'name_nl', 
    'created', 'modified','tenant_id'];

    public function blocked() {
        return $this->hasOne(TenantBlockedLeague::class, 'pulls_league_id');//->where('tenant_id', Auth::User()->tenant_id);
    }

    public function country() {
        return $this->hasOne('App\Models\Location', 'id', 'location_id');
    }

    public function sport() {
        return $this->hasOne('App\Models\Sport', 'id', 'sport_id');
    }

    public function events() {
        return $this->hasMany(Event::class);
    }

    public $timestamps = true;

}
