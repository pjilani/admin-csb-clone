<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserLoginHistory extends Model
{
    use HasFactory;

    protected $table="user_login_history";

    protected $fillable=[
        'tenant_id',
        'value',
        'ip',
        'network',
        'user_id',
        'version',
        'updated_at',
        'device_id',
        'device_type',
        'device_model',
        'data',
        'sign_in_count',
        'last_login_date',
        'created_at',	
        'updated_at'
    ];
}

