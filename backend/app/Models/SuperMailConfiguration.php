<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;

class SuperMailConfiguration extends Authenticatable
{
    use HasFactory,HasApiTokens,HasFactory, Notifiable;
    /**
     * @var string
     */
    protected $table="super_admin_mail_configuration";

    
    protected $fillable = [
        'display_key',
        'key_provided_by_account',
        'value',
        'parent_type',
        'last_updated_parent_id',
        'created_at',
        'updated_at'
    ];

    


    public static function getMailConfiguration()
    {
        $that = new self();
        $record = DB::table($that->table, 'm')->orderBy('id','ASC')->get();
        return ['data' => $record];
    }
    
    public static function updateMailConfiguration($data)
    {
        $that = new self();
        $record = false;
        foreach ($data['phaseExecutions']['PRE'] as $key => $value) {
          $record = DB::table($that->table)->where('key_provided_by_account',array_keys($value)[0])->update(['value'=>$value[array_keys($value)[0]]]);
        }
        return ['data' => $record];
    }

}
