<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DepositWithdrawJob extends Model
{
    use HasFactory;

     /**
     * @var string
     */
    protected $table="deposit_withdraw_job";

    /**
     * @var array
     */
    protected $fillable = [
        'type',
        'request_object',
        'status',
        'created_by',
        'tenant_id',
        'csv'
    ];

    public function users() {
        return $this->hasMany(DepositWithdrawUsers::class, 'job_id', 'id');
    }
    

}
