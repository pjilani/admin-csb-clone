<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;

class Admin extends Authenticatable
{
    use HasFactory,HasApiTokens,HasFactory, Notifiable;

    /**
     * @var string
     */
    protected $table="admin_users";
    protected $tableAdminRoles="admin_users_admin_roles";
    protected $tableAdminUserPermissionRoles = "admin_users_permission_roles";
    protected $tableWallets="wallets";
    protected $tableUsers="users";


    /**
     * @var array
     */
    protected $guarded = ['id'];

    public function getAuthPassword()
    {
        return $this->encrypted_password;
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'phone',
        'phone_verified',
        'parent_type',
        'parent_id',
        'remember_token',
        'encrypted_password',
        'reset_password_token',
        'reset_password_sent_at',
        'confirmation_token',
        'confirmed_at',
        'unconfirmed_email',
        'confirmation_sent_at',
        'created_at',
        'updated_at',
        'tenant_id',
        'agent_name',
        'active',
        'deactivated_by_id',
        'deactivated_at',
        'deactivated_by_type',
        'kyc_regulated',
        'login_count',
        'affiliate_token',
        'ip_whitelist',
        'ip_whitelist_type',
        'is_applied_ip_whitelist',
        'timezone',
        'agent_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'encrypted_password',
        'remember_token',
    ];

    /**
     * @param $limit
     * @param $offset
     * @param $filter
     * @return array
     */
    public static function getList($limit,$offset,$filter){
        $that=new self();

        $whereStr='';

        if($offset==1){
            $offset=0;
        }
//        DB::enableQueryLog();
        $record = DB::table($that->table,'p')
            ->select(['id','first_name','last_name','email','phone','phone_verified','parent_id',
                'tenant_id','agent_name','active','deactivated_by_id','deactivated_at','deactivated_by_type',
                'created_at','updated_at','confirmed_at','confirmation_sent_at','unconfirmed_email']);

        if(@$filter['email']!=''){
            $whereStr =" p.email like '%".$filter['email']."%'";
            $record->WhereRaw($whereStr);
        }
        if(@$filter['first_name']!=''){
            $whereStr =" p.first_name like '%".$filter['first_name']."%'";
            $record->WhereRaw($whereStr);
        }
        if(@$filter['last_name']!=''){
            $whereStr =" p.last_name like '%".$filter['last_name']."%'";
            $record->WhereRaw($whereStr);
        }
        if(@$filter['phone']!=''){
            $whereStr =" p.phone like '%".$filter['phone']."%'";
            $record->WhereRaw($whereStr);
        }
        if(@$filter['active']!=''){
            $whereStr =" p.active = '".$filter['active']."'";
            $record->WhereRaw($whereStr);
        }
        if(@$filter['agent_name']!=''){
            $whereStr =" p.agent_name like '%".$filter['agent_name']."%'";
            $record->WhereRaw($whereStr);
        }
        if(@$filter['tenant_id']!=''){
            $whereStr =" p.tenant_id = '".$filter['tenant_id']."'";
            $record->WhereRaw($whereStr);
        }
       if(@$filter['parent_id']!=''){
            $whereStr =" p.parent_id = '".$filter['parent_id']."'";
            $record->WhereRaw($whereStr);
        }

        $count=$record->count();
        $result=$record->orderByDesc('id')->forPage($offset,$limit)->get();

//               $quries = DB::getQueryLog();


//        du($quries);
        return ['data'=>$result,'count'=>@$count];
    }


    /**
     * @param $tenant_id
     * @param $adminId
     * @return array
     */
    public static function getAdminsAgentsById($tenant_id, $adminId, $page, $limit, $filter, $isAgent)
    {
        $that = new self();
        $offset = ($page > 1) ? $page * $limit : 0;
        if ($page > 1) {
            $offset = $limit * ($page - 1);
        }
        $whereStrParentSQL = '';
        $whereStrParentSQCountArray = [];
        $whereStrParentSQCountArray['tenant_id'] = $searchParam['tenant_id'] = $tenant_id;
        $searchParam['limit'] = $limit;
        $searchParam['page'] = $offset;

        if ($adminId != 0) {
            $Hierarchy = getAdminHierarchy($adminId, false);
            $ids = flatten($Hierarchy);
            unset($searchParam['adminId']);
            $roles = getRolesDetails($adminId);
            if (in_array('owner', $roles)) {
                $whereStrParentSQL = "and ( u.parent_id =" . $adminId . ")";
            } else {
                if ($ids)
                    $whereStrParentSQL = " and (u.id in (" . implode(',', $ids) . "))";
                else
                    $whereStrParentSQL = "and  u.parent_id =" . $adminId;
            }
        }
        $whereStr = '';
        if (@$filter['search'] != '') {
            $filter['search'] = str_replace("'", "", $filter['search']);
            $whereStr = " and (u.email ilike '%" . $filter['search'] . "%' or u.agent_name ilike '%" . $filter['search'] . "%' or u.first_name ilike '%" . $filter['search'] . "%' or u.last_name ilike '%" . $filter['search'] . "%')";
        }
        if(@$filter['login_id']){
            $whereStr .= " and u.id <>" . (int)$filter['login_id'];
        }
        if(@$filter['agent_type']!== null){
            $whereStr .= " and u.agent_type =" . (int)$filter['agent_type'];
        }
        $joinAgentRole = 'LEFT JOIN admin_users_admin_roles on admin_user_id  = u.id ';
        if ($isAgent) {
            $whereAgentRole = ' AND admin_role_id=2'; //agent role
        } else {
            $whereAgentRole = ' AND admin_role_id in (3,4,5)'; //sub admin roles
        }

        $orderByStr = "ORDER BY " . $filter['sort_by']. " " . $filter['order'];
        $sqlCheckV = "SELECT
                        u.id,u.first_name,u.last_name,u.email,u.agent_type ,u.kyc_regulated ,u.phone,u.phone_verified,u.parent_id,
                        u.tenant_id,u.agent_name,u.active,u.deactivated_by_id,u.deactivated_at,u.deactivated_by_type,
                        u.created_at,u.updated_at,u.confirmed_at,u.confirmation_sent_at,u.unconfirmed_email,
                        INITCAP( (SELECT ARRAY_TO_STRING(ARRAY_AGG(name), ',')FROM admin_roles where id in
                         ( SELECT admin_role_id FROM {$that->tableAdminRoles} WHERE admin_user_id  = u.id )
                         )) as roles
                        FROM {$that->table} as u $joinAgentRole
                        where u.tenant_id = :tenant_id

                         $whereStrParentSQL  $whereStr $whereAgentRole
                         $orderByStr
                            limit :limit offset :page
                        ";

        $users = DB::select($sqlCheckV, $searchParam);
        $sqlCheckV = "SELECT
                        count(u.id)
                        FROM {$that->table} as u $joinAgentRole
                        where  u.tenant_id = :tenant_id  $whereStrParentSQL $whereStr  $whereAgentRole

                        ";

        $countTotleRecord = DB::select($sqlCheckV, $whereStrParentSQCountArray);
        $sqlGetParent = "SELECT id
                        FROM admin_users where tenant_id = {$tenant_id} order by id asc
                        LIMIT 1 ";
        $getParentId = DB::select($sqlGetParent);
        return ['data' => $users, 'count' => @(int)$countTotleRecord[0]->count, "parent_id" => @$getParentId[0]->id];
    }

    /**
     * @param $id
     * @return array
     */
    public static function getAdminUserById($id){

        $that=new self();
        $sqlCheckV="SELECT u.* , w.id as wallet_id,w.updated_at as wallet_updated_at,w.currency_id,
                    w.amount,w.non_cash_amount,w.amount as last_deposited_amount,(SELECT email FROM {$that->table} where id=u.parent_id ) as parent_email,
                    (SELECT ARRAY_TO_STRING(ARRAY_AGG(admin_role_id), ',') FROM {$that->tableAdminRoles} WHERE admin_user_id  = ? )as rolesids,
                    (SELECT ARRAY_TO_STRING(ARRAY_AGG(permission_role_id), ',') FROM {$that->tableAdminUserPermissionRoles} WHERE admin_user_id  = ? )as permissionRoleId
                    FROM {$that->table} as u
                    left outer join {$that->tableWallets} as w on u.id=w.owner_id where owner_type ='AdminUser' and u.id = ?
                    ";

        $return= DB::select($sqlCheckV,[$id,$id,$id]);
        if(@$return[0]->encrypted_password){
            unset($return[0]->encrypted_password);
        }

        if(Auth::user()->parent_type=='AdminUser' && Auth::user()->tenant_id) {
            @$return[0]->setting = DB::table('admin_user_settings')->where('admin_user_id',$id)->get();
        }

//        @$return[0]->rolesids = DB::select("(SELECT admin_role_id as rolesids FROM {$that->tableAdminRoles} WHERE admin_user_id  = $id )");
        @$return[0]->roles = getRolesDetails( $id );

        @$return[0]->permissionRoleName = DB::select('select pr.role_name from admin_users_permission_roles aupr
        join permission_role pr on pr.id = aupr.permission_role_id 
        where aupr.admin_user_id = ?',[$id])[0]->role_name;

        return $return;
    }

    /**
     * @description getAdminCurrencyName
     * @param $id
     * @return array
     */
    public static function getAdminCurrencyName($id){

        $sql="
              SELECT ARRAY_TO_STRING(ARRAY_AGG(c.code), ',') as name
              FROM currencies as c where c.id in (
                      select w.currency_id
                      from wallets as w
                      where w.owner_id=?
                      and w.owner_type='AdminUser'
            )";

        return DB::select($sql,[$id]);

    }

    /**
     * @description getAdminWalletsDetails
     * @param $id
     * @return array
     */
    public static function getAdminWalletsDetails($id){

        $sql="select w.*,c.code,c.name
              from wallets as w
              left JOIN currencies as c on c.id = w.currency_id
              where (w.owner_type = 'AdminUser' and w.owner_id = ?)
            ";

        return DB::select($sql,[$id]);

    }
   
   
    public static function getSuperAdminWalletsDetails($id){

      $data = DB::table('wallets as w')
              ->selectRaw('w.*,c.code,c.name')
              ->leftJoin('currencies as c','c.id','=','w.currency_id')
              ->where('w.owner_type','SuperAdminUser')
              ->where('w.owner_id',$id)
              ->first();

      return $data;

    }


    /**
     * @param $tenant_id
     * @param $adminId
     * @param $page
     * @param $limit
     * @param $filter
     * @return array
     */
    public static function getAdminsAjentsByPlayers($tenant_id,$adminId,$page,$limit,$filter){
        $that=new self();
        $whereStr='';
        // $offset = ($page > 1) ? $page * $limit : 0;
        if ($limit != 'all' && $page != 'all') {
            $offset = $limit * ($page - 1);
        }

        // dd($offset);
        if($adminId!=0) {
            $isTopLevel=true;
            $loginAgentRole = getRoleByAdminId($adminId);
            if($loginAgentRole && ($loginAgentRole->admin_role_id==4||$loginAgentRole->admin_role_id==5)){
                $adminId = getParentAgent($adminId);
            }
            if(!in_array($loginAgentRole->admin_role_id,[1,3])){
                $whereStr = "and parent_type ='AdminUser' and u.parent_id =" . $adminId ;
            }
        }

        $searchParam=[$tenant_id];

        if(@$filter['search']!=''){
            $filter['search']=str_replace("'", "", $filter['search']);
            $whereStr .=" and (u.email ilike '%".$filter['search']."%' or u.user_name ilike '%".$filter['search']."%' or u.first_name ilike '%".$filter['search']."%' or u.last_name ilike '%".$filter['search']."%' or u.phone ilike '%".$filter['search']."%' or u.nick_name ilike '%".$filter['search']."%' or u.zip_code like '%".$filter['search']."%' or u.city ilike '%".$filter['search']."%' )";
        }

        if(@$filter['status']){
            $whereStr .=" and u.active= {$filter['status']}";
        }
        if(@$filter['sort']==''){
            @$filter['sort']='DESC';
        }else{
            $filter['sort']='ASC';
        }

        if(@$filter['phone_verified']){
            $whereStr .=" and u.phone_verified= {$filter['phone_verified']}";
        }
        if(@$filter['email_verified']){
            $whereStr .=" and u.email_verified= {$filter['email_verified']}";
        }
        if(@$filter['kyc_done']){
            $whereStr .=" and u.kyc_done= {$filter['kyc_done']}";
        }

        if(@$filter['min_amount']){
            $whereStr .=" and (w.amount + w.non_cash_amount) >= {$filter['min_amount']}";
        }

        if(@$filter['max_amount']){
            $whereStr .=" and (w.amount + w.non_cash_amount) <= {$filter['max_amount']}";
        }

        if (@$filter['date_range'] != '' && count($filter['date_range'])) {
            $start_date = date('Y-m-d H:i:s', strtotime($filter['date_range']['start_date']));
            $end_date = date('Y-m-d H:i:s', strtotime($filter['date_range']['end_date']));
        
            $whereStr .= " and u.created_at BETWEEN '$start_date' AND '$end_date'";
        }
        
        if(@$filter['vip_levels'] != ''){
            $whereStr .=" and vip_level IN ({$filter['vip_levels']})";
        }

        $sqlCheckV="SELECT
                        u.id,u.first_name,u.last_name, u.kyc_done, u.email,u.user_name,u.nick_name,u.active,u.demo,u.created_at,u.last_login_date,u.phone_code,u.phone,u.zip_code,u.city,u.phone_verified,u.parent_id,
                        w.currency_id,(SELECT code FROM currencies where id=w.currency_id) as currency_code,
                         (SELECT agent_name FROM admin_users WHERE id = u.parent_id AND parent_type = 'AdminUser') as agentname,
                         (w.amount + w.non_cash_amount) as total_amount
                        FROM {$that->tableUsers} as u
                        left join {$that->tableWallets} as w on u.id=w.owner_id and w.owner_type='User'
                    
                        where u.tenant_id = ? $whereStr";
        if($limit != 'all' && $page != 'all'){
            $sqlCheckV .= " ORDER BY {$filter['sort_by']} {$filter['order']} LIMIT ? OFFSET ?";
            $searchParam[] = (int)$limit;
            $searchParam[] = (int)$offset;
        }
        else{
            $sqlCheckV .= " ORDER BY {$filter['sort_by']} {$filter['order']}";
        }
        $users = DB::connection('read_db')->select($sqlCheckV,$searchParam);

        $sqlCheckV="SELECT
                        count(u.id)
                        FROM {$that->tableUsers} as u
                        left join {$that->tableWallets} as w on u.id=w.owner_id and w.owner_type='User'
            
                        where u.tenant_id = $tenant_id $whereStr
                        ";
        $countTotleRecord = DB::connection('read_db')->select($sqlCheckV);

        return ['data'=>$users,'count'=>@(int)$countTotleRecord[0]->count];
    }


    /**
     * @param $user_id
     * @return array
     */
    public static function getByPlayersID($user_id){

        $that=new self();
        $where = "and u.id = ? ";
        if(Auth::user()->parent_type=='SuperAdminUser' || Auth::user()->parent_type=='Manager') {
            $searchParam = [$user_id];
        }else{
            $searchParam = [$user_id, Auth::user()->tenant_id];
            $where .= " and u.tenant_id = ? ";
        }

        $sqlCheckV="SELECT
                        u.id,u.first_name,u.last_name,u.email,u.kyc_done,u.user_name,u.nick_name,u.active,u.phone,u.phone_verified,TO_CHAR(u.date_of_birth :: DATE, 'yyyy-mm-dd')as date_of_birth,
                        u.gender,u.locale,u.locale,u.sign_in_count,u.sign_in_ip,u.parent_type,u.parent_id,u.created_at,u.demo,u.zip_code,
                        u.updated_at,u.country_code,u.tenant_id,u.active,u.last_login_date,u.self_exclusion,u.deleted_at,
                        u.vip_level,u.disabled_at,u.disabled_by_type,u.phone_code,u.email_verified,u.city,
                        (SELECT email FROM admin_users where id=u.parent_id) as parent_email,
                        w.amount,w.id as wallet_id,w.updated_at as wallet_updated_at,w.currency_id,c.code as currency_code,w.amount as last_deposited_amount,w.non_cash_amount
                        FROM {$that->tableUsers} as u
                        left join {$that->tableWallets} as w on u.id=w.owner_id and w.owner_type='User'
                        left join currencies as c on c.id=w.currency_id
                        where 1=1 $where
                         order by u.id desc
                        ";

        $return = DB::connection('read_db')->select($sqlCheckV,$searchParam);



        if(@$return[0]){
//            if(Auth::user()->parent_type=='AdminUser' && Auth::user()->tenant_id) {
                @$return[0]->setting = DB::connection('read_db')->table('user_settings')->where('user_id',$user_id)->get();

                @$return[0]->user_documents = DB::connection('read_db')->table('user_documents')->where('user_id',$user_id)->get();
                @$return[0]->bonus = DB::connection('read_db')->table('user_bonus')->where('user_id',$user_id)->get();

                //maximum value for player commission percentage
                if(!(Auth::user()->parent_type=='SuperAdminUser' || Auth::user()->parent_type=='Manager')) {
                    @$return[0]->player_commission_max_percentage = DB::connection('read_db')->table('tenant_credentials')->where(['tenant_id' => Auth::user()->tenant_id, 'key' => 'PLAYER_COMMISSION_MAX_PERCENTAGE'])->value('value');
                }
//            }
            return @$return[0];
        }else{
            return [];
        }


    }

    public static function getEmails($email){

        $that=new self();
        $sqlSelectEmail ='id,email AS text';
        if(Auth::user()->parent_type =='SuperAdminUser') {
            $sqlSelectEmail ="id,(CONCAT(email,' (',(SELECT name FROM tenants where id =p.tenant_id),')')) AS text";
        }
        $sqlSelectEmail .=",INITCAP( (SELECT ARRAY_TO_STRING(ARRAY_AGG(name), ',')FROM admin_roles where id in
                            ( SELECT admin_role_id FROM {$that->tableAdminRoles} WHERE admin_user_id  = p.id )
                            )) as roles";

        $record = DB::table(DB::raw($that->table.' as p'))
            ->select(DB::raw($sqlSelectEmail));
        $record->leftJoin("{$that->tableAdminRoles} as role", function($join) {
                            $join->on('role.admin_user_id', '=', 'p.id');
                        })
                        ->where('role.admin_role_id',(Auth::user()->parent_type =='SuperAdminUser'?1:2));
        if(@$email!=''){
            $whereStr =" p.email like '%".$email."%'";
            $record->WhereRaw($whereStr);
        }

        if(Auth::user()->parent_type=='AdminUser') {
            $record->Where('p.tenant_id',Auth::user()->tenant_id);
        }


        $result=$record->get();

        return @$result;
    }

    public static function getAgentEmails($email){

        $that=new self();
        $agentUser = [];
        $agent = getAgentAdminHierarchy(Auth::user()->id,true,$email);
        foreach ($agent[Auth::user()->id]['child_id'] as $key => $value) {
          // dd($value);
              $agentUser[] = [
                "id" => $value['id'],
                "text" => $value['email']
              ];

        }

        // if(Auth::user()->parent_type=='AdminUser') {
        //     $record->Where('p.tenant_id',Auth::user()->tenant_id);
        // }


        // $result=$record->get();

        return @$agentUser;
    }

    public static function getListOfUser($id){

        $that=new self();
        $record['admin'] = DB::table($that->table)
            ->select('id','email')
            ->where('parent_type', '=', 'AdminUser')
            ->where('parent_id', '=', $id)
            ->get();

        $record['user'] = DB::table($that->tableUsers)
            ->select('id','email')
            ->where('parent_type', '=', 'AdminUser')
            ->where('parent_id', '=', $id)
            ->get();


        $idsAdmin= DB::table($that->table)
            ->select(DB::raw("ARRAY_TO_STRING(ARRAY_AGG(id), ',') as ids"))
            ->where('parent_type', '=', 'AdminUser')
            ->where('parent_id', '=', $id)
            ->get();

        $idsUsers= DB::table($that->tableUsers)
            ->select(DB::raw("ARRAY_TO_STRING(ARRAY_AGG(id), ',') as ids"))
            ->where('parent_type', '=', 'AdminUser')
            ->where('parent_id', '=', $id)
            ->get();

        $updateData = array('active'=>false,
            'deactivated_at' => date('Y-m-d H:i:s'),
            'deactivated_by_type' =>Auth::user()->parent_type,
            'deactivated_by_id'=> Auth::user()->id
        );

        if(@$idsAdmin[0]->ids!='')
        $updateValues['admin'] = DB::table($that->table)
            ->whereRaw("id in(".$idsAdmin[0]->ids.")")
            ->update($updateData);


        $updateDataUser = array('active'=>false,
            'disabled_at' => date('Y-m-d H:i:s'),
            'disabled_by_type' =>Auth::user()->parent_type,
            'disabled_by_id'=> Auth::user()->id
        );



        if(@$idsUsers[0]->ids!='')
        $updateValues['users'] = DB::table($that->tableUsers)
            ->whereRaw("id in(".$idsUsers[0]->ids.") ")
            ->update($updateDataUser);



        return ['data'=>$record];
    }

    public static function getListOfUserActive($id){

        $that=new self();
        $record['admin'] = DB::table($that->table)
            ->select('id','email')
            ->where('parent_type', '=', 'AdminUser')
            ->where('parent_id', '=', $id)
            ->get();

        $record['user'] = DB::table($that->tableUsers)
            ->select('id','email')
            ->where('parent_type', '=', 'AdminUser')
            ->where('parent_id', '=', $id)
            ->get();


        $idsAdmin= DB::table($that->table)
            ->select(DB::raw("ARRAY_TO_STRING(ARRAY_AGG(id), ',') as ids"))
            ->where('parent_type', '=', 'AdminUser')
            ->where('parent_id', '=', $id)
            ->get();

        $idsUsers= DB::table($that->tableUsers)
            ->select(DB::raw("ARRAY_TO_STRING(ARRAY_AGG(id), ',') as ids"))
            ->where('parent_type', '=', 'AdminUser')
            ->where('parent_id', '=', $id)
            ->get();

        $updateData = array('active'=>true,
           /* 'deactivated_at' => date('Y-m-d H:i:s'),
            'deactivated_by_type' =>Auth::user()->parent_type,
            'deactivated_by_id'=> Auth::user()->id*/
        );

        if(@$idsAdmin[0]->ids!='')
            $updateValues['admin'] = DB::table($that->table)
                ->whereRaw("id in(".$idsAdmin[0]->ids.")")
                ->update($updateData);


        $updateDataUser = array('active'=>true,
            /*'disabled_at' => date('Y-m-d H:i:s'),
            'disabled_by_type' =>Auth::user()->parent_type,
            'disabled_by_id'=> Auth::user()->id*/
        );



        if(@$idsUsers[0]->ids!='')
            $updateValues['users'] = DB::table($that->tableUsers)
                ->whereRaw("id in(".$idsUsers[0]->ids.") ")
                ->update($updateDataUser);



        return ['data'=>$record];
    }

    /**
     * @description getAdminUserHierarchy
     * @param $startWithId
     * @param $parent_type
     * @param $tenant_id
     * @return array
     */
    public static function getAdminUserHierarchy($startWithId, $parent_type, $tenant_id,$startID='') {

        $where = '';

        if($parent_type == ADMIN_TYPE) {
            if(@Auth::User()->id){
                $where = 'and c.id <> '. Auth::User()->id;
            }
        }

        $sql = 'WITH RECURSIVE cte_query AS
              (
                SELECT id, email, parent_type, parent_id, tenant_id,agent_name FROM admin_users as m WHERE id = ' . $startWithId . ' and tenant_id =' . $tenant_id . '

                UNION

                SELECT e.id, e.email, e.parent_type, e.parent_id, e.tenant_id,e.agent_name FROM admin_users as e
                INNER JOIN cte_query c ON c.parent_id = e.id '. $where .' and e.tenant_id =' . $tenant_id . '
              )
          SELECT * FROM cte_query';
        $admin_users = DB::select($sql);

        return $admin_users;
    }

    /**
     * @description getAdminDerectUser
     * @param $parent_id
     * @return array
     */
    public static function getAdminDerectUser($parent_id)
    {
        $sql = "
            SELECT admin_users.id
            FROM   admin_users
            WHERE  admin_users.parent_id = $parent_id
            AND    admin_users.parent_type = 'AdminUser'
            AND    admin_users.id NOT IN
                   (
                              SELECT     admin_users.id
                              FROM       admin_users
                              INNER JOIN admin_users_admin_roles
                              ON         admin_users_admin_roles.admin_user_id = admin_users.id
                              INNER JOIN admin_roles
                              ON         admin_roles.id = admin_users_admin_roles.admin_role_id
                              WHERE      admin_users.parent_id = $parent_id
                              AND        admin_users.parent_type = 'AdminUser'
                              AND        (((
                                                                          admin_roles.NAME = 'owner')
                                                    AND        (
                                                                          admin_roles.resource_type IS NULL)
                                                    AND        (
                                                                          admin_roles.resource_id IS NULL))))

        ";

        $admin_users = DB::connection('read_db')->select($sql);

        return $admin_users;
    }

    /**
     * @description getSQLSuperAdminTree
     * @param $tenant_id
     * @return string
     */
    public static function getSQLSuperAdminTree($tenant_id)
    {
        $sqlChanges = "SELECT  admin_users.id,CONCAT(agent_name, ' <br> &nbsp; &nbsp; &nbsp; &nbsp; players : ',
                                (SELECT count(id) FROM users where users.parent_id=admin_users.id and users.parent_type='AdminUser')) AS text
                            FROM       admin_users
                            INNER JOIN admin_users_admin_roles
                            ON         admin_users_admin_roles.admin_user_id = admin_users.id
                            INNER JOIN admin_roles
                            ON         admin_roles.id = admin_users_admin_roles.admin_role_id
                            WHERE      admin_users.tenant_id = $tenant_id


                            AND        (((
                                admin_roles.name = 'owner')
                                AND        (
                                    admin_roles.resource_type IS NULL)
                                      AND        (
                                admin_roles.resource_id IS NULL)))";

        return $sqlChanges;
    }

    /**
     * @description gwtSqlAdminTree
     * @param $id
     * @return string
     */
    public static function gwtSqlAdminTree($id){

        $sqlChanges = "SELECT admin_users.id,CONCAT(agent_name, ' <br> &nbsp; &nbsp; &nbsp; &nbsp; players : ',
                                            (SELECT count(id) FROM users where users.parent_id=admin_users.id and users.parent_type='AdminUser')) AS text
                            FROM   admin_users
                            WHERE  admin_users.parent_id = $id
                                        AND admin_users.parent_type = 'AdminUser'
                                        AND admin_users.id NOT IN (SELECT admin_users.id
                                                                  FROM   admin_users
                                       INNER JOIN admin_users_admin_roles
                                               ON
                                       admin_users_admin_roles.admin_user_id =
                                           admin_users.id
                                           INNER JOIN admin_roles
                                                 ON admin_roles.id =
                                            admin_users_admin_roles.admin_role_id
                                       WHERE  admin_users.parent_id = $id
                                        AND admin_users.parent_type = 'AdminUser'
                                        AND (( ( admin_roles.name = 'owner' )
                                            AND ( admin_roles.resource_type IS NULL )
                                       AND ( admin_roles.resource_id IS NULL ) )))
                          ";

        return $sqlChanges;
    }

    public function wallets() {
        return $this->hasMany(Wallets::class, 'owner_id')->where('owner_type', ADMIN_TYPE);
    }

    public function getRoleByAdminId($id){
        return AdminUsersAdminRoles::where('admin_user_id', $id)->select('admin_role_id')->get();
    }


}
