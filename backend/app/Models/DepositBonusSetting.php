<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DepositBonusSetting extends Model
{
    use HasFactory;

    protected $table ="deposit_bonus_settings";

    protected $fillable = [  ];

    public function bonus() {
        return $this->belongsTo(Bonus::class);
    }

}
