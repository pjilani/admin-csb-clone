<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventParticipant extends Model
{
    use HasFactory;

    protected $table="pulls_eventparticipants";

    protected $fillable = [  ];

    public function participant() {
        return $this->belongsTo(Participant::class);
    }

    public $timestamps = false;

}
