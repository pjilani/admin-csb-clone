<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bet extends Model
{
    use HasFactory;

    protected $table="bets_bets";

    protected $fillable = [];

    public function event() {
        return $this->belongsTo(Event::class);
    }

    public function betslip() {
        return $this->belongsTo(Betslip::class);
    }

    public function eventParticapnt() {
        return $this->hasOne(EventParticipant::class, 'event_id', 'event_id');
    }

    public $timestamps = false;

}
