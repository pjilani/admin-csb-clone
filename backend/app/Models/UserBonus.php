<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserBonus extends Model
{
    use HasFactory;

    protected $table ="user_bonus";

    protected $fillable = [ 'rollover_balance', 'status', 'transaction_id' ,'bonus_amount', 'updated_at'];

    public function bonus() {
        return $this->belongsTo(Bonus::class);
    }

    public function depositBonusSetting() {
        return $this->belongsTo(DepositBonusSetting::class, 'bonus_id', 'bonus_id');
    }

}
