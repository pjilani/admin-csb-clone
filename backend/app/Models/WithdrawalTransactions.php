<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WithdrawalTransactions extends Model
{
    use HasFactory;

    protected $table = 'withdrawal_transactions';

    protected $fillable = [
        "actionee_type",
        "actionee_id",
        "source_wallet_id",
        "target_wallet_id",
        "source_currency_id",
        "target_currency_id",
        "conversion_rate",
        "amount",
        "source_before_balance",
        "source_after_balance",
        "target_before_balance",
        "target_after_balance",
        "status",
        "comments",
        "created_at",
        "updated_at",
        "tenant_id",
        "transaction_id",
        "timestamp",
        "transaction_type",
        "success",
        "server_id",
        "round_id",
        "game_id",
        "table_id",
        "bet_type_id",
        "seat_id",
        "platform_id",
        "error_code",
        "error_description",
        "return_reason",
        "is_end_round",
        "credit_index",
        "debit_transaction_id",
        "payment_method",
        "other_currency_amount"
    ];


   
}
