<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TenantBankConfiguration extends Model
{
    use HasFactory;
    protected $table="tenant_bank_configuration";

    protected $fillable = [
        'tenant_id','used_for','bank_name','bank_ifsc_code','account_holder_name','account_number','status','created_by','last_updated_owner_id','created_at','updated_at'
    ];

    static function getList($tenant_id,$id = 0){
      $data = TenantBankConfiguration::select('tenant_bank_configuration.*','au.first_name','au.last_name')->where('tenant_bank_configuration.tenant_id',$tenant_id)->leftJoin('admin_users as au','au.id','=','tenant_bank_configuration.last_updated_owner_id');
      if($id != 0)
      $data = $data->where('tenant_bank_configuration.id',$id);

      $data = $data->orderBy('tenant_bank_configuration.id','DESC')
                   ->get();
      return $data;
    }
}
