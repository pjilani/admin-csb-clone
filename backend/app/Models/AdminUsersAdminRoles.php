<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminUsersAdminRoles extends Model
{
    use HasFactory;

    protected $table="admin_users_admin_roles";

    protected $fillable = [
        'admin_user_id',
        'admin_role_id'
    ];


}
