<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TenantSiteMaintenance extends Model
{
    use HasFactory;

    protected $table="tenant_site_maintenance";

    protected $fillable = [
        'tenant_id',
        'type',
        'title',
        'announcement_title',
        'is_announcement_active',
        'site_down',
        'created_at',
        'updated_atl'
    ];
}
