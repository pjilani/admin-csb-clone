<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wallets extends Model
{
    use HasFactory;

    protected $fillable = [
        'amount',
        'primary',
        'currency_id',
        'owner_type',
        'owner_id',
        'created_at',
        'updated_at',
        'non_cash_amount',
        'withdrawal_amount'
    ];

    public function currency() {
        return $this->belongsTo(Currencies::class);
    }

    public function user() {
        return $this->belongsTo(User::class, 'owner_id');
    }

}




