<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlayerCommission extends Model
{
    use HasFactory;

    protected $table="player_commission";

    protected $fillable=[
        'commission_amount',
        'commission_percentage',
        'status',
        'user_id',
        'ngr',
        'interval_type',
        'month_id',
        'week_id',
        'actioned_at'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id' );
    }
}