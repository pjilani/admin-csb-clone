<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasFactory,HasApiTokens,HasFactory, Notifiable;

    protected $table='users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'email_verified',
        'encrypted_password',
        'phone',
        'phone_verified',
        'date_of_birth',
        'gender',
        'locale',
        'sign_in_count',
        'sign_in_ip',
        'parent_type',
        'parent_id',
        'created_at',
        'updated_at',
        'user_name',
        'country_code',
        'tenant_id',
        'active',
        'last_login_date',
        'self_exclusion',
        'deleted_at',
        'vip_level',
        'nick_name',
        'disabled_at',
        'disabled_by_type',
        'disabled_by_id',
        'phone_code',
        'kyc_done',
        'city',
        'demo',
        'zip_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'encrypted_password'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function wallet() {
        return $this->hasOne(Wallets::class, 'owner_id')->where('owner_type', 'User');
        // return $this->hasOne('App\Models\Wallets', 'owner_id', 'id')->where('owner_type', 'User');
    }

    /**
     * @description getEmails
     * @param $email
     * @return \Illuminate\Support\Collection
     */
    public static function getEmails($email){

        $that=new self();

        $sqlSelectEmail ='id,user_name AS text';
        if(Auth::user()->parent_type =='SuperAdminUser') {
            $sqlSelectEmail ="id,(CONCAT(email,' (',(SELECT name FROM tenants where id =p.tenant_id),')')) AS text";
        }
        $record = DB::table(DB::raw($that->table.' as p'))
            ->select(DB::raw($sqlSelectEmail));

        if(@$email!=''){
            $whereStr =" p.email like '%".$email."%' or p.user_name like '%".$email."%'";
            $record->WhereRaw($whereStr);
        }

        if(Auth::user()->parent_type=='AdminUser') {
            $record->Where('p.tenant_id',Auth::user()->tenant_id);
        }
        $result=$record->get();

        return @$result;
    }


    public static function getPlayerEmails($email){

        $that=new self();

        $sqlSelectEmail ='id,email AS text';
        if(Auth::user()->parent_type =='SuperAdminUser') {
            $sqlSelectEmail ="id,(CONCAT(email,' (',(SELECT name FROM tenants where id =p.tenant_id),')')) AS text";
        }
        $record = DB::table(DB::raw($that->table.' as p'))
            ->select(DB::raw($sqlSelectEmail));

        if(@$email!=''){
            $whereStr =" p.email like '%".$email."%'";
            $record->WhereRaw($whereStr);
        }

        if(Auth::user()->parent_type=='AdminUser') {
            $record->Where('p.tenant_id',Auth::user()->tenant_id);
            $record->Where('p.parent_id',Auth::user()->id);
        }
        $result=$record->get();

        return @$result;
    }

    /**
     * @description storeElasticSearchData
     * @param $userId
     */
    public static function storeElasticSearchData($userId) {
        
        
        $Obj=new self();
        $userInfo         = $Obj->getUserInfo($userId);
        $transaction_data = $Obj->getTotalBets($userId);
        $totalBetsCount = $Obj->getTotalDistinctBets($userId);
        $userParentIds    = $Obj->getUserParentids($userId, $userInfo->tenant_id);
        $betDetails= $Obj->getBetDetails($userId);
        $userFirstDeposit = $Obj->getFirstDepositAmount($userId,$userInfo->wallet_id);
        if(!empty($userFirstDeposit) && count($userFirstDeposit)>0){
            $userFirstDepositId = $userFirstDeposit["id"];
            $userFirstDepositAmount = $userFirstDeposit["amount"];
        }else{
            $userFirstDepositId = 0;
            $userFirstDepositAmount = 0;
        }
       // echo $userFirstDeposit;die;
        $data = array(
            'player_id' => (int)$userId,
            'player_id_s' => (string)strval($userId),
            'vip_level' => $userInfo->vip_level,
            'tenant_id' => $userInfo->tenant_id,
            'nick_name' => $userInfo->nick_name,
            'user_name' => $userInfo->user_name,
            'email' => $userInfo->email,
            'phone' => $userInfo->phone,
            'phone_code' => $userInfo->phone_code,
            'agent_full_name' => $userInfo->agent_first_name . ' ' . $userInfo->agent_last_name,
            'agent_name' => $userInfo->agent_name,
            'last_login' => getTimeZoneWiseTimeISO($userInfo->last_login_date),
            'country' => $userInfo->country_code,
            'creation_date' => getTimeZoneWiseTimeISO($userInfo->created_at),
            'real_balance' => floatval($userInfo->amount),
            'non_cash_balance' => floatval($userInfo->non_cash_amount),
            'total_balance' => floatval($userInfo->amount) + floatval($userInfo->non_cash_amount),
            'currency' => $userInfo->currency,
            'wallet_id' => intval($userInfo->wallet_id),
            'total_bets' => intval($totalBetsCount??0),
            'total_bet_amount' => floatval($transaction_data->total_bet_amount??0),
            'total_sport_bets' => intval($betDetails->total_bets??0),
            'total_sport_bet_amount' => floatval($betDetails->total_bet_amount??0),
            'parent_id' => intval($userInfo->parent_id),
            'parent_type' => $userInfo->parent_type,
            'parent_chain_ids' => $userParentIds['ids'],
            'kyc_done' => $userInfo->kyc_done ? "true" : "false",
            'demo' => $userInfo->demo ? "true" : "false",
            'status' => getUserStatusString($userInfo->active),
            'first_deposit_amount' => floatval($userFirstDepositAmount),
            'first_deposit_amount_transaction_id' => $userFirstDepositId
        );

        $params = [
            'body' =>  $data,
            'index' => ELASTICSEARCH_INDEX['users'],
            'type' => ELASTICSEARCH_TYPE,
            'id' => $userId,
        ];


        $client = getEsClientObj();
        $return = $client->index($params);


    }

    /**
     * @description getUserInfo
     * @param $userId
     * @return \Illuminate\Database\Eloquent\Model|null|object|static
     */
    public function getUserInfo($userId) {
        $Obj = new self();

        $userInfo = DB::connection('read_db')->table("{$Obj->table} as U" )
                    ->leftjoin('wallets AS W', function($join){
                        $join->on('W.owner_id', '=', 'U.id');
                        $join->on('W.owner_type', DB::raw(" 'User' "));
                    })
                ->leftJoin('admin_users AS AU', 'AU.id', '=', 'U.parent_id')
                ->leftJoin('currencies AS C', 'C.id', '=', 'W.currency_id')
                ->select(['U.first_name', 'U.last_name', 'U.email', 'U.phone', 'U.date_of_birth', 'U.gender', 'U.locale', 'U.parent_type',
                            'U.parent_id', 'U.created_at', 'U.updated_at', 'U.user_name', 'U.country_code', 'U.tenant_id', 'U.active', 'U.demo',
                            'U.last_login_date', 'U.self_exclusion', 'U.vip_level', 'U.nick_name', 'U.phone_code', 'U.kyc_done',
                            'W.id as wallet_id', 'W.amount','W.non_cash_amount', 'W.currency_id',
                            'AU.first_name as agent_first_name','AU.email as agent_email', 'AU.last_name as agent_last_name', 'AU.agent_name',
                            'C.code as currency'] )
                ->where('U.id', $userId)
                ->first();
        return $userInfo;
    }


    /* Transaction type status code
        BET = 0, WIN = 1, REFUND = 2, DEPOSIT = 3, WITHDRAW = 4, NON_CASH_GRANTED_BY_ADMIN = 5,
        NON_CASH_WITHDRAW_BY_ADMIN = 6, TIP = 7, BET_NON_CASH = 8, WIN_NON_CASH = 9, REFUND_NON_CASH = 10,
        NON_CASH_BONUS_CLAIM = 11, DEPOSIT_BONUS_CLAIM = 12, TIP_NON_CASH = 13, WITHDRAW_CANCEL = 14
    */
    private function getTotalBets($userId) {

        $data =  DB::connection('read_db')->table('transactions')
                    ->where('actionee_id', $userId)
                    ->where('actionee_type', DB::raw(" 'User' "))
                    ->whereIn('transaction_type', TRANSACTION_TYPE_ARRAY_REPORT)
                    ->groupBy('actionee_id')
                    ->select(DB::raw('COUNT( id ) AS total_bets, SUM( amount ) AS total_bet_amount'))
                    ->first();

        return $data;
    }

    private function getTotalDistinctBets($userId) {

        $data =  DB::connection('read_db')->table('transactions')
                    ->where('actionee_id', $userId)
                    ->where('actionee_type', DB::raw(" 'User' "))
                    ->whereIn('transaction_type', TRANSACTION_TYPE_ARRAY_REPORT)
                    ->groupBy('actionee_id')
                    ->distinct()->count('transaction_id');
                   
        return $data;
    }

    /**
     * @description getBetDetails
     * @param $playerId
     * @return \Illuminate\Database\Eloquent\Model|null|object|static
     */
    private function getBetDetails($playerId){
        $data =  DB::connection('read_db')->table('bets_transactions')
            ->where('user_id', '=',$playerId)
            ->where('payment_for', '=',1)
            ->select(DB::raw('COUNT( id ) AS total_bets, SUM(COALESCE(non_cash_amount, 0) + COALESCE(amount,0)) AS total_bet_amount'))
            ->first();

        return $data;

    }

    /**
     * @description getUserParentids
     * @param $userId
     * @param $tenantID
     * @return array
     */
    public function getUserParentids($userId, $tenantID) {

        $sql = "WITH RECURSIVE children AS (
                SELECT id, 'User' AS type, parent_id, parent_type, tenant_id, active, 0 AS relative_depth, first_name,first_name AS agent_name, last_name, email
                FROM users
                WHERE id = $userId AND tenant_id = $tenantID

                UNION ALL

                SELECT parent.id, 'AdminUser' AS type, parent.parent_id, parent.parent_type, parent.tenant_id, parent.active, child.relative_depth - 1, parent.first_name,parent.agent_name, parent.last_name, parent.email
                FROM admin_users parent, children child
                WHERE parent.id = child.parent_id AND child.parent_type LIKE 'AdminUser' AND
                  (parent.id != child.id OR child.type != 'AdminUser') AND parent.tenant_id = child.tenant_id
              )
              SELECT * FROM children order by relative_depth desc";

        $userParentsData = DB::connection('read_db')->select($sql);

        $parentIds = [];
        $parentInfo = [];

        if(!empty($userParentsData)) {
            foreach($userParentsData as $val) {
                if($val->type == 'AdminUser') {
                    $parentIds[] = $val->id;
                    $parentInfo[] = $val;
                }
            }
        }

        return ['ids' => $parentIds, 'data' => $parentInfo] ;
    }

    public static function storeElasticSearchDataAdminUser($param=[]) {

        if(!$param){
            return false;
        }

        $data['tenant_id'] = $param['tenant_id'];
        $data['created_at'] = getTimeZoneWiseTimeISO($param['created_at']);
        $data['transaction_type'] = "dummy_for_admin";
        $data['player_details']['agent_full_name'] = $param['first_name'] ." ".$param['last_name'];
        $data['player_details']['agent_name'] = $param['agent_name'];
        $data['player_details']['agent_email'] = $param['email'];
        $data['player_details']['currency'] = $param['currency'];
        $data['player_details']['demo'] = @$param['demo']?true:false;
        $data['player_details']['parent_id'] = $param['parent_id'];

        /*
                $parentChainIdsArray = [];
                $parentChainDetailsArray = [];

                $parent_chain_ids = Admin::getAdminUserHierarchy($param['id'],ADMIN_TYPE, $param['tenant_id']);
                foreach ($parent_chain_ids as $key => $value){
                    $value = (array)$value;
                    $parentChainIdsArray[] = $value['id'];

                    $commissionPercentage = AdminUserSetting::where('admin_user_id',$value['id'])->get('value')->first();
                    $commission = '000.00';
                    if(!is_null($commissionPercentage)){
                        $commissionPercentage=$commissionPercentage->toArray();
                        $commission=number_format($commissionPercentage['value'],2);
                        $commissionCheck=explode('.',$commission);
                        if(is_array($commissionCheck)){
                            if(strlen($commissionCheck[0])==1){
                                $commissionCheck[0]=sprintf('%03d', $commissionCheck[0]);
                            }elseif (strlen($commissionCheck[0])==2){
                                $commissionCheck[0]=sprintf('%03d', $commissionCheck[0]);
                            }
                        }
                        $commission =implode('.',$commissionCheck);
                    }
                    $parentChainDetailsArray[] = $value['id']."-".$value['agent_name']."-".$param['currency']."-".$commission;
                }*/

        $parentChainIdsArrayfun =self::getAdminParentDetails($param);
        $data['player_details']['parent_chain_ids'] = $parentChainIdsArrayfun['parentChainIdsArray'];
        $data['player_details']['parent_chain_detailed'] = $parentChainIdsArrayfun['parentChainDetailsArray'];

        $params = [
            'body' =>  $data,
            'index' => ELASTICSEARCH_INDEX['transactions'],
            'type' => ELASTICSEARCH_TYPE,
            'id' => "dummy_for_admin_".$param['id']."_".$param['currency'],
        ];


        $client =getEsClientObj();
        $return = $client->index($params);
    }

    public static function  getAdminParentDetails($param = []){
        if(!$param){
            return [];
        }
        $parent_chain_ids = Admin::getAdminUserHierarchy($param['id'],ADMIN_TYPE, $param['tenant_id']);
        foreach ($parent_chain_ids as $key => $value){
            $value = (array)$value;
            $parentChainIdsArray[] = $value['id'];

            $commissionPercentage = AdminUserSetting::where('admin_user_id',$value['id'])->get('value')->first();
            $commission = '000.00';
            if(!is_null($commissionPercentage)){
                $commissionPercentage=$commissionPercentage->toArray();
                $commission=number_format($commissionPercentage['value'],2);
                $commissionCheck=explode('.',$commission);
                if(is_array($commissionCheck)){
                    if(strlen($commissionCheck[0])==1){
                        $commissionCheck[0]=sprintf('%03d', $commissionCheck[0]);
                    }elseif (strlen($commissionCheck[0])==2){
                        $commissionCheck[0]=sprintf('%03d', $commissionCheck[0]);
                    }
                }
                $commission =implode('.',$commissionCheck);
            }
            $parentChainDetailsArray[] = $value['id']."-".$value['agent_name']."-".$param['currency']."-".$commission;
        }
        return ['parentChainIdsArray'=>$parentChainIdsArray,'parentChainDetailsArray'=>$parentChainDetailsArray];
    }

    private function getFirstDepositAmount($playerId,$walletId){
        $data = DB::select("SELECT id, amount from transactions WHERE transaction_type = 3   AND (( comments = 'Deposit Request Approved'  AND target_wallet_id = $walletId) OR (actionee_type='User' AND actionee_id = $playerId AND comments='Deposit Request')) order by id ASC LIMIT 1 OFFSET 0");  
        if(!empty($data) && count($data)>0){
            $firstDeposit = array("id"=>$data[0]->id,"amount"=>$data[0]->amount);
            return $firstDeposit;
        }else{
            return 0;
        }
    }

    public static function storeElasticBulkUserTransactionData($transactions)
    {
        $bulkActions = [];
       
        foreach ($transactions as $transaction) {
            $userId = $transaction->id;
            $Obj=new self();
            $userInfo         = $Obj->getUserInfo($userId);
            $transaction_data = $Obj->getTotalBets($userId);
            $totalBetsCount = $Obj->getTotalDistinctBets($userId);
            $userParentIds    = $Obj->getUserParentids($userId, $userInfo->tenant_id);
            $betDetails= $Obj->getBetDetails($userId);
            $userFirstDeposit = $Obj->getFirstDepositAmount($userId,$userInfo->wallet_id);
            if(!empty($userFirstDeposit) && count($userFirstDeposit)>0){
                $userFirstDepositId = $userFirstDeposit["id"];
                $userFirstDepositAmount = $userFirstDeposit["amount"];
            }else{
                $userFirstDepositId = 0;
                $userFirstDepositAmount = 0;
            }
        // echo $userFirstDeposit;die;
            $data = array(
                'player_id' => (int)$userId,
                'player_id_s' => (string)strval($userId),
                'vip_level' => $userInfo->vip_level,
                'tenant_id' => $userInfo->tenant_id,
                'nick_name' => $userInfo->nick_name,
                'user_name' => $userInfo->user_name,
                'email' => $userInfo->email,
                'phone' => $userInfo->phone,
                'phone_code' => $userInfo->phone_code,
                'agent_full_name' => $userInfo->agent_first_name . ' ' . $userInfo->agent_last_name,
                'agent_name' => $userInfo->agent_name,
                'last_login' => getTimeZoneWiseTimeISO($userInfo->last_login_date),
                'country' => $userInfo->country_code,
                'creation_date' => getTimeZoneWiseTimeISO($userInfo->created_at),
                'real_balance' => floatval($userInfo->amount),
                'non_cash_balance' => floatval($userInfo->non_cash_amount),
                'total_balance' => floatval($userInfo->amount) + floatval($userInfo->non_cash_amount),
                'currency' => $userInfo->currency,
                'wallet_id' => intval($userInfo->wallet_id),
                'total_bets' => intval($transaction_data->total_bets??0),
                'total_bet_amount' => floatval($totalBetsCount??0),
                'total_sport_bets' => intval($betDetails->total_bets??0),
                'total_sport_bet_amount' => floatval($betDetails->total_bet_amount??0),
                'parent_id' => intval($userInfo->parent_id),
                'parent_type' => $userInfo->parent_type,
                'parent_chain_ids' => $userParentIds['ids'],
                'kyc_done' => $userInfo->kyc_done ? "true" : "false",
                'demo' => $userInfo->demo ? "true" : "false",
                'status' => getUserStatusString($userInfo->active),
                'first_deposit_amount' => floatval($userFirstDepositAmount),
                'first_deposit_amount_transaction_id' => $userFirstDepositId
            );
            $bulkActions[] = $data; // Add the document data directly
       }
       $params = ['body' => []];
       $client = getEsClientObj();
       for ($i = 0; $i < count($transactions); $i++) {
          $params['body'][] = [
               'index' => [
                   '_index' => ELASTICSEARCH_INDEX['users'],
                   '_type' => ELASTICSEARCH_TYPE,
                   '_id'    => $bulkActions[$i]['player_id_s']
               ]
           ];

           $params['body'][] = $bulkActions[$i];
           // Every 1000 documents stop and send the bulk request
           if ($i % 1000 == 0) {
            
               $responses = $client->bulk($params);

               // erase the old bulk request
               $params = ['body' => []];

               // unset the bulk response when you are done to save memory
               unset($responses);
           }
       }

       // Send the last batch if it exists
       if (!empty($params['body'])) {
           $responses = $client->bulk($params);
       }
      // echo "casino transaction re-index successfully done";
      
       
    }
}
