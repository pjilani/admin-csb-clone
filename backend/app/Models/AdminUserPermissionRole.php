<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminUserPermissionRole extends Model
{
    use HasFactory;

    protected $table="admin_users_permission_roles";

    protected $fillable = [
        'admin_user_id',
        'permission_role_id'
    ];


}
