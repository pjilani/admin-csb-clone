<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TenantThemeSettings extends Model
{
    use HasFactory;

    protected $table="tenant_theme_settings";

    protected $fillable = [
        'tenant_id',
        'layout_id',
        'language_id',
        'theme',
        'created_at',
        'updated_at',
        'user_login_type',
        'sms_gateway',
        'logo_url',
        'fab_icon_url',
        'assigned_providers',
        'allowed_modules',
        'payment_providers',
        'signup_popup_image_url',
        'whatsapp_number',
        'powered_by'
    ];
}
