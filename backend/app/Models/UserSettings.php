<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserSettings extends Model
{
    use HasFactory;

    protected $table="user_settings";

    protected $fillable=[
        'key',
        'value',
        'duration',
        'description',
        'user_id',
        'created_at',
        'updated_at'
    ];
}
