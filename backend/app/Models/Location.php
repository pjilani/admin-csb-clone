<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasFactory;

    protected $table="pulls_locations";

    protected $fillable = ['is_deleted', 'location_id', 'name_de', 'name_en', 'name_fr', 'name_ru', 'name_tr', 'name_nl'];

}
