<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Casino\CasinoGames;

class Transactions extends Model
{
    use HasFactory;

    protected $table = 'transactions';
    protected $tableWithdraw = 'withdraw_requests';
    protected $tableDepositRequest = 'deposit_requests';
    protected $tableWallets = 'wallets';

    protected $fillable = [
        "actionee_type",
        "actionee_id",
        "source_wallet_id",
        "target_wallet_id",
        "source_currency_id",
        "target_currency_id",
        "conversion_rate",
        "amount",
        "source_before_balance",
        "source_after_balance",
        "target_before_balance",
        "target_after_balance",
        "status",
        "comments",
        "created_at",
        "updated_at",
        "tenant_id",
        "transaction_id",
        "timestamp",
        "transaction_type",
        "success",
        "server_id",
        "round_id",
        "game_id",
        "table_id",
        "bet_type_id",
        "seat_id",
        "platform_id",
        "error_code",
        "error_description",
        "return_reason",
        "is_end_round",
        "credit_index",
        "debit_transaction_id",
        "payment_method",
        "other_currency_amount"
    ];


    public static function getList($limit, $offset, $filter)
    {
        $that = new self();

        $whereStr = '';

        if ($offset == 1) {
            $offset = 0;
        }
//        DB::enableQueryLog();
        $record = DB::connection('read_db2')->table($that->table, 'p')
            ->select('p.*',
                DB::raw("
                  (SELECT code FROM currencies sc where sc.id=p.source_currency_id) as source_currency_id,
                  (SELECT code FROM currencies tc where tc.id= p.target_currency_id) as target_currency_id,
                   (CASE 
                        WHEN p.actionee_type = 'AdminUser' THEN (
                            SELECT 
                                concat(au.email , ' - ', au.first_name, ' ', au.last_name )
                              FROM admin_users as au where au.id=p.actionee_id 
                        )
                        WHEN p.actionee_type = 'SuperAdminUser' THEN (
                            SELECT 
                                concat(sau.email , ' - ', sau.first_name, ' ', sau.last_name )
                              FROM super_admin_users as sau where sau.id=p.actionee_id 
                        )
                        ELSE (
                              SELECT 
                                concat(u.email , ' - ', u.first_name, ' ', u.last_name )
                              FROM users as u where u.id=p.actionee_id 
                        )
                    END) AS actionee_name,
                     (
                            SELECT concat(au.first_name, ' ', au.last_name, ' - ', au.email) 
                    FROM admin_users as au where au.id=(
                    
                            SELECT w.owner_id
                    FROM wallets as w
                    WHERE w.id = p.target_wallet_id and w.owner_type ='AdminUser')
                    
                            )as target_wallet_name_admin,
                     (
                            SELECT concat(au.first_name, ' ', au.last_name, ' - ', au.email) 
                    FROM super_admin_users as au where au.id=(
                    
                            SELECT w.owner_id
                    FROM wallets as w
                    WHERE w.id = p.target_wallet_id and w.owner_type ='SuperAdminUser')
                    
                            )as target_wallet_name_super_admin,
                            (
                            SELECT concat(u.first_name, ' ', u.last_name, ' - ', u.email) 
                        FROM users as u where u.id=(
                    
                            SELECT w1.owner_id
                            FROM wallets as w1
                            WHERE w1.id = p.target_wallet_id 
                            and w1.owner_type ='User')
                            ) as target_wallet_name,
                          
                            (
                            SELECT concat(au.first_name, ' ', au.last_name, ' - ', au.email) 
                    FROM admin_users as au where au.id=(
                    
                            SELECT w.owner_id
                    FROM wallets as w
                    WHERE w.id = p.source_wallet_id and w.owner_type ='AdminUser')
                    
                            )as source_wallet_name_admin,

                            (
                              SELECT concat(su.first_name, ' ', su.last_name, ' - ', su.email) 
                      FROM super_admin_users as su where su.id=(
                      
                              SELECT w.owner_id
                      FROM wallets as w
                      WHERE w.id = p.source_wallet_id and w.owner_type ='SuperAdminUser')
                      
                              )as source_wallet_name_super_admin,
                            (
                            SELECT concat(u.first_name, ' ', u.last_name, ' - ', u.email) 
                        FROM users as u where u.id=(
                    
                            SELECT w1.owner_id
                            FROM wallets as w1
                            WHERE w1.id = p.source_wallet_id 
                            and w1.owner_type ='User')
                            ) as source_wallet_name
                            
                             ")
            )->leftjoin('users AS u', function ($join) {
                $join->on('p.actionee_id', '=', 'u.id');
            });

        $filter['search']=str_replace("'", "", $filter['search']);
        if (@$filter['search'] != '' && is_integer((int)$filter['search']) && (int)$filter['search']) {
            $whereStr = " p.id  = " . (int)$filter['search'] . " OR p.amount  = " . (float)$filter['search'] ." OR p.actionee_id  = " . (int)$filter['search'] . " OR u.phone like '%".(string)$filter['search']."%' OR p.round_id like '%" . (string)$filter['search'] . "%' ";
            $record->WhereRaw($whereStr);
        }else{
            //        DB::enableQueryLog();
            $whereStrSearch = '';
            if($filter['search'] !==''){
                $whereStrSearch = "  u.email like '%".$filter['search']."%' OR au.email like '%".$filter['search']."%' OR u.user_name like '%".$filter['search']."%'";
            }
            $wallet_info = DB::connection('read_db2')->table('wallets as w')
                ->leftjoin('users AS u', function ($join) {
                    $join->on('w.owner_id', '=', 'u.id');
                    $join->on('w.owner_type', DB::raw(" 'User' "));
                })
                ->leftjoin('admin_users AS au', function ($join) {
                    $join->on('w.owner_id', '=', 'au.id');
                    $join->on('w.owner_type', DB::raw(" 'AdminUser' "));
                });
                if($whereStrSearch !=='') {
                    $wallet_info->whereRaw($whereStrSearch);
                }
            $wallet_infoSQl = $wallet_info->select(['w.id'])->toSql();
            if ($wallet_infoSQl){
                $whereStr = " (p.source_wallet_id  in (" .$wallet_infoSQl. ") OR  p.target_wallet_id  in (" .$wallet_infoSQl . ") )";

                $record->WhereRaw($whereStr);
            }
            else {
                $record->where('p.source_wallet_id', 0)->orWhere('p.target_wallet_id', 0);

            }
            //       $quries = DB::getQueryLog();


//        du($quries);
        }

       /* if (@$filter['wallet_id'] != ''){
            $whereStr = " (p.source_wallet_id  = " . (int)$filter['wallet_id'] . " OR p.target_wallet_id  = " . (int)$filter['wallet_id'] . ")";

            $record->WhereRaw($whereStr);
        }*/


        if (@$filter['tenant_id'] != '') {
            $whereStr = " p.tenant_id = " . (int)$filter['tenant_id'];
            $record->WhereRaw($whereStr);
        }
        if (@$filter['action_type'] != '') {
            $whereStr = " p.transaction_type = " . (int)$filter['action_type'];
            $record->WhereRaw($whereStr);
        }

        if (Auth::user()->parent_type == 'AdminUser') {
            $roles = getRolesDetails();
            if (!in_array('owner', $roles)) {
                if (@$filter['login_id'] != '' && @$filter['login_type'] != '') {
                    $isTopLevel=true;
                    $loginAgentRole = getRoleByAdminId(Auth::user()->id);
                    if($loginAgentRole && ($loginAgentRole->admin_role_id==3||$loginAgentRole->admin_role_id==4||$loginAgentRole->admin_role_id==5)){
                        $isTopLevel =false;
                    }
                    $Hierarchy=getAdminHierarchy(Auth::user()->id, $isTopLevel);
                    $ids=flatten($Hierarchy);
                    $whereStrParentSQL='';
                    if($ids)
                    $whereStrParentSQL = "and owner_id in (" . implode(',',$ids).")"; 
//                    $sql = "select ARRAY_TO_STRING(ARRAY_AGG(id), ',') as wallets_ids from wallets where owner_type ='AdminUser' $whereStrParentSQL";
                    $sql = "select id as wallets_ids from wallets where owner_type ='AdminUser' $whereStrParentSQL";
                   // $idResult = DB::connection('read_db')->select($sql);
                    $whereStrChild='';
                    if($sql){
//                        $idResult=$idResult[0]->wallets_ids;
                        $whereStrChild=" OR  (source_wallet_id in (".$sql.")   OR  target_wallet_id in (".$sql.")   )";
                    }
                    if ($ids && $loginAgentRole->admin_role_id!==3) {
                        $idsString = implode(',', $ids);
                        $whereStr = "(( p.actionee_type = 'AdminUser' and p.actionee_id in(" . $idsString . "))
                        $whereStrChild
                        Or ( p.actionee_type = 'User' and p.actionee_id in( SELECT id FROM users  where parent_id in( " . $idsString . "))))";
                        $record->WhereRaw($whereStr);
                    }
                }
            }
        }
        if (@$filter['currency_id'] != '') {
            $whereStr = " (p.source_currency_id = " . (int)$filter['currency_id']." OR p.target_currency_id = " . (int)$filter['currency_id'].")";
            $record->WhereRaw($whereStr);
        }
        if (@$filter['time_period'] != '' && count($filter['time_period'])) {
            // $whereStr = " (p.created_at >= '" . date('Y-m-d H:i:s.u', strtotime($filter['time_period']['start_date'])) . "' and p.created_at <= '" . date('Y-m-d H:i:s.u', strtotime($filter['time_period']['end_date'])) . "')";
            // $record->WhereRaw($whereStr);
            $record->whereBetween('p.created_at', [$filter['time_period']['fromdate'], str_replace("000000", "999999", $filter['time_period']['enddate'])]);
        }
        $record->whereNotIn('transaction_type', [20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37]);
        $count = $record->count();

        if($limit=='all' && $offset =='all'){
            $result = $record->orderBy('p.' . @$filter['sort_by'], @$filter['order'])->get();
        }else{
            $result = $record->orderBy('p.' . @$filter['sort_by'], @$filter['order'])->forPage($offset, $limit)->get();

        }
//       $quries = DB::getQueryLog();


//        du($quries);
        return ['data' => $result, 'count' => @$count];
    }

    public static function getForWithdrawList($limit, $offset, $filter,$type="unverified")
    {
        $that = new self();
        
        $whereStr = '';

        if ($offset == 1) {
            $offset = 0;
        }
        $record = DB::connection('read_db')->table($that->tableWithdraw, 'p')
            ->select('p.*','w.id as wallet_id',
                DB::raw("(SELECT user_name FROM users WHERE id=p.user_id) as user_name"),
                DB::raw("(SELECT vip_level FROM users WHERE id=p.user_id) as vip_level")
            )->leftJoin('wallets as w','w.owner_id','=','p.user_id');
        if (@$filter['search'] != '') {
            // $whereStr = " lower(p.name) like '%" . strtolower($filter['search']) . "%'";
            $searchTerm =$filter['search'];
            $whereStr = " ((LOWER(p.name) iLIKE '%" . $searchTerm . "%' OR 
                          CAST(p.user_id AS TEXT) iLIKE '%" . $searchTerm . "%' OR LOWER(phone_number) iLIKE '%" . $searchTerm . "%' OR CAST(p.id AS TEXT) iLIKE '%" . $searchTerm . "%') OR (SELECT user_name FROM users WHERE id = p.user_id) iLIKE '%" . $searchTerm . "%')";
            $record->WhereRaw($whereStr);
        }

        if(@$filter['tenant_id']!=''){
            $whereStr =" p.tenant_id = ".(int)$filter['tenant_id'];
            $record->WhereRaw($whereStr);
        }
  
        if(@$type != ''){
          if(@$type == 'rejected'){
            $whereStr = " (p.status = 'rejected' OR p.status = 'rejected_by_gateway' OR p.status = 'cancelled') ";
            $record->WhereRaw($whereStr);
          }elseif(@$type == 'verified'){
            $whereStr ="(p.verify_status = '" . $type . "' and ( p.status != 'rejected' and p.status != 'rejected_by_gateway' and p.status != 'cancelled'))";
            $record->WhereRaw($whereStr);
          }else{
            $whereStr ="(p.verify_status = '" . $type . "' and (p.status = 'pending' and p.status != 'cancelled') )";
            $record->WhereRaw($whereStr);
          }
            
        }

        if (@$filter['status'] != '') {
            $whereStr = " p.status = '" . $filter['status'] . "'";
            $record->WhereRaw($whereStr);
        }
        
        if (@$filter['withdrawal_type'] != 'all') {
            $whereStr = " p.withdrawal_type = '" . $filter['withdrawal_type'] . "'";
            $record->WhereRaw($whereStr);
        }
        // if (@$filter['time_period'] != '' && count($filter['time_period'])) {
        //     $whereStr = " (p.created_at >= '" . $filter['time_period']['start_date'] . " 00:00:00' and p.created_at <= '" . $filter['time_period']['end_date'] . " 23:59:59')";
        //     $record->WhereRaw($whereStr);
        // }
        if (@$filter['time_period'] != '' && count($filter['time_period'])) {
            // $whereStr = " (p.created_at >= '" . date('Y-m-d H:i:s.u', strtotime($filter['time_period']['start_date'])) . "' and p.created_at <= '" . date('Y-m-d H:i:s.u', strtotime($filter['time_period']['end_date'])) . "')";
            // $record->WhereRaw($whereStr);
            $record->whereBetween('p.created_at', [$filter['time_period']['fromdate'], str_replace("000000", "999999", $filter['time_period']['enddate'])]);
        }
        $count = $record->count();
        $sum = $record->sum('p.amount');
        // DB::enableQueryLog();
        if(array_key_exists('download', $filter)){
            $result = $record->orderBy(@$filter['sort_by'], @$filter['order'])->distinct(@$filter['sort_by'])->get();
        }
        else {
            $result = $record->orderBy(@$filter['sort_by'], @$filter['order'])->forPage($offset, $limit)->distinct(@$filter['sort_by'])->get();
        }

        return ['data' => $result, 'count' => @$count,'total' => $sum];
    }
    
    
    
    public static function getDepositRequestList($limit, $offset, $filter)
    {
        $that = new self();

        $whereStr = '';

        if ($offset == 1) {
            $offset = 0;
        }
//        DB::enableQueryLog();
        $record = DB::connection('read_db')->table($that->tableDepositRequest, 'p')
            ->select('p.*','w.id as wallet_id',DB::raw("CONCAT(au.first_name,au.last_name) AS action_by"),'u.user_name', 'pp.provider_name'
            )
            ->leftJoin('admin_users as au','au.id','=','p.action_id')
            ->leftJoin('wallets as w','w.owner_id','=','p.user_id')
            ->leftJoin('users as u','u.id','=','p.user_id')
            ->leftJoin('payment_providers as pp','pp.id','=','p.payment_provider_id');
        if (@$filter['search'] != '') {
            $whereStr = " (lower(u.user_name) like '%" . strtolower($filter['search']) . "%' OR (p.utr_number) like '%" . $filter['search'] . "%' OR (p.amount::varchar) like '%" . strtolower($filter['search']) . "%' OR (p.id::varchar) like '%" . strtolower($filter['search']) . "%')";
            $record->WhereRaw($whereStr);
        }

        if (@$filter['transaction_id'] != '') {
            $whereStr = "  (p.order_id) like '%" . $filter['transaction_id'] . "%' or (p.utr_number) like '%" . $filter['transaction_id'] . "%'";
            $record->WhereRaw($whereStr);
        }

        if(@$filter['deposit_type']!=''){
            $whereStr =" p.deposit_type = '".$filter['deposit_type']."'";
            $record->WhereRaw($whereStr);
        }
       
        if(@$filter['tenant_id']!=''){
            $whereStr =" p.tenant_id = ".(int)$filter['tenant_id'];
            $record->WhereRaw($whereStr);
        }

        if (@$filter['status'] != '') {
            $whereStr = " p.status = '" . $filter['status'] . "'";
            $record->WhereRaw($whereStr);
        }
       
        if(@$filter['vip_levels'] != ''){
            $whereStr =" u.vip_level IN ({$filter['vip_levels']})";
            $record->WhereRaw($whereStr);
        }
      
        // if (@$filter['time_period'] != '' && count($filter['time_period'])) {
        //     $whereStr = " (p.created_at >= '" . $filter['time_period']['start_date'] . " 00:00:00' and p.created_at <= '" . $filter['time_period']['end_date'] . " 23:59:59')";
        //     $record->WhereRaw($whereStr);
        // }
        if (@$filter['time_period'] != '' && count($filter['time_period'])) {
            // $whereStr = " (p.created_at >= '" . date('Y-m-d H:i:s.u', strtotime($filter['time_period']['start_date'])) . "' and p.created_at <= '" . date('Y-m-d H:i:s.u', strtotime($filter['time_period']['end_date'])) . "')";
            // $record->WhereRaw($whereStr);
            $record->whereBetween('p.created_at', [$filter['time_period']['fromdate'], str_replace("000000", "999999", $filter['time_period']['enddate'])]);
        }

        if (@$filter['time_period_actioned_at'] != '' && count($filter['time_period_actioned_at'])) {
            $record->where('status', '!=','opened')->whereBetween('p.updated_at', [$filter['time_period_actioned_at']['fromdate'], str_replace("000000", "999999", $filter['time_period_actioned_at']['enddate'])]);
        }

        $count = $record->count();
        $totalAMount = $record->sum('p.amount');
        if(array_key_exists('download', $filter)) {
            $result = $record->orderBy(@$filter['sort_by'], @$filter['order'])->distinct(@$filter['sort_by'])->get();
        }
        else {
            $result = $record->orderBy(@$filter['sort_by'], @$filter['order'])->forPage($offset, $limit)->distinct(@$filter['sort_by'])->get();
        }
//       $quries = DB::getQueryLog();

        

//        du($quries);
        return ['data' => $result, 'count' => @$count , 'totalDeposit'=>$totalAMount];
    }

    /**
     * @description getWallet
     * @param $target_wallete_id
     * @param $source_wallete_id
     * @return array
     */
    public static function getWallet($target_wallete_id, $source_wallete_id)
    {
        $sqlCheck = "SELECT 
                        *
                        FROM wallets as u
                        where u.id = ?;
                                              
                        ";

        $targetWallet = DB::select($sqlCheck, [$target_wallete_id]);

        if ($targetWallet) {
            $targetWallet = $targetWallet[0];
        }

        $sourceWallet = DB::select($sqlCheck, [$source_wallete_id]);

        if ($sourceWallet) {
            $sourceWallet = $sourceWallet[0];
        }

        return ["targetWallet" => $targetWallet, "sourceWallet" => $sourceWallet];
    }

    /**
     * @description getDepositNoCash
     * @param $id
     * @param $non_cash_amount
     * @return int
     */
    public static function getDepositNoCash($id, $non_cash_amount)
    {

            $createDeposit = Wallets::where('id', $id)->lockForUpdate()->first();
            $createDeposit->refresh();
            $newAmount = $createDeposit->non_cash_amount + $non_cash_amount;
            $createDeposit->non_cash_amount = $newAmount; 
            $createDeposit->save();

            return $createDeposit;
    }
    
    /**
     * @description getWithdrawNoCash
     * @param $id
     * @param $non_cash_amount
     * @return int
     */
    public static function getWithdrawNoCash($id, $non_cash_amount)
    {
        $createWithdraw = Wallets::where('id', $id)
        ->where('non_cash_amount', '>=', $non_cash_amount)
        ->lockForUpdate()->first();

        if($createWithdraw){
            $createWithdraw->refresh();
            $newAmount = $createWithdraw->non_cash_amount - $non_cash_amount;
            $createWithdraw->non_cash_amount = $newAmount; 
            $createWithdraw->save();
        }
        return $createWithdraw;
    }

    /**
     * @description getDepositAmount
     * @param $id
     * @param $type
     * @param string $userType
     * @return array
     */
    public static function getDepositAmount($id, $type, $userType = '')
    {
        $result = [];

        if ($userType != '')
            $walletId = getWalletDetails($id, $userType);
        else
            $walletId = getWalletDetails($id);

        $that = new self();
        if (@$walletId[0]->id) {
            $sql = "select t.amount as last_deposited_amount, t.updated_at from {$that->table} as t 
            where t.transaction_type = ? and t.target_wallet_id = ? order by t.id desc limit 1";

            $result = DB::select($sql, [$type, @$walletId[0]->id]);
        }
        return $result;
    }

    public static function getDepositAmountUser($id, $type, $userType = '')
    {
        $result = [];

        if ($userType != '')
            $walletId = getWalletDetails($id, $userType);
        else
            $walletId = getWalletDetails($id);

        $that = new self();
        if (@$walletId[0]->id) {
            $sql = "select t.amount as last_deposited_amount, t.updated_at from {$that->table} as t 
            where t.transaction_type in ($type,5) and t.target_wallet_id = ? order by t.id desc limit 1";

            $result = DB::select($sql, [ @$walletId[0]->id]);
        }
        return $result;
    }

    /* Elastic search data */
    /**
     * @description storeElasticSearchData
     * @param $transaction_id
     */
    public static function storeElasticSearchData($transaction_id,$rolloverAmount=0)
    {

        $Obj = new self();

        $txnInfo = $Obj->getTransactionInfo($transaction_id);

        if(!$txnInfo){
            return;
        }
        $actionee = [];
        if ($txnInfo->actionee_type != '') {
            $actionee = $Obj->getActioneeInfo($txnInfo->actionee_type, $txnInfo->actionee_id, $txnInfo->tenant_id);
        }

        $source_wallet = $target_wallet = $player_details = $gameProvider=[];
        $currenBalance = 0;
        if ($txnInfo->source_wallet_id != '') {
            $source_wallet = $Obj->walletOwnerInfo($txnInfo->source_wallet_id,$txnInfo->tenant_id);

            if (@$source_wallet->type == 'User') {
                $player_details = $Obj->getPlayerDetails($source_wallet->owner_id, $txnInfo->tenant_id);
                @$player_details->before_balance= @$txnInfo->source_before_balance??"0.00";
                @$player_details->after_balance= @$txnInfo->source_after_balance??"0.00";
                $currenBalance = $txnInfo->source_after_balance;
            }
            @$source_wallet->id=@$source_wallet->owner_id;
            @$source_wallet->before_balance = @$txnInfo->source_before_balance??"0.00";
            @$source_wallet->after_balance = @$txnInfo->source_after_balance??"0.00";
            unset($source_wallet->owner_id);
        }

        if (@$txnInfo->target_wallet_id != '') {
            $target_wallet = $Obj->walletOwnerInfo($txnInfo->target_wallet_id,$txnInfo->tenant_id);

            if($target_wallet) {
                if ($target_wallet->type == 'User') {
                    $player_details = $Obj->getPlayerDetails($target_wallet->owner_id, $txnInfo->tenant_id);
                    @$player_details->before_balance = @$txnInfo->target_before_balance;
                    @$player_details->after_balance = @$txnInfo->target_after_balance;
                    $currenBalance = $txnInfo->target_after_balance;
                }
                $target_wallet->id = $target_wallet->owner_id;
                $target_wallet->before_balance = @$txnInfo->target_before_balance??"0.00";
                $target_wallet->after_balance = @$txnInfo->target_after_balance??"0.00";
                unset($target_wallet->owner_id);
            }
        }

        if(!empty($player_details)){
            $player_details->self_exclusion = null;
            $player_details->active = $player_details->active?'true':'false';
            $player_details->demo = $player_details->demo?'true':'false';
            $player_details->amount = (float)0;
            $player_details->non_cash_amount = (float)0;


            $balance_difference = @$player_details->after_balance - @$player_details->before_balance;
            $player_details->revenue = $balance_difference;
            $userConditionArray = [
                'bet', 'win', 'refund', 'tip', 'deposit', 'withdraw', 'withdraw_cancel', 'deposit_bonus_claim',
                'exchange_place_bet_cash_debit', 'exchange_refund_cancel_bet_cash_debit', 'exchange_refund_market_cancel_cash_debit',
                'exchange_settle_market_cash_debit', 'exchange_resettle_market_cash_debit', 'exchange_cancel_settled_market_cash_debit',
                'exchange_place_bet_cash_credit', 'exchange_refund_cancel_bet_cash_credit', 'exchange_refund_market_cancel_cash_credit',
                'exchange_settle_market_cash_credit', 'exchange_resettle_market_cash_credit', 'exchange_cancel_settled_market_cash_credit',
                'exchange_deposit_bonus_claim'
            ];
            if(in_array(getTransactionTypeString($txnInfo->transaction_type), $userConditionArray)){
                $player_details->amount = (float)abs($currenBalance);
                $player_details->non_cash_amount = (float)0;
            }else{
                $player_details->amount = (float)0;
                $player_details->non_cash_amount = (float)abs($currenBalance);
            }

            $DEDUCTED_FROM_WALLET_TYPES = [
                'bet', 'bet_non_cash',
                'withdraw', 'tip',
                'tip_non_cash','joining_bonus_claimed',
                'exchange_place_bet_non_cash_debit', 'exchange_place_bet_cash_debit', 'exchange_refund_cancel_bet_cash_debit',
                'exchange_refund_cancel_bet_non_cash_debit', 'exchange_refund_market_cancel_non_cash_debit', 'exchange_refund_market_cancel_cash_debit',
                'exchange_settle_market_cash_debit', 'exchange_resettle_market_cash_debit', 'exchange_cancel_settled_market_cash_debit'
            ];
            if(in_array(getTransactionTypeString($txnInfo->transaction_type), $DEDUCTED_FROM_WALLET_TYPES)){
                $player_details->deducted_amount = (float)abs($balance_difference);
            }else{
                $player_details->deducted_amount = 0.0;
            }

            $ADDED_TO_WALLET_TYPES = [
                'win', 'win_non_cash',
                'refund', 'refund_non_cash',
                'withdraw_cancel',
                'deposit', 'deposit_bonus_claim','joining_bonus_claimed',
                'exchange_place_bet_cash_credit', 'exchange_refund_cancel_bet_non_cash_credit', 'exchange_refund_market_cancel_non_cash_credit',
                'exchange_refund_cancel_bet_cash_credit', 'exchange_refund_market_cancel_cash_credit', 'exchange_settle_market_cash_credit',
                'exchange_resettle_market_cash_credit', 'exchange_cancel_settled_market_cash_credit', 'exchange_deposit_bonus_claim'
            ];
            if(in_array(getTransactionTypeString($txnInfo->transaction_type), $ADDED_TO_WALLET_TYPES)){
                $player_details->added_amount = (float)abs($balance_difference);
            }else{
                $player_details->added_amount = 0.0;
            }

            if ($txnInfo->transaction_type == 3 || $txnInfo->transaction_type == 12) {

                if ($txnInfo->transaction_type == 12) {

                    $player_details->deposit_bonus_details = [

                        /* const userActiveDepositBonus = await UserBonus.findOne({
                            where: {
                                                status: BONUS_STATUS.ACTIVE,
                              kind: BONUS_TYPES.DEPOSIT,
                              userId: post.actioneeId
                            },
                            raw: true
                          })*/
                        "unconverted_active_deposit_bonus" => (float)$txnInfo->amount,
                          "active_deposit_bonus_remaining_rollover" => (float)$rolloverAmount
                        ];
                }

            }

        }
        /*if(!$actionee){
            return;
            throw new Exception("actionee id Not found");
        }*/
        $gameProvider = CasinoGames::getGameProviderDetailsByGameId($txnInfo->game_id,$txnInfo->provider_id);
        $casino_providers_name = null;
        $gameProviderName = null;
        if($gameProvider){
            $casino_providers_name = @$gameProvider->casino_providers_name;
            $gameProviderName = @$gameProvider->name;
        }
        if($casino_providers_name && $casino_providers_name==ST8_PROVIDER){
            $roundId = -9999;
        }else{
            $roundId = $txnInfo->round_id ? (int)$txnInfo->round_id: null;
        }
        $user = new User();
        $parentChainIds=[] ;
        if($txnInfo->actionee_type=="User"){
            $userParentIds = $user->getUserParentids($txnInfo->actionee_id, $txnInfo->tenant_id);
            $parentChainIds = $userParentIds['ids']??[];
        }
        if($txnInfo->actionee_type=="AdminUser"){
            $param = array("id"=>$txnInfo->actionee_id,"tenant_id"=>$txnInfo->tenant_id);
            $parentChainIds = $Obj->getAdminParentDetails($param);
        }
        $data = array(
            'internal_tracking_id' => $transaction_id,
            'tenant_id' => $txnInfo->tenant_id,
            'actionee_id' => $txnInfo->actionee_id,
            'actionee' => $actionee,
            'source_wallet_owner' => $source_wallet,
            'target_wallet_owner' => $target_wallet,
            'player_details' => ($player_details?$player_details:null),
            'target_currency' => $txnInfo->target_currency_code,
            'source_currency' => $txnInfo->source_currency_code,
            'transaction_type' => getTransactionTypeString($txnInfo->transaction_type),
            'transaction_id' => $txnInfo->transaction_id,
            'amount' => (float)$txnInfo->amount,
            'conversion_rate' => (float)$txnInfo->conversion_rate,
            'status' => $txnInfo->status,
            'description' => $txnInfo->comments,
            'created_at' => getTimeZoneWiseTimeISO($txnInfo->created_at),
            'game_provider' => $casino_providers_name,
            'game_type' =>  $gameProviderName,
            'table_id' => strval($txnInfo->table_id),
            'round_id' => $roundId,
            'bonus_id' => null,
            'round_id_s' => strval($txnInfo->round_id),
            'gp_error_code' => null,
            'internal_error_code' => $txnInfo->error_code,
            'internal_error_description' => $txnInfo->error_description,
            'type' => 'financial',
            'transfer_method' => $txnInfo->payment_method,
            'amount_in_currencies' => ($txnInfo->other_currency_amount != "" ? json_decode($txnInfo->other_currency_amount,true) : []),
            'agent_parent_chain_ids' => $parentChainIds,
            'custom_1' => '',
            'custom_2' => '',
            'custom_3' => '',
            'custom_4' => '',
            'custom_5' => '',
            'custom_6' => '',
            'custom_7' => '',
            'custom_8' => '',
            'custom_9' => '',
            'custom_10' => ''
        );

        // $configurations = TenantConfigurations::select('allowed_currencies')->where('tenant_id', $txnInfo->tenant_id)->get();
        // $configurations = $configurations->toArray();
        // $CurrenciesValue=[];
        // if ($configurations) {

        //     $configurations = $configurations[0];
        //     if (strlen($configurations['allowed_currencies']) > 1) {
        //         $configurations = str_replace('{', '', $configurations['allowed_currencies']);
        //         $configurations = str_replace('}', '', $configurations);
        //         $configurations = explode(',', $configurations);

        //         if (@$configurations[0] && count($configurations) > 0) {
        //             $CurrenciesValue = Currencies::select('code')->whereIn('id', $configurations)->pluck('code');
        //             $CurrenciesValue = $CurrenciesValue->toArray();
        //         }
        //     }
        // }
        // $currenciesTempArray = $data['amount_in_currencies'] = [];

        // $exMainTransactionCurrecy = '';
        // if($txnInfo->source_wallet_id != ''){
        //     $exMainTransactionCurrecy = $txnInfo->source_currency_code;
        // }else{
        //     $exMainTransactionCurrecy = $txnInfo->target_currency_code;
        // }



        // $amount_currency_ex = Currencies:: getExchangeRateCurrency($exMainTransactionCurrecy);
        // foreach ($CurrenciesValue as $key => $value) {
        //     if($exMainTransactionCurrecy != $value){
        //         $getExchangeRateCurrency = Currencies:: getExchangeRateCurrency($value);
        //         $currenciesTempArray[$value] = (float)$data['amount'] * ($amount_currency_ex / $getExchangeRateCurrency);
        //     }else{
        //         $currenciesTempArray[$value] = (float)$data['amount'];
        //     }

        // }
        // $data['amount_in_currencies'] = $currenciesTempArray;



/*        echo '<pre>';
        print_r($data);
        echo '<HR> </pre>';*/

        $params = [
            'body' => $data,
            'index' => ELASTICSEARCH_INDEX['transactions'],
            'type' => ELASTICSEARCH_TYPE,
            'id' => $transaction_id,
        ];


        
        $client = getEsClientObj();
        $client->index($params);
        unset($obj,$client);
        return true;
    }
   
   
   
    public static function storeElasticSearchSuperAdminTransactionData($transaction_id,$type='deposit')
    {

        $Obj = new self();

        $txnInfo = $Obj->getTransactionInfo($transaction_id);
        
        
        if(!$txnInfo){
            return;
        }
        $actionee = [];
        if ($txnInfo->actionee_type != '') {
            $actionee = $Obj->getActioneeInfo($txnInfo->actionee_type, $txnInfo->actionee_id, $txnInfo->tenant_id);
        }
        // dd($actionee);
        $target_wallet = $player_details = $gameProvider=[];

        $sourceWallet = @Admin::getSuperAdminWalletsDetails(@Auth::user()->id);

        if (@$txnInfo->target_wallet_id != '') {
          if($type == "deposit"){
            $target_wallet = $Obj->superAdminWalletOwnerInfo($txnInfo->target_wallet_id,$txnInfo->tenant_id);
          }else{
            $target_wallet = $Obj->walletOwnerInfo($txnInfo->target_wallet_id,$txnInfo->tenant_id);
          }

            if($target_wallet) {
                if ($target_wallet->type == 'User') {
                    $player_details = $Obj->getPlayerDetails($target_wallet->owner_id, $txnInfo->tenant_id);
                    @$player_details->before_balance = @$txnInfo->target_before_balance;
                    @$player_details->after_balance = @$txnInfo->target_after_balance;
                }
                $target_wallet->id = $target_wallet->owner_id;
                unset($target_wallet->owner_id);
            }
        }
        if(!empty($player_details)){
            $player_details->self_exclusion = null;
            $player_details->active = $player_details->active?'true':'false';
            $player_details->demo = $player_details->demo?'true':'false';
            $player_details->amount = (float)0;
            $player_details->non_cash_amount = (float)0;


            $balance_difference = @$player_details->after_balance - @$player_details->before_balance;
            $player_details->revenue = $balance_difference;
            $userConditionArray = ['bet',
                'win', 'refund', 'tip', 'deposit', 'withdraw', 'withdraw_cancel', 'deposit_bonus_claim'];
            if(in_array(getTransactionTypeString($txnInfo->transaction_type), $userConditionArray)){
                $player_details->amount = (float)abs($balance_difference);
                $player_details->non_cash_amount = (float)0;
            }else{
                $player_details->amount = (float)0;
                $player_details->non_cash_amount = (float)abs($balance_difference);
            }

            $DEDUCTED_FROM_WALLET_TYPES = [
                'bet', 'bet_non_cash',
                'withdraw', 'tip',
                'tip_non_cash','joining_bonus_claimed'
            ];
            if(in_array(getTransactionTypeString($txnInfo->transaction_type), $DEDUCTED_FROM_WALLET_TYPES)){
                $player_details->deducted_amount = (float)abs($balance_difference);
            }else{
                $player_details->deducted_amount = 0.0;
            }

            $ADDED_TO_WALLET_TYPES = [
                'win', 'win_non_cash',
                'refund', 'refund_non_cash',
                'withdraw_cancel',
                'deposit', 'deposit_bonus_claim','joining_bonus_claimed'
            ];
            if(in_array(getTransactionTypeString($txnInfo->transaction_type), $ADDED_TO_WALLET_TYPES)){
                $player_details->added_amount = (float)abs($balance_difference);
            }else{
                $player_details->added_amount = 0.0;
            }

            if ($txnInfo->transaction_type == 3 || $txnInfo->transaction_type == 12) {

                if ($txnInfo->transaction_type == 12) {

                    $player_details->deposit_bonus_details = [

                        /* const userActiveDepositBonus = await UserBonus.findOne({
                            where: {
                                                status: BONUS_STATUS.ACTIVE,
                              kind: BONUS_TYPES.DEPOSIT,
                              userId: post.actioneeId
                            },
                            raw: true
                          })*/
                        "unconverted_active_deposit_bonus" => (float)$txnInfo->amount,
                          "active_deposit_bonus_remaining_rollover" => 0
                        ];
                }

            }

        }
        $data = array(
            'internal_tracking_id' => $transaction_id,
            'tenant_id' => $txnInfo->tenant_id,
            'actionee_id' => $txnInfo->actionee_id,
            'actionee' => $actionee,
            'source_wallet_owner' => $sourceWallet,
            'target_wallet_owner' => $target_wallet,
            'player_details' => ($player_details?$player_details:null),
            'target_currency' => $txnInfo->target_currency_code,
            'source_currency' => $txnInfo->source_currency_code,
            'transaction_type' => ($type == 'deposit' ? "super_admin_deposit" : "super_admin_debit"),
            'transaction_id' => $txnInfo->transaction_id,
            'amount' => (float)$txnInfo->amount,
            'conversion_rate' => (float)$txnInfo->conversion_rate,
            'status' => $txnInfo->status,
            'description' => $txnInfo->comments,
            'created_at' => getTimeZoneWiseTimeISO($txnInfo->created_at),
            'game_provider' => null,
            'game_type' =>  null,
            'table_id' => strval($txnInfo->table_id),
            'round_id' => $txnInfo->round_id,
            'bonus_id' => null,
            'round_id_s' => strval($txnInfo->round_id),
            'gp_error_code' => null,
            'internal_error_code' => $txnInfo->error_code,
            'internal_error_description' => $txnInfo->error_description,
            'type' => 'financial',
            'transfer_method' => $txnInfo->payment_method,

        );


/*        echo '<pre>';
        print_r($data);
        echo '<HR> </pre>';*/

        $params = [
            'body' => $data,
            'index' => ELASTICSEARCH_INDEX['transactions'],
            'type' => ELASTICSEARCH_TYPE,
            'id' => $transaction_id."_super",
        ];


        
        $client = getEsClientObj();
        $client->index($params);
        unset($obj,$client);
        return true;
    }

    /**
     * @description getTransactionInfo
     * @param $transaction_id
     * @return Model|null|object|static
     */
    private function getTransactionInfo($transaction_id)
    {
        $Obj = new self();

        $transactionInfo = DB::connection('read_db')->table("{$Obj->table} as t")
            ->leftjoin('wallets AS sw', 'sw.owner_id', '=', 't.source_wallet_id')
            ->leftjoin('wallets AS tw', 'tw.owner_id', '=', 't.target_wallet_id')
            ->leftJoin('currencies AS sc', 'sc.id', '=', 't.source_currency_id')
            ->leftJoin('currencies AS tc', 'tc.id', '=', 't.target_currency_id')
            ->select(['t.id', 't.payment_provider_id', 't.tenant_id', 't.actionee_id', 't.actionee_type', 't.source_wallet_id', 't.target_wallet_id', 't.target_currency_id',
                't.source_currency_id', 't.transaction_type', 't.transaction_id', 't.amount', 't.conversion_rate', 't.status', 't.comments', 't.created_at',
                't.source_after_balance',
                't.source_before_balance',
                't.target_after_balance',
                't.target_before_balance',
                't.other_currency_amount',
                't.table_id', 't.round_id', 't.error_code', 't.error_description', 't.payment_method', 't.game_id', 't.provider_id',
                'sw.owner_type as source_owner_type', 'sw.owner_id as source_owner_id',
                'tw.owner_type as target_owner_type', 'tw.owner_id as target_owner_id',
                'sc.code as source_currency_code',
                'tc.code as target_currency_code'])
            ->where('t.id', $transaction_id)
            ->first();
        return $transactionInfo;
    }

    /**
     * @description getActioneeInfo
     * @param $actionee_type
     * @param $actionee_id
     * @param $tenant_id
     * @return array|Model|null|object|static
     */
    private function getActioneeInfo($actionee_type, $actionee_id, $tenant_id)
    {

        $select = ['id','first_name', 'last_name', 'email'];
        switch ($actionee_type) {
            case 'AdminUser':
                $table_name = 'admin_users';
//                $select[] = 'agent_name';
                unset($select[0]);
                $select[] = DB::raw(' null as user_name');
                break;
            case 'User' :
                $table_name = 'users';
                $select[] = 'user_name';
                break;
            case 'SuperAdminUser':
                $table_name = 'super_admin_users';
                break;
            default:
                $table_name = '';
                break;
        }

        if ($table_name == '')
            return [];

        $actioneeInfo = DB::connection('read_db')->table($table_name)->select($select)->where('id', $actionee_id)->first();

        $user = new User();
        $parent_ids = $user->getUserParentids($actionee_id, $tenant_id);
        unset($parent_ids['data']);

        @$actioneeInfo->type = $actionee_type;
        if($parent_ids['ids']){
            $actioneeInfo->parent_chain_ids = $parent_ids['ids'];
        }else{
            if($actionee_type =='AdminUser'){
                $actioneeInfo->parent_chain_ids = [$actionee_id];
            }else{
                $actioneeInfo->parent_chain_ids = [];
            }
        }

        return $actioneeInfo;
    }

    /**
     * @description walletOwnerInfo
     * @param $wallet_id
     * @return Model|null|object|static
     */
    public function walletOwnerInfo($wallet_id, $tenant_id)
    {
        $wallet_info = DB::connection('read_db')->table('wallets as w')
            ->leftjoin('users AS u', function ($join) {
                $join->on('w.owner_id', '=', 'u.id');
                $join->on('w.owner_type', DB::raw(" 'User' "));
            })
            ->leftjoin('admin_users AS au', function ($join) {
                $join->on('w.owner_id', '=', 'au.id');
                $join->on('w.owner_type', DB::raw(" 'AdminUser' "));
            })
            ->leftjoin('super_admin_users AS su', function ($join) {
              $join->on('w.owner_id', '=', 'su.id');
              $join->on('w.owner_type', DB::raw(" 'SuperAdminUser' "));
          })
            ->select(DB::raw('w.id, w.owner_type as type, w.owner_id, COALESCE (u.email, au.email,su.email)  as email, COALESCE (u.first_name, au.first_name,su.first_name) as first_name, COALESCE(u.last_name, au.last_name,su.last_name)  as last_name, u.user_name'))
            ->where('w.id', $wallet_id)
            ->first();
        if($wallet_info) {
            $user = new User();

            $parent_ids = $user->getUserParentids($wallet_info->owner_id, $tenant_id);
            unset($parent_ids['data']);
            unset($wallet_info->tenant_id);

            if ($parent_ids['ids']) {
                $wallet_info->parent_chain_ids = $parent_ids['ids'];
            } else {
                $wallet_info->parent_chain_ids = [$wallet_info->owner_id];

            }
        }
        return $wallet_info;
    }
   
   
    public function superAdminWalletOwnerInfo($wallet_id, $tenant_id)
    {
        $wallet_info = DB::table('wallets as w')
            ->leftjoin('super_admin_users AS u', function ($join) {
                $join->on('w.owner_id', '=', 'u.id');
                $join->on('w.owner_type', DB::raw(" 'SuperAdminUser' "));
            })
            ->select(DB::raw('w.id, w.owner_type as type, w.owner_id, COALESCE (u.email)  as email, COALESCE (u.first_name) as first_name, COALESCE(u.last_name)  as last_name'))
            ->where('w.id', $wallet_id)
            ->first();
        if($wallet_info) {
            $user = new User();

            $parent_ids = $user->getUserParentids($wallet_info->owner_id, $tenant_id);
            unset($parent_ids['data']);
            unset($wallet_info->tenant_id);

            if ($parent_ids['ids']) {
                $wallet_info->parent_chain_ids = $parent_ids['ids'];
            } else {
                $wallet_info->parent_chain_ids = [$wallet_info->owner_id];

            }
        }
        return $wallet_info;
    }
    
    
    static function getSuperAdminTransaction($limit = 10, $offset = 0,$filter)
    {

      if ($offset == 1) {
        $offset = 0;
      }
        $transaction = DB::table('transactions as p')
            ->select('p.*','au.first_name','au.last_name','au.email',DB::raw(
              "(
                SELECT concat(au.first_name, ' ', au.last_name, ' - ', au.email) 
        FROM admin_users as au where au.id=(
        
                SELECT w.owner_id
        FROM wallets as w
        WHERE w.id = p.target_wallet_id and w.owner_type ='AdminUser')
        
                )as target_wallet_name_admin,
         (
                SELECT concat(au.first_name, ' ', au.last_name, ' - ', au.email) 
        FROM super_admin_users as au where au.id=(
        
                SELECT w.owner_id
        FROM wallets as w
        WHERE w.id = p.target_wallet_id and w.owner_type ='SuperAdminUser')
        
                )as target_wallet_name_super_admin,
                (
                SELECT concat(u.first_name, ' ', u.last_name, ' - ', u.email) 
            FROM users as u where u.id=(
        
                SELECT w1.owner_id
                FROM wallets as w1
                WHERE w1.id = p.target_wallet_id 
                and w1.owner_type ='User')
                ) as target_wallet_name,
              
                (
                SELECT concat(au.first_name, ' ', au.last_name, ' - ', au.email) 
        FROM admin_users as au where au.id=(
        
                SELECT w.owner_id
        FROM wallets as w
        WHERE w.id = p.source_wallet_id and w.owner_type ='AdminUser')
        
                )as source_wallet_name_admin,

                (
                  SELECT concat(su.first_name, ' ', su.last_name, ' - ', su.email) 
          FROM super_admin_users as su where su.id=(
          
                  SELECT w.owner_id
          FROM wallets as w
          WHERE w.id = p.source_wallet_id and w.owner_type ='SuperAdminUser')
          
                  )as source_wallet_name_super_admin,
                (
                SELECT concat(u.first_name, ' ', u.last_name, ' - ', u.email) 
            FROM users as u where u.id=(
        
                SELECT w1.owner_id
                FROM wallets as w1
                WHERE w1.id = p.source_wallet_id 
                and w1.owner_type ='User')
                ) as source_wallet_name"
            ))
            ->leftJoin('admin_users as au','au.id','=','p.tenant_id')
            ->where('p.actionee_type', @Auth::user()->parent_type)
            ->where('p.actionee_id', @Auth::user()->id)
            ->orderBy('p.created_at','desc');

            if(isset($filter['type']) && $filter['type'] != 'all'){
              if(isset($filter['type']) && $filter['type'] == '1'){
                $transaction = $transaction->where('p.tenant_id',0);
              }elseif(isset($filter['type']) && $filter['type'] == '2'){
                $transaction = $transaction->where('p.tenant_id','!=',0);
                $transaction = $transaction->where('p.transaction_type',3);
              }else{
                $transaction = $transaction->where('p.tenant_id','!=',0);
                $transaction = $transaction->where('p.transaction_type',4);
              }
            }else{
              $transaction = $transaction->whereIn('p.transaction_type', [3,4]);
            }
            
            
            if(isset($filter['name']) && $filter['name'] != ''){
              $whereStrSearch = "  u.email like '%".$filter['name']."%' OR au.email like '%".$filter['name']."%' OR su.email like '%".$filter['name']."%'";
              $whereStrSearch .= " OR  concat(u.first_name, ' ', u.last_name)  like '%".$filter['name']."%' OR concat(au.first_name, ' ', au.last_name) like '%".$filter['name']."%' OR concat(su.first_name, ' ', su.last_name) like '%".$filter['name']."%'";
              $wallet_info = DB::table('wallets as w')
                  ->leftjoin('users AS u', function ($join) {
                      $join->on('w.owner_id', '=', 'u.id');
                      $join->on('w.owner_type', DB::raw(" 'User' "));
                  })
                  ->leftjoin('admin_users AS au', function ($join) {
                      $join->on('w.owner_id', '=', 'au.id');
                      $join->on('w.owner_type', DB::raw(" 'AdminUser' "));
                  })
                  ->leftjoin('super_admin_users AS su', function ($join) {
                      $join->on('w.owner_id', '=', 'su.id');
                      $join->on('w.owner_type', DB::raw(" 'SuperAdminUser' "));
                  })
                  ->whereRaw($whereStrSearch)
                  ->pluck('w.id')->toArray();
                  // dd($wallet_info);
                  if ($wallet_info){
                    $whereStr = " (p.source_wallet_id  in (" .implode(',',$wallet_info). ") OR  p.target_wallet_id  in (" .implode(',',$wallet_info) . ") )";
                  }else{
                    $whereStr = " (p.source_wallet_id  = 0 AND  p.target_wallet_id  = 0 )";
                  }
                   
                  $transaction = $transaction->WhereRaw($whereStr);
            }


        $count = $transaction->count();
        $result = $transaction->forPage($offset, $limit)->get();
        // dd($count);
        
        
        if($result) {
          
          foreach ($result as $key => $value) {
              if($value->tenant_id != 0){

                if($value->transaction_type == 3){
                  $result[$key]->type = 'Debit';
                  $result[$key]->transfer_type = 'Tenant Wallet Deposit';
                }else{
                  $result[$key]->type = 'Withdrawal';
                  $result[$key]->transfer_type = 'Tenant Wallet Withdrawal';
                } 

              }else{
                $result[$key]->type = 'Deposit';
                $result[$key]->transfer_type = 'Super Admin Wallet Deposit';
              }

               
          }
 
        }
        return ['data' => $result, 'count' => @$count];
    }

    /**
     * @description getPlayerDetails
     * @param $player_id
     * @param $tenant_id
     * @return Model|null|object|static
     */
    public function getPlayerDetails($player_id, $tenant_id)
    {
        $user = new User();
        $parentInfo=[] ;
        $userInfo = $user->getUserInfo($player_id);
        $commission='000.00';
        if (!empty($userInfo)) {
            $userParentIds = $user->getUserParentids($player_id, $tenant_id);

            $userInfo->parent_chain_ids = $userParentIds['ids']??[];
            $userInfo->player_id = $player_id;
            $userInfo->player_id_s = strval($player_id);
            // $userInfo->player_name = $userInfo->first_name ." ".$userInfo->last_name;
            $userInfo->player_name = $userInfo->user_name;
            $userInfo->demo = false;
            $userInfo->agent_full_name= $userInfo->agent_first_name." ".$userInfo->agent_last_name;

            if(!empty($userParentIds['data'])) {
                foreach($userParentIds['data'] as $val) {
                    $commissionPercentage = AdminUserSetting::where('admin_user_id',$val->id)->get('value')->first();
                    $commission='000.00';
                    if(!is_null($commissionPercentage)){
                        $commissionPercentage=$commissionPercentage->toArray();
                        $commission=number_format($commissionPercentage['value'],2);
                        $commissionCheck=explode('.',$commission);
                        if(is_array($commissionCheck)){
                            if(strlen($commissionCheck[0])==1){
                                $commissionCheck[0]=sprintf('%03d', $commissionCheck[0]);
                            }elseif (strlen($commissionCheck[0])==2){
                                $commissionCheck[0]=sprintf('%03d', $commissionCheck[0]);
                            }
                        }
                        $commission =implode('.',$commissionCheck);
                    }
                    if($val->type == 'AdminUser') {
                        $parentInfo[] = $val->id."-".$val->agent_name."-".$userInfo->currency."-".$commission;
                    }
                }
            }
            $userInfo->parent_chain_detailed = $parentInfo??[];
        }
        return $userInfo;

    }
    public static function storeElasticBulkCasinoTransactionData($transactions,$rolloverAmount=0)
    {
        $bulkActions = [];
       
        foreach ($transactions as $transaction) {
            
            $Obj = new self();
            
            $txnInfo = $Obj->getTransactionInfo($transaction->id);
            if(!$txnInfo){
                return;
            }
            $actionee = [];
            if ($txnInfo->actionee_type != '') {
                $actionee = $Obj->getActioneeInfo($txnInfo->actionee_type, $txnInfo->actionee_id, $txnInfo->tenant_id);
            }
    
            $source_wallet = $target_wallet = $player_details = $gameProvider=[];
            $currenBalance = 0;
            if ($txnInfo->source_wallet_id != '') {
                $source_wallet = $Obj->walletOwnerInfo($txnInfo->source_wallet_id,$txnInfo->tenant_id);
    
                if (@$source_wallet->type == 'User') {
                    $player_details = $Obj->getPlayerDetails($source_wallet->owner_id, $txnInfo->tenant_id);
                    @$player_details->before_balance= @$txnInfo->source_before_balance??"0.00";
                    @$player_details->after_balance= @$txnInfo->source_after_balance??"0.00";
                    $currenBalance = (float)$txnInfo->source_after_balance;
                }
                @$source_wallet->id=@$source_wallet->owner_id;
                @$source_wallet->before_balance=@$txnInfo->source_before_balance??"0.00";
                @$source_wallet->after_balance=@$txnInfo->source_after_balance??"0.00";
                unset($source_wallet->owner_id);
            }
    
            if (@$txnInfo->target_wallet_id != '') {
                $target_wallet = $Obj->walletOwnerInfo($txnInfo->target_wallet_id,$txnInfo->tenant_id);
    
                if($target_wallet) {
                    if ($target_wallet->type == 'User') {
                        $player_details = $Obj->getPlayerDetails($target_wallet->owner_id, $txnInfo->tenant_id);
                        @$player_details->before_balance = @$txnInfo->target_before_balance;
                        @$player_details->after_balance = @$txnInfo->target_after_balance;
                        $currenBalance = (float)$txnInfo->target_after_balance;
                    }
                    $target_wallet->id = $target_wallet->owner_id;
                    $target_wallet->before_balance = @$txnInfo->target_before_balance??"0.00";
                    $target_wallet->after_balance = @$txnInfo->target_after_balance??"0.00";
                    unset($target_wallet->owner_id);
                }
            }
    
            if(!empty($player_details)){
                $player_details->self_exclusion = null;
                $player_details->active = $player_details->active?'true':'false';
                $player_details->demo = $player_details->demo?'true':'false';
                $player_details->amount = (float)0;
                $player_details->non_cash_amount = (float)0;
    
    
                $balance_difference = @$player_details->after_balance - @$player_details->before_balance;
                $player_details->revenue = $balance_difference;
                $userConditionArray = [
                    'bet', 'win', 'refund', 'tip', 'deposit', 'withdraw', 'withdraw_cancel', 'deposit_bonus_claim',
                    'exchange_place_bet_cash_debit', 'exchange_refund_cancel_bet_cash_debit', 'exchange_refund_market_cancel_cash_debit',
                    'exchange_settle_market_cash_debit', 'exchange_resettle_market_cash_debit', 'exchange_cancel_settled_market_cash_debit',
                    'exchange_place_bet_cash_credit', 'exchange_refund_cancel_bet_cash_credit', 'exchange_refund_market_cancel_cash_credit',
                    'exchange_settle_market_cash_credit', 'exchange_resettle_market_cash_credit', 'exchange_cancel_settled_market_cash_credit',
                    'exchange_deposit_bonus_claim'
                ];
                if(in_array(getTransactionTypeString($txnInfo->transaction_type), $userConditionArray)){
                    $player_details->amount = (float)abs($currenBalance);
                    $player_details->non_cash_amount = (float)0;
                }else{
                    $player_details->amount = (float)0;
                    $player_details->non_cash_amount = (float)abs($currenBalance);
                }
    
                $DEDUCTED_FROM_WALLET_TYPES = [
                    'bet', 'bet_non_cash',
                    'withdraw', 'tip',
                    'tip_non_cash','joining_bonus_claimed',
                    'exchange_place_bet_non_cash_debit', 'exchange_place_bet_cash_debit', 'exchange_refund_cancel_bet_cash_debit',
                    'exchange_refund_cancel_bet_non_cash_debit', 'exchange_refund_market_cancel_non_cash_debit', 'exchange_refund_market_cancel_cash_debit',
                    'exchange_settle_market_cash_debit', 'exchange_resettle_market_cash_debit', 'exchange_cancel_settled_market_cash_debit'
                ];
                if(in_array(getTransactionTypeString($txnInfo->transaction_type), $DEDUCTED_FROM_WALLET_TYPES)){
                    $player_details->deducted_amount = (float)abs($balance_difference);
                }else{
                    $player_details->deducted_amount = 0.0;
                }
    
                $ADDED_TO_WALLET_TYPES = [
                    'win', 'win_non_cash',
                    'refund', 'refund_non_cash',
                    'withdraw_cancel',
                    'deposit', 'deposit_bonus_claim','joining_bonus_claimed',
                    'exchange_place_bet_cash_credit', 'exchange_refund_cancel_bet_non_cash_credit', 'exchange_refund_market_cancel_non_cash_credit',
                    'exchange_refund_cancel_bet_cash_credit', 'exchange_refund_market_cancel_cash_credit', 'exchange_settle_market_cash_credit',
                    'exchange_resettle_market_cash_credit', 'exchange_cancel_settled_market_cash_credit', 'exchange_deposit_bonus_claim'
                ];
                if(in_array(getTransactionTypeString($txnInfo->transaction_type), $ADDED_TO_WALLET_TYPES)){
                    $player_details->added_amount = (float)abs($balance_difference);
                }else{
                    $player_details->added_amount = 0.0;
                }
    
                if ($txnInfo->transaction_type == 3 || $txnInfo->transaction_type == 12) {
    
                    if ($txnInfo->transaction_type == 12) {
    
                        $player_details->deposit_bonus_details = [
    
                            /* const userActiveDepositBonus = await UserBonus.findOne({
                                where: {
                                                    status: BONUS_STATUS.ACTIVE,
                                  kind: BONUS_TYPES.DEPOSIT,
                                  userId: post.actioneeId
                                },
                                raw: true
                              })*/
                            "unconverted_active_deposit_bonus" => (float)$txnInfo->amount,
                              "active_deposit_bonus_remaining_rollover" => (float)$rolloverAmount
                            ];
                    }
    
                }
    
            }
            /*if(!$actionee){
                return;
                throw new Exception("actionee id Not found");
            }*/
            $gameProvider = CasinoGames::getGameProviderDetailsByGameId($txnInfo->game_id,$txnInfo->provider_id);
            $casino_providers_name = null;
            $gameProviderName = null;
            if($gameProvider){
                $casino_providers_name = @$gameProvider->casino_providers_name;
                $gameProviderName = @$gameProvider->name;
            }
            if($casino_providers_name && $casino_providers_name==ST8_PROVIDER){
                $roundId = -9999;
            }else{
                $roundId = $txnInfo->round_id ? (int)$txnInfo->round_id: null;
            }
            $user = new User();
            $parentChainIds=[] ;
            if($txnInfo->actionee_type=="User"){
                $userParentIds = $user->getUserParentids($txnInfo->actionee_id, $txnInfo->tenant_id);
                $parentChainIds = $userParentIds['ids']??[];
            }
            if($txnInfo->actionee_type=="AdminUser"){
                $param = array("id"=>$txnInfo->actionee_id,"tenant_id"=>$txnInfo->tenant_id);
                $parentChainIds = $Obj->getAdminParentDetails($param);
            }
            $paymentProviderName = null;
            if($txnInfo->payment_provider_id){
                $paymentProviderName = $transactionInfo = DB::connection('read_db')->table("payment_providers as pp")
                    ->select(['pp.id','pp.provider_name'])
                    ->where('pp.id', $txnInfo->payment_provider_id)
                    ->first();
            }

            $data = array(
                'internal_tracking_id' => (string)$transaction->id,
                'tenant_id' => $txnInfo->tenant_id,
                'actionee_id' => $txnInfo->actionee_id,
                'actionee' => $actionee,
                'source_wallet_owner' => $source_wallet,
                'target_wallet_owner' => $target_wallet,
                'player_details' => ($player_details?$player_details:null),
                'target_currency' => $txnInfo->target_currency_code,
                'source_currency' => $txnInfo->source_currency_code,
                'transaction_type' => getTransactionTypeString($txnInfo->transaction_type),
                'transaction_id' => $txnInfo->transaction_id,
                'amount' => (float)$txnInfo->amount,
                'conversion_rate' => (float)$txnInfo->conversion_rate,
                'status' => $txnInfo->status,
                'description' => $txnInfo->comments,
                'created_at' => getTimeZoneWiseTimeISO($txnInfo->created_at),
                'game_provider' => $casino_providers_name,
                'game_type' =>  $gameProviderName,
                'table_id' => strval($txnInfo->table_id),
                'round_id' => $roundId,
                'bonus_id' => null,
                'round_id_s' => strval($txnInfo->round_id),
                'gp_error_code' => null,
                'internal_error_code' => $txnInfo->error_code,
                'internal_error_description' => $txnInfo->error_description,
                'type' => 'financial',
                'payment_provider_id' => $txnInfo->payment_provider_id,
                'payment_provider_name' => @$paymentProviderName->provider_name,
                'transfer_method' => $txnInfo->payment_method,
                'amount_in_currencies' => ($txnInfo->other_currency_amount != "" ? json_decode($txnInfo->other_currency_amount,true) : []),
                'agent_parent_chain_ids' => $parentChainIds,
                'custom_1' => '',
                'custom_2' => '',
                'custom_3' => '',
                'custom_4' => '',
                'custom_5' => '',
                'custom_6' => '',
                'custom_7' => '',
                'custom_8' => '',
                'custom_9' => '',
                'custom_10' => ''
            );
            $bulkActions[] = $data; // Add the document data directly
   
        }
       $params = ['body' => []];
       $client = getEsClientObj();
       for ($i = 0; $i < count($transactions); $i++) {
          $params['body'][] = [
               'index' => [
                   '_index' => 'transactions',//ELASTICSEARCH_INDEX['transactions'],
                   '_type' => ELASTICSEARCH_TYPE,
                   '_id'    => $bulkActions[$i]['internal_tracking_id']
               ]
           ];

           $params['body'][] = $bulkActions[$i];
           // Every 1000 documents stop and send the bulk request
           if ($i % 1000 == 0) {
            
               $responses = $client->bulk($params);

               // erase the old bulk request
               $params = ['body' => []];

               // unset the bulk response when you are done to save memory
               unset($responses);
           }
       }

       // Send the last batch if it exists
       if (!empty($params['body'])) {
           $responses = $client->bulk($params);
       }
      // echo "casino transaction re-index successfully done";
      
       
    }

    public static function  getAdminParentDetails($param = []){
        if(!$param){
            return [];
        }
        
        $parentChainIdsArray = [];
        $parent_chain_ids = Admin::getAdminUserHierarchy($param['id'],ADMIN_TYPE, $param['tenant_id']);
        foreach ($parent_chain_ids as $key => $value){
            $value = (array)$value;
            $parentChainIdsArray[] = $value['id'];
        }
        return $parentChainIdsArray;
    }

}
