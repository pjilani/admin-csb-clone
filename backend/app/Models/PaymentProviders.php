<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;

class PaymentProviders extends Authenticatable
{
    use HasFactory,HasApiTokens,HasFactory, Notifiable;
    /**
     * @var string
     */
    protected $table="payment_providers";

    /**
     * @var array
     */
    // protected $guarded = ['id'];

    /**
     * @return mixed|string
     */
    public function getAuthPassword()
    {
        return $this->encrypted_password;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'provider_name',
        'provider_keys',
        'provider_type',
        'active',
        'logo',
        'created_at',
        'updated_at',
        'currencies'
    ];

  


    public static function getList($limit = 10, $offset = 0, $filter = array(),$id = 0)
    {
        $that = new self();

        $whereStr = '';

        if ($offset == 1) {
            $offset = 0;
        }

       

        $record = DB::table($that->table, 'p')
                    ->select('*');

        if (@$filter['search'] != '') {
            $whereStr = " p.provider_name like '%".$filter['search']."%'  ";
            $record->WhereRaw($whereStr);
            $record->orWhereJsonContains("p.provider_keys" ,$filter['search']);
        }
        
        if($id != 0){
          $record->where('p.id',$id);
        }

        $count = $record->count();
        $result = $record->orderByDesc('id')->forPage($offset, $limit)->get();

        foreach ($result as $value) {
            if($value->currencies){
                $CurrenciesValue = Currencies::on('read_db')->select('id', 'name')->whereIn('id', json_decode($value->currencies))->get()->toArray();
                $value->currencyList = $CurrenciesValue;
            }else{
                $value->currencyList = [];
            }
        }

        // dd($result);
        return ['data' => $result, 'count' => @$count];
    }

}
