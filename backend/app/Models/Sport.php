<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Sport extends Model
{
    use HasFactory;

    protected $table = "pulls_sports";

    protected $fillable = ['is_deleted', 'sport_id', 'name_de', 'name_en', 'name_fr', 'name_ru', 'name_tr', 'name_nl', 'tenant_id'];

    public $timestamps = true;

    public static function getSportIds()
    {
        $that = new self();
        $result = DB::table($that->table)
            ->select('id')
            ->where('is_deleted', '=', false)
            ->get();
        return $result;
    }

}
