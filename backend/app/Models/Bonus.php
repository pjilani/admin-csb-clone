<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class Bonus extends Model
{
    use HasFactory;

    protected $table ="bonus";

    protected $fillable=[
        'code',
        'percentage',
        'enabled',
        'valid_from',
        'valid_upto',
        'kind',
        'currency_id',
        'promotion_title',
        'image',
        'terms_and_conditions',
        'vip_levels',
        'usage_count',
        'created_at',
        'updated_at',
        'tenant_id',
        'flat'
    ];

    /**
     * @param $limit
     * @param $offset
     * @param string $type
     * @param string $filter
     * @return array
     */
    public static function getListBonus($limit,$offset,$type='deposit'){

        $that=new self();
        if($offset==1){
            $offset=0;
        }
        $record = DB::table($that->table,'p');


        if($type=='deposit'){
            $record->select('p.*',
                DB::raw(" (SELECT code FROM  currencies where id=p.currency_id) as currency_name "),
                'deposit_bonus_settings.min_deposit',
                'deposit_bonus_settings.max_deposit',
                'deposit_bonus_settings.max_bonus',
                'deposit_bonus_settings.rollover_multiplier',
                'max_rollover_per_bet',
                'valid_for_days'
            );
            $record->join('deposit_bonus_settings','deposit_bonus_settings.bonus_id','=','p.id');


        }elseif($type=='losing'){
            $record->select('p.*','losing_bonus_settings.claim_days', 'losing_bonus_settings.id as settings_id',
            DB::raw(" (SELECT code FROM  currencies where id=p.currency_id) as currency_name ") );
            $record->join('losing_bonus_settings','losing_bonus_settings.bonus_id','=','p.id');
            // $record->join('losing_bonus_tiers','losing_bonus_tiers.losing_bonus_setting_id','=','losing_bonus_settings.id');
        }

        $record = $record->addSelect('p.*', DB::raw("(SELECT count(id) FROM user_bonus where p.id=user_bonus.bonus_id and ( user_bonus.status='claimed' or user_bonus.status='active' )) AS total_used"));

        if(@$type!='') {
            if(@$type == 'deposit') {
                $whereStr ="( p.kind = 'deposit' or p.kind = 'deposit_sport' )";
                $record->WhereRaw($whereStr);
            } else {
                $whereStr =" p.kind = '".$type."'";
                $record->WhereRaw($whereStr);
            }
        }

        $record->where('p.tenant_id','=',Auth::user()->tenant_id);
        $count=$record->count();
        $result=$record->orderByDesc('p.id')->forPage($offset,$limit)->get();
        if($type=='losing'){
            foreach($result as $res) {
                $res->losing_bonus_tier = DB::table('losing_bonus_tiers')->select('min_losing_amount', 'percentage')->where('losing_bonus_setting_id', $res->settings_id)->get();
            }
        }

        return ['data'=>$result,'count'=>@$count];
    }
}
