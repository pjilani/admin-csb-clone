<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WithdrawRequest extends Model
{
    use HasFactory;

    protected $table='withdraw_requests';

    protected $fillable = [
        "status",
        "remark",
        "withdrawal_type",
        "verify_status",
        "bank_id",
        "bank_name",
        "fd_transaction_id",
        "payment_transaction_id",
        "mode",
        "actioned_at",
        "payment_provider_name"
    ];

}
