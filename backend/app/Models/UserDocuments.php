<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserDocuments extends Model
{
    use HasFactory;

    protected $table="user_documents";

    protected $fillable=[
        "user_id",
        "document_url",
        "document_name",
        "is_verified",
        "status",
        "created_at",
        "updated_at",
        "reason"
    ];
}
