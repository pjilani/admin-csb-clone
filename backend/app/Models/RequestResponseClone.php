<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequestResponseClone extends Model
{
    use HasFactory;

    /**
    * @var string
    */
   protected $table="request_response_logs";

   protected $connection = 'read_db';

   /**
    * @var array
    */
   protected $fillable = [
       'request_json',
       'response_json',
       'service',
       'url',
       'tenant_id',
       'response_code',
       'response_status',
       'error_code'
   ];
}
