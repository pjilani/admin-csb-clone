<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Meta extends Model
{
    use HasFactory;
    protected $table="tenant_page_meta";

    protected $fillable = [
        'meta_title',
        'meta_description',
        'menu_id',
        'meta_keyword',
        'created_at',
        'updated_at',
        'tenant_id',
        'active'
    ];

    static function getList($tenant_id){
          $data = DB::table((new Meta)->getTable() . ' as tp');
          $data->select(['tp.*','mm.name']);
          $data->join('menu_tenant_setting as ms', 'tp.menu_id', '=', 'ms.id')->join((new MenuMaster)->getTable() . ' as mm', 'ms.menu_id', '=', 'mm.id');
          $data->where('tp.tenant_id', $tenant_id);
          $data = $data->orderBy('tp.id','DESC')->get();
          return $data;
    }
}
