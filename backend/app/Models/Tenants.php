<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tenants extends Model
{
    use HasFactory;

    protected $table = "tenants";
    protected $fillable = [
        'name',
        'domain',
        'created_at',
        'updated_at',
        'active'
    ];

    /**
     * @param int $limit
     * @param int $offset
     * @param array $filter
     * @return array
     */
    public static function getList($limit = 10, $offset = 0, $filter = array())
    {
        $that = new self();

        $whereStr = '';

        if ($offset == 1) {
            $offset = 0;
        }

        $record = DB::table($that->table, 'p');

        if (@$filter['name'] != '') {
            $whereStr = DB::raw('lower(p.name)')." like '%" . strtolower($filter['name']) . "%'";
            $record->WhereRaw($whereStr);
        }
        if (@$filter['domain'] != '') {
            $whereStr = DB::raw('lower(p.domain)')."like '%" . strtolower($filter['domain']) . "%'";
            $record->WhereRaw($whereStr);
        }

        if (@$filter['active'] != '') {
            $whereStr = " p.active = '" . $filter['active'] . "'";
            $record->WhereRaw($whereStr);
        }

        $count = $record->count();
        $result = $record->orderByDesc('id')->forPage($offset, $limit)->get();

        return ['data' => $result, 'count' => @$count];
    }

    /**
     * @param $id
     * @param string $userType
     * @return \Illuminate\Support\Collection
     */
    public static function getTenantId($id, $userType = USER_TYPE)
    {
        $tableName = 'User';
        if ($userType == ADMIN_TYPE) {
            $tableName = 'admin_users';
        }
        $result = DB::table($tableName)
            ->select('tenant_id')
            ->where('id', '=', $id)
            ->get();
        return $result;
    }

    /**
     * @param $id
     * @param string $userType
     * @return \Illuminate\Support\Collection
     */
    public static function getSuperTenantId($id, $userType = USER_TYPE)
    {
        $tableName = 'User';
        if ($userType == ADMIN_TYPE) {
            $tableName = 'admin_users';
            $result = Admin::find($id)->tenant_id;
        }else{
            $result = User::find($id)->tenant_id;
        }
        return $result;
    }
   
}

