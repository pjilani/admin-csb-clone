<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Currencies extends Model
{
    use HasFactory;
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'code',
        'primary',
        'exchange_rate',
        'currency_response_id',
        'internal_code'
    ];

    /**
     * @return mixed
     */
    public static function getPrimaryCurrency()
    {
        $that=new self();
        $result = DB::table($that->getTable())
            ->where('primary', '=', '1')
            ->get();
        return $result[0];

    }

    /**
     * @description getExchangeRateCurrency
     * @param $code
     * @return mixed
     */
    public static function getExchangeRateCurrency($code)
    {
        $that=new self();
        if($code) {
            $result = DB::table($that->getTable())
                ->select(['exchange_rate'])
                ->where('code', '=', $code)
                ->first();
            return @$result->exchange_rate;
        }else{
            return 0;
        }

    }
}
