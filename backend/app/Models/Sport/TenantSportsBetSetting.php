<?php

namespace App\Models\Sport;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TenantSportsBetSetting extends Model
{
    use HasFactory;

    protected $table = "tenant_sports_bet_setting";

    protected $fillable = [
        "tenant_id",
        "event_liability",
        "max_single_bet",
        "min_bet",
        "max_multiple_bet",
        "max_bet_on_event",
        "deposit_limit",
        "max_win_amount",
        "max_odd",
        "created_at",
        "updated_at",
        "cashout_percentage",
        "bet_disabled"
    ];
}
