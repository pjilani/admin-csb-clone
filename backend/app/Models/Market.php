<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Market extends Model
{
    use HasFactory;

    protected $table="pulls_markets";

    protected $fillable = [
        'is_deleted',
        'market_id',
        'name_de',
        'name_en',
        'name_fr',
        'name_ru',
        'name_tr',
        'name_nl'
    ];

}
