<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TenantCredentials extends Model
{
    use HasFactory;
    protected $table="tenant_credentials";

    protected $fillable = [
        'key',
        'value',
        'tenant_id',
        'description',
        'created_at',
        'updated_at'
    ];

}
