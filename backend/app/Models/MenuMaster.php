<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MenuMaster extends Model
{
  use HasFactory;
  protected $table="menu_master";

  protected $fillable = [
      'name','path','active','component','component_name','created_at','updated_at','image'
  ];
}
