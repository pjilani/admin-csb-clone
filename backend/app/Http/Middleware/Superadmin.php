<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Super;
class Superadmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        $userData     = request()->user();
        $authRecord=Super::find($userData->id)->where('email',$userData->email)->get();
        if(!$authRecord){
            return returnResponse(false,$userData->first_name." ".$userData->last_name .'account not found',[],200);
        }elseif($userData->parent_type =='SuperAdminUser' || $userData->parent_type =='Manager') {
            return $next($request);
        }else{
            return returnResponse(false,$userData->first_name." ".$userData->last_name .'account type Super Admin User not  found',[],200);
        }
    }
}
