<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Admin as AdminModel;
class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        $auth = auth()->user();
        if(!$auth->active )
        {
            return returnResponse(false, $auth->first_name." ".$auth->last_name .' account is not active', [], 401);
        }elseif($auth->parent_type =='AdminUser'){
            return $next($request);
        }else{
            return returnResponse(false,$auth->first_name." ".$auth->last_name .' account type Admin User not found', [], 404);
        }
    }
}
