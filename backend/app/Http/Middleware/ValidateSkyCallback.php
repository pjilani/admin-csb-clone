<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Models\RequestResponseLogs;
use App\Models\User;
use App\Models\WithdrawRequest;

class ValidateSkyCallback
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $headers = $request->header();
        $body = $request->all();
        $jsonRequest = [
            'headers' => $headers,
            'body' => $body,
        ];
        $signature = $request->header('hash');

        $data = DB::connection('read_db')->table('tenant_payment_configurations as tpc')->select('provider_key_values')->where('pp.provider_name', 'Sky Withdraw Provider')->leftJoin('payment_providers as pp', 'pp.id', '=', 'tpc.provider_id')->first();

        $secret_key = json_decode($data->provider_key_values)->payoutApiKey;
        $hashStr = 'Balance=' . $request->Balance . '|Bank_RefID=' . $request->Bank_RefID . '|Charge=' . $request->Charge . '|Gateway_RefID=' . $request->Gateway_RefID .'|GST=' . $request->GST . '|Merchant_RefID=' . $request->Merchant_RefID . '|StatusCode=' . $request->StatusCode .'|TDS=' . $request->TDS . '|Total_Amount=' . $request->Total_Amount . '|Transfer_Amount=' . $request->Transfer_Amount . '|TxnStatus=' . $request->TxnStatus;

        if(!$request->Balance){
            $hashStr = 'Bank_RefID=' . $request->Bank_RefID . '|Charge=' . $request->Charge . '|Gateway_RefID=' . $request->Gateway_RefID .'|GST=' . $request->GST . '|Merchant_RefID=' . $request->Merchant_RefID . '|StatusCode=' . $request->StatusCode .'|TDS=' . $request->TDS . '|Total_Amount=' . $request->Total_Amount . '|Transfer_Amount=' . $request->Transfer_Amount . '|TxnStatus=' . $request->TxnStatus;
        }
        
        $recreatedSignature = hash_hmac('sha256', $hashStr, $secret_key);
        $tenant_id = WithdrawRequest::on('read_db')->where('fd_transaction_id', $request->Merchant_RefID)->select('tenant_id')->first();
        if ($signature === strtoupper($recreatedSignature)) {
            return $next($request);
        }
        RequestResponseLogs::create([
            'request_json' => json_encode($jsonRequest),
            'response_json' => json_encode(['Error' => INVALID_SIGN]),
            'service' => SKY_SERVICE,
            'url' => $request->url(),
            'tenant_id' => $tenant_id->tenant_id,
            'response_code' => Response::HTTP_EXPECTATION_FAILED,
            'response_status' => FAILED,
            'error_code' => Response::HTTP_EXPECTATION_FAILED
        ]);
    }
}