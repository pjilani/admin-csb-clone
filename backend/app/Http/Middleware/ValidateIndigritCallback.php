<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Models\RequestResponseLogs;
use App\Models\User;

class ValidateIndigritCallback
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $payload = $request->header('x-indigrit-payload');
        $signature = $request->header('x-indigrit-signature');
        $data = DB::connection('read_db')->table('tenant_payment_configurations as tpc')->select('provider_key_values')->where('pp.provider_name', 'Indigrit Withdrawal Provider')->leftJoin('payment_providers as pp','pp.id','=','tpc.provider_id')->first();
        $secret_key = json_decode($data->provider_key_values)->secret_key;   
        $recreatedSignature = hash_hmac('sha512', $payload, $secret_key);
        $rawBody = request()->getContent();
        $json = json_decode($rawBody, true);
        $data = json_decode($payload, true);
        $tenant_id = User::on('read_db')->where('id', $request->customer_id)->select('id','tenant_id')->first()->tenant_id;
        if($signature === $recreatedSignature && $data === $json){
            return $next($request);
        }
        RequestResponseLogs::create([
            'request_json' => json_encode(['body' => $json, 'headers' => $request->header()]),
            'response_json' => json_encode(['Error' => INVALID_SIGN]),
            'service' => INDIGRIT_SERVICE ,
            'url' => $request->url(),
            'tenant_id' => $tenant_id,
            'response_code' => Response::HTTP_EXPECTATION_FAILED,
            'response_status' => FAILED,
            'error_code' => Response::HTTP_EXPECTATION_FAILED
          ]);
    }
}
