<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Api\LoginController;
use App\Models\TenantCredentials;
use Closure;
use Illuminate\Http\Client\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BlockIpMiddleware
{
   
    
  
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
      
      if(request()->user()){
        
        if(request()->user()->is_applied_ip_whitelist){
            if(request()->user()->ip_whitelist_type == 'manual'){
              $blockIps = request()->user()->ip_whitelist;
              if(!empty(json_decode($blockIps)) && $blockIps != null){
                  if (!in_array(explode(',',$_SERVER['HTTP_X_FORWARDED_FOR'])[0], json_decode($blockIps,true))) {
                      abort(403, "You are restricted to access the site.");
                  }
                }
            }else{
              //get Global Ip Whitelist address
              $getGlobalIPWhitelist = TenantCredentials::where('tenant_id',request()->user()->tenant_id)->where('key','GLOBAL_IP_ADDRESS_FOR_WHITELIST')->first();
              // dd($getGlobalIPWhitelist);
              if($getGlobalIPWhitelist){
                  if (explode(',',$_SERVER['HTTP_X_FORWARDED_FOR'])[0] != $getGlobalIPWhitelist->value) {
                      abort(403, "You are restricted to access the site.");
                  }
              }else{
                // Found No IP address for this tenant but we have restrict to login user
                abort(403, "You are restricted to access the site.");
              }
            }
        }



        
      }
        
  
        return $next($request);
    }
}
