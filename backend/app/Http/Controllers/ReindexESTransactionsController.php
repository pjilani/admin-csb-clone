<?php

namespace App\Http\Controllers;

use App\Models\Transactions;
use App\Models\BetTransaction;
use App\Services\TransactionsService;
use Illuminate\Support\Facades\DB;
use App\Services\SportsService;


class ReindexESTransactionsController extends Controller
{

    public function reIndexTransactions($id = null)
    {
        ini_set('max_execution_time', 0);
        ini_set("memory_limit", "-1");
        $errors = [];

         if (is_null($id)) {
             TransactionsService::reIndex();
         } else {
             try {
                 Transactions::storeElasticSearchData($id);
             } catch (Exception $e) {
                 $errors[] = ['transaction_id' => $id, 'error' => $e->getMessage()];
             }
         }

        if (empty($errors)) {
            //return $admins;
             echo 'Process successfully completed';
        } else {
            print_r($errors);
        }

    }

    public function reIndexBetTransactions($id = null)
    {
        ini_set('max_execution_time', 0);
        ini_set("memory_limit", "-1");
        $errors = [];
        if (is_null($id)) {
            TransactionsService::reBetIndex();
        } else {
            try {
                SportsService::storeElasticBetSearchData($id);
            } catch (Exception $e) {
                $errors[] = ['transaction_id' => $id, 'error' => $e->getMessage()];
            }
        }
        echo '<pre>';
        if (empty($errors)) {
            echo 'Process successfully completed';
        } else {
            print_r($errors);
        }

    }

    public function reIndexAdminDummyTransactions($id = null)
    {
        ini_set('max_execution_time', 0);
        ini_set("memory_limit", "-1");
        $errors = [];

         if (is_null($id)) {
             TransactionsService::reIndexAdminDummyTransaction();
         } else {
            //  try {
            //      Transactions::storeElasticSearchData($id);
            //  } catch (Exception $e) {
            //      $errors[] = ['transaction_id' => $id, 'error' => $e->getMessage()];
            //  }
         }

        if (empty($errors)) {
            //return $admins;
             echo 'Process successfully completed';
        } else {
            print_r($errors);
        }

    }
}
