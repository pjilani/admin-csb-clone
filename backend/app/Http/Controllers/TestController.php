<?php

namespace App\Http\Controllers;

use App;
use App\Models\Admin;
use App\Models\Currencies;
use App\Models\Transactions;
use App\Models\User;
use App\Models\Wallets;
use App\Models\BulkReindex;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Validator;

class TestController extends Controller
{

    /**
     * @description createPlayer
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function createPlayer(Request $request)
    {

        ini_set('max_execution_time', 0);
        ini_set("memory_limit", "-1");
        // if (App::environment(['local'])) {//, 'staging'
            try {

                $validatorArray = [
                    'tenant_id' => 'required',
                    'agent_id' => 'required',
                    'player_count' => 'required'
                ];

                $validator = Validator::make($request->all(), $validatorArray);

                if ($validator->fails()) {
                    return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
                }

                $last_row = User::select('id')->orderBy('id', 'desc')->first();

                $num = $last_row->id;

                DB::beginTransaction();

                for ($x = 0; $x < $request->player_count; $x++) {

                    $num = $num + 1;

                    $insertData = [
                        'first_name' => "dummy_first" . $num,
                        'last_name' => "dummy_last" . $num,
                        'email' => "dummyxyz" . $num . "@yopmail.com",
                        'encrypted_password' => md5(base64_decode("123456789")),
                        'phone' => "123456" . $num,
                        'phone_verified' => true,
                        'email_verified' => true,
                        'date_of_birth' => "2000-01-01",
                        'nick_name' => "dummy_nuck" . $num,
                        'user_name' => "dummy_user_name" . $num,
                        'country_code' => "IN",
                        'parent_type' => ADMIN_TYPE,
                        'parent_id' => $request->agent_id,
                        'tenant_id' => $request->tenant_id,
                        'vip_level' => 0,
                        'active' => true
                    ];

                    $agent = Admin::find($request->agent_id);

                    $agentWallet = Wallets::select(['id', 'currency_id', 'amount'])->where(['owner_id' => $agent->id, 'owner_type' => ADMIN_TYPE])->first();

//              $insertData['kyc_done'] = $agent->kyc_regulated ? null : true;
                    $insertData['kyc_done'] = true;

                    $createdUser = User::create($insertData);

                    $insertWallets['owner_id'] = $createdUser->id;
                    $insertWallets['currency_id'] = $agentWallet->currency_id;
                    $insertWallets['owner_type'] = USER_TYPE;

                    $userWallet = Wallets::create($insertWallets);
                    User::storeElasticSearchData($createdUser->id);
                    // ==================

                    $transaction_amount = 1000;

                    Wallets::where('id', $agentWallet->id)->update(['amount' => $agentWallet->amount + $transaction_amount]);

                    $bothWallet = Transactions::getWallet($userWallet->id, $agentWallet->id);

                    $currencySource = Currencies::find($bothWallet['sourceWallet']->currency_id)->first();

                    $targetWallet = Currencies::find($bothWallet['targetWallet']->currency_id)->first();

                    $insertData = [
                        'actionee_type' => ADMIN_TYPE,
                        'actionee_id' => $agent->id,
                        'source_currency_id' => $bothWallet['sourceWallet']->currency_id,
                        'target_currency_id' => $bothWallet['targetWallet']->currency_id,
                        'source_wallet_id' => $bothWallet['sourceWallet']->id,
                        'target_wallet_id' => $bothWallet['targetWallet']->id,
                        'source_before_balance' => @$bothWallet['sourceWallet']->amount,
                        'target_before_balance' => @$bothWallet['targetWallet']->amount,
                        'status' => 'success',
                        'parent_id' => $agent->id,
                        'tenant_id' => $request->tenant_id,
                        'created_at' => @date('Y-m-d H:i:s'),
                        'comments' => 'create user',
                        'transaction_type' => 3,
                        'payment_method' => 'manual',
                        'transaction_amount' => $transaction_amount,
                        'amount' => $transaction_amount,
                        'source_after_balance' => $agentWallet->amount,
                        'target_after_balance' => $transaction_amount,
                    ];

                    $insertData['conversion_rate'] = ($currencySource->exchange_rate / $targetWallet->exchange_rate);

                    $createdValues = Transactions::create($insertData);

                    Wallets::where('id', $bothWallet['sourceWallet']->id)->update(['amount' => $insertData['source_after_balance']]);

                    Wallets::where('id', $bothWallet['targetWallet']->id)->update(['amount' => $insertData['target_after_balance']]);
                    Transactions::storeElasticSearchData($createdValues->id);

                }

                DB::commit();

                return array('message' => 'Player Created successfully');

            } catch (\Exception $e) {
                DB::rollback();
                if (!App::environment(['local'])) {//, 'staging'
                    return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
                } else {
                    return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                        'LineNo' => $e->getLine(),
                        'FileName' => $e->getFile()
                    ], Response::HTTP_EXPECTATION_FAILED);
                }
            }
        // }

    }


    public function agentsHierarchy(Request $request) {
         $user = User::select(['id', 'email', 'parent_id', 'tenant_id'])->find($request->target_email);
         $startWithId = $user->parent_id;
         $parent_type = $request->parent_type;
         $tenant_id = $user->tenant_id;
         $where = '';

        if($parent_type == ADMIN_TYPE) {
            $where = 'and c.id <> '. $request->auth_id;
        }

        $sql = 'WITH RECURSIVE cte_query AS
              (
                SELECT id, email, parent_type, parent_id, tenant_id FROM admin_users as m WHERE id = ' . $startWithId . ' and tenant_id =' . $tenant_id . '

                UNION

                SELECT e.id, e.email, e.parent_type, e.parent_id, e.tenant_id FROM admin_users as e
                INNER JOIN cte_query c ON c.parent_id = e.id '. $where .' and e.tenant_id =' . $tenant_id . '
              )
          SELECT * FROM cte_query';
        $agents = DB::select($sql);

        return [ 'user' => $user, 'agents' =>  $agents];
    }

    public function updatePossibleWinAmount($betType){
       if($betType==1){
        $update = DB::table('public.bets_betslip')
            ->where('bettype', 1)
            ->update(array(
            'possible_win_amount' => DB::raw('stake * multi_price')
        ));
       }else{
        $update = DB::table('public.bets_betslip')
            ->where('bettype', 2)
            ->update(array(
            'possible_win_amount' => DB::raw('stake')
        ));
       } 

       if($update){
        echo 'Process successfully completed';
       }
      
    }

    public function createReindexTableData($type)
    {
        $offset = 0;
        for($i=0;$i<=400;$i++){
            $tableData[] = array("offset"=>$offset,"status"=>"pending","type"=>$type,"created_at"=>date('Y-m-d H:i:s'),"updated_at"=>date('Y-m-d H:i:s'));
            $offset = $offset+5000;
        }
        BulkReindex::insert($tableData);
        echo "Success";
    }

}
