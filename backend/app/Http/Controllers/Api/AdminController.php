<?php

namespace App\Http\Controllers\Api;

use App\Aws3;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Admin as AdminModel;
use App\Models\AdminUserPermissionRole;
use App\Models\AdminUsersAdminRoles;
use App\Models\AdminUserSetting;
use App\Models\Currencies;
use App\Models\PlayerCommission;
use App\Models\Tenants;
use App\Models\Transactions;
use App\Models\User;
use App\Models\UserSettings;
use App\Models\Wallets;
use App\Services\PlayerCommissionService;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Validator;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $page = $request->has('page') ? $request->post('page') : 1;
            $limit = $request->has('size') ? $request->post('size') : 10;
            $filter['email'] = $request->has('email') ? $request->post('email') : '';
            $filter['first_name'] = $request->has('first_name') ? $request->post('first_name') : '';
            $filter['last_name'] = $request->has('last_name') ? $request->post('last_name') : '';
            $filter['phone'] = $request->has('phone') ? $request->post('phone') : '';
            $filter['active'] = $request->has('active') ? $request->post('active') : '';
            $filter['agent_name'] = $request->has('agent_name') ? $request->post('agent_name') : '';
            $filter['tenant_id'] = $request->has('tenant_id') ? $request->post('tenant_id') : '';
            $filter['parent_id'] = $request->has('parent_id') ? $request->post('parent_id') : '';

            $record = AdminModel::getList($limit, $page, $filter);

            return returnResponse(true, "Record get Successfully", $record);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // ================= permissions starts here =====================
        if (request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager") {
            $params = new \stdClass();
            if ($request->role[0] == 2) {
                $params->module = 'agents';
                $params->action = 'C';
            }
            if ($request->role[0] == 3) {
                $params->module = 'sub_admin';
                $params->action = 'C';
            }
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);
            if (!$permission) {
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
        }
        // ================= permissions ends here =====================          
        $input = [];
        $isSaveALl = false;
        $rolesObject = getRoles(Auth::user()->id);
        $validatorArray = [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'encrypted_password' => 'required',
            'role' => 'required',
            'agent_name' => 'required',
            'phone' => 'required',
            'phone_verified' => 'required',
            'currency_id' => 'required',
            'time_zone' => 'required',
        ];
        if (Auth::user()->parent_type != 'AdminUser') {
            $validatorArray['tenant_id'] = 'required';
        }
        $validator = Validator::make($request->all(), $validatorArray);

        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
        } else {
            /**
             * check here email id is registered for this tenant or not
             */
            $checkEmail = checkEmailForTenant($request->email, $request->tenant_id, AGENT);
            if ($checkEmail == 0) {
                $input = $request->all();
            } else {
                return returnResponse(false, '', ["email" => [
                    ($request->role[0] == 2) ? AGENT_EXIST : (($request->role[0] == 3) ? SUB_ADMIN_EXIST : USER_EXIST)
                ]], Response::HTTP_BAD_REQUEST);
            }
        }
        DB::beginTransaction();
        try {
            $input['agent_name'] = str_replace(' ', '_', $input['agent_name']);
            $input['agent_name'] = str_replace('-', '_', $input['agent_name']); // Replaces all spaces with hyphens.

            $input['agent_name'] = preg_replace('/[^A-Za-z0-9\-]/', '_', $input['agent_name']);
            $ip_address = [];
            if (isset($input['ip_address_group']['ip_address'])) {
                foreach ($input['ip_address_group']['ip_address'] as $key => $value) {
                    $ip_address[] = $value['ip_address'];
                  }
                }
                $insertData = [
                    'first_name' => $input['first_name'],
                    'last_name' => $input['last_name'],
                    'email' => $input['email'],
                    'encrypted_password' => bcrypt(base64_decode($input['encrypted_password'])),
                    'phone' => $input['phone'],
                    'phone_verified' => $input['phone_verified'],
                    'parent_type' => ADMIN_TYPE,
                    'agent_type' => (isset($input['agent_type']) ? (int)$input['agent_type'] : 0),
                    'agent_name' => $input['agent_name'],
                    'active' => $input['status'],
                    'ip_whitelist' => json_encode($ip_address),
                    'is_applied_ip_whitelist' => $request->enableWhitelist,
                    'ip_whitelist_type' => $request->ipWhitelistType,
                    'timezone' => $request->time_zone,
                    'kyc_regulated' => $request->kyc_regulated ?? false,
                ];
    
                if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                    $insertData['tenant_id'] = Auth::user()->tenant_id;
                    $insertData['parent_id'] = Auth::user()->id;
    
                } else {
                    $insertData['tenant_id'] = $input['tenant_id'];
                    $insertData['parent_id'] = getParentOwner($input['tenant_id']);
                    $insertData['parent_type'] = ADMIN_TYPE;
                }
                $createdValues = AdminModel::create($insertData);
                $insertArray['admin_user_id'] = $createdValues->id;
                $insertArray['value'] = 20;
                $insertArray['key'] = COMMISSION_PERCENTAGE;
                AdminUserSetting::create($insertArray);
                if ($createdValues) {
                    foreach ($input['currency_id'] as $currencyKey => $value) {
                        $insertWallets['owner_id'] = $createdValues->id;
                        $insertWallets['currency_id'] = $value;
                        $insertWallets['owner_type'] = ADMIN_TYPE;
                        Wallets::create($insertWallets);
                        $currencies = Currencies::where('id', $value)->select('code')->first();
                        $params = [];
                        $params = array_merge($insertData, [
                            "id" => $createdValues->id,
                            "currency" => $currencies->code,
                            "created_at" => $createdValues->created_at->toArray()['formatted']
                        ]);
                        if (isset($input['role'][0]) && !in_array($input['role'][0], [3, 4, 5])) {
                            User::storeElasticSearchDataAdminUser($params);
                        }
                    }

                if (is_array($input['role']) && count($input['role'])) {
                    AdminUsersAdminRoles::where('admin_user_id', $createdValues->id)->delete();
                    foreach ($input['role'] as $key => $value) {

                        $sql = "insert into admin_users_admin_roles (admin_user_id, admin_role_id) values ($createdValues->id, $value)";
                        DB::statement($sql);
                    }
                }

                if ($input['permissionRole'] || $rolesObject->IsAgent) {
                    AdminUserPermissionRole::where('admin_user_id', $createdValues->id)->delete();
                    // foreach ($input['role'] as $key => $value) {

                    $permissionRoleId = ($rolesObject->IsAgent) ? DB::table('admin_users_permission_roles')
                        ->where('admin_user_id', Auth::user()->id)
                        ->value('permission_role_id') : $input['permissionRole'];

                    $sql = "insert into admin_users_permission_roles (admin_user_id, permission_role_id) values ($createdValues->id, $permissionRoleId)";
                    DB::statement($sql);
                    // }
                }


                // Create Agent unique Link for user onboard
                $agentData = [
                    'agentId' => $createdValues->id,
                    'agentName' => $input['agent_name'],
                ];
                $affiliateToken = base64_encode(json_encode($agentData));
                AdminModel::find($createdValues->id)->update(['affiliate_token' => $affiliateToken]);

            }
            $isSaveALl = true;
            } catch (\Exception $e) { // something went wrong
                DB::rollback();
                if (!App::environment(['local'])) {//, 'staging'
                    return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
                } else {
                    return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                        'LineNo' => $e->getLine(),
                        'FileName' => $e->getFile()
                    ], Response::HTTP_EXPECTATION_FAILED);
                }
            }
            DB::commit();
            if ($isSaveALl) {
                return returnResponse(true, 'Created successfully.', AdminModel::find($createdValues->id), Response::HTTP_OK, true);
            } else {
                return returnResponse(false, 'Created unsuccessfully.', [], 403, true);
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
                $roles = getRolesDetails($id);
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                if($roles[0] === 'agent'){
                    $params->module = 'agents';
                    $params->action = 'R';
                }
                if($roles[0] === 'sub-admin' || $roles[0] === 'deposit-admin' ||  $roles[0] === 'withdrawal-admin'){
                    $params->module = 'sub_admin';
                    $params->action = 'R';
                }
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            if ($id == '') {
                return returnResponse(true, "Record get Successfully", AdminModel::all());
            } else {
                $users = Admin::getAdminUserById($id);
                $transactionRecord = Transactions::getDepositAmount($id, TRANSACTION_TYPE_DEPOSIT, 'AdminUser');
                @$users[0]->last_deposited_amount = @$transactionRecord[0]->last_deposited_amount;
                @$users[0]->last_updated_at = @$transactionRecord[0]->updated_at;
//                @$users[0]->currency_name = @Admin::getAdminCurrencyName($id)[0]->name;
//                @$users[0]->wallets = @Wallets::where(['owner_type' => ADMIN_TYPE, 'owner_id' => $users[0]->id])->get();
                $rolesLoginUsers = getRolesDetails();
                $roles = getRolesDetails($id);
                if (!in_array('owner', $rolesLoginUsers)) {
                    if (!in_array('owner', $roles)) {
                        if(Auth::user()->parent_type == 'AdminUser' && $id != Auth::user()->id){
                            @$users[0]->wallets =@Admin::getAdminWalletsDetails($id);
                        }
                    }
                }else{
                    @$users[0]->wallets =@Admin::getAdminWalletsDetails($id);
                }
                if($users[0]->affiliate_token){
                  $tenantDetails = Tenants::find($users[0]->tenant_id);
                  $users[0]->affiliate_token = getAffiliateUrl($tenantDetails->domain).$users[0]->affiliate_token;
                }
                $users[0]->permissions = '';
                return returnResponse(true, "Record get Successfully", @$users[0], Response::HTTP_OK, true);
            }
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }
    
    
    
    public function getSuperAdminWalletDetails($id)
    {
      
        try {
            if ($id == '') {
                return returnResponse(true, "Record get Successfully", AdminModel::all());
            } else {
                $users = [];
                $transactionRecord = Transactions::getDepositAmount($id, TRANSACTION_TYPE_DEPOSIT, 'SuperAdminUser');
                @$users[0]->last_deposited_amount = @$transactionRecord[0]->last_deposited_amount;
                @$users[0]->last_updated_at = @$transactionRecord[0]->updated_at;
//                @$users[0]->currency_name = @Admin::getAdminCurrencyName($id)[0]->name;
//                @$users[0]->wallets = @Wallets::where(['owner_type' => ADMIN_TYPE, 'owner_id' => $users[0]->id])->get();
                $rolesLoginUsers = getRolesDetails();
                $roles = getRolesDetails($id);
                 @$users[0]->wallets =@Admin::getSuperAdminWalletsDetails($id);
                return returnResponse(true, "Record get Successfully", @$users[0], Response::HTTP_OK, true);
            }
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }
   
   
    public function getSuperAdminTransactionDetails(Request $request)
    {
      
        try {

          $page = $request->has('page') ? $request->post('page') : 1;
          $limit = $request->has('size') ? $request->post('size') : 10;
            $filter = $request->all();
            $transactionRecord = [];
            $transactionRecord = Transactions::getSuperAdminTransaction($limit, $page,$filter);
            return returnResponse(true, "Record get Successfully", $transactionRecord, Response::HTTP_OK, true);
            
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    function superAdminWalletDeposit(Request $request){
      try {
        $validatorArray = [
            'transaction_amount' => 'required',
            'transaction_type' => 'required',
        ];
        if ($request->transaction_type == "deposit") {
            $transactionType=TRANSACTION_TYPE_DEPOSIT;
            $validatorArray ['target_wallete_id'] = 'required';
            $walletId = $request->target_wallete_id;
            $successMessage = MESSAGE_DEPOSIT;
            $errorMessage = MESSAGE_DEPOSIT_FAIL;
        }

        $validator = Validator::make($request->all(), $validatorArray);
        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
        } else {
            $input = $request->all();
        }


        $sqlCheck="SELECT 
                    *
                    FROM wallets as u
                    where u.id = ?;
                                          
                    ";

        $targetWallet = DB::select($sqlCheck,[$walletId]);
        // dd($targetWallet);
        $createWallet = [];
        $target_wallet_id = 0;
        $target_before_balance = 0;
        if(!$targetWallet){
          // create wallet for super admin user
          $superWallet = [
            "amount" => $input['transaction_amount'],
            "primary" => true,
            "currency_id" => 1,
            "owner_type" => @Auth::user()->parent_type,
            "owner_id" => @Auth::user()->id,
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s'),
            "non_cash_amount" => 0
          ];

          $createWallet = Wallets::create($superWallet);
          $target_wallet_id = $createWallet->id;
          $target_currency_id = $createWallet->currency_id;
          $target_exchange_rate = Currencies::where('id',$createWallet->currency_id)->select('exchange_rate')->first()->exchange_rate;
        }else{
          
          $target_wallet_id = $targetWallet[0]->id;
          $target_before_balance = $targetWallet[0]->amount;
          $target_currency_id = $targetWallet[0]->currency_id;
          $target_exchange_rate = Currencies::where('id',$targetWallet[0]->currency_id)->select('exchange_rate')->first()->exchange_rate;
        }

        $currencySource = Currencies::getPrimaryCurrency();
        // $tenant_id = Tenants::getSuperTenantId($targetWallet[0]->owner_id, $targetWallet[0]->owner_type);

        // if (!$tenant_id) {
        //     throw new \ErrorException($tenant_id . ' tenant ID not found');
        // }

        DB::beginTransaction();

        $insertData = [
            'actionee_type' => @Auth::user()->parent_type,
            'actionee_id' => @Auth::user()->id,
            'source_wallet_id' =>0,
            'source_currency_id' =>0,
            'target_wallet_id' => $target_wallet_id,
            'target_before_balance' => $target_before_balance,
            'status' => 'success',
            'tenant_id' => 0,
            'target_currency_id' => 1,
            'created_at' => @date('Y-m-d H:i:s'),
            'comments' => 'Manually transfer to wallet',
            'transaction_type' => $transactionType,
            'amount' => $input['transaction_amount'],
            'payment_method' => 'manual',
            'transaction_id'=>getUUIDTransactionId()
        ];
        /*  NOTE - EUR is primary currency. Any conversion from currency A to currency B will consider EUR as base.
        e.g. Currency A amount * Exchange Rate of Currency A = Primary Currency(EUR) amount
        e.g. Currency A amount  (Exchange Rate of Currency A / Exchange Rate of Currency B) = Currency B amount */




        if($target_before_balance < 0.00000000){
            $target_before_balance = 0.00000000;
        }
       if ($input['transaction_type'] == "deposit") {

            $insertData['conversion_rate'] = $target_exchange_rate;
            $amount = @(float)$input['transaction_amount'];


            $insertData['target_after_balance'] = (float)$target_before_balance + (float)@$amount;
        }

        //  =======================================Add Other Currency Amount =======================================
        $currenciesTempArray = $insertData['other_currency_amount'] = [];
        $exMainTransactionCurrecy = Currencies::select('code')->where('id', $target_currency_id)->pluck('code');
        $exMainTransactionCurrecy = $exMainTransactionCurrecy->toArray();;
        $amount_currency_ex = Currencies:: getExchangeRateCurrency($exMainTransactionCurrecy);
        // foreach ($CurrenciesValue as $key => $value) {
            if($exMainTransactionCurrecy){
                $getExchangeRateCurrency = Currencies:: getExchangeRateCurrency($exMainTransactionCurrecy);
                $currenciesTempArray[$exMainTransactionCurrecy[0]] = (float)number_format($input['transaction_amount'] * ($getExchangeRateCurrency / $amount_currency_ex ),4,".","");
            }else{
                $currenciesTempArray[$exMainTransactionCurrecy] = $input['transaction_amount'];
            }
        // }
        $insertData['other_currency_amount'] = json_encode($currenciesTempArray);
//  =======================================Add Other Currency Amount ENd =======================================

        $createdValues = Transactions::create($insertData);

        Wallets::where('id', $target_wallet_id)->update(['amount' => $insertData['target_after_balance']]);

        DB::commit();

        if(@$createdValues->id){


          Transactions::storeElasticSearchSuperAdminTransactionData($createdValues->id);

         

        }


        if ($createdValues) {
            return returnResponse(true, $successMessage, [], Response::HTTP_CREATED, true);
        } else {

            return returnResponse(false, $errorMessage, [], Response::HTTP_BAD_REQUEST, true);
        }
    } catch (\Exception $e) {
        DB::rollback();
        if (!App::environment(['local'])) {//, 'staging'
            return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
        } else {
            return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                'LineNo' => $e->getLine(),
                'FileName' => $e->getFile()
            ], Response::HTTP_EXPECTATION_FAILED);
        }

    }
    }




    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        // ================= permissions starts here =====================
        if (request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager") {
            $params = new \stdClass();
            if ($request->role[0] == 2) {
                $params->module = 'agents';
                $params->action = 'U';
            }
            if ($request->role[0] == 3 || $request->role[0] == 4 || $request->role[0] == 5) {
                $params->module = 'sub_admin';
                $params->action = 'U';
            }
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);
            if (!$permission) {
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
        }
        // ================= permissions ends here =====================          
      
        $rolesObject = getRoles(Auth::user()->id);
            $input = [];
        $isSaveALl = false;

        $oldRecord = AdminModel::find($id);

        if (!$oldRecord) {
            return returnResponse(false, 'Record not found', [], 404);
        } else {
//            $oldRecord=$oldRecord->first()->toArray();
        }

        $validatorArray = [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',//|email|unique:admin_users
//            'role' => 'required',
            'agent_name' => 'required',
            'phone' => 'required',
            'phone_verified' => 'required',
            'currency_id' => 'required',
            'time_zone' => 'required',
        ];

        $validator = Validator::make($request->all(), $validatorArray);

        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), 400);
        } else {
            $input = $request->all();
        }

        $insertData['parent_type'] = ADMIN_TYPE;

        DB::beginTransaction();
        try {

            $ip_address = [];
            if(isset($input['ip_address_group']['ip_address'])){
              foreach ($input['ip_address_group']['ip_address'] as $key => $value) {
                $ip_address[] = $value['ip_address'];
              }
            }
            $insertData = [
                'first_name' => $input['first_name'],
                'last_name' => $input['last_name'],
                'email' => $input['email'],
                'phone_verified' => $input['phone_verified'],
                // 'parent_type' => ADMIN_TYPE,
//                'parent_id' => getParentOwner($input['tenant_id']),
                //'agent_name' => $input['agent_name'],
                'agent_type' => (isset($input['agent_type']) ? (int)$input['agent_type'] : 0),
                'ip_whitelist' => json_encode($ip_address),
                'active' => @$input['status']?true:false,
                'kyc_regulated' => $request->kyc_regulated ?? false,
                'is_applied_ip_whitelist' => $request->enableWhitelist,
                'ip_whitelist_type' => $request->ipWhitelistType,
                'timezone' => $request->time_zone,
            ];

            if (@$input['encrypted_password'] && @$input['encrypted_password'] != '') {
                $insertData['encrypted_password'] = bcrypt(base64_decode($input['encrypted_password']));
            }

            if (count($input['role'])) {

                if (is_array($input['role']) && count($input['role'])) {

                    $getRoles = getRolesDetails($id);

                    if (@$getRoles['owner'] == 1) {
                        if (Auth::user()->parent_type != 'AdminUser') {
                            return returnResponse(false, 'Permission Denied', [], Response::HTTP_FORBIDDEN);
                        }
                    }

                    AdminUsersAdminRoles::where('admin_user_id', $id)->delete();

                    foreach ($input['role'] as $key => $value) {
                        $sql = "insert into admin_users_admin_roles (admin_user_id, admin_role_id) values ($id, $value)";
                        DB::statement($sql);
                    }
                }
            }

            if ($input['permissionRole']) {
                AdminUserPermissionRole::where('admin_user_id', $id)->delete();
                // foreach ($input['role'] as $key => $value) {

                    $permissionRoleId = $input['permissionRole']; 

                    $sql = "insert into admin_users_permission_roles (admin_user_id, permission_role_id) values ($id, $permissionRoleId)";
                    DB::statement($sql);
                // }

                // ============checking if updating a agent, all its children permission role should also be changed===========
                $updatedRecordRoleObject = getRoles($id);
                if($updatedRecordRoleObject->IsAgent){
                    $Hierarchy = getAdminHierarchy($id,true);
                    $ids = flatten($Hierarchy);
                    AdminUserPermissionRole::whereIn('admin_user_id', $ids)
                        ->update(['permission_role_id' => $permissionRoleId]);
                }
            }


            $createdValues = AdminModel::where(['id' => $id])->update($insertData);
            if ($createdValues) {
                // Wallets::where(['owner_id' => $id, 'owner_type' => ADMIN_TYPE])->delete();
                foreach ($input['currency_id'] as $currencyKey => $value) {
                    $wallets = Wallets::where(['owner_id' => $id, 'owner_type' => ADMIN_TYPE, 'currency_id' => $value])->first();
                    if (!$wallets) {
                        $insertWallets['owner_id'] = $id;
                        $insertWallets['currency_id'] = $value;
                        $insertWallets['owner_type'] = ADMIN_TYPE;
                        Wallets::create($insertWallets);
                        $currencies = Currencies::where('id', $value)->select('code')->first();
                        $params = [];
                        $params = array_merge($insertData, [
                            "tenant_id" =>$oldRecord->tenant_id,
                            "id" => $id,
                            "currency" => $currencies->code,
                            "created_at" => date('Y-m-d H:i:s'),
                            "parent_id" =>$oldRecord->parent_id,
                        ]);
                        User::storeElasticSearchDataAdminUser($params);
                    }
                }

                // Create Agent unique Link for user onboard
                if($oldRecord->affiliate_token == '' || $oldRecord->affiliate_token == null){
                    $agentData = [
                      'agentId' => $id,
                      'agentName' => $input['agent_name'],
                    ];

                    $affiliateToken =  base64_encode(json_encode($agentData));
                    AdminModel::find($id)->update(['affiliate_token'=>$affiliateToken]);
                }



                $isSaveALl = true;
            }
        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
        DB::commit();
        if ($isSaveALl) {
            return returnResponse(true, MESSAGE_UPDATED, AdminModel::find($id), Response::HTTP_OK, true);
        } else {
            return returnResponse(false, MESSAGE_UPDATED_FAIL, [], 403, true);
        }     

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
                $roles = getRolesDetails($id);

                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                if($roles[0] === 'agent'){
                    $params->module = 'agents';
                    $params->action = 'T';
                }
                if($roles[0] === 'sub-admin' || $roles[0] === 'deposit-admin' ||  $roles[0] === 'withdrawal-admin'){
                    $params->module = 'sub_admin';
                    $params->action = 'T';
                }
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            $admin = AdminModel::where('id', $id)->where('active', true)->get();
            $adminArray = $admin->toArray();
            if (!count($adminArray)) {
                return returnResponse(false, 'record not found', [], 404, true);
            } else {
                $record = Admin::getListOfUser($id);
                AdminModel::where(['id' => $id])->update(['active' => false, 'deactivated_by_id' => auth()->id(), 'deactivated_at' => date('Y-m-d H:i:s'), 'deactivated_by_type' => auth()->user()->parent_type]);

                return returnResponse(true, MESSAGE_DEACTIVE, $record, Response::HTTP_OK, true);
            }
        } catch (\Exception $e) {
         
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function activeUsers($id)
    {
        try {
                $roles = getRolesDetails($id);

                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                if($roles[0] === 'agent'){
                    $params->module = 'agents';
                    $params->action = 'T';
                }
                if($roles[0] === 'sub-admin' || $roles[0] === 'deposit-admin' ||  $roles[0] === 'withdrawal-admin'){
                    $params->module = 'sub_admin';
                    $params->action = 'T';
                }
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            $admin = AdminModel::where('id', $id)->where('active', false)->get();
            $adminArray = $admin->toArray();
            if (!count($adminArray)) {
                return returnResponse(false, 'record not found', [], 404, true);
            } else {
                $record = Admin::getListOfUser($id);
                AdminModel::where(['id' => $id])->update(['active' => true, 'deactivated_by_id' => auth()->id(), 'deactivated_at' => date('Y-m-d H:i:s'), 'deactivated_by_type' => auth()->user()->parent_type]);

                return returnResponse(true, MESSAGE_ACTIVE, $record, Response::HTTP_OK, true);
            }
        } catch (\Exception $e) {
         
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdminAgents(Request $request)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
        $params = new \stdClass();
        $params->module = 'agents';
        $params->action = 'R';
        $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

        $permission = checkPermissions($params);   
        if(!$permission){ 
            return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
        }
        }
        // ================= permissions ends here =====================          
      
        $adminId = 0;
        $rolesObject = getRoles(Auth::user()->id);
            try {

                $page = $request->has('page') ? $request->post('page') : 1;
                $limit = $request->has('size') ? $request->post('size') : 10;
                $filter['search'] = $request->has('search') ? $request->post('search') : '';
                $filter['agent_type'] = $request->has('agent_type') ? $request->post('agent_type') : '';
                $filter['sort_by'] = $request->has('sort_by') ? $request->post('sort_by') : 'id';
                $filter['order'] = $request->has('order') ? $request->post('order') : 'desc';

                $roles = getRolesDetails();

                if (in_array('owner', $roles, true) || in_array('sub-admin', $roles, true)) {
                    $requestAdminId =  $request->has('adminId') ? $request->post('adminId') :0;
                    if(Auth::user()->id === $requestAdminId){
                        $adminId = 0;
                    } else {
                        $adminId = $request->has('adminId') ? $request->post('adminId') :0;
                    }

                } else {
                    $adminId = $request->has('adminId') ? $request->post('adminId') : Auth::user()->id;
                }
                if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                    $tenant_id = Auth::user()->tenant_id;
                    $filter['login_id']=Auth::user()->id;
                } else {
                    $tenant_id = $request->has('tenant_id') ? $request->post('tenant_id') : '';
                }
                if ($adminId !== 0 && Auth::user()->parent_type == 'SuperAdminUser') {
                    $rolesForProfileViewCheck = getRolesDetails($adminId);
                    if (in_array(@$rolesForProfileViewCheck[0], ['owner'])) {
                        $adminId = 0;
                    }
                }
                $record = Admin::getAdminsAgentsById($tenant_id, $adminId, $page, $limit, $filter, 1);
                return returnResponse(true, MESSAGE_GET_SUCCESS, $record, Response::HTTP_OK);
    
            } catch (\Exception $e) {
                if (!App::environment(['local'])) {//, 'staging'
                    return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
                } else {
                    return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                        'LineNo' => $e->getLine(),
                        'FileName' => $e->getFile()
                    ], Response::HTTP_EXPECTATION_FAILED);
                }
            }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSubadmins(Request $request)
    {
            // ================= permissions starts here =====================
            if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'sub_admin';
            $params->action = 'R';
            $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;

                $permission = checkPermissions($params);
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here =====================          
      
        $adminId = 0;
        $rolesObject = getRoles(Auth::user()->id);
            try {

                $page = $request->has('page') ? $request->post('page') : 1;
                $limit = $request->has('size') ? $request->post('size') : 10;
                $filter['search'] = $request->has('search') ? $request->post('search') : '';
                $filter['sort_by'] = $request->has('sort_by') ? $request->post('sort_by') : 'id';
                $filter['order'] = $request->has('order') ? $request->post('order') : 'desc';

                $roles = getRolesDetails();
                if (in_array('owner', $roles, true)) {
    
                    if ($request->has('subadminUser') && $request->post('subadminUser')) {
                        $adminId = $request->has('adminId') ? $request->post('adminId') : Auth::user()->id;
                    }
                } elseif (in_array('agent', $roles, true)) {
                    $adminId = $request->has('adminId') ? $request->post('adminId') : Auth::user()->id;
                }
                if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                    $tenant_id = Auth::user()->tenant_id;
                    $filter['login_id']=Auth::user()->id;
                } else {
                    $tenant_id = $request->has('tenant_id') ? $request->post('tenant_id') : '';
                }
                //}
                $record = Admin::getAdminsAgentsById($tenant_id, $adminId, $page, $limit, $filter, 0);
                return returnResponse(true, MESSAGE_GET_SUCCESS, $record, Response::HTTP_OK);
    
            } catch (\Exception $e) {
                if (!App::environment(['local'])) {//, 'staging'
                    return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
                } else {
                    return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                        'LineNo' => $e->getLine(),
                        'FileName' => $e->getFile()
                    ], Response::HTTP_EXPECTATION_FAILED);
                }
            }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserPlayer(Request $request)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
        $params = new \stdClass();
        $params->module = 'players';
        $params->action = 'R';
        $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

        $permission = checkPermissions($params);   
        if(!$permission){ 
            return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
        }
        }
        // ================= permissions ends here =====================

        try {
            $page = $request->has('page') ? $request->post('page') : 1;
            $limit = $request->has('size') ? $request->post('size') : 10;
            $filter['search'] = $request->has('search') ? $request->post('search') : '';
            $filter['status'] = @$request->status;
            $filter['phone_verified'] = $request->has('phone_verified') ? $request->post('phone_verified') : '';
            $filter['email_verified'] = $request->has('email_verified') ? $request->post('email_verified') : '';
            $filter['kyc_done'] = $request->has('kyc_done') ? $request->post('kyc_done') : '';
            $filter['date_range'] = !empty($request->has('date_range')) ? $request->post('date_range') : [];
            $filter['min_amount'] = $request->has('min_amount') ? $request->post('min_amount') : '';
            $filter['max_amount'] = $request->has('max_amount') ? $request->post('max_amount') : '';
            $filter['sort_by'] = $request->has('sort_by') ? $request->post('sort_by') : 'id';
            $filter['order'] = $request->has('order') ? $request->post('order') : 'desc';
            $filter['vip_levels'] = $request->has('vip_levels') ? $request->post('vip_levels') : '';


            $adminId = $request->has('adminId') ? $request->post('adminId') : Auth::user()->id;

            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $tenant_id = Auth::user()->tenant_id;
            } else {
                $tenant_id = $request->has('tenant_id') ? $request->post('tenant_id') : '';
            }
            // }
            $record = Admin::getAdminsAjentsByPlayers($tenant_id, $adminId, $page, $limit, $filter);
            $users = Admin::on('read_db')->select(['affiliate_token','tenant_id'])->find($adminId);
            $affiliate_token = '';
            if($users->affiliate_token){
              $tenantDetails = Tenants::on('read_db')->select(['domain'])->find($users->tenant_id);
              $affiliate_token = getAffiliateUrl($tenantDetails->domain).$users->affiliate_token;
            }

            $record['affiliate_token'] = $affiliate_token;
            return returnResponse(true, 'record get successfully.', $record, Response::HTTP_OK);

        } catch (\Exception $e) {
            
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    public function getPlayerCommission(Request $request)
    {
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'players_commission';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            setUnlimited();
            $page = $request->page ?? 1;
            $limit = $request->size ?? 10;
            $searchKeyword = $request->search ?? '';

            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $tenantId = $request->tenant_id?? Auth::user()->tenant_id;
                $agentId = $request->agent_id ?? @Auth::user()->id;

            } else {
                $tenantId = $request->tenant_id?? null;
                $agentId = $request->agent_id ?? null;
            }
            $timeZone = $request->time_zone_name?? null;

            $offset = ($page > 1) ? $page * $limit : 0;
            if ($page > 1) {
                $offset = $limit * ($page - 1);
            }
            $time_period = $request['datetime'] ?? [];
            $time_type = $request['time_type'] ?? 'today';
                
                $query = PlayerCommission::on('read_db');
                if($searchKeyword) {
                    $query->whereHas('user', function ($query) use ($searchKeyword) {
                        $query->where('user_name', 'ilike', '%' . $searchKeyword . '%');
                        $query->orwhere('email', 'ilike', '%' . $searchKeyword . '%');
                        $query->orwhere('phone', 'ilike', '%' . $searchKeyword . '%');
                    });
                }
                if($agentId) {
                    $loginAgentRole = getRoleByAdminId($agentId);
                    if($loginAgentRole && ($loginAgentRole->admin_role_id==4||$loginAgentRole->admin_role_id==5)){
                        $agentId = getParentAgent($agentId);
                    }
                    if(!in_array($loginAgentRole->admin_role_id,[1,3])){
                        $query->whereHas('user', function ($query) use ($agentId) {
                            $query->where('parent_id', $agentId)->where('parent_type','AdminUser');
                        });
                    }
                }
                if($request->status){
                    $query->where('status', $request->status);
                }
                if($request->week_id){
                    $query->where('week_id', $request->week_id);
                }
                if($request->interval_type){
                    $query->where('interval_type', $request->interval_type);
                }
                if($request->month_id){
                    $query->where('month_id', $request->month_id);
                }
                if($time_period){
                    $time_period['fromdate']= getTimeZoneWiseTimeISO(@$time_period['start_date']);
                    $time_period['enddate']= getTimeZoneWiseTimeISO(@$time_period['end_date']);
                    unset($time_period['start_date'],$time_period['end_date']);
                    $query->whereBetween('actioned_at', [$time_period['fromdate'], $time_period['enddate']]); 
            }
            $query->whereHas('user', function ($query) use ($tenantId) {
                $query->where('tenant_id', $tenantId);
            });
            $result['data'] = $query->with('user:id,user_name,email,phone')
            ->orderBy('id', 'desc')->paginate($limit);
            $result['total_pending'] = PlayerCommission::on('read_db')->where('status', 0)->
            whereHas('user', function ($query) use ($tenantId) {
                $query->where('tenant_id', $tenantId);
            })
            ->count();
            $collection  = $result['data']->getCollection();
            foreach($collection as $data){
                if($data->interval_type === 'weekly'){
                    $week_id = $data->week_id;
                    $data->actioned_at = DB::connection('read_db')->table('week_details')->where('id',$week_id)->select('start_date', 'end_date')->first();
                    $formattedDates = $data->actioned_at->start_date . ' - ' . $data->actioned_at->end_date;
                    $data->actioned_at = $formattedDates;
                }
                if($data->interval_type === 'monthly'){
                    $month_id = $data->month_id;
                    $data->actioned_at = DB::connection('read_db')->table('month_details')->where('id',$month_id)->select('start_date', 'end_date')->first();
                    $formattedDates = $data->actioned_at->start_date . ' - ' . $data->actioned_at->end_date;
                    $data->actioned_at = $formattedDates;
                }
                if($data->interval_type === 'daily'){
                    $formattedDates = $data->actioned_at . ' - ' . Carbon::parse($data->actioned_at)->endOfDay();
                    $data->actioned_at = $formattedDates;
                }
            }
            $total_pending = $collection->where('status', 0)->count();
            $result['per_page_total_pending'] = $total_pending;
            return returnResponse(true, '', $result, Response::HTTP_OK, true);


        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }
    
    function downloadPlayerCommission(Request $request) {
        try {
               // ================= permissions starts here =====================
               if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'players_commission';
                $params->action = 'export';
                $params->admin_user_id = Auth::user()->id;
                   $params->tenant_id = Auth::user()->tenant_id;

                   $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            setUnlimited();
            $page = $request->page ?? 1;
            $limit = $request->size ?? 10;
            $searchKeyword = $request->search ?? '';

            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $tenantId = $request->tenant_id?? Auth::user()->tenant_id;
                $agentId = $request->agent_id ?? @Auth::user()->id;

            } else {
                $tenantId = $request->tenant_id?? null;
                $agentId = $request->agent_id ?? null;
            }
            $timeZone = $request->time_zone_name?? null;

            $offset = ($page > 1) ? $page * $limit : 0;
            if ($page > 1) {
                $offset = $limit * ($page - 1);
            }
            $time_period = $request['datetime'] ?? [];
            $time_type = $request['time_type'] ?? 'today';
                
                $query = PlayerCommission::on('read_db');
                if($searchKeyword) {
                    $query->whereHas('user', function ($query) use ($searchKeyword) {
                        $query->where('user_name', 'ilike', '%' . $searchKeyword . '%');
                        $query->orwhere('email', 'ilike', '%' . $searchKeyword . '%');
                        $query->orwhere('phone', 'ilike', '%' . $searchKeyword . '%');
                    });
                }
                if($agentId) {
                    $loginAgentRole = getRoleByAdminId($agentId);
                    if($loginAgentRole && ($loginAgentRole->admin_role_id==4||$loginAgentRole->admin_role_id==5)){
                        $agentId = getParentAgent($agentId);
                    }
                    if(!in_array($loginAgentRole->admin_role_id,[1,3])){
                        $query->whereHas('user', function ($query) use ($agentId) {
                            $query->where('parent_id', $agentId)->where('parent_type','AdminUser');
                        });
                    }
                }
                if($request->status){
                    $query->where('status', $request->status);
                }
                if($request->week_id){
                    $query->where('week_id', $request->week_id);
                }
                if($request->month_id){
                    $query->where('month_id', $request->month_id);
                }
                if($request->interval_type){
                    $query->where('interval_type', $request->interval_type);
                }
                if($time_period){
                    $time_period['fromdate']= getTimeZoneWiseTimeISO(@$time_period['start_date']);
                    $time_period['enddate']= getTimeZoneWiseTimeISO(@$time_period['end_date']);
                    unset($time_period['start_date'],$time_period['end_date']);
                    $query->whereBetween('actioned_at', [$time_period['fromdate'], $time_period['enddate']]); 
            }
            $query->whereHas('user', function ($query) use ($tenantId) {
                $query->where('tenant_id', $tenantId);
            });
            $result = $query->with('user:id,user_name,email,phone')
            ->orderBy('id', 'desc')->get();
            if($result){

                    $columns = array(
                        'ID',
                        'User Name',
                        'Email',
                        'Phone Number',
                        'Status',
                        'NGR',
                        'Commission %',
                        'Commission Amount',
                        'Interval Type',
                        'Date Range'
                    );
        
                    $path = Storage::path('csv/');
                    if (!is_dir($path)) {
                        mkdir($path, 0775, true);
                        chmod($path, 0775);
                    }
        
        
                    $fileName = time() . '.csv';
                    $file = fopen($path . $fileName, 'w');
                    fputcsv($file, $columns);
        
                    
                    foreach ($result as $row) {
                            if($row->interval_type === 'weekly'){
                                $week_id = $row->week_id;
                                $row->actioned_at = DB::connection('read_db')->table('week_details')->where('id',$week_id)->select('start_date', 'end_date')->first();
                                $formattedDates = $row->actioned_at->start_date . ' - ' . $row->actioned_at->end_date;
                                $row->actioned_at = $formattedDates;
                            }
                            if($row->interval_type === 'monthly'){
                                $month_id = $row->month_id;
                                $row->actioned_at = DB::connection('read_db')->table('month_details')->where('id',$month_id)->select('start_date', 'end_date')->first();
                                $formattedDates = $row->actioned_at->start_date . ' - ' . $row->actioned_at->end_date;
                                $row->actioned_at = $formattedDates;
                            }
                            if($row->interval_type === 'daily'){
                                $formattedDates = $row->actioned_at . ' - ' . Carbon::parse($row->actioned_at)->endOfDay();
                                $row->actioned_at = $formattedDates;
                            }
                        fputcsv($file, array(
                                $row['id'],
                                ($row['user']['user_name'] ?? '-'),
                                ($row['user']['email']?? '-'),
                                ($row['user']['phone']?? '-'),
                                $row["status"] === 0 ? 'Pending' : ($row["status"] === 1 ? 'Approved' : 'Rejected'),
                                ($row['ngr'] ?? '-'),
                                ($row['commission_percentage']?? '-'),
                                ($row['commission_amount']?? '-'),
                                ($row['interval_type']?? '-'),
                                ($row['actioned_at']?? '-'),
                            )
                        );
                    }
                    fclose($file);
            }
          
            // $result = ["url" => (request()->secure() ? secure_url(('storage/app/csv/' . $fileName)): url('storage/app/csv/' . $fileName))];
            $result = ["url" => secure_url('storage/app/csv/' . $fileName)];


                    return returnResponse(true, '', $result, Response::HTTP_OK, true);
    } catch (\Exception $e) {
        if (!App::environment(['local'])) {//, 'staging'
            return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
        } else {
            return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                'LineNo' => $e->getLine(),
                'FileName' => $e->getFile()
            ], Response::HTTP_EXPECTATION_FAILED);
        }
    }

        
    }
    public function downloadUserPlayer(Request $request)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
        $params = new \stdClass();
        $params->module = 'players';
        $params->action = 'export';
        $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

        $permission = checkPermissions($params);   
        if(!$permission){ 
            return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
        }
        }
        // ================= permissions ends here =====================          
      

        ini_set('max_execution_time', 0);
        ini_set("memory_limit", "-1");
        try {

            $page = 'all';
            $limit = 'all';
            $filter['search'] = $request->has('search') ? $request->post('search') : '';
            $filter['status'] = @$request->status;
            $filter['phone_verified'] = $request->has('phone_verified') ? $request->post('phone_verified') : '';
            $filter['email_verified'] = $request->has('email_verified') ? $request->post('email_verified') : '';
            $filter['kyc_done'] = $request->has('kyc_done') ? $request->post('kyc_done') : '';
            $filter['date_range'] = !empty($request->has('date_range')) ? $request->post('date_range') : [];
            $filter['min_amount'] = $request->has('min_amount') ? $request->post('min_amount') : '';
            $filter['max_amount'] = $request->has('max_amount') ? $request->post('max_amount') : '';
            $filter['sort_by'] = $request->has('sort_by') ? $request->post('sort_by') : 'id';
            $filter['order'] = $request->has('order') ? $request->post('order') : 'desc';
            $filter['vip_levels'] = $request->has('vip_levels') ? $request->post('vip_levels') : '';

            $adminId = $request->has('adminId') ? $request->post('adminId') : Auth::user()->id;

            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $tenant_id = Auth::user()->tenant_id;
            } else {
                $tenant_id = $request->has('tenant_id') ? $request->post('tenant_id') : '';
            }
            // }
            $record = Admin::getAdminsAjentsByPlayers($tenant_id, $adminId, $page, $limit, $filter);

            if( $request->exists('sampleCsv') && $request->sampleCsv){
                $columns = array(
                    'Email',
                    'Username',
                    'Phone',
                    'Amount'
                );
            }
            else{
                $columns = array(
                    'ID',
                    'First Name',
                    'Last Name',
                    'Email',
                    'User Name',
                    'Nick Name',
                    'Agent name',
                    'Total Balance',
                    'Currency',
                    'Phone Code',
                    'Phone Number',
                    'Status',
                    'Registered At',
                    'Last Login Date'
                );
            }

            $path = Storage::path('csv/');
            if (!is_dir($path)) {
                mkdir($path, 0775, true);
                chmod($path, 0775);
            }


            $uid = Str::random(16);
            $fileName = 'player_list_'. now()->toDateTimeLocalString(). '_'. $uid . '.csv';
            $file = fopen($path . $fileName, 'w');
            fputcsv($file, $columns);


            foreach ($record['data'] as $row) {
                $row=(array)$row;
               /* if ($timeZone) {
                    if ($task["creation_date"])
                        $row["creation_date"] = getTimeZoneWiseTime($task['creation_date'], $timeZone);
                    else
                        $row["creation_date"] = $task['creation_date'];


                } else {
                    $row["creation_date"] = $task['creation_date'];


                }*/

                if ($row["created_at"]!='')
                        $row["created_at"] = date('d-m-Y h:i:s', strtotime($row['created_at']));
                    else
                        $row["created_at"] = '-';

                if ($row["last_login_date"]!='')
                        $row["last_login_date"] = date('d-m-Y h:i:s', strtotime($row['last_login_date']));
                    else
                        $row["last_login_date"] = '-';
                if ($row["active"] == true)
                        $row["active"] = 'Active';
                    else
                        $row["active"] = 'In-Active';

            if( $request->exists('sampleCsv') && $request->sampleCsv){
                fputcsv($file, array(

                    ($row["email"]?? '-'),
                    ($row["user_name"]?? '-'),
                    ($row['phone']?? '-'),
                    '0.00'
                )
            );
            }
            else{
                fputcsv($file, array(

                        $row["id"],
                        ($row["first_name"]?? '-'),
                        ($row["last_name"]?? '-'),
                        ($row["email"]?? '-'),
                        ($row["user_name"]?? '-'),
                        ($row["nick_name"]?? '-'),
                        ($row["agentname"]?? '-'),
                        round($row["total_amount"],4),
                        ($row["currency_code"]?? '-'),
                        ($row["phone_code"]?? '-'),
                        ($row['phone']?? '-'),
                        $row['active'],
                        $row["created_at"],
                        $row["last_login_date"],
                    )
                );
                
            }
            }
            fclose($file);
             // $result = ["url" => (request()->secure() ? secure_url(('storage/app/csv/' . $fileName)): url('storage/app/csv/' . $fileName))];
           $result = ["url" => secure_url('storage/app/csv/' . $fileName)];


            return returnResponse(true, "Record get Successfully", $result);
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setting(Request $request)
    {
        $findRecord = AdminUserSetting::where('admin_user_id', $request->admin_user_id)->first();
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
        $params = new \stdClass();
        $params->module = 'agents';
        if($findRecord){
            $params->action = 'U';
        }
        else {
            $params->action = 'C';
        }
        $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

        $permission = checkPermissions($params);   
        if(!$permission){ 
            return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
        }
        }
        // ================= permissions ends here =====================          
      

        if (Auth::user()->parent_type != 'AdminUser') {
            return returnResponse(false, 'Permission Denied', [], Response::HTTP_FORBIDDEN);
        }
        try {

            $validatorArray = [
                'value' => 'required',
                'admin_user_id' => 'required|integer',
            ];

            $validator = Validator::make($request->all(), $validatorArray);

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
            } else {

                if ($findRecord && count($findRecord->toArray()) > 0) {
                    $findRecord->value = $request->value;
                    $findRecord->update();
                    return returnResponse(true, 'Update successfully.', $findRecord->toArray(), Response::HTTP_OK, true);
                } else {
                    $insertArray['admin_user_id'] = $request->admin_user_id;
                    $insertArray['value'] = $request->value;
                    $insertArray['key'] = COMMISSION_PERCENTAGE;
                    AdminUserSetting::create($insertArray);
                    return returnResponse(true, 'Created successfully.', $insertArray, Response::HTTP_CREATED, true);
                }
            }
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteSetting($id)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
        $params = new \stdClass();
        $params->module = 'agents';
        $params->action = 'D';
        $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

        $permission = checkPermissions($params);   
        if(!$permission){ 
            return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
        }
        }
        // ================= permissions ends here =====================          

        if (Auth::user()->parent_type != 'AdminUser') {
            return returnResponse(false, 'Permission Denied', [], Response::HTTP_FORBIDDEN);
        }
        try {

            $findRecord = AdminUserSetting::find($id);

            if ($findRecord && count($findRecord->toArray()) > 0) {
                $res = AdminUserSetting::where('id', $id)->delete();

                return returnResponse(true, 'Deleted Successfully.', [], Response::HTTP_OK, true);
            }
            throw new \Exception('Record not found ');


        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }


    /**
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdminUserTree(Request $request)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
        $params = new \stdClass();
        $params->module = 'agents';
        $params->action = 'R';
        $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

        $permission = checkPermissions($params);   
        if(!$permission){ 
            return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
        }
        }
        // ================= permissions ends here =====================          
      
        $id = $request->id;
        try {
            $findRecord = Admin::on('read_db')->select('id','active',
                DB::RAW("CONCAT(agent_name, ' <br> &nbsp; &nbsp; &nbsp; &nbsp; players : ',
                (SELECT count(id) FROM users where users.parent_id=admin_users.id and users.parent_type='AdminUser')) AS text"));

            if ($id) {
                if($request->type == 'child') {
                    $findRecord->where('parent_id', $id);
                    $findRecord->where('id','<>', $id);
                } else {
                    $findRecord->where('id', $id);
                }
            } else {
                if(request()->User()->parent_type == "SuperAdminUser" || request()->User()->parent_type == "Manager") {
                    $findRecord->where(['tenant_id' => $request->tenant_id]);
                } else {
                    if($request->type == 'child') {
                        $findRecord->where('parent_id', Auth::User()->id);
                        $findRecord->where('id','<>', Auth::User()->id);
                    } else {
                        $findRecord->where('id', Auth::User()->id);
                    }
                }
            }

            $findRecord = $findRecord->orderBy('id', 'asc')->get();

            if ($findRecord && count($findRecord->toArray()) > 0) {
                foreach ($findRecord as $i => $rds) {
                    if($rds->active)
                        $findRecord[$i]->a_attr = ["style" => "color: green; height: 50px;"];
                    else
                        $findRecord[$i]->a_attr = ["style" => "color: red; height: 50px;"];

                    unset($findRecord[$i]->active);
                }
                return response()->json($findRecord);
                // return returnResponse(true, '', $findRecord, Response::HTTP_OK);
            } else {
                return response()->json([]);
                // return returnResponse(true, '', [], Response::HTTP_OK);
            }

        } catch (\Exception $e) {
            return response()->json([]);
            // return returnResponse(false, $e->getMessage(), [] ,Response::HTTP_EXPECTATION_FAILED,true);
        }
    }

    /**
     * @description getSuperAdminUserTree
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSuperAdminUserTree(Request $request)
    {
        $findRecord = [];
        $id = $request->id;
        try {

            if (request()->User()->parent_type == "SuperAdminUser" || request()->User()->parent_type == "Manager") {
                if (!$id) {

                    $sqlChanges = Admin::on('read_db')->getSQLSuperAdminTree($request->tenant_id);

                } else {
                    $sqlChanges = Admin::on('read_db')->gwtSqlAdminTree($id);

                }
                $findRecord = DB::select($sqlChanges);
            }
            if ($findRecord && count($findRecord) > 0) {
                foreach ($findRecord as $i => $rds) {
                    $findRecord[$i]->a_attr = ["style" => "color: green; height: 50px;"];
                }
                return response()->json($findRecord);
                // return returnResponse(true, '', $findRecord, Response::HTTP_OK);
            } else {
                return response()->json([]);
                // return returnResponse(true, '', [], Response::HTTP_OK);
            }

        } catch (\Exception $e) {
            return response()->json([]);
            // return returnResponse(false, $e->getMessage(), [] ,Response::HTTP_EXPECTATION_FAILED,true);
        }
    }

    /**
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAWSUrl(Request $request)
    {

        try {
            if ($request->keyname) {
                return returnResponse(true, 'Record Get Successfully.', ['url' => Aws3::getAwsUrl($request->keyname)], Response::HTTP_OK);
            } else {
                return returnResponse(false, 'key not found', [], Response::HTTP_OK);
            }

        } catch (\Exception $e) {
            return returnResponse(false, $e->getMessage(), [], Response::HTTP_EXPECTATION_FAILED, true);
        }
    }

    public function bulkFinance(Request $request){
        dd($request->ids);
        return $request;
    }

    public function bulkEdit(Request $request){
        try {
            // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            if($request->admin_type == 'agents'){
                $params->module = 'agents';
                $params->action = 'U';
            }
            if($request->admin_type == 'sub_admin'){
                $params->module = 'sub_admin';
                $params->action = 'U';
            }
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here =====================

            $validator = Validator::make($request->all(), [
                // 'isActive' => 'required',
                // 'whitelistOption' => 'required',
                'ids' => 'required',
                // 'ips' => 'array',
            ]);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()], 400);
            }

            $isActive = ($request->input('isActive')) ? $request->input('isActive') : null;
            $whitelistOption = ($request->input('whitelistOption')) ? $request->input('whitelistOption') : null;
            $ids = $request->input('ids');
            $ips = ($request->input('ips')) ? $request->input('ips') : null;

            $inputArray=[];

            if($isActive){
                $inputArray['active'] = $isActive == 'active' ? true : false;
            }

            if ($whitelistOption) {
                $inputArray['is_applied_ip_whitelist'] = true;
                $inputArray['ip_whitelist_type'] = $whitelistOption;
                $inputArray['ip_whitelist'] = json_encode($ips);
            }

            AdminModel::whereIn('id', $ids)->update($inputArray);

            return response()->json(['message' => 'Bulk Edited successfully']);
        } catch (\Exception $e) { // something went wrong
            if (!App::environment(['local'])) { //, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, [
                    'Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

}



