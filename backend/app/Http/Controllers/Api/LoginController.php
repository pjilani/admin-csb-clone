<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Models\Admin;
use App\Models\TenantCredentials;
use App\Models\Tenants;
use App\Models\TenantThemeSettings;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Client as OClient;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\App;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id = '')
    {
        try {
            if ($id == '') {
                return returnResponse(true, "Record get Successfully", User::all());
            } else {

                return returnResponse(true, "Record get Successfully", User::find($id));
            }
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required',
            ]);

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), 400);
            }


            //get Tenant details from host
            $keyword = getDomainNameFromHost();

            $tenant = Tenants::on('read_db')->where('domain', 'like', '%' . $keyword . '%')
                ->where('active', true)
                ->first();

            $credentials = [
                'email' => $request->email,
                'password' => base64_decode($request->password),
            ];
            
            if ($tenant) {
                $credentials['tenant_id'] = $tenant->id;
            }


            if (Auth::guard('admin')->attempt($credentials, $request->remember_me)) {


                $user = Auth::guard('admin')->user();

                if ($user->active ? false : true ) {
                    return returnResponse(false, 'Your account is Deactivated, Please contact to Admin to Activated Account.', [], 403);
                }

                // $blockIps = $user->ip_whitelist;
                // if(!empty(json_decode($blockIps)) && $blockIps != null){
                //   if (!in_array($request->ip(), json_decode($blockIps,true))) {
                //       // abort(403, "You are restricted to access the site.");
                //       return returnResponse(false, 'You are restricted to access the site.', [], 403);
                //   }
                // }

                if($user->is_applied_ip_whitelist){
                  if($user->ip_whitelist_type == 'manual'){
                    $blockIps = $user->ip_whitelist;
                    if(!empty(json_decode($blockIps)) && $blockIps != null){
                        if (!in_array(explode(',',$_SERVER['HTTP_X_FORWARDED_FOR'])[0], json_decode($blockIps,true))) {
                          return returnResponse(false, 'You are restricted to access the site.', [], 403);
                        }
                      }
                  }else{
                    //get Global Ip Whitelist address
                    $getGlobalIPWhitelist = TenantCredentials::where('tenant_id',$user->tenant_id)->where('key','GLOBAL_IP_ADDRESS_FOR_WHITELIST')->first();
                    if($getGlobalIPWhitelist){
                        if (explode(',',$_SERVER['HTTP_X_FORWARDED_FOR'])[0] != $getGlobalIPWhitelist['value']) {
                          return returnResponse(false, 'You are restricted to access the site.', [], 403);
                        }
                    }else{
                      // Found No IP address for this tenant but we have restrict to login user
                      return returnResponse(false, 'You are restricted to access the site.', [], 403);
                    }
                  }
              }





                $success['token_type'] = 'Bearer';
                $success['token'] = $user->createToken(env('APP_NAME' . "_admin"))->accessToken;
                $success['user'] = $user;
                $success['user']->roles = getRolesDetails($user->id);
                $success['tenant'] = getTenantDetails($user->tenant_id);
                $success['permissions'] = getPermissionsForAdminUser($user->id,$user->tenant_id);

                $lc = 1;
                Admin::where('id', $user->id)->update([ 'login_count' => DB::raw('login_count + '.$lc ) ]);

                return returnResponse(true, $user->name . ' login successfully.', $success);
            } else {
                return returnResponse(false, 'unauthorized', [], 403);
            }
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    public function superAdminlogin(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required',
            ]);

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), 400);
            }
            if (Auth::guard('super')->attempt(['email' => $request->email, 'password' => base64_decode($request->password)], $request->remember_me)) {

                $user = Auth::guard('super')->user();

                if ($user->active ? false : true) {
                    return returnResponse(false, 'Your account is Deactivated, Please contact to Admin to Activated Account.', [], 403);
                }


                $role_details = DB::table('super_admin_users_super_roles as sr')->select("sr.super_role_id", "r.name")->leftJoin('super_roles as r', 'r.id', '=', 'sr.super_role_id')->where("sr.super_admin_user_id", $user->id)->first();

                if ($role_details) {
                    $user->role_id = $role_details->super_role_id;
                    $user->role = $role_details->name;
                } else {
                    return returnResponse(false, 'unauthorized', [], 403);
                }
                $success['token_type'] = 'Bearer';
                $success['token'] = $user->createToken(env('APP_NAME' . "_superadmin"))->accessToken;
                $success['user'] = $user;

                return returnResponse(true, $user->name . ' login successfully.', $success);
            } else {
                return returnResponse(false, 'unauthorized', [], 403);
            }
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function adminLogout(Request $request)
    {
        try{
        $user = $request->user();
        foreach ($user->tokens as $token) {
            $token->revoke();
        }
        Auth::guard('admin')->logout();
//        DB::delete('delete from sessions where user_id = ?',[$user->id]);

        return returnResponse(true, "Logout Successfully");
    } catch (\Exception $e) {
        if (!App::environment(['local'])) {//, 'staging'
            return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
        } else {
            return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                'LineNo' => $e->getLine(),
                'FileName' => $e->getFile()
            ], Response::HTTP_EXPECTATION_FAILED);
        }
    }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function adminSuperLogout(Request $request)
    {
       try{
        $user = $request->user();
        foreach ($user->tokens as $token) {
            $token->revoke();
        }
        Auth::guard('super')->logout();
//        DB::delete('delete from sessions where user_id = ?',[$user->id]);

        return returnResponse(true, "Logout Successfully");
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    protected function getTokenAndRefreshToken($email, $password)
    {
        $oClient = OClient::where('password_client', 1)->first();
        $http = new Client;
        $response = $http->request('POST', url('/') . '/oauth/token', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => $oClient->id,
                'client_secret' => $oClient->secret,
                'username' => $email,
                'password' => $password,
                'scope' => '*',
            ],
        ]);

        $result = json_decode((string)$response->getBody(), true);
//        return $result;
        return response()->json($result, 200);
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function profileUpdate(Request $request)
    {
        try
        {
        $input = $request->all();
        $file = $request->file();
        $filePath = '';
        $validatorArray = [
            'name' => 'required|string|max:50',
            'last_name' => 'required|string|max:50',
        ];
        if (isset($input['email']) && Auth::user()->email != $input['email']) {
            $validatorArray['email'] = 'required|email|unique:users';
            $updateArray['email'] = $input['email'];
        }
        if (isset($input['password'])) {
            $validatorArray['password'] = 'string|max:50';
        }
        if (isset($input['file_upload'])) {
            $validatorArray['file_upload'] = 'mimes:png,jpeg,jpg|max:5120';
        }

        $validator = Validator::make($input, $validatorArray);
        if ($validator->fails()) {
            return returnResponse(true, $validator->errors());
//            return redirect()->back()->withInput()->withErrors($validator);
        }
        if ($file) {
            $fileName = time() . '____' . $request->file('profile_pic')->getClientOriginalName();
            $fileName = str_replace(' ', '_', $fileName);
            $path = '/uploads/user_profile';
            $request->file('profile_pic')->storeAs($path, $fileName);
            $updateArray ['profile_pic'] = '/storage/app' . $path . '/' . $fileName;
        }
        $updateArray['first_name'] = $input['first_name'];
        $updateArray['last_name'] = $input['last_name'];
        if (isset($input['password'])) {
            $updateArray['password'] = Hash::make($input['password']);
        }
        $affected = DB::table('users')
            ->where('id', request()->user()->id)
            ->update($updateArray);
        if ($affected) {
            return returnResponse(true, 'User updated successfully.', User::find(request()->user()->id));
        } else {
            return returnResponse(false, 'Nothing Updated"');
        }
    } catch (\Exception $e) {
        if (!App::environment(['local'])) {//, 'staging'
            return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
        } else {
            return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                'LineNo' => $e->getLine(),
                'FileName' => $e->getFile()
            ], Response::HTTP_EXPECTATION_FAILED);
        }
    }
    }

    public function getTenantsDetailsFromDomain(){
        
        $keyword = getDomainNameFromHost();

        $tenant = Tenants::on('read_db')->where('domain', 'like', '%' . $keyword . '%')
            ->where('active', true)
            ->first();
        if($tenant){
            $tenant->logo_url = @TenantThemeSettings::on('read_db')->where('tenant_id', $tenant->id)->first()->logo_url;
        }

        return returnResponse(true, 'Tenant Details.', $tenant);
    }


}
