<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Bonus as BonusModel;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Aws3;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\App;

class BonusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDeposit($id)
    {
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'deposit_bonus';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          

            $data = BonusModel::select('*', DB::raw("(SELECT code FROM  currencies where id=bonus.currency_id) as currency_name "));
            if ($id != '') {
                $data->where('id', $id);
            }
            $data->whereIn('kind', ['deposit', 'deposit_sport']);
            //$dataArray=$data->forPage(1,5)->get();
            $dataArray = $data->get();
            $sqlCheckV = "SELECT * FROM deposit_bonus_settings where bonus_id= ?";

            $settingArray = DB::select($sqlCheckV, [@$dataArray[0]->id]);

            @$dataArray[0]->setting = @$settingArray[0];
            return returnResponse(true, "Record get Successfully", @$dataArray[0]);
        } catch (\Exception $e) {
            
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllDeposit(Request $request)
    {
        $page = $request->has('page') ? $request->post('page') : 1;
        $limit = $request->has('size') ? $request->post('size') : 10;
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'joining_bonus';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;

                $params1 = new \stdClass();
                $params1->module = 'losing_bonus';
                $params1->action = 'R';
                $params1->admin_user_id = Auth::user()->id;
                $params1->tenant_id = Auth::user()->tenant_id;

                $params2 = new \stdClass();
                $params2->module = 'deposit_bonus';
                $params2->action = 'R';
                $params2->admin_user_id = Auth::user()->id;
                $params2->tenant_id = Auth::user()->tenant_id;

                $permission = checkPermissions($params);
                $permission1 = checkPermissions($params1);   
                $permission2 = checkPermissions($params2);   

                if(!($permission || $permission1 || $permission2)){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          

            $type = $request->kind;

            $dataArray = BonusModel::getListBonus($limit, $page, $type);
            return returnResponse(true, "Record get Successfully", $dataArray);
        } catch (\Exception $e) {
         
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLosing($id)
    {
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'losing_bonus';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            $data = BonusModel::select('*', DB::raw("(SELECT code FROM  currencies where id=bonus.currency_id) as currency_name "));

            if ($id != '') {
                $data->where('id', $id);
            }

            $data->where('kind', '=', 'losing');
            $data->where('tenant_id', '=', Auth::user()->tenant_id);
            $dataArray = $data->get();
            $sqlCheckV = "SELECT * FROM losing_bonus_settings where bonus_id= ?";
            $settingArray = DB::select($sqlCheckV, [$dataArray[0]->id]);
            @$dataArray[0]->setting = @$settingArray[0];

            $sqlTiers = "SELECT * FROM losing_bonus_tiers where losing_bonus_setting_id= ?";
            $settingArray = DB::select($sqlTiers, [$dataArray[0]->setting->id]);
            @$dataArray[0]->tiers = @$settingArray;

            return returnResponse(true, "Record get Successfully", $dataArray[0]);
        } catch (\Exception $e) {
           
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bonusSegment = $request->segment(4);
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
        $params = new \stdClass();
        if($bonusSegment == 'deposit'){
            $params->module = 'deposit_bonus';
            $params->action = 'C';
        }
        if($bonusSegment == 'losing'){
            $params->module = 'losing_bonus';
            $params->action = 'C';
        }
        
        $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

        $permission = checkPermissions($params);   
        if(!$permission){ 
            return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
        }
        }
        // ================= permissions ends here =====================          
      

        $input = [];
        $isSaveALl = false;

        $validatorArray = [
            'code' => 'required|alpha_num',
            'currency_id' => 'required|exists:currencies,id',
            'vip_levels' => 'required',
            'image' => 'required',
            'terms_and_conditions' => 'required',
            'enabled' => 'required',
        ];

        if ($bonusSegment == 'deposit') {
            $validatorArray['kind'] = 'required';
            $validatorArray['percentage'] = 'required';
            $validatorArray['campaign_period'] = 'required';
            $validatorArray['min_deposit'] = 'required';
            $validatorArray['max_deposit'] = 'required';
            $validatorArray['max_bonus'] = 'required';
            $validatorArray['rollover_multiplier'] = 'required';
            $validatorArray['max_rollover_per_bet'] = 'required';
            $validatorArray['valid_for_days'] = 'required';


        } elseif ($bonusSegment == 'losing') {
            $validatorArray['claim_days'] = 'required';
        }


        $validator = Validator::make($request->all(), $validatorArray);

        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
        } else {
            $input = $request->all();
        }
        DB::beginTransaction();
        try {

            $insertArray = [
                'code' => @$input['code'],
                'percentage' => @$input['percentage'],
                'enabled' => @$input['enabled'],
                'valid_from' => @$input['campaign_period']['valid_from'],
                'valid_upto' => @$input['campaign_period']['valid_upto'],
                'currency_id' => @$input['currency_id'],
                'promotion_title' => @$input['promotion_title'],
                'terms_and_conditions' => @$input['terms_and_conditions'],
                'vip_levels' => "{" . @implode(',', json_decode($input['vip_levels'])) . "}",
//                'usage_count'=>@$input['usage_count'],
                'created_at' => date('Y-m-d H:i:s'),
            ];

              // code duplicate 
              $checkCode = BonusModel::where('code',@$input['code'])->where('tenant_id',Auth::user()->tenant_id)->where('kind',$bonusSegment)->first();
              if($checkCode){
                  return returnResponse(false, 'Bonus Code can not be duplicate', [], Response::HTTP_INTERNAL_SERVER_ERROR);
              }

            $bonusSegment = $request->segment(4);
            if ($bonusSegment == 'deposit') {
                $insertArray['kind'] = @$input['kind'];
            } elseif ($bonusSegment == 'losing') {
                $insertArray['kind'] = 'losing';
            }
            //login In user Tenant Id
            $insertArray['tenant_id'] = Auth::user()->tenant_id;

            if (@$request->file('image')) {

                $aws3 = new Aws3();
                $fileName = Str::uuid() . '____' . $request->file('image')->getClientOriginalName();
                $fileName = str_replace(' ', '_', $fileName);
                $fileNamePath = "tenants/bonus/" . $fileName;

                $PathS3Key = $aws3->uploadFile($fileNamePath, $request->file('image')->getPathname(), $request->file('image')->getClientMimeType());
                $insertArray['image'] = @$PathS3Key;
            }

            $createdValues = BonusModel::create($insertArray);
            if ($createdValues) {
                if ($bonusSegment == 'deposit') {

                    $sqlBonusSetting = 'insert into 
                                    deposit_bonus_settings (
                                    min_deposit, 
                                    max_deposit, 
                                    max_bonus,
                                    rollover_multiplier,
                                    max_rollover_per_bet,
                                    valid_for_days,
                                    bonus_id,created_at,updated_at) values(?,?,?,?,?,?,?,?,?)';
                    DB::insert($sqlBonusSetting,
                        [
                            $input['min_deposit'],
                            $input['max_deposit'],
                            $input['max_bonus'],
                            $input['rollover_multiplier'],
                            $input['max_rollover_per_bet'],
                            $input['valid_for_days'],
                            $createdValues->id,
                            date('Y-m-d H:i:s'),
                            date('Y-m-d H:i:s')
                        ]);
                    $isSaveALl = true;
                } elseif ($bonusSegment == 'losing') {

                    $sqlBonusSetting = 'insert into
                                    losing_bonus_settings (
                                    claim_days,
                                    bonus_id,
                                    created_at,
                                    updated_at) values(?,?,?,?)';
                    DB::insert($sqlBonusSetting,
                        [
                            $input['claim_days'],
                            $createdValues->id,
                            date('Y-m-d H:i:s'),
                            date('Y-m-d H:i:s')
                        ]);
                    $insertIdLosingSettings = DB::getPdo()->lastInsertId();

                    $tiersArray = @$input['tiers'];

                    if ($tiersArray)
                        foreach ($tiersArray as $key => $value) {
                            $sqlBonusSetting = 'insert into
                                    losing_bonus_tiers (
                                    losing_bonus_setting_id,
                                    min_losing_amount,
                                    percentage,
                                    created_at,
                                    updated_at) values(?,?,?,?,?)';
                            DB::insert($sqlBonusSetting,
                                [
                                    $insertIdLosingSettings,
                                    @$value['min_losing_amount'],
                                    @$value['percentage'],
                                    date('Y-m-d H:i:s'),
                                    date('Y-m-d H:i:s')
                                ]);
                        }
                    $isSaveALl = true;
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
        DB::commit();
        if ($isSaveALl) {
            return returnResponse(true, MESSAGE_CREATED, BonusModel::find($createdValues->id), Response::HTTP_CREATED, true);
        } else {
            return returnResponse(false, MESSAGE_CREATED_FAIL, [], Response::HTTP_BAD_REQUEST, true);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $bonusSegment = $request->segment(4);
            // ================= permissions starts here =====================
            if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            if($bonusSegment == 'deposit'){
                $params->module = 'deposit_bonus';
                $params->action = 'U';
            }
            if($bonusSegment == 'losing'){
                $params->module = 'losing_bonus';
                $params->action = 'U';
            }
            $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;

                $permission = checkPermissions($params);
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here =====================          
      
        $input = [];
        $isSaveALl = false;

        $validatorArray = [
            'code' => 'required|alpha_num',
            'currency_id' => 'required|exists:currencies,id',
            'vip_levels' => 'required',
//            'image' => 'required',
            'terms_and_conditions' => 'required',
            'enabled' => 'required',
        ];

        if ($bonusSegment == 'deposit') {
            $validatorArray['kind'] = 'required';
            $validatorArray['percentage'] = 'required';
            $validatorArray['campaign_period'] = 'required';
            $validatorArray['min_deposit'] = 'required';
            $validatorArray['max_deposit'] = 'required';
            $validatorArray['max_bonus'] = 'required';
            $validatorArray['rollover_multiplier'] = 'required';
            $validatorArray['max_rollover_per_bet'] = 'required';
            $validatorArray['valid_for_days'] = 'required';


        } elseif ($bonusSegment == 'losing') {
            $validatorArray['claim_days'] = 'required';
        }


        $validator = Validator::make($request->all(), $validatorArray);

        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
        } else {
            $input = $request->all();
//            du(json_decode(json_encode($input['campaign_period']));
        }
        DB::beginTransaction();
        try {

            $insertArray = [
                'code' => @$input['code'],
                'percentage' => @$input['percentage'],
                'enabled' => $input['enabled'],
                'valid_from' => @$input['campaign_period']['valid_from'],
                'valid_upto' => @$input['campaign_period']['valid_upto'],
                'currency_id' => @$input['currency_id'],
                'promotion_title' => @$input['promotion_title'],
                'terms_and_conditions' => @$input['terms_and_conditions'],
                'vip_levels' => "{" . @implode(',', json_decode($input['vip_levels'])) . "}",
//                'usage_count'=>@$input['usage_count'],
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $bonusSegment = $request->segment(4);

                   // code duplicate 
            $checkCode = BonusModel::where('code',$request->code)->where('tenant_id',Auth::user()->tenant_id)->where('id','!=',$id)->where('kind',$bonusSegment)->first();
            if($checkCode){
                return returnResponse(false, 'Bonus Code can not be duplicate', [], Response::HTTP_INTERNAL_SERVER_ERROR);
            }


            if($bonusSegment=='deposit') {
                $insertArray['kind'] = @$input['kind'];
            }
            /*
            elseif($bonusSegment=='losing'){
                $insertArray['kind']='losing';
            }*/
            if (@$request->file('image')) {

                $aws3 = new Aws3();
                $fileName = Str::uuid() . '____' . $request->file('image')->getClientOriginalName();
                $fileName = str_replace(' ', '_', $fileName);
                $fileNamePath = "tenants/bonus/" . $fileName;

                $PathS3Key = $aws3->uploadFile($fileNamePath, $request->file('image')->getPathname(), $request->file('image')->getClientMimeType());
                $insertArray['image'] = @$PathS3Key;
            }
            $createdValues = BonusModel::where('id', $id)->update($insertArray);

            if ($createdValues) {
                if ($bonusSegment == 'deposit') {

                    $sqlCheckV = "SELECT * FROM deposit_bonus_settings where bonus_id= ?";
                    $settingArray = DB::select($sqlCheckV, [$id]);
                    @$setting = @$settingArray[0];

                    if ($setting) {
                        $sqlBonusSetting = 'update  
                                    deposit_bonus_settings set 
                                    min_deposit = :min_deposit, 
                                    max_deposit = :max_deposit, 
                                    max_bonus = :max_bonus,
                                    rollover_multiplier= :rollover_multiplier,
                                    max_rollover_per_bet =:max_rollover_per_bet,
                                    valid_for_days= :valid_for_days,
                                    updated_at =:updated_at where bonus_id = :bonus_id and id =:id';
                        DB::update($sqlBonusSetting,
                            [
                                "min_deposit" => $input['min_deposit'],
                                "max_deposit" => $input['max_deposit'],
                                "max_bonus" => $input['max_bonus'],
                                "rollover_multiplier" => $input['rollover_multiplier'],
                                "max_rollover_per_bet" => $input['max_rollover_per_bet'],
                                "valid_for_days" => $input['valid_for_days'],
                                "updated_at" => date('Y-m-d H:i:s'),
                                "bonus_id" => $id,
                                "id" => $setting->id
                            ]);
                        $isSaveALl = true;
                    }

                } elseif ($bonusSegment == 'losing') {

                    $sqlCheckV = "SELECT * FROM losing_bonus_settings where bonus_id= ?";
                    $settingArray = DB::select($sqlCheckV, [$id]);
                    @$settingLosing = @$settingArray[0];

                    if ($settingLosing) {

                        if ($input['claim_days'] != $settingLosing->claim_days) {
                            $sqlBonusSetting = 'update  
                                    losing_bonus_settings set 
                                    claim_days = :claim_days, 
                                    updated_at = :updated_at
                                    where id =:id';
                            DB::update($sqlBonusSetting,
                                [
                                    "claim_days" => $input['claim_days'],
                                    "updated_at" => date('Y-m-d H:i:s'),
                                    "id" => $settingLosing->id
                                ]);
                        }
                    }
                    $tiersArray = @$input['tiers'];

                    if ($tiersArray) {
                        DB::table('losing_bonus_tiers')
                            ->where('losing_bonus_setting_id', $settingLosing->id)
                            ->delete();

                        foreach ($tiersArray as $key => $value) {
                            $sqlBonusSetting = 'insert into
                                    losing_bonus_tiers (
                                    losing_bonus_setting_id,
                                    min_losing_amount,
                                    percentage,
                                    created_at,
                                    updated_at) values(?,?,?,?,?)';
                            DB::insert($sqlBonusSetting,
                                [
                                    $settingLosing->id,
                                    $value['min_losing_amount'],
                                    $value['percentage'],
                                    date('Y-m-d H:i:s'),
                                    date('Y-m-d H:i:s')
                                ]);
                        }

                    }
                    $isSaveALl = true;
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
        DB::commit();
        if ($isSaveALl) {
            return returnResponse(true, MESSAGE_UPDATED, BonusModel::find($id), Response::HTTP_CREATED, true);
        } else {
            return returnResponse(false, MESSAGE_UPDATED_FAIL, [], Response::HTTP_BAD_REQUEST, true);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        DB::beginTransaction();
        try {
                $bonusSegment = $request->segment(4);
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                if($bonusSegment == 'deposit'){
                    $params->module = 'deposit_bonus';
                    $params->action = 'D';

                }
                if($bonusSegment == 'losing'){
                    $params->module = 'losing_bonus';
                    $params->action = 'D';
                }
                if($bonusSegment !== 'losing' && $bonusSegment !== 'deposit'){
                    $params->module = 'joining_bonus';
                    $params->action = 'D';
                }
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          


            $bonusModel = BonusModel::where('id', $id)->first();
            if (!$bonusModel) {
                return returnResponse(false, 'record Not found', [], Response::HTTP_NOT_FOUND, true);
            } else {
                if ($bonusSegment == 'deposit') {

                    $sqlCheckV = "delete FROM deposit_bonus_settings where bonus_id= ?";
                    DB::delete($sqlCheckV, [$bonusModel->id]);

                } elseif ($bonusSegment == 'losing') {

                    $sqlCheckV = "delete FROM losing_bonus_tiers where losing_bonus_setting_id= (SELECT id FROM losing_bonus_settings WHERE bonus_id = ?) ";
                    DB::delete($sqlCheckV, [$bonusModel->id]);

                    $sqlCheckV = "delete FROM losing_bonus_settings where bonus_id= ?";
                    DB::delete($sqlCheckV, [$bonusModel->id]);
                }
                BonusModel::where(['id' => $id])->delete();
            }
        } catch (\Exception $e) {

            DB::rollback();

            return returnResponse(false, "You cannot delete the deposit bonus ", [], Response::HTTP_EXPECTATION_FAILED);

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
        DB::commit();
        return returnResponse(true, 'deleted successfully.', [], Response::HTTP_OK, true);
    }


// join bonus
    public function storeJoining(Request $request) {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
        $params = new \stdClass();
        $params->module = 'joining_bonus';
        $params->action = 'C';
        $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

        $permission = checkPermissions($params);   
        if(!$permission){ 
            return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
        }
        }
        // ================= permissions ends here =====================          

        $validatorArray = [
            'code' => 'required|alpha_num',
            'flat' => 'required',
            'promotion_title' => 'required',
            'image' => 'required',
            'terms_and_conditions' => 'required',
            'enabled' => 'required'
        ];

        $validator = Validator::make($request->all(), $validatorArray);

        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
        }
        if(Auth::User()->parent_type !='AdminUser') {
            return returnResponse(false, ERROR_DENIED, [], Response::HTTP_BAD_REQUEST);
        }
        if ($request->enabled == "true" && BonusModel::where([ 'kind' => 'joining', 'enabled' => true, 'tenant_id' => Auth::User()->tenant_id])->exists()) {
            return returnResponse(false, 'Joining bonus already enabled. Please disable it then create.', [], Response::HTTP_BAD_REQUEST);
        }

        DB::beginTransaction();
        try {


               // code duplicate 
            $checkCode = BonusModel::where('code',$request->code)->where('tenant_id',Auth::user()->tenant_id)->where('kind','joining')->first();
            if($checkCode){
                return returnResponse(false, 'Bonus Code can not be duplicate', [], Response::HTTP_INTERNAL_SERVER_ERROR);
            }


            $insertArray = [
                'code' => $request->code,
                'flat' => $request->flat,
                'promotion_title' => $request->promotion_title,
                'terms_and_conditions' => $request->terms_and_conditions,
                'kind' => 'joining',
                'tenant_id' => Auth::User()->tenant_id,
                'enabled' => $request->enabled,
                'created_at' => date('Y-m-d H:i:s')
            ];

            if (@$request->file('image')) {
                $aws3 = new Aws3();
                $fileName = Str::uuid() . '____' . $request->file('image')->getClientOriginalName();
                $fileName = str_replace(' ', '_', $fileName);
                $fileNamePath = "tenants/bonus/" . $fileName;

                $PathS3Key = $aws3->uploadFile($fileNamePath, $request->file('image')->getPathname(), $request->file('image')->getClientMimeType());
                $insertArray['image'] = @$PathS3Key;
            }

           $bonus = BonusModel::create($insertArray);

           DB::commit();
           return returnResponse(true, MESSAGE_CREATED, $bonus, Response::HTTP_CREATED, true);
           
        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    public function getJoining($id) {
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'joining_bonus';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            $data = BonusModel::select(['id', 'code', 'enabled', 'flat', 'image', 'promotion_title', 'terms_and_conditions'])
            ->where('id', $id)->where('kind', '=', 'joining')->where('tenant_id', Auth::User()->tenant_id)->first();

            return returnResponse(true, "Record get Successfully", $data);
        } catch (\Exception $e) {
           
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function updateJoining($id, Request $request) {

        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
        $params = new \stdClass();
        $params->module = 'joining_bonus';
        $params->action = 'U';
        $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

        $permission = checkPermissions($params);   
        if(!$permission){ 
            return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
        }
        }
        // ================= permissions ends here =====================          

        $validatorArray = [
            'code' => 'required|alpha_num',
            'flat' => 'required',
            'promotion_title' => 'required',
            'terms_and_conditions' => 'required',
            'enabled' => 'required'
        ];

        $validator = Validator::make($request->all(), $validatorArray);

        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
        }

        $bonus = BonusModel::find($id);

        if(empty($bonus)) {
            return returnResponse(true, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED, true);;
        }

        if ($request->enabled == "true" && BonusModel::where([ 'kind' => 'joining', 'enabled' => true ])->whereNotIn('id', [$bonus->id])->exists()) {
            return returnResponse(false, 'Joining bonus already enabled. Please disable it then update.', [], Response::HTTP_BAD_REQUEST);
        }

            // code duplicate 
        $checkCode = BonusModel::where('code',$request->code)->where('tenant_id',Auth::user()->tenant_id)->where('id','!=',$id)->where('kind','joining')->first();
        if($checkCode){
            return returnResponse(false, 'Bonus Code can not be duplicate', [], Response::HTTP_INTERNAL_SERVER_ERROR);
        }


        DB::beginTransaction();
        try {

            $insertArray = [
                'code' => $request->code,
                'flat' => $request->flat,
                'promotion_title' => $request->promotion_title,
                'terms_and_conditions' => $request->terms_and_conditions,
                'kind' => 'joining',
                'tenant_id' => Auth::User()->tenant_id,
                'enabled' => $request->enabled
            ];

            if (@$request->file('image')) {
                $aws3 = new Aws3();
                $fileName = Str::uuid() . '____' . $request->file('image')->getClientOriginalName();
                $fileName = str_replace(' ', '_', $fileName);
                $fileNamePath = "tenants/bonus/" . $fileName;

                $PathS3Key = $aws3->uploadFile($fileNamePath, $request->file('image')->getPathname(), $request->file('image')->getClientMimeType());
                $insertArray['image'] = @$PathS3Key;
            }

           $bonus->update($insertArray);

           DB::commit();
           return returnResponse(true, MESSAGE_UPDATED, $bonus, Response::HTTP_OK, true);
           
        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    public function updateJoiningStatus(Request $request) {

        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
        $params = new \stdClass();
        $params->module = 'joining_bonus';
        $params->action = 'T';
        $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

        $permission = checkPermissions($params);   
        if(!$permission){ 
            return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
        }
        }
        // ================= permissions ends here =====================          
      
        $validatorArray = [
            'enabled' => 'required'
        ];

        $validator = Validator::make($request->all(), $validatorArray);

        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
        }

        $bonus = BonusModel::find($request->id);

        if(empty($bonus)) {
            return returnResponse(true, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED, true);;
        }

        if ($request->enabled == "true" && BonusModel::where([ 'kind' => 'joining', 'enabled' => true,'tenant_id' => Auth::user()->tenant_id ])->whereNotIn('id', [$bonus->id])->exists()) {
            return returnResponse(false, 'Joining bonus already enabled. Please Disable Enabled joining bonus then update.', [], Response::HTTP_BAD_REQUEST);
        }

        DB::beginTransaction();
        try {

           $bonus->update( ['enabled' => $request->enabled?false:true ]);

           DB::commit();
           return returnResponse(true, MESSAGE_UPDATED, $bonus, Response::HTTP_OK, true);
           
        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }


    public function updateDepositAndLosingStatus(Request $request) {

        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
        $params = new \stdClass();
        $params->module = 'losing_bonus';
        $params->action = 'T';
        $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

        $permission = checkPermissions($params);   
        if(!$permission){ 
            $param1 = new \stdClass();
            $param1->module = 'deposit_bonus';
            $param1->action = 'T';
            $param1->admin_user_id = Auth::user()->id;
            $param1->tenant_id = Auth::user()->tenant_id;
            $permission1 = checkPermissions($param1);
            if(!$permission1){
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
        }
        }
        // ================= permissions ends here =====================          

        $validatorArray = [
            'enabled' => 'required'
        ];

        $validator = Validator::make($request->all(), $validatorArray);

        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
        }

        $bonus = BonusModel::find($request->id);

        if(empty($bonus)) {
            return returnResponse(true, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED, true);;
        }

        // if ($request->enabled == "true" && BonusModel::where([ 'kind' => 'deposit', 'enabled' => true,'tenant_id' => Auth::user()->tenant_id ])->whereNotIn('id', [$bonus->id])->exists()) {
        //     return returnResponse(false, 'Deposit bonus already enabled. Please Disable Enabled deposit bonus then update.', [], Response::HTTP_BAD_REQUEST);
        // }

        DB::beginTransaction();
        try {
           $bonus->update( ['enabled' => $request->enabled?false:true ]);

           DB::commit();
           return returnResponse(true, MESSAGE_UPDATED, $bonus, Response::HTTP_OK, true);
           
        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

}
