<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Themes;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use Illuminate\Support\Facades\App;

class ThemesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $record = Themes::orderBy('name', 'asc')->get();
            return returnResponse(true, "Record get Successfully", $record);
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'options' => 'required',
        ]);

        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), 400);
        }

        $insertData = [
            'name' => $request->name,
            'options' => json_encode($request->options),
        ];
        try {
            $createdValues = Themes::create($insertData);
            if ($createdValues) {
                return returnResponse(true, 'insert successfully.', $insertData, 200, true);
            } else {
                return returnResponse(false, 'insert unsuccessfully.', [], 403, true);
            }
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Themes $themes
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if ($id == '') {
                return returnResponse(true, "Record get Successfully", Themes::all());
            } else {
                return returnResponse(true, "Record get Successfully", Themes::find($id)->toArray(), 200, true);
            }
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Themes $themes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Themes $themes)
    {

        try {
            $themes = $themes->where('id', $request->id)->first();
            $themesArray = $themes->toArray();
            if (!count($themesArray)) {
                return returnResponse(false, 'record Not found', [], 404, true);
            } else {
                $input = $request->all();
                $postData['name'] = $input['name'];
                $postData['options'] = json_encode($input['options']);

                Themes::where(['id' => $request->id])->update($postData);

                return returnResponse(true, 'Update successfully.', Themes::find($request->id), 200, true);
            }
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Themes $themes
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $themes = Themes::where('id', $id)->first();
            $themesArray = $themes->toArray();
            if (!count($themesArray)) {
                return returnResponse(false, 'record Not found', [], 404, true);
            } else {

                Themes::where(['id' => $id])->delete();
                return returnResponse(true, 'deleted successfully.', [], 200, true);
            }
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

}
