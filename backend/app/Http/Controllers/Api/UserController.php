<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\CommonEmail;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use App\Models\Transactions;
use App\Models\User;
use App\Models\UserToken;
use App\Models\Admin;
use App\Models\SuperMailConfiguration;
use App\Models\Wallets;
use App\Models\QueueLog;
use App\Models\Tenants;
use App\Models\UserBankDeatils;
use App\Models\UserLoginHistory;
use Illuminate\Mail\MailServiceProvider;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Validator;
use Illuminate\Support\Facades\Redis;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function index()
    {
        //
    }*/

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
        $params = new \stdClass();
        $params->module = 'players';
        $params->action = 'C';
        $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;
        $permission = checkPermissions($params);   
        if(!$permission){ 
            return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
        }
        }
        // ================= permissions ends here =====================          
      

        if (Auth::user()->parent_type != 'AdminUser') {
            return returnResponse(false, 'Permission Denied', [], Response::HTTP_FORBIDDEN);
        }
        $isSave = false;
        try {

            $validatorArray = [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email',
                'user_name' => 'required',
                'encrypted_password' => 'required',
                'phone' => 'required',
                // 'nick_name' => 'required',
                // 'phone_verified' => 'required',
                'currency_id' => 'required',
                'country_code' => 'required',
                'date_of_birth' => 'required',
                 'city' => 'required',

                'vip_level' => 'required',
                'zip_code' => 'required'
            ];
            if (Auth::user()->parent_type != 'AdminUser') {
                $validatorArray['tenant_id'] = 'required';
            }else{
                $request->tenant_id = Auth::user()->tenant_id;
            }
            $validator = Validator::make($request->all(), $validatorArray);

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
            } else {
//                $input=$request->all();

                $checkPhone = checkPhoneForTenant($request->phone, $request->tenant_id, PLAYER);

                if ($checkPhone == 0) {
                    // $input = $request->all();
                } else {
                    return returnResponse(false, '', ["phone" => [
                      PHONE_EXIST
                    ]], Response::HTTP_BAD_REQUEST);
                }

                $checkEmail = checkEmailForTenant($request->email, $request->tenant_id, PLAYER);

                if ($checkEmail == 0) {
                    $input = $request->all();
                } else {
                    return returnResponse(false, '', ["email" => [
                        USER_EXIST
                    ]], Response::HTTP_BAD_REQUEST);
                }
                $checkUserName = User::where('tenant_id', $request->tenant_id)
                    ->where(DB::raw('lower(user_name)'), '=', strtolower($request->user_name))
                    ->select('id')->get()->count();

                if ($checkUserName != 0) {
                    return returnResponse(false, '', ["user_name" => [
                        USER_EXIST
                    ]], Response::HTTP_BAD_REQUEST);
                }

                if (trim($request->user_name) == trim($request->nick_name)) {
                    return returnResponse(false, '', ["user_name" => [
                        USER_NICK_CHECK
                    ]], Response::HTTP_BAD_REQUEST);
                }

            }

            $years = Carbon::parse($input['date_of_birth'])->age;

            if ($years < 18) {
                return returnResponse(false, '', ["date_of_birth" => [
                    "Date of birth Age must be 18 years or above."
                ]], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();

            $insertData = [
                'first_name' => @$input['first_name'],
                'last_name' => @$input['last_name'],
                'email' => @$input['email'],
                'encrypted_password' => md5(base64_decode(@$input['encrypted_password'])),
                'phone' => @$input['phone'],
                'zip_code' => @$input['zip_code'],
                'city' => @$input['city'],
                'phone_verified' => @$input['phone_verified'],
                'email_verified' => @$input['email_verified'],
                'date_of_birth' => @$input['date_of_birth'],
                'gender' => @$input['gender'],
                'locale' => @$input['locale'],
                'nick_name' => @$input['nick_name'],
                'user_name' => @$input['user_name'],
                'country_code' => @$input['country_code'],
                'parent_type' => ADMIN_TYPE,
                'parent_id' => @Auth::user()->id,
                'tenant_id' => @Auth::user()->tenant_id,
                'vip_level' => @$input['vip_level'],
                'active' => @$input['active']
            ];
            
            if(Auth::User()->parent_type =='AdminUser') {
                $insertData['kyc_done'] = Auth::User()->kyc_regulated ? null : true;
            }
            $createdValues = User::create($insertData);
//            du($insertData);
            if ($createdValues) {
                $isWallets=Wallets::where('owner_id',$createdValues->id)
                                    ->where('owner_type' , USER_TYPE)
                                    ->get();

                if($isWallets->toArray()) {
                    Wallets::where('owner_id',$createdValues->id)
                        ->where('owner_type' , USER_TYPE)
                        ->delete();
                }
                $insertWallets['owner_id'] = $createdValues->id;
                $insertWallets['currency_id'] = $input['currency_id'];
                $insertWallets['owner_type'] = USER_TYPE;
                Wallets::create($insertWallets);
                $isSave = true;

               // User::storeElasticSearchData($createdValues->id);

            }
            $queueLog = QueueLog::create([
                'type' => USER_TRANSACTION,
                'ids' => json_encode([$createdValues->id])
            ]);
            DB::commit();
           
            try {
                $redis = Redis::connection();
                $jsonArray=[];
                $jsonArray['QueueLog']= [
                    'queueLogId'=> $queueLog->id
                ];
                $redis->publish('QUEUE_WORKER', json_encode($jsonArray)
                );
            } catch (\Throwable $th) {
            //throw $th;
            }
        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }


        // Enter the data into the Elastic search index.
        // for development purpose, I have commented for time being
        // User::storeElasticSearchData($createdValues->id);


        if ($isSave) {
            return returnResponse(true, 'Created Successfully.', [], Response::HTTP_CREATED, true);

        } else {

            return returnResponse(false, 'Create not Successfully.', [], Response::HTTP_BAD_REQUEST, true);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    /* public function show($id)
     {

     }*/

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
//        if (Auth::user()->parent_type != 'AdminUser' ) {
//            return returnResponse(false, 'Permission Denied', [], Response::HTTP_UNAUTHORIZED);
//        }
        $isSave = false;
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'players';
                $params->action = 'U';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;
                $permission = checkPermissions($params);   
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          

            $validatorArray = [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email',
//                'user_name' => 'required|unique:admin_users', need to work
                // 'encrypted_password' => 'required',
                'phone' => 'required',
                'city' => 'required',
//                'currency_id' => 'required',
                'country_code' => 'required',
                'date_of_birth' => 'required',
//                'gender' => 'required',
                'vip_level' => 'required',
                'zip_code' => 'required'
            ];
            $tenant_id = @Auth::user()->tenant_id;
            if (Auth::user()->parent_type == 'SuperAdminUser') {
               // $validatorArray['tenant_id'] = 'required';
               $user = User::find($request->id);
               $tenant_id = $user->tenant_id;
            }elseif (Auth::user()->parent_type != 'AdminUser') {
                $validatorArray['tenant_id'] = 'required';
            }
            $validator = Validator::make($request->all(), $validatorArray);

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
            } else {
                $input = $request->all();
            }
            $checkPhone = User::where('tenant_id', $tenant_id)->where('phone', $request->phone)->where("id",'!=',$request->id)->select('id')->count();
                if ($checkPhone == 0) {
                    // $input = $request->all();
                } else {
                    return returnResponse(false,'' , ["phone" => [
                      PHONE_EXIST
                    ]], Response::HTTP_BAD_REQUEST);
                }

            $checkUserName = User::where('tenant_id', $tenant_id)->where("id",'!=',$request->id)
                ->where(DB::raw('lower(user_name)'), '=', strtolower($request->user_name))
                ->select('id')->get()->count();

            if ($checkUserName != 0) {
                return returnResponse(false, '', ["user_name" => [
                    USER_EXIST
                ]], Response::HTTP_BAD_REQUEST);
            }

            $years = Carbon::parse($input['date_of_birth'])->age;

            if ($years < 18) {
                return returnResponse(false, '', ["date_of_birth" => [
                    "Date of birth Age must be 18 years or above."
                ]], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();

            $insertData = [
                'first_name' => @$input['first_name'],
                'last_name' => @$input['last_name'],
                'email' => @$input['email'],
                'city'=>@$input['city'],
                'phone' => @$input['phone'],
                'zip_code'=> @$input['zip_code'],
                'phone_verified' => @$input['phone_verified'],
                'email_verified' => @$input['email_verified'],
                'date_of_birth' => @$input['date_of_birth'],
                'gender' => @$input['gender'],
                'locale' => @$input['locale'],
                'nick_name' => @$input['nick_name'],
                'user_name' => @$input['user_name'],
                'country_code' => @$input['country_code'],
                /*'parent_type'=>ADMIN_TYPE,
                'parent_id'=>@Auth::user()->id,  // This should not be the case because player information can be change by any of their parent.
                'tenant_id'=>@Auth::user()->tenant_id,*/
                'vip_level' => @$input['vip_level'],
                'active' => @$input['active'],
                'demo' => @$input['demo'],
            ];

            if (!empty(@$input['encrypted_password'])) {
                $insertData['encrypted_password'] = md5(base64_decode(@$input['encrypted_password']));
            }
            $whereArray=[];
            $whereArray['id']= $id = @$input['id'];

            if(@Auth::user()->tenant_id){
                $whereArray['tenant_id'] = @Auth::user()->tenant_id;
            }
            $createdValues = User::where($whereArray)
                ->update($insertData);

            if ($createdValues) {
//                $insertWallets['owner_id'] = $id;
//                $insertWallets['currency_id'] = $input['currency_id'];
//                Wallets::where('owner_id', $id)->update($insertWallets);
                $isSave = true;

            }
            //User::storeElasticSearchData($id);
            
            $queueLog = QueueLog::create([
                'type' => USER_TRANSACTION,
                'ids' => json_encode([$id])
            ]);
            DB::commit();
           
            try {
                $redis = Redis::connection();
                $jsonArray=[];
                $jsonArray['QueueLog']= [
                    'queueLogId'=> $queueLog->id
                ];
                $redis->publish('QUEUE_WORKER', json_encode($jsonArray)
                );
            } catch (\Throwable $th) {
            //throw $th;
            }
        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
        if ($isSave) {
            //User::storeElasticSearchData($id);
            return returnResponse(true, 'Update successfully.', [], Response::HTTP_OK, true);

        } else {

            return returnResponse(false, 'Update not successfully.', [], Response::HTTP_BAD_REQUEST, true);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function statusUserPlayer($id, $status)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
        $params = new \stdClass();
        $params->module = 'players';
        $params->action = 'T';
        $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;
        $permission = checkPermissions($params);   
        if(!$permission){ 
            return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
        }
        }
        // ================= permissions ends here =====================          
      
      
        // if (Auth::user()->parent_type != 'AdminUser') {
        //     return returnResponse(false, 'Permission Denied', [], Response::HTTP_FORBIDDEN);
        // }
        try {

            $findRecord = User::find($id);

            if ($findRecord && count($findRecord->toArray()) > 0) {
                User::where('id', $id)->update(['active' => $status]);
                // User::storeElasticSearchData($id);
              $getMailConfiguration = [];

                // Mail Send Part 
                if (Auth::user()->parent_type == 'SuperAdminUser') {
                  $deactivated_by = "Super Admin";
                  // By Super Admin User Send Mail to Player
                  $getMailConfiguration = SuperMailConfiguration::get();
                  foreach ($getMailConfiguration as $key => $value) {

                    if($value->key_provided_by_account == 'APP_SENDGRID_PORT')
                    Config::set('mail.mailers.smtp.post', $getMailConfiguration[$key]->value);

                    if($value->key_provided_by_account == 'APP_SENDGRID_HOST')
                    Config::set('mail.mailers.smtp.host', $getMailConfiguration[$key]->value);

                    if($value->key_provided_by_account == 'APP_SENDGRID_USERNAME')
                    Config::set('mail.mailers.smtp.username', $getMailConfiguration[$key]->value);

                    if($value->key_provided_by_account == 'APP_SENDGRID_RELAY_KEY')
                    Config::set('mail.mailers.smtp.password', $getMailConfiguration[$key]->value);
                    
                  }
                  

                    
              }else{
                $deactivated_by = "Admin";
                  // By Tenant or Agent send Mail to Player 
                  $getMailConfiguration = DB::table('tenant_credentials')->where('tenant_id',Auth::user()->tenant_id)->get();
                    foreach ($getMailConfiguration as $key => $value) {

                      if($value->key == 'APP_SENDGRID_PORT')
                      Config::set('mail.mailers.smtp.post', $getMailConfiguration[$key]->value);

                      if($value->key == 'APP_SENDGRID_HOST')
                      Config::set('mail.mailers.smtp.host', $getMailConfiguration[$key]->value);

                      if($value->key == 'APP_SENDGRID_USERNAME')
                      Config::set('mail.mailers.smtp.username', $getMailConfiguration[$key]->value);

                      if($value->key == 'APP_SENDGRID_RELAY_KEY')
                      Config::set('mail.mailers.smtp.password', $getMailConfiguration[$key]->value);
                      
                    }
                  
                }

                if ($status == true) {
                  $message = "Active Successfully";
                  $mail_subject = "Your Profile is active Now.";
                  $data['status'] = "Activated";
                } else {
                  $message = "Inactive Successfully";
                  $mail_subject = "Your Profile is deactivated by ".$deactivated_by.".";
                  $data['status'] = "Deactivated";
                }


              $randomKey = Str::uuid()->toString();

              $data['randomKey'] = $randomKey;
              $data['id'] = $id;
              $data['user_name'] = $findRecord->user_name;
              $data['host'] = Config::get('mail.host');
              $data['deactivated_by'] = $deactivated_by;
              $findRecord['mail_subject'] = $mail_subject;

              try {
                Mail::send('emails.player_profile_status', $data, function($message) use ($findRecord) {
                  $message->to($findRecord->email);
                  $message->subject($findRecord['mail_subject']);
                });
              } catch (\Throwable $th) {
                //Do nothing... if any error occurs...
              }

              return returnResponse(true, $message, [], Response::HTTP_OK, true);


                
            }
            throw new \Exception('Record not found ');


        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPlayer($user_id, Request $request)
    {

        try {
                // ================= permissions starts here =====================
            if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'players';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;
                $permission = checkPermissions($params);   
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================    
            $findRecord = Admin::getByPlayersID($user_id);

            if ($findRecord) {

                $transactionRecord = Transactions::getDepositAmount($user_id, TRANSACTION_TYPE_DEPOSIT);
                @$findRecord->last_deposited_amount = @$transactionRecord[0]->last_deposited_amount;
                @$findRecord->last_updated_at = @$transactionRecord[0]->updated_at;


                $transactionRecord = Transactions::getDepositAmountUser($user_id, MESSAGE_DEPOSIT_CLAIM);

                @$findRecord->nocash_bonus_last_amount = (float)@$transactionRecord[0]->last_deposited_amount;
//                @$findRecord->nocash_bonus_updated_at = @$transactionRecord[0]->updated_at;

                return returnResponse(true, 'Record Get Successfully.', $findRecord, Response::HTTP_OK);
            }
            throw new \Exception('Record not found ');


        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginHistory(Request $request, $id)
    {

        $userLoginHistory = UserLoginHistory::on('read_db')->where('user_id', $id)->orderBy('created_at', 'DESC')->paginate($request->size);
        return returnResponse(true, MESSAGE_GET_SUCCESS, $userLoginHistory, Response::HTTP_OK);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function bankDetails(Request $request, $id)
    {
            try {
                  $permission_allowed=true;
                  // ================= permissions starts here =====================
                  if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                      $params = new \stdClass();
                      $params->module = 'player_bank_details';
                      $params->action = 'R';
                      $params->admin_user_id = Auth::user()->id;
                      $params->tenant_id = Auth::user()->tenant_id;

                      $permission = checkPermissions($params);
                      if(!$permission){
                          $permission_allowed = false;
                      }
                  }
              // ================= permissions ends here =====================
              $bankDetails=[];
              if($permission_allowed)
                  $bankDetails = DB::connection('read_db')->table('user_bank_details')->where('user_id', $id)->orderBy('created_at', 'DESC')->paginate($request->size);

              return returnResponse(true, MESSAGE_GET_SUCCESS, $bankDetails, Response::HTTP_OK);
            } catch (\Exception $e) {
              if (!App::environment(['local'])) { //, 'staging'
                  return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
              } else {
                  return returnResponse(false, ERROR_MESSAGE, [
                      'Error' => $e->getMessage(),
                      'LineNo' => $e->getLine(),
                      'FileName' => $e->getFile()
                  ], Response::HTTP_EXPECTATION_FAILED);
              }
          }
    }

    public function searchUsers(Request $request)
    {
        try {
            $input = $request->all();

            $validatorArray = [
                'owner_type' => 'required',
                'search' => 'required',
            ];


            $validator = Validator::make($request->all(), $validatorArray);

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
            } else {
                $input = $request->all();
            }
            $roleDetails =  getRolesDetails(Auth::user()->id);
            if ($input['owner_type'] == 'AdminUser' && @$input['search'] && $input['search'] != '') {
                if( isset($roleDetails[0]) && $roleDetails[0] == "agent"){
                  $findRecord = Admin::getAgentEmails($input['search']);
                }else{
                  $findRecord = Admin::getEmails($input['search']);
                }

            } elseif ($input['owner_type'] == 'User' && @$input['search'] && $input['search'] != '') {

                if(isset($roleDetails[0]) && $roleDetails[0] == "agent"){
                  $findRecord = User::getPlayerEmails($input['search']);
                }else{
                  $findRecord = User::getEmails($input['search']);
                }
            } else {
                $findRecord = [];
            }

            if ($findRecord) {
                return returnResponse(true, 'Record Get Successfully.', $findRecord, Response::HTTP_OK);
            } else {
                return returnResponse(true, 'Record not found.', $findRecord, Response::HTTP_OK);
            }
//            throw new \Exception('Record not found ');

        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function getwalletsDetails($id, Request $request)
    {
        try {
            $roles = getRolesDetails($id);
            // ================= permissions starts here =====================
            if (request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager") {
                $params = new \stdClass();

                if ($request->type === 'User') {
                    $params->module = 'players';
                    $params->action = 'R';
                }
                if ($request->type === 'AdminUser') {
                    if ($roles[0] === 'agent') {
                        $params->module = 'agents';
                        $params->action = 'R';
                    }
                    if ($roles[0] === 'owner'||$roles[0] === 'sub-admin' || $roles[0] === 'deposit-admin' || $roles[0] === 'withdrawal-admin') {
                        $params->module = 'sub_admin';
                        $params->action = 'R';
                    }
                }
                $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;

                $permission = checkPermissions($params);
                if (!$permission) {
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
            }
            // ================= permissions ends here =====================


            $userType = $request->type ?? null;

            if (!$id) {
                throw new \Exception('id not found ');
            }
            $data = getWalletDetails($id, $userType);
            return returnResponse(true, 'Record Get Successfully.', $data, Response::HTTP_OK);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function updateKyc(Request $request) {
     try {
            // ================= permissions starts here =====================
            if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'players';
            $params->action = 'U';
            $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;
            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here =====================          
      

            if (empty($request->id)) {
                return returnResponse(false, 'Id and Kyc not found', [], Response::HTTP_EXPECTATION_FAILED);
            }
            
            User::where('id', $request->id)->update(['kyc_done' => $request->kyc_done]);
            //User::storeElasticSearchData($request->id);
            //Entry in queueLog
            $queueLog = QueueLog::create([
                'type' => USER_TRANSACTION,
                'ids' => json_encode([$request->id])
            ]);
            try {
                $redis = Redis::connection();
                $jsonArray=[];
                $jsonArray['QueueLog']= [
                    'queueLogId'=> $queueLog->id
                ];
                $redis->publish('QUEUE_WORKER', json_encode($jsonArray)
                );
            } catch (\Throwable $th) {
            //throw $th;
            }
            if($request->kyc_done){
                return returnResponse(true, 'KYC status changed to DONE for this user', [], Response::HTTP_OK);
            }else{
                return returnResponse(true, 'KYC status changed to NOT DONE for this user', [], Response::HTTP_EXPECTATION_FAILED);
            }

        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }   
    }
    
    
    public function selfExclusionUpdate(Request $request) {
     try {

            if (empty($request->id)) {
                return returnResponse(false, 'Player not found', [], Response::HTTP_EXPECTATION_FAILED);
            }
            
            $update  = User::where('id', $request->id)->update(['self_exclusion' => null]);
            //User::storeElasticSearchData($request->id);
            //Entry in queueLog
            $queueLog = QueueLog::create([
                'type' => USER_TRANSACTION,
                'ids' => json_encode([$request->id])
            ]);
            try {
                $redis = Redis::connection();
                $jsonArray=[];
                $jsonArray['QueueLog']= [
                    'queueLogId'=> $queueLog->id
                ];
                $redis->publish('QUEUE_WORKER', json_encode($jsonArray)
                );
            } catch (\Throwable $th) {
            //throw $th;
            }
            if($update){
                return returnResponse(true, 'Self Exclusion reset successfully', [], Response::HTTP_OK);
            }else{
                return returnResponse(true, 'Something went wrong.', [], Response::HTTP_EXPECTATION_FAILED);
            }

        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }   
    }

    public function resetPassword(Request $request) {
        try {
            $validator = Validator::make(
            $request->all(), [ 'id' => 'required' ] );

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_EXPECTATION_FAILED);
            }

            $user = User::find($request->id);

            if(empty($user)) {
                return returnResponse(false, 'User Not Found', [], Response::HTTP_NOT_FOUND);
            }

            $randomKey = Str::uuid()->toString();

            $data['randomKey'] = $randomKey;
            $data['id'] = $user->id;
            $data['user_name'] = $user->user_name;
            $tenant_details = Tenants::on('read_db')->where('id', Auth::user()->tenant_id)->select('id','domain','name')->first();
            $data['host'] = $tenant_details->domain . '/';
            $defaultFromAddress = config('mail.from.address');
            Mail::send('emails.player_reset_password', $data, function($message) use ($user, $tenant_details, $defaultFromAddress) {
                $message->to($user->email);
                $message->subject('Reset password');
                $message->from($defaultFromAddress, $tenant_details->name);
            });
            
            UserToken::create(array('token_type' => 'passwordReset', 'token' => $randomKey, 'user_id' => $user->id));
            
            return returnResponse(true, 'Email is sent for reset password!', [], Response::HTTP_OK);

        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function editPlayers(Request $request){
        try {
            $validatorArray = [
                'ids' => 'required',
            ];
            $validator = Validator::make($request->all(), $validatorArray);

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
            }
            $updates = [];

            if ($request->filled('status')) {
                $updates['active'] = $request->status;
            }

            if ($request->filled('vip_level')) {
                $updates['vip_level'] = $request->vip_level;
            }

            User::whereIn('id', $request->ids)->update($updates);

            return returnResponse(true, 'Players updated successfully.', [], Response::HTTP_OK);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) { //, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, [
                    'Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }
    public function updateBankDetails(Request $request, $bankId , $playerId){
        try {
                // ================= permissions starts here =====================
              if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'player_bank_details';
                $params->action = 'U';
                $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;
                $permission = checkPermissions($params);
                if(!$permission){
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
            }
            // ================= permissions ends here =====================

            $messages = array(
                'account_holder_name.required' => 'Account holder name is required',
                'account_holder_name.regex' => 'Account holder name must be valid',
                'account_number.required' => 'Account number is required',
                'account_number.regex' => 'Account number must be valid',
                'bank_name.required' => 'Bank name is required',
                'bank_name.regex' => 'Bank name must be valid',
                'ifsc_code.required' => 'Ifsc code is required',
                'ifsc_code.regex' => 'Ifsc code must be valid',
            );

            $validator = Validator::make($request->all(), [
                'account_holder_name' =>   ['required', 'regex:/^[a-zA-Z_ ]+$/'],
                'account_number' => ['required', 'regex:/^[0-9]{9,18}$/'],
                'bank_name' => ['required', 'regex:/^[a-zA-Z ]+$/'],
                'ifsc_code' => ['required', 'regex:/^[A-Za-z]{4}[a-zA-Z0-9]{7}$/'],
            ], $messages);
            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();
            $updateData = [
                'name' => $request->account_holder_name,
                'account_number' => $request->account_number,
                'status' => $request->active == true ? 'active':'inactive',
                'bank_name' => $request->bank_name,
                'bank_ifsc_code' => $request->ifsc_code
            ];

            $updatedValues = UserBankDeatils::where('id', $bankId)->where('user_id', $playerId)->where('tenant_id', Auth::user()->tenant_id)->update($updateData);
            if($updatedValues){
              DB::commit();
                return returnResponse(true, 'Bank detail updated successfully', [], Response::HTTP_OK);
            }else{
              DB::rollback();
                return returnResponse(false, 'Bank detail not updated', [], Response::HTTP_OK);
            }
        } catch (\Exception $e) {
          DB::rollback();
          if (!App::environment(['local'])) { //, 'staging'
              return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
          } else {
              return returnResponse(false, ERROR_MESSAGE, [
                  'Error' => $e->getMessage(),
                  'LineNo' => $e->getLine(),
                  'FileName' => $e->getFile()
              ], Response::HTTP_EXPECTATION_FAILED);
          }
      }
    }

    public function createBankDetails(Request $request, $id){
        try {
                // ================= permissions starts here =====================
              if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'player_bank_details';
                $params->action = 'C';
                $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;
                $permission = checkPermissions($params);
                if(!$permission){
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
            }
            // ================= permissions ends here =====================
            $messages = array(
                'account_holder_name.required' => 'Account holder name is required',
                'account_holder_name.regex' => 'Account holder name must be valid',
                'account_number.required' => 'Account number is required',
                'account_number.regex' => 'Account number must be valid',
                'bank_name.required' => 'Bank name is required',
                'bank_name.regex' => 'Bank name must be valid',
                'ifsc_code.required' => 'Ifsc code is required',
                'ifsc_code.regex' => 'Ifsc code must be valid',
            );

            $validator = Validator::make($request->all(), [
                'account_holder_name' =>   ['required', 'regex:/^[a-zA-Z_ ]+$/'],
                'account_number' => ['required', 'regex:/^[0-9]{9,18}$/'],
                'bank_name' => ['required', 'regex:/^[a-zA-Z ]+$/'],
                'ifsc_code' => ['required', 'regex:/^[A-Za-z]{4}[a-zA-Z0-9]{7}$/'],
            ], $messages);
            if ($validator->fails()) {
                $errors = $validator->errors()->all();

                return response()->json([
                    'success' => false,
                    'data' => '',
                    'message' => implode(PHP_EOL, $errors)
                ], 400);
            }
            DB::beginTransaction();
            $createData = [
                'name' => $request->account_holder_name,
                'account_number' => $request->account_number,
                'status' => $request->active == true ? 'active':'inactive',
                'bank_name' => $request->bank_name,
                'tenant_id' => Auth::user()->tenant_id,
                'user_id' => $id,
                'bank_ifsc_code' => $request->ifsc_code
            ];
            
            $createdValues = UserBankDeatils::create($createData);
            if($createdValues){
              DB::commit();
                return returnResponse(true, 'Bank detail created successfully', [], Response::HTTP_OK);
            }else{
              DB::rollback();
                return returnResponse(false, 'Bank detail not created', [], Response::HTTP_OK);
            }
        } catch (\Exception $e) {
          DB::rollback();
          if (!App::environment(['local'])) { //, 'staging'
              return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
          } else {
              return returnResponse(false, ERROR_MESSAGE, [
                  'Error' => $e->getMessage(),
                  'LineNo' => $e->getLine(),
                  'FileName' => $e->getFile()
              ], Response::HTTP_EXPECTATION_FAILED);
          }
      }
    }
}
