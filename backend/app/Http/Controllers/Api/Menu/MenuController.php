<?php

namespace App\Http\Controllers\Api\Menu;

use App\Http\Controllers\Controller;
use App\Models\Menu\MenuMasterModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Aws3;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $record = MenuMasterModel::all()->sortBy("id");
            return returnResponse(true, MESSAGE_GET_SUCCESS, array_values($record->toArray()));
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, [
                    'Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'path' => 'required|unique:menu_master',
                'active' => 'required',
            ]);

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
            }


            $insertData = [
                'name' => $request->name,
                'path' => $request->path,
                'active' => $request->active?true:false,
            ];
            $createdValues = MenuMasterModel::create($insertData);
            if ($createdValues) {
                return returnResponse(true, MESSAGE_CREATED, $insertData, Response::HTTP_OK, true);
            } else {
                return returnResponse(false, MESSAGE_CREATED_FAIL, [], Response::HTTP_NOT_MODIFIED, true);
            }
        } catch (\Exception $e) {
            
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MenuMasterModel $languages
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if ($id == '') {
                return returnResponse(true, MESSAGE_GET_SUCCESS, MenuMasterModel::all());
            } else {
                return returnResponse(true, MESSAGE_GET_SUCCESS, MenuMasterModel::find($id)->toArray(), 200, true);
            }
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @description Update the specified resource in storage.
     * @param Request $request
     * @param MenuMasterModel $menu
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, MenuMasterModel $menu)
    {
        try {
            $menu = $menu->where('id', $request->id)->first();
            $menuArray = $menu->toArray();
            if (!count($menuArray)) {
                return returnResponse(false, MESSAGE_RECORD_NOT_FOUND, [], 404, true);
            } else {
                $input = $request->all();
                $validatorArray = [];
                $postData = [];
                if (@$input['name'] != $menuArray['name']) {
                    $validatorArray['name'] = 'required';
                    $postData['name'] = @$input['name'];
                }
                if (@$input['path'] != $menuArray['path']) {
                    $validatorArray['path'] = 'required|unique:menu_master';
                    $postData['path'] = @$input['path'];
                }
                if (@$input['active'] != $menuArray['active']) {
                    $validatorArray['active'] = 'required';
                    $postData['active'] = @$input['active'];
                }
                if (count($validatorArray)) {
                    $validator = Validator::make($request->all(), $validatorArray);
                    if ($validator->fails()) {
                        return returnResponse(false, '', $validator->errors(), 400);
                    }
                }

                if (count($postData)) {
                    MenuMasterModel::where(['id' => $request->id])->update($postData);
                }

                return returnResponse(true, MESSAGE_UPDATED, MenuMasterModel::find($request->id), 200, true);

            }
        } catch (\Exception $e) {
        
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Languages $languages
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $menu = MenuMasterModel::where('id', $id)->first();
            $menuArray = $menu->toArray();
            if (!count($menuArray)) {
                return returnResponse(false, MESSAGE_RECORD_NOT_FOUND, [], 404, true);
            } else {
                MenuMasterModel::where(['id' => $id])->delete();
                return returnResponse(true, MESSAGE_DELETE, [], 200, true);
            }
        } catch (\Exception $e) {
           
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, "Not delete this due to some tenant used this ", [], Response::HTTP_EXPECTATION_FAILED);
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function updateIcon(Request $request){
        $messages = array(
            'image.required' => 'SENTENCES.IMAGE_REQUIRED',
        );


        $validator = Validator::make($request->all(), [
            'image' => 'required', //image_url
        ], $messages);
        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
        }
        if (@$request->file('image')) {
            $aws3 = new Aws3();
            $fileFabName = Str::uuid() . '____' . $request->file('image')->getClientOriginalName();
            $fileFabName = str_replace(' ', '_', $fileFabName);
            $fileFabNamePath = "menus/".$request->id."/logo/".$fileFabName;
            $menuImagePathS3Key = $aws3->uploadFile($fileFabNamePath, $request->file('image')->getPathname(), $request->file('image')->getClientMimeType());
    }
    MenuMasterModel::where('id',$request->id)->update(['image'=>$menuImagePathS3Key]);
    return returnResponse(true, 'Icon uploaded successfully.', [], Response::HTTP_CREATED, true);
}

}