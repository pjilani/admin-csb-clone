<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Meta;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Validator;

class MetaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            // ================= permissions starts here =====================
            if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'site_seo_management';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;

                $permission = checkPermissions($params);   
                if(!$permission){ 
                  return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
              }
            // ================= permissions ends here ===================== 
          $data = Meta::getList(Auth::user()->tenant_id);
          return returnResponse(true, '', $data, 200, true);
        } catch (\Exception $e) {

          if (!App::environment(['local'])) {//, 'staging'
              return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
          } else {
              return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                  'LineNo' => $e->getLine(),
                  'FileName' => $e->getFile()
              ], Response::HTTP_EXPECTATION_FAILED);
          }
        }
    }
    public function addEdit(Request $request)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'site_seo_management';
            if(!$request->id){
              $params->action = 'C';
            }
            else{
              $params->action = 'U';
            }
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
              return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
          }
        // ================= permissions ends here =====================          
        $validator = Validator::make($request->all(), [
            'meta_title' => 'required',
            'meta_description' => 'required',
            'menu_id' => 'required',
            'meta_keyword' => 'required',
        ]);

        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), 400);
        }

        $data = $request->all();
        try {

            if ($data) {
                $insertData = [
                    'meta_title' => $data['meta_title'],
                    'meta_description' => $data['meta_description'],
                    'menu_id' => $data['menu_id'],
                    'active' => ($data['active'] ? true : false),
                    'meta_keyword' => $data['meta_keyword'],
                ];
                $tenant_id = Auth::user()->tenant_id;
                $admin_user_id = Auth::user()->id;
                if (isset($data['id']) && $data['id'] != '') {
                    // Edit Code

                    $DuplicateMenuCheck = Meta::where("menu_id", $request->menu_id)
                        ->where(
                            function ($query) use ($data) {
                                return $query
                                    ->where('id', '!=', $data['id'])
                                    ->where('tenant_id', Auth::user()->tenant_id);
                            })->first();

                    if ($DuplicateMenuCheck) {

                        return returnResponse(false, 'This menu id is already Added.', [], 500, true);
                    }
                    $createdValues = Meta::where('id', $data['id'])->update($insertData);
                    $message = "Updated successfully.";
                } else {

                    $DuplicateMenuCheck = Meta::where("menu_id", $request->menu_id)
                        ->where(
                            function ($query) use ($data) {
                                return $query
                                    ->where('tenant_id', Auth::user()->tenant_id);
                            })
                        ->first();
                    if ($DuplicateMenuCheck) {
                        return returnResponse(false, 'This menu id is already Added.', [], 500, true);
                    }
                    $insertData['tenant_id'] = $tenant_id;

                    $createdValues = Meta::create($insertData);
                    $message = "insert successfully.";
                }
                if ($createdValues) {
                    return returnResponse(true, $message, $insertData, 200, true);
                } else {
                    return returnResponse(false, $message, [], 403, true);
                }
            }
        } catch (\Exception $e) {
            if (!App::environment(['local'])) { //, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, [
                    'Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile(),
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function delete(Request $request)
    {

        try {
            // ================= permissions starts here =====================
            if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'site_seo_management';
                $params->action = 'T';
                $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;

                $permission = checkPermissions($params);   
                if(!$permission){ 
                  return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
              }
            // ================= permissions ends here ===================== 
            if (!empty($request->id)) {
                Meta::where('id', $request->id)->update(["active" => $request->status]);
                return returnResponse(true, "Status changed Successfully", Meta::all());
            } else {
                return returnResponse(false, "Selected id is invalid!", [], 500);
            }
        } catch (\Exception $e) {

            if (!App::environment(['local'])) { //, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, [
                    'Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile(),
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }
}
