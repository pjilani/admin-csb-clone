<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Notifications;
use App\Models\NotificationReceivers;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use App\Services\NotificationService;

class NotificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'notifications';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = @Auth::user()->tenant_id;

                $permission = checkPermissions($params);   
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
//            DB::enableQueryLog();
            // $record['result'] = Notifications::whereHas('receivers', function ($q) {
            //         $q->where('is_read', false);
            //     })->where('reference_type', 'UserDocument')
            // ->orderBy('id','desc')->get();
            // $record['tenant_id'] = @Auth::user()->tenant_id;
//            $quries = DB::getQueryLog();
//            du($quries);

            $record = Notifications::select(['notifications.id as id', 'message', 'reference_type', 'sender_id', 'notifications.created_at as created_at'])
                ->join('notification_receivers as nr', 'notifications.id', '=', 'nr.notification_id')
                ->where(['nr.receiver_id' => Auth::User()->id, 'nr.receiver_type' => ADMIN_TYPE])
                ->orderBy('notifications.id', 'DESC')->distinct('notifications.id')->paginate($request->size ?? 10);

            return returnResponse(true, "Record get Successfully", $record);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function unread() {
        try {
               // ================= permissions starts here =====================
               if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'notifications';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;

                $permission = checkPermissions($params);   
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================   

            $record['count'] = Notifications::join('notification_receivers as nr', 'notifications.id', '=', 'nr.notification_id')
                ->where(['nr.receiver_id' => Auth::User()->id, 'is_read' => false, 'nr.receiver_type' => ADMIN_TYPE])
                ->orderBy('notifications.id', 'DESC')->distinct('notifications.id')->count();

            $record['data'] = Notifications::select(['message', 'reference_type', 'sender_id', 'notifications.created_at as created_at'])
                ->join('notification_receivers as nr', 'notifications.id', '=', 'nr.notification_id')
                ->where(['nr.receiver_id' => Auth::User()->id, 'nr.receiver_type' => ADMIN_TYPE])
                ->orderBy('notifications.id', 'DESC')->distinct('notifications.id')->take(10)->get();
            
            return returnResponse(true, "Record get Successfully", $record);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function allRead() {
        try{
            
            NotificationReceivers::where(['receiver_id' => Auth::User()->id, 'receiver_type' => ADMIN_TYPE])->update(['is_read' => true]);

            return returnResponse(true, MESSAGE_NOTIFICATION_READ, [], Response::HTTP_OK, true);
            
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
               return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
           } else {
               return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                   'LineNo' => $e->getLine(),
                   'FileName' => $e->getFile()
               ], Response::HTTP_EXPECTATION_FAILED);
           }
       }
    }

    public function read($id)
    {
        
        try{
            $result = NotificationReceivers::find($id);
            if($result)
            {
                if($result->is_read == false)
                {
                    NotificationReceivers::where(['id' => $id])->update(['is_read'=>true]);
                    return returnResponse(true, MESSAGE_NOTIFICATION_READ, [], Response::HTTP_CREATED, true);
                }else{
                    return returnResponse(true, MESSAGE_NOTIFICATION_ALREADY_READ, [], Response::HTTP_CREATED, true);
                }
            }else{
                return returnResponse(true, MESSAGE_RECORD_NOT_FOUND, [], Response::HTTP_ACCEPTED, true);
            }
            NotificationReceivers::where(['id' => $id])->update(['is_read'=>true]);
            
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
               return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
           } else {
               return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                   'LineNo' => $e->getLine(),
                   'FileName' => $e->getFile()
               ], Response::HTTP_EXPECTATION_FAILED);
           }
       }
    }

}
