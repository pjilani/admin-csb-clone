<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Currencies;
use App\Models\Tenants;
use App\Models\Transactions;
use App\Models\User;
use App\Models\UserBonus;
use App\Models\Wallets;
use App\Models\WithdrawRequest;
use App\Services\TransactionsService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\BetTransaction;
use App\Models\DepositRequest;
use App\Models\DepositWithdrawJob;
use App\Models\DepositWithdrawUsers;
use App\Models\QueueLog;
use App\Models\TenantConfigurations;
use App\Models\WithdrawalTransactions;
use Validator;
use App\Services\SportsService;
use Illuminate\Support\Facades\Redis;
use App\Services\NotificationService;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Client;
use Illuminate\Support\Str;

class TransactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        return returnResponse(false, ERROR_service_maintenance, [], Response::HTTP_EXPECTATION_FAILED);
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'casino_transactions';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          

            $page = $request->has('page') ? $request->post('page') : 1;
            $limit = $request->has('size') ? $request->post('size') : 10;

            $filter['time_period'] = $request->has('time_period') ? $request->post('time_period') : [];


            $filter['time_zone_name'] = $request->time_zone_name?? 'UTC +00:00';
            $filter['time_period'] = $time_period = $request['datetime'] ?? []; 
            $filter['time_type'] = $request['time_type'] ?? 'today';


            if($filter['time_period']) {
                    $time_period['fromdate'] = getTimeZoneWiseTimeISO($time_period['start_date']);
                    $time_period['enddate']  = getTimeZoneWiseTimeISO($time_period['end_date']);
                $filter['time_period'] = $time_period;
            }

            $filter['action_type'] = $request->has('action_type') ? $request->post('action_type') : '';
            $filter['currency_id'] = $request->has('currency_id') ? $request->post('currency_id') : '';
            $filter['search'] = $request->has('search') ? trim($request->post('search')) : '';
            $filter['wallet_id'] = $request->has('wallet_id') ? $request->post('wallet_id') : '';
            $filter['sort_by'] = $request->has('sort_by') ? $request->post('sort_by') : 'id';
            $filter['order'] = $request->has('order') ? $request->post('order') : 'desc';
        
            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {

                $filter['tenant_id'] = Auth::user()->tenant_id;

            } else {
                $filter['tenant_id'] = $request->has('tenant_id') ? $request->post('tenant_id') : '';
            }
            $urlSegment = $request->segment(2);
            if ($urlSegment === 'admin') {
                $filter['login_id'] = Auth::user()->id;
                $filter['login_type'] = ADMIN_TYPE;

                $loginAgentRole = getRoleByAdminId(Auth::user()->id);
                if($loginAgentRole && $loginAgentRole->admin_role_id==4){
                    $filter['action_type'] = 3;
                }
                if($loginAgentRole && $loginAgentRole->admin_role_id==5){
                    $filter['action_type'] = 4;
                }
            }
            $record = Transactions::getList($limit, $page, $filter);

            // Manage Amount respect to login role
            if(Auth::user()->parent_type == 'AdminUser'){
                // check is agent or owner 
                $check = DB::table('admin_users_admin_roles')->where('admin_user_id',Auth::user()->id)->first();
                
                if($check->admin_role_id == 1){
                  // tenant owner
                    foreach ($record['data'] as $key => $value) {
                        if ($filter['time_zone_name']) {
                            if ($record['data'][$key]->created_at)
                                $record['data'][$key]->created_at = getTimeZoneWiseTime(@ $record['data'][$key]->created_at, $filter['time_zone_name']);
                        }
                        if($value->actionee_type == "SuperAdminUser"){
                            if($value->transaction_type == 3){
                                $record['data'][$key]->source_before_balance = 0.00;
                                $record['data'][$key]->source_after_balance = 0.00;
                            }elseif($value->transaction_type == 4){
                                $record['data'][$key]->target_before_balance = 0.00;
                                $record['data'][$key]->target_after_balance = 0.00;
                            }
                        }
                    }
                }else{
                    foreach ($record['data'] as $key => $value) {
                        if ($filter['time_zone_name']) {
                            if ($record['data'][$key]->created_at)
                                $record['data'][$key]->created_at = date('d-M-Y h:i:s', strtotime(getTimeZoneWiseTime($record['data'][$key]->created_at, $filter['time_zone_name'])));
                        }

                      if($value->actionee_type == "AdminUser"){
                      $check_agent = DB::table('admin_users_admin_roles')->where('admin_user_id',$value->actionee_id)->first();
                      
                        if($check_agent->admin_role_id == 1){
                          // owner
                          if($value->transaction_type == 3){
                            $record['data'][$key]->source_before_balance = 0.00;
                            $record['data'][$key]->source_after_balance = 0.00;
                          }elseif($value->transaction_type == 4){
                            $record['data'][$key]->target_before_balance = 0.00;
                            $record['data'][$key]->target_after_balance = 0.00;
                          }
                        }else{

                            $sql = "select parent_id from admin_users where id = " . (int)Auth::user()->id;
                            $idTopParentSql = DB::select($sql);
                            $idTopParent = 0;
                            if (@$idTopParentSql[0]) {
                                $idTopParent = $idTopParentSql[0]->parent_id;
                            }

                            //agent owner
                            if($value->actionee_id == $idTopParent){
                                if($value->transaction_type == 3){
                                    $record['data'][$key]->source_before_balance = 0.00;
                                    $record['data'][$key]->source_after_balance = 0.00;
                                }elseif($value->transaction_type == 4){
                                    $record['data'][$key]->target_before_balance = 0.00;
                                    $record['data'][$key]->target_after_balance = 0.00;
                                }
                            }

                        }
                      }
                   }
                    
                  
                }
            }


              // du($record);

            return returnResponse(true, "Record get Successfully", $record);
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @description download
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function download(Request $request)
    {
       // return returnResponse(false, ERROR_service_maintenance, [], Response::HTTP_EXPECTATION_FAILED);
        setUnlimited();
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'casino_transactions';
                $params->action = 'export';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          

            $page = 'all';
            $limit = 'all';
            $filter['sort_by'] = $request->has('sort_by') ? $request->post('sort_by') : '';
            $filter['order'] = $request->has('order') ? $request->post('order') : '';
            $filter['time_period'] = $request->has('time_period') ? $request->post('time_period') : [];
            $filter['action_type'] = $request->has('action_type') ? $request->post('action_type') : '';
            $filter['currency_id'] = $request->has('currency_id') ? $request->post('currency_id') : '';
            $filter['search'] = $request->has('search') ? $request->post('search') : '';

            $timeZone = $filter['time_zone_name'] = $request->time_zone_name?? 'UTC +00:00'; 
            $filter['time_period'] = $time_period = $request['datetime'] ?? [];
            $filter['time_type'] = $request['time_type'] ?? 'today';
            $filter['sort_by'] = $request->has('sort_by') ? $request->post('sort_by') : 'id';
            $filter['order'] = $request->has('order') ? $request->post('order') : 'desc';


            if($filter['time_period']) {
                $time_period['fromdate'] = getTimeZoneWiseTimeISO($time_period['start_date']);
                $time_period['enddate']  = getTimeZoneWiseTimeISO($time_period['end_date']);
                $filter['time_period'] = $time_period;
            }

            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {

                $filter['tenant_id'] = Auth::user()->tenant_id;

            } else {
                $filter['tenant_id'] = $request->has('tenant_id') ? $request->post('tenant_id') : '';
            }
            $urlSegment = $request->segment(2);
            if ($urlSegment === 'admin') {
                $filter['login_id'] = Auth::user()->id;
                $filter['login_type'] = ADMIN_TYPE;
            }

            $record = Transactions::getList($limit, $page, $filter);


            $columns = array(
                'ID',
                'Created at',
                'Round ID',
                'From wallet',
                'From currency',
                'To wallet',
                'To currency',
                'Amount',
                'Conversion rate',
                'Action Type',
                'Comments',
                'Action by',

            );

            $path = Storage::path('csv/');
            if (!is_dir($path)) {
                mkdir($path, 0775, true);
                chmod($path, 0775);
            }


            $fileName = time() . '.csv';
            $file = fopen($path . $fileName, 'w');
            fputcsv($file, $columns);


            foreach ($record['data'] as $row) {
                $row=(array)$row;

                if ($timeZone) { 
                    if ($row["created_at"])
                        $row["created_at"] = getTimeZoneWiseTime($row['created_at'], $timeZone);
                    else
                        $row["created_at"] = $row['creation_date'];
                } else {
                    $row["creation_date"] = $row['creation_date'];
                } 

//                if ($row["created_at"]!='')
//                        $row["created_at"] = date('d-m-Y h:i:s', strtotime($row['created_at']));
//                    else
//                        $row["created_at"] = '-';


                fputcsv($file, array(

                        $row["id"],
                        $row["created_at"],
                        $row["round_id"],
                        ($row["source_wallet_name"]??$row["source_wallet_name_admin"]),
                        $row["source_currency_id"],
                        ($row["target_wallet_name"]??$row["target_wallet_name_admin"]),
                        $row["target_currency_id"],
                        $row["amount"],
                        $row["conversion_rate"],
                        getTransactionTypeString($row["transaction_type"]),
                        $row["comments"],
                        $row["actionee_name"],

                    )
                );
            }
            fclose($file);
            // $result = ["url" => (request()->secure() ? secure_url(('storage/app/csv/' . $fileName)): url('storage/app/csv/' . $fileName))];
            $result = ["url" => secure_url('storage/app/csv/' . $fileName)];


            return returnResponse(true, "Record get Successfully", $result);
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getWithdrawal(Request $request)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
        $params = new \stdClass();
        $params->module = 'withdrawal_request';
        $params->action = 'R';
        $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

        $permission = checkPermissions($params);   
        if(!$permission){ 
            return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
        }
        }
        // ================= permissions ends here =====================          
      
        //        $record = Transactions::all();
        //        return returnResponse(true,"Record get Successfully",$record);
        $rolesObject = getRoles(Auth::user()->id);
            $page = $request->has('page') ? $request->post('page') : 1;
            $limit = $request->has('size') ? $request->post('size') : 10;

            $filter['time_period'] = $request->has('time_period') ? $request->post('time_period') : [];


            $filter['time_zone_name'] = $request->time_zone_name?? 'UTC +00:00';
            $filter['time_period'] = $time_period = $request['datetime'] ?? []; 
            $filter['time_type'] = $request['time_type'] ?? 'today';
    
    
            if($filter['time_period']) {
                    $time_period['fromdate'] = getTimeZoneWiseTimeISO($time_period['start_date']);
                    $time_period['enddate']  = getTimeZoneWiseTimeISO($time_period['end_date']);
                $filter['time_period'] = $time_period;
            }
            $filter['status'] = $request->has('status') ? $request->post('status') : '';
            $filter['search'] = $request->has('search') ? $request->post('search') : '';
            $filter['withdrawal_type'] = $request->has('withdrawal_type') ? $request->post('withdrawal_type') : 'all';
            $filter['search'] = str_replace("'", "", $filter['search']);
            $filter['sort_by'] = $request->has('sort_by') ? $request->post('sort_by') : 'id';
            $filter['order'] = $request->has('order') ? $request->post('order') : 'desc';
            try {
                if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                    $filter['tenant_id'] = Auth::user()->tenant_id;
                } else {
                    $filter['tenant_id'] = $request->has('tenant_id') ? $request->post('tenant_id') : '';
                }
                $record = Transactions::getForWithdrawList($limit, $page, $filter, ($request->post('type')));
                if(Auth::user()->parent_type == 'AdminUser'){
                    $check = DB::table('admin_users_admin_roles')->where('admin_user_id',Auth::user()->id)->first();
                    
                    if($check->admin_role_id == 1){
                      // tenant owner
                        foreach ($record['data'] as $key => $value) {
                            if ($filter['time_zone_name']) {
                                if ($record['data'][$key]->created_at)
                                {
                                    $record['data'][$key]->actioned_at = getTimeZoneWiseTime(@ $record['data'][$key]->actioned_at, $filter['time_zone_name']);
                                    $record['data'][$key]->created_at = getTimeZoneWiseTime(@ $record['data'][$key]->created_at, $filter['time_zone_name']);
                                }
                            }
                        }
                    }else{
                        foreach ($record['data'] as $key => $value) {
                            if ($filter['time_zone_name']) {
                                if ($record['data'][$key]->created_at)
                                {
                                    $record['data'][$key]->created_at = date('d-M-Y h:i:s', strtotime(getTimeZoneWiseTime($record['data'][$key]->created_at, $filter['time_zone_name'])));
                                    $record['data'][$key]->actioned_at = date('d-M-Y h:i:s', strtotime(getTimeZoneWiseTime($record['data'][$key]->actioned_at, $filter['time_zone_name'])));
    
                                }
                            }
                       }
                    }
                }
                return returnResponse(true, "Record get Successfully", $record);
            } catch (\Exception $e) {

                if (!App::environment(['local'])) { //, 'staging'
                    return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
                } else {
                    return returnResponse(false, ERROR_MESSAGE, [
                        'Error' => $e->getMessage(),
                        'LineNo' => $e->getLine(),
                        'FileName' => $e->getFile()
                    ], Response::HTTP_EXPECTATION_FAILED);
                }
            }
    }
    
    public function downloadWithdrawal(Request $request)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
        $params = new \stdClass();
        $params->module = 'withdrawal_request';
        $params->action = 'export';
        $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

        $permission = checkPermissions($params);   
        if(!$permission){ 
            return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
        }
        }
        // ================= permissions ends here =====================          
            $rolesObject = getRoles(Auth::user()->id);
            $page = $request->has('page') ? $request->post('page') : 1;
            $limit = $request->has('size') ? $request->post('size') : 10;
            $filter['time_period'] = $request->has('time_period') ? $request->post('time_period') : [];
            $timeZone = $filter['time_zone_name'] = $request->time_zone_name?? 'UTC +00:00';
            $filter['time_period'] = $time_period = $request['datetime'] ?? []; 
            $filter['time_type'] = $request['time_type'] ?? 'today';
            if($filter['time_period']) {
                $time_period['fromdate'] = getTimeZoneWiseTimeISO($time_period['start_date']);
                $time_period['enddate']  = getTimeZoneWiseTimeISO($time_period['end_date']);
                $filter['time_period'] = $time_period;
            }
            $filter['status'] = $request->has('status') ? $request->post('status') : '';
            $filter['search'] = $request->has('search') ? $request->post('search') : '';
            $filter['withdrawal_type'] = $request->has('withdrawal_type') ? $request->post('withdrawal_type') : 'all';
            $filter['search'] = str_replace("'", "", $filter['search']);
            $filter['sort_by'] = $request->has('sort_by') ? $request->post('sort_by') : 'id';
            $filter['order'] = $request->has('order') ? $request->post('order') : 'desc';
            try {
                if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                    $filter['tenant_id'] = Auth::user()->tenant_id;
                } else {
                    $filter['tenant_id'] = $request->has('tenant_id') ? $request->post('tenant_id') : '';
                }
                $filter['download'] = true;
                $record = Transactions::getForWithdrawList($limit, $page, $filter, ($request->post('type')));

                $columns = array(
                    'ID',
                    'Username',
                    'Bank Name',
                    'Account Holder Name',
                    'Account number',
                    'IFSC Code',
                    'Amount',
                    'Remark',
                    'Withdrawal Type',
                    'Requested At',
                    'Actioned At',   
                );
                $path = Storage::path('csv/');
                if (!is_dir($path)) {
                    mkdir($path, 0775, true);
                    chmod($path, 0775);
                }
                $fileName = time() . '.csv';
                $file = fopen($path . $fileName, 'w');
                fputcsv($file, $columns);

                foreach ($record['data'] as $row) {
                    $row=(array)$row;
    
                    if ($timeZone) { 
                        if ($row["created_at"])
                            $row["created_at"] = getTimeZoneWiseTime($row['created_at'], $timeZone);
                        else
                            $row["created_at"] = $row['creation_date'];
                    } else {
                        $row["creation_date"] = $row['creation_date'];
                    } 

                    fputcsv($file, array(
                            $row["id"],
                            $row["user_name"],
                            $row["bank_name"],
                            $row["name"],
                            $row["account_number"],
                            $row["ifsc_code"],
                            $row["amount"],
                            $row["remark"],
                            $row["withdrawal_type"],
                            $row['created_at'],
                            $row['actioned_at']
                        )
                    );
                }
                fclose($file);
                // $result = ["url" => (request()->secure() ? secure_url(('storage/app/csv/' . $fileName)): url('storage/app/csv/' . $fileName))];
                $result = ["url" => secure_url('storage/app/csv/' . $fileName)];
                return returnResponse(true, "Record get Successfully", $result);
            } catch (\Exception $e) {
                if (!App::environment(['local'])) { //, 'staging'
                    return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
                } else {
                    return returnResponse(false, ERROR_MESSAGE, [
                        'Error' => $e->getMessage(),
                        'LineNo' => $e->getLine(),
                        'FileName' => $e->getFile()
                    ], Response::HTTP_EXPECTATION_FAILED);
                }
            }
    }
    
    public function getDepositRequest(Request $request)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
        $params = new \stdClass();
        $params->module = 'deposit_requests';
        $params->action = 'R';
        $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

        $permission = checkPermissions($params);   
        if(!$permission){ 
            return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
        }
        }
        // ================= permissions ends here =====================          
      
        //        $record = Transactions::all();
//        return returnResponse(true,"Record get Successfully",$record);
        $rolesObject = getRoles(Auth::user()->id);
            $page = $request->has('page') ? $request->post('page') : 1;
        $limit = $request->has('size') ? $request->post('size') : 10;

        $filter['time_period'] = $request->has('time_period') ? $request->post('time_period') : [];
        $filter['time_period_actioned_at'] = $request->has('timePeriodActionedAt') ? $request->post('timePeriodActionedAt') : [];


        $filter['time_zone_name'] = $request->time_zone_name?? 'UTC +00:00';
        $filter['time_period'] = $time_period = $request['datetime'] ?? []; 
        $filter['time_period_actioned_at'] = $time_period_actioned_at = $request['datetimeActionedAt'] ?? []; 
        $filter['time_type'] = $request['time_type'] ?? 'today';


        if($filter['time_period']) {
                $time_period['fromdate'] = getTimeZoneWiseTimeISO($time_period['start_date']);
                $time_period['enddate']  = getTimeZoneWiseTimeISO($time_period['end_date']);
            $filter['time_period'] = $time_period;
        }
        if($filter['time_period_actioned_at']) {
            $time_period_actioned_at['fromdate'] = getTimeZoneWiseTimeISO($time_period_actioned_at['start_date']);
            $time_period_actioned_at['enddate']  = getTimeZoneWiseTimeISO($time_period_actioned_at['end_date']);
        $filter['time_period_actioned_at'] = $time_period_actioned_at;
        }
        $filter['deposit_type'] = $request->has('deposit_type') ? $request->post('deposit_type') : '';
        $filter['status'] = $request->has('status') ? $request->post('status') : '';
        $filter['search'] = $request->has('search') ? $request->post('search') : '';
        $filter['transaction_id'] = $request->has('transaction_id') ? $request->post('transaction_id') : '';
        $filter['search']=str_replace("'", "", $filter['search']);
        $filter['transaction_id']=str_replace("'", "", $filter['transaction_id']);
        $filter['vip_levels'] = $request->has('vip_levels') ? $request->post('vip_levels') : '';
        $filter['sort_by'] = $request->has('sort_by') ? $request->post('sort_by') : 'id';
        $filter['order'] = $request->has('order') ? $request->post('order') : 'desc';

        try {
            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $filter['tenant_id'] = Auth::user()->tenant_id;
            } else {
                $filter['tenant_id'] = $request->has('tenant_id') ? $request->post('tenant_id') : '';
            }
            $record = Transactions::getDepositRequestList($limit, $page, $filter);
            if(Auth::user()->parent_type == 'AdminUser'){
                $check = DB::table('admin_users_admin_roles')->where('admin_user_id',Auth::user()->id)->first();
                
                if($check->admin_role_id == 1){
                  // tenant owner
                    foreach ($record['data'] as $key => $value) {
                        if ($filter['time_zone_name']) {
                            if ($record['data'][$key]->created_at)
                            {
                                $record['data'][$key]->updated_at = getTimeZoneWiseTime(@ $record['data'][$key]->updated_at, $filter['time_zone_name']);
                                $record['data'][$key]->created_at = getTimeZoneWiseTime(@ $record['data'][$key]->created_at, $filter['time_zone_name']);
                            }
                        }
                    }
                }else{
                    foreach ($record['data'] as $key => $value) {
                        if ($filter['time_zone_name']) {
                            if ($record['data'][$key]->created_at)
                            {
                                $record['data'][$key]->created_at = date('d-M-Y h:i:s', strtotime(getTimeZoneWiseTime($record['data'][$key]->created_at, $filter['time_zone_name'])));
                                $record['data'][$key]->updated_at = date('d-M-Y h:i:s', strtotime(getTimeZoneWiseTime($record['data'][$key]->updated_at, $filter['time_zone_name'])));

                            }
                        }
                   }
                }
            }
            return returnResponse(true, "Record get Successfully", $record);
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function downloadDepositRequest(Request $request)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
        $params = new \stdClass();
        $params->module = 'deposit_requests';
        $params->action = 'export';
        $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

        $permission = checkPermissions($params);   
        if(!$permission){ 
            return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
        }
        }
        // ================= permissions ends here =====================          

        $rolesObject = getRoles(Auth::user()->id);
        $page = $request->has('page') ? $request->post('page') : 1;
        $limit = $request->has('size') ? $request->post('size') : 10;
        $filter['time_period'] = $request->has('time_period') ? $request->post('time_period') : [];
        $filter['time_period_actioned_at'] = $request->has('timePeriodActionedAt') ? $request->post('timePeriodActionedAt') : [];

        $timeZone = $filter['time_zone_name'] = $request->time_zone_name?? 'UTC +00:00';
        $filter['time_period'] = $time_period = $request['datetime'] ?? []; 
        $filter['time_period_actioned_at'] = $time_period_actioned_at = $request['datetimeActionedAt'] ?? []; 
        $filter['time_type'] = $request['time_type'] ?? 'today';

        if($filter['time_period']) {
            $time_period['fromdate'] = getTimeZoneWiseTimeISO($time_period['start_date']);
            $time_period['enddate']  = getTimeZoneWiseTimeISO($time_period['end_date']);
            $filter['time_period'] = $time_period;
        }
        if($filter['time_period_actioned_at']) {
          $time_period_actioned_at['fromdate'] = getTimeZoneWiseTimeISO($time_period_actioned_at['start_date']);
          $time_period_actioned_at['enddate']  = getTimeZoneWiseTimeISO($time_period_actioned_at['end_date']);
          $filter['time_period_actioned_at'] = $time_period_actioned_at;
      }
        $filter['deposit_type'] = $request->has('deposit_type') ? $request->post('deposit_type') : '';
        $filter['status'] = $request->has('status') ? $request->post('status') : '';
        $filter['search'] = $request->has('search') ? $request->post('search') : '';
        $filter['search']=str_replace("'", "", $filter['search']);
        $filter['sort_by'] = $request->has('sort_by') ? $request->post('sort_by') : 'id';
        $filter['order'] = $request->has('order') ? $request->post('order') : 'desc';
        try {
            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $filter['tenant_id'] = Auth::user()->tenant_id;
            } else {
                $filter['tenant_id'] = $request->has('tenant_id') ? $request->post('tenant_id') : '';
            }
            $filter['download'] = true;
            $record = Transactions::getDepositRequestList($limit, $page, $filter);
            $columns = array(
                'ID',
                'Username',
                'Status',
                'UTR Number / Transaction Id',
                'Transaction Receipt',
                'Amount',
                'Remark',
                'Deposit Type',
                'Action By',
                'Requested At',
                'Actioned At',
            );

            $path = Storage::path('csv/');
            if (!is_dir($path)) {
                mkdir($path, 0775, true);
                chmod($path, 0775);
            }

            $fileName = time() . '.csv';
            $file = fopen($path . $fileName, 'w');
            fputcsv($file, $columns);

            foreach ($record['data'] as $row) {
                $row=(array)$row;

                if ($timeZone) { 
                    if ($row["created_at"]){
                      $row["created_at"] = getTimeZoneWiseTime($row['created_at'], $timeZone);
                    }   
                    else{
                      $row["created_at"] = $row['creation_date'];
                    }

                    if ($row["updated_at"]){
                      $row["updated_at"] = getTimeZoneWiseTime($row['updated_at'], $timeZone);
                    }   
                    else{
                      $row["updated_at"] = $row['creation_date'];
                    }
                } else {
                    $row["creation_date"] = $row['creation_date'];
                    $row["updated_at"] = $row['updated_at'];
                }
                fputcsv($file, array(
                        $row["id"],
                        $row["user_name"],
                        $row["status"],
                        $row["utr_number"] ? $row["utr_number"] : $row['order_id'],
                        $row["transaction_receipt"],
                        $row["amount"],
                        $row["remark"],
                        $row["deposit_type"],
                        $row["action_by"],
                        $row["created_at"],
                        $row['updated_at'],
                    )
                );
            }
            fclose($file);
            // $result = ["url" => (request()->secure() ? secure_url(('storage/app/csv/' . $fileName)): url('storage/app/csv/' . $fileName))];
            $result = ["url" => secure_url('storage/app/csv/' . $fileName)];
            return returnResponse(true, "Record get Successfully", $result);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                if ($request->transaction_type == "deposit") {
                    $params->module = 'fund_transfer_deposit';
                    $params->action = 'C';
                }
                else {
                    $params->module = 'fund_transfer_withdraw';
                    $params->action = 'C';
                }
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            $validatorArray = [
                'target_type' => 'required',
                'target_wallete_id' => 'required',
                // 'source_wallete_id' => 'required',
                'transaction_amount' => 'required',
            ];

            if($request->transferType != 'self'){
              $validatorArray['source_wallete_id'] = 'required';
            }

            $validator = Validator::make($request->all(), $validatorArray);

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
            } else {
                $input = $request->all();
            }


            if($request->transferType == 'self'){
                return $this->tenantAdminSelfTransaction($request);
            }

            $bothWallet = Transactions::getWallet($input['target_wallete_id'], $input['source_wallete_id']);


            if(empty($bothWallet['targetWallet']) && empty($bothWallet['sourceWallet'])){
                return returnResponse(false, ERROR_MESSAGE . "Please refresh the page",
                    [], Response::HTTP_EXPECTATION_FAILED);
            }

            DB::beginTransaction();

            // wallet locking 
            $getSourceWallet = Wallets::where('id', $bothWallet['sourceWallet']->id)->lockForUpdate()->first();
            $getTargetWallet = Wallets::where('id', $bothWallet['targetWallet']->id)->lockForUpdate()->first();

            if (@$bothWallet['sourceWallet']->id == @$bothWallet['targetWallet']->id) {
                return returnResponse(false, 'Source wallet and target wallets are some',
                    [], Response::HTTP_EXPECTATION_FAILED);
            }
            $currencySource = Currencies::where('id',$bothWallet['sourceWallet']->currency_id)->first();
            $targetWallet = Currencies::where('id',$bothWallet['targetWallet']->currency_id)->first();
            if ((float)round($bothWallet['sourceWallet']->amount,5) == (float)round($input['transaction_amount'],5)) {
            }elseif ((float)round($bothWallet['sourceWallet']->amount,5) < (float)round($input['transaction_amount'],5)) {
                return returnResponse(false, 'Source wallet amount not greater then target Wallet',
                    [], Response::HTTP_EXPECTATION_FAILED);
            }


            if (!(float)$input['transaction_amount']) {
                return returnResponse(false, 'Please fill Proper Amount ',
                    [], Response::HTTP_EXPECTATION_FAILED);
            }


            if ($request->transaction_type == "deposit") {
                $transactionType=TRANSACTION_TYPE_DEPOSIT;

            } else {
                $transactionType=TRANSACTION_TYPE_WITHDRAWAL;

            }

            $insertData = [
                'actionee_type' => @Auth::user()->parent_type,
                'actionee_id' => @Auth::user()->id,
                'source_currency_id' => $bothWallet['sourceWallet']->currency_id,
                'target_currency_id' => $bothWallet['targetWallet']->currency_id,
                'source_wallet_id' => $bothWallet['sourceWallet']->id,
                'target_wallet_id' => $bothWallet['targetWallet']->id,
                'source_before_balance' => @$bothWallet['sourceWallet']->amount,
                'target_before_balance' => @$bothWallet['targetWallet']->amount,
                'status' => 'success',
                'parent_id' => @Auth::user()->id,
                'tenant_id' => @Auth::user()->tenant_id,
                'created_at' => @date('Y-m-d H:i:s'),
                'comments' => @$input['transaction_comments'],
                'transaction_type' => $transactionType,
                'payment_method' => 'manual',
                'transaction_id'=>getUUIDTransactionId()
            ];
            /*        NOTE - EUR is primary currency. Any conversion from currency A to currency B will consider EUR as base.
                e.g. Currency A amount * Exchange Rate of Currency A = Primary Currency(EUR) amount
            e.g. Currency A amount * (Exchange Rate of Currency A / Exchange Rate of Currency B) = Currency B amount */

//            $amount = @$input['transaction_amount'] * ($currencySource->exchange_rate / $targetWallet->exchange_rate);


//            $insertData['conversion_rate'] = ($currencySource->exchange_rate / $targetWallet->exchange_rate);

            $amount = @$input['transaction_amount'] * ($targetWallet->exchange_rate/$currencySource->exchange_rate );


            $insertData['conversion_rate'] = ($targetWallet->exchange_rate/$currencySource->exchange_rate );


            $insertData['amount'] = floatval(@$input['transaction_amount']);
            if($transactionType == 3){
                $insertData['amount'] = (float)$amount;  
            }


            $insertData['source_after_balance'] = floatval(@$bothWallet['sourceWallet']->amount) - floatval(@$input['transaction_amount']);

            $insertData['target_after_balance'] = floatval(@$bothWallet['targetWallet']->amount) + floatval(@$amount);


            //  =======================================Add Other Currency Amount =======================================
            $configurations = TenantConfigurations::select('allowed_currencies')->where('tenant_id', @Auth::user()->tenant_id)->get();
            $configurations = $configurations->toArray();
            $CurrenciesValue=[];
            if ($configurations) {

                $configurations = $configurations[0];
                if (strlen($configurations['allowed_currencies']) > 1) {
                    $configurations = str_replace('{', '', $configurations['allowed_currencies']);
                    $configurations = str_replace('}', '', $configurations);
                    $configurations = explode(',', $configurations);

                    if (@$configurations[0] && count($configurations) > 0) {
                        $CurrenciesValue = Currencies::select('code')->whereIn('id', $configurations)->pluck('code');
                        $CurrenciesValue = $CurrenciesValue->toArray();
                    }
                }
            }

            $currenciesTempArray = $insertData['other_currency_amount'] = [];

            $exMainTransactionCurrecy = '';
            if(isset($currencySource->id)){
                $exMainTransactionCurrecy = $currencySource->code;
            }else{
                $exMainTransactionCurrecy =Currencies::select('code')->where('id',   $targetWallet->currency_id)->pluck('code');
                $exMainTransactionCurrecy =$exMainTransactionCurrecy->toArray();;
            }
            $amount_currency_ex = Currencies:: getExchangeRateCurrency($exMainTransactionCurrecy);
            foreach ($CurrenciesValue as $key => $value) {
                if($exMainTransactionCurrecy != $value){
                    $getExchangeRateCurrency = Currencies:: getExchangeRateCurrency($value);
                    $currenciesTempArray[$value] = (float)number_format((float)$input['transaction_amount'] * (  $getExchangeRateCurrency/$amount_currency_ex),4,".","");
                }else{
                    $currenciesTempArray[$value] = (float)$input['transaction_amount'];
                }
            }
            $insertData['other_currency_amount'] = json_encode($currenciesTempArray);
            //  =======================================Add Other Currency Amount ENd =======================================

            $createdValues = Transactions::create($insertData);

            $getSourceWallet->refresh();
            $getTargetWallet->refresh();
            
            $getSourceWallet->amount = $insertData['source_after_balance'];
            $getTargetWallet->amount = $insertData['target_after_balance']; 


            $getSourceWallet->save();
            $getTargetWallet->save();

            if ($createdValues) {
//                NotificationService::createNotification($bothWallet['sourceWallet']->id, $insertData['amount'], 'withdraw', 'other');
//                NotificationService::createNotification($bothWallet['targetWallet']->id, $insertData['amount'], 'deposit', 'other');
                // Elastic search transaction 
                //Transactions::storeElasticSearchData($createdValues->id);
                $this->updateUserBonus($request->all(),$createdValues);
                $queueLog = QueueLog::create([
                    'type' => CASINO_TRANSACTION,
                    'ids' => json_encode([$createdValues->id])
                ]);
                DB::commit();
               
                try {
                    $redis = Redis::connection();
                    $jsonArray=[];
                    $jsonArray['QueueLog']= [
                        'queueLogId'=> $queueLog->id
                    ];
                    $redis->publish('QUEUE_WORKER', json_encode($jsonArray)
                    );
                } catch (\Throwable $th) {
                //throw $th;
                }
                $isSave = true;

                $userSourceWalletDetails = TransactionsService :: getUserWalletDetails($bothWallet['sourceWallet']->id);
                $usertargetWalletDetails = TransactionsService :: getUserWalletDetails($bothWallet['targetWallet']->id);

                // if($userSourceWalletDetails->owner_type == 'User'){
                //     User::storeElasticSearchData($userSourceWalletDetails->user_id);
                // }

                // if($usertargetWalletDetails->owner_type == 'User'){
                //     User::storeElasticSearchData($usertargetWalletDetails->user_id);
                // }
            }
        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

        if ($isSave) {
            return returnResponse(true, @$request->transaction_type . ' Successfully.', ['transaction_id' => $createdValues->id ], Response::HTTP_CREATED, true);
        } else {
            return returnResponse(false, @$request->transaction_type . ' not Successfully.', [], Response::HTTP_BAD_REQUEST, true);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transactions $transactions
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return returnResponse(true, "Record get Successfully", Transactions::find($id)->toArray(), 200, true);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function walletDeposit(Request $request)
    {
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'fund_transfer_deposit';
                $params->action = 'C';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            DB::beginTransaction();
            $validatorArray = [
                'wallet_id' => 'required',
                'non_cash_amount' => 'required',

            ];

            $validator = Validator::make($request->all(), $validatorArray);
            if ($validator->fails()) {
                return returnResponse(false, MESSAGE_VALIDATION_FAIL, $validator->errors(), Response::HTTP_BAD_REQUEST);
            }
            $createDeposit = Transactions::getDepositNoCash($request->wallet_id, $request->non_cash_amount);
            if ($createDeposit) {

                // Elastic search create transaction
//                NotificationService::createNotification($request->wallet_id, $request->non_cash_amount, 'deposit', 'nocash');
                $lastId=TransactionsService::noCashTransactionHistory($request->wallet_id, $request->non_cash_amount, 5);
                //Entry in queue log
                $queueLog = QueueLog::create([
                    'type' => CASINO_TRANSACTION,
                    'ids' => json_encode([$lastId->id])
                ]);
            
                DB::commit();

                if($lastId->id)
                //Transactions::storeElasticSearchData($lastId->id);

                $userWalletDetails = TransactionsService :: getUserWalletDetails($request->wallet_id);
                //User::storeElasticSearchData($userWalletDetails->user_id);
                try {
                    $redis = Redis::connection();
                    $jsonArray=[];
                    $jsonArray['QueueLog']= [
                        'queueLogId'=> $queueLog->id
                    ];
                    $redis->publish('QUEUE_WORKER', json_encode($jsonArray)
                    );
                } catch (\Throwable $th) {
                //throw $th;
                }
                return returnResponse(true, MESSAGE_DEPOSIT, [], Response::HTTP_CREATED, true);
            } else {
                DB::rollback();
                return returnResponse(true, MESSAGE_DEPOSIT_FAIL, [], Response::HTTP_EXPECTATION_FAILED, true);
            }
        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function walletWithdraw(Request $request)
    {
        try {
             // ================= permissions starts here =====================
             if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'fund_transfer_withdraw';
                $params->action = 'C';
                $params->admin_user_id = Auth::user()->id;
                 $params->tenant_id = Auth::user()->tenant_id;

                 $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================  
            DB::beginTransaction();
            $validatorArray = [
                'wallet_id' => 'required',
                'non_cash_amount' => 'required',

            ];
            $validator = Validator::make($request->all(), $validatorArray);
            if ($validator->fails()) {
                return returnResponse(false, MESSAGE_VALIDATION_FAIL, $validator->errors(), Response::HTTP_BAD_REQUEST);
            }

            $userWalletDetails = TransactionsService :: getUserWalletDetails($request->wallet_id);
            
            if($userWalletDetails->non_cash_amount && $userWalletDetails->non_cash_amount < $request->non_cash_amount){
                return returnResponse(true, MESSAGE_WITHDRAW_NOT_AVAILABLE, [], Response::HTTP_FORBIDDEN, true);
            }
            $createDeposit = Transactions::getWithdrawNoCash($request->wallet_id, $request->non_cash_amount);

            if ($createDeposit) {
                // Elastic search create transaction
//                NotificationService::createNotification($request->wallet_id, $request->non_cash_amount, 'withdraw', 'nocash');

                $lastId = TransactionsService::noCashTransactionHistory($request->wallet_id, $request->non_cash_amount, 6);
                //Entry in queue log
                $queueLog = QueueLog::create([
                    'type' => CASINO_TRANSACTION,
                    'ids' => json_encode([$lastId->id])
                ]);
                DB::commit();

                if($lastId){
                    //Transactions::storeElasticSearchData($lastId->id);
                    
                try {
                    $redis = Redis::connection();
                    $jsonArray=[];
                    $jsonArray['QueueLog']= [
                        'queueLogId'=> $queueLog->id
                    ];
                    $redis->publish('QUEUE_WORKER', json_encode($jsonArray)
                    );
                } catch (\Throwable $th) {
                //throw $th;
                }
                }
                    

                //User::storeElasticSearchData($userWalletDetails->user_id);

                return returnResponse(true, MESSAGE_WITHDRAW, [], Response::HTTP_CREATED, true);
            } else {
                DB::rollback();
                return returnResponse(true, MESSAGE_WITHDRAW_FAIL, [], Response::HTTP_EXPECTATION_FAILED, true);
            }
        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }


    }


    public function tenantAdminSelfTransaction($request)
    {

        try {
          
            $transactionType=TRANSACTION_TYPE_DEPOSIT;
            $walletId = $request->target_wallete_id;
            $successMessage = MESSAGE_DEPOSIT;
            $errorMessage = MESSAGE_DEPOSIT_FAIL;
          
            $sqlCheck="SELECT 
                        *
                        FROM wallets as u
                        where u.id = ?;
                                              
                        ";

            $targetWallet = DB::select($sqlCheck,[$walletId])[0];

            $currencySource = Currencies::getPrimaryCurrency();

            $tenant_id = @Auth::user()->tenant_id;

            DB::beginTransaction();
            if (!(float)$request['transaction_amount']) {
              return returnResponse(false, 'Please fill Proper Amount ',
                  [], Response::HTTP_EXPECTATION_FAILED);
            }
            $insertData = [
                'actionee_type' => @Auth::user()->parent_type,
                'actionee_id' => @Auth::user()->id,
                'target_wallet_id' => $targetWallet->id,
                'target_before_balance' => $targetWallet->amount,
                'status' => 'success',
                'tenant_id' => $tenant_id,
                'target_currency_id' => $targetWallet->currency_id,
                'created_at' => @date('Y-m-d H:i:s'),
                'comments' => @$request['transaction_comments'],
                'transaction_type' => $transactionType,
                'amount' => $request['transaction_amount'],
                'payment_method' => 'manual',
                'transaction_id'=>getUUIDTransactionId()
            ];

         

                $insertData['source_currency_id'] = NULL;
                $insertData['source_wallet_id'] = NULL;
                $insertData['source_before_balance'] = NULL;
  
            $adminUpdatedWalletAmount = 0;

            if($targetWallet->amount < 0.00000000){
                $targetWallet->amount = 0.00000000;
            }

            // $insertData['conversion_rate'] = $currencySource->exchange_rate;
            // $amount = @(float)$request['transaction_amount'] * ((float)$currencySource->exchange_rate);
            $amount = @(float)$request['transaction_amount'];

            $insertData['source_after_balance'] = NULL;

            $insertData['target_after_balance'] = (float)$targetWallet->amount + (float)@$amount;
    


             //  =======================================Add Other Currency Amount =======================================
            //  =======================================Add Other Currency Amount =======================================
            $configurations = TenantConfigurations::select('allowed_currencies')->where('tenant_id', $tenant_id)->get();
            $configurations = $configurations->toArray();
            $CurrenciesValue=[];
            if ($configurations) {

                $configurations = $configurations[0];
                if (strlen($configurations['allowed_currencies']) > 1) {
                    $configurations = str_replace('{', '', $configurations['allowed_currencies']);
                    $configurations = str_replace('}', '', $configurations);
                    $configurations = explode(',', $configurations);

                    if (@$configurations[0] && count($configurations) > 0) {
                        $CurrenciesValue = Currencies::select('code')->whereIn('id', $configurations)->pluck('code');
                        $CurrenciesValue = $CurrenciesValue->toArray();
                    }
                }
            }

            $currenciesTempArray = $insertData['other_currency_amount'] = [];

            $exMainTransactionCurrecy = '';
            if(isset($currencySource->id)){
                $exMainTransactionCurrecy = $currencySource->code;
            }else{
                $exMainTransactionCurrecy =Currencies::select('code')->whereIn('id',   $targetWallet->currency_id)->pluck('code');
                $exMainTransactionCurrecy =$exMainTransactionCurrecy->toArray();;
            }

            $amount_currency_ex = Currencies:: getExchangeRateCurrency($exMainTransactionCurrecy);
            // dd($amount_currency_ex);
            foreach ($CurrenciesValue as $key => $value) {
                if($exMainTransactionCurrecy != $value){
                    $getExchangeRateCurrency = Currencies:: getExchangeRateCurrency($value);
                    $currenciesTempArray[$value] = (float)number_format((float)$request['transaction_amount'] * ($amount_currency_ex / $getExchangeRateCurrency),4,".","");
                }else{
                    $currenciesTempArray[$value] = (float)$request['transaction_amount'];
                }

            }
            $insertData['other_currency_amount'] = json_encode($currenciesTempArray);

            //  =======================================Add Other Currency Amount END =======================================
            //  =======================================Add Other Currency Amount ENd =======================================


            $createdValues = Transactions::create($insertData);

            Wallets::where('id', $targetWallet->id)->update(['amount' => $insertData['target_after_balance']]);

            if(@$createdValues->id){

                //Transactions::storeElasticSearchData($createdValues->id);
                // if ($input['transaction_type'] == "deposit") {
                //   Transactions::storeElasticSearchSuperAdminTransactionData($createdValues->id,"debit");
                // }
                $userSourceWalletDetails = TransactionsService :: getUserWalletDetails($targetWallet->id);

                if($userSourceWalletDetails->owner_type == 'User'){
                    //User::storeElasticSearchData($userSourceWalletDetails->user_id);
                }

            }
            //Entry in queueLog
            $queueLog = QueueLog::create([
                'type' => CASINO_TRANSACTION,
                'ids' => json_encode([$createdValues->id])
            ]);
            DB::commit();


            if ($createdValues) {
                try {
                    $redis = Redis::connection();
                    $jsonArray=[];
                    $jsonArray['QueueLog']= [
                        'queueLogId'=> $queueLog->id
                    ];
                    $redis->publish('QUEUE_WORKER', json_encode($jsonArray)
                    );
                } catch (\Throwable $th) {
                //throw $th;
                }
                return returnResponse(true, $successMessage, [], Response::HTTP_CREATED, true);
            } else {

                return returnResponse(false, $errorMessage, [], Response::HTTP_BAD_REQUEST, true);
            }
        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }

        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function adminTransaction(Request $request)
    {

        try {
            $validatorArray = [
                'target_type' => 'required',
                'transaction_amount' => 'required',
                'transaction_type' => 'required',
            ];
            if ($request->transaction_type == "deposit") {
                $transactionType=TRANSACTION_TYPE_DEPOSIT;
                $validatorArray ['target_wallete_id'] = 'required';
                $walletId = $request->target_wallete_id;
                $successMessage = MESSAGE_DEPOSIT;
                $errorMessage = MESSAGE_DEPOSIT_FAIL;
            } else {
                $transactionType=TRANSACTION_TYPE_WITHDRAWAL;
                $validatorArray ['source_wallete_id'] = 'required';
                $walletId = $request->source_wallete_id;
                $successMessage = MESSAGE_WITHDRAW;
                $errorMessage = MESSAGE_WITHDRAW_FAIL;
            }

            $validator = Validator::make($request->all(), $validatorArray);
            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
            } else {
                $input = $request->all();
            }


            $sqlCheck="SELECT 
                        *
                        FROM wallets as u
                        where u.id = ?;
                                              
                        ";

            $targetWallet = DB::select($sqlCheck,[$walletId])[0];
            $owner_id = $owner_type = 0;


            if ($request->transaction_type == "deposit") {
              $sourceWallet = @Admin::getSuperAdminWalletsDetails(@Auth::user()->id);
              // dd($sourceWallet);
              if(!$sourceWallet){
                throw new \ErrorException('No Wallet for supper admin');
              }

              $owner_id = $targetWallet->owner_id;
              $owner_type = $targetWallet->owner_type;


              if ((float)round($sourceWallet->amount,5) < (float)round($input['transaction_amount'],5)) {
                return returnResponse(false, 'Source wallet amount not greater then target Wallet',
                    [], Response::HTTP_EXPECTATION_FAILED);
              }
            }else{
              $targetWallet = @Admin::getSuperAdminWalletsDetails(@Auth::user()->id);
              $sourceWallet = DB::select($sqlCheck,[$walletId])[0];

              $owner_id = $sourceWallet->owner_id;
              $owner_type = $sourceWallet->owner_type;
              // dd($targetWallet);
              if(!$targetWallet){
                throw new \ErrorException('No Wallet for supper admin');
              }


              if ((float)round($sourceWallet->amount,5) < (float)round($input['transaction_amount'],5)) {
                return returnResponse(false, 'No sufficient balance for withdrawal, Try with less amount.',
                    [], Response::HTTP_EXPECTATION_FAILED);
              }

            }



            $currencySource = Currencies::getPrimaryCurrency();

            $tenant_id = Tenants::getSuperTenantId($owner_id, $owner_type);

            if (!$tenant_id) {
                throw new \ErrorException($tenant_id . ' tenant ID not found');
            }

            DB::beginTransaction();
            if (!(float)$input['transaction_amount']) {
              return returnResponse(false, 'Please fill Proper Amount ',
                  [], Response::HTTP_EXPECTATION_FAILED);
            }
            $insertData = [
                'actionee_type' => @Auth::user()->parent_type,
                'actionee_id' => @Auth::user()->id,
                'target_wallet_id' => $targetWallet->id,
                'target_before_balance' => $targetWallet->amount,
                'status' => 'success',
                'tenant_id' => $tenant_id,
                'target_currency_id' => $targetWallet->currency_id,
                'created_at' => @date('Y-m-d H:i:s'),
                'comments' => @$input['transaction_comments'],
                'transaction_type' => $transactionType,
                'amount' => $input['transaction_amount'],
                'payment_method' => 'manual',
                'transaction_id'=>getUUIDTransactionId()
            ];

            // if ($request->transaction_type == "deposit") {
                $insertData['target_wallet_id'] = $targetWallet->id;
                $insertData['target_before_balance'] = $targetWallet->amount;
                $insertData['target_currency_id'] = $targetWallet->currency_id;

                $insertData['source_currency_id'] = $sourceWallet->currency_id;
                $insertData['source_wallet_id'] = $sourceWallet->id;
                $insertData['source_before_balance'] = @$sourceWallet->amount;
            // }





            /*  NOTE - EUR is primary currency. Any conversion from currency A to currency B will consider EUR as base.
            e.g. Currency A amount * Exchange Rate of Currency A = Primary Currency(EUR) amount
            e.g. Currency A amount  (Exchange Rate of Currency A / Exchange Rate of Currency B) = Currency B amount */

            $adminUpdatedWalletAmount = 0;

            if($targetWallet->amount < 0.00000000){
                $targetWallet->amount = 0.00000000;
            }
            if ($input['transaction_type'] == "withdraw") {
                $insertData['conversion_rate'] = $currencySource->exchange_rate;
                $amount = @(float)$input['transaction_amount'] * ((float)$currencySource->exchange_rate);
               // $amount = @(float)$input['transaction_amount'] ;
                if ((float)$targetWallet->amount == (float)$input['transaction_amount']) {
                }elseif ((float)$targetWallet->amount < (float)$input['transaction_amount']) {
                    return returnResponse(false, 'Source wallet amount ('.$targetWallet->amount.
                        ') not greater then  Transfer Fund  Amount ('.(float)$input['transaction_amount'].')',
                        [], Response::HTTP_EXPECTATION_FAILED);
                }

                $insertData['target_after_balance'] = (float)$targetWallet->amount + (float)@$amount;

                $insertData['source_after_balance'] = floatval(@$sourceWallet->amount) - floatval(@$amount);
                $adminUpdatedWalletAmount = $insertData['source_after_balance'] ;
                Wallets::where('id', $sourceWallet->id)->update(['amount' => $insertData['source_after_balance']]);


            } else if ($input['transaction_type'] == "deposit") {

                $insertData['conversion_rate'] = $currencySource->exchange_rate;
                $amount = @(float)$input['transaction_amount'] * ((float)$currencySource->exchange_rate);

                $insertData['source_after_balance'] = floatval(@$sourceWallet->amount) - floatval(@$input['transaction_amount']);

                $insertData['target_after_balance'] = (float)$targetWallet->amount + (float)@$amount;
                Wallets::where('id', $sourceWallet->id)->update(['amount' => $insertData['source_after_balance']]);
            }

            // dd($insertData);

             //  =======================================Add Other Currency Amount =======================================
            //  =======================================Add Other Currency Amount =======================================
            $configurations = TenantConfigurations::select('allowed_currencies')->where('tenant_id', $tenant_id)->get();
            $configurations = $configurations->toArray();
            $CurrenciesValue=[];
            if ($configurations) {

                $configurations = $configurations[0];
                if (strlen($configurations['allowed_currencies']) > 1) {
                    $configurations = str_replace('{', '', $configurations['allowed_currencies']);
                    $configurations = str_replace('}', '', $configurations);
                    $configurations = explode(',', $configurations);

                    if (@$configurations[0] && count($configurations) > 0) {
                        $CurrenciesValue = Currencies::select('code')->whereIn('id', $configurations)->pluck('code');
                        $CurrenciesValue = $CurrenciesValue->toArray();
                    }
                }
            }

            $currenciesTempArray = $insertData['other_currency_amount'] = [];

            $exMainTransactionCurrecy = '';
            if(isset($currencySource->id)){
                $exMainTransactionCurrecy = $currencySource->code;
            }else{
                $exMainTransactionCurrecy =Currencies::select('code')->whereIn('id',   $targetWallet->currency_id)->pluck('code');
                $exMainTransactionCurrecy =$exMainTransactionCurrecy->toArray();;
            }

            $amount_currency_ex = Currencies:: getExchangeRateCurrency($exMainTransactionCurrecy);
            // dd($amount_currency_ex);
            foreach ($CurrenciesValue as $key => $value) {
                if($exMainTransactionCurrecy != $value){
                    $getExchangeRateCurrency = Currencies:: getExchangeRateCurrency($value);
                    $currenciesTempArray[$value] = (float)number_format((float)$input['transaction_amount'] * ($amount_currency_ex / $getExchangeRateCurrency),4,".","");
                }else{
                    $currenciesTempArray[$value] = (float)$input['transaction_amount'];
                }

            }
            $insertData['other_currency_amount'] = json_encode($currenciesTempArray);

            //  =======================================Add Other Currency Amount END =======================================
            //  =======================================Add Other Currency Amount ENd =======================================


            $createdValues = Transactions::create($insertData);

            Wallets::where('id', $targetWallet->id)->update(['amount' => $insertData['target_after_balance']]);

            if(@$createdValues->id){

               // Transactions::storeElasticSearchData($createdValues->id);
                // if ($input['transaction_type'] == "deposit") {
                //   Transactions::storeElasticSearchSuperAdminTransactionData($createdValues->id,"debit");
                // }
                


                if($targetWallet->owner_type == USER_TYPE){
                    $this->updateUserBonus($request->all(),$createdValues);
                }

                $userSourceWalletDetails = TransactionsService :: getUserWalletDetails($targetWallet->id);

                if($userSourceWalletDetails->owner_type == 'User'){
                    //User::storeElasticSearchData($userSourceWalletDetails->user_id);
                }

            }
            //Entry in queueLog
            $queueLog = QueueLog::create([
                'type' => CASINO_TRANSACTION,
                'ids' => json_encode([$createdValues->id])
            ]);
            DB::commit();


            if ($createdValues) {
                try {
                    $redis = Redis::connection();
                    $jsonArray=[];
                    $jsonArray['QueueLog']= [
                        'queueLogId'=> $queueLog->id
                    ];
                    $redis->publish('QUEUE_WORKER', json_encode($jsonArray)
                    );
                } catch (\Throwable $th) {
                //throw $th;
                }
                return returnResponse(true, $successMessage, [], Response::HTTP_CREATED, true);
            } else {

                return returnResponse(false, $errorMessage, [], Response::HTTP_BAD_REQUEST, true);
            }
        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }

        }
    }

    /**
     * @description updateWithdrawalStatus
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateWithdrawalStatus(Request $request)
    {
        try {
                    // ================= permissions starts here =====================
                    if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                        $params = new \stdClass();
                        $params->module = 'withdrawal_request';
                        $params->action = 'T';
                        $params->admin_user_id = Auth::user()->id;
                        $params->tenant_id = Auth::user()->tenant_id;
    
                        $permission = checkPermissions($params);   
                        if(!$permission){ 
                            return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                        }
                        }
                        // ================= permissions ends here ===================== 
    
            $validatorArray = [
                'id' => 'required',
                'status' => 'required'
            ];
            
    
            $validator = Validator::make($request->all(), $validatorArray);
    
            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
            }
    
             // Verify Admin Password
             if(!isset($request->data[1])){
              throw new \ErrorException('Please enter valid password');
            }
    
            if(!Hash::check($request->data[1], Auth::user()->encrypted_password)){
              return returnResponse(false, 'Password Not Matched', [], Response::HTTP_EXPECTATION_FAILED);
            }
    
            $withdrawRequest = WithdrawRequest::find($request->id, ['withdrawal_type',
            'status',
            'fd_transaction_id',
            'payment_transaction_id',
            'mode',
            'remark',
            'actioned_at',
            'transaction_id',
            'user_id',
            'verify_status',
            'amount',
            'id'
            ]);
    
            if (empty($withdrawRequest)) {
                return returnResponse(false, 'Withdraw Request not Found', [], Response::HTTP_NOT_FOUND);
            }
                        
            $transaction = Transactions::find($withdrawRequest->transaction_id, [ 'actionee_type',
            'actionee_id',
            'status',
            'comments',
            'source_currency_id',
            'amount',
            'source_wallet_id',
            'target_wallet_id',
            'target_currency_id',
            'target_before_balance',
            'target_after_balance',
            'updated_at',
            'transaction_id',
            'id'
            ]);
            if (empty($transaction)) {
                return returnResponse(false, 'Transaction not Found', [], Response::HTTP_NOT_FOUND);
            }

            $type = 'manual';
            if(isset($request->type)){
              $type = $request->type;
            }
            
            DB::beginTransaction();
            $withdrawRequest->update(
              [
                'withdrawal_type'=>$type,
                'status' => $request->status,
                'fd_transaction_id' => (isset($request->fd_transaction_id) ? $request->fd_transaction_id : 'null'),
                'payment_transaction_id' => (isset($request->payment_transaction_id) ? $request->payment_transaction_id : 'null'),
                'mode' => (isset($request->mode) ? $request->mode : 'null'),
                'remark'=>$request->data[0], 
                'actioned_at' => date('Y-m-d H:i:s')
              ]);
              
    
            $status = "approve by Admin";
            if($request->status == 'approved') {
                $userBonuses = UserBonus::select('id')->where(['user_id' => $withdrawRequest->user_id, 'status' => 'active'])->pluck('id')->toArray();
                if (count($userBonuses) > 0) {
                    UserBonus::whereIn('id', $userBonuses)->update(['status' => 'cancelled','updated_at' => date('Y-m-d H:i:s')]);
                }

                if($transaction){
                    $transaction->actionee_type = @Auth::user()->parent_type;
                    $transaction->actionee_id = @Auth::user()->id;
                    $transaction->status ='success';
                    $transaction->comments =($type == 'payment_gateway' ? 'Approved By Payment gateway' : 'approved by admin');
                    $transaction->update();
    
                }
    
                // Update Withdrawal Amount in tenant wallet (Not use in any case)
                // Find Tenant wallet id 
    
                  $tenantWallet = Wallets::select('withdrawal_amount','currency_id','id')->where('owner_id',Auth::user()->id)->where('currency_id',$transaction->source_currency_id)->lockForUpdate()->first();
                  $newTenantWithdrawalAmount = (float)$tenantWallet->withdrawal_amount + (float)$transaction->amount;
                  $tenantWallet->refresh();
                  $tenantWallet->withdrawal_amount = $newTenantWithdrawalAmount;
                  $tenantWallet->save();
                
    
    
                  // Create Withdrawal Transaction
                  $withdrawal_transaction = $transaction;
                  if($withdrawal_transaction){
                    unset($withdrawal_transaction->id);
                    $withdrawal_transaction->target_wallet_id = $tenantWallet->id;
                    $withdrawal_transaction->target_currency_id = $tenantWallet->currency_id;
                    $withdrawal_transaction->target_before_balance = $tenantWallet->withdrawal_amount;
                    $withdrawal_transaction->target_after_balance = $newTenantWithdrawalAmount;
                    $withdrawal_transaction->updated_at = date('Y-m-d H:i:s');
                    $withdrawal_transaction->transaction_id = getUUIDTransactionId();
                    WithdrawalTransactions::create($withdrawal_transaction->toArray());
                  }
    
    
            }elseif ($request->status == 'rejected'){
                if($transaction){
                    $status = 'rejected by Admin';
                    $transaction->actionee_type = @Auth::user()->parent_type;
                    $transaction->actionee_id = @Auth::user()->id;
                    $transaction->status ='cancelled';
                    $transaction->comments ='cancelled by admin';
                    $transaction->update();
                    if($request->page == 'unverified')
                    $withdrawRequest->update(['verify_status'=>'rejected']);
    
                    $userWallet = Wallets::where('id', $transaction->source_wallet_id)->lockForUpdate()->first();    
                    $userWallet->refresh();
                    $updatedAmount = $withdrawRequest->amount + $userWallet->amount;
                    $userWallet->amount  = $updatedAmount;
                    $userWallet->save();
                    //User::storeElasticSearchData($withdrawRequest->user_id);
                    $queueLog = QueueLog::create([
                        'type' => USER_TRANSACTION,
                        'ids' => json_encode([$withdrawRequest->user_id])
                    ]);
                    try {
                        $redis = Redis::connection();
                        $jsonArray=[];
                        $jsonArray['QueueLog']= [
                            'queueLogId'=> $queueLog->id
                        ];
                        $redis->publish('QUEUE_WORKER', json_encode($jsonArray)
                        );
                    } catch (\Throwable $th) {
                    //throw $th;
                    }
    
                }
            }elseif ($request->status == 'rejected_by_gateway'){
              $withdrawRequest->update(['remark'=>$request->remark]);
              if($transaction){
                  $status = 'rejected by gateway';
                  $transaction->actionee_type = @Auth::user()->parent_type;
                  $transaction->actionee_id = @Auth::user()->id;
                  $transaction->status ='rejected';
                  $transaction->comments ='rejected by payment gateway';
                  $transaction->update();
                  
                      $userWallet = Wallets::where('id', $transaction->source_wallet_id)->lockForUpdate()->first();    
                      $userWallet->refresh();
                      $updatedAmount = $withdrawRequest->amount + $userWallet->amount;
                      $userWallet->amount  = $updatedAmount;
                      $userWallet->save();
                  //User::storeElasticSearchData($withdrawRequest->user_id);
                  $queueLog = QueueLog::create([
                    'type' => USER_TRANSACTION,
                    'ids' => json_encode([$withdrawRequest->user_id])
                 ]);
                 try {
                    $redis = Redis::connection();
                    $jsonArray=[];
                    $jsonArray['QueueLog']= [
                        'queueLogId'=> $queueLog->id
                    ];
                    $redis->publish('QUEUE_WORKER', json_encode($jsonArray)
                    );
                } catch (\Throwable $th) {
                //throw $th;
                }
    
              }
          }
            
            if(@$withdrawRequest->transaction_id){
                //Transactions::storeElasticSearchData($withdrawRequest->transaction_id);
                //Entry in queueLog
                $queueLog = QueueLog::create([
                    'type' => CASINO_TRANSACTION,
                    'ids' => json_encode([$withdrawRequest->transaction_id])
                ]);
                try {
                    $redis = Redis::connection();
                    $queueArray=[];
                    $queueArray['QueueLog']= [
                        'queueLogId'=> $queueLog->id
                    ];
                    $redis->publish('QUEUE_WORKER', json_encode($queueArray)
                    );
                } catch (\Throwable $th) {
                //throw $th;
                }
                $receiverId = $withdrawRequest->user_id;
                $receiverType = USER_TYPE;
                $message = "Your withdrawal request of ".$withdrawRequest->amount." has been ".$status;
                $id = NotificationService::addNotification($receiverId, $receiverType, $message);
    
               try {
                $redis = Redis::connection();
                $jsonArray=[];
                $jsonArray['PlayerNotification']= [
                    'message' => $message,
                    'userId' => "$withdrawRequest->user_id",
                    'senderType'=>ADMIN_TYPE,
                    'referenceId'=>$receiverId,
                    'referenceType'=>USER_TYPE,
                    'id'=>@$id->id
    
                ];
                $redis->publish('PLAYER_NOTIFICATION_CHANNEL', json_encode($jsonArray)
                );
               } catch (\Throwable $th) {
                //throw $th;
               }
            }
            DB::commit();
            return returnResponse(true, 'Withdraw Request updated successfully', [], Response::HTTP_OK);
    
        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
    
        }
    }
   
   // Used for auto payment provider update status
    public function updateAutoVerifyWithdrawalStatus(Request $request)
    { 
           // ================= permissions starts here =====================
           if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'withdrawal_request';
            $params->action = 'U';
            $params->admin_user_id = Auth::user()->id;
               $params->tenant_id = Auth::user()->tenant_id;

               $permission = checkPermissions($params);
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here =====================   
        try {

            $validatorArray = [
                'id' => 'required',
                'status' => 'required'
            ];

            $validator = Validator::make($request->all(), $validatorArray);

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
            }

            // Verify Admin Password
            if(!isset($request->data[1])){
              throw new \ErrorException('Please enter valid password');
            }

            if(!Hash::check($request->data[1], Auth::user()->encrypted_password)){
              return returnResponse(false, 'Password Not Matched', [], Response::HTTP_EXPECTATION_FAILED);
            }
            DB::beginTransaction();
            // get withdrawal payment data
            $paymentCredentials = [];

            
            // $gatewayData = DB::table('tenant_payment_configurations')->where('id',$request->data[2])->first();
            $gatewayData = DB::connection('read_db')->table('tenant_payment_configurations as pc')->select('provider_name','provider_key_values')->where(['pc.id'=>$request->data[2]])->where("pp.provider_type","withdrawal")->leftJoin('payment_providers as pp','pp.id','=','pc.provider_id')->first();
            if($gatewayData){
              $paymentCredentials =  json_decode($gatewayData->provider_key_values,true);
            }else{
              return returnResponse(false, 'No Payment Provider Found', [], Response::HTTP_EXPECTATION_FAILED);
            }
            

            $withdrawRequest = WithdrawRequest::where('id', $request->id)->where('tenant_id',Auth::user()->tenant_id)->select('user_id','transaction_id','ifsc_code','amount','account_number','bank_name')->first();
            $user = User::on('read_db')->where('id',$withdrawRequest->user_id)->where('tenant_id',Auth::user()->tenant_id)->select('email', 'phone', 'first_name','last_name', 'id')->first();
            if (empty($withdrawRequest)) {
                return returnResponse(false, 'Withdrawal Request not Found', [], Response::HTTP_NOT_FOUND);
            }


            $transaction = Transactions::where('id',$withdrawRequest->transaction_id)->where('tenant_id',Auth::user()->tenant_id)->select('id','transaction_id','actionee_type','actionee_id','status','comments')->first();
            if (empty($transaction)) {
                return returnResponse(false, 'Transaction not Found', [], Response::HTTP_NOT_FOUND);
            }
           
            if (empty($user)) {
                return returnResponse(false, 'User not Found', [], Response::HTTP_NOT_FOUND);
            }


            // send Payout Request to the partner system 
            try {
                $fdTxnId = rand('100','999').'_'.rand('100000','999999');
                if($gatewayData->provider_name === SKY_WITHDRAW){
                    $merchant_RefID = Str::random(16);
                    $guzzle_client = new Client(['base_uri' => $paymentCredentials['baseUrl'] . '/payout/Transfer']);
                    $hash = 'AccountNo='. $withdrawRequest['account_number'] . '|Amount='. (float)$withdrawRequest['amount'].'|Beneficiary_Email='. $user['email']. '|Beneficiary_Mobile=' . $user['phone'] . '|Beneficiary_Name=' . $user['first_name'] . '|IFSC=' . $withdrawRequest['ifsc_code'] . '|Merchant_RefID=' . $merchant_RefID . '|Remark=withdraw|TxnMode=IMPS';
                    $sha256Hash = hash_hmac('sha256', $hash, $paymentCredentials['payoutApiKey'] );
                    $response = $guzzle_client->request('POST', '', [
                        'headers' => [
                            'Content-Type' => 'application/json',
                        ],
                        'auth' => [$paymentCredentials['merchantUserId'], $paymentCredentials['payoutApiKey']],
                        'json' => [
                            "AccountNo"=>$withdrawRequest['account_number'],
                            "Amount"=>(float)$withdrawRequest['amount'],
                            "Beneficiary_Email"=>$user['email'],
                            "Beneficiary_Mobile"=>$user['phone'],
                            "Beneficiary_Name"=>$user['first_name'],
                            "IFSC"=>$withdrawRequest['ifsc_code'],
                            "Merchant_RefID"=>$merchant_RefID,
                            "Remark"=>"withdraw",
                            "TxnMode"=> $paymentCredentials['paymentMode'] ?? 'IMPS',
                            "Hash"=>$sha256Hash
                        ]
                    ])->getBody()->getContents();
                    $response = json_decode($response);
                            if($response->respCode === "0" && $response->respStatus === "SUCCESS"){
                                if($response->data->StatusCode === '2' && $response->data->TxnStatus === 'PENDING'){
                                    WithdrawRequest::find($request->id)->update(
                                      ['withdrawal_type'=>'payment_gateway',
                                      'status' => 'pending_by_gateway',
                                      'fd_transaction_id' => $merchant_RefID,
                                      'payment_transaction_id' => $response->data->Gateway_RefID,
                                      'mode' => $paymentCredentials['paymentMode'] ?? 'IMPS',
                                      'remark'=>$request->data[0], 
                                      'actioned_at' => date('Y-m-d H:i:s'
                                    )]);
                                    if(isset($response->data->Gateway_RefID))
                                    $transaction->transaction_id = $response->data->Gateway_RefID;
                                    $transaction->actionee_type = @Auth::user()->parent_type;
                                    $transaction->actionee_id = @Auth::user()->id;
                                    $transaction->status ='pending';
                                    $transaction->comments ='Pending by payment gateway';
                                    $transaction->update();

                                        if(isset($withdrawRequest['transaction_id'])){
                                            // Transactions::storeElasticSearchData($withdrawRequest['transaction_id']);
                                            $queueLog = QueueLog::create([
                                                'type' => CASINO_TRANSACTION,
                                                'ids' => json_encode([$withdrawRequest['transaction_id']])
                                            ]);
                                            try {
                                                $redis = Redis::connection();
                                                $jsonArray=[];
                                                $jsonArray['QueueLog']= [
                                                    'queueLogId'=> $queueLog->id
                                                ];
                                                $redis->publish('QUEUE_WORKER', json_encode($jsonArray)
                                                );
                                            } catch (\Throwable $th) {
                                            //throw $th;
                                            }
                                        }
                                
                                }
                                else if ($response->data->StatusCode === '0' && $response->data->TxnStatus === 'SUCCESS'){
                                    $request['status'] = 'approved';
                                    $request['type'] = 'payment_gateway';
                                    $request['mode'] = $paymentCredentials['paymentMode'] ?? 'IMPS';
                                    $request['payment_transaction_id'] = $response->data->Gateway_RefID;
                                    $request['fd_transaction_id'] = $merchant_RefID;
                                    TransactionsController::updateWithdrawalStatus($request);
              
                                  }
                              else{
                                       $request['status'] = 'rejected_by_gateway';
                                       $request['type'] = 'payment_gateway';
                                       $request['mode'] = $paymentCredentials['paymentMode'] ?? 'IMPS';
                                       $request['remark'] = $response->data->TxnStatus;
                                       $request['payment_transaction_id'] = (isset($response->data->Gateway_RefID) ? $response->data->Gateway_RefID : null);
                                       $request['fd_transaction_id'] = $merchant_RefID;
                                       TransactionsController::updateWithdrawalStatus($request);
                
                                  DB::commit();
                                  return returnResponse(false, 'Withdrawal request has been rejected by payment gateway: Response: '.$response->respMsg, [], Response::HTTP_EXPECTATION_FAILED);
                
                              }
                            }
                }
                if($gatewayData->provider_name === INDIGRIT_WITHDRAW){
                    $guzzle_client = new Client(['base_uri' => $paymentCredentials['payment_url'] . '/payouts']);
                    $data = [
                        'type' => $paymentCredentials['payment_mode'] ?? 'imps', 
                        'customer_id' => (string)$user['id'],
                        'tracking_id' =>$fdTxnId, 
                        'amount' => (string)$withdrawRequest['amount'],
                        'acc_id' => $withdrawRequest['account_number'],
                        'name' => $user['first_name'] .' '.$user['last_name'],
                        // 'branch', //optional
                        'bank_name' => $withdrawRequest['bank_name'],
                        'ifsc' => $withdrawRequest['ifsc_code'],
                        'acc_type' => 'savings',
                        'email' => $user['email'],
                        'mobile' => $user['phone'],
                        'simulate' => "success" // The possible values are “success” or “failed”.
                    ];
                    // if($paymentCredentials['payment_mode'] == 'upi'){
                    //     $data = [
                    //         'type' => $paymentCredentials['payment_mode'], 
                    //         'customer_id' => (string)$user['id'],
                    //         'tracking_id' =>$fdTxnId, 
                    //         'amount' => (string)$withdrawRequest['amount'],
                    //         'name' => $user['first_name'] .' '.$user['last_name'],
                    //         'vpa', $withdrawRequest['upi_id'],
                    //         'email' => $user['email'],
                    //         'mobile' => $user['phone'],
                    //         'simulate' => "success"
                    //     ];
                    // }
                    $response = $guzzle_client->request('POST', '', [
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'idempotency-key' => $fdTxnId
                        ],
                        'auth' => [$paymentCredentials['secret_key'], ''],
                        'json' => $data
                    ])->getBody()->getContents();
                    $response = json_decode($response);
                    if($response->status === 'pending'){
                          // Pending Case
                          WithdrawRequest::find($request->id)->update(
                            ['withdrawal_type'=>'payment_gateway',
                            'status' => 'pending_by_gateway',
                            'fd_transaction_id' => $fdTxnId,
                            'payment_transaction_id' => $response->payout_id,
                            'mode' => $paymentCredentials['payment_mode'] ?? 'imps',
                            'remark'=>$request->data[0], 
                            'actioned_at' => date('Y-m-d H:i:s'
                          )]);
                          if(isset($response->payout_id))
                          $transaction->transaction_id = $response->payout_id;
                          $transaction->actionee_type = @Auth::user()->parent_type;
                          $transaction->actionee_id = @Auth::user()->id;
                          $transaction->status ='pending';
                          $transaction->comments ='Pending by payment gateway';
                          $transaction->update();

                          if(isset($withdrawRequest['transaction_id'])){
                              //   Transactions::storeElasticSearchData($withdrawRequest['transaction_id']);
                              $queueLog = QueueLog::create([
                                  'type' => CASINO_TRANSACTION,
                                  'ids' => json_encode([$withdrawRequest['transaction_id']])
                              ]);
                              try {
                                  $redis = Redis::connection();
                                  $jsonArray=[];
                                  $jsonArray['QueueLog']= [
                                      'queueLogId'=> $queueLog->id
                                  ];
                                  $redis->publish('QUEUE_WORKER', json_encode($jsonArray)
                                  );
                              } catch (\Throwable $th) {
                              //throw $th;
                              }
                          }
                    
                    }
                    else{
    
                             $request['status'] = 'rejected_by_gateway';
                             $request['type'] = 'payment_gateway';
                             $request['mode'] = $paymentCredentials['payment_mode'] ?? 'imps';
                             $request['remark'] = $response->status;
                             $request['payment_transaction_id'] = (isset($response->payout_id) ? $response->payout_id : null);
                             $request['fd_transaction_id'] = $fdTxnId;
                             TransactionsController::updateWithdrawalStatus($request);
      
                        DB::commit();
                        return returnResponse(false, 'Withdrawal request has been rejected by payment gateway', [], Response::HTTP_EXPECTATION_FAILED);
      
                    }
                }
                if($gatewayData->provider_name === PAYOUT_WITHDRAW){
                    $endpoint = $paymentCredentials['payment_url'].'/api/payout/FundTransfer';
                    $headers = array (
                      'secretKey:'.$paymentCredentials['secret_key'].''
                    );
                    $data = [
                        "partnerUserName" => $paymentCredentials['partner_user_name'],
                        "secretKey"=>$paymentCredentials['secret_key'], 
                        "fdTransactionID" => $fdTxnId, 
                        "accountNumber" => $withdrawRequest['account_number'],
                        "ifsc" => $withdrawRequest['ifsc_code'],
                        "name"=>$user['first_name'] .' '.$user['last_name'],  
                        "amount" => (int)$withdrawRequest['amount'],
                        "mode" => $paymentCredentials['payment_mode'] ?? 'IMPS',
                        "email" =>$user['email'],
                        "phone" => $user['phone']
                    ];
                    // Call provider gateway to initiate payment request
                    $response = payoutCall($endpoint,json_encode($data),$headers);
                    $response = json_decode($response,true);
                    if(!$response){
                        DB::rollBack();
                        return returnResponse(false, 'Payment provider is not activated', [], Response::HTTP_OK);
                    }
                    if($response['status']['code'] == 100){
    
                        if($response['data']['status'] == 'Pending'){
                          // Pending Case
                          WithdrawRequest::find($request->id)->update(
                            ['withdrawal_type'=>'payment_gateway',
                            'status' => 'pending_by_gateway',
                            'fd_transaction_id' => $fdTxnId,
                            'payment_transaction_id' => $response['data']['transactionID'],
                            'mode' => $paymentCredentials['payment_mode'] ?? 'IMPS',
                            'remark'=>$request->data[0], 
                            'actioned_at' => date('Y-m-d H:i:s'
                          )]);
                          if(isset($response['data']['transactionID']))
                          $transaction->transaction_id = $response['data']['transactionID'];
                          $transaction->actionee_type = @Auth::user()->parent_type;
                          $transaction->actionee_id = @Auth::user()->id;
                          $transaction->status ='pending';
                          $transaction->comments ='Pending by payment gateway';
                          $transaction->update();

                          if(isset($withdrawRequest['transaction_id'])){
                              //   Transactions::storeElasticSearchData($withdrawRequest['transaction_id']);
                              $queueLog = QueueLog::create([
                                  'type' => CASINO_TRANSACTION,
                                  'ids' => json_encode([$withdrawRequest['transaction_id']])
                              ]);
                              try {
                                  $redis = Redis::connection();
                                  $jsonArray=[];
                                  $jsonArray['QueueLog']= [
                                      'queueLogId'=> $queueLog->id
                                  ];
                                  $redis->publish('QUEUE_WORKER', json_encode($jsonArray)
                                  );
                              } catch (\Throwable $th) {
                              //throw $th;
                              }
                          }
    
    
                        }else if ($response['data']['status'] == 'Completed'){
                          // success
                          $request['status'] = 'approved';
                          $request['type'] = 'payment_gateway';
                          $request['mode'] = $response['data']['mode'];
                          $request['payment_transaction_id'] = $response['data']['transactionID'];
                          $request['fd_transaction_id'] = $fdTxnId;
                          TransactionsController::updateWithdrawalStatus($request);
    
                        }
                    
                    }else{
    
    
                      if(in_array($response['status']['code'],PAYOUT_FAILED_TRANSACTION)){
                           //failed Case
                           $request['status'] = 'rejected_by_gateway';
                           $request['type'] = 'payment_gateway';
                           $request['mode'] = $response['data']['mode'];
                           $request['remark'] = $response['status']['returnMessage'];
                           $request['payment_transaction_id'] = (isset($response['data']['transactionID']) ? $response['data']['transactionID'] : null);
                           $request['fd_transaction_id'] = $fdTxnId;
                           TransactionsController::updateWithdrawalStatus($request);
                           DB::commit();
                           return returnResponse(false, 'Withdrawal request has been rejected by payment gateway: Response: '.$response['status']['returnMessage'], [], Response::HTTP_OK);
    
                      }
    
                      DB::commit();
                      return returnResponse(false, 'Error From Payment Gateway:  '.$response['status']['returnMessage'], [], Response::HTTP_EXPECTATION_FAILED);
    
                    }
    
    
                }

            }catch (\Exception $e) {
              DB::rollback();
              if (!App::environment(['local'])) {//, 'staging'
                  return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage()], Response::HTTP_EXPECTATION_FAILED);
              } else {
                  return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage()], Response::HTTP_EXPECTATION_FAILED);
              }
            }


           
            
            
                
            DB::commit();
            return returnResponse(true, 'Withdrawal Request updated successfully', $withdrawRequest, Response::HTTP_OK);

        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }

        }
    }
    
    
    
    public function updateVerifyWithdrawalStatus(Request $request)
    {
        try {
              // ================= permissions starts here =====================
              if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'withdrawal_request';
                $params->action = 'T';
                $params->admin_user_id = Auth::user()->id;
                  $params->tenant_id = Auth::user()->tenant_id;

                  $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================   

            $validatorArray = [
                'id' => 'required',
                'status' => 'required'
            ];

            $validator = Validator::make($request->all(), $validatorArray);

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
            }

            // Verify Admin Password
            if(!isset($request->data[1])){
              throw new \ErrorException('Please enter valid password');
            }

            if(!Hash::check($request->data[1], Auth::user()->encrypted_password)){
              return returnResponse(false, 'Password Not Matched', [], Response::HTTP_EXPECTATION_FAILED);
            }



            $WithdrawRequest = WithdrawRequest::find($request->id, ['id']);

            if (empty($WithdrawRequest)) {
                return returnResponse(false, 'Withdrawal Request not Found', [], Response::HTTP_NOT_FOUND);
            }
            DB::beginTransaction();
            
            $status = "rejected";
            if($request->status == 'verify') {
              $status = 'verified';
                  
            }
            $update = [
                "verify_status" => $status,
                "remark" => $request->data[0],
                'updated_at' => date('Y-m-d H:i:s'),
                'actioned_at' => date('Y-m-d H:i:s'),
                'actionable_type' => @Auth::user()->parent_type,
                'actionable_id' => @Auth::user()->id,
            ];
        
        
            WithdrawRequest::where('id', $request->id)->update($update);
        
                
            DB::commit();
            return returnResponse(true, 'Withdrawal Request updated successfully', [], Response::HTTP_OK);

        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }

        }
    }
    
    
    
    public function getWithdrawalPaymentList(Request $request)
    {
        try {
                    // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                    $params = new \stdClass();
                    $params->module = 'withdrawal_request';
                    $params->action = 'R';
                    $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);   
                    if(!$permission){ 
                        return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                    }
                    }
                    // ================= permissions ends here =====================   

          
            // payment provider data
            $data['paymentProvider'] = DB::connection('read_db')->table('tenant_payment_configurations as pc')->select('pc.id','pp.provider_name','pp.provider_type', 'pc.vip_level')->where(['pc.tenant_id'=>@Auth::user()->tenant_id])->where("pp.provider_type","withdrawal")->leftJoin('payment_providers as pp','pp.id','=','pc.provider_id')->get();
            return returnResponse(true, 'Withdrawal Payment Data', $data, Response::HTTP_OK);

        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }

        }
    }
    
    
    
    public function updateDepositRequest(Request $request)
    {
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'deposit_requests';
                $params->action = 'T';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          

            $validatorArray = [
                'id' => 'required',
                'status' => 'required'
            ];

            $validator = Validator::make($request->all(), $validatorArray);

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
            }

            // Verify Admin Password
            if(!isset($request->data[1])){
              throw new \ErrorException('Please enter valid password');
            }

            if(!Hash::check($request->data[1], Auth::user()->encrypted_password)){
              return returnResponse(false, 'Password Not Matched', [], Response::HTTP_EXPECTATION_FAILED);
            }



            $depositRequest = DepositRequest::find($request->id);

            if (empty($depositRequest)) {
                return returnResponse(false, 'Deposit Request not Found', [], Response::HTTP_NOT_FOUND);
            }
            DB::beginTransaction();
            
            $status = "rejected";
            $wallet = [];
            if($request->status == 'approved') {

              // Wallet Check fot tenant 
              $sqlCheck="SELECT 
              *
              FROM wallets as u
              where u.id = ?;
                                    
              ";

              $currencySource = Currencies::getPrimaryCurrency();
              $sourceWallet = DB::select($sqlCheck,[$request->data[2]])[0];
                 //Get user wallet Id
                 $walletDetails = getWalletDetails($depositRequest['user_id']);
                 if(!$walletDetails){
                   throw new \ErrorException('User Wallet not found');
                 }
 
                 $targetWallet = $walletDetails[0];
              // dd($sourceWallet);
              $source_exchange_rate = Currencies::where('id', $sourceWallet->currency_id)->select('exchange_rate')->first();
              $target_exchange_rate = Currencies::where('id', $targetWallet->currency_id)->select('exchange_rate')->first();
              $trnAmount = (float)$depositRequest['amount'];
                (float)$depositRequest['amount'] = (float)number_format((float)$depositRequest['amount'] * ($source_exchange_rate->exchange_rate/$target_exchange_rate->exchange_rate),4,".","");
                if(!$sourceWallet){
                throw new \ErrorException('This Wallet is not available. Try with other wallet.');
              }
              if ((float)round($sourceWallet->amount,5) < (float)round($depositRequest['amount'],5)) {
                return returnResponse(false, 'Insufficient amount in your wallet!',
                    [], Response::HTTP_EXPECTATION_FAILED);
              }
            
              $status = 'completed';
                $transactionType=TRANSACTION_TYPE_DEPOSIT;
                  
             
                $owner_id = $owner_type = 0;
                $owner_id = $targetWallet->owner_id;
                $owner_type = $targetWallet->owner_type;
        
        
        
        
                    $tenant_id = Tenants::getSuperTenantId($owner_id, $owner_type);
                  

                    if (!$tenant_id) {
                        throw new \ErrorException($tenant_id . ' tenant ID not found');
                    }
        
                   
                    if (!(float)$depositRequest['amount']) {
                      return returnResponse(false, 'Please fill Proper Amount ',
                          [], Response::HTTP_EXPECTATION_FAILED);
                    }
                    $insertData = [
                        'actionee_type' => @Auth::user()->parent_type,
                        'actionee_id' => @Auth::user()->id,
                        'target_wallet_id' => $targetWallet->id,
                        'target_before_balance' => $targetWallet->amount,
                        'status' => 'success',
                        'tenant_id' => $tenant_id,
                        'target_currency_id' => $targetWallet->currency_id,
                        'created_at' => @date('Y-m-d H:i:s'),
                        'comments' => 'Deposit Request Approved',
                        'transaction_type' => $transactionType,
                        'amount' => $trnAmount,
                        'payment_method' => 'manual',
                        'transaction_id'=>getUUIDTransactionId(),
                        'target_wallet_id'=> $targetWallet->id,
                        'target_before_balance'=> $targetWallet->amount,
                        'target_currency_id'=> $targetWallet->currency_id,
                        'source_currency_id'=> $sourceWallet->currency_id,
                        'source_wallet_id'=>  $sourceWallet->id,
                        'source_before_balance'=> $sourceWallet->amount,
                    ];
        
                    // if($targetWallet->amount < 0.00000000){
                    //     $targetWallet->amount = 0.00000000;
                    // }
                    $insertData['conversion_rate'] = ($target_exchange_rate->exchange_rate/$source_exchange_rate->exchange_rate); 
                    $amount = @(float)$depositRequest['amount'] * (float)($target_exchange_rate->exchange_rate/$source_exchange_rate->exchange_rate);

                    // $insertData['source_after_balance'] = 0.00;
                    $insertData['source_after_balance'] = floatval($sourceWallet->amount) - floatval($depositRequest['amount']);
        
                    $insertData['target_after_balance'] = (float)$targetWallet->amount + (float)@$amount;
                    
        
        
        
                     //  =======================================Add Other Currency Amount =======================================
                    //  =======================================Add Other Currency Amount =======================================
                    $configurations = TenantConfigurations::select('allowed_currencies')->where('tenant_id', $tenant_id)->get();
                    $configurations = $configurations->toArray();
                    $CurrenciesValue=[];
                    if ($configurations) {
        
                        $configurations = $configurations[0];
                        if (strlen($configurations['allowed_currencies']) > 1) {
                            $configurations = str_replace('{', '', $configurations['allowed_currencies']);
                            $configurations = str_replace('}', '', $configurations);
                            $configurations = explode(',', $configurations);
        
                            if (@$configurations[0] && count($configurations) > 0) {
                                $CurrenciesValue = Currencies::select('code')->whereIn('id', $configurations)->pluck('code');
                                $CurrenciesValue = $CurrenciesValue->toArray();
                            }
                        }
                    }
        
                    $currenciesTempArray = $insertData['other_currency_amount'] = [];
        
                    $exMainTransactionCurrecy = '';
                    if(isset($currencySource->id)){
                        $exMainTransactionCurrecy = $currencySource->code;
                    }else{
                        $exMainTransactionCurrecy =Currencies::select('code')->whereIn('id',   $targetWallet->currency_id)->pluck('code');
                        $exMainTransactionCurrecy =$exMainTransactionCurrecy->toArray();;
                    }
        
                    $amount_currency_ex = Currencies:: getExchangeRateCurrency($exMainTransactionCurrecy);
                    foreach ($CurrenciesValue as $key => $value) {
                        // if($exMainTransactionCurrecy != $value){
                            $getExchangeRateCurrency = Currencies:: getExchangeRateCurrency($value);
                            $currenciesTempArray[$value] = (float)number_format((float)$depositRequest['amount'] * ($getExchangeRateCurrency/$source_exchange_rate->exchange_rate),4,".","");
                        // }else{
                        //     $currenciesTempArray[$value] = (float)$depositRequest['amount'];
                        // }
                        
                    }
                    $insertData['other_currency_amount'] = json_encode($currenciesTempArray);
        
                    //  =======================================Add Other Currency Amount END =======================================
                    //  =======================================Add Other Currency Amount ENd =======================================
        
        
        
        
                    $createdValues = Transactions::create($insertData);

                    Wallets::where('id', $sourceWallet->id)->update(['amount' => $insertData['source_after_balance']]);

        
                    Wallets::where('id', $targetWallet->id)->update(['amount' => $insertData['target_after_balance']]);
        
                    if(@$createdValues->id){
                        //Transactions::storeElasticSearchData($createdValues->id);
                        //Entry in queueLog
                        $queueLog = QueueLog::create([
                            'type' => CASINO_TRANSACTION,
                            'ids' => json_encode([$createdValues->id])
                        ]);
                        try {
                            $redis = Redis::connection();
                            $jsonArray=[];
                            $jsonArray['QueueLog']= [
                                'queueLogId'=> $queueLog->id
                            ];
                            $redis->publish('QUEUE_WORKER', json_encode($jsonArray)
                            );
                        } catch (\Throwable $th) {
                        //throw $th;
                        }
        
        
                        if($targetWallet->owner_type == USER_TYPE){
                          $userBonusData = [
                            'target_email' => $depositRequest['user_id'],
                            'transaction_amount' => $depositRequest['amount'],
                            'transaction_type' => 'deposit'
                          ];
                            $this->updateUserBonus($userBonusData,$createdValues);
                        }
        
                        $userSourceWalletDetails = TransactionsService::getUserWalletDetails($targetWallet->id);
        
                        if($userSourceWalletDetails->owner_type == 'User'){
                           // User::storeElasticSearchData($userSourceWalletDetails->user_id);
                        }
        
                    }
        
                    $wallet = Wallets::where('id', $sourceWallet->id)->first();
                  }
            DepositRequest::find($request->id)->update(['status' => $status,"remark"=>$request->data[0], 'updated_at' => date('Y-m-d H:i:s'),'action_id'=>Auth::user()->id,'action_type' => Auth::user()->parent_type]); 
            
            DB::commit();
           
            return returnResponse(true, 'Deposit Request updated successfully', ["wallet"=>$wallet], Response::HTTP_OK);

        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }

        }
    }

    /**
     * @description updateUserBonus
     * @param $request
     * @param $trans
     */
    private function updateUserBonus($request, $trans)
    {

        $userBonusesExpiresAt = UserBonus::
            where(['user_id' => $request['target_email'],
            'status' => 'active', 'rollover_balance' => 0, 'bonus_amount' => 0])
            ->whereRaw("((kind = 'deposit') OR (kind = 'deposit_sport'))")
            ->whereHas('depositBonusSetting', function ($q) use ($request) {
                $q->where('min_deposit', '<=', $request['transaction_amount']);
                $q->where('max_deposit', '>=', $request['transaction_amount']);
            })
            ->where('expires_at', '>=', Carbon::now()->toDateTimeString())
            ->get();

        $userBonusesBonus = UserBonus::where(['user_id' => $request['target_email'], 'status' => 'active', 'rollover_balance' => 0, 'bonus_amount' => 0])
            ->whereRaw("((kind = 'deposit') OR (kind = 'deposit_sport'))")
            ->whereHas('depositBonusSetting', function ($q) use ($request) {
                $q->where('min_deposit', '<=', $request['transaction_amount']);
                $q->where('max_deposit', '>=', $request['transaction_amount']);
            })
            ->whereNull('expires_at')
            ->whereHas('bonus', function ($bq) {
                $bq->where('valid_from', '<=', Carbon::now()->toDateTimeString());
                $bq->where('valid_upto', '>=', Carbon::now()->toDateTimeString());
            })
            ->get();

        $userBonuses = $userBonusesExpiresAt->merge($userBonusesBonus);
        if (count($userBonuses) > 0) {
            $y=0;
            foreach ($userBonuses as $key => $userBonus) {

                if (!empty($userBonus) && !empty($userBonus->bonus) && !empty($userBonus->depositBonusSetting)) {

                    if ($request['transaction_type'] == 'deposit') {
                        $rolloverAmount = (($request['transaction_amount'] * $userBonus->bonus->percentage) / 100) * $userBonus->depositBonusSetting->rollover_multiplier;

                        $max_bonus = $userBonus->depositBonusSetting->max_bonus;
                        $bonusAmount = (($request['transaction_amount'] * $userBonus->bonus->percentage) / 100);
                        if ($max_bonus && $bonusAmount) {
                            if ($max_bonus <= $bonusAmount) {
                                $bonusAmount = $max_bonus;
                                //$rolloverAmount = (($bonusAmount * $userBonus->bonus->percentage) / 100) * $userBonus->depositBonusSetting->rollover_multiplier;
                                $rolloverAmount = $bonusAmount* $userBonus->depositBonusSetting->rollover_multiplier;
                            }
                        }
                        $user = User::find($request['target_email']);

                        if($user) {
                            $userwallets = Wallets::where('owner_type', 'User')->where('owner_id', $user->id)->first();

                            if ($userBonus->bonus->kind == 'deposit_sport') {

                                if ($y == 0) {
                                    $insertData = [
                                        'amount' => (float)$bonusAmount,
                                        'is_deleted' => false,
                                        'journal_entry' => 'credit',
                                        'reference' => $user->first_name . ' ' . $user->last_name . '-' . $user->id,
                                        'user_id' => $user->id,
                                        'target_currency_id' => $userwallets->currency_id,
                                        'conversion_rate' => $trans->conversion_rate,
                                        'target_wallet_id' => @$userwallets->id,
                                        'target_before_balance' => @$userwallets->amount,
                                        'target_after_balance' => @$userwallets->amount + $bonusAmount,
                                        'status' => 'pending',
                                        'tenant_id' => $user->tenant_id,
                                        'created_at' => @date('Y-m-d H:i:s'),
                                        'updated_at' => @date('Y-m-d H:i:s'),
                                        'description' => 'Pending Transaction for deposit bonus',
                                        'payment_for' => 7,
                                        'current_balance' => @$userwallets->amount
                                    ];
                                    $createdValues = BetTransaction::create($insertData);

                                    UserBonus::where('id',$userBonus->id)
                                    ->update(
                                        [
                                            'rollover_balance' => $rolloverAmount,
                                            'transaction_id' => '' . $createdValues->id,
                                            'bonus_amount' => $bonusAmount
                                        ]);
                                    //create casino transaction
                                    $casinoTxnData = [
                                        'actionee_type' => 'User',
                                        'actionee_id' => $user->id,
                                        'target_currency_id' => $userwallets->currency_id,
                                        'conversion_rate' => $trans->conversion_rate,
                                        'target_wallet_id' => @$userwallets->id,
                                        'target_before_balance' => @$userwallets->amount,
                                        'target_after_balance' => @$userwallets->amount + $bonusAmount,
                                        'amount' => $bonusAmount,
                                        'status' => 'pending',
                                        'tenant_id' => $user->tenant_id,
                                        'created_at' => @date('Y-m-d H:i:s'),
                                        'comments' => 'Pending Transaction for deposit bonus',
                                        'transaction_type' => 37,
                                        'payment_method' => 'manual',
                                        'transaction_id' => getUUIDTransactionId(),
                                        'debit_transaction_id' => $createdValues->id    
                                    ];

                                    $casinoTxnDataCreate = Transactions::create($casinoTxnData);    

                                    // SportsService::storeElasticBetSearchData($createdValues->id, $rolloverAmount);
                                    $queueLog = QueueLog::create([
                                        'type' => BET_TRANSACTION,
                                        'ids' => json_encode([$createdValues->id])
                                    ]);
                                    $casinoTxnQueueLog = QueueLog::create([
                                        'type' => CASINO_TRANSACTION,
                                        'ids' => json_encode([$casinoTxnDataCreate->id])
                                    ]);
                                    try {
                                        $redis = Redis::connection();
                                        $jsonArray=[];
                                        $jsonArray['QueueLog']= [
                                            'queueLogId'=> $queueLog->id
                                        ];
                                        $redis->publish('QUEUE_WORKER', json_encode($jsonArray));
                                        $casinoJsonArray['QueueLog']= [
                                            'queueLogId'=> $casinoTxnQueueLog->id
                                        ];
                                        $redis->publish('QUEUE_WORKER', json_encode($casinoJsonArray)
                                        );
                                    } catch (\Throwable $th) {
                                        //throw $th;
                                    }
                                    $y++;
                                }
                            } else {
                                $insertData = [
                                    'actionee_type' => 'User',
                                    'actionee_id' => $user->id,
                                    'target_currency_id' => $userwallets->currency_id,
                                    'conversion_rate' => $trans->conversion_rate,
                                    'target_wallet_id' => @$userwallets->id,
                                    'target_before_balance' => @$userwallets->amount,
                                    'target_after_balance' => @$userwallets->amount + $bonusAmount,
                                    'amount' => $bonusAmount,
                                    'status' => 'pending',
                                    'tenant_id' => $user->tenant_id,
                                    'created_at' => @date('Y-m-d H:i:s'),
                                    'comments' => 'Pending Transaction for deposit bonus',
                                    'transaction_type' => 12,
                                    'payment_method' => 'manual',
                                    'transaction_id' => getUUIDTransactionId()

                                ];
                                $createdValues = Transactions::create($insertData);

                                $userBonus->update(
                                    [
                                        'rollover_balance' => $rolloverAmount,
                                        'transaction_id' => '' . $createdValues->id,
                                        'bonus_amount' => $bonusAmount
                                    ]);

                                //Transactions::storeElasticSearchData($createdValues->id, $rolloverAmount);
                                $queueLog = QueueLog::create([
                                    'type' => CASINO_TRANSACTION,
                                    'ids' => json_encode([$createdValues->id])
                                ]);
                                try {
                                    $redis = Redis::connection();
                                    $jsonArray=[];
                                    $jsonArray['QueueLog']= [
                                        'queueLogId'=> $queueLog->id
                                    ];
                                    $redis->publish('QUEUE_WORKER', json_encode($jsonArray)
                                    );
                                } catch (\Throwable $th) {
                                //throw $th;
                                }
                                }
                        }

                    } else if ($request['transaction_type'] == 'withdraw') {

                        if($userBonus['transaction_id'] && $userBonus['transaction_id'] !='') {

                            if($userBonus->bonus->kind == 'deposit_sport') {
                                $transaction = BetTransaction::find($userBonus['transaction_id']);
                                if ($transaction) {
                                    $transaction->status = 'failed';
                                    $transaction->payment_for = 8;
                                    $transaction->comments = 'Deposit Bonus cancelled due to amount withdrawal';
                                    $transaction->update();
                                    //SportsService::storeElasticBetSearchData($userBonus['transaction_id'],$rolloverAmount);
                                    $queueLog = QueueLog::create([
                                        'type' => BET_TRANSACTION,
                                        'ids' => json_encode([$userBonus['transaction_id']])
                                    ]);
                                    try {
                                        $redis = Redis::connection();
                                        $jsonArray=[];
                                        $jsonArray['QueueLog']= [
                                            'queueLogId'=> $queueLog->id
                                        ];
                                        $redis->publish('QUEUE_WORKER', json_encode($jsonArray)
                                        );
                                    } catch (\Throwable $th) {
                                    //throw $th;
                                    }
                                }
                            }else {
                                $transaction = Transactions::find($userBonus['transaction_id']);
                                if ($transaction) {
                                    $transaction->status = 'failed';
                                    $transaction->comments = 'Deposit Bonus cancelled due to amount withdrawal';
                                    $transaction->update();
                                   //Transactions::storeElasticSearchData($userBonus['transaction_id']);
                                   $queueLog = QueueLog::create([
                                    'type' => CASINO_TRANSACTION,
                                    'ids' => json_encode([$userBonus['transaction_id']])
                                    ]);
                                    try {
                                        $redis = Redis::connection();
                                        $jsonArray=[];
                                        $jsonArray['QueueLog']= [
                                            'queueLogId'=> $queueLog->id
                                        ];
                                        $redis->publish('QUEUE_WORKER', json_encode($jsonArray)
                                        );
                                    } catch (\Throwable $th) {
                                    //throw $th;
                                    }
                                                                    }
                            }
                        }
                        $userBonus->update(['status' => 'cancelled','updated_at'=>date('Y-m-d H:i:s')]);

                    }

                }

            }

        }else{
            $userBonuses = UserBonus::where(['user_id' => $request['target_email'], 'status' => 'active'])
                ->get();
            if (count($userBonuses) > 0) {
                foreach ($userBonuses as $key => $userBonus) {
                    $userBonusesObj = UserBonus::where(['id' => $userBonus['id']]);
                    $userBonusesObj->update(['status' => 'cancelled', 'updated_at'=>date('Y-m-d H:i:s')]);
                }
                if($userBonus['transaction_id'] && $userBonus['transaction_id'] !='') {

                    $transaction = Transactions::find($userBonus['transaction_id']);
                    if ($transaction) {
                        $transaction->status = 'failed';
                        $transaction->comments = 'Deposit Bonus cancelled due to amount withdrawal';
                        $transaction->update();
                        //Transactions::storeElasticSearchData($userBonus['transaction_id']);
                        $queueLog = QueueLog::create([
                            'type' => CASINO_TRANSACTION,
                            'ids' => json_encode([$userBonus['transaction_id']])
                            ]);
                            try {
                                $redis = Redis::connection();
                                $jsonArray=[];
                                $jsonArray['QueueLog']= [
                                    'queueLogId'=> $queueLog->id
                                ];
                                $redis->publish('QUEUE_WORKER', json_encode($jsonArray)
                                );
                            } catch (\Throwable $th) {
                            //throw $th;
                            }
                        
                    }

                }
            }

        }

    }

    /**
     * @description betList
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function betList(Request $request){
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'sport_transactions';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            $page = $request->has('page') ? $request->post('page') : 1;
            $limit = $request->has('size') ? $request->post('size') : 10;
            $filter['sortBy'] = $request->has('sort_by') ? $request->post('sort_by') : 'created_at';
            $filter['sortOrder'] = $request->has('order') ? $request->post('order') : 'desc';

            $filter['time_period'] = $request->has('time_period') ? $request->post('time_period') : [];
            $filter['action_type'] = $request->has('action_type') ? $request->post('action_type') : '';
            $filter['currency_id'] = $request->has('currency_id') ? $request->post('currency_id') : '';
            $filter['searchKeyword'] = $request->has('search') ? trim($request->post('search')) : '';
            $filter['emailsearch'] = $request->has('emailsearch') ? trim($request->post('emailsearch')) : '';
            $filter['wallet_id'] = $request->has('wallet_id') ? $request->post('wallet_id') : '';
            
            $filter['searchKeyword']=str_replace("'", "", $filter['searchKeyword']);
            
            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $filter['tenant_id'] = Auth::user()->tenant_id;
            } else {
                $filter['tenant_id'] = $request->has('tenant_id') ? $request->post('tenant_id') : '';
            }
            $urlSegment = $request->segment(2);
            if ($urlSegment === 'admin') {
                $filter['login_id'] = Auth::user()->id;
                $filter['login_type'] = ADMIN_TYPE;
            }
            $timeZone = $request->time_zone_name?? 'UTC +00:00';
            $time_period = $request['time_period'] ?? [];
            $time_type = $request['time_type'] ?? 'today';
            $filter['time_period'] = $time_period = $request['datetime'] ?? []; //a
            

            if($filter['time_period']) {
                    $time_period['fromdate'] = getTimeZoneWiseTimeISO($time_period['start_date']);
                    $time_period['enddate']  = getTimeZoneWiseTimeISO($time_period['end_date']);
                $filter['time_period'] = $time_period;
            }
            $record = SportsService::getBetListByES($limit, $page, $filter);
            if ($timeZone) {
                foreach ($record['hits']['hits'] as $task1 => $task) {
                    $task = $task['_source'];
                    if ($task["created_at"])
                        $record['hits']['hits'][$task1]['_source']["created_at"] = getTimeZoneWiseTime(@$task['created_at'], $timeZone);
                    else
                        $record['hits']['hits'][$task1]['_source']["created_at"] = @$task['created_at'];

                }
            }
            return returnResponse(true, "Record get Successfully", $record);

        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }

        }
    }

    public function indexByES(Request $request)
    { 
        try {
            // ================= permissions starts here =====================
            if (request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager") {
                $params = new \stdClass();
                $params->module = 'casino_transactions';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;

                $permission = checkPermissions($params);
                if (!$permission) {
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
            }
            // ================= permissions ends here =====================

            $page = $request->has('page') ? $request->post('page') : 1;
            $limit = $request->has('size') ? $request->post('size') : 10;
            $filter['sortBy'] = $request->has('sort_by') ? $request->post('sort_by') : 'internal_tracking_id';
            $filter['sortOrder'] = $request->has('order') ? $request->post('order') : 'desc';

            $filter['time_period'] = $request->has('time_period') ? $request->post('time_period') : [];
            $filter['action_type'] = $request->has('action_type') ? $request->post('action_type') : '';
            $filter['currency_id'] = $request->has('currency_id') ? $request->post('currency_id') : '';
            $filter['search'] = $request->has('search') ? trim($request->post('search')) : '';
            $filter['round_id'] = $request->has('round_id') ? $request->post('round_id') : '';
            $filter['round_id_s'] = $request->has('round_id_s') ? $request->post('round_id_s') : '';
            $filter['id'] = $request->has('id') ? $request->post('id') : '';
            $filter['wallet_id'] = $request->has('wallet_id') ? $request->post('wallet_id') : '';
            $filter['player_id'] = $request->has('player_id') ? $request->post('player_id') : '';
            $filter['transaction_id'] = $request->has('transaction_id') ? $request->post('transaction_id') : '';
            $filter['amount'] = $request->has('amount') ? $request->post('amount') : '';
        
            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $filter['tenant_id'] = Auth::user()->tenant_id;
            } else {
                $filter['tenant_id'] = $request->has('tenant_id') ? $request->post('tenant_id') : '';
            }
            $urlSegment = $request->segment(2);
            if ($urlSegment === 'admin') {
                $filter['login_id'] = Auth::user()->id;
                $filter['login_type'] = ADMIN_TYPE;

                $loginAgentRole = getRoleByAdminId(Auth::user()->id);
                if($loginAgentRole && $loginAgentRole->admin_role_id==4){
                    $filter['action_type'] = 3;
                }
                if($loginAgentRole && $loginAgentRole->admin_role_id==5){
                    $filter['action_type'] = 4;
                }
            }
            $timeZone = $request->time_zone_name?? 'UTC +00:00';
            $time_period = $request['time_period'] ?? [];
            $time_type = $request['time_type'] ?? 'today';
            $filter['time_period'] = $time_period = $request['datetime'] ?? []; //a
            

            if($filter['time_period']) {
                    $time_period['fromdate'] = getTimeZoneWiseTimeISO($time_period['start_date']);
                    $time_period['enddate']  = getTimeZoneWiseTimeISO($time_period['end_date']);
                $filter['time_period'] = $time_period;
            }
            $record = TransactionsService::getListByES($limit, $page, $filter);
            if ($timeZone) {
                foreach ($record['hits']['hits'] as $task1 => $task) {
                    $task = $task['_source'];
                    if ($task["created_at"])
                        $record['hits']['hits'][$task1]['_source']["created_at"] = getTimeZoneWiseTime(@$task['created_at'], $timeZone);
                    else
                        $record['hits']['hits'][$task1]['_source']["created_at"] = @$task['created_at'];

                }
            }
            return returnResponse(true, "Record get Successfully", $record);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function downloadByES(Request $request)
    {

        setUnlimited();
        try {

            $page = 'all';
            $limit = 'all';
            $filter['sortBy'] = $request->has('sort_by') ? $request->post('sort_by') : 'internal_tracking_id';
            $filter['sortOrder'] = $request->has('order') ? $request->post('order') : 'desc';
            $filter['time_period'] = $request->has('time_period') ? $request->post('time_period') : [];
            $filter['action_type'] = $request->has('action_type') ? $request->post('action_type') : '';
            $filter['currency_id'] = $request->has('currency_id') ? $request->post('currency_id') : '';
            $filter['search'] = $request->has('search') ? $request->post('search') : '';
            $filter['player_id'] = $request->has('player_id') ? $request->post('player_id') : '';

            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {

                $filter['tenant_id'] = Auth::user()->tenant_id;

            } else {
                $filter['tenant_id'] = $request->has('tenant_id') ? $request->post('tenant_id') : '';
            }
            $urlSegment = $request->segment(2);
            if ($urlSegment === 'admin') {
                $filter['login_id'] = Auth::user()->id;
                $filter['login_type'] = ADMIN_TYPE;
            }

            $record = TransactionsService::getListByES($limit, $page, $filter);


            $columns = array(
                'ID',
                'Created at',
                'Round ID',
                'Transaction Id',
                'From wallet',
                'From currency',
                'From before balance',
                'From after balance',
                'To wallet',
                'To currency',
                'To before balance',
                'To after balance',
                'Amount',
                'Conversion rate',
                'Action Type',
                'Comments',
                'Action by',

            );

            $path = Storage::path('csv/');
            if (!is_dir($path)) {
                mkdir($path, 0775, true);
                chmod($path, 0775);
            }


            $fileName = time() . '.csv';
            $file = fopen($path . $fileName, 'w');
            fputcsv($file, $columns);


            foreach (@$record['hits']['hits'] as $row) {
                $row=(array)$row['_source'];
                
                if ($row["created_at"]!='')
                        $row["created_at"] = date('d-m-Y h:i:s', strtotime($row['created_at']));
                    else
                        $row["created_at"] = '-';


                fputcsv($file, array(

                        $row["internal_tracking_id"],
                        $row["created_at"],
                        $row["round_id"],
                        $row["transaction_id"],
                        isset($row["source_wallet_owner"]["email"]) ? $row["source_wallet_owner"]["email"] : "-",
                        $row["source_currency"],
                        isset($row["source_wallet_owner"]["before_balance"]) ? $row["source_wallet_owner"]["before_balance"] : "-",
                        isset($row["source_wallet_owner"]["after_balance"]) ? $row["source_wallet_owner"]["after_balance"] : "-",
                        isset($row["target_wallet_owner"]["email"]) ? $row["target_wallet_owner"]["email"] : "-",
                        $row["target_currency"],
                        isset($row["target_wallet_owner"]["before_balance"]) ? $row["target_wallet_owner"]["before_balance"] : "-",
                        isset($row["target_wallet_owner"]["after_balance"]) ? $row["target_wallet_owner"]["after_balance"] : "-",
                        $row["amount"],
                        $row["conversion_rate"],
                        $row["transaction_type"],
                        $row["description"],
                        $row["actionee"]["email"],

                    )
                );
            }
            fclose($file);
             // $result = ["url" => (request()->secure() ? secure_url(('storage/app/csv/' . $fileName)): url('storage/app/csv/' . $fileName))];
           $result = ["url" => secure_url('storage/app/csv/' . $fileName)];


            return returnResponse(true, "Record get Successfully", $result);
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @description download
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function betListDownload(Request $request)
    {
        setUnlimited();
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'sport_transactions';
                $params->action = 'export';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          

            $page = 'all';
            $limit = 'all';

            $filter['sortBy'] = $request->has('sortBy') ? $request->post('sortBy') : 'internal_tracking_id';
            $filter['sortOrder'] = $request->has('orderBy') ? $request->post('orderBy') : 'desc';

            $filter['time_period'] = $request->has('time_period') ? $request->post('time_period') : [];
            $filter['action_type'] = $request->has('action_type') ? $request->post('action_type') : '';
            $filter['currency_id'] = $request->has('currency_id') ? $request->post('currency_id') : '';
            $filter['searchKeyword'] = $request->has('search') ? trim($request->post('search')) : '';
            $filter['emailsearch'] = $request->has('emailsearch') ? trim($request->post('emailsearch')) : '';
            $filter['wallet_id'] = $request->has('wallet_id') ? $request->post('wallet_id') : '';

            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {

                $filter['tenant_id'] = Auth::user()->tenant_id;

            } else {
                $filter['tenant_id'] = $request->has('tenant_id') ? $request->post('tenant_id') : '';
            }
            $urlSegment = $request->segment(2);
            if ($urlSegment === 'admin') {
                $filter['login_id'] = Auth::user()->id;
                $filter['login_type'] = ADMIN_TYPE;
            }

            $timeZone = $filter['time_zone_name'] = $request->time_zone_name?? 'UTC +00:00';
            $filter['time_period'] = $time_period = $request['datetime'] ?? [];
            $filter['time_type'] = $request['time_type'] ?? 'today';


            if($filter['time_period']) {
                $time_period['fromdate'] = getTimeZoneWiseTimeISO($time_period['start_date']);
                $time_period['enddate']  = getTimeZoneWiseTimeISO($time_period['end_date']);
                $filter['time_period'] = $time_period;
            }
            $record = SportsService::getBetListByES($limit, $page, $filter);


            $columns = array(
                'ID',
                'Created at',
                'Bet Ids',
                'journal_entry',
                'From wallet',
                'From currency',
                'To wallet',
                'To currency',
                'Amount',
                'Conversion rate',
                'Action Type',
                'Comments',
                'Action by',

            );

            $path = Storage::path('csv/');
            if (!is_dir($path)) {
                mkdir($path, 0775, true);
                chmod($path, 0775);
            }


            $fileName = time() . '.csv';
            $file = fopen($path . $fileName, 'w');
            fputcsv($file, $columns);

            foreach (@$record['hits']['hits'] as $row) {
                $row=(array)$row['_source'];

                $strBetIts = "";
                $strBetItsArray = [];

                if(isset($row['betslip_details']['bets'])) {
                    foreach ($row['betslip_details']['bets'] as $key => $value) {
                        $strBetItsArray [] = $value['bet_id'];
                    }
                }
                if(count($strBetItsArray)){
                    $strBetIts = "'";
                    $strBetIts .= implode(',',$strBetItsArray);
                }
                if ($timeZone) { 
                    if ($row["created_at"])
                        $row["created_at"] = getTimeZoneWiseTime($row['created_at'], $timeZone);
                    else
                        $row["created_at"] = $row['creation_date'];
                } else {
                    $row["creation_date"] = $row['creation_date'];
                } 

               /* if ($row["created_at"]!='')
                    $row["created_at"] = date('d-m-Y h:i:s', strtotime($row['created_at']));
                else
                    $row["created_at"] = '-';*/


                $source_name = '';

                if ($row["source_wallet"])
                    $source_name = @$row["source_wallet"]['email'] ." ".@$row["source_wallet"]['first_name'] ." ".@$row["source_wallet"]['last_name'];
                else
                    $source_name = '-';

                $target_name = '';
                if (@$row["target_wallet"])
                    $target_name = @$row["target_wallet"]['email'] ." ".@$row["target_wallet"]['first_name'] ." ".@$row["target_wallet"]['last_name'];
                else
                    $target_name = '-';


                $actionee_name = '';
                if ($row["player_details"])
                    $actionee_name = @$row["player_details"]['email'] ." ".@$row["player_details"]['first_name'] ." ".@$row["player_details"]['last_name'];
                else
                    $actionee_name = '-';


                fputcsv($file, array(

                        $row["internal_tracking_id"],
                        $row["created_at"],
                        $strBetIts,
                        $row["journal_entry"],
                        $source_name,
                        $row["source_currency"],
                        $target_name,
                        $row["target_currency"],
                        ((float)$row["amount"]+(float)$row["non_cash_amount"]),
                        $row["conversion_rate"],
                        $row["transaction_type"],
                        $row["description"],
                        $actionee_name,

                    )
                );
            }
            fclose($file);
            $result = ["url" => secure_url('storage/app/csv/' . $fileName)];


            return returnResponse(true, "Record get Successfully", $result);
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }
        
}
