<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Bet;
use App\Models\BetTransaction;
use App\Models\Event;
use App\Models\League;
use App\Models\Location;
use App\Models\Market;
use App\Models\Sport;
use App\Models\TenantBlockedEvent;
use App\Models\TenantBlockedLeague;
use App\Models\TenantBlockedSports;
use App\Models\User;
use App\Services\SportsService as SportsService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use App\Services\BetSportsReportService;
use Validator;

class SportsController extends Controller
{
    /**
     * @description getBetReports Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBetReportsByES(Request $request)
    {
        
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'bet_report';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          

            setUnlimited();
            $page = $request->has('page') ? $request->post('page') : 1;
            $limit = $request->has('size') ? $request->post('size') : 10;
            $filter = $request->all();
            $filter['sortBy'] = $request->has('sort_by') ? $request->post('sort_by') : 'internal_tracking_id';
            $filter['sortOrder'] = $request->has('order') ? $request->post('order') : 'desc';
            
            $filter['time_period'] = $request->has('time_period') ? $request->post('time_period') : [];
            $filter['action_type'] = $request->has('action_type') ? $request->post('action_type') : '';
            $filter['currency_id'] = $request->has('currency_id') ? $request->post('currency_id') : '';
            $filter['searchKeyword'] = $request->has('search') ? trim($request->post('search')) : '';
            $filter['wallet_id'] = $request->has('wallet_id') ? $request->post('wallet_id') : '';

            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $filter['tenant_id'] = Auth::user()->tenant_id;
            } else {
                $filter['tenant_id'] = $request->has('tenant_id') ? $request->post('tenant_id') : '';
            }
            /*$urlSegment = $request->segment(2);
            if ($urlSegment === 'admin') {
                $filter['login_id'] = Auth::user()->id;
                $filter['login_type'] = ADMIN_TYPE;
            }*/
            $timeZone = $request->time_zone_name ?? 'UTC +00:00';
            $time_period = $request['time_period'] ?? [];
            $time_type = $request['time_type'] ?? 'custom';

            if (@$filter['start_date'] && @$filter['end_date']) {
                $filter['start_date'] = convertToUTC($filter['start_date'] .':00', $timeZone)->format('Y-m-d H:i:s.v');
                $filter['end_date'] = convertToUTC($filter['end_date'].':59', $timeZone)->format('Y-m-d H:i:s.v');

            }
            $query = BetSportsReportService::getBetReportByES($limit,$page,$filter);
            
            /* $totalStakeSumArray =[];
            $totalPossibleWinSumArray =[];
            $totalSake = $query->get()->toArray();
            if(count($totalSake)>0) {
                foreach ($totalSake as $key => $value) {
                    $totalStakeSumArray[] = $value['betslip']['stake'];
                    $totalPossibleWinSumArray[] = $value['betslip']['possible_win_amount'];
                }
                
            }*/

            
            /* if($request->sort_by == 'created_at') {
                $bet_transactions = $query->orderBy($request->sort_by ?? 'created_at', $request->order ?? 'asc')->paginate($request->size ? $request->size : 10);
            } else {
                $bet_transactions = $query->paginate($request->size ?? 10);
            }

            if ($bet_transactions)
            $bet_transactions = $bet_transactions->toArray();*/

            if ($timeZone) {
                foreach ($query['hits']['hits'] as $task1 => $task) {
                    $task = $task['_source'];
                    if ($task["created_at"])
                        $query['hits']['hits'][$task1]['_source']["created_at"] = getTimeZoneWiseTime($task['created_at'], $timeZone);
                }
            }
            $data['data'] = $query['hits']['hits'];

            $data['total'] = $query['hits']['total']['value'];
            $data['stake_sum'] = 0;//array_sum($totalStakeSumArray);
            $data['possible_win_sum'] =0;// array_sum($totalPossibleWinSumArray);
//            unset($bet_transactions);


            return returnResponse(true, "Record get Successfully", $data);

        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }


    /**
     * @description getBetReports Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBetReports(Request $request)
    {

        try {
            ini_set('max_execution_time', 0);
            ini_set("memory_limit", "-1");
            $query = $this->betReportsQuery($request->all());

            $totalStakeSumArray =[];
            $totalPossibleWinSumArray =[];
            $totalSake = $query->get()->toArray();
            if(count($totalSake)>0) {
                foreach ($totalSake as $key => $value) {
                    $totalStakeSumArray[] = $value['betslip']['stake'];
                    $totalPossibleWinSumArray[] = $value['betslip']['possible_win_amount'];
                }

            }


            if($request->sort_by == 'created_at') {
                $bet_transactions = $query->orderBy($request->sort_by ?? 'created_at', $request->order ?? 'asc')->paginate($request->size ? $request->size : 10);
            } else {
                $bet_transactions = $query->paginate($request->size ?? 10);
            }

            if ($bet_transactions)
                $bet_transactions = $bet_transactions->toArray();

            $data['data'] = (array)$bet_transactions['data'];
            $data['total'] = (int)$bet_transactions['total'];
            $data['stake_sum'] = array_sum($totalStakeSumArray);
            $data['possible_win_sum'] = array_sum($totalPossibleWinSumArray);
             unset($bet_transactions);


            return returnResponse(true, "Record get Successfully", $data);

        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @description getAllBetReports
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllBetReports(Request $request)
    {

        try {
            // ================= permissions starts here =====================
            if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'bet_report';
                $params->action = 'export';
                $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;

                $permission = checkPermissions($params);   
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================   
            setUnlimited();
            $page = $request->has('page') ? $request->post('page') : 1;
            $limit = $request->has('size') ? $request->post('size') : 10;
            $filter = $request->all();
            $filter['sortBy'] = $request->has('sort_by') ? $request->post('sort_by') : 'internal_tracking_id';
            $filter['sortOrder'] = $request->has('order') ? $request->post('order') : 'desc';

            $filter['time_period'] = $request->has('time_period') ? $request->post('time_period') : [];
            $filter['action_type'] = $request->has('action_type') ? $request->post('action_type') : '';
            $filter['currency_id'] = $request->has('currency_id') ? $request->post('currency_id') : '';
            $filter['searchKeyword'] = $request->has('search') ? trim($request->post('search')) : '';
            $filter['wallet_id'] = $request->has('wallet_id') ? $request->post('wallet_id') : '';

            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $filter['tenant_id'] = Auth::user()->tenant_id;
            } else {
                $filter['tenant_id'] = $request->has('tenant_id') ? $request->post('tenant_id') : '';
            }

            $timeZone = $request->time_zone_name ?? 'UTC +00:00';
            $time_period = $request['time_period'] ?? [];
            $time_type = $request['time_type'] ?? 'today';

            if ($time_period) {
                if ($time_type != 'custom') {
                    $time_period = dateConversionWithTimeZone($time_type);
                    $time_period['fromdate'] = getTimeZoneWiseTimeISO(getTimeZoneWiseTime($time_period['fromdate'], $timeZone));
                    $time_period['enddate'] = getTimeZoneWiseTimeISO(getTimeZoneWiseTime($time_period['enddate'], $timeZone));
                } else {
                    $time_period['fromdate'] = getTimeZoneWiseTimeISO(getTimeZoneWiseTime($time_period['start_date'], $timeZone));
                    $time_period['enddate'] = getTimeZoneWiseTimeISO(getTimeZoneWiseTime($time_period['end_date'], $timeZone));
                    unset($time_period['start_date'], $time_period['end_date']);
                }
                $filter['time_period'] = $time_period;
            }
            $bet_transactions = BetSportsReportService::getBetReportByES('all', $page, $filter);
            $bet_transactions = $bet_transactions['hits']['hits'];

            if ($request->bet_type == 'not_combo') {

                $columns = array(
                    'Bet ID',
                    'Created At',
                    'Email',
                    'Sport',
                    'Tournament',
                    'Match',
                    'Market name',
                    'Schedule at',
                    'Currency',
                    'Stake',
                    'Odds',
                    'Posible Win Amount',
                    'Status'
                );

            } else {

                $columns = array(
                    'Bet ID',
                    'Created At',
                    'Email',
                    'Currency',
                    'Stake',
                    'Overall Odds',
                    'Status',
                    'Posible Win Amount'
                );

            }

            $path = Storage::path('csv/');

            if (!is_dir($path)) {
                mkdir($path, 0775, true);
                chmod($path, 0775);
            }

            $fileName = time() . '.csv';
            $file = fopen($path . $fileName, 'w');
            fputcsv($file, $columns);

            foreach ($bet_transactions as $bet_transaction) {

                if ($request->bet_type == 'not_combo') {
                    if(!empty($bet_transaction['_source']['betslip_details'])){
                        $row['id'] = "`".$bet_transaction['_source']['internal_tracking_id']?? '-';
                       
                        if(!empty($bet_transaction['_source']['betslip_details']['bets'][0])){
                            $row['sport'] = $bet_transaction['_source']['betslip_details']['bets'][0]['event']['sport_name_en'] ?? '-';
                            $row['tournament'] = $bet_transaction['_source']['betslip_details']['bets'][0]['event']['league_name_en'] ?? '-';
                            $row['match'] = $bet_transaction['_source']['betslip_details']['bets'][0]['match'] ?? '-';
                            $row['market'] = $bet_transaction['_source']['betslip_details']['bets'][0]['market'] ?? '-';
                            if(array_key_exists("start_date",$bet_transaction['_source']['betslip_details']['bets'][0])){
                                if($bet_transaction['_source']['betslip_details']['bets'][0]['start_date']!=''){
                                    if ($timeZone) {
                                        $scheduleDate = Carbon::parse(   getTimeZoneWiseTime($bet_transaction['_source']['betslip_details']['bets'][0]['start_date'], $timeZone))->format('Y-m-d H:m') ?? '-';
                                    }else{
                                        $scheduleDate = Carbon::parse($bet_transaction['_source']['betslip_details']['bets'][0]['start_date'])->format('Y-m-d H:m') ?? '-';
                                    }
                                }else{
                                    $scheduleDate = '-';
                                }
                            }
                        }else{
                            $row['sport'] =  '-';
                            $row['tournament'] =  '-';
                            $row['match'] = '-';
                            $row['market'] = '-';
                        }
                        
                        $row['schedule_at'] = $scheduleDate;
                        $row['stake'] = $bet_transaction['_source']['betslip_details']['stake'] ?? '-';
                        $row['odds'] = $bet_transaction['_source']['betslip_details']['multi_price']  ?? '-';
                        $row['possible_win_amount'] = $bet_transaction['_source']['betslip_details']['possible_win_amount'] ?? '-';
                        $row['status'] = $bet_transaction['_source']['betslip_details']['settlement_status'] ?? '-';
                    }else{
                        $row['id'] =  '-';
                        $row['sport'] =  '-';
                        $row['tournament'] =  '-';
                        $row['match'] = '-';
                        $row['market'] = '-';
                        $row['schedule_at'] = '-';
                        $row['stake'] = '-';
                        $row['odds'] = '-';
                        $row['status'] = '-';
                        $row['possible_win_amount'] = '-';
                        $row['status'] = '-';
                    }

                    if ($timeZone) {
                        $row['created'] = Carbon::parse(getTimeZoneWiseTime($bet_transaction['_source']['created_at'], $timeZone))->format('Y-m-d H:m') ?? '-';
                    }else{
                        $row['created'] = Carbon::parse($bet_transaction['_source']['created_at'])->format('Y-m-d H:m') ?? '-';
                    }
                    $row['email'] = $bet_transaction['_source']['player_details']['email'] ?? '-';
                    $row['currency'] = $bet_transaction['_source']['player_details']['currency'] ?? '-';
                    
                   

                    fputcsv($file, array(
                            $row['id'],
                            $row['created'],
                            $row['email'],
                            $row['sport'],
                            $row['tournament'],
                            $row['match'],
                            $row['market'],
                            $row['schedule_at'],
                            $row['currency'],
                            $row['stake'],
                            $row['odds'],
                            $row['possible_win_amount'],
                            $row['status'],
                        )
                    );

                } else {
                   if(!empty($bet_transaction['_source']['betslip_details'])){
                    $row['id'] =  "`".$bet_transaction['_source']['internal_tracking_id'] ?? '-';
                    $row['stake'] = $bet_transaction['_source']['betslip_details']['stake'] ?? '-';
                    $row['overall_odds'] = $bet_transaction['_source']['betslip_details']['multi_price'] ?? '-';
                    $row['status'] = $bet_transaction['_source']['betslip_details']['settlement_status'] ?? '-';
                    $row['possible_win_amount'] = $bet_transaction['_source']['betslip_details']['possible_win_amount'] ?? '-';
                   }else{
                    $row['id'] =   '-';
                    $row['stake'] =  '-';
                    $row['overall_odds'] =  '-';
                    $row['status'] =  '';
                    $row['possible_win_amount'] =  '-';
                   }

                    if ($timeZone) {
                        $row['created'] = Carbon::parse(getTimeZoneWiseTime($bet_transaction['_source']['created_at'], $timeZone))->format('Y-m-d H:m') ?? '-';
                    } else{
                        $row['created'] = Carbon::parse($bet_transaction['_source']['created_at'])->format('Y-m-d H:m') ?? '-';
                    }

                    $row['email'] = $bet_transaction['_source']['player_details']['email'] ?? '-';
                    $row['currency'] = $bet_transaction['_source']['player_details']['currency'] ?? '-';
                    //$row['bonus'] = $bet_transaction['_source']['deposit_bonus_details'] ?? '';
                 

                    fputcsv($file, array(
                            $row['id'],
                            $row['created'],
                            $row['email'],
                            $row['currency'],
                            $row['stake'],
                            $row['overall_odds'],
                            $row['status'],
                            $row['possible_win_amount']
                        )
                    );

                }

            }

            fclose($file);
          

            return returnResponse(true, "Record get Successfully", ["url" => secure_url('storage/app/csv/' . $fileName)]);

        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @description betReportsQuery
     * @param $request
     * @return BetTransaction
     */
    private function betReportsQuery($request)
    {
        $query = new BetTransaction;

        if (!empty($request['user_id'])) {

            $request['tenant_id'] = User::find($request['user_id'])->select(['tenant_id'])->first()->tenant_id;
            $query = $query->WhereRaw("bets_transactions.user_id = " . $request['user_id']);
        }

        if (!empty($request['agent_id'])) {
            $Hierarchy = getAdminHierarchy($request['agent_id'],false);
            $ids = flatten($Hierarchy);
            if($ids) {
                $whereStrParentSQL = implode(',', $ids);
                $sql = "select id from users where parent_type = 'AdminUser' and parent_id in ($whereStrParentSQL)";
                $userParentsData = DB::select($sql);
                $userIdArray = [];

                foreach ($userParentsData as $key =>$value){
                    $userIdArray[]=$value->id;
                }

                if(count($userIdArray)){
                    $query = $query->WhereRaw("bets_transactions.user_id in( " . implode(',', $userIdArray) . ")");
                }else{
                    $query = $query->WhereRaw("bets_transactions.user_id = null");
                }
            }else{
                $query = $query->WhereRaw("bets_transactions.user_id =null");
            }
        }

        if (Auth::User()->parent_type == "SuperAdminUser" || Auth::User()->parent_type == "Manager") {
            if (!empty($request['tenant_id'])) {
                $query = $query->WhereRaw("bets_transactions.tenant_id = " . $request['tenant_id']);
            }
        } else {
            $query = $query->WhereRaw("bets_transactions.tenant_id = " . Auth::User()->tenant_id);
        }


        if (!empty($request['bet_type'])) {
            $query = $query->whereHas('betslip', function ($q) use ($request) {
                $q->where('bettype', $request['bet_type'] == 'not_combo' ? 1 : 2);
            });
            $query = $query->with(['betslip.bets', 'betslip.bets.event.sport', 'betslip.bets.event.league']);
        }

        if (!empty($request['sport_id'])) {
            if ($request['bet_type'] == 'not_combo') {
                $query = $query->whereHas('betslip.bet.event.sport', function ($q) use ($request) {
                    $q->where('id', $request['sport_id']);
                });
            } else {
                $query = $query->whereHas('betslip.bets.event.sport', function ($q) use ($request) {
                    $q->where('id', $request['sport_id']);
                });
            }
        }

        if (!empty($request['country'])) {
            if ($request['bet_type'] == 'not_combo') {
                $query = $query->whereHas('betslip.bet.event.country', function ($q) use ($request) {
                    $q->where('id', $request['country']);
                });
            } else {
                $query = $query->whereHas('betslip.bets.event.country', function ($q) use ($request) {
                    $q->where('id', $request['country']);
                });
            }
        }

        if (!empty($request['tournament_id'])) {
            $query = $query->whereHas('betslip.bet.event.league', function ($q) use ($request) {
                $q->where('id', $request['tournament_id']);
            });
        }

        if (!empty($request['match_id'])) {
            if ($request['bet_type'] == 'not_combo') {
                $query = $query->whereHas('betslip.bet.event', function ($q) use ($request) {
                    $q->where('id', $request['match_id']);
                });
            } else {
                $query = $query->whereHas('betslip.bets.event', function ($q) use ($request) {
                    $q->where('id', $request['match_id']);
                });
            }
        }

        if (!empty($request['status'])) {
            if ($request['status'] == "-2") {
                $query = $query->whereHas('betslip.bets', function ($q) use ($request) {
                    $q->whereNull('settlement_status');
                });
            } else {
                $query = $query->whereHas('betslip.bets', function ($q) use ($request) {
                    $q->where('settlement_status', $request['status']);
                });
            }
        }

        if (!empty($request['stake_min_amt'])) {
            $query = $query->whereHas('betslip', function ($q) use ($request) {
                $q->where('stake', '>=', $request['stake_min_amt']);
            });
        }

        if (!empty($request['stake_max_amt'])) {
            $query = $query->whereHas('betslip', function ($q) use ($request) {
                $q->where('stake', '<=', $request['stake_max_amt']);
            });
        }

        if (!empty($request['start_date']) && !empty($request['end_date'])) {
            if ($request['bet_type'] == 'not_combo') {
                $query = $query->whereHas('betslip.bet', function ($q) use ($request) {
                    $q->whereBetween('start_date', [$request['start_date'], $request['end_date']]);
                });
            } else {
                $query = $query->whereHas('betslip.bets', function ($q) use ($request) {
                    $q->whereBetween('start_date', [$request['start_date'], $request['end_date']]);
                });
            }
        }


        if (!empty($request['winning_min_amt'])) {
            $query = $query->whereHas('betslip', function ($q) use ($request) {
                $q->where('possible_win_amount', '>=', $request['winning_min_amt']);
            });
        }

        if (!empty($request['winning_max_amt'])) {
            $query = $query->whereHas('betslip', function ($q) use ($request) {
                $q->where('possible_win_amount', '<=', $request['winning_max_amt']);
            });
        }

        if (!empty($request['search'])) {
            $query = $query->whereHas('betslip.bets', function ($q) use ($request) {
                $q->where(DB::raw('lower(bet_id)'), 'like', '%' . strtolower($request['search']) . '%');
            });
        }

        $query = $query->with(['user.wallet.currency', 'betslip.bet.event.league', 'betslip.bet.event.sport']);

        if ($request['sort_by'] == 'id') {
            $query = $query->join('bets_betslip', 'bets_betslip.id', '=', 'betslip_id')
                ->join('bets_bets', 'bets_betslip.id', '=', 'bets_bets.betslip_id')
                ->orderBy('bets_bets.bet_id', $request['order']);
        }

        if ($request['sort_by'] == 'win_amount') {
            $query = $query->join('bets_betslip', 'bets_betslip.id', '=', 'betslip_id')
                ->orderBy('bets_betslip.possible_win_amount', $request['order']);
        }

        if ($request['sort_by'] == 'sport') {
            $query = $query->join('bets_betslip', 'bets_betslip.id', '=', 'betslip_id')
                ->join('bets_bets', 'bets_betslip.id', '=', 'bets_bets.betslip_id')
                ->join('pulls_events', 'bets_bets.event_id', '=', 'pulls_events.id')
                ->join('pulls_sports', 'pulls_sports.id', '=', 'pulls_events.sport_id')
                ->orderBy('pulls_sports.name_en', $request['order']);
        }

        if ($request['sort_by'] == 'league') {
            $query = $query->join('bets_betslip', 'bets_betslip.id', '=', 'betslip_id')
                ->join('bets_bets', 'bets_betslip.id', '=', 'bets_bets.betslip_id')
                ->join('pulls_events', 'bets_bets.event_id', '=', 'pulls_events.id')
                ->join('pulls_leagues', 'pulls_leagues.id', '=', 'pulls_events.league_id')
                ->orderBy('pulls_leagues.name_en', $request['order']);
        }

        if ($request['sort_by'] == 'match') {
            $query = $query->join('bets_betslip', 'bets_betslip.id', '=', 'betslip_id')
                ->join('bets_bets', 'bets_betslip.id', '=', 'bets_bets.betslip_id')
                ->orderBy('bets_bets.match', $request['order']);
        }

        if ($request['sort_by'] == 'market') {
            $query = $query->join('bets_betslip', 'bets_betslip.id', '=', 'betslip_id')
                ->join('bets_bets', 'bets_betslip.id', '=', 'bets_bets.betslip_id')
                ->orderBy('bets_bets.market', $request['order']);
        }

        if ($request['sort_by'] == 'outcome') {
            $query = $query->join('bets_betslip', 'bets_betslip.id', '=', 'betslip_id')
                ->join('bets_bets', 'bets_betslip.id', '=', 'bets_bets.betslip_id')
                ->orderBy('bets_bets.name', $request['order']);
        }

        if ($request['sort_by'] == 'event_date') {
            $query = $query->join('bets_betslip', 'bets_betslip.id', '=', 'betslip_id')
                ->join('bets_bets', 'bets_betslip.id', '=', 'bets_bets.betslip_id')
                ->orderBy('bets_bets.start_date', $request['order']);
        }

        if ($request['sort_by'] == 'odds') {
            $query = $query->join('bets_betslip', 'bets_betslip.id', '=', 'betslip_id')
                ->join('bets_bets', 'bets_betslip.id', '=', 'bets_bets.betslip_id')
                ->orderBy('bets_bets.price', $request['order']);
        }

        if ($request['sort_by'] == 'overall_odds') {
            $query = $query->join('bets_betslip', 'bets_betslip.id', '=', 'betslip_id')
                ->orderBy('bets_betslip.multi_price', $request['order']);
        }

        if ($request['sort_by'] == 'status') {
            $query = $query->join('bets_betslip', 'bets_betslip.id', '=', 'betslip_id')
                ->join('bets_bets', 'bets_betslip.id', '=', 'bets_bets.betslip_id')
                ->orderBy('bets_bets.settlement_status', $request['order']);
        }

        if ($request['sort_by'] == 'stake') {
            $query = $query->join('bets_betslip', 'bets_betslip.id', '=', 'betslip_id')
                ->orderBy('bets_betslip.stake', $request['order']);
        }

        if ($request['sort_by'] == 'email') {
            $query = $query->join('users', 'bets_transactions.user_id', '=', 'users.id')
                ->orderBy('users.email', $request['order']);
        }

        if ($request['sort_by'] == 'currency_code') {
            $query = $query->join('users', 'users.id', '=', 'user_id')
                ->join('wallets', 'wallets.owner_id', '=', 'users.id')
                ->join('currencies', 'wallets.currency_id', '=', 'currencies.id')
                ->orderBy('currencies.code', $request['order']);
        }

        return $query;
    }

    /**
     * @description allSports
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function allSports(Request $request)
    {
        try {
               // ================= permissions starts here =====================
            //    if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            //     $params = new \stdClass();
            //     $params->module = 'sports_book';
            //     $params->action = 'R';
            //     $params->admin_user_id = Auth::user()->id;
        
            //     $permission = checkPermissions($params);   
            //     if(!$permission){ 
            //         return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            //     }
            //     }
                // ================= permissions ends here =====================
            $tenant_id = '';
            if (Auth::User()->parent_type == 'AdminUser' && Auth::User()->tenant_id) {
                $tenant_id = Auth::User()->tenant_id;
            } else {
                $tenant_id = $request->has('tenant_id') ? $request->post('tenant_id') : '';
            }
           
            if(!empty($tenant_id)){
                $sports = Sport::select('id','name_en')->where('tenant_id',$tenant_id)->orderBy('name_en', 'ASC')->get();    
            }else{
                $sports = Sport::select('id','name_en')->distinct('name_en')->orderBy('name_en', 'ASC')->get();
            }
            
            return returnResponse(true, "Record get Successfully", $sports);

        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @description sports
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sports(Request $request)
    {
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'sports_book';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            // $filter=[
            //     'size'=>$request->size ? $request->size : 10,
            //     'page'=>$request->page ? $request->page : 1
            // ];
            
            // if (!empty($request->search)) {
            //     $filter['search'] = strtolower($request->search);
            // }

            $tenant_id='';
            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $tenant_id = Auth::user()->tenant_id;
            } else {
                $tenant_id = $request->has('tenant_id') ? $request->post('tenant_id') : '';
            }
            // $sports=SportsService::getSportsData($tenant_id, $filter);
            $sports['result'] = Sport::where(function ($query) use ($request) {
                $query->where('name_en', 'ilike', '%' . $request->search . '%');
                $query->orwhere('name_de', 'ilike', '%' . $request->search . '%');
                $query->orwhere('name_fr', 'ilike', '%' . $request->search . '%');
            });

            if ($request->enabled != '') {
                $filter['enabled'] = $request->enabled === 'enable' ? true : false;
                
                $sports['result'] = $sports['result']->where('is_deleted', $filter['enabled']);
            }

            if ($tenant_id != '') {
                $sports['result'] = $sports['result']->where('tenant_id', $tenant_id);
            }
            $sports['count'] = $sports['result']->count();
            $sports['result'] = $sports['result']->paginate($request->size ?? 10, ['*'], 'Page', $request->page ?? 1);

            $data['data'] = $sports['result'];
            $data['total'] = $sports['count'];
            // unset($sports);
            return returnResponse(true, "Record get Successfully", $data);

        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @description sportsMainList
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sportsMainList(Request $request){
        try {
            $query = new Sport;
            if (!empty($request->search)) {
                $query = $query->where(DB::raw('lower(name_en)'), 'like', '%' . strtolower($request->search) . '%');
            }

            if ($request->enabled == 'enable') {
                $query = $query->where('is_deleted', true);
            }

            if ($request->enabled == 'disable') {
                $query = $query->where('is_deleted', false);
            }

            $query = $query->whereIn('sport_id', explode(",",SPORT_ENABLED_ID));

            $sports = $query->orderBy('updated_at', 'ASC')->paginate($request->size ? $request->size : 10);

            if($sports)
                $sports=$sports->toArray();

            $data['data']=(array)$sports['data'];
            $data['total']=(int)$sports['total'];
            unset($sports);
            return returnResponse(true, "Record get Successfully", $data);

        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @description updateSportEnabled
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateSportEnabled(Request $request)
    {
        try {
         
            $sport = TenantBlockedSports::select('id')->where('pulls_sport_id',$request->id)->get();
            
            if (empty($sport)) {
               
                $insertData = array('tenant_id' => Auth::user()->tenant_id,
                                    'admin_user_id' => Auth::user()->id,
                                    'pulls_sport_id' => $request->id,
                                    'is_deleted' => true
                                );
                if($request->status == 'disable'){
                    TenantBlockedSports::create($insertData);
                    return returnResponse(false, SPORT_DISABLED_SUCCESSFULLY, [], Response::HTTP_OK, true);
                }else{
                    TenantBlockedSports::where('pulls_sport_id', $request->id)
                                    ->where('tenant_id' , Auth::user()->tenant_id)
                                    ->delete();
                    return returnResponse(true, SPORT_ENABLED_SUCCESSFULLY, [], Response::HTTP_OK, true);
                }
                
            }else{

                if($request->id && @$request->status=='disable') {
                    TenantBlockedSports::create(
                        [
                            'pulls_sport_id' => $request->id,
                            'admin_user_id' => @Auth::user()->id,
                            'tenant_id' => @Auth::user()->tenant_id,
                            'is_deleted' => true,
                        ]
                    );
                    return returnResponse(true, SPORT_DISABLED_SUCCESSFULLY, [], Response::HTTP_OK, true);
                }else{
                    TenantBlockedSports::where('pulls_sport_id', $request->id)
                        ->where('tenant_id' , Auth::user()->tenant_id)
                        ->delete();
                    return returnResponse(true, SPORT_ENABLED_SUCCESSFULLY, [], Response::HTTP_OK, true);
                }

            }


            
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }


    /**
     * @description allCountries
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function allCountries(Request $request)
    {
        try {
              // ================= permissions starts here =====================
            //   if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            //     $params = new \stdClass();
            //     $params->module = 'sports_book';
            //     $params->action = 'R';
            //     $params->admin_user_id = Auth::user()->id;
        
            //     $permission = checkPermissions($params);   
            //     if(!$permission){ 
            //         return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            //     }
            //     }
                // ================= permissions ends here =====================       
                
            $locations = Location::orderBy('name_en', 'ASC')->select(['id', 'name_en', 'location_id']);
            if (!empty($request->location_ids)) {
                $locations = $locations->whereIn('location_id', explode(',', $request->location_ids));
            }

            $locations = $locations->get();

            return returnResponse(true, "Record get Successfully", $locations);

        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @description countries
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function countries(Request $request)
    {
        try {

            $query = new Location;

            if (!empty($request->location_ids)) {
                $query = $query->whereIn('location_id', explode(',', $request->location_ids));
            }

            if (!empty($request->location_id)) {
                $query = $query->where('location_id', $request->location_id);
            }

            $locations = $query->orderBy('updated_at', 'ASC')->paginate($request->size ? $request->size : 10);

            if ($locations)
                $locations = $locations->toArray();


            $data['data'] = (array)$locations['data'];
            $data['total'] = (int)$locations['total'];
            unset($locations);
            return returnResponse(true, "Record get Successfully", $data);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function allLeaguesSearch(Request $request){
        try {
            $tenant_id = '';

            $authUser = Auth::User();
            
            if ($authUser->parent_type == 'AdminUser' && $authUser->tenant_id) {
                $tenant_id = $authUser->tenant_id;
            } else {
                $tenant_id = $request->has('tenant_id') ? $request->post('tenant_id') : '';
            }

            $leagues = League::orderBy('name_en', 'ASC')->where(DB::raw('lower(name_en)'), 'like', '%' . strtolower($request->search) . '%')->whereDoesntHave('blocked', function($q) use($tenant_id) {
                $q->where('tenant_id', $tenant_id);
            })->select(['id', 'name_en AS text'])->get();

            return returnResponse(true, "Record get Successfully", $leagues);

        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @description leagues
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function leagues(Request $request)
    {
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                    $params = new \stdClass();
                    $params->module = 'sports_book';
                    $params->action = 'R';
                    $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);   
                    if(!$permission){ 
                        return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                    }
                    }
                    // ================= permissions ends here =====================    

            $query = new League;

            $tenant_id = '';
            
            if (Auth::User()->parent_type == 'AdminUser' && Auth::User()->tenant_id) {
                $tenant_id = Auth::User()->tenant_id;
            } else {
                $tenant_id = $request->has('tenant_id') ? $request->post('tenant_id') : '';
            }
            // $tenant_str = '';

            // if($tenant_id != '') {
            //     $tenant_str = " and tbs.tenant_id = $tenant_id";
            // }

            // $query->getRawOriginal('tbl.id as disabled');

            // $query->joiningTable(DB::raw('tenant_blocked_leagues tbl on (tbl.pulls_league_id = pulls_leagues.id '.$tenant_str));
//            leftJoin('posts', 'posts.author_id', '=', 'authors.id')

            if (!empty($request->search)) {
                $query = $query->where(DB::raw('lower(name_en)'), 'like', '%' . strtolower($request->search) . '%');
            }

            if ($request->enabled != '') {
                
                if ($request->enabled == 'enable') {
                // $query = $query->where('is_deleted', true);
                    $query = $query->doesnthave('blocked');
                } else if ($request->enabled == 'disable') {
                    // $query = $query->where('is_deleted', false);
                    $query = $query->has('blocked');
                }
                
            }

            if (!empty($request->country)) {
                $query = $query->whereHas('country', function ($q) use ($request) {
                    $q->where('id', $request->country);
                });
            }

            if (!empty($tenant_id)) {
                $query = $query->where('tenant_id',$tenant_id);
            }
            
            if (!empty($request->sport_id)) {
                $query = $query->whereHas('sport', function ($q) use ($request) {
                    $q->where('id', $request->sport_id);
                });
            }

            $query = $query->withCount('events')->with(['country', 'sport', 'blocked' => function($q) use($tenant_id) {
                $q->where('tenant_id', $tenant_id);
            }]);

            $leagues = $query->orderBy('updated_at', 'ASC')->paginate($request->size ? $request->size : 10);

            if ($leagues)
                $leagues = $leagues->toArray();

            $data['data'] = (array)$leagues['data'];
            $data['total'] = (int)$leagues['total'];
            unset($leagues);
            return returnResponse(true, "Record get Successfully", $data);
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @description updateLeagueDisabled
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateLeagueDisabled(Request $request) {
        try {

            $tenantBlockedLeague = TenantBlockedLeague::create([
                'tenant_id' => Auth::User()->tenant_id,
                'admin_user_id' => Auth::User()->id,
                'pulls_league_id' => $request->id
            ]);

            return returnResponse(true, 'Tournament Disabled Successfully.', $tenantBlockedLeague ,Response::HTTP_OK, true);

        }
        catch(\Exception $e) {
            return returnResponse(false, $e->getMessage(), [] ,Response::HTTP_EXPECTATION_FAILED, true);
        }
    }

    /**
     * @description updateLeagueEnabled
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateLeagueEnabled(Request $request) {
        try {
            $blockLeague = TenantBlockedLeague::find($request->id);

            if(empty($blockLeague)) {
                return returnResponse(false, 'Blocked Tournament not found', [] ,Response::HTTP_NOT_FOUND, true);
            }

            $blockLeague->delete();

            return returnResponse(true, 'Tournament Enabled Successfully.', [] ,Response::HTTP_OK, true);

        }
        catch(\Exception $e) {
            return returnResponse(false, $e->getMessage(), [] ,Response::HTTP_EXPECTATION_FAILED, true);
        }
    }


    /**
     * Match
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function matches(Request $request)
    {
        try {
              // ================= permissions starts here =====================
              if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'sports_book';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                  $params->tenant_id = Auth::user()->tenant_id;

                  $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================    


            $query = new Event;

            $tenant_id = '';
            
            if (Auth::User()->parent_type == 'AdminUser' && Auth::User()->tenant_id) {
                $tenant_id = Auth::User()->tenant_id;
                $query = $query->where('tenant_id', $tenant_id);
            } else {
                $tenant_id = $request->has('tenant_id') ? $request->post('tenant_id') : '';
                $query = $query->where('tenant_id', $tenant_id);
            }

            // if (!empty($request->search)) {
            //     $query = $query->whereHas('eventParticapnt.participant', function ($q) use($request) {
            //             $q->where(DB::raw('lower(name_en)'), 'like', '%' . strtolower($request->search) . '%');
            //     });
            // }
            if($request->search) {
                $query = $query->where(DB::raw('name_en'), 'ilike', '%' . strtolower($request->search) . '%');
            }

            if ($request->enabled != '') {
                if($request->enabled == 1) {
                    $query = $query->doesnthave('blocked');
                } else {
                    $query = $query->whereHas('blocked');
                }
            }

            if($request->status > 0) {
                $query = $query->where('fixture_status', $request->status);
            }

            if (!empty($request->country)) {
              $query = $query->where('location_id', $request->country);
            }

            if (!empty($request->league_id)) {
              $query = $query->where('league_id', $request->league_id);
            }

            if($request->sport_id > 0) {
                $query = $query->where('sport_id', $request->sport_id);
            }

            $query = $query->with(['sport', 'league', 'blocked' => function($q) use($tenant_id) {
                $q->where('tenant_id', $tenant_id);
            }]);

            $matches = $query->orderBy('start_date', 'DESC')->paginate($request->size ? $request->size : 10);
            
            if ($matches)
                $matches = $matches->toArray();

            $data['data'] = (array)$matches['data'];

            foreach ($data['data'] as $key => $value) {
                $getName= SportsService :: getParticipantName($value['id']);
                if($getName){
                    $data['data'][$key]['participant_name'] = $getName;
                }
            }

            $data['total'] = (int)$matches['total'];
            unset($matches);
            return returnResponse(true, "Record get Successfully", $data);

        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function allMatches(Request $request)
    {
        try {
            $query = new Event;

            $tenant_id = '';
            
            if (Auth::User()->parent_type == 'AdminUser' && Auth::User()->tenant_id) {
                $tenant_id = Auth::User()->tenant_id;
            } else {
                $tenant_id = $request->has('tenant_id') ? $request->post('tenant_id') : '';
            }

            $query = $query->whereDoesntHave('blocked', function($q) use($tenant_id) {
                $q->where('tenant_id', $tenant_id);
            });

            if (!empty($request->league_id)) {
              $query = $query->whereHas('league', function ($q) use($request) {
                        $q->where('id', $request->league_id);
                });
            }

            // if(request()->user()->parent_type == "AdminUser") {
            //     $query = $query->doesnthave('blocked');
            // }

            $matches = $query->get();

            return returnResponse(true, "Record get Successfully", $matches);
        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @description updateMatchDisabled
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateMatchDisabled(Request $request) {
        try {

            $tenantBlockedMatch = TenantBlockedEvent::create([
                'tenant_id' => Auth::User()->tenant_id,
                'admin_user_id' => Auth::User()->id,
                'pulls_event_fixture_id' => $request->id
            ]);

            return returnResponse(true, 'Match Disabled Successfully.', $tenantBlockedMatch ,Response::HTTP_OK, true);

        }
        catch(\Exception $e) {
            return returnResponse(false, $e->getMessage(), [] ,Response::HTTP_EXPECTATION_FAILED, true);
        }
    }

    /**
     * @description updateMatchEnabled
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateMatchEnabled(Request $request) {
        try {
            $blockMatch = TenantBlockedEvent::find($request->id);

            if(empty($blockMatch)) {
                return returnResponse(false, 'Blocked Match not found', [] ,Response::HTTP_NOT_FOUND, true);
            }

            $blockMatch->delete();

            return returnResponse(true, 'Match Enabled Successfully.', [] ,Response::HTTP_OK, true);

        }
        catch(\Exception $e) {
            return returnResponse(false, $e->getMessage(), [] ,Response::HTTP_EXPECTATION_FAILED, true);
        }
    }

    /**
     * @description markets
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function markets(Request $request)
    {
        try {
              // ================= permissions starts here =====================
              if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'sports_book';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                  $params->tenant_id = Auth::user()->tenant_id;

                  $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here ===================== 

            $query = new Market;

            if (!empty($request->search)) {
                $query = $query->where(DB::raw('lower(name_en)'), 'like', '%' . strtolower($request->search) . '%');
            }

            $sql="
                 SELECT distinct market_id
                 FROM bets_bets
                 where event_id in (SELECT distinct id
                         FROM pulls_events where sport_id =$request->sport_id )
                         ";
             $resultMarketId= DB::select($sql);
             $resultID=[];
             foreach ($resultMarketId as $key =>$value){
                 $resultID[]=$value->market_id;
             }
             if (!empty($request->sport_id) && count($resultID)) {
                 $query = $query->whereIn('id',$resultID);
             }

             $markets = $query->orderBy('id', 'desc')->paginate($request->size ? $request->size : 10);

            /*
             * There are los of duplicate record
             * if (!empty($request->sport_id)) {
                $query = $query->join((new Bet)->getTable().' as b', 'pulls_markets.id', '=', 'b.market_id')
                ->join((new Event)->getTable().' as e', function($j) use($request) {
                    $j->on('e.id', '=', 'b.event_id')
                    ->where('e.sport_id', $request->sport_id);
                });
            }

            $markets = $query->orderBy('pulls_markets.updated_at', 'ASC')->paginate($request->size ? $request->size : 10);
            */
            if ($markets)
                $markets = $markets->toArray();


            $data['data'] = $markets['data'];
            $data['total'] = (int)$markets['total'];
            
            unset($markets);

            return returnResponse(true, "Record get Successfully", $data);

        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @description getSportsCountriesList
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSportsCountriesList()
    {
        try {

            $result = SportsService::getSportsCountry();
            return returnResponse(true, '', $result, Response::HTTP_OK, true);

        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @description bets
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function bets(Request $request) {
        try {

            $query = new Bet;

            if (!empty($request->search)) {
                $query = $query->where(DB::raw('lower(bet_id)'), 'like', '%' . strtolower($request->search) . '%');
            } else {
                $query = $query->whereNull('settlement_status');
            }
            $query->orderByDesc('created_at');
            $result = $query->with(['eventParticapnt' => function($e) { $e->with(['participant:id,name_en']); },
            'betslip.user' => function($u) { $u->select('id', 'email'); }])
            /*->whereHas('betslip.user', function ($q) {
                $q->where('tenant_id', Auth::User()->tenant_id);
            })*/->paginate($request->size ?? 10);

            if ($result)
                $result = $result->toArray();

            $data['data'] = $result['data'];
            $data['total'] = (int)$result['total'];
            unset($result);

            $timeZone = $request->time_zone_name ?? null;

            if($timeZone){
                foreach ($data['data'] as $key => $value) {
                    if($value["created_at"])
                        $data['data'][$key]["created_at"] = getTimeZoneWiseTime($value['created_at'],$timeZone);
                }
            }

            return returnResponse(true, '', $data, Response::HTTP_OK, true);

        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @description settlement
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function settlement(Request $request) {
        try {

            $data = [];

            $validator = Validator::make(
                $request->all(), [ 'otp_token' => 'required', 'otp' => 'required' ,'bets' => 'required' ] );

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_EXPECTATION_FAILED);
            }else{
                $input = $request->all();
                $decryptedOTPToken = Crypt::decryptString($input['otp_token']);
                $decryptedOTPTokenTime = explode('_',$decryptedOTPToken);
                $dateNowTime = time();
                $mins = ($dateNowTime - $decryptedOTPTokenTime[1]);


                if((int)$input['otp'] !== (int)$decryptedOTPTokenTime[0]){
                    return returnResponse(false, 'OTP can not be match', [], Response::HTTP_EXPECTATION_FAILED);
                }

                if($mins >300){
                    return returnResponse(false, 'OTP Expire Please try again', [], Response::HTTP_EXPECTATION_FAILED);
                }

                $url = env('NODE_API','http://52.202.64.110:8006/api/admin/settle-bet/');
                if(!$url){
                    return returnResponse(false, 'NODE API URL Not found', [], Response::HTTP_EXPECTATION_FAILED);

                }
                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS =>json_encode(["bets"=>$input['bets']]),
                        CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json'
                    ),
                ));
                $response = curl_exec($curl);
                $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                curl_close($curl);
            }

            if((int)$httpcode === 204){
                return returnResponse(true, 'Settlement Successfully.', $data, Response::HTTP_OK, true);
            }else{
                return returnResponse(false, 'Please try again', [], Response::HTTP_OK, true);
            }
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @description settlementOtpToMail
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function settlementOtpToMail(Request $request) {
        try {
            $data = [];
            $user = Auth::user();

            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                return returnResponse(false, '', $data, Response::HTTP_FORBIDDEN, true);
            }
            $email = Auth::user()->email;


            if (!$email) {
                return returnResponse(false, 'Email id not found', [], Response::HTTP_EXPECTATION_FAILED);
            }

            $randomnumber = 12345;//random_int(100000, 999999);;

            $data['randomKey'] = $randomnumber;
            $data['id'] = $user->id;
            $data['email'] = $user->email;
            $data['host'] = env('APP_NAME');

            Mail::send('emails.bet_settlement_otp_password', $data, function($message) use ($user) {
                $message->to($user->email);
                $message->subject('Bet-Settlement OTP | '.env('APP_NAME'));
            });

            $encrypted = Crypt::encryptString($randomnumber."_".time());

//            $decrypted = Crypt::decryptString($encrypted);
            return returnResponse(true, 'OTP is sent at Email '.$email, ['otp_token'=>$encrypted], Response::HTTP_OK);


        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

}
