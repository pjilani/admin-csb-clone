<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Currencies;
use App\Models\Betslip;
use App\Services\DashboardService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Validator;
use \Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Services\SportDashboardService;
use App\Models\TenantConfigurations;
use App\Models\Wallets;
use App\Services\TransactionsService;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        try {
                // ================= permissions starts here =====================
            if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'dashboard';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;

                $permission = checkPermissions($params);   
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here ===================== 
            setUnlimited();
            $params = [];
            $validatorArray['time_period'] = 'required';


            $validator = Validator::make($request->all(), $validatorArray);

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
            } else {
            $timeZone = $request->time_zone_name ?? 'UTC +00:00';
            $time_period = $request['time_period'] ?? [];

            if($time_period) {
                $params['time_period']['fromdate'] = getTimeZoneWiseTimeISO($request->start_date);
                $params['time_period']['enddate'] = getTimeZoneWiseTimeISO($request->end_date);
            }
            
                $agentId = $request->agent_id ?? '';
                $isDirectPlayer = @$request->player_type ?? "all";

                $agentIds = null;
                if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                    $roles = getRolesDetails();
                    $tenantId = $request->tenant_id?? Auth::user()->tenant_id;
                    if(in_array('sub-admin', $roles)){
                        $agentId = $request->agent_id ?? null;
                    } else {
                        $agentId = $request->agent_id ?? @Auth::user()->id;
                    }
                    if ($isDirectPlayer == "all" && $agentId) {
                        $params['agentIdTop'] = $agentId;
                    } else {
                        $params['agentIdTop'] = null;
                    }

                } else {
                    $tenantId = $request->tenant_id ?? null;
                }

                $params['agent_id'] = $agentId;
                $params['tenantId'] = $tenantId;
                $params['interval'] = $time_period;
                $params['isDirectPlayer'] = $isDirectPlayer;

                $transaction = DashboardService::fetchResult($params);
                $user = UserService ::fetchResult($params);
                $sport = SportDashboardService ::fetchResult($params);

                $CurrenciesValue = $this->getCurrencies($tenantId);

                $record ['deposit'] = $this->depositSeries($transaction, $request->time_period);
                $record ['deposit_cash_admin'] = $this->prepareObject($transaction, 'deposit_cash_admin');
                $record ['deposit_non_cash_admin'] = $this->prepareObject($transaction, 'deposit_non_cash_admin');
                $record ['deposit_manual_cash_user'] = $this->prepareObject($transaction, 'deposit_manual_cash_user');
                $record ['deposit_auto_cash_user'] = $this->prepareObject($transaction, 'deposit_auto_cash_user');
                $record ['ggr'] = $this->ggrSeries($transaction, $request->time_period);
                $record ['stake'] = $this->stakeSeries($transaction, $request->time_period);
                $record ['win_amount'] = $this->winAmount($transaction, $request->time_period);
                //$record ['withdraw'] = $this->withdraw($transaction);
                $record ['withdraw_cash_by_admin'] =  $this->prepareObjectWithdraw($transaction, 'withdraw_cash_admin');
                $record ['withdraw_non_cash_by_admin'] = $this->prepareObjectWithdraw($transaction, 'withdraw_non_cash_admin');
                $record ['withdraw_auto_cash_user'] = $this->prepareObjectWithdraw($transaction, 'withdraw_auto_cash_user');
                $record ['withdraw_manual_cash_user'] = $this->prepareObjectWithdraw($transaction, 'withdraw_manual_cash_user');
                $record ['casino_profit'] = $this->casinoProfit($record ['win_amount'], $record ['stake'], $CurrenciesValue);
                $record ['sport_profit'] = $this->sportProfit( $sport['aggregations']['currencies']['buckets'] );
                $record ['casino_esport_profit'] = $this->casinoEsportProfit($record ['casino_profit'], $record ['sport_profit'], $CurrenciesValue);
                $record ['active_players'] = $this->activePlayersSeries($transaction,$params);
                $record ['reg_player'] = @$user['aggregations']['players']['total_players']['value'];
                $record ['user_balance'] = $this->userBalance($user);
                $record ['betslip'] = $this->betslip($request->all(), $request->time_period);
                $record ['withdraw'] = $this->withdrawTotal($record);
                $record ['deposit_amount'] = $this->depositTotal($record);

//                $betTransaction = $this->betPlayerRecords($params);
                $record ['totalBetCount'] = 0;//$betTransaction['hits']['total']['value'] ?? 0;

                return returnResponse(true, "Record get Successfully", $record, Response::HTTP_OK, true);
            }
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }


    // function for player activity
    public function userDashboard(Request $request) {
      try {
              // ================= permissions starts here =====================
          if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
              $params = new \stdClass();
              $params->module = 'dashboard';
              $params->action = 'R';
              $params->admin_user_id = Auth::user()->id;
              $params->tenant_id = Auth::user()->tenant_id;

              $permission = checkPermissions($params);   
              if(!$permission){ 
                  return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
              }
              }
              // ================= permissions ends here ===================== 
          setUnlimited();
          $params = [];
          $validatorArray['time_period'] = 'required';


          $validator = Validator::make($request->all(), $validatorArray);

          if ($validator->fails()) {
              return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
          } else {
          
          $time_period = $request['time_period'] ?? [];

          if($time_period) {
              $params['time_period']['fromdate'] = getTimeZoneWiseTimeISO($request->start_date);
              $params['time_period']['enddate'] = getTimeZoneWiseTimeISO($request->end_date);
          }
          
              $agentId = $request->agent_id ?? '';
              $isDirectPlayer = @$request->player_type ?? "all";

              
              if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                  $roles = getRolesDetails();
                  $tenantId = $request->tenant_id?? Auth::user()->tenant_id;
                  if(in_array('sub-admin', $roles)){
                      $agentId = $request->agent_id ?? null;
                  } else {
                      $agentId = $request->agent_id ?? @Auth::user()->id;
                  }
                  if ($isDirectPlayer == "all" && $agentId) {
                      $params['agentIdTop'] = $agentId;
                  } else {
                      $params['agentIdTop'] = null;
                  }

              } else {
                  $tenantId = $request->tenant_id ?? null;
              }

              $params['agent_id'] = $agentId;
              $params['tenantId'] = $tenantId;
              $params['interval'] = $time_period;
              $params['isDirectPlayer'] = $isDirectPlayer;

              $transaction = DashboardService::fetchActivePlayerResult($params);
              $user = UserService ::fetchResult($params);
             

              $record ['active_players'] = $this->activePlayersSeries($transaction,$params);
              $record ['reg_player'] = @$user['aggregations']['players']['total_players']['value'];
              $record ['user_balance'] = $this->userBalance($user);


              $betTransaction = DashboardService::fetchWageredResult($params);
            $player_ids = [];
            if (is_array(@$betTransaction['aggregations']['uniq_players']['buckets'])) {
                $player_ids = array_column($betTransaction['aggregations']['uniq_players']['buckets'], 'key');
            }
            $record['players'] = UserService ::fetchPlayerResult($player_ids,$params);
             
              
//                $betTransaction = $this->betPlayerRecords($params);
            //   $record ['totalBetCount'] = 0;//$betTransaction['hits']['total']['value'] ?? 0;

              return returnResponse(true, "Record get Successfully", $record, Response::HTTP_OK, true);
          }
      } catch (\Exception $e) {
          if (!App::environment(['local'])) {//, 'staging'
              return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
          } else {
              return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                  'LineNo' => $e->getLine(),
                  'FileName' => $e->getFile()
              ], Response::HTTP_EXPECTATION_FAILED);
          }
      }
  }

   // function for player balance
   public function userDashboardBalanceInfo(Request $request) {
    try {
            // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'dashboard';
            $params->action = 'R';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 
        setUnlimited();
        $params = [];
        $agentId = $request->agent_id ?? '';
        if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
          $roles = getRolesDetails();
          $tenantId = $request->tenant_id?? Auth::user()->tenant_id;
          if(in_array('sub-admin', $roles)){
              $agentId = $request->agent_id ?? null;
          } else {
              $agentId = $request->agent_id ?? @Auth::user()->id;
          }
        } else {
          $tenantId = $request->tenant_id ?? null;
        }

      $params['agent_id'] = $agentId;
      $params['tenantId'] = $tenantId;
      
      $user = UserService ::fetchUserBalanceResult($params);
      $record ['user_balance'] = $this->userBalance($user);
      return returnResponse(true, "Record get Successfully", $record, Response::HTTP_OK, true);
    } catch (\Exception $e) {
        if (!App::environment(['local'])) {//, 'staging'
            return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
        } else {
            return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                'LineNo' => $e->getLine(),
                'FileName' => $e->getFile()
            ], Response::HTTP_EXPECTATION_FAILED);
        }
    }
}

    // function for sports activity
    public function sportsDashboard(Request $request) {
      try {
              // ================= permissions starts here =====================
          if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
              $params = new \stdClass();
              $params->module = 'dashboard';
              $params->action = 'R';
              $params->admin_user_id = Auth::user()->id;
              $params->tenant_id = Auth::user()->tenant_id;

              $permission = checkPermissions($params);   
              if(!$permission){ 
                  return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
              }
              }
              // ================= permissions ends here =====================
          $params = [];
          $validatorArray['time_period'] = 'required';
          $validator = Validator::make($request->all(), $validatorArray);

          if ($validator->fails()) {
              return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
          } else {
          $time_period = $request['time_period'] ?? [];

          if($time_period) {
              $params['time_period']['fromdate'] = getTimeZoneWiseTimeISO($request->start_date);
              $params['time_period']['enddate'] = getTimeZoneWiseTimeISO($request->end_date);
          }
          
              $agentId = $request->agent_id ?? '';
              $isDirectPlayer = @$request->player_type ?? "all";

              if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                  $roles = getRolesDetails();
                  $tenantId = $request->tenant_id?? Auth::user()->tenant_id;
                  if(in_array('sub-admin', $roles)){
                      $agentId = $request->agent_id ?? null;
                  } else {
                      $agentId = $request->agent_id ?? @Auth::user()->id;
                  }
                  if ($isDirectPlayer == "all" && $agentId) {
                      $params['agentIdTop'] = $agentId;
                  } else {
                      $params['agentIdTop'] = null;
                  }

              } else {
                  $tenantId = $request->tenant_id ?? null;
              }

              $params['agent_id'] = $agentId;
              $params['tenantId'] = $tenantId;
              $params['interval'] = $time_period;
              $params['isDirectPlayer'] = $isDirectPlayer;
              $sport = SportDashboardService ::fetchResult($params);
              $record ['sport_profit'] = $this->sportProfit( $sport['aggregations']['currencies']['buckets'] );
              $record ['betslip'] = $this->betslip($request->all(), $request->time_period);

              return returnResponse(true, "Record get Successfully", $record, Response::HTTP_OK, true);
          }
      } catch (\Exception $e) {
          if (!App::environment(['local'])) {//, 'staging'
              return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
          } else {
              return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                  'LineNo' => $e->getLine(),
                  'FileName' => $e->getFile()
              ], Response::HTTP_EXPECTATION_FAILED);
          }
      }
  }
    //function for casino activity
    public function gamingDashboard(Request $request) {
      try {
              // ================= permissions starts here =====================
          if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
              $params = new \stdClass();
              $params->module = 'dashboard';
              $params->action = 'R';
              $params->admin_user_id = Auth::user()->id;
              $params->tenant_id = Auth::user()->tenant_id;

              $permission = checkPermissions($params);   
              if(!$permission){ 
                  return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
              }
              }
              // ================= permissions ends here ===================== 
          setUnlimited();
          $params = [];
          $validatorArray['time_period'] = 'required';


          $validator = Validator::make($request->all(), $validatorArray);

          if ($validator->fails()) {
              return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
          } else {
          $time_period = $request['time_period'] ?? [];

          if($time_period) {
              $params['time_period']['fromdate'] = getTimeZoneWiseTimeISO($request->start_date);
              $params['time_period']['enddate'] = getTimeZoneWiseTimeISO($request->end_date);
          }
          
              $agentId = $request->agent_id ?? '';
              $isDirectPlayer = @$request->player_type ?? "all";

             
              if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                  $roles = getRolesDetails();
                  $tenantId = $request->tenant_id?? Auth::user()->tenant_id;
                  if(in_array('sub-admin', $roles)){
                      $agentId = $request->agent_id ?? null;
                  } else {
                      $agentId = $request->agent_id ?? @Auth::user()->id;
                  }
                  if ($isDirectPlayer == "all" && $agentId) {
                      $params['agentIdTop'] = $agentId;
                  } else {
                      $params['agentIdTop'] = null;
                  }

              } else {
                  $tenantId = $request->tenant_id ?? null;
              }

              $params['agent_id'] = $agentId;
              $params['tenantId'] = $tenantId;
              $params['interval'] = $time_period;
              $params['isDirectPlayer'] = $isDirectPlayer;

              $transaction = DashboardService::gamingResult($params);

              $CurrenciesValue = $this->getCurrencies($tenantId);
              $record ['ggr'] = $this->ggrSeries($transaction, $request->time_period);
              $record ['stake'] = $this->stakeSeries($transaction, $request->time_period);
              $record ['win_amount'] = $this->winAmount($transaction, $request->time_period);
              $record ['casino_profit'] = $this->casinoProfit($record ['win_amount'], $record ['stake'], $CurrenciesValue);

//                $betTransaction = $this->betPlayerRecords($params);
            //   $record ['totalBetCount'] = 0;//$betTransaction['hits']['total']['value'] ?? 0;

              return returnResponse(true, "Record get Successfully", $record, Response::HTTP_OK, true);
          }
      } catch (\Exception $e) {
          if (!App::environment(['local'])) {//, 'staging'
              return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
          } else {
              return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                  'LineNo' => $e->getLine(),
                  'FileName' => $e->getFile()
              ], Response::HTTP_EXPECTATION_FAILED);
          }
      }
  }

    //function for financial activity
    public function financialDashboard(Request $request) {
      try {
              // ================= permissions starts here =====================

            if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'dashboard';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;

                $permission = checkPermissions($params);   
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
            }
              // ================= permissions ends here ===================== 

          setUnlimited();
          $params = [];
          $validatorArray['time_period'] = 'required';


          $validator = Validator::make($request->all(), $validatorArray);

          if ($validator->fails()) {
              return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
          } else {

              $time_period = $request['time_period'] ?? [];

              if($time_period) {
                  $params['time_period']['fromdate'] = getTimeZoneWiseTimeISO($request->start_date);
                  $params['time_period']['enddate'] = getTimeZoneWiseTimeISO($request->end_date);
              }
              
              $agentId = $request->agent_id ?? '';
              $isDirectPlayer = @$request->player_type ?? "all";

            
              if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                  $roles = getRolesDetails();
                  $tenantId = $request->tenant_id?? Auth::user()->tenant_id;
                  if(in_array('sub-admin', $roles)){
                      $agentId = $request->agent_id ?? null;
                  } else {
                      $agentId = $request->agent_id ?? @Auth::user()->id;
                  }
                  if ($isDirectPlayer == "all" && $agentId) {
                      $params['agentIdTop'] = $agentId;
                  } else {
                      $params['agentIdTop'] = null;
                  }

              } else {
                  $tenantId = $request->tenant_id ?? null;
              }

              $params['agent_id'] = $agentId;
              $params['tenantId'] = $tenantId;
              $params['interval'] = $time_period;
              $params['isDirectPlayer'] = $isDirectPlayer;

              $transaction = DashboardService::financialResult($params);
              $record ['deposit'] = $this->depositSeries($transaction, $request->time_period);
              $record ['deposit_cash_admin'] = $this->prepareObject($transaction, 'deposit_cash_admin');
              $record ['deposit_non_cash_admin'] = $this->prepareObject($transaction, 'deposit_non_cash_admin');
              $record ['deposit_manual_cash_user'] = $this->prepareObject($transaction, 'deposit_manual_cash_user');
              $record ['deposit_auto_cash_user'] = $this->prepareObject($transaction, 'deposit_auto_cash_user');
              $record ['withdraw_cash_by_admin'] =  $this->prepareObjectWithdraw($transaction, 'withdraw_cash_admin');
              $record ['withdraw_non_cash_by_admin'] = $this->prepareObjectWithdraw($transaction, 'withdraw_non_cash_admin');
              $record ['withdraw_auto_cash_user'] = $this->prepareObjectWithdraw($transaction, 'withdraw_auto_cash_user');
              $record ['withdraw_manual_cash_user'] = $this->prepareObjectWithdraw($transaction, 'withdraw_manual_cash_user');
              $record ['withdraw'] = $this->withdrawTotal($record);
              $record ['deposit_amount'] = $this->depositTotal($record);

              return returnResponse(true, "Record get Successfully", $record, Response::HTTP_OK, true);
          }
      } catch (\Exception $e) {
          if (!App::environment(['local'])) {//, 'staging'
              return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
          } else {
              return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                  'LineNo' => $e->getLine(),
                  'FileName' => $e->getFile()
              ], Response::HTTP_EXPECTATION_FAILED);
          }
      }
  }

    public function activeUniquePlayersCount(Request $request) {
        try {
             // ================= permissions starts here =====================
             if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'dashboard';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                 $params->tenant_id = Auth::user()->tenant_id;

                 $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================
            $params = [];
            $validatorArray['time_period'] = 'required';
    
    
            $validator = Validator::make($request->all(), $validatorArray);
    
            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
            } else {
            $timeZone = $request->time_zone_name ?? 'UTC +00:00';
            $time_period = $request['time_period'] ?? [];
            if($time_period) {
                if ($time_period != 'custom') {
                    $time_periodQ = dateConversionWithTimeZone($time_period);
                    $params['time_period']['fromdate'] = getTimeZoneWiseTimeISO(getTimeZoneWiseTime($time_periodQ['fromdate'], $timeZone));
                    $params['time_period']['enddate'] = getTimeZoneWiseTimeISO(getTimeZoneWiseTime($time_periodQ['enddate'], $timeZone));
                } else {
                    $params['time_period']['fromdate'] = getTimeZoneWiseTimeISO(getTimeZoneWiseTime($request->start_date, $timeZone));
                    $params['time_period']['enddate'] = getTimeZoneWiseTimeISO(getTimeZoneWiseTime($request->end_date, $timeZone));
                }
            }
              $agentId = $request->agent_id ?? '';
              $isDirectPlayer = @$request->player_type ?? "all";
    
              if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                  $tenantId = @Auth::user()->tenant_id;
                  $roles = getRolesDetails();
                  $agentId = $request->agent_id ?? @Auth::user()->id;
                  if(in_array('sub-admin', $roles)){
                      $agentId = $request->agent_id ?? null;
                  }
                  if ($isDirectPlayer == "all" && $agentId) {
                      $params['agentIdTop'] = $agentId;
                  } else {
                      $params['agentIdTop'] = null;
                  }
    
              } else {
                  $tenantId = $request->tenant_id ?? null;
              }
    
              $params['is_dashboard'] = (boolean)@$request->is_dashboard??0;
              $params['agent_id'] = $agentId;
              $params['tenantId'] = $tenantId;
              $params['interval'] = $time_period;
              $params['isDirectPlayer'] = $isDirectPlayer;
    
            }
            // =====================
            $betTransaction = DashboardService::fetchWageredResult($params);
            $player_ids = [];
            if (is_array(@$betTransaction['aggregations']['uniq_players']['buckets'])) {
                $player_ids = array_column($betTransaction['aggregations']['uniq_players']['buckets'], 'key');
            }
            $players = UserService ::fetchPlayerResult($player_ids,$params);
            return $players;
        }
        catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    function betPlayerRecords($params){
      $betTransaction = DashboardService::fetchWageredResult($params);
      $player_ids = [];
      // du($betTransaction);
      // get User Player ID
      foreach ($betTransaction as $key => $value) {
        if (is_array(@$betTransaction['aggregations']['uniq_players']['buckets']))
        foreach ($betTransaction['aggregations']['uniq_players']['buckets'] as $key => $rec) {
            $player_ids[$key] = $rec['key'];
          }
      } 
      $players = [];
      $players = UserService ::fetchPlayerResult($player_ids,$params);
       
        
        return $players;
    }

   
   
   
    public function activePLayers(Request $request) {
        try {
             // ================= permissions starts here =====================
             if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'dashboard';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                 $params->tenant_id = Auth::user()->tenant_id;

                 $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================
            setUnlimited();
            $params = [];
            $validatorArray['time_period'] = 'required';


            $validator = Validator::make($request->all(), $validatorArray);

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
            } else {

                $time_period = $request->time_period ? $request->time_period : 'today';
                $params['time_period'] = dateConversion($time_period);
                $agentId = $request->agent_id ?? '';
                $isDirectPlayer = @$request->player_type ?? "all";

                $agentIds = null;
                if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                  
                    $tenantId = $request->tenant_id?? Auth::user()->tenant_id;
                    $agentId = $request->agent_id ?? @Auth::user()->id;

                    if ($isDirectPlayer == "all" && $agentId) {
                        $params['agentIdTop'] = $agentId;
                    } else {
                        $params['agentIdTop'] = null;
                    }

                } else {
                    $tenantId = $request->tenant_id ?? null;
                    // $topAgent = DB::select('SELECT id FROM "admin_users" where tenant_id = '.$tenantId.' order by id asc LIMIT 1');  
                    // if(!$topAgent){
                    //   return returnResponse(true, "Agent Not Found for this tenant",[],500, true);
                    // }
                    // $agentId = !empty($topAgent) ? $topAgent[0]->id : '';
                }

                $params['agent_id'] = $agentId;
                $params['tenantId'] = $tenantId;
                $params['interval'] = $time_period;
                $params['isDirectPlayer'] = $isDirectPlayer;
                $params['page'] = $request->page;

              
                

                $user = $this->betPlayerRecords($params);
                $record ['totalBetCount'] = $user['hits']['total']['value'] ?? 0;
                // $record ['totalBetCount'] = $this->totalBetCount($user);
                $record ['betList'] = $this->betList($user);
                $record ['total'] = $user['hits']['total']['value'] ?? 0;
                
                return returnResponse(true, "Record get Successfull", $record, Response::HTTP_OK, true);
            }
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    private function timePeriod($time_period) {
        if($time_period == "today") {
            return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        } else if($time_period == "weekly") {
            return [0, 0, 0, 0, 0, 0, 0];
        } else if($time_period == "monthly") {
            $arr = [];
            for ($x = 0; $x <= date('t'); $x++) {
                $arr[$x] = 0;
            }
            return $arr;
        } if($time_period == "yearly") {
            return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        } else {
            return [];
        }
    }

    /**
     * @description depositSeries
     * @param $record
     * @return array
     */
    
    private function depositSeries($record, $time_period)
    {
        $series = [];
        $totalDepinEurdata = [];

        foreach ($record['aggregations']['currencies']['buckets'] as $key => $rec) {

            $series[$key]['name'] = $rec['key'];
            $series[$key]['type'] = 'line';
            $series[$key]['data'] = [];
            $series[$key]['chart_total'] = [];

            foreach ($rec['chart_result']['dashboard_charts']['buckets'] as $key2 => $recd) {
                $series[$key]['data'][$key2] = round($recd['deposit_for_chart']['total']['value'], 2);
                $series[$key]['chart_total'][$key2] = round($recd['deposit_for_chart']['total_in_EUR']['value'], 2);
            }

            $totalDepinEurdata[$key] = $series[$key]['chart_total'];

            $series[$key]['total'] = round(array_sum($series[$key]['data']), 2);
            $series[$key]['eur_total'] = round(array_sum($series[$key]['chart_total']), 2);

        }

        $eurData = [];
        $c = count($series);

        if(is_array(@$totalDepinEurdata[0]))
        foreach (@$totalDepinEurdata[0] as $ekey => $val) {
            $ecdata = [];

            for ($x = 0; $x <= $c - 1; $x++) {
                $ecdata[$x] = $totalDepinEurdata[$x][$ekey];
            }

            $eurData[$ekey] = round(array_sum($ecdata), 2);

        }

        $series[$c]['name'] = 'Total in Base Currency';
        $series[$c]['type'] = 'line';
        // $series[$c]['data'] = @$eurData;
        $series[$c]['data'] = array_sum(@$eurData) > 0 ? @$eurData : $this->timePeriod($time_period);
        $series[$c]['total'] = round(array_sum(@$eurData), 2);

        return $series;
    }

    private function ggrSeries($record, $time_period)
    {

        $series = [];
        $totalDepinEurdata = [];

        if(is_array(@$record['aggregations']['currencies']['buckets']))
        foreach ($record['aggregations']['currencies']['buckets'] as $key => $rec) {

            $series[$key]['name'] = $rec['key'];
            $series[$key]['type'] = 'line';
            $series[$key]['data'] = [];
            $series[$key]['chart_total'] = [];

            if(is_array(@$rec['chart_result']['dashboard_charts']['buckets']))
            foreach ($rec['chart_result']['dashboard_charts']['buckets'] as $key2 => $recd) {

                if (!empty($recd['ggr_for_chart_in_EUR']['value'])) {
                    $series[$key]['chart_total'][$key2] = round($recd['ggr_for_chart_in_EUR']['value'], 2);
                } else {
                    $series[$key]['chart_total'][$key2] = 0;
                }

                if (!empty($recd['ggr_for_chart']['value'])) {
                    $series[$key]['data'][$key2] = round($recd['ggr_for_chart']['value'], 2);
                } else {
                    $series[$key]['data'][$key2] = 0;
                }

            }

            $totalDepinEurdata[$key] = $series[$key]['chart_total'];

            $series[$key]['total'] = round(array_sum($series[$key]['data']), 2);
            $series[$key]['eur_total'] = round(array_sum($series[$key]['chart_total']), 2);

        }

        $eurData = [];

        $c = @count($series);

        if(is_array(@$totalDepinEurdata[0]))
        foreach (@$totalDepinEurdata[0] as $ekey => $val) {
            $ecdata = [];

            for ($x = 0; $x <= $c - 1; $x++) {
                $ecdata[$x] = $totalDepinEurdata[$x][$ekey];
            }

            $eurData[$ekey] = round(array_sum($ecdata), 2);

        }

        $series[$c]['name'] = 'Total in Base Currency';
        $series[$c]['type'] = 'line';
        $series[$c]['data'] = array_sum(@$eurData) > 0 ? @$eurData : $this->timePeriod($time_period); //@$eurData;
        $series[$c]['total'] = round(array_sum(@$eurData), 2);

        return $series;
    }

    private function stakeSeries($record, $time_period)
    {

        $series = [];
        $totalDepinEurdata = [];

        if(is_array(@$record['aggregations']['currencies']['buckets']))
        foreach ($record['aggregations']['currencies']['buckets'] as $key => $rec) {

            $series[$key]['name'] = $rec['key'];
            $series[$key]['type'] = 'line';
            $series[$key]['data'] = [];
            $series[$key]['chart_total'] = [];

            if(is_array(@$rec['chart_result']['dashboard_charts']['buckets']))
            foreach ($rec['chart_result']['dashboard_charts']['buckets'] as $key2 => $recd) {

                if (!empty($recd['bet_after_refund_for_chart_in_EUR']['value'])) {
                    $series[$key]['chart_total'][$key2] = round($recd['bet_after_refund_for_chart_in_EUR']['value'], 2);
                } else {
                    $series[$key]['chart_total'][$key2] = 0;
                }

                if (!empty($recd['bet_after_refund_for_chart']['value'])) {
                    $series[$key]['data'][$key2] = round($recd['bet_after_refund_for_chart']['value'], 2);
                } else {
                    $series[$key]['data'][$key2] = 0;
                }

            }

            $totalDepinEurdata[$key] = $series[$key]['chart_total'];

            $series[$key]['total'] = round(array_sum($series[$key]['data']), 2);
            $series[$key]['eur_total'] = round(array_sum($series[$key]['chart_total']), 2);

        }

        $eurData = [];
        $c = @count($series);

        if (is_array(@$totalDepinEurdata[0]))
        foreach (@$totalDepinEurdata[0] as $ekey => $val) {
            $ecdata = [];

            for ($x = 0; $x <= $c - 1; $x++) {
                $ecdata[$x] = $totalDepinEurdata[$x][$ekey];
            }

            $eurData[$ekey] = round(array_sum($ecdata), 2);

        }

        $series[$c]['name'] = 'Total in Base Currency';
        $series[$c]['type'] = 'line';
        $series[$c]['data'] = array_sum(@$eurData) > 0 ? @$eurData : $this->timePeriod($time_period); //@$eurData;
        $series[$c]['total'] = round(array_sum(@$eurData), 2);

        return $series;
    }

    // helper function
    private function findValue($arr, $mot) {
        foreach ($arr as $k => $v) {
            if($v['name'] == $mot) {
                return $v;
            }
        }
        return false;
    }

    private function getCurrencies($tenantId) {
        $CurrenciesValue = array();
        
        if(Auth::User()->parent_type == "SuperAdminUser") {

            if(!empty($tenantId)) {
            
                $configurations = TenantConfigurations::on('read_db')->select('allowed_currencies')->where('tenant_id', $tenantId)->get();
                $configurations = $configurations->toArray();

                if($configurations) {
                    $configurations = $configurations[0];
                    if (strlen($configurations['allowed_currencies']) > 0) {

                        $configurations = str_replace('{', '', $configurations['allowed_currencies']);
                        $configurations = str_replace('}', '', $configurations);
                        $configurations = explode(',', $configurations);
                        
                        if (@$configurations[0] && count($configurations) > 0) {
                            $CurrenciesValue = Currencies::select('code')->whereIn('id', $configurations)->get();
                            $CurrenciesValue = $CurrenciesValue->toArray();
                        }
                    }
                }

            } else {
                $CurrenciesValue = Currencies::on('read_db')->select('code')->get();
                $CurrenciesValue = $CurrenciesValue->toArray();
            }

        } else {
            $CurrenciesValue = Wallets::on('read_db')->select(DB::raw('(SELECT code FROM "currencies" where id=wallets.currency_id) as code'))
            ->where(['owner_type'=> ADMIN_TYPE, 'owner_id' => Auth::User()->id])->get();
            $CurrenciesValue = $CurrenciesValue->toArray();
        }

        return $CurrenciesValue;
    }

    private function casinoProfit($winAmount, $stake, $CurrenciesValue) {
        $winAmountData = $winAmount['data'];
        array_pop($stake);

        $casinoProfit = array();
        $totalInEur = 0;

        foreach($CurrenciesValue as $key => $value) {
            
            // $stakeKey = array_search($value['name'], array_column($stake, 'name'));
            $stakeVal = $this->findValue($stake, $value['code']);
            $winVal = $this->findValue($winAmountData, $value['code']);

            $casinoProfit[$key]['name'] = $value['code'];
            $casinoProfit[$key]['total_in'] = round( ($stakeVal['total'] ?? 0 ), 2);
            $casinoProfit[$key]['total_out'] = round( ($winVal['total'] ?? 0), 2);
            $casinoProfit[$key]['grand_total'] = round( ( $casinoProfit[$key]['total_in'] - $casinoProfit[$key]['total_out'] ), 2);
            $casinoProfit[$key]['total_in_eur'] = round( ( ($stakeVal['eur_total'] ?? 0) - ($winVal['eur_total'] ?? 0) ), 2);
            $totalInEur += round( $casinoProfit[$key]['total_in_eur'], 2);

        }

        return [ 'data' => $casinoProfit, 'grand_total_in_eur' => round( $totalInEur, 2) ];
    }

    private function casinoEsportProfit($casinoData, $sportData, $CurrenciesValue) {
        $casinoProfit = array();
        $sport = $sportData['data'];
        $casino = $casinoData['data'];
        
        foreach($CurrenciesValue as $key => $value) {

            // $sportKey = array_search($value['name'], array_column($sport, 'name'));
            $sportVal = $this->findValue($sport, $value['code']);
            $casinoVal = $this->findValue($casino, $value['code']);

            $casinoProfit[$key]['name'] = $value['code'];
            $casinoProfit[$key]['total'] = round( ( ($casinoVal['grand_total'] ?? 0) + ( $sportVal['grand_total'] ?? 0)), 2);
            $casinoProfit[$key]['total_in_eur'] = round( ( ($casinoVal['total_in_eur'] ?? 0) +( $sportVal['total_in_eur'] ?? 0) ), 2);

        }

        return [ 'data' => $casinoProfit, 'grand_total_in_eur' => round( ($casinoData['grand_total_in_eur'] + $sportData['grand_total_in_eur']), 2) ];
    }

    /**
     * @description sportProfit
     * @param $sport
     * @return array
     */
    private function sportProfit($sport)
    {
        $sportProfit = array();
        $totalInEur = 0;

        foreach ($sport as $key => $value) {

            $sportProfit[$key]['name'] = strtoupper($value['key']);
            $sportProfit[$key]['total_in'] = ($value['bet']['bet_amount']['value'] + $value['win_dr']['total_win_amount']['value']);
            $sportProfit[$key]['total_in_non_cash'] = ($value['bet_non_cash']['bet_amount']['value'] + $value['win_non_cash_dr']['total_win_amount']['value']);

            //for eur
            $sportProfit[$key]['total_in_eur'] = ($value['bet']['bet_amount_in_EUR']['value'] + $value['win_dr']['total_win_amount_in_EUR']['value']);
            $sportProfit[$key]['total_in_non_cash_eur'] = ($value['bet_non_cash']['bet_amount_in_EUR']['value'] + $value['win_non_cash_dr']['total_win_amount_in_EUR']['value']);




            $sportProfit[$key]['total_in_amount'] = round( $sportProfit[$key]['total_in'] + $sportProfit[$key]['total_in_non_cash'] ,2);

            $sportProfit[$key]['total_in_amount_eur'] = round( $sportProfit[$key]['total_in_eur'] ,2);


            $sportProfit[$key]['total_out'] = round(($value['win']['total_win_amount']['value']+$value['bet_cr']['bet_amount']['value']), 2);
            $sportProfit[$key]['total_out_non_cash'] = round(($value['win_non_cash']['total_win_amount']['value']+$value['bet_non_cash_cr']['bet_amount']['value']), 2);

            //for eur
            $sportProfit[$key]['total_out_eur'] = round(($value['win']['total_win_amount_in_EUR']['value']+$value['bet_cr']['bet_amount_in_EUR']['value']), 2);
            $sportProfit[$key]['total_out_non_cash_eur'] = round(($value['win_non_cash']['total_win_amount_in_EUR']['value']), 2);

            $sportProfit[$key]['commission_amount'] =round($value['commission_amount']['commission_amount']['value'],2);
            $sportProfit[$key]['commission_amount_eur'] =round($value['commission_amount']['total_commission_amount_in_EUR']['value'],2);

            $sportProfit[$key]['total_out_amount'] =round($sportProfit[$key]['total_out']+$sportProfit[$key]['total_out_non_cash'],2);
            $sportProfit[$key]['total_out_amount_eur'] =round($sportProfit[$key]['total_out_eur']+$sportProfit[$key]['total_out_non_cash_eur'],2);
            /* $sportProfit[$key]['grand_total'] = round(
                 ($sportProfit[$key]['total_in_amount'] - $sportProfit[$key]['total_out_amount']) -

                 ($sportProfit[$key]['total_in_non_cash'] + $value['win_non_cash']['total_win_amount']['value'])

                 , 2);*/
           /* Changes suggestted by roy client side*/

            $sportProfit[$key]['grand_total'] = round( ($sportProfit[$key]['total_in_amount'] - $sportProfit[$key]['total_out_amount']) , 2)-$sportProfit[$key]['commission_amount'];

            $sportProfit[$key]['total_in_eur'] = $totalSportProfit = round(
                    ($sportProfit[$key]['total_in_amount_eur'] - $sportProfit[$key]['total_out_amount_eur']) , 2);

            $totalInEur += round(@$totalSportProfit, 2);

        }

        return ['data' => $sportProfit, 'grand_total_in_eur' => round($totalInEur, 2)];
    }

    /**
     * @description winAmount
     * @param $record
     * @param $time_period
     * @return array
     */
    private function winAmount($record, $time_period)
    {

        $series = [];
        $eurArr = [];

        if (is_array(@$record['aggregations']['currencies']['buckets']))
            foreach ($record['aggregations']['currencies']['buckets'] as $key => $rec) {

                $series[$key]['name'] = $rec['key'];
                $series[$key]['total'] = round($rec['win']['total_win_amount']['value'], 2);
                $series[$key]['eur_total'] = round($rec['win']['total_win_amount_in_EUR']['value'], 2);
                $eurArr[] = $series[$key]['eur_total'];
            }

        return ['data' => $series, 'eur_total' => round(array_sum($eurArr), 2)];
    }

    private function prepareObject($record,$objKye)
    {

        $series = [];
        $eurArr = [];
        if (is_array(@$record['aggregations']['currencies']['buckets']))
            foreach ($record['aggregations']['currencies']['buckets'] as $key => $rec) {
                $series[$key]['name'] = @$rec['key'];
                if (isset($rec[$objKye])) {
                    $series[$key]['total'] = @round($rec[$objKye]['total_deposit']['value'], 2);
                    $series[$key]['eur_total'] = @round($rec[$objKye]['total_deposit_in_EUR']['value'], 2);
                    $eurArr[] = $series[$key]['eur_total'];
                }
            }
        return ['data' => $series, 'eur_total' => round(array_sum($eurArr), 2)];
    }
    private function prepareObjectWithdraw($record,$objKye)
    {

        $series = [];
        $eurArr = [];
        if (is_array(@$record['aggregations']['currencies']['buckets']))
            foreach ($record['aggregations']['currencies']['buckets'] as $key => $rec) {
                $series[$key]['name'] = @$rec['key'];
                if (isset($rec[$objKye])) {
                    $series[$key]['total'] = @round($rec[$objKye]['total_withdraw']['value'], 2);
                    $series[$key]['eur_total'] = @round($rec[$objKye]['total_withdraw_in_EUR']['value'], 2);
                    $eurArr[] = $series[$key]['eur_total'];
                }
            }
        return ['data' => $series, 'eur_total' => round(array_sum($eurArr), 2)];
    }
    /**
     * @description withdraw
     * @param $record
     * @return array
     */
    private function withdraw($record)
    {

        $series = [];
        $eurArr = [];
        if (is_array(@$record['aggregations']['currencies']['buckets']))
            foreach ($record['aggregations']['currencies']['buckets'] as $key => $rec) {
                $series[$key]['name'] = $rec['key'];
                $series[$key]['total'] = round($rec['final_withdraw']['value'], 2);
                $series[$key]['eur_total'] = round($rec['final_withdraw_in_EUR']['value'], 2);
                $eurArr[] = $series[$key]['eur_total'];
            }

        return ['data' => $series, 'eur_total' => round(array_sum($eurArr), 2)];
    }

    private function withdrawTotal($record)
    {
        $series = [];
        $seriesData = [];
        $overAllTotal = 0;
        foreach ($record['withdraw_cash_by_admin']['data'] as $index => $data) {
            $total = (float)$record['withdraw_cash_by_admin']['data'][$index]['total'] +
                     (float)$record['withdraw_non_cash_by_admin']['data'][$index]['total'] +
                     (float)$record['withdraw_auto_cash_user']['data'][$index]['total'] +
                     (float)$record['withdraw_manual_cash_user']['data'][$index]['total'];
    
            $eurTotal = (float)$record['withdraw_cash_by_admin']['data'][$index]['eur_total'] +
                        (float)$record['withdraw_non_cash_by_admin']['data'][$index]['eur_total'] +
                        (float)$record['withdraw_auto_cash_user']['data'][$index]['eur_total'] +
                        (float)$record['withdraw_manual_cash_user']['data'][$index]['eur_total'];
    
            $series[$data['name']] = $total;
            $overAllTotal += $eurTotal;
            $seriesDataItem = [
                "name" => $data['name'],
                "total" => $total,
                "eur_total" => $eurTotal
            ];
    
            array_push($seriesData, $seriesDataItem);
        }

        $lastEurTotal = $overAllTotal;
    
        return [
            'data' => $seriesData,
            'eur_total' => round($lastEurTotal, 2)
        ];
    }    
    private function depositTotal($record)
    {
        $series = [];
        $seriesData = [];
    
        foreach ($record['deposit_cash_admin']['data'] as $index => $data) {
            $total = (float)$record['deposit_non_cash_admin']['data'][$index]['total'] +
                     (float)$data['total'] +
                     (float)$record['deposit_manual_cash_user']['data'][$index]['total'] +
                     (float)$record['deposit_auto_cash_user']['data'][$index]['total'];
    
            $eurTotal = (float)$record['deposit_non_cash_admin']['data'][$index]['eur_total'] +
                        (float)$data['eur_total'] +
                        (float)$record['deposit_manual_cash_user']['data'][$index]['eur_total'] +
                        (float)$record['deposit_auto_cash_user']['data'][$index]['eur_total'];
    

                        $series[$data['name']] = $total;
    
            $seriesDataItem = [
                "name" => $data['name'],
                "total" => round($total, 2),
                "eur_total" => round($eurTotal, 2)
            ];
    
            array_push($seriesData, $seriesDataItem);
        }
    
        $lastEurTotal = @end($seriesData)['eur_total'];
    
        return [
            'data' => $seriesData,
            'eur_total' => round($lastEurTotal, 2)
        ];
    }    
    private function totalBetCount($record)
    {

        $series = [];
        $eurArr = [];

        if (is_array(@$record['aggregations']['currencies']['buckets']))
            foreach ($record['aggregations']['currencies']['buckets'] as $key => $rec) {
                $series[$key]['name'] = $rec['key'];
                $series[$key]['total_bets'] = round($rec['bet']['doc_count'], 2);
                
            }

        return ['data' => $series];
    }
   
    private function betList($record)
    {

      
        $series = [];
    
            foreach ($record['hits']['hits'] as $key => $rec) {

              if(isset($rec['_source']['player_id'])){
                $series[] = [
                  // "first_name" => $rec['_source']['first_name'],
                  // "last_name" => $rec['_source']['last_name'],
                  "user_name" => @$rec['_source']['user_name'],
                  "email" => @$rec['_source']['email'],
                   "phone_number" => @$rec['_source']['phone'],
                  "agent_name" => @$rec['_source']['agent_full_name'],
                  "total_bet_amount" => @$rec['_source']['total_bet_amount'],
                  "total_bets" => @$rec['_source']['total_bets'],
               ];
                
              }
            }

            // dd(($series));

            // $collection = collect(array_values(array_unique($series, SORT_REGULAR)));

            // $page = $params['page'] ?? 1;;
            // $perPage = 10;
            // // $currentItems = array_slice($record, $perPage * ($page - 1), $perPage);
            // $paginate = new LengthAwarePaginator(
            //     // $currentItems,
            //     $collection->forPage($page, $perPage)->values(),
            //     $collection->count(),
            //     $perPage,
            //     $page
            // );
           
            
            
          
            
        // return ['data' => array_values(array_unique($series, SORT_REGULAR))];
        return ['data' => $series];
    }

    /**
     * @description activePlayersSeries
     * @param $record
     * @return array
     */
    private function activePlayersSeries($record,$params)
    {

        $interval='hour';
        $intervalParam=$params['interval']??"today";
        if($intervalParam==='weekly'){
            $interval='day';
        }elseif ($intervalParam==='lastMonth'){
          $interval='day';
        }elseif ($intervalParam==='monthly'){
            $interval='day';
        }elseif ($intervalParam==='yearly'){
            $interval='month';
        }
        $series['name'] = 'Player Count';
        $series['type'] = 'line';
        $series['data'] = [];
//du($record['aggregations']['active_player_chart']['active_player']['buckets']);

        if($intervalParam==='yearly') {
            $resultSQL = TransactionsService::getTransactionInfoDashboard($params);
            $resultSQLByMonth = [];
            foreach ($resultSQL as $key =>$value){
                $resultSQLByMonth[$value->created_at_date]=$value->total_active_user;
            }
            if (is_array(@$record['aggregations']['active_player_chart']['active_player']['buckets']))
                foreach ($record['aggregations']['active_player_chart']['active_player']['buckets'] as $key => $rec) {
                    $dateMonth = explode('-', $rec['key_as_string'])[1];
                    if(@$resultSQLByMonth[$dateMonth]){
                        $series['data'][$key] = $resultSQLByMonth[$dateMonth];
                    }else{
                        $series['data'][$key] = 0;
                    }
                }
        }   elseif($intervalParam==='lastMonth' ||$intervalParam==='weekly' || $intervalParam==='custom') {
          $resultSQL = TransactionsService::getTransactionInfoDashboard($params);
          $resultSQLByMonth = [];
          foreach ($resultSQL as $key =>$value){
              $day = @explode('/',$value->created_at_date);
              $resultSQLByMonth[@$day[2]]=$value->total_active_user;
          }
          if (is_array(@$record['aggregations']['active_player_chart']['active_player']['buckets']))
              foreach ($record['aggregations']['active_player_chart']['active_player']['buckets'] as $key => $rec) {
                  $dateMonthDay = @explode('-', $rec['key_as_string'])[2];
                  $dateMonthDay = @explode('T', $dateMonthDay)[0];
                  if(@$resultSQLByMonth[$dateMonthDay]){
                      $series['data'][$key] = $resultSQLByMonth[$dateMonthDay];
                  }else{
                      $series['data'][$key] = 0;
                  }
              }
          }elseif($intervalParam==='monthly' ||$intervalParam==='weekly' || $intervalParam==='custom') {
            $resultSQL = TransactionsService::getTransactionInfoDashboard($params);
            $resultSQLByMonth = [];
            foreach ($resultSQL as $key =>$value){
                $day = @explode('/',$value->created_at_date);
                $resultSQLByMonth[@$day[2]]=$value->total_active_user;
            }
            if (is_array(@$record['aggregations']['active_player_chart']['active_player']['buckets']))
                foreach ($record['aggregations']['active_player_chart']['active_player']['buckets'] as $key => $rec) {
                    $dateMonthDay = @explode('-', $rec['key_as_string'])[2];
                    $dateMonthDay = @explode('T', $dateMonthDay)[0];
                    if(@$resultSQLByMonth[$dateMonthDay]){
                        $series['data'][$key] = $resultSQLByMonth[$dateMonthDay];
                    }else{
                        $series['data'][$key] = 0;
                    }
                }
        }elseif($intervalParam==='today') {
            $date = date('Y-m-d');
            $params['time_period']['fromdate']=$date." 00:00:00";
            $params['time_period']['enddate']=$date." 23:59:59";
            $resultSQL = TransactionsService::getTransactionInfoDashboard($params);
            $resultSQLByMonth = [];
            if(is_array($resultSQL) && count($resultSQL)>1) {
                foreach ($resultSQL as $key => $value) {
                    $resultSQLByMonth[$value->created_at_date] = $value->total_active_user;
                }
            }
            if (is_array(@$record['aggregations']['active_player_chart']['active_player']['buckets']))
                foreach ($record['aggregations']['active_player_chart']['active_player']['buckets'] as $key => $rec) {
                    $dateMonthDay = @explode('T', $rec['key_as_string'])[1];
                    $dateMonthDayH = @explode(':',$dateMonthDay)[0];
                    if(@$resultSQLByMonth[$dateMonthDayH]){
                        $series['data'][$key] = (int)@$resultSQLByMonth[$dateMonthDayH];
                    }else{
                        $series['data'][$key] = 0;
                    }
                }
        }
        return [$series];
    }

    /**
     * @description userBalance
     * @param $record
     * @return array
     */
    private function userBalance($record)
    {
        // dd($record['aggregations']['currencies']['buckets']);
        $data = [];
        // $eurbonusArr = [];
        $bonusArr= [];
        $eurRealArr = [];
        $currencySource = Currencies::getPrimaryCurrency();
        if (is_array(@$record['aggregations']['currencies']['buckets']))
            foreach ($record['aggregations']['currencies']['buckets'] as $key => $cur) {
                $data[$key]['name'] = $cur['key'];
                $data[$key]['bonus_balance'] = round($cur['bonus_balance']['value'], 4);
                $data[$key]['real_balance'] = round($cur['real_balance']['value'], 4);
                $data[$key]['bonus_total'] = $data[$key]['bonus_balance'];

                $currency = Currencies::where('code', @$data[$key]['name'])->first();

                // $data[$key]['eur_bonus'] = round($data[$key]['bonus_balance'] * ($currencySource->exchange_rate /@(float)$currency->exchange_rate), 2);
                $data[$key]['eur_real'] = round($data[$key]['real_balance'] * ($currencySource->exchange_rate/@(float)$currency->exchange_rate), 4);

                // $data[$key]['eur_bonus_total'] = $data[$key]['eur_bonus'];
                $data[$key]['eur_real_total'] = $data[$key]['eur_real'];

                $eurRealArr[] = $data[$key]['eur_real_total'];
                // $eurbonusArr[] = $data[$key]['eur_bonus_total'];
                $bonusArr[]=$data[$key]['bonus_total'];
            }

        return ['data' => $data, 'eur_real_total' => round(array_sum($eurRealArr), 4),'bonus_total' => round(array_sum($bonusArr), 4)];

    }

    /**
     * @description betslip
     * @param $request
     * @param $time_period
     * @return array
     */
    private function betslip($request, $time_period)
    {

        $tenant_id = '';

        $authUser = Auth::User();
        if ($authUser->parent_type == 'AdminUser' && $authUser->tenant_id) {
            $tenant_id = $request['tenant_id']?? $authUser->tenant_id;
            $roles = getRolesDetails();
            if (!in_array('owner', $roles)) {
                if ($request['agent_id']) {
                    $agent_id = $request['agent_id'] ?? null;
                } else {
                    $agent_id = $authUser->id;
                }
                //condition for new sub-admin role
                if (in_array('sub-admin', $roles)) {
                    $agent_id = $request['agent_id'] ?? null;
                }

            } else {
                $agent_id = $request['agent_id'] ?? null;

            }
        }else{
            $tenant_id = $request['tenant_id'];
        }
        $lbquery = new Betslip;
        $oquery = new Betslip;
        $tquery = new Betslip;
        $cquery = new Betslip;

        if (!empty($tenant_id)) {
           
            if(!empty($agent_id)){
                $lbquery = $lbquery->whereHas('user', function ($q) use ($tenant_id, $agent_id) {
                    $q->where('tenant_id', $tenant_id)->where('parent_id',$agent_id);
                });
               
               
                $oquery = $oquery->whereHas('user', function ($q) use ($tenant_id, $agent_id) {
                    $q->where('tenant_id', $tenant_id)->where('parent_id',$agent_id);
                });
    
                $tquery = $tquery->whereHas('user', function ($q) use ($tenant_id, $agent_id) {
                    $q->where('tenant_id', $tenant_id)->where('parent_id',$agent_id);
                });
    
                $cquery = $cquery->whereHas('user', function ($q) use ($tenant_id, $agent_id) {
                    $q->where('tenant_id', $tenant_id)->where('parent_id',$agent_id);
                });
            }else{
                $lbquery = $lbquery->whereHas('user', function ($q) use ($tenant_id) {
                    $q->where('tenant_id', $tenant_id);
                });
               
               
                $oquery = $oquery->whereHas('user', function ($q) use ($tenant_id) {
                    $q->where('tenant_id', $tenant_id);
                });
    
                $tquery = $tquery->whereHas('user', function ($q) use ($tenant_id) {
                    $q->where('tenant_id', $tenant_id);
                });
    
                $cquery = $cquery->whereHas('user', function ($q) use ($tenant_id) {
                    $q->where('tenant_id', $tenant_id);
                });
            }
           
        }
        $betslip = $lbquery->select('id', 'stake', 'possible_win_amount')
            ->orderBy('id', 'desc')
            ->where('created_at', '>', Carbon::parse($request['start_date'])->toDateTimeString())
            ->where('created_at', '<', Carbon::parse($request['end_date'])->toDateTimeString())
            ->take(5)->get();
           
        $totalBetslip = $tquery->where('created_at', '>', Carbon::parse($request['start_date'])
            ->toDateTimeString())->where('created_at', '<', Carbon::parse($request['end_date'])->toDateTimeString())
            ->count();

        $openbetslip = $oquery->where('settlement_status', 'in_game')
            ->where('created_at', '>', Carbon::parse($request['start_date'])->toDateTimeString())
            ->where('created_at', '<', Carbon::parse($request['end_date'])->toDateTimeString())
            ->count();

        $betslipChartData = $cquery->select('id', 'stake','created_at',
            DB::raw("(SELECT id FROM wallets as w where w.owner_id=bets_betslip.user_id and owner_type='User') AS wallet_id"),
            DB::raw("(SELECT amount FROM wallets as w where w.owner_id=bets_betslip.user_id and owner_type='User') AS wallet_amount"),
            DB::raw("(SELECT code FROM currencies as c where c.id IN (SELECT currency_id FROM wallets as w where w.owner_id=bets_betslip.user_id and owner_type='User') ) AS currency_code")
        )->where('created_at', '>', Carbon::parse($request['start_date'])->toDateTimeString())
            ->where('created_at', '<', Carbon::parse($request['end_date'])->toDateTimeString())->get()->groupBy('currency_code');

        $series = [];

        $seriesData = [];
        $seriesData2 = [];
        $chartCount = 0;

        foreach ($betslipChartData as $key => $value) {
            $series[$chartCount]['name'] = $key;
            $series[$chartCount]['type'] = 'line';
            $chgd = $value->groupBy(function ($date) use ($time_period) {

                switch ($time_period) {
                    case 'today':
                        return Carbon::parse($date->created_at)->format('H'); // grouping by hours
                        break;
                    case 'weekly':
                        return Carbon::parse($date->created_at)->format('w'); // grouping by weeks
                        break;
                    case 'monthly':
                        return Carbon::parse($date->created_at)->format('d'); // grouping by dates
                        break;
                    case 'yearly':
                        return Carbon::parse($date->created_at)->format('m'); // grouping by months
                        break;
                    case 'custom':
                        return Carbon::parse($date->created_at)->format('d'); // grouping by days
                        break;
                    default:
                        return Carbon::parse($date->created_at)->format('H'); // grouping by hours
                }

            });

            $btcount = [];
            $Arr = [];
            $Arr1 = [];

            $btcountBetstake = [];
            $btcountBetstakeTotal = [];
            foreach ($chgd as $key => $value) {

                //$btcount[(int)$key] = count($value);
                foreach ($value as $key1 => $value2) {
//                    $btcountBetstake[(int)$key."-".$value2['currency_code']][$key1] = $value2['stake'];
                    $btcountBetstake[(int)$key] = @$btcountBetstake[(int)$key] + $value2['stake'];
                    $btcountBetstakeTotal[(int)$key] = @$btcountBetstakeTotal[$value2['currency_code']] + (Currencies::getExchangeRateCurrency($value2['currency_code']) * $value2['stake']);

                }
                $btcount[(int)$key] = @$btcountBetstake[$key];
            }
            $count = 24;

            $earlier = new DateTime($request['start_date']);
            $later = new DateTime($request['end_date']);
            $chartData = [];
            switch ($time_period) {
                case 'today':
                    $count = 24;
                    break;
                case 'weekly':
                    $count = 7;
                    break;
                case 'monthly':
                    $count = date('t');
                    break;
                case 'yearly':
                    $count = 12;
                    break;
                case 'custom':
                      $period = new DatePeriod(
                        new DateTime($request['start_date']),
                        new DateInterval('P1D'),
                        new DateTime($request['end_date'])
                      );
                      foreach ($period as $key => $value) {
                        $dayKey = $value->format('d');
    
                        if (array_key_exists($dayKey, $btcount)) {
                            $chartData[$key] = $btcount[$dayKey];
                        } else {
                            $chartData[$key] = 0;
                        }
                      }
                      $btcount = $chartData;
                      $count = $earlier->diff($later)->format("%r%a") + 1; 
                      break;
                default:
                    $count = 24;
            }

            for ($i = 0; $i < $count; $i++) {
                if (!empty($btcount[$i])) {
                    $Arr[] = $btcount[$i];
                } else {
                    $Arr[] = 0;
                }
            }

            for ($i = 0; $i < $count; $i++) {
                if (!empty($btcountBetstakeTotal[$i])) {
                    $Arr1[] = $btcountBetstakeTotal[$i];
                } else {
                    $Arr1[] = 0;
                }
            }

            $series[$chartCount]['data'] = $Arr;
            $seriesData[$chartCount] = $Arr;
            $seriesData2[$chartCount] = $Arr1;

            $chartCount++;
        }//print_r($seriesData);du($seriesData2);

        $sc = @count($series);

        $final = [];
        if ($sc > 0) {
            array_walk_recursive($seriesData2, function ($item, $key) use (&$final) {
                $final[$key] = isset($final[$key]) ? round($item + $final[$key], 3) : $item;
            });
        }

        $series[$sc]['name'] = 'Total in Base Currency';
        $series[$sc]['type'] = 'line';
        $series[$sc]['data'] = $sc > 0 ? $final : $this->timePeriod($time_period);

        return ['data' => $series, 'latest_betslips' => $betslip, 'open_betslips' => $openbetslip, 'total_betslips' => $totalBetslip];
    }

}
