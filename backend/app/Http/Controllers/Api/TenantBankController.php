<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Models\Admin;
use App\Models\TenantBankConfiguration;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;

use App\Models\Currencies;

 
class TenantBankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getList(Request $request)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
        $params = new \stdClass();
        $params->module = 'bank_details';
        $params->action = 'R';
        $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

        $permission = checkPermissions($params);   
        if(!$permission){ 
          return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
        }
      }
      // ================= permissions ends here =====================          
    
      $rolesObject = getRoles(Auth::user()->id);
        try {
          $tenant_id = Auth::user()->tenant_id;
          $admin_user_id = Auth::user()->id;
          $id = 0;
          if(isset($request->id) && $request->id != ''){
            $id = $request->id;
          }
          $data['list'] = TenantBankConfiguration::getList($tenant_id,$id);
          $data['total'] = count($data['list']);
          return returnResponse(true, 'All Data', $data, 200, true);
        } catch (\Exception $e) {
        
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function addNewBank(Request $request)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
          $params = new \stdClass();
          $params->module = 'bank_details';
          if($request->id){
            $params->action = 'U';
          }
          else {
             $params->action = 'C';
          }
          $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);
          if(!$permission){ 
            return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
          }
        }
        // ================= permissions ends here =====================  
       
        $validator = Validator::make($request->all(), [
            'bank_name' => 'required',
            'bank_ifsc_code' => 'required',
            'account_number' => 'required',
            'account_holder_name' => 'required',
        ]);

        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), 400);
        }

        $data = $request->all();
       
        try {
          if($data){
            $tenant_id = Auth::user()->tenant_id;
            $admin_user_id = Auth::user()->id;
            $insertData = [
              'used_for' => 'Manual Deposit',
              'bank_name' => $data['bank_name'],
              'last_updated_owner_id' => $admin_user_id,
              'bank_ifsc_code' => $data['bank_ifsc_code'],
              'account_number' => $data['account_number'],
              'account_holder_name' => $data['account_holder_name'],
              'status' => ($data['active'] ? true : false),
              'updated_at' =>date('Y-m-d H:i:s'),
            ];

            if(isset($data['id']) && $data['id'] != ''){
              // Edit Code

             
              $DuplicateBankCheck = TenantBankConfiguration::where("account_number",$request->account_number)
                                ->where(
                                  function($query)  use ($data) {
                                    return $query
                                          ->where('id', '!=', $data['id'])
                                          ->where('tenant_id', Auth::user()->tenant_id);
                                  })
                                ->first();
              if($DuplicateBankCheck){
                return returnResponse(false, 'This Bank Account is already Added.', [], 500, true);
              }


              $createdValues = TenantBankConfiguration::where('id',$data['id'])->update($insertData);
              $message = "Updated successfully.";
            }else{
              
              $DuplicateBankCheck = TenantBankConfiguration::where("account_number",$request->account_number)
                                    ->where(
                                      function($query)  use ($data) {
                                        return $query
                                              ->where('tenant_id', Auth::user()->tenant_id);
                                      })
                                    ->first();
              if($DuplicateBankCheck){
              return returnResponse(false, 'This Bank Account is already Added.', [], 500, true);
              }


              $insertData['tenant_id'] = $tenant_id;
              $insertData['created_by'] = $admin_user_id;
              $insertData['created_at'] = date('Y-m-d H:i:s');

              $createdValues = TenantBankConfiguration::create($insertData);
              $message = "insert successfully.";
            }



            
            if ($createdValues) {
                return returnResponse(true, $message, $insertData, 200, true);
            } else {
                return returnResponse(false, $message, [], 403, true);
            }
            
          }
            
        } catch (\Exception $e) {
         
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Currencies $currencies
     * @return \Illuminate\Http\Response
     */
    public function updateStatus(Request $request)
    {

        try {
                  // ================= permissions starts here =====================
              if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'bank_details';
                $params->action = 'T';
                $params->admin_user_id = Auth::user()->id;
                  $params->tenant_id = Auth::user()->tenant_id;

                  $permission = checkPermissions($params);
                if(!$permission){ 
                  return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
              }
              // ================= permissions ends here =====================   
            if ($request->id != '') {
              TenantBankConfiguration::where('id',$request->id)->update(["status"=>$request->status]);
              return returnResponse(true, "Bank List Updated Successfully", Currencies::all());
            } else {
                return returnResponse(false, "Selected Bank is invalid!", [], 500);
            }
        } catch (\Exception $e) {
          
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    
}
