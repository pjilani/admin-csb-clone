<?php

namespace App\Http\Controllers\Api;

use App\Aws3;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Casino\CasinoItems;
use App\Models\Casino\CasinoProviders;
use App\Models\Casino\MenuItems;
use App\Models\Currencies;
use App\Models\Menu\MenuTenantSettingModel;
use App\Models\PageBanners;
use App\Models\PaymentProviders;
use App\Models\PermissionRole;
use App\Models\Reports\TenantReportConfigurations;
use App\Models\Sport\TenantSportsBetSetting;
use App\Models\TenantBlockedSports;
use App\Models\TenantConfigurations;
use App\Models\TenantCredentials;
use App\Models\TenantCredentialsKeys;
use App\Models\TenantPermission;
use App\Models\Tenants;
use App\Models\Faq;
use App\Models\FaqCategory;
use App\Models\TenantThemeSettings;
use App\Models\TenantSocialMedia;
use App\Services\SportsService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Rules\UniqueSlug;
use Illuminate\Support\Str;
use Validator;
use Carbon\Carbon;

class TenantsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $record = Tenants::select('tenants.id','tenants.name', DB::raw("(SELECT ARRAY_TO_STRING(ARRAY_AGG(admin_role_id), ',') FROM admin_users_admin_roles WHERE admin_user_id  =  admin_users.id)as rolesIds"))
            ->WhereRaw('admin_users.id in (SELECT admin_user_id FROM admin_users_admin_roles where admin_role_id=1 and admin_user_id=admin_users.id)')->orderBy('tenants.name', 'ASC')->join('admin_users','admin_users.tenant_id','=','tenants.id')->get();
            
            //   $users = [];
            // foreach ($record as $key => $value) {
            //   $users = Admin::select('*', DB::raw("(SELECT ARRAY_TO_STRING(ARRAY_AGG(admin_role_id), ',') FROM admin_users_admin_roles WHERE admin_user_id  =  admin_users.id)as rolesIds"))
            //   ->WhereRaw('id in (SELECT admin_user_id FROM admin_users_admin_roles where admin_role_id=1 and admin_user_id=admin_users.id)')
            //   ->where('tenant_id', $value['id'])->count();
            //    $record[$key]['Agent'] = 1;
            //   if(!$users){
            //     // unset($record[$key]);
            //     $record[$key]['Agent'] = 0;
            //   }
            // }

            
            
             


            return returnResponse(true, "Record get Successfully", $record);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function listTenants(Request $request)
    {
        try {
            $page = $request->has('page') ? $request->post('page') : 1;
            $limit = $request->has('size') ? $request->post('size') : 10;


            $filter['name'] = $request->has('name') ? $request->post('name') : '';
            $filter['domain'] = $request->has('domain') ? $request->post('domain') : '';
            $filter['active'] = $request->has('active') ? $request->post('active') : '';

            $record = Tenants::getList($limit, $page, $filter);


            return returnResponse(true, "Record get Successfully", $record);
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'domain' => 'required|unique:tenants',
            'language' => 'required',
            'layout' => 'required|int',
            'status' => 'required',
            'loginMethod' => 'required',
            'smsGateway' => 'required',
            'forgotPasswordOption' => 'required',
            'assignedProvider' => 'required',
            // 'assignedPaymentProvider' => 'required',
            'currencies' => 'required',

        ]);

        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), 400);
        }

        $isSaveALl = false;
        DB::beginTransaction();
        try {
            $insertData = [
                'name' => $request->name,
                'domain' => $request->domain,
                'active' => ($request->status ? true : false),
            ];
            $createdValues = Tenants::create($insertData);
            if ($createdValues) {
                $insertedTanantsId = $createdValues->id;

               /* $allSportIds = Sport::getSportIds(); not usefully as per jay discussion

                if(count($allSportIds)){
                    $sportData=[];
                    foreach ($allSportIds as $key => $value) {
                        $sportData []= array('tenant_id' => $insertedTanantsId,
                            'admin_user_id' => Auth::user()->id,
                            'pulls_sport_id' => $value->id,
                            'is_deleted' => false,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        );
                    }
                    TenantBlockedSports::insert($sportData);
                }*/

                $sportSetting=[
                    "tenant_id"=>$insertedTanantsId,
                    "event_liability" =>EVENT_LIABILITY,
                    "max_single_bet"=>MAX_SINGLE_BET,
                    "min_bet"=>MIN_BET,
                    "max_multiple_bet"=>MAX_MULTIPLE_BET,
                    "max_bet_on_event"=>MAX_BET_ON_EVENT,
                    "deposit_limit"=>DEPOSIT_LIMIT,
                    "max_win_amount"=>MAX_WIN_AMOUNT,
                    "max_odd"=>MAX_ODD,
                    "cashout_percentage"=>CASHOUT_PERCENTSGE,
                    "bet_disabled" => BET_DISABLED
                ];
                TenantSportsBetSetting::create($sportSetting);

                MenuTenantSettingModel::create(["tenant_id" => $insertedTanantsId, "menu_id" => PROMOTION]);


                // Create Registration form setting form
                $tenantRegistrationFormFieldArray = [
                  'tenant_id'=> $insertedTanantsId,
                  "first_name" => VISIBLE_WITH_REQUIRED,
                  "last_name" => VISIBLE_WITH_REQUIRED,
                  "email" => ($request->loginMethod == 0 ? VISIBLE_WITH_REQUIRED : VISIBLE_WITH_OPTIONAL),
                  "phone" => ($request->loginMethod == 1 ? VISIBLE_WITH_REQUIRED : VISIBLE_WITH_OPTIONAL),
                  "username" => VISIBLE_WITH_REQUIRED,
                  "nick_name" => VISIBLE_WITH_REQUIRED,
                  "dob" => VISIBLE_WITH_REQUIRED,
                  "currency" => VISIBLE_WITH_REQUIRED,
                  "city" => VISIBLE_WITH_REQUIRED,
                  "zip_code" => VISIBLE_WITH_REQUIRED,
                  "password" => VISIBLE_WITH_REQUIRED,
                  "confirm_password" => VISIBLE_WITH_REQUIRED,
                  "promo_code" => NOT_VISIBLE,
                  "eighteen_year_check" => VISIBLE_WITH_REQUIRED,
                ];

                DB::table('tenant_user_registration_fields')->insert($tenantRegistrationFormFieldArray);

                $aws3 = new Aws3();
              $PathS3Key = '';
              $fileName = Str::uuid() . '____' . $request->file('logo')->getClientOriginalName();
              $fileName = str_replace(' ', '_', $fileName);
              $fileNamePath = "tenants/" . $insertedTanantsId . "/logo/" . $fileName;

              $PathS3Key = $aws3->uploadFile($fileNamePath, $request->file('logo')->getPathname(), $request->file('logo')->getClientMimeType());
              $fabIconPathS3Key = '';
              if($request->file('fab_icon_url')){
              // Fab Icon Upload
              $fileFabName = Str::uuid() . '____' . $request->file('fab_icon_url')->getClientOriginalName();
              $fileFabName = str_replace(' ', '_', $fileFabName);
              $fileFabNamePath = "tenants/" . $insertedTanantsId . "/logo/" . $fileFabName;
              $fabIconPathS3Key = $aws3->uploadFile($fileFabNamePath, $request->file('fab_icon_url')->getPathname(), $request->file('fab_icon_url')->getClientMimeType());

              }
                $popupImagePathS3Key = '';
              if (@$request->file('signup_popup_image_url')) {
              // Fab Icon Upload

              $fileFabName = Str::uuid() . '____' . $request->file('signup_popup_image_url')->getClientOriginalName();
              $fileFabName = str_replace(' ', '_', $fileFabName);
              $fileFabNamePath = "tenants/" . $request->id . "/logo/" . $fileFabName;
              $popupImagePathS3Key = $aws3->uploadFile($fileFabNamePath, $request->file('signup_popup_image_url')->getPathname(), $request->file('signup_popup_image_url')->getClientMimeType());

              }
               

                $postDataTanants['tenant_id'] = $insertedTanantsId;
                $postDataTanants['allowed_currencies'] =  implode(',', $request->currencies);//json_encode($request->currencies);//($request->currencies ? json_encode([$request->currencies] ) : null);
                $response = TenantConfigurations::create($postDataTanants);


                $postDataTanants['tenant_id'] = $insertedTanantsId;
                $postDataTanants['layout_id'] = $request->layout;
                $postDataTanants['language_id'] = $request->language;
                $postDataTanants['theme'] = $request->theme;
                $postDataTanants['logo_url'] = $PathS3Key;
                $postDataTanants['fab_icon_url'] = $fabIconPathS3Key;
                $postDataTanants['signup_popup_image_url'] = $popupImagePathS3Key;
                $postDataTanants['whatsapp_number'] = (isset($request->whatsappNumber) ? $request->whatsappNumber : '');
                $postDataTanants['powered_by'] = (isset($request->poweredBy) ? $request->poweredBy : '');
                $postDataTanants['user_login_type'] = $request->loginMethod; // 1-> Mobile Base Login, 2-> Email Login Base
                $postDataTanants['sms_gateway'] = $request->smsGateway; // Twilio, Fast2SMS
                $postDataTanants['forgot_password_option'] = $request->forgotPasswordOption; // email, mobile
                $postDataTanants['assigned_providers'] = $request->assignedProvider;
                $postDataTanants['allowed_modules'] =$request->allowedModules;
                $postDataTanants['payment_providers'] =$request->assignedPaymentProvider;
                $response = TenantThemeSettings::create($postDataTanants);

                $isSaveALl = true;
            }

        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
        DB::commit();
        if ($isSaveALl) {
            return returnResponse(true, 'insert successfully.', $insertData, 200, true);
        } else {
            return returnResponse(false, 'insert unsuccessfully.', [], 403, true);
        }

    }


    public function getListCredentials(Request $request){
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'tenant_credentials';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $id = Auth::user()->tenant_id;
            }
            $data['credentials'] = TenantCredentials::orderBy('id', 'asc')->where('tenant_id', $id)->get();

            return returnResponse(true, "Record get Successfully", $data, Response::HTTP_OK, true);
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @description getThemeSetting
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getThemeSetting(Request $request){
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'tenant_theme_settings';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){
                    $params1 = new \stdClass();
                    $params1->module = ' tenant_configurations';
                    $params1->action = 'R';
                    $params1->admin_user_id = Auth::user()->id;
            
                    $permission1 = checkPermissions($params1);
                    if(!$permission1){
                        return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                    }    
                }
                }
                // ================= permissions ends here =====================          
          
            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $id = Auth::user()->tenant_id;
            }
            $setting = @TenantThemeSettings::where('tenant_id', $id)->get()->first();

            if ($setting && count($setting->toArray())) {
                $aws3 = new Aws3();

                if ($aws3->FileExists($setting['logo_url']))
                    $setting['logo_url'] = Aws3::getAwsUrl($setting['logo_url']);
            }
            $values = [];
            if($setting->assigned_providers){
              $assigned_providers_values = explode(',',$setting->assigned_providers);
              foreach ($assigned_providers_values as $key => $value) {
                $values[$key]['provider_name'] = CasinoProviders::select('name')->where('id',$value)->first()->name;
              }
            }
            $setting['assigned_providers_values'] = $values;

            $data['setting'] = $setting;
            return returnResponse(true, "Record get Successfully", $data, Response::HTTP_OK, true);
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                'LineNo' => $e->getLine(),
                'FileName' => $e->getFile()
            ], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tenants $tenants
     * @return \Illuminate\Http\Response
     */
    public function show($id = '')
    {
        try {
            $CurrenciesValue = [];

            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $id = Auth::user()->tenant_id;
            }
            if ($id == '') {
                return returnResponse(true, "Record get Successfully", Tenants::all());
            } else {
                $data['tenants'] = Tenants::find($id)->toArray();

                if (Auth::user()->parent_type != 'AdminUser') {
                    $users = Admin::select('*', DB::raw("(SELECT ARRAY_TO_STRING(ARRAY_AGG(admin_role_id), ',') FROM admin_users_admin_roles WHERE admin_user_id  =  admin_users.id)as rolesIds"))
                        ->WhereRaw('id in (SELECT admin_user_id FROM admin_users_admin_roles where admin_role_id=1 and admin_user_id=admin_users.id)')
                        ->where('tenant_id', $id)->get();
                    $data['users'] = $users;
                }
                $configurations = TenantConfigurations::select('allowed_currencies')->where('tenant_id', $id)->get();
                $configurations = $configurations->toArray();
                if ($configurations) {

                    $configurations = $configurations[0];
                    if (strlen($configurations['allowed_currencies']) > 1) {
                        $configurations = str_replace('{', '', $configurations['allowed_currencies']);
                        $configurations = str_replace('}', '', $configurations);
                        $configurations = explode(',', $configurations);

                        if (@$configurations[0] && count($configurations) > 0) {
                            $CurrenciesValue = Currencies::select('id', 'name', 'code')->whereIn('id', $configurations)->get();
                            $CurrenciesValue = $CurrenciesValue->toArray();
                        }
                    }

                    $data['configurations'] = $CurrenciesValue;
                    $data['credentials'] = TenantCredentials::where('tenant_id', $id)->get();
                    $data['sports'] = [];//SportsService::getSportsData($id);
                    $data['sports_bet_setting'] = TenantSportsBetSetting::where('tenant_id',$id)->get();
                    $setting = @TenantThemeSettings::where('tenant_id', $id)->get()->first();

                    if ($setting && count($setting->toArray())) {
                        //$aws3 = new Aws3();

                        /*if ($aws3->FileExists($setting['logo_url']))
                            $setting['logo_url'] = Aws3::getAwsUrl($setting['logo_url']);*/
                    }


                    // payment provider data
                    $data['paymentProvider'] = DB::connection('read_db')->table('tenant_payment_configurations as pc')->select('pc.*','pp.provider_name','pp.provider_type','pp.currencies', 'pp.active as provider_active')->where(['pc.tenant_id'=>$id])->leftJoin('payment_providers as pp','pp.id','=','pc.provider_id')->get();
                    
                    foreach ($data['paymentProvider'] as $value) {
                        if($value->currencies){
                            $CurrenciesValue = Currencies::on('read_db')->select('id', 'name')->whereIn('id', json_decode($value->currencies))->get()->toArray();
                            $value->currencyList = $CurrenciesValue;
                        }else{
                            $value->currencyList = [];
                        }
                    }

                    if (!is_null($data['paymentProvider'])) {
                        $data['paymentProvider'] = $data['paymentProvider']->toArray();
                        foreach ($data['paymentProvider'] as $key => $value) {
                            $list = json_decode($value->provider_key_values,true);
                            $data['paymentProvider'][$key]->key_values = $list; 
                        }
                    }
                  
                  
                    // Tenant Register Form Fields
                    $data['tenantRegistrationFields'] = DB::table('tenant_user_registration_fields')->selectRaw('first_name,last_name,email,phone,username,nick_name,dob,currency,city,zip_code,password,confirm_password,promo_code,eighteen_year_check')->where(['tenant_id'=>$id])->first();
                    
                    $values = [];
                    if($setting->assigned_providers){
                      $assigned_providers_values = explode(',',$setting->assigned_providers);
                      // dd($assigned_providers_values);
                      foreach ($assigned_providers_values as $key => $value) {
                        $values[$key]['provider_name'] = CasinoProviders::select('name')->where('id',$value)->first()->name;
                      }
                    }
                    $setting['assigned_providers_values'] = $values;
                    $paymentValues = [];
                    if($setting->payment_providers){
                      $payment_providers_values = explode(',',$setting->payment_providers);
                      // dd($assigned_providers_values);
                      foreach ($payment_providers_values as $key => $value) {
                        $paymentValues[$key]['payment_provider_name'] = PaymentProviders::select('provider_name')->where('id',$value)->first()->provider_name;
                      }
                    }
                    $setting['payment_providers_values'] = $paymentValues;
                    $setting['allowed_payment_providers'] = explode(',',$setting->payment_providers);
                    $data['setting'] = $setting;
                    $reportSetting= DB::table('report_master as rm')->where(["active"=>true])->get(['id','name']);
                    $reportTenant= DB::table('tenant_report_configurations')
                                    ->where('tenant_id','=',$id)
                                    ->get(DB::raw("(ARRAY_TO_STRING(ARRAY_AGG(report_id), ',')) as report_ids"));
                    $reportTenantId=[];
                    if($reportTenant && @$reportTenant->toArray()[0]->report_ids !=''){
                        $reportTenantId=explode(',',$reportTenant->toArray()[0]->report_ids);
                    }
                    $reportSettingArray=[];
                    foreach ($reportSetting as $key =>$value){
                        if(in_array($value->id,$reportTenantId)){
                            $value->status='enable';
                        }else{
                            $value->status='disable';
                        }
                        $reportSettingArray[]=$value;
                    }
                    $data['report_setting'] = $reportSettingArray;
                    $menu = DB::table('menu_master')
                        ->where('active','=',true)
                        ->get()->toArray();
                    $data['menu_setting']= DB::table('menu_tenant_setting')
                        ->where('tenant_id','=',$id)
                        ->get(['menu_id'])->pluck('menu_id','menu_id');
                    if(count($menu)) {
                        foreach ($menu as $key =>$value) {
                            if (isset($data['menu_setting'][$value->id]) && $data['menu_setting'][$value->id] == $value->id) {
                                $menu[$key]->selectValue = true;
                            } else {
                                $menu[$key]->selectValue = false;
                            }
                        }
                    }
                    $data['menu_setting'] = $menu;

                    // Tenant Permissions Field
                    $tenantPermissions = DB::table('tenant_permissions')->select('permission')->where(['tenant_id'=>$id])->first();

                    $data['tenantPermissions'] = ($tenantPermissions) ? $tenantPermissions->permission : null;

                    //player commission interval
                    $playerCommissionInterval = TenantConfigurations::on('read_db')->select('player_commission_interval')->where('tenant_id', $id)->first();
                    $data['playerCommissionInterval'] = ($playerCommissionInterval) ? $playerCommissionInterval->player_commission_interval : 'null';


                    return returnResponse(true, "Record get Successfully", $data, 200, true);
                }
            }

        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }
    
    
    public function getregistrationFields()
    {
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'tenant_registration_fields';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            $CurrenciesValue = [];

            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $id = Auth::user()->tenant_id;
            }else{
              return returnResponse(false, "No Tenant Found", [], 500);

            }
                $data['tenants'] = Tenants::find($id)->toArray();

                $configurations = TenantConfigurations::select('allowed_currencies')->where('tenant_id', $id)->get();
                $configurations = $configurations->toArray();
                if ($configurations) {

                    $setting = @TenantThemeSettings::where('tenant_id', $id)->get()->first();

                    // Tenant Register Form Fields
                    $data['tenantRegistrationFields'] = DB::table('tenant_user_registration_fields')->selectRaw('first_name,last_name,email,phone,username,nick_name,dob,currency,city,zip_code,password,promo_code,confirm_password,eighteen_year_check')->where(['tenant_id'=>$id])->first();
                    
                    $data['setting'] = $setting;
                   
                    return returnResponse(true, "Record get Successfully", $data, 200, true);
                }
            

        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function getTenantSportsBetSetting(Request $request) {
        try {
            
            if(empty($request->id)) {
                return returnResponse(false, 'Setting Id is required', [], Response::HTTP_EXPECTATION_FAILED);
            }
            $tenantSportsBetSetting = TenantSportsBetSetting::find((int)$request->id);

            if(empty($tenantSportsBetSetting)) {
                return returnResponse(false, 'Setting Not found', [], Response::HTTP_NOT_FOUND);
            }

            return returnResponse(true, "Record get Successfully", $tenantSportsBetSetting, 200, true);

        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    public function updateTenantSportsBetSetting(Request $request) {
        try {

            $validator = Validator::make($request->all(), [
                'event_liability' => 'required',
                'max_single_bet' => 'required',
                'min_bet' => 'required',
                'max_multiple_bet' => 'required',
//                'max_bet_on_event' => 'required',
                'deposit_limit' => 'required',
                'max_win_amount' => 'required',
                'max_odd' => 'required',
                'cashout_percentage' => 'required'
            ]);

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), 400);
            }

            $tenantSportsBetSetting = TenantSportsBetSetting::find($request->id);

            if(empty($tenantSportsBetSetting)) {
                return returnResponse(false, 'Setting Not found', [], Response::HTTP_NOT_FOUND);
            }

            $tenantSportsBetSetting->update([
                'event_liability' => $request->event_liability,
                'max_single_bet' => $request->max_single_bet,
                'min_bet' => $request->min_bet,
                'max_multiple_bet' => $request->max_multiple_bet,
//                'max_bet_on_event' => $request->max_bet_on_event,
                'deposit_limit' => $request->deposit_limit,
                'max_win_amount' => $request->max_win_amount,
                'max_odd' => $request->max_odd,
                'cashout_percentage' => $request->cashout_percentage,
                'bet_disabled' => $request->bet_disabled
            ]);

            return returnResponse(true, "Record updated Successfully", [], 200, true);

        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }
    
    
    public function registrationFieldUpdate(Request $request) {
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'tenant_registration_fields';
                $params->action = 'U';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          

            $validator = Validator::make($request->all(), [
                'tenantId' => 'required'
            ]);

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), 400);
            }

            $tenantRegistrationField = DB::table('tenant_user_registration_fields')->where('tenant_id',$request->tenantId)->first();
            $update = [];
            if($tenantRegistrationField){
                foreach ($request['phaseExecutions']['PRE'] as $key => $value) {
                  foreach ($value as $key1 => $value1) {
                    $update[$key1] = $value1; 
                  }
                }
                if(!empty($update)){
                  DB::table('tenant_user_registration_fields')->where('id',$tenantRegistrationField->id)->update($update);
                  return returnResponse(true, "Record updated Successfully", [], 200, true);

                }
                else{
                  return returnResponse(false, 'No Configuration Found! Try with update the tenant.', [], Response::HTTP_NOT_FOUND);
                }
            }else{
              return returnResponse(false, 'No Configuration Found! Try with update the tenant.', [], Response::HTTP_NOT_FOUND);
            }
         } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }


    /**
     * Update the specified resource in storage.  tenant_configurations,tenant_credentials,tenant_theme_settings we need to work
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Tenants $tenants
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tenants $tenants)
    {
        try {
            DB::beginTransaction();
            $tenants = $tenants->where('id', $request->id)->first();
            $tenantsArray = $tenants->toArray();
            if (!count($tenantsArray)) {
                return returnResponse(false, 'record Not found', [], 404, true);
            } else {

                $input = $request->all();

                $validatorArray = array();
                if (@$input['name'] != $tenantsArray['name']) {
                    $validatorArray['name'] = 'required';
                    $postData['name'] = @$input['name'];
                }
                if (@$input['domain'] != $tenantsArray['domain']) {
                    $validatorArray['domain'] = 'required|unique:tenants';
                    $postData['domain'] = @$input['domain'];
                }

                if (@$input['domain'] != $tenantsArray['domain']) {
                    $validatorArray['domain'] = 'required|unique:tenants';
                    $postData['domain'] = @$input['domain'];
                }


                if (count($validatorArray)) {
                    $validator = Validator::make($request->all(), $validatorArray);

                    if ($validator->fails()) {
                        return returnResponse(false, '', $validator->errors(), 400);
                    }
                }


                $postData['active'] = @$input['status'] ? @$input['status'] : false;
                Tenants::where(['id' => $request->id])->update($postData);


                // Create Registration form setting form
                $checkRegistrationFields = DB::table('tenant_user_registration_fields')->where('tenant_id',$request->id)->first();
                if(!$checkRegistrationFields){
                  $tenantRegistrationFormFieldArray = [
                    'tenant_id'=> $request->id,
                    "first_name" => VISIBLE_WITH_REQUIRED,
                    "last_name" => VISIBLE_WITH_REQUIRED,
                    "email" => ($request->loginMethod == 0 ? VISIBLE_WITH_REQUIRED : VISIBLE_WITH_OPTIONAL),
                    "phone" => ($request->loginMethod == 1 ? VISIBLE_WITH_REQUIRED : VISIBLE_WITH_OPTIONAL),
                    "username" => VISIBLE_WITH_REQUIRED,
                    "nick_name" => VISIBLE_WITH_REQUIRED,
                    "dob" => VISIBLE_WITH_REQUIRED,
                    "currency" => VISIBLE_WITH_REQUIRED,
                    "city" => VISIBLE_WITH_REQUIRED,
                    "zip_code" => VISIBLE_WITH_REQUIRED,
                    "password" => VISIBLE_WITH_REQUIRED,
                    "confirm_password" => VISIBLE_WITH_REQUIRED,
                    "promo_code" => NOT_VISIBLE,
                    "eighteen_year_check" => VISIBLE_WITH_REQUIRED,
                  ];
                  DB::table('tenant_user_registration_fields')->insert($tenantRegistrationFormFieldArray);
                }
                



                // $postDataTanants['allowed_currencies'] = '{' . implode(',', $request->currencies) . '}';
                if(!empty($request->currencies) && $request->id) {
                    $tenantCurrencyList = TenantConfigurations::where(['tenant_id' => $request->id])->first();
                    if(!$tenantCurrencyList){
                        return returnResponse(false,ERROR_MESSAGE , [], 200, true);
                    }


                    // $tenantCurrencyList['allowed_currencies'] = str_replace("{", "[", $tenantCurrencyList['allowed_currencies']);
                    // $tenantCurrencyList['allowed_currencies'] = str_replace("}", "]", $tenantCurrencyList['allowed_currencies']);

                    $exitingKey = explode(',',$tenantCurrencyList['allowed_currencies']);

                    if(!empty($exitingKey)){
                        $currencyList = implode(',', array_unique(array_merge($request->currencies,$exitingKey )));
                    }else{
                        $currencyList = implode(',', array_unique($request->currencies));
                    }
                    $postDataTanants = [];
                    $postDataTanants['allowed_currencies'] = $currencyList;


                    if($postDataTanants['allowed_currencies']!='')
                    TenantConfigurations::where(['tenant_id' => $request->id])->update($postDataTanants);
                }
                
                $postDataTanants = [];
                $postDataTanants['layout_id'] = $request->layout;
                $postDataTanants['language_id'] = $request->language;
                $postDataTanants['theme'] = $request->theme;
                $postDataTanants['whatsapp_number'] = (isset($request->whatsappNumber) ? $request->whatsappNumber : '');
                $postDataTanants['powered_by'] = (isset($request->poweredBy) ? $request->poweredBy : '');
                $postDataTanants['user_login_type'] = $request->loginMethod; // 1-> Mobile Base Login, 2-> Email Login Base
                $postDataTanants['sms_gateway'] = $request->smsGateway; // Twilio, Fast2SMS
                $postDataTanants['forgot_password_option'] = $request->forgotPasswordOption; // email, mobile
                $postDataTanants['assigned_providers'] = $request->assignedProvider;
                $postDataTanants['allowed_modules'] = $request->allowedModules;
                $postDataTanants['payment_providers'] = $request->assignedPaymentProvider;
                $assigned_providers_values = explode(',',$request->assignedProvider);
                $payment_providers_values = ($request->assignedPaymentProvider) ? explode(',',$request->assignedPaymentProvider) : null;
                CasinoItems::where('tenant_id',$request->id)->whereNotIn('provider', $assigned_providers_values)->update(['active'=>false]);
                CasinoItems::where('tenant_id',$request->id)->whereIn('provider', $assigned_providers_values)->update(['active'=>true]);
                $enableCasinoGame = CasinoItems::select('id')->where('tenant_id',$request->id)->whereIn('provider', $assigned_providers_values)->pluck('id')->toArray();
                MenuItems::whereIn('casino_item_id',$enableCasinoGame)->update(['active'=> true]);
                $disableCasinoGame = CasinoItems::select('id')->where('tenant_id',$request->id)->whereNotIn('provider', $assigned_providers_values)->pluck('id')->toArray();
                MenuItems::whereIn('casino_item_id',$disableCasinoGame)->update(['active'=> false]);
                  // dd($enableCasinoGame);
                $aws3 = new Aws3();
              if (@$request->file('logo')) {

              $fileName = Str::uuid() . '____' . $request->file('logo')->getClientOriginalName();
              $fileName = str_replace(' ', '_', $fileName);
              $fileNamePath = "tenants/" . $request->id . "/logo/" . $fileName;

              $PathS3Key = $aws3->uploadFile($fileNamePath, $request->file('logo')->getPathname(), $request->file('logo')->getClientMimeType());
              $postDataTanants['logo_url'] = @$PathS3Key;
              }

              if (@$request->file('fab_icon_url')) {
              // Fab Icon Upload

              $fileFabName = Str::uuid() . '____' . $request->file('fab_icon_url')->getClientOriginalName();
              $fileFabName = str_replace(' ', '_', $fileFabName);
              $fileFabNamePath = "tenants/" . $request->id . "/logo/" . $fileFabName;
              $fabIconPathS3Key = $aws3->uploadFile($fileFabNamePath, $request->file('fab_icon_url')->getPathname(), $request->file('fab_icon_url')->getClientMimeType());

              $postDataTanants['fab_icon_url'] = @$fabIconPathS3Key;
              }
               
              
              if (@$request->file('signup_popup_image_url')) {
              // Fab Icon Upload

              $fileFabName = Str::uuid() . '____' . $request->file('signup_popup_image_url')->getClientOriginalName();
              $fileFabName = str_replace(' ', '_', $fileFabName);
              $fileFabNamePath = "tenants/" . $request->id . "/logo/" . $fileFabName;
              $popupImagePathS3Key = $aws3->uploadFile($fileFabNamePath, $request->file('signup_popup_image_url')->getPathname(), $request->file('signup_popup_image_url')->getClientMimeType());

              $postDataTanants['signup_popup_image_url'] = @$popupImagePathS3Key;
              }


                
                

                $response = TenantThemeSettings::where(['tenant_id' => $request->id])->update($postDataTanants);
                DB::table('tenant_payment_configurations')->where(['tenant_id' => $request->id])->whereNotIn('provider_id', $payment_providers_values ?? [])->update(['active' => false]);
                $isSaveALl = true;
                DB::commit();

                return returnResponse(true, 'Update successfully.', [], 200, true);
            }

        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tenants $tenants
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $tenants = Tenants::where('id', $id)->get();
            $tenantsArray = $tenants->toArray();
            if (!count($tenantsArray)) {
                return returnResponse(false, 'record Not found', [], 404, true);
            } else {
                Tenants::where(['id' => $id])->update(['active' => false]);
                return returnResponse(true, 'deleted successfully.', [], 200, true);
            }
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Tenants $tenants
     * @param Tenants $tenants
     */
    public function getCredentials(Request $request)
    {

        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                    $params = new \stdClass();
                    $params->module = 'tenant_credentials';
                    $params->action = 'R';
                    $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);   
                    if(!$permission){ 
                        return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                    }
                    }
                    // ================= permissions ends here ===================== 
            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $id = Auth::user()->tenant_id;
            } else {
                $id = $request->id;
            }
            $record = TenantCredentials::select(DB::raw("ARRAY_TO_STRING(ARRAY_AGG(key), ',')as key"))->where('tenant_id', $id)->get();
            if (!is_null($record)) {
                $record = $record->toArray();
                $record = explode(',', $record[0]['key']);
            }
            $credentialsKeys = TenantCredentialsKeys::whereNotIn('name', $record)->get();
            return returnResponse(true, "Record get Successfully", $credentialsKeys);

        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }
    
    
    public function selectedPaymentProvider(Request $request)
    {

        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'tenant_payment_providers';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $id = Auth::user()->tenant_id;
            } else {
                $id = $request->id;
            }

             // payment provider data
             $data['paymentProvider'] = DB::connection('read_db')->table('tenant_payment_configurations as pc')->select('pc.*','pp.provider_name','pp.provider_type','pp.currencies','pp.active as provider_active')->where(['pc.tenant_id'=>$id])->leftJoin('payment_providers as pp','pp.id','=','pc.provider_id')->get();

             foreach ($data['paymentProvider'] as $value) {
                if($value->currencies){
                    $CurrenciesValue = Currencies::on('read_db')->select('id', 'name')->whereIn('id', json_decode($value->currencies))->get()->toArray();
                    $value->currencyList = $CurrenciesValue;
                }else{
                    $value->currencyList = [];
                }
            }

             if (!is_null($data['paymentProvider'])) {
                 $data['paymentProvider'] = $data['paymentProvider']->toArray();
                 foreach ($data['paymentProvider'] as $key => $value) {
                     $list = json_decode($value->provider_key_values,true);
                     $data['paymentProvider'][$key]->key_values = $list; 
                 }
             }
             $paymentProviders =  TenantThemeSettings::select('payment_providers')->where('tenant_id',$id)->pluck('payment_providers');
             $data['providers_disabled'] = explode(',',$paymentProviders[0]); 
            return returnResponse(true, "Record get Successfully", $data);

        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }
    
    
    public function getPaymentProvider(Request $request)
    {

        try {
               // ================= permissions starts here =====================
               if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'tenant_payment_providers';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                   $params->tenant_id = Auth::user()->tenant_id;

                   $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here ===================== 
            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $id = Auth::user()->tenant_id;
            } else {
                $id = $request->id;
            }

            $paymentProviders =  TenantThemeSettings::on('read_db')->select('payment_providers')->where('tenant_id',$id)->first();
            $allProvider =  PaymentProviders::where("active",true)->whereIn('id',explode(',', $paymentProviders['payment_providers'] ?? 0 ))->get();
            $data = [];
            
            $record = DB::connection('read_db')->table('tenant_payment_configurations as pc')->select('pc.provider_id')->where(['pc.tenant_id'=>$id])->pluck('provider_id')->toArray();
            if (!is_null($allProvider)) {
              foreach ($allProvider as $key => $value) {
                  if($value['id'] != $request->provider_id){
                    if(!in_array($value['id'],$record)){
                      $data[] = $value;
                    }
                  }else{
                    $data[] = $value;
                  }
                  
                }
              };
              
            return returnResponse(true, "Record get Successfully", $data);

        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }
   
   
    public function addPaymentProvider(Request $request)
    {

        try {
            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $created_by = 'AdminUser';
                $created_by_id = Auth::user()->tenant_id;
            } else {
                $created_by = 'SuperAdminUser';
                $created_by_id = Auth::user()->id;
            }

            
            $data = [];
            if(isset($request->tenant_id) && $request->tenant_id != ''){
              $data['tenant_id'] = $request->tenant_id;
            }else{
              
              $data['tenant_id'] = Auth::user()->tenant_id;
            }
            $data['provider_id'] = $request->paymentProvider;

            $data['parent_type'] = $created_by;
            $data['parent_id'] = $created_by_id;
           

            $mainData = $request->all();

            unset($mainData['tenant_id']);
            unset($mainData['id']);
            unset($mainData['status']);
            unset($mainData['paymentProvider']);
            unset($mainData['vip_level']);
            unset($mainData['min_deposit_limit']);
            unset($mainData['max_deposit_limit']);
            unset($mainData['description']);

            $data['provider_key_values'] = json_encode($mainData);
            $data['active'] = $request->status;
            $data['description'] = $request->description;
            $data['vip_level'] = json_encode($request->vip_level);
            $data['min_deposit_limit'] = $request->min_deposit_limit;
            $data['max_deposit_limit'] = $request->max_deposit_limit;
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            
            DB::table('tenant_payment_configurations')->insert($data);
           
            return returnResponse(true, "Record get Successfully");

        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }
    
    
    public function updatePaymentProvider(Request $request)
    {

        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'tenant_payment_providers';
                $params->action = 'U';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $created_by = 'AdminUser';
                $created_by_id = Auth::user()->tenant_id;
            } else {
                $created_by = 'SuperAdminUser';
                $created_by_id = Auth::user()->id;
            }

            
            $data = [];
            
            $data['provider_id'] = $request->paymentProvider;

            $data['parent_type'] = $created_by;
            $data['parent_id'] = $created_by_id;
           

            $mainData = $request->all();

            $id = $mainData['id'];
            
            unset($mainData['tenant_id']);
            unset($mainData['id']);
            unset($mainData['status']);
            unset($mainData['paymentProvider']);
            unset($mainData['vip_level']);
            unset($mainData['min_deposit_limit']);
            unset($mainData['max_deposit_limit']);
            unset($mainData['description']);

            $data['provider_key_values'] = json_encode($mainData);
            $data['active'] = $request->status;
            $data['description'] = $request->description;
            $data['vip_level'] = $request->vip_level;
            $data['min_deposit_limit'] = $request->min_deposit_limit;
            $data['max_deposit_limit'] = $request->max_deposit_limit;
            $data['updated_at'] = date('Y-m-d H:i:s');
            DB::table('tenant_payment_configurations')->where(["id"=>$id])->update($data);
           
            return returnResponse(true, "Record get Successfully");

        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }
  
    public function getPaymentProvidersById(Request $request)
    {

        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'tenant_payment_providers';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            

            $provider = PaymentProviders::where("active",true)->where("id",$request->id)->first();
           
            return returnResponse(true, "Record get Successfully", $provider);

        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    public function tenantPaymentProviderStatus(Request $request)
    {
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'tenant_payment_providers';
                $params->action = 'T';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            $providers = DB::table('tenant_payment_configurations')->where('id', $request->id)->get();
            $providersArray = $providers->toArray();
            if (!count($providersArray)) {
                return returnResponse(false, 'record Not found', [], 404, true);
            } else {
              DB::table('tenant_payment_configurations')->where(['id' => $request->id])->update(['active' => $request->status]);
                return returnResponse(true, 'Updated successfully.', [], 200, true);
            }
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Tenants $tenants
     * @param Tenants $tenants
     */
    public function setCredentials(Request $request)
    {
         // ================= permissions starts here =====================
         if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'tenant_credentials';
            $params->action = 'C';
            $params->admin_user_id = Auth::user()->id;
             $params->tenant_id = Auth::user()->tenant_id;

             $permission = checkPermissions($params);
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 

        $validationArray = [
            'key' => 'required',
            'keyvalue' => 'required',
//            'description' => 'required',

        ];

        if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
        } else {
            $validationArray['tenant_id'] = 'required';
        }

        $validator = Validator::make($request->all(), $validationArray);


        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), 400);
        }

        $input = $request->all();

        if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
            $input['tenant_id'] = Auth::user()->tenant_id;
        }
        //validating Key
        try {
            $record = TenantCredentials::select(DB::raw("ARRAY_TO_STRING(ARRAY_AGG(key), ',')as key"))->where('tenant_id', $input['tenant_id'])->get();
            if (!is_null($record)) {
                $record = $record->toArray();
                $record = explode(',', $record[0]['key']);
            }
            $credentialsKeys = TenantCredentialsKeys::select(DB::raw("ARRAY_TO_STRING(ARRAY_AGG(name), ',')as key"))->whereNotIn('name', $record)->get();
            if (!is_null($credentialsKeys)) {
                $record = $credentialsKeys->toArray();
                $credentialsKeys = explode(',', $credentialsKeys[0]['key']);
            }
            if (in_array($input['key'], $credentialsKeys)) {
                $postDataTanants['key'] = $input['key'];
            }


            $postDataTanants['value'] = $input['keyvalue'];
            $postDataTanants['tenant_id'] = $input['tenant_id'];
            $postDataTanants['description'] = $input['description'];

            $record = TenantCredentials::create($postDataTanants);

            if ($record) {
                return returnResponse(true, "Record Save Successfully", $input);
            } else {
                return returnResponse(false, "Record not save Successfully", $input);
            }

        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @param Request $request
     */
    public function updateCredentials($id, Request $request)
    {
        try {
               // ================= permissions starts here =====================
               if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'tenant_credentials';
                $params->action = 'U';
                $params->admin_user_id = Auth::user()->id;
                   $params->tenant_id = Auth::user()->tenant_id;

                   $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here ===================== 

            $recordUpdate = false;
            $input = $request->all();

            $validationArray = [
                'key' => 'required',
                'keyvalue' => 'required',
                'description' => 'required',

            ];

            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $input['tenant_id'] = Auth::user()->tenant_id;
            } else {
                $validationArray['tenant_id'] = 'required';
            }
            $validator = Validator::make($request->all(), $validationArray);
            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), 400);
            }

            $record = TenantCredentials::find($id);
            $postDataTanants = [];
            if ($record->value != $input['keyvalue']) {
                $postDataTanants['value'] = $input['keyvalue'];
            }

            if ($record->description != $input['description']) {
                $postDataTanants['description'] = $input['description'];
            }

//            DB::enableQueryLog();
            if (count($postDataTanants)) {
                $recordUpdate = TenantCredentials::where(['tenant_id' => $input['tenant_id'], 'key' => $input['key']])
                    ->update($postDataTanants);
            }
//            $quries = DB::getQueryLog();

            if ($recordUpdate) {
                return returnResponse(true, MESSAGE_UPDATED, $input);
            } else {
                return returnResponse(false, MESSAGE_NOTHING, $input);
            }
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteCredentials($id)
    {

        if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
            $tenant_id = Auth::user()->tenant_id;
        }
        try {
            $tenants = TenantCredentials::where('id', $id)->where('tenant_id', $tenant_id)->get();
            $tenantsArray = $tenants->toArray();
            if (!count($tenantsArray)) {
                return returnResponse(false, 'record Not found', [], 404, true);
            } else {
                TenantCredentials::where(['id' => $id])->delete();
                return returnResponse(true, 'deleted successfully.', [], 200, true);
            }
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @param Request $request
     * @param Tenants $tenants
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateThemeSetting(Request $request, Tenants $tenants)
    {
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'tenant_theme_settings';
                $params->action = 'U';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $tenant_id = Auth::user()->tenant_id;
            } else {
                return returnResponse(false, 'Permission Denied', [], Response::HTTP_FORBIDDEN);
            }

            $tenantConfigurations = TenantConfigurations::where(['tenant_id' => $tenant_id])->first();

            if (!count($tenantConfigurations->toArray())) {
                return returnResponse(false, 'record Not found', [], Response::HTTP_NOT_FOUND, true);
            } else {

                $postDataTanants = [];
                $postDataTanants['layout_id'] = $request->layout;
                $postDataTanants['whatsapp_number'] = (isset($request->whatsappNumber) ? $request->whatsappNumber : '');
                $postDataTanants['powered_by'] = (isset($request->poweredBy) ? $request->poweredBy : '');
                $postDataTanants['language_id'] = $request->language;
                $postDataTanants['theme'] = $request->theme;
                $aws3 = new Aws3();
                if (@$request->file('logo')) {

                    

                    $fileName = Str::uuid() . '____' . $request->file('logo')->getClientOriginalName();
                    $fileName = str_replace(' ', '_', $fileName);
                    $fileNamePath = "tenants/" . $tenant_id . "/logo/" . $fileName;

                    $PathS3Key = $aws3->uploadFile($fileNamePath, $request->file('logo')->getPathname(), $request->file('logo')->getClientMimeType());
                    $postDataTanants['logo_url'] = @$PathS3Key;
                }

                if (@$request->file('fab_icon_url')) {
                  // Fab Icon Upload

                  $fileFabName = Str::uuid() . '____' . $request->file('fab_icon_url')->getClientOriginalName();
                  $fileFabName = str_replace(' ', '_', $fileFabName);
                  $fileFabNamePath = "tenants/" . $tenant_id . "/logo/" . $fileFabName;
                  $fabIconPathS3Key = $aws3->uploadFile($fileFabNamePath, $request->file('fab_icon_url')->getPathname(), $request->file('fab_icon_url')->getClientMimeType());

                  $postDataTanants['fab_icon_url'] = @$fabIconPathS3Key;
              }


              if (@$request->file('signup_popup_image_url')) {
                // Fab Icon Upload

                $fileFabName = Str::uuid() . '____' . $request->file('signup_popup_image_url')->getClientOriginalName();
                $fileFabName = str_replace(' ', '_', $fileFabName);
                $fileFabNamePath = "tenants/" . $request->id . "/logo/" . $fileFabName;
                $popupImagePathS3Key = $aws3->uploadFile($fileFabNamePath, $request->file('signup_popup_image_url')->getPathname(), $request->file('signup_popup_image_url')->getClientMimeType());

                $postDataTanants['signup_popup_image_url'] = @$popupImagePathS3Key;
            }

                TenantThemeSettings::where(['tenant_id' => $tenant_id])->update($postDataTanants);


                return returnResponse(true, 'Update successfully.', [], Response::HTTP_OK, true);
            }
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }


    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addBanners(Request $request)
    {
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'tenant_banners';
                $params->action = 'C';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $tenant_id = Auth::user()->tenant_id;
            } else {
                return returnResponse(false, 'Permission Denied', [], Response::HTTP_FORBIDDEN);
            }

            $validator = Validator::make($request->all(), [
                'name' => 'required',
//                'image' => 'required',//image_url
                // 'redirect_url' => 'required',
                'banner_type' => 'required',
//                'order' => 'required',
                'enabled' => 'required',

            ]);
            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
            }


            $postData = [];
            $postData['name'] = $request->name;
            $postData['redirect_url'] = ($request->redirect_url != '' ? $request->redirect_url : '');
            $postData['banner_type'] = $request->banner_type;
//                $postData['order'] = $request->order;
            $postData['enabled'] = $request->enabled;
            $postData['tenant_id'] = $tenant_id;
            $postData['game_name'] = $request->game_name;
            $postData['open_table'] = $request->open_table;
            $postData['provider_name'] = $request->provider_name;

            if (@$request->file('image')) {

                $aws3 = new Aws3();

                $fileName = Str::uuid() . '____' . $request->file('image')->getClientOriginalName();
                $fileName = str_replace(' ', '_', $fileName);
                $fileNamePath = "tenants/" . $tenant_id . "/banners/" . $fileName;

                $PathS3Key = $aws3->uploadFile($fileNamePath, $request->file('image')->getPathname(), $request->file('image')->getClientMimeType());
                $postData['image_url'] = @$PathS3Key;
            }

            PageBanners::create($postData);


            return returnResponse(true, 'Create successfully.', [], Response::HTTP_CREATED, true);
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBanners(Request $request)
    {
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'tenant_banners';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $tenant_id = Auth::user()->tenant_id;
            } else {
                return returnResponse(false, 'Permission Denied', [], Response::HTTP_FORBIDDEN);
            }
            $aws3 = new Aws3();


            $pageBanners = PageBanners::where('tenant_id', $tenant_id)->get();
//            $pageBannersArray = [];
            /*if ($pageBanners) {
                foreach ($pageBanners->toArray() as $key => $value) {//du($value);
                    $value['url'] = Aws3::getAwsUrl($value['image_url']);
                    $pageBannersArray[] = $value;
                }
            }*/
            
            return returnResponse(true, 'get record list successfully.', $pageBanners, Response::HTTP_OK, true);

        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }


    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateBanners(Request $request)
    {
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'tenant_banners';
                $params->action = 'U';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $tenant_id = Auth::user()->tenant_id;
            } else {
                return returnResponse(false, 'Permission Denied', [], Response::HTTP_FORBIDDEN);
            }

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                // 'image' => 'required',//image_url
                // 'redirect_url' => 'required',
                'banner_type' => 'required',
//                'order' => 'required',
                'enabled' => 'required',
                'id' => 'required',

            ]);
            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
            }

            $postData = [];
            $postData['name'] = $request->name;
            $postData['redirect_url'] = $request->redirect_url;
            $postData['banner_type'] = $request->banner_type;
            $postData['order'] = $request->order;
            $postData['enabled'] = $request->enabled;
            $postData['tenant_id'] = $tenant_id;
            $postData['game_name'] = $request->game_name;
            $postData['open_table'] = $request->open_table;
            $postData['provider_name'] = $request->provider_name;

            if (@$request->file('image')) {

                $aws3 = new Aws3();

                $fileName = Str::uuid() . '____' . $request->file('image')->getClientOriginalName();
                $fileName = str_replace(' ', '_', $fileName);
                $fileNamePath = "tenants/" . $tenant_id . "/banners/" . $fileName;

                $PathS3Key = $aws3->uploadFile($fileNamePath, $request->file('image')->getPathname(), $request->file('image')->getClientMimeType());
                $postData['image_url'] = @$PathS3Key;
            }

            PageBanners::where('id', $request->id)->update($postData);


            return returnResponse(true, 'Update successfully.', [], Response::HTTP_OK, true);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }


    public function updateBannersOrders(Request $request) {
        if(empty($request->orders)) {
            return returnResponse(false, 'Orders is required', [], 403,true);
        }

        foreach($request->orders as $key => $id) {
            PageBanners::find($id)->update(['order' => $key+1]);
        }

        return returnResponse(true, 'Update successfully.', [], Response::HTTP_OK,true);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteBanners($id)
    {
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'tenant_banners';
                $params->action = 'D';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $tenant_id = Auth::user()->tenant_id;
            } else {
                return returnResponse(false, 'Permission Denied', [], Response::HTTP_FORBIDDEN);
            }
            PageBanners::where('id', $id)->delete();

            return returnResponse(true, 'record deleted successfully.', [], Response::HTTP_OK, true);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @description sportsStatus
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sportsStatus(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'pulls_sport_id' => 'required',
                'tenant_id' => 'required',


            ]);

            $tenant_id = $request->tenant_id;
            $pulls_sport_id = $request->pulls_sport_id;

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), 400);
            };
            $sportsData = TenantBlockedSports::getStatus($tenant_id, $pulls_sport_id, Auth::user()->id);

            if ($sportsData[0]->is_deleted == 0) {
                $data = ['is_deleted' => 't'];
            } else {
                $data = ['is_deleted' => 'f'];
            }


            TenantBlockedSports::where('id', $sportsData[0]->id)->update($data);

            return returnResponse(true, MESSAGE_UPDATED, [], Response::HTTP_OK, true);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @description reportsStatus
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reportsStatus(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'report_id' => 'required',
                'tenant_id' => 'required',
                'status' => 'required'
            ]);

            $tenant_id = $request->tenant_id;
            $report_id = $request->report_id;
            $status = $request->status;

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), 400);
            };
            if (!in_array($status, ["enable", "disable"])) {
                return returnResponse(false, '', ["status" => [
                    "only accept  enable/disable."
                ]], Response::HTTP_BAD_REQUEST);
            }
            $TenantReportConfigurations = TenantReportConfigurations::where(['report_id' => $report_id, 'tenant_id' => $tenant_id])->first();

            if ($status == 'disable') {

                if (is_null($TenantReportConfigurations)) {
                    return returnResponse(false, MESSAGE_RECORD_NOT_FOUND, [], Response::HTTP_NOT_FOUND, true);
                } else {

                    $insertArray = ['report_id' => $report_id, 'tenant_id' => $tenant_id];
                    TenantReportConfigurations::create($insertArray);
                }
                TenantReportConfigurations::where(['report_id' => $report_id, 'tenant_id' => $tenant_id])->delete();

            } elseif ($status == 'enable') {

                if (is_null($TenantReportConfigurations)) {
                    $insertArray = ['report_id' => $report_id, 'tenant_id' => $tenant_id];
                    TenantReportConfigurations::create($insertArray);
                } else {
                    return returnResponse(false, MESSAGE_ALREADY_ENABLE, [], Response::HTTP_ALREADY_REPORTED, true);
                }

            }
            return returnResponse(true, MESSAGE_UPDATED, [], Response::HTTP_OK, true);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @description updateTenantMenuSetting
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateTenantMenuSetting(Request $request){
        try {

            $validator = Validator::make($request->all(), [
                'menu_id' => 'required',
                'selected' => 'required',
                'tenant_id' => 'required',

            ]);

            $tenant_id = $request->tenant_id;
            $menu_id = $request->menu_id;
            $selected = (boolean)$request->selected;

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), 400);
            };
            $sportsData = MenuTenantSettingModel::select('id')->where(['tenant_id' =>$tenant_id,'menu_id'=>$menu_id])->get();
            if($selected === true){
               $result = $sportsData->toArray();
               if(count($result)<=0){
                   MenuTenantSettingModel::create(['tenant_id' =>$tenant_id,'menu_id'=>$menu_id]);
               }
            }else{
                $sportsDataAll = MenuTenantSettingModel::where(['tenant_id' =>$tenant_id])->count();
                if($sportsDataAll>1){
                    if(count($sportsData->toArray())>0){
                        MenuTenantSettingModel::where(['tenant_id' =>$tenant_id,'menu_id'=>$menu_id])->delete();
                    }
                } else {
                    return returnResponse(false, MESSAGE_UPDATED_MINIMUM, [], Response::HTTP_EXPECTATION_FAILED, true);
                }
            }
            return returnResponse(true, MESSAGE_UPDATED, [], Response::HTTP_OK, true);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }


    public function getTenantMenuSetting(Request $request){
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'tenant_menu_setting';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    $params1 = new \stdClass();
                    $params1->module = 'casino_management';
                    $params1->action = 'R';
                    $params1->admin_user_id = Auth::user()->id;
                    $permission1 = checkPermissions($params1);
                    if(!$permission1){
                        return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                    }
                }
                }
                // ================= permissions ends here =====================          
          
            $tenant_id = Auth::user()->tenant_id;
            $sportsData = MenuTenantSettingModel::select(
                ['menu_tenant_setting.id',
                    'ordering',
                    'menu_id',
                    DB::raw('mm.name  as menu_name'),
                    DB::raw('mm.path  as menu_path')
                ])
                ->leftjoin('menu_master as mm','mm.id','menu_tenant_setting.menu_id')
                ->where(['tenant_id' =>$tenant_id,"active"=>true])
//                ->orderByDesc('ordering')
                ->get();
            return returnResponse(true, MESSAGE_GET_SUCCESS, $sportsData, Response::HTTP_OK, true);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @description updateMenuOrders
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateMenuOrders(Request $request){
        try {
            $order = $request->orders;
            if(count($order)>0){
                $k=0;
                foreach ($order as $key=>$value){
                    $id = (int)$value;
                    $k++;

                    MenuTenantSettingModel::where('id',$id)->update(['ordering'=>$k]);
                }
            }

            return returnResponse(true, MESSAGE_UPDATED, [], Response::HTTP_OK, true);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }
    /**
     * @description updatePermission
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function updatePermission(Request $request){
        $validator = Validator::make($request->all(), [
            'tenant_id' => 'required',
            'permission' => 'required',
        ]);

        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), 400);
        }

        try {
            $createdValues = TenantPermission::updateOrCreate(
            [
                'tenant_id' => $request->tenant_id,
            ],
            [
                'permission' => $request->permission
            ]);

            if ($createdValues) {

                // change permission of permission roles if tenant permission changes
                $permissionRoles = PermissionRole::where('tenant_id', $request->tenant_id)->get();

                foreach ($permissionRoles as $role) {
                    $existingPermissions = json_decode($role->permission,true);
                    $tenantPermissions = json_decode($request->permission,true);
                    if (is_array($existingPermissions) && is_array($tenantPermissions)) {
                        $newPermissions = [];
                
                        foreach ($tenantPermissions as $key => $values) {
                            if (isset($existingPermissions[$key]) && is_array($values) && is_array($existingPermissions[$key])) {
                                $commonValues = array_intersect($values, $existingPermissions[$key]);
                                
                                if (!empty($commonValues)) {
                                    $newPermissions[$key] = array_values($commonValues);
                                }
                            }
                        }
                
                        $role->update(['permission' => json_encode($newPermissions)]);
                    }
                }
                return returnResponse(true, 'permission updated successfully.', $createdValues, 200, true);
            } else {
                return returnResponse(false, 'permission updatation failed.', [], 403, true);
            }
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @description updatePlayerCommissionInterval
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

     public function updatePlayerCommissionInterval(Request $request){
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'tenant_configurations';
            $params->action = 'T';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
              return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
          }
          // ================= permissions ends here =====================  

        $validator = Validator::make($request->all(), [
            'interval' => 'required',
        ]);

        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), 400);
        }

        try {
            $createdValues = DB::table('tenant_configurations')->where('tenant_id',$request->tenant_id ?? Auth::user()->tenant_id)->update(
            [
                'player_commission_interval' => $request->interval
            ]);

            if ($createdValues) {
                return returnResponse(true, 'player commission interval updated successfully.', $createdValues, 200, true);
            } else {
                return returnResponse(false, 'player commission interval updatation failed.', [], 403, true);
            }
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function getPlayerCommissionInterval() {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'tenant_configurations';
            $params->action = 'R';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
              return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
          }
          // ================= permissions ends here =====================  
        try {

            $playerCommissionInterval = TenantConfigurations::on('read_db')->select('player_commission_interval')->where('tenant_id', Auth::user()->tenant_id)->first();

            if($playerCommissionInterval) {
                return returnResponse(true, "Record get Successfully", $playerCommissionInterval, 200, true);
            }
            
            return returnResponse(false, 'Player Commission Interval Not found', [], Response::HTTP_NOT_FOUND);
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function updateGoogleSetting(Request $request){
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'tenant_configurations';
            $params->action = 'U';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
              return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
          }
          // ================= permissions ends here =====================  

        try {
            $createdValues = DB::table('tenant_theme_settings')->where('tenant_id',$request->tenant_id ?? Auth::user()->tenant_id)->update(
                $request->all()
            );

            if ($createdValues) {
                return returnResponse(true, 'Tenant Theme Settings updated successfully.', $createdValues, 200, true);
            } else {
                return returnResponse(false, 'Tenant Theme Settings updatation failed.', [], 403, true);
            }
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function changePassword(Request $request){
        try {
        $messages = array(
            'old_password.required' => 'SENTENCES.IMAGE_REQUIRED',
            'confirm_password.required' => 'SENTENCES.IMAGE_REQUIRED',
        );

        $validator = Validator::make($request->all(), [
            'confirm_password' => 'required',
            'old_password' => 'required',
        ], $messages);
        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
        }        
        $credentials = [
            'email' => Auth::user()->email,
            'password' => base64_decode($request->old_password),
        ];

        if (base64_decode($request->old_password) == base64_decode($request->confirm_password)) {
            return returnResponse(false, '', 'Old password and new password should not match', Response::HTTP_BAD_REQUEST);
        }
        if (Auth::guard('admin')->attempt($credentials)){
            $password = bcrypt(base64_decode($request->confirm_password));
            $res = Admin::where('id', Auth::id())->update(['encrypted_password'=>  $password]);
        }else{
            return returnResponse(false, 'Old password is wrong', [], Response::HTTP_BAD_REQUEST);
        }
        if($res){
            return returnResponse(true, 'Password changed successfully', [], Response::HTTP_OK);
        }else{
            return returnResponse(false, 'Password not changed', [], Response::HTTP_OK);
        }
    } catch (\Exception $e) {

        if (!App::environment(['local'])) {//, 'staging'
            return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
        } else {
            return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                'LineNo' => $e->getLine(),
                'FileName' => $e->getFile()
            ], Response::HTTP_EXPECTATION_FAILED);
        }
    }
    }


    public function getSocialmedia(Request $request){
      
        try {
            $record = TenantSocialMedia::where('tenant_id', Auth::user()->tenant_id)->get();
            return returnResponse(true, "Record get Successfully", $record);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function createSocialmedia(Request $request){
        try {
            DB::beginTransaction();
        $messages = array(
            'name.required' => 'Name is required',
            'redirect_url.required' => 'Redirect url is required',
            'image.required' => 'Image is required',
        );

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'redirect_url' => 'required',
            'image' => 'required',
        ], $messages);
        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
        }        
        if (@$request->file('image')) {
            $aws3 = new Aws3();
            $fileFabName = Str::uuid() . '____' . $request->file('image')->getClientOriginalName();
            $fileFabName = str_replace(' ', '_', $fileFabName);
            $fileFabNamePath = "social_media/"."/logo/".$fileFabName;
            $socialImagePathS3Key = $aws3->uploadFile($fileFabNamePath, $request->file('image')->getPathname(), $request->file('image')->getClientMimeType());
        }
        $insertData = [
            'name' => $request->name,
            'redirect_url' => $request->redirect_url,
            'image' => $socialImagePathS3Key,
            'tenant_id' => Auth::user()->tenant_id
        ];

        $createdValues = TenantSocialMedia::create($insertData);
        DB::commit();
        if($createdValues){
            return returnResponse(true, 'Social media created successfully', [], Response::HTTP_OK);
        }else{
            DB::rollback();
            return returnResponse(false, 'Social media not created', [], Response::HTTP_OK);
        }
    } catch (\Exception $e) {
        DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function updateSocialmedia(Request $request){
        try {
        $messages = array(
            'id.required' => 'Id is required',
            'name.required' => 'Name is required',
            'redirect_url.required' => 'Redirect url is required',
        );

        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'name' => 'required',
            'redirect_url' => 'required',
        ], $messages);
        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
        }        
        if (@$request->file('image')) {
            $aws3 = new Aws3();
            $fileFabName = Str::uuid() . '____' . $request->file('image')->getClientOriginalName();
            $fileFabName = str_replace(' ', '_', $fileFabName);
            $fileFabNamePath = "social_media/"."/logo/".$fileFabName;
            $socialImagePathS3Key = $aws3->uploadFile($fileFabNamePath, $request->file('image')->getPathname(), $request->file('image')->getClientMimeType());
            $updateData = [
                'name' => $request->name,
                'redirect_url' => $request->redirect_url,
                'image' => isset($socialImagePathS3Key) ? $socialImagePathS3Key : null,
                'tenant_id' => Auth::user()->tenant_id
            ];
        }else{
            $updateData = [
                'name' => $request->name,
                'redirect_url' => $request->redirect_url,
                'tenant_id' => Auth::user()->tenant_id
            ];
        }

        $updatedValues = TenantSocialMedia::where('id', $request->id)->where('tenant_id', Auth::user()->tenant_id)->update($updateData);
        if($updatedValues){
            return returnResponse(true, 'Social media updated successfully', [], Response::HTTP_OK);
        }else{

            return returnResponse(false, 'Social media not updated', [], Response::HTTP_OK);
        }
    } catch (\Exception $e) {
        if (!App::environment(['local'])) {//, 'staging'
            return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
        } else {
            return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                'LineNo' => $e->getLine(),
                'FileName' => $e->getFile()
            ], Response::HTTP_EXPECTATION_FAILED);
        }
    }
    }

    public function activateSocialMedia($id){
        try {
        $updatedValues = TenantSocialMedia::where('id', $id)->where('tenant_id', Auth::user()->tenant_id)->update(['status'=>true]);
        if($updatedValues){
            return returnResponse(true, 'Social media status updated successfully', [], Response::HTTP_OK);
        }else{
            return returnResponse(false, 'Social media status not updated', [], Response::HTTP_OK);
        }
    } catch (\Exception $e) {
        if (!App::environment(['local'])) {//, 'staging'
            return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
        } else {
            return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                'LineNo' => $e->getLine(),
                'FileName' => $e->getFile()
            ], Response::HTTP_EXPECTATION_FAILED);
        }
    }
    }

    public function deactivateSocialMedia($id){
        try {
        $updatedValues = TenantSocialMedia::where('id', $id)->where('tenant_id', Auth::user()->tenant_id)->update(['status'=>false]);
        if($updatedValues){
            return returnResponse(true, 'Social media status updated successfully', [], Response::HTTP_OK);
        }else{
            return returnResponse(false, 'Social media status not updated', [], Response::HTTP_OK);
        }
    } catch (\Exception $e) {
        if (!App::environment(['local'])) {//, 'staging'
            return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
        } else {
            return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                'LineNo' => $e->getLine(),
                'FileName' => $e->getFile()
            ], Response::HTTP_EXPECTATION_FAILED);
        }
    }
    }

    public function getPaymentPorviders(){
        try {
        $paymentProviders = PaymentProviders::on('read_db')->get();
        if($paymentProviders){
            return returnResponse(true, 'Payment providers fetched successfully', $paymentProviders, Response::HTTP_OK);
        }else{
            return returnResponse(false, 'Payment providers status not updated', [], Response::HTTP_OK);
        }
    } catch (\Exception $e) {

        if (!App::environment(['local'])) {//, 'staging'
            return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
        } else {
            return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                'LineNo' => $e->getLine(),
                'FileName' => $e->getFile()
            ], Response::HTTP_EXPECTATION_FAILED);
        }
    }
        
    }

    public function getFaqs(Request $request){
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                    $params = new \stdClass();
                    $params->module = 'faq_management';
                    $params->action = 'R';
                    $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;
            
                    $permission = checkPermissions($params);   
                    if(!$permission){ 
                        return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                    }
                    }
                    // ================= permissions ends here =====================          
        $faqs = Faq::on('read_db')->where(function ($q) use ($request) {
            if ($request->search && !empty($request->search)) {
                $search = $request->search;
                $q->orWhere('question', 'ilike', '%' . strtolower($search) . '%');
            }
            if (!empty($request->date_range['start_date']) && !empty($request->date_range['end_date'])) {
                $start = Carbon::parse($request->date_range['start_date']);
                $end = Carbon::parse($request->date_range['end_date']);

                $q->whereBetween('created_at', [$start, $end]);
            }

        })->where('tenant_id', Auth::user()->tenant_id)
        ->select('*', DB::raw('(SELECT name FROM faq_categories WHERE id = faqs.category_id) as category_name'))
        ->orderBy('created_at', $request->created_at ? $request->created_at: 'DESC' )->paginate($request->size ?? 10);
        if($faqs){
            return returnResponse(true, 'Faqs fetched successfully', $faqs, Response::HTTP_OK);
        }else{
            return returnResponse(false, 'Unable to fetch FAQs , Please try again later in sometime', [], Response::HTTP_OK);
        }
    }catch (\Exception $e) {
        if (!App::environment(['local'])) {//, 'staging'
            return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
        } else {
            return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                'LineNo' => $e->getLine(),
                'FileName' => $e->getFile()
            ], Response::HTTP_EXPECTATION_FAILED);
        }
    }
        
    }

    public function createFaq(Request $request){
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                    $params = new \stdClass();
                    $params->module = 'faq_management';
                    $params->action = 'C';
                    $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;
            
                    $permission = checkPermissions($params);   
                    if(!$permission){ 
                        return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                    }
                    }
                    // ================= permissions ends here =====================        
        $messages = array(
            'question.required' => 'Question is required',
            'answer.required' => 'Answer is required',
        );
        $validator = Validator::make($request->all(), [
            'question' => 'required',
            'answer' => 'required',
        ], $messages);
        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
        }
        $insertData = [
            'question' => $request->question,
            'answer' => json_encode($request->answer),
            'tenant_id' => Auth::user()->tenant_id,
            'active'=> $request->active,
            'featured'=> $request->featured,
            'created_by' => Auth::id(),
            'category_id' => $request->category_id
        ];

        $createdValues = Faq::create($insertData);
        if($createdValues){
            return returnResponse(true, 'Faq created successfully', [], Response::HTTP_OK);
        }else{
            return returnResponse(false, 'Unable to create FAQ , Please try again later in sometime', [], Response::HTTP_OK);
        }
    }catch (\Exception $e) {
        if (!App::environment(['local'])) {//, 'staging'
            return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
        } else {
            return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                'LineNo' => $e->getLine(),
                'FileName' => $e->getFile()
            ], Response::HTTP_EXPECTATION_FAILED);
        }
    }
        
    }

    public function updateFaq(Request $request, $id){
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                    $params = new \stdClass();
                    $params->module = 'faq_management';
                    $params->action = 'U';
                    $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;
            
                    $permission = checkPermissions($params);   
                    if(!$permission){ 
                        return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                    }
                    }
                    // ================= permissions ends here =====================  
        $messages = array(
            'question.required' => 'Question is required',
            'answer.required' => 'Answer is required',
        );
        $validator = Validator::make($request->all(), [
            'question' => 'required',
            'answer' => 'required',
        ], $messages);
        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
        }
        $updateData = [
            'question' => $request->question,
            'answer' => $request->answer,
            'tenant_id' => Auth::user()->tenant_id,
            'active'=> $request->active,
            'featured'=> $request->featured,
            'created_by' => Auth::id(),
            'category_id' => $request->category_id
        ];

        $createdValues = Faq::where('id', $id)->update($updateData);
        if($createdValues){
            return returnResponse(true, 'Faq updated successfully', [], Response::HTTP_OK);
        }else{
            return returnResponse(false, 'Unable to update FAQ , Please try again later in sometime', [], Response::HTTP_OK);
        }
    }catch (\Exception $e) {
        if (!App::environment(['local'])) {//, 'staging'
            return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
        } else {
            return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                'LineNo' => $e->getLine(),
                'FileName' => $e->getFile()
            ], Response::HTTP_EXPECTATION_FAILED);
        }
    }
        
    }

    public function getFaq($id){
        try {
        $faq = Faq::on('read_db')->where('id',$id)->where('tenant_id', Auth::user()->tenant_id)->get();
        if($faq){
            return returnResponse(true, 'Faq fetched successfully', $faq, Response::HTTP_OK);
        }else{

            return returnResponse(false, 'Unable to fetch FAQ , Please try again later in sometime', [], Response::HTTP_OK);
        }
    }catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
        
    }

    public function deleteFaq(Request $request){
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                    $params = new \stdClass();
                    $params->module = 'faq_management';
                    $params->action = 'D';
                    $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;
            
                    $permission = checkPermissions($params);   
                    if(!$permission){ 
                        return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                    }
                    }
                    // ================= permissions ends here =====================             
        $faq = Faq::where('id',$request->id)->where('tenant_id', Auth::user()->tenant_id)->delete();
        if($faq){
            return returnResponse(true, 'Faq deleted successfully', [], Response::HTTP_OK);
        }else{
            return returnResponse(false, 'Unable to delete FAQ , Please try again later in sometime', [], Response::HTTP_OK);
        }
    } catch (\Exception $e) {
        if (!App::environment(['local'])) {//, 'staging'
            return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
        } else {
            return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                'LineNo' => $e->getLine(),
                'FileName' => $e->getFile()
            ], Response::HTTP_EXPECTATION_FAILED);
        }
    }
    }

    public function updateFaqStatus(Request $request)
    {
       
        try {
               // ================= permissions starts here =====================
               if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'faq_management';
                $params->action = 'T';
                $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;
        
                $permission = checkPermissions($params);   
                if(!$permission){ 
                  return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
              }
              // ================= permissions ends here =====================          
              if ($request->id != '') {
                  Faq::where('id',$request->id)->update(["active"=>$request->status]);
              return returnResponse(true, "Faq status Updated Successfully");
            } else {
                return returnResponse(false, "Unable to update FAQ , Please try again later in sometime", [], 500);
            }
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function getFaqsCategories(Request $request){
        try {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'faq_categories';
            $params->action = 'R';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;
    
            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here =====================          
$faqs = FaqCategory::on('read_db')->where(function ($q) use ($request) {
    if ($request->search && !empty($request->search)) {
        $search = $request->search;
        $q->orWhere('name', 'ilike', '%' . strtolower($search) . '%')
        ->orWhere('slug', 'ilike', '%' . strtolower($search) . '%');
    }
    if (!empty($request->date_range['start_date']) && !empty($request->date_range['end_date'])) {
        $start = Carbon::parse($request->date_range['start_date']);
        $end = Carbon::parse($request->date_range['end_date']);

        $q->whereBetween('created_at', [$start, $end]);
    }

})->where('tenant_id', Auth::user()->tenant_id)->orderBy('created_at', $request->created_at ? $request->created_at: 'DESC' )->paginate($request->size ?? 10);
if($faqs){
    return returnResponse(true, 'Faq categories fetched successfully', $faqs, Response::HTTP_OK);
}else{
    return returnResponse(false, 'Unable to fetch FAQ category, Please try again later in sometime', [], Response::HTTP_OK);
}

}catch (\Exception $e) {
    if (!App::environment(['local'])) {//, 'staging'
        return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
    } else {
        return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
            'LineNo' => $e->getLine(),
            'FileName' => $e->getFile()
        ], Response::HTTP_EXPECTATION_FAILED);
    }
}

}

public function createFaqsCategories(Request $request){
    try {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'faq_categories';
            $params->action = 'C';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;
    
            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here =====================  
    $messages = array(
        'name.required' => 'Name is required',
        'name.regex' => 'Name must only contain letters, spaces, and underscores',
        'slug.required' => 'Slug is required',
        'slug.regex' => 'Slug must be valid',
        'image.required' => 'Image is required',
    );

    $validator = Validator::make($request->all(), [
        'name' => ['required','regex:/^[a-zA-Z_ ]+$/'],
        'slug' => ['required','regex:/^[a-z]+$/', new UniqueSlug],
        'image' => 'required',
    ], $messages);
    if ($validator->fails()) {
        return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
    }        
    $data = $request->all();

    if (@$request->file('image')) {
        $aws3 = new Aws3();
        $fileFabName = Str::uuid() . '____' . $request->file('image')->getClientOriginalName();
        $fileFabName = str_replace(' ', '_', $fileFabName);
        $fileFabNamePath = "faq-category/"."/logo/".$fileFabName;
        $faqImagePathS3Key = $aws3->uploadFile($fileFabNamePath, $request->file('image')->getPathname(), $request->file('image')->getClientMimeType());
    }
    $insertData = [
        'name' => $request->name,
        'slug' => strtolower($request->slug),
        'active' => $request->active,
        'image' => $faqImagePathS3Key,
        'tenant_id' => Auth::user()->tenant_id,
        'created_by' => Auth::user()->id,
    ];

    $createdValues = FaqCategory::create($insertData);
    if($createdValues){
        return returnResponse(true, 'Faq category created successfully', [], Response::HTTP_OK);
    }else{
        return returnResponse(false, 'Unable to create FAQ category, Please try again later in sometime', [], Response::HTTP_OK);
    }
} catch (\Exception $e) {
    if (!App::environment(['local'])) {//, 'staging'
        return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
    } else {
        return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
            'LineNo' => $e->getLine(),
            'FileName' => $e->getFile()
        ], Response::HTTP_EXPECTATION_FAILED);
    }
}

}

public function getFaqsCategory($id){
    try {
    $faqCategory = FaqCategory::on('read_db')->where('id',$id)->where('tenant_id', Auth::user()->tenant_id)->get();
    if($faqCategory){
        return returnResponse(true, 'Faq category fetched successfully', $faqCategory, Response::HTTP_OK);
    }else{
        return returnResponse(false, 'Unable to fetch FAQ category, Please try again later in sometime', [], Response::HTTP_OK);
    }
} catch (\Exception $e) {
    if (!App::environment(['local'])) {//, 'staging'
        return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
    } else {
        return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
            'LineNo' => $e->getLine(),
            'FileName' => $e->getFile()
        ], Response::HTTP_EXPECTATION_FAILED);
    }
}
    
}

public function updateFaqsCategories(Request $request){
    try {
    // ================= permissions starts here =====================
    if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
        $params = new \stdClass();
        $params->module = 'faq_categories';
        $params->action = 'U';
        $params->admin_user_id = Auth::user()->id;
        $params->tenant_id = Auth::user()->tenant_id;

        $permission = checkPermissions($params);   
        if(!$permission){ 
            return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
        }
        }
        // ================= permissions ends here =====================  
    $messages = array(
        'id.required' => 'Id is required',
        'name.required' => 'Name is required',
        'name.regex' => 'Name must only contain letters, spaces, and underscores',
        'slug.required' => 'Slug is required',
        'slug.regex' => 'Slug must be valid',
    );

    $validator = Validator::make($request->all(), [
        'id' => 'required',
        'name' => ['required','regex:/^[a-zA-Z_ ]+$/'],
        'slug' => ['required', 'regex:/^[a-z]+$/', new UniqueSlug($request->id)],
    ], $messages);
    if ($validator->fails()) {
        return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
    }     
            $data = $request->all();

        $updateData = [
            'name' => $request->name,
            'slug' => strtolower($request->slug),
            'active' => $request->active,
            'tenant_id' => Auth::user()->tenant_id
        ];
        
        // Check if an image is present in the request
        if ($request->hasFile('image')) {
            $aws3 = new Aws3();
            
            // Process file name
            $fileFabName = Str::uuid() . '____' . $request->file('image')->getClientOriginalName();
            $fileFabName = str_replace(' ', '_', $fileFabName);
            $fileFabNamePath = "faq-category/logo/{$fileFabName}";
        
            // Upload file to AWS S3 and update 'image' in updateData
            $faqImagePathS3Key = $aws3->uploadFile($fileFabNamePath, $request->file('image')->getPathname(), $request->file('image')->getClientMimeType());
            $updateData['image'] = $faqImagePathS3Key;
        }
        
        // Update FaqCategory based on provided conditions
        $updatedValues = FaqCategory::where('id', $request->id)
            ->where('tenant_id', Auth::user()->tenant_id)
            ->update($updateData);
    if($updatedValues){
        return returnResponse(true, 'Faq category updated successfully', [], Response::HTTP_OK);
    }else{
        return returnResponse(false, 'Unable to update the FAQ category, Please try again later in sometime', [], Response::HTTP_OK);
    }
} catch (\Exception $e) {
    if (!App::environment(['local'])) {//, 'staging'
        return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
    } else {
        return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
            'LineNo' => $e->getLine(),
            'FileName' => $e->getFile()
        ], Response::HTTP_EXPECTATION_FAILED);
    }
}
}

// updateFaqsCategoriesStatus

public function updateFaqsCategoriesStatus(Request $request)
{
   
    try {
           // ================= permissions starts here =====================
           if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'faq_categories';
            $params->action = 'T';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;
    
            $permission = checkPermissions($params);   
            if(!$permission){ 
              return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
          }
          // ================= permissions ends here =====================          
          if ($request->id != '') {
              FaqCategory::where('id',$request->id)->update(["active"=>$request->status]);
          return returnResponse(true, "Faq category status Updated Successfully");
        } else {
            return returnResponse(false, "Unable to change status of the FAQ category, Please try again later in sometime", [], 500);
        }
    } catch (\Exception $e) {
        if (!App::environment(['local'])) {//, 'staging'
            return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
        } else {
            return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                'LineNo' => $e->getLine(),
                'FileName' => $e->getFile()
            ], Response::HTTP_EXPECTATION_FAILED);
        }
    }
}

public function deleteFaqCategory(Request $request){
    try {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'faq_categories';
            $params->action = 'D';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;
            
            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
        }
        // ================= permissions ends here =====================             
        DB::beginTransaction();
        $faqCategory = FaqCategory::where('id',$request->id)->where('tenant_id', Auth::user()->tenant_id)->delete();
        $faq = Faq::where('category_id', $request->id)->delete();
        DB::commit();
        if($faq){
        return returnResponse(true, 'Faq category deleted successfully', [], Response::HTTP_OK);
        }else{
        DB::rollback();
        return returnResponse(false, 'Unable to delete the FAQ category, Please try again later in sometime', [], Response::HTTP_OK);
        }

}catch (\Exception $e) {
    DB::rollback();
    if (!App::environment(['local'])) {//, 'staging'
        return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
    } else {
        return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
            'LineNo' => $e->getLine(),
            'FileName' => $e->getFile()
        ], Response::HTTP_EXPECTATION_FAILED);
    }
}
}

}
