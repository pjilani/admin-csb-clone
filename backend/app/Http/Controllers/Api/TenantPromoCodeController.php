<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Models\Admin;
use App\Models\TenantBankConfiguration;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;

use App\Models\Currencies;
use App\Models\TenantPromoCode;
use Carbon\Carbon;
class TenantPromoCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getList(Request $request)
    {
      try {
            // ================= permissions starts here =====================
            if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
              $params = new \stdClass();
              $params->module = 'promo_code';
              $params->action = 'R';
              $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;
              $permission = checkPermissions($params);   
              if(!$permission){ 
                  return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
              }
              }
              // ================= permissions ends here =====================      
        $tenant_id = Auth::user()->tenant_id;
        $admin_user_id = Auth::user()->id;
        $id = 0;
        if(isset($request->id) && $request->id != ''){
          $id = $request->id;
        }
        $data['list'] = TenantPromoCode::getList($tenant_id,$id);
        $data['total'] = count($data['list']);
        return returnResponse(true, 'All Data', $data, 200, true);
      } catch (\Exception $e) {
      
          if (!App::environment(['local'])) {//, 'staging'
              return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
          } else {
              return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                  'LineNo' => $e->getLine(),
                  'FileName' => $e->getFile()
              ], Response::HTTP_EXPECTATION_FAILED);
          }
      }
    }
    
    
    public function getDetails(Request $request)
    {
      try {
          // ================= permissions starts here =====================
          if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'promo_code';
            $params->action = 'R';
            $params->admin_user_id = Auth::user()->id;
              $params->tenant_id = Auth::user()->tenant_id;
            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here =====================      
        $page = $request->has('page') ? $request->post('page') : 1;
        $limit = $request->has('size') ? $request->post('size') : 10;

        $tenant_id = Auth::user()->tenant_id;
        $id = 0;
        if(isset($request->id) && $request->id != ''){
          $id = $request->id;
        }else{
          return returnResponse(false, 'This Promo Code is Invalid.', [], 500, true);
        }
        $data['list'] = TenantPromoCode::getDetails($limit, $page,$tenant_id,$id);
        $data['total'] = $data['list']['users_count'];
        return returnResponse(true, 'All Data', $data, 200, true);
      } catch (\Exception $e) {
      
          if (!App::environment(['local'])) {//, 'staging'
              return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
          } else {
              return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                  'LineNo' => $e->getLine(),
                  'FileName' => $e->getFile()
              ], Response::HTTP_EXPECTATION_FAILED);
          }
      }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function addNewPromoCode(Request $request)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
          $params = new \stdClass();
          $params->module = 'promo_code';
          $params->action = 'C';
          $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;
          $params1 = new \stdClass();
          $params1->module = 'promo_code';
          $params1->action = 'U';
          $params1->admin_user_id = Auth::user()->id;
            $params1->tenant_id = Auth::user()->tenant_id;
          $permission = checkPermissions($params);
          $permission1 = checkPermissions($params1);
          if(!($permission || $permission1)){ 
              return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
          }
          }
          // ================= permissions ends here =====================      
       
        $validator = Validator::make($request->all(), [
            'promo_name' => 'required',
            'code' => 'required',
            'promo_code_used_in' => 'required',
            'description' => 'required',
            'valid_from' => 'required',
            'valid_till' => 'required',
            'promo_code_bonus_type' => 'required',
            'wallet_type' => 'required',
            'bonus_amount' => 'required',
        ]);

        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), 400);
        }

        $data = $request->all();

        try {
          if($data){
            $tenant_id = Auth::user()->tenant_id;
            $admin_user_id = Auth::user()->id;
            $insertData = [
              'promo_name' => $data['promo_name'],
              'code' => $data['code'],
              'promo_code_used_in' => $data['promo_code_used_in'],
              'description' => $data['description'],
              'valid_from' => $data['valid_from'],
              'valid_till' => Carbon::createFromDate($data['valid_till'])->setTime(23, 59, 59, 999999),
              'promo_code_bonus_type' => $data['promo_code_bonus_type'],
              'wallet_type' => $data['wallet_type'],
              'bonus_amount' => $data['bonus_amount'],
              'status' => ($data['status'] ? true : false),
              'updated_at' =>date('Y-m-d H:i:s'),
            ];

            if(isset($data['id']) && $data['id'] != ''){
              // Edit Code

             
              $DuplicatePromoCheck = TenantPromoCode::where("code",$request->code)
                                ->where(
                                  function($query)  use ($data) {
                                    return $query
                                          ->where('id', '!=', $data['id'])
                                          ->where('tenant_id', Auth::user()->tenant_id);
                                  })
                                ->first();
              if($DuplicatePromoCheck){
                return returnResponse(false, 'This Promo Code is already Added.', [], 500, true);
              }
              $createdValues = TenantPromoCode::where('id',$data['id'])->update($insertData);
              $message = "Updated successfully.";
            }else{
              
              $DuplicatePromoCheck = TenantPromoCode::where("code",$request->code)
                                    ->where(
                                      function($query)  use ($data) {
                                        return $query
                                              ->where('tenant_id', Auth::user()->tenant_id);
                                      })
                                    ->first();
              if($DuplicatePromoCheck){
              return returnResponse(false, 'This Promo Code is already Added.', [], 500, true);
              }


              $insertData['tenant_id'] = $tenant_id;
              $insertData['created_at'] = date('Y-m-d H:i:s');
              $insertData['updated_at'] = date('Y-m-d H:i:s');

              $createdValues = TenantPromoCode::create($insertData);
              $message = "insert successfully.";
            }

            if ($createdValues) {
                return returnResponse(true, $message, $insertData, 200, true);
            } else {
                return returnResponse(false, $message, [], 403, true);
            }
            
          }
            
        } catch (\Exception $e) {
         
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Currencies $currencies
     * @return \Illuminate\Http\Response
     */
    public function updateStatus(Request $request)
    {

        try {
              // ================= permissions starts here =====================
              if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'promo_code';
                $params->action = 'T';
                $params->admin_user_id = Auth::user()->id;
                  $params->tenant_id = Auth::user()->tenant_id;
                $permission = checkPermissions($params);   
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================      
            if ($request->id != '') {
              TenantPromoCode::where('id',$request->id)->update(["status"=>$request->status]);
              return returnResponse(true, "Promo Code Updated Successfully", Currencies::all());
            } else {
                return returnResponse(false, "Selected Promo Code is invalid!", [], 500);
            }
        } catch (\Exception $e) {
          
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    
}
