<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Currencies;
use App\Models\Tenants;
use App\Models\Transactions;
use App\Models\User;
use App\Models\UserBonus;
use App\Models\Wallets;
use App\Models\WithdrawRequest;
use App\Services\TransactionsService;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\BetTransaction;
use App\Models\DepositRequest;
use App\Models\DepositWithdrawJob;
use App\Models\DepositWithdrawUsers;
use App\Models\PlayerCommission;
use App\Models\QueueLog;
use App\Models\TenantConfigurations;
use App\Models\WithdrawalTransactions;
use Validator;
use App\Services\SportsService;
use Illuminate\Support\Facades\Redis;
use App\Services\NotificationService;
use Exception;
use Hamcrest\Type\IsNumeric;
use Illuminate\Support\Facades\Hash;
use League\Csv\Reader;

class BulkDepositWithdrawController extends Controller
{

    public function storeBulkJobs(Request $request)
    {
        try {
            if ($request->exists('isCsv') && $request->isCsv) {
                $validatorArray = [
                    'type' => 'required',
                    'wallet_type' => 'required',
                    'csvFile' => 'required|file|mimes:csv,txt'
                ];


                $validator = Validator::make($request->all(), $validatorArray);
                
                if ($validator->fails()) {
                    return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
                }
                $csv = Reader::createFromPath($request->file('csvFile')->path());
                $csv->setDelimiter(',');

                $amount_index = null;
                $username_index = null;

                if (empty($csv->fetchOne(0)) || count($csv->fetchOne(0)) != 4)
                {
                    return returnResponse(false, 'Invalid Csv',[], Response::HTTP_BAD_REQUEST);
                }     

                foreach ($csv->fetchOne(0) as $i => $column_name) {
                    if (stripos($column_name, 'Amount') !== false) $amount_index = $i;
                    if (stripos($column_name, 'Username') !== false) $username_index = $i;
                }

                if (!$amount_index && !$username_index)
                {
                    return returnResponse(false, 'Invalid Csv', [], Response::HTTP_BAD_REQUEST);
                }    

                $csv->setHeaderOffset(0);
                
                $csv = collect($csv->getRecords());

                $filteredCsv = $csv->filter(function($data) {
                    return filter_var($data['Amount'], FILTER_VALIDATE_FLOAT) !== false && $data['Amount'] > 0;
                }); 
                
                $total_amount = $filteredCsv->sum('Amount');
                if($total_amount <= 0) {
                        return returnResponse(false, 'Total amount should be greater than zero.', [], Response::HTTP_BAD_REQUEST);
                }
                $filteredCsv = $filteredCsv->filter(function($data) {
                    return !empty(trim($data['Username']));
                });
                
                if($total_amount > 0) {
                    DB::beginTransaction();
                    $users = User::on('read_db')
                        ->whereIn('user_name', $filteredCsv->pluck('Username')->all())
                        ->where('tenant_id', Auth::user()->tenant_id)
                        ->get(['id', 'user_name']);
                        if($users->isEmpty()){
                            DB::commit();
                            return returnResponse(
                                false,
                                'Users not found.',
                                [],
                                Response::HTTP_EXPECTATION_FAILED
                            );
                        }      
                    if ($request->source_wallete_id) {
                        $wallet = Wallets::on('read_db')->where('id', $request->source_wallete_id)->select('id', 'amount')->first();
                        if ((float)round($wallet->amount, 5) < (float)round($total_amount, 5)) {
                            DB::commit();
                            return returnResponse(
                                false,
                                'Insufficient funds',
                                [],
                                Response::HTTP_EXPECTATION_FAILED
                            );
                        }
                    }
                    if ($request->wallet_type != 2) {
                        $walletId = $request->type == 1 ? $request->source_wallete_id : $request->target_wallete_id;
                        $code = Wallets::where('id', $walletId)->with('currency:id,code')->first('currency_id')->currency->code;
                        $obj = ['amount_type' => $request->wallet_type == 1 ? 'cash' : 'non_cash', 'wallet_id' => $walletId, 'currency_code' => $code];
                    } else {
                        $obj = ['amount_type' => $request->wallet_type == 1 ? 'cash' : 'non_cash'];
                    }                
    
                    $createdValues = DepositWithdrawJob::create(
                        [
                            'type' => $request->type,
                            'request_object' => json_encode($obj),
                            'created_by' => Auth::user()->id,
                            'tenant_id' => Auth::user()->tenant_id,
                            'csv' => true
                        ]
                    );
    
                    $queueLog = QueueLog::create([
                        'type' => 'bulk_deposit_withdraw',
                        'ids' => json_encode([$createdValues->id])
                    ]);
    
                    $insertData = [];
    
                    foreach ($users as $user) {
                            $insertData[] = [
                                'job_id' => $createdValues->id,
                                'user_id' => $user->id,
                                'amount' =>  $filteredCsv->where('Username', $user->user_name)->first()['Amount'],
                                'created_at' => now(),
                                'updated_at'  => now()
                            ];
                    }
                    DepositWithdrawUsers::insert($insertData);
                }
                
            } else {
                $validatorArray = [
                    'type' => 'required',
                    'amount' => 'required',
                    'wallet_type' => 'required',
                    'ids' => 'required',
                ];


                $validator = Validator::make($request->all(), $validatorArray);

                if ($validator->fails()) {
                    return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
                }
                DB::beginTransaction();
                if ($request->source_wallete_id) {
                    $wallet = Wallets::on('read_db')->where('id', $request->source_wallete_id)->select('id', 'amount')->first();
                    $totalAmount = $request->amount * count($request->ids);
                    if ((float)round($wallet->amount, 5) < (float)round($totalAmount, 5)) {
                        DB::commit();
                        return returnResponse(
                            false,
                            'Insufficient funds',
                            [],
                            Response::HTTP_EXPECTATION_FAILED
                        );
                    }
                }
                if ($request->wallet_type !== 2) {
                    $walletId = $request->type === 1 ? $request->source_wallete_id : $request->target_wallete_id;
                    $code = Wallets::where('id', $walletId)->with('currency:id,code')->first('currency_id')->currency->code;
                    $obj = ['amount_type' => $request->wallet_type === 1 ? 'cash' : 'non_cash', 'amount' => number_format((float)$request->amount, 2, '.', ''), 'wallet_id' => $walletId, 'currency_code' => $code];
                } else {
                    $obj = ['amount_type' => $request->wallet_type === 1 ? 'cash' : 'non_cash', 'amount' => number_format((float)$request->amount, 2, '.', '')];
                }

                $createdValues = DepositWithdrawJob::create(
                    [
                        'type' => $request->type,
                        'request_object' => json_encode($obj),
                        'created_by' => Auth::user()->id,
                        'tenant_id' => Auth::user()->tenant_id,
                    ]
                );

                $queueLog = QueueLog::create([
                    'type' => 'bulk_deposit_withdraw',
                    'ids' => json_encode([$createdValues->id])
                ]);
                $insertData = [];
                foreach ($request->ids as $id) {
                    $insertData[] = ['job_id' => $createdValues->id, 'user_id' => $id, 'created_at' => now(),
                    'updated_at'  => now()];
                }
                DepositWithdrawUsers::insert($insertData);
            }
            DB::commit();
            try {
                $redis = Redis::connection();
                $jsonArray = [];
                $jsonArray['QueueLog'] = [
                    'queueLogId' => $queueLog->id
                ];
                $redis->publish(
                    'QUEUE_WORKER',
                    json_encode($jsonArray)
                );
            } catch (\Throwable $th) {
                //throw $th;
            }
            return returnResponse(true, "Job Created Successfully.  Job ID: $createdValues->id", Response::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollBack();
            if (!App::environment(['local'])) { //, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, [
                    'Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function listJobs(Request $request)
    {
        try {
            $page = $request->has('page') ? $request->post('page') : 1;
            $limit = $request->has('size') ? $request->post('size') : 10;
            $filter['sortBy'] = $request->has('sort_by') ? $request->post('sort_by') : 'created_at';
            $filter['sortOrder'] = $request->has('order') ? $request->post('order') : 'desc';

            if ($page == 1) {
                $page = 0;
            }
            $filter['time_period'] = $request->has('time_period') ? $request->post('time_period') : [];


            $filter['time_zone_name'] = $request->time_zone_name ?? 'UTC +00:00';
            $filter['time_period'] = $time_period = $request['datetime'] ?? [];
            $filter['time_type'] = $request['time_type'] ?? 'today';

            $query = DepositWithdrawJob::query();
            $total_not_started = DepositWithdrawJob::query();
            $total_in_progress = DepositWithdrawJob::query();
            $total_failed = DepositWithdrawJob::query();
            $total_completed = DepositWithdrawJob::query();
            $total_partially_failed = DepositWithdrawJob::query();
            if ($filter['time_period']) {
                $formatted_start_date = convertToUTC($filter['time_period']['start_date'], $filter['time_zone_name'])->format('Y-m-d H:i:s.v');
                $formatted_end_date = convertToUTC($filter['time_period']['end_date'], $filter['time_zone_name'])->format('Y-m-d H:i:s.v');
                $query->whereBetween('created_at', [$formatted_start_date, str_replace("000", "999", $formatted_end_date)]);
                $total_not_started->whereBetween('created_at', [$formatted_start_date, str_replace("000", "999", $formatted_end_date)]);
                $total_in_progress->whereBetween('created_at', [$formatted_start_date, str_replace("000", "999", $formatted_end_date)]);
                $total_failed->whereBetween('created_at', [$formatted_start_date, str_replace("000", "999", $formatted_end_date)]);
                $total_completed->whereBetween('created_at', [$formatted_start_date, str_replace("000", "999", $formatted_end_date)]);
                $total_partially_failed->whereBetween('created_at', [$formatted_start_date, str_replace("000", "999", $formatted_end_date)]);
            }
            if ($request->status || $request->status === '0') {
                $query->where('status', $request->status);
                $total_not_started->where('status', $request->status);
                $total_in_progress->where('status', $request->status);
                $total_failed->where('status', $request->status);
                $total_completed->where('status', $request->status);
                $total_partially_failed->where('status', $request->status);
            }
            if ($request->search && is_numeric($request->search)) {
                $query->where('id', $request->search);
                $total_not_started->where('id', $request->search);
                $total_in_progress->where('id', $request->search);
                $total_failed->where('id', $request->search);
                $total_completed->where('id', $request->search);
                $total_partially_failed->where('id', $request->search);
            }
            if ($request->type) {
                $query->where('type', $request->type);
                $total_not_started->where('type', $request->type);
                $total_in_progress->where('type', $request->type);
                $total_failed->where('type', $request->type);
                $total_completed->where('type', $request->type);
                $total_partially_failed->where('type', $request->type);
            }
            if ($request->player_commission) {
                $query->where('type', '=', 3);
                $total_not_started->where('type', '=', 3);
                $total_in_progress->where('type', '=', 3);
                $total_failed->where('type', '=', 3);
                $total_completed->where('type', '=', 3);
                $total_partially_failed->where('type', '=', 3);
            } else {
                $query->where('type', '!=', 3);
                $total_not_started->where('type', '!=', 3);
                $total_in_progress->where('type', '!=', 3);
                $total_failed->where('type', '!=', 3);
                $total_completed->where('type', '!=', 3);
                $total_partially_failed->where('type', '!=', 3);
            }

            $query->where('tenant_id', Auth::user()->tenant_id);

            $record['total_not_started'] = $total_not_started->where('status', 0)->where('tenant_id', Auth::user()->tenant_id)->count();
            $record['total_in_progress'] = $total_in_progress->where('status', 1)->where('tenant_id', Auth::user()->tenant_id)->count();
            $record['total_failed'] = $total_failed->where('status', 2)->where('tenant_id', Auth::user()->tenant_id)->count();
            $record['total_completed'] = $total_completed->where('status', 3)->where('tenant_id', Auth::user()->tenant_id)->count();
            $record['total_partially_failed'] = $total_partially_failed->where('status', 4)->where('tenant_id', Auth::user()->tenant_id)->count();

            $record['data'] = $query->withCount('users')->orderBy($filter['sortBy'], $filter['sortOrder'])->paginate($limit);
            return returnResponse(true, "Record get Successfully", $record);
        } catch (\Exception $e) {
            DB::rollBack();
            if (!App::environment(['local'])) { //, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, [
                    'Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function getJobById(Request $request)
    {
        try {
            $page = $request->has('page') ? $request->post('page') : 1;
            $limit = $request->has('size') ? $request->post('size') : 10;
            $filter['sortBy'] = $request->has('sort_by') ? $request->post('sort_by') : 'created_at';
            $filter['sortOrder'] = $request->has('order') ? $request->post('order') : 'desc';
            if ($page == 1) {
                $page = 0;
            }

            $query = DepositWithdrawUsers::query();
            if ($request->time_period) {
                $query->whereBetween('created_at', [getTimeZoneWiseTimeISO($request->time_period['start_date']), str_replace("000000", "999999", getTimeZoneWiseTimeISO($request['time_period']['end_date']))]);
            }
            if ($request->status || $request->status === '0') {
                $query->where('status', $request->status);
            }
            if ($request->search && is_numeric($request->search)) {
                $query->where('id', $request->search);
            } else {
                $query->whereHas('user', function ($query) use ($request) {
                    $query->where('user_name', 'ilike', '%' . $request->search . '%')
                        ->orWhere('email', 'ilike', '%' . $request->search . '%');
                });
            }

            $record = $query->with('user:id,user_name,email,phone')->where('job_id', $request->id)->orderBy($filter['sortBy'], $filter['sortOrder'])->paginate($limit);

            return returnResponse(true, "Record get Successfully", $record);
        } catch (\Exception $e) {
            DB::rollBack();
            if (!App::environment(['local'])) { //, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, [
                    'Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function processBulkDepositWithdraw(Request $request)
    {
        try {
            $validatorArray = [
                'job_id' => 'required',
                'user_id' => 'required',
                'secret_token' => 'required',
            ];

            $validator = Validator::make($request->all(), $validatorArray);

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
            }
            if ($request->secret_token !== env('SECRET_TOKEN')) {
                return returnResponse(false, '', 'Secret Key Not Matched', Response::HTTP_BAD_REQUEST);
            }

            $checkProcessed =  DepositWithdrawUsers::where('job_id', $request->job_id)->where('user_id', $request->user_id)->where('status', 2)->first();
            if ($checkProcessed) {
                return returnResponse(true, "This action has already been completed", [], Response::HTTP_OK);
            }
            DB::beginTransaction();
            $userStatus = 0;
            $error = [];
            $data = new \stdClass();
            $data->id = $request->user_id;
            $query = DepositWithdrawJob::where('id', $request->job_id)->first();
            if ($query) {
                if ($query->type !== 3) {
                    $data->type = $query->type;
                    $json = json_decode($query->request_object);
                    if($query->csv){
                        $amount = DepositWithdrawUsers::on('read_db')->where('job_id', $request->job_id)->where('user_id', $request->user_id)->select('amount')->first();
                        $data->amount = $amount->amount;
                    }
                    else {
                        $data->amount = $json->amount;
                    }
                    $data->actionee_id = $query->created_by;
                    $adminData = Admin::on('read_db')->where('id', $query->created_by)->select('parent_type')->first();
                    $data->actionee_type = $adminData->parent_type;
                    $data->tenant_id = $query->tenant_id;
                    $data->wallet_type = $json->amount_type === 'cash' ? 1 : 2;
                    if ($json->amount_type === 'cash') {
                        $data->source_wallete_id = $json->wallet_id;
                    }

                    // ==========================data formed====================================

                    $targetWalletId = Wallets::where('owner_id', $data->id)->where('owner_type', 'User')->select('id', 'owner_id')->first();
                    // $sourceWalletData = Wallets::where('owner_id', $data->id)->where('owner_type', 'AdminUser')->select('id', 'owner_id')->first();
                    if ($data->wallet_type === 1 && ($data->type === 1 || $data->type === 2)) {
                        $values = new \stdClass();
                        $values->target_type = 'User';
                        $values->target_email = $data->id;
                        $values->transaction_amount = $data->amount;
                        $values->actionee_id = $data->actionee_id;
                        $values->actionee_type = $data->actionee_type;
                        $values->tenant_id = $data->tenant_id;

                        if ($data->type === 1) {
                            $values->transaction_comments = 'bulk deposit';
                            $values->target_wallete_id = $targetWalletId->id;
                            $values->transaction_type = 'deposit';
                            $values->source_wallete_id = $data->source_wallete_id;
                        }
                        if ($data->type === 2) {
                            $values->transaction_comments = 'bulk withdraw';
                            $values->target_wallete_id = $data->source_wallete_id; //changing for job work
                            $values->transaction_type = 'withdraw';
                            $values->source_wallete_id = $targetWalletId->id;
                        }

                        $response = $this->store($values);
                        if ($response->getData()->success) {
                            $userStatus = 2;
                        } else {
                            $error['record'] = $response->getData()->record;
                            $error['message'] = $response->getData()->message;

                            $userStatus = 1;
                        }
                    } else {
                        $nonCashValues = new \stdClass();
                        $nonCashValues->wallet_id = $targetWalletId->id;
                        $nonCashValues->non_cash_amount = $data->amount;
                        $nonCashValues->actionee_id = $data->actionee_id;
                        $nonCashValues->actionee_type = $data->actionee_type;
                        $nonCashValues->tenant_id = $data->tenant_id;
                        if ($data->type === 1) {
                            $res = $this->walletDeposit($nonCashValues);
                            if ($res->getData()->success) {
                                $userStatus = 2;
                            } else {
                                $error['record'] = $res->getData()->record;
                                $error['message'] = $res->getData()->message;
                                $userStatus = 1;
                            }
                        } else {
                            $res = $this->walletWithdraw($nonCashValues);
                            if ($res->getData()->success) {
                                $userStatus = 2;
                            } else {
                                $error['record'] = $res->getData()->record;
                                $error['message'] = $res->getData()->message;
                                $userStatus = 1;
                            }
                        }
                    }
                } else {

                    $amount = DepositWithdrawUsers::on('read_db')->where('job_id', $request->job_id)->where('user_id', $request->user_id)->select('amount')->first();
                    $data->amount = $amount->amount;
                    $data->actionee_id = $query->created_by;
                    $adminData = Admin::on('read_db')->where('id', $query->created_by)->select('parent_type')->first();
                    $data->actionee_type = $adminData->parent_type;
                    $data->tenant_id = $query->tenant_id;

                    // ==========================data formed====================================

                    if ((float)$data->amount) {
                        $targetWalletId = Wallets::where('owner_id', $data->id)->where('owner_type', 'User')->select('id', 'owner_id')->first();
                        $nonCashValues = new \stdClass();
                        $nonCashValues->wallet_id = $targetWalletId->id;
                        $nonCashValues->non_cash_amount = (float)$data->amount;
                        $nonCashValues->actionee_id = $data->actionee_id;
                        $nonCashValues->actionee_type = $data->actionee_type;
                        $nonCashValues->tenant_id = $data->tenant_id;
                        $nonCashValues->location = 'player-commission';
                        $res = $this->walletDeposit($nonCashValues);
                        if ($res->getData()->success) {
                            $userStatus = 2;
                        } else {
                            $error['record'] = $res->getData()->record;
                            $error['message'] = $res->getData()->message;
                            $userStatus = 1;
                        }
                    } else {
                        $userStatus = 2;
                    }
                }
            }

            DepositWithdrawUsers::where('job_id', $request->job_id)->where('user_id', $request->user_id)->update(['status' => $userStatus]); // updating user status complete/failed
            $totalNotStartedUsers = DepositWithdrawUsers::where('job_id', $request->job_id)->where('status', 0)->count();
            $failedUsers = DepositWithdrawUsers::where('job_id', $request->job_id)->where('status', 1)->count();
            $completedUsers = DepositWithdrawUsers::where('job_id', $request->job_id)->where('status', 2)->count();
            $totalUsers = DepositWithdrawUsers::where('job_id', $request->job_id)->count();

            if ($totalNotStartedUsers !== 0) {
                DepositWithdrawJob::where('id', $request->job_id)->update(['status' => 1]); // updating job in progress if there are users left to process
            }
            if ($failedUsers === $totalUsers) {

                DepositWithdrawJob::where('id', $request->job_id)->update(['status' => 2]); // updating job failed 
            }
            if ($failedUsers !== 0 && $failedUsers < $totalUsers) {
                DepositWithdrawJob::where('id', $request->job_id)->update(['status' => 4]); // updating job partially failed 
            }
            if ($completedUsers === $totalUsers) {
                DepositWithdrawJob::where('id', $request->job_id)->update(['status' => 3]); // updating job completed
            }

            DB::commit();
            if ($error) {
                DepositWithdrawUsers::where('job_id', $request->job_id)->where('user_id', $request->user_id)->update(['error' => json_encode($error)]);
                return returnResponse(false, $error['message'], $error['record'], Response::HTTP_EXPECTATION_FAILED);
            }
            return returnResponse(true, "Bulk Operation Successfully Executed", [], Response::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollBack();
            if (!App::environment(['local'])) { //, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, [
                    'Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function store($request)
    {
        try {

            $input = json_decode(json_encode($request), true);
            if (isset($request->transferType) && $request->transferType == 'self') {
                return $this->tenantAdminSelfTransaction($request);
            }
            $bothWallet = Transactions::getWallet($input['target_wallete_id'], $input['source_wallete_id']);


            if (empty($bothWallet['targetWallet']) && empty($bothWallet['sourceWallet'])) {
                return returnResponse(
                    false,
                    ERROR_MESSAGE . "Please refresh the page",
                    [],
                    Response::HTTP_EXPECTATION_FAILED
                );
            }

            DB::beginTransaction();

            // wallet locking 
            $getSourceWallet = Wallets::where('id', $bothWallet['sourceWallet']->id)->lockForUpdate()->first();
            $getTargetWallet = Wallets::where('id', $bothWallet['targetWallet']->id)->lockForUpdate()->first();

            if (@$bothWallet['sourceWallet']->id == @$bothWallet['targetWallet']->id) {
                return returnResponse(
                    false,
                    'Source wallet and target wallets are some',
                    [],
                    Response::HTTP_EXPECTATION_FAILED
                );
            }
            $currencySource = Currencies::where('id', $bothWallet['sourceWallet']->currency_id)->first();
            $targetWallet = Currencies::where('id', $bothWallet['targetWallet']->currency_id)->first();
            if ((float)round($bothWallet['sourceWallet']->amount, 5) == (float)round($input['transaction_amount'], 5)) {
            } elseif ((float)round($bothWallet['sourceWallet']->amount, 5) < (float)round($input['transaction_amount'], 5)) {
                return returnResponse(
                    false,
                    'Source wallet amount not greater then target Wallet',
                    [],
                    Response::HTTP_EXPECTATION_FAILED
                );
            }


            if (!(float)$input['transaction_amount']) {
                return returnResponse(
                    false,
                    'Please fill Proper Amount ',
                    [],
                    Response::HTTP_EXPECTATION_FAILED
                );
            }


            if ($request->transaction_type == "deposit") {
                $transactionType = TRANSACTION_TYPE_DEPOSIT;
            } else {
                $transactionType = TRANSACTION_TYPE_WITHDRAWAL;
            }
            $insertData = [
                'actionee_type' => $request->actionee_type,
                'actionee_id' => $request->actionee_id,
                'source_currency_id' => $bothWallet['sourceWallet']->currency_id,
                'target_currency_id' => $bothWallet['targetWallet']->currency_id,
                'source_wallet_id' => $bothWallet['sourceWallet']->id,
                'target_wallet_id' => $bothWallet['targetWallet']->id,
                'source_before_balance' => @$bothWallet['sourceWallet']->amount,
                'target_before_balance' => @$bothWallet['targetWallet']->amount,
                'status' => 'success',
                'parent_id' => $request->actionee_id,
                'tenant_id' => $request->tenant_id,
                'created_at' => @date('Y-m-d H:i:s'),
                'comments' => @$input['transaction_comments'],
                'transaction_type' => $transactionType,
                'payment_method' => 'manual',
                'transaction_id' => getUUIDTransactionId()
            ];

            $amount = @$input['transaction_amount'] * ($targetWallet->exchange_rate / $currencySource->exchange_rate);


            $insertData['conversion_rate'] = ($targetWallet->exchange_rate / $currencySource->exchange_rate);
            $insertData['amount'] = floatval(@$input['transaction_amount']);
            if($transactionType == 3){
                $insertData['amount'] = (float)$amount;  
            }

            $insertData['source_after_balance'] = floatval(@$bothWallet['sourceWallet']->amount) - floatval(@$input['transaction_amount']);

            $insertData['target_after_balance'] = floatval(@$bothWallet['targetWallet']->amount) + floatval(@$amount);


            //  =======================================Add Other Currency Amount =======================================
            $configurations = TenantConfigurations::select('allowed_currencies')->where('tenant_id', $request->tenant_id)->get();
            $configurations = $configurations->toArray();
            $CurrenciesValue = [];
            if ($configurations) {

                $configurations = $configurations[0];
                if (strlen($configurations['allowed_currencies']) > 1) {
                    $configurations = str_replace('{', '', $configurations['allowed_currencies']);
                    $configurations = str_replace('}', '', $configurations);
                    $configurations = explode(',', $configurations);

                    if (@$configurations[0] && count($configurations) > 0) {
                        $CurrenciesValue = Currencies::select('code')->whereIn('id', $configurations)->pluck('code');
                        $CurrenciesValue = $CurrenciesValue->toArray();
                    }
                }
            }

            $currenciesTempArray = $insertData['other_currency_amount'] = [];

            $exMainTransactionCurrecy = '';
            if (isset($currencySource->id)) {
                $exMainTransactionCurrecy = $currencySource->code;
            } else {
                $exMainTransactionCurrecy = Currencies::select('code')->where('id',   $targetWallet->currency_id)->pluck('code');
                $exMainTransactionCurrecy = $exMainTransactionCurrecy->toArray();;
            }
            $amount_currency_ex = Currencies::getExchangeRateCurrency($exMainTransactionCurrecy);
            foreach ($CurrenciesValue as $key => $value) {
                if ($exMainTransactionCurrecy != $value) {
                    $getExchangeRateCurrency = Currencies::getExchangeRateCurrency($value);
                    $currenciesTempArray[$value] = (float)number_format((float)$input['transaction_amount'] * ($getExchangeRateCurrency / $amount_currency_ex), 4, ".", "");
                } else {
                    $currenciesTempArray[$value] = (float)$input['transaction_amount'];
                }
            }
            $insertData['other_currency_amount'] = json_encode($currenciesTempArray);
            //  =======================================Add Other Currency Amount ENd =======================================
            $createdValues = Transactions::create($insertData);

            $getSourceWallet->refresh();
            $getTargetWallet->refresh();

            $getSourceWallet->amount = $insertData['source_after_balance'];
            $getTargetWallet->amount = $insertData['target_after_balance'];


            $getSourceWallet->save();
            $getTargetWallet->save();

            if ($createdValues) {
                //Transactions::storeElasticSearchData($createdValues->id);
                $queueLog = QueueLog::create([
                    'type' => CASINO_TRANSACTION,
                    'ids' => json_encode([$createdValues->id])
                ]);

                $this->updateUserBonus($input, $createdValues);
                DB::commit();

                try {
                    $redis = Redis::connection();
                    $jsonArray = [];
                    $jsonArray['QueueLog'] = [
                        'queueLogId' => $queueLog->id
                    ];
                    $redis->publish(
                        'QUEUE_WORKER',
                        json_encode($jsonArray)
                    );
                } catch (\Throwable $th) {
                    //throw $th;
                }

                $isSave = true;

                // $userSourceWalletDetails = TransactionsService :: getUserWalletDetails($bothWallet['sourceWallet']->id);
                // $usertargetWalletDetails = TransactionsService :: getUserWalletDetails($bothWallet['targetWallet']->id);

                // if($userSourceWalletDetails->owner_type == 'User'){
                //     User::storeElasticSearchData($userSourceWalletDetails->user_id);
                // }

                // if($usertargetWalletDetails->owner_type == 'User'){
                //     User::storeElasticSearchData($usertargetWalletDetails->user_id);
                // }
            }
        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) { //, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, [
                    'Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

        if ($isSave) {
            return returnResponse(true, @$request->transaction_type . ' Successfully.', ['transaction_id' => $createdValues->id], Response::HTTP_CREATED, true);
        } else {
            return returnResponse(false, @$request->transaction_type . ' not Successfully.', [], Response::HTTP_BAD_REQUEST, true);
        }
    }


    public function walletDeposit($request)
    {
        try {
            DB::beginTransaction();

            $createDeposit = Transactions::getDepositNoCash($request->wallet_id, $request->non_cash_amount);
            if ($createDeposit) {

                if (property_exists($request, 'location')) {
                    $lastId = TransactionsService::bulkNoCashTransactionHistory($request->wallet_id, $request->non_cash_amount, 19, $request);
                } else {
                    $lastId = TransactionsService::bulkNoCashTransactionHistory($request->wallet_id, $request->non_cash_amount, 5, $request);
                }
                //Entry in queue log
                $queueLog = QueueLog::create([
                    'type' => CASINO_TRANSACTION,
                    'ids' => json_encode([$lastId->id])
                ]);

                DB::commit();

                if ($lastId->id)
                    //Transactions::storeElasticSearchData($lastId->id);
                    try {
                        $redis = Redis::connection();
                        $jsonArray = [];
                        $jsonArray['QueueLog'] = [
                            'queueLogId' => $queueLog->id
                        ];
                        $redis->publish(
                            'QUEUE_WORKER',
                            json_encode($jsonArray)
                        );
                    } catch (\Throwable $th) {
                        //throw $th;
                    }

                // $userWalletDetails = TransactionsService :: getUserWalletDetails($request->wallet_id);
                // User::storeElasticSearchData($userWalletDetails->user_id);

                return returnResponse(true, MESSAGE_DEPOSIT, [], Response::HTTP_CREATED, true);
            } else {
                DB::rollback();
                return returnResponse(false, MESSAGE_DEPOSIT_FAIL, [], Response::HTTP_EXPECTATION_FAILED, true);
            }
        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) { //, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, [
                    'Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function walletWithdraw($request)
    {
        try {
            DB::beginTransaction();

            $userWalletDetails = TransactionsService::getUserWalletDetails($request->wallet_id);

            if ($userWalletDetails->non_cash_amount && $userWalletDetails->non_cash_amount < $request->non_cash_amount) {
                return returnResponse(true, MESSAGE_WITHDRAW_NOT_AVAILABLE, [], Response::HTTP_FORBIDDEN, true);
            }
            $createDeposit = Transactions::getWithdrawNoCash($request->wallet_id, $request->non_cash_amount);

            if ($createDeposit) {

                $lastId = TransactionsService::bulkNoCashTransactionHistory($request->wallet_id, $request->non_cash_amount, 6, $request);
                //Entry in queue log
                $queueLog = QueueLog::create([
                    'type' => CASINO_TRANSACTION,
                    'ids' => json_encode([$lastId->id])
                ]);
                DB::commit();

                if ($lastId)
                    //Transactions::storeElasticSearchData($lastId->id);

                    //User::storeElasticSearchData($userWalletDetails->user_id);
                    try {
                        $redis = Redis::connection();
                        $jsonArray = [];
                        $jsonArray['QueueLog'] = [
                            'queueLogId' => $queueLog->id
                        ];
                        $redis->publish(
                            'QUEUE_WORKER',
                            json_encode($jsonArray)
                        );
                    } catch (\Throwable $th) {
                        //throw $th;
                    }

                return returnResponse(true, MESSAGE_WITHDRAW, [], Response::HTTP_CREATED, true);
            } else {
                DB::rollback();
                return returnResponse(false, 'Insufficient balance in your account', [], Response::HTTP_EXPECTATION_FAILED, true);
            }
        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) { //, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, [
                    'Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }
    private function updateUserBonus($request, $trans)
    {

        $userBonusesExpiresAt = UserBonus::where([
            'user_id' => $request['target_email'],
            'status' => 'active', 'rollover_balance' => 0, 'bonus_amount' => 0
        ])
            ->whereRaw("((kind = 'deposit') OR (kind = 'deposit_sport'))")
            ->whereHas('depositBonusSetting', function ($q) use ($request) {
                $q->where('min_deposit', '<=', $request['transaction_amount']);
                $q->where('max_deposit', '>=', $request['transaction_amount']);
            })
            ->where('expires_at', '>=', Carbon::now()->toDateTimeString())
            ->get();

        $userBonusesBonus = UserBonus::where(['user_id' => $request['target_email'], 'status' => 'active', 'rollover_balance' => 0, 'bonus_amount' => 0])
            ->whereRaw("((kind = 'deposit') OR (kind = 'deposit_sport'))")
            ->whereHas('depositBonusSetting', function ($q) use ($request) {
                $q->where('min_deposit', '<=', $request['transaction_amount']);
                $q->where('max_deposit', '>=', $request['transaction_amount']);
            })
            ->whereNull('expires_at')
            ->whereHas('bonus', function ($bq) {
                $bq->where('valid_from', '<=', Carbon::now()->toDateTimeString());
                $bq->where('valid_upto', '>=', Carbon::now()->toDateTimeString());
            })
            ->get();

        $userBonuses = $userBonusesExpiresAt->merge($userBonusesBonus);
        if (count($userBonuses) > 0) {
            $y = 0;
            foreach ($userBonuses as $key => $userBonus) {

                if (!empty($userBonus) && !empty($userBonus->bonus) && !empty($userBonus->depositBonusSetting)) {

                    if ($request['transaction_type'] == 'deposit') {
                        $rolloverAmount = (($request['transaction_amount'] * $userBonus->bonus->percentage) / 100) * $userBonus->depositBonusSetting->rollover_multiplier;

                        $max_bonus = $userBonus->depositBonusSetting->max_bonus;
                        $bonusAmount = (($request['transaction_amount'] * $userBonus->bonus->percentage) / 100);
                        if ($max_bonus && $bonusAmount) {
                            if ($max_bonus <= $bonusAmount) {
                                $bonusAmount = $max_bonus;
                                $rolloverAmount = $bonusAmount * $userBonus->depositBonusSetting->rollover_multiplier;
                            }
                        }
                        $user = User::find($request['target_email']);

                        if ($user) {
                            $userwallets = Wallets::where('owner_type', 'User')->where('owner_id', $user->id)->first();

                            if ($userBonus->bonus->kind == 'deposit_sport') {

                                if ($y == 0) {
                                    $insertData = [
                                        'amount' => (float)$bonusAmount,
                                        'is_deleted' => false,
                                        'journal_entry' => 'credit',
                                        'reference' => $user->first_name . ' ' . $user->last_name . '-' . $user->id,
                                        'user_id' => $user->id,
                                        'target_currency_id' => $userwallets->currency_id,
                                        'conversion_rate' => $trans->conversion_rate,
                                        'target_wallet_id' => @$userwallets->id,
                                        'target_before_balance' => @$userwallets->amount,
                                        'target_after_balance' => @$userwallets->amount + $bonusAmount,
                                        'status' => 'pending',
                                        'tenant_id' => $user->tenant_id,
                                        'created_at' => @date('Y-m-d H:i:s'),
                                        'updated_at' => @date('Y-m-d H:i:s'),
                                        'description' => 'Pending Transaction for deposit bonus',
                                        'payment_for' => 7,
                                        'current_balance' => @$userwallets->amount
                                    ];
                                    $createdValues = BetTransaction::create($insertData);

                                    UserBonus::where('id', $userBonus->id)
                                        ->update(
                                            [
                                                'rollover_balance' => $rolloverAmount,
                                                'transaction_id' => '' . $createdValues->id,
                                                'bonus_amount' => $bonusAmount
                                            ]
                                        );

                                    //SportsService::storeElasticBetSearchData($createdValues->id, $rolloverAmount);
                                    $queueLog = QueueLog::create([
                                        'type' => BET_TRANSACTION,
                                        'ids' => json_encode([$createdValues->id])
                                    ]);
                                    try {
                                        $redis = Redis::connection();
                                        $jsonArray = [];
                                        $jsonArray['QueueLog'] = [
                                            'queueLogId' => $queueLog->id
                                        ];
                                        $redis->publish(
                                            'QUEUE_WORKER',
                                            json_encode($jsonArray)
                                        );
                                    } catch (\Throwable $th) {
                                        //throw $th;
                                    }
                                    $y++;
                                }
                            } else {
                                $insertData = [
                                    'actionee_type' => 'User',
                                    'actionee_id' => $user->id,
                                    'target_currency_id' => $userwallets->currency_id,
                                    'conversion_rate' => $trans->conversion_rate,
                                    'target_wallet_id' => @$userwallets->id,
                                    'target_before_balance' => @$userwallets->amount,
                                    'target_after_balance' => @$userwallets->amount + $bonusAmount,
                                    'amount' => $bonusAmount,
                                    'status' => 'pending',
                                    'tenant_id' => $user->tenant_id,
                                    'created_at' => @date('Y-m-d H:i:s'),
                                    'comments' => 'Pending Transaction for deposit bonus',
                                    'transaction_type' => 12,
                                    'payment_method' => 'manual',
                                    'transaction_id' => getUUIDTransactionId()

                                ];
                                $createdValues = Transactions::create($insertData);

                                $userBonus->update(
                                    [
                                        'rollover_balance' => $rolloverAmount,
                                        'transaction_id' => '' . $createdValues->id,
                                        'bonus_amount' => $bonusAmount
                                    ]
                                );

                                //Transactions::storeElasticSearchData($createdValues->id, $rolloverAmount);
                                //Entry in queue log
                                $queueLog = QueueLog::create([
                                    'type' => CASINO_TRANSACTION,
                                    'ids' => json_encode([$createdValues->id])
                                ]);
                                try {
                                    $redis = Redis::connection();
                                    $jsonArray = [];
                                    $jsonArray['QueueLog'] = [
                                        'queueLogId' => $queueLog->id
                                    ];
                                    $redis->publish(
                                        'QUEUE_WORKER',
                                        json_encode($jsonArray)
                                    );
                                } catch (\Throwable $th) {
                                    //throw $th;
                                }
                            }
                        }
                    } else if ($request['transaction_type'] == 'withdraw') {

                        if ($userBonus['transaction_id'] && $userBonus['transaction_id'] != '') {

                            if ($userBonus->bonus->kind == 'deposit_sport') {
                                $transaction = BetTransaction::find($userBonus['transaction_id']);
                                if ($transaction) {
                                    $transaction->status = 'failed';
                                    $transaction->payment_for = 8;
                                    $transaction->comments = 'Deposit Bonus cancelled due to amount withdrawal';
                                    $transaction->update();
                                    //SportsService::storeElasticBetSearchData($userBonus['transaction_id'],$rolloverAmount);
                                    $queueLog = QueueLog::create([
                                        'type' => BET_TRANSACTION,
                                        'ids' => json_encode([$userBonus['transaction_id']])
                                    ]);
                                    try {
                                        $redis = Redis::connection();
                                        $jsonArray = [];
                                        $jsonArray['QueueLog'] = [
                                            'queueLogId' => $queueLog->id
                                        ];
                                        $redis->publish(
                                            'QUEUE_WORKER',
                                            json_encode($jsonArray)
                                        );
                                    } catch (\Throwable $th) {
                                        //throw $th;
                                    }
                                }
                            } else {
                                $transaction = Transactions::find($userBonus['transaction_id']);
                                if ($transaction) {
                                    $transaction->status = 'failed';
                                    $transaction->comments = 'Deposit Bonus cancelled due to amount withdrawal';
                                    $transaction->update();
                                    //Transactions::storeElasticSearchData($userBonus['transaction_id']);
                                    //Entry in queue log
                                    $queueLog = QueueLog::create([
                                        'type' => CASINO_TRANSACTION,
                                        'ids' => json_encode([$userBonus['transaction_id']])
                                    ]);
                                    try {
                                        $redis = Redis::connection();
                                        $jsonArray = [];
                                        $jsonArray['QueueLog'] = [
                                            'queueLogId' => $queueLog->id
                                        ];
                                        $redis->publish(
                                            'QUEUE_WORKER',
                                            json_encode($jsonArray)
                                        );
                                    } catch (\Throwable $th) {
                                        //throw $th;
                                    }
                                }
                            }
                        }
                        $userBonus->update(['status' => 'cancelled', 'updated_at' => date('Y-m-d H:i:s')]);
                    }
                }
            }
        } else {
            $userBonuses = UserBonus::where(['user_id' => $request['target_email'], 'status' => 'active'])
                ->get();
            if (count($userBonuses) > 0) {
                foreach ($userBonuses as $key => $userBonus) {
                    $userBonusesObj = UserBonus::where(['id' => $userBonus['id']]);
                    $userBonusesObj->update(['status' => 'cancelled', 'updated_at' => date('Y-m-d H:i:s')]);
                }
                if ($userBonus['transaction_id'] && $userBonus['transaction_id'] != '') {

                    $transaction = Transactions::find($userBonus['transaction_id']);
                    if ($transaction) {
                        $transaction->status = 'failed';
                        $transaction->comments = 'Deposit Bonus cancelled due to amount withdrawal';
                        $transaction->update();
                        //Transactions::storeElasticSearchData($userBonus['transaction_id']);
                        //Entry in queue log
                        $queueLog = QueueLog::create([
                            'type' => CASINO_TRANSACTION,
                            'ids' => json_encode([$userBonus['transaction_id']])
                        ]);
                        try {
                            $redis = Redis::connection();
                            $jsonArray = [];
                            $jsonArray['QueueLog'] = [
                                'queueLogId' => $queueLog->id
                            ];
                            $redis->publish(
                                'QUEUE_WORKER',
                                json_encode($jsonArray)
                            );
                        } catch (\Throwable $th) {
                            //throw $th;
                        }
                    }
                }
            }
        }
    }

    public function tenantAdminSelfTransaction($request)
    {

        try {

            $transactionType = TRANSACTION_TYPE_DEPOSIT;
            $walletId = $request->target_wallete_id;
            $successMessage = MESSAGE_DEPOSIT;
            $errorMessage = MESSAGE_DEPOSIT_FAIL;

            $sqlCheck = "SELECT 
                        *
                        FROM wallets as u
                        where u.id = ?;
                                              
                        ";

            $targetWallet = DB::select($sqlCheck, [$walletId])[0];

            $currencySource = Currencies::getPrimaryCurrency();

            $tenant_id = $request->tenant_id;

            DB::beginTransaction();
            if (!(float)$request['transaction_amount']) {
                return returnResponse(
                    false,
                    'Please fill Proper Amount ',
                    [],
                    Response::HTTP_EXPECTATION_FAILED
                );
            }
            $insertData = [
                'actionee_type' => $request->actionee_type,
                'actionee_id' => $request->actionee_id,
                'target_wallet_id' => $targetWallet->id,
                'target_before_balance' => $targetWallet->amount,
                'status' => 'success',
                'tenant_id' => $tenant_id,
                'target_currency_id' => $targetWallet->currency_id,
                'created_at' => @date('Y-m-d H:i:s'),
                'comments' => @$request['transaction_comments'],
                'transaction_type' => $transactionType,
                'amount' => $request['transaction_amount'],
                'payment_method' => 'manual',
                'transaction_id' => getUUIDTransactionId()
            ];



            $insertData['source_currency_id'] = NULL;
            $insertData['source_wallet_id'] = NULL;
            $insertData['source_before_balance'] = NULL;

            $adminUpdatedWalletAmount = 0;

            if ($targetWallet->amount < 0.00000000) {
                $targetWallet->amount = 0.00000000;
            }

            $insertData['conversion_rate'] = $currencySource->exchange_rate;
            $amount = @(float)$request['transaction_amount'] * ((float)$currencySource->exchange_rate);

            $insertData['source_after_balance'] = NULL;

            $insertData['target_after_balance'] = (float)$targetWallet->amount + (float)@$amount;



            //  =======================================Add Other Currency Amount =======================================
            //  =======================================Add Other Currency Amount =======================================
            $configurations = TenantConfigurations::select('allowed_currencies')->where('tenant_id', $tenant_id)->get();
            $configurations = $configurations->toArray();
            $CurrenciesValue = [];
            if ($configurations) {

                $configurations = $configurations[0];
                if (strlen($configurations['allowed_currencies']) > 1) {
                    $configurations = str_replace('{', '', $configurations['allowed_currencies']);
                    $configurations = str_replace('}', '', $configurations);
                    $configurations = explode(',', $configurations);

                    if (@$configurations[0] && count($configurations) > 0) {
                        $CurrenciesValue = Currencies::select('code')->whereIn('id', $configurations)->pluck('code');
                        $CurrenciesValue = $CurrenciesValue->toArray();
                    }
                }
            }

            $currenciesTempArray = $insertData['other_currency_amount'] = [];

            $exMainTransactionCurrecy = '';
            if (isset($currencySource->id)) {
                $exMainTransactionCurrecy = $currencySource->code;
            } else {
                $exMainTransactionCurrecy = Currencies::select('code')->whereIn('id',   $targetWallet->currency_id)->pluck('code');
                $exMainTransactionCurrecy = $exMainTransactionCurrecy->toArray();;
            }

            $amount_currency_ex = Currencies::getExchangeRateCurrency($exMainTransactionCurrecy);
            // dd($amount_currency_ex);
            foreach ($CurrenciesValue as $key => $value) {
                if ($exMainTransactionCurrecy != $value) {
                    $getExchangeRateCurrency = Currencies::getExchangeRateCurrency($value);
                    $currenciesTempArray[$value] = (float)number_format((float)$request['transaction_amount'] * ($amount_currency_ex / $getExchangeRateCurrency), 4, ".", "");
                } else {
                    $currenciesTempArray[$value] = (float)$request['transaction_amount'];
                }
            }
            $insertData['other_currency_amount'] = json_encode($currenciesTempArray);

            //  =======================================Add Other Currency Amount END =======================================
            //  =======================================Add Other Currency Amount ENd =======================================


            $createdValues = Transactions::create($insertData);

            Wallets::where('id', $targetWallet->id)->update(['amount' => $insertData['target_after_balance']]);

            if (@$createdValues->id) {

                //Transactions::storeElasticSearchData($createdValues->id);
                $queueLog = QueueLog::create([
                    'type' => CASINO_TRANSACTION,
                    'ids' => json_encode([$createdValues->id])
                ]);
                // $userSourceWalletDetails = TransactionsService :: getUserWalletDetails($targetWallet->id);

                // if($userSourceWalletDetails->owner_type == 'User'){
                //     User::storeElasticSearchData($userSourceWalletDetails->user_id);
                // }

            }

            DB::commit();

            try {
                $redis = Redis::connection();
                $jsonArray = [];
                $jsonArray['QueueLog'] = [
                    'queueLogId' => $queueLog->id
                ];
                $redis->publish(
                    'QUEUE_WORKER',
                    json_encode($jsonArray)
                );
            } catch (\Throwable $th) {
                //throw $th;
            }
            if ($createdValues) {
                return returnResponse(true, $successMessage, [], Response::HTTP_CREATED, true);
            } else {

                return returnResponse(false, $errorMessage, [], Response::HTTP_BAD_REQUEST, true);
            }
        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) { //, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, [
                    'Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function retryJob(Request $request, $id)
    {
        try {
            $validatorArray = [
                'id' => 'required',
            ];
            $validator = Validator::make($request->all(), $validatorArray);
            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
            }
            DB::beginTransaction();
            $type = DepositWithdrawJob::where('id', $id)->select('type')->first();
            $queueLog = QueueLog::create([
                'type' => ($type && $type->type === 3) ? 'player_commission' : 'bulk_deposit_withdraw',
                'ids' => json_encode([(int)$id])
            ]);
            DepositWithdrawJob::where('id', $id)->update(['status' => 5]);
            DB::commit();
            try {
                $redis = Redis::connection();
                $jsonArray = [];
                $jsonArray['QueueLog'] = [
                    'queueLogId' => $queueLog->id
                ];
                $redis->publish(
                    'QUEUE_WORKER',
                    json_encode($jsonArray)
                );
            } catch (\Throwable $th) {
                //throw $th;
            }
            return returnResponse(true, "Job retry Successfully", [], Response::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollBack();
            if (!App::environment(['local'])) { //, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, [
                    'Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }


    public function updateBulkStatusAll(Request $request)
    {
        try {
            // ================= permissions starts here =====================
            if (request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager") {
                $params = new \stdClass();
                $params->module = 'players_commission';
                $params->action = 'T';
                $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;

                $permission = checkPermissions($params);
                if (!$permission) {
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
            }
            // ================= permissions ends here =====================          
            DB::beginTransaction();
            $tenantId = Auth::user()->tenant_id;
            if ($request->status === 2) {
                if (isset($request->bulk_all) && $request->bulk_all == 2) {
                    PlayerCommission::on('read_db')->where('status', 0)->select('id', 'commission_amount', 'status', 'user_id')
                        ->chunk(1000, function ($records) use ($request) {
                            $recordIds = $records->pluck('id')->toArray();
                            PlayerCommission::whereIn('id', $recordIds)->update(['status' => $request->status]);
                        });
                    DB::commit();
                    return returnResponse(true, 'Status updated successfully.', [], Response::HTTP_OK);
                }
                $idsToUpdate = array_column($request->data, 'id');
                PlayerCommission::whereIn('id', $idsToUpdate)->update(['status' => $request->status]);
                DB::commit();
                return returnResponse(true, 'Status updated successfully.', [], Response::HTTP_OK);
            }
            $createdValues = DepositWithdrawJob::create(
                [
                    'type' => 3,
                    'request_object' => json_encode([]),
                    'created_by' => Auth::user()->id,
                    'tenant_id' => Auth::user()->tenant_id,
                ]
            );

            $queueLog = QueueLog::create([
                'type' => 'player_commission',
                'ids' => json_encode([$createdValues->id])
            ]);
            if (isset($request->bulk_all) && $request->bulk_all == 1) {
                PlayerCommission::on('read_db')->where('status', 0)
                ->whereHas('user', function ($query) use ($tenantId) {
                    $query->where('tenant_id', $tenantId);
                })
                ->select('id', 'commission_amount', 'status', 'user_id')
                    ->chunk(1000, function ($chunk) use ($request, $createdValues) {
                        $recordsToUpdate = $chunk->pluck('id')->toArray();
                        PlayerCommission::whereIn('id', $recordsToUpdate)->update(['status' => $request->status]);
                        $depositWithdrawData = $chunk->map(function ($record) use ($createdValues) {
                            return [
                                'job_id' => $createdValues->id,
                                'user_id' => $record->user_id,
                                'amount' => (float)$record->commission_amount
                            ];
                        });
                        DepositWithdrawUsers::insert($depositWithdrawData->toArray());
                    });
            } else {
                $commissionUpdates = [];
                $depositWithdrawData = [];
                foreach ($request->data as $value) {
                    $commissionUpdates[$value['id']] = $value['status'];
                    $depositWithdrawData[] = [
                        'job_id' => $createdValues->id,
                        'user_id' => $value['user_id'],
                        'amount' => (float)$value['amount'],
                        'created_at' => now()
                    ];
                }
                PlayerCommission::whereIn('id', array_keys($commissionUpdates))
                    ->update(['status' => $request->status]);
                DepositWithdrawUsers::insert($depositWithdrawData);
            }
            DB::commit();
            try {
                $redis = Redis::connection();
                $jsonArray = [];
                $jsonArray['QueueLog'] = [
                    'queueLogId' => $queueLog->id
                ];
                $redis->publish(
                    'QUEUE_WORKER',
                    json_encode($jsonArray)
                );
            } catch (\Throwable $th) {
                //throw $th;
            }
            return returnResponse(true, 'Status updated successfully.', [], Response::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollBack();
            if (!App::environment(['local'])) { //, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, [
                    'Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function getDates(Request $request)
    {
        try {
            $validatorArray = [
                'type' => 'required',
            ];
            $validator = Validator::make($request->all(), $validatorArray);

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
            }
            $data = [];
            if ($request->type === 'weekly') {
                $data  = DB::connection('read_db')->table('week_details')->get();
            }
            if ($request->type === 'monthly') {
                $data  = DB::connection('read_db')->table('month_details')->get();
            }
            return returnResponse(true, 'Data fetched successfully.', $data, Response::HTTP_OK);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) { //, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, [
                    'Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

}
