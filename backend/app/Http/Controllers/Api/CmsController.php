<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Models\Admin;
use App\Models\Cms;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;

use App\Models\Currencies;

 
class CmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getList(Request $request)
    {
      try {
               // ================= permissions starts here =====================
              if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                  $params = new \stdClass();
                  $params->module = 'cms';
                  $params->action = 'R';
                  $params->admin_user_id = Auth::user()->id;
                  $params->tenant_id = Auth::user()->tenant_id;

                  $permission = checkPermissions($params);   
                  if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                  }
                }
                // ================= permissions ends here =====================          

        $tenant_id = Auth::user()->tenant_id;
        $admin_user_id = Auth::user()->id;
        $id = 0;
        if(isset($request->id) && $request->id != ''){
          $id = $request->id;
        }
        $data['list'] = Cms::getList($tenant_id,$admin_user_id,$id);
        $data['total'] = count($data['list']);
        return returnResponse(true, 'All Data', $data, 200, true);
      } catch (\Exception $e) {
      
          if (!App::environment(['local'])) {//, 'staging'
              return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
          } else {
              return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                  'LineNo' => $e->getLine(),
                  'FileName' => $e->getFile()
              ], Response::HTTP_EXPECTATION_FAILED);
          }
      }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function addCmsPages(Request $request)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
          $params = new \stdClass();
          $params->module = 'cms';
          if(!$request->id){
            $params->action = 'C';
          }
          else{
            $params->action = 'U';
          }
          $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);
          if(!$permission){ 
            return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
          }
        }
        // ================= permissions ends here =====================          
        $messages = array(
          'title.required' => 'Title is required',
          'slug.required' => 'Slug is required',
          'slug.regex' => 'Slug must be valid',
      );
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'slug' => ['required', 'regex:/^[a-z]+$/'],
        ], $messages);

        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), 400);
        }

        $data = $request->all();
       
        try {
          if($data){
            $insertData = [
              'title' => $data['title'],
              'slug' => strtolower($data['slug']),
              'active' => ($data['active'] ? true : false),
              'content' => json_encode($data['content']),
              'updated_at' =>date('Y-m-d H:i:s'),
              'enable_cms_for_register' => $data['enable_flag']
            ];
            $tenant_id = Auth::user()->tenant_id;
            $admin_user_id = Auth::user()->id;
            if($data['enable_flag']){
              // update enable flag for register page
              cms::where('tenant_id',$tenant_id)->where('admin_user_id',$admin_user_id)->update(['enable_cms_for_register'=>false]);
            }
            


            if(isset($data['id']) && $data['id'] != ''){
              // Edit Code

              // check Slug duplication
              $slugCheck = cms::where("slug",$request->slug)
                                ->where(
                                  function($query)  use ($data) {
                                    return $query
                                          ->where('id', '!=', $data['id'])
                                          ->where('tenant_id', Auth::user()->tenant_id);
                                  })
                                ->first();
              if($slugCheck){
                return returnResponse(false, 'Slug already taken by other tenant.', [], 500, true);
              }


              $createdValues = cms::where('id',$data['id'])->update($insertData);
              $message = "Updated successfully.";
            }else{
              
               // check Slug duplication
               $slugCheck = cms::where("slug",$request->slug)
               ->where(
                 function($query) {
                   return $query
                         ->where('tenant_id', Auth::user()->tenant_id);
                 })
               ->first();
              if($slugCheck){
              return returnResponse(false, 'Slug already taken by other tenant.', [], 500, true);
              }

              $insertData['tenant_id'] = $tenant_id;
              $insertData['admin_user_id'] = $admin_user_id;
              $insertData['created_at'] = date('Y-m-d H:i:s');

              $createdValues = cms::create($insertData);
              $message = "insert successfully.";
            }



            
            if ($createdValues) {
                return returnResponse(true, $message, $insertData, 200, true);
            } else {
                return returnResponse(false, $message, [], 403, true);
            }
            
          }
            
        } catch (\Exception $e) {
         
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Currencies $currencies
     * @return \Illuminate\Http\Response
     */
    public function updateStatus(Request $request)
    {

        try {
               // ================= permissions starts here =====================
               if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'cms';
                $params->action = 'T';
                $params->admin_user_id = Auth::user()->id;
                   $params->tenant_id = Auth::user()->tenant_id;

                   $permission = checkPermissions($params);
                if(!$permission){ 
                  return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
              }
              // ================= permissions ends here =====================          

            if ($request->id != '') {
              Cms::where('id',$request->id)->update(["active"=>$request->status]);
              return returnResponse(true, "CMS Page Updated Successfully", Currencies::all());
            } else {
                return returnResponse(false, "Selected page is invalid", [], 500);
            }
        } catch (\Exception $e) {
          
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    
}
