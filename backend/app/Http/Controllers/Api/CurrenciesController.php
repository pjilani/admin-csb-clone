<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Models\Admin;
use App\Models\ConversionHistory;
use Illuminate\Support\Facades\DB;
use App\Models\TenantConfigurations;
use Illuminate\Http\Response;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\App;

use App\Models\Currencies;
use App\Models\CurrencyResponse;

/*
 * European Central Bank Feed
 * Docs: http://www.ecb.int/stats/exchange/eurofxref/html/index.en.html#dev
 * Request: http://www.ecb.int/stats/eurofxref/eurofxref-daily.xml
*/

class CurrenciesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return returnResponse(true, "Record get Successfully", Currencies::orderBy('id', 'DESC')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:currencies',
            'code' => 'required|unique:currencies',
            // 'primary' => 'required',
            'exchange_rate' => 'required',
            'internal_code' => 'required',
        ]);

        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), 400);
        }

        $insertData = [
            'name' => $request->name,
            'code' => $request->code,
            'primary' => ($request->primary ? true : false),
            'exchange_rate' => $request->exchange_rate,
            'internal_code' =>  $request->internal_code,
        ];
        try {
            $createdValues = Currencies::create($insertData);
            if ($createdValues) {
                return returnResponse(true, 'insert successfully.', $insertData, 200, true);
            } else {
                return returnResponse(false, 'insert unsuccessfully.', [], 403, true);
            }
        } catch (\Exception $e) {
         
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Currencies $currencies
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        try {
            if ($id == '') {
                return returnResponse(true, "Record get Successfully", Currencies::all());
            } else {

                return returnResponse(true, "Record get Successfully", Currencies::find($id)->toArray(), 200, true);
            }
        } catch (\Exception $e) {
          
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Currencies $currencies
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Currencies $currencies)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'unique:currencies,name, ' . $request->id,
                'code' => 'unique:currencies,code, ' . $request->id,
                // unique:users,email,'.$user->id
            ]);
    
            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), 400);
            }
            $currencies = $currencies->where('id', $request->id)->get();
            $currenciesArray = $currencies->toArray();
            if (!count($currenciesArray)) {
                return returnResponse(false, 'record Not found', [], 404, true);
            } else {
                $input = $request->all();
                $postData['name'] = $input['name'];
                $postData['code'] = $input['code'];
                $postData['internal_code'] =  $input['internal_code'];
                // if($input['primary']) {
                //     $currenciesCheckPrimary=Currencies::where('primary',true)->get();
                //     if(!is_null($currenciesCheckPrimary)){
                //         $currenciesCheckPrimary = $currenciesCheckPrimary->toArray();
                //         foreach ($currenciesCheckPrimary as $k =>$value){
                //             Currencies::where(['id' => $value['id']])->update(['primary'=> false]);
                //         }
                //     }
                //     $postData['primary'] = $input['primary'];
                // } else {
                //     $postData['primary'] = false;
                // }
                $postData['exchange_rate'] = $input['exchange_rate'];

                Currencies::where(['id' => $request->id])->update($postData);
                return returnResponse(true, 'Update successfully.', Currencies::find($request->id), 200, true);
            }
        } catch (\Exception $e) {
             if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Currencies $currencies
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $currencies = Currencies::where('id', $id)->get();
            $currenciesArray = $currencies->toArray();
            if (!count($currenciesArray)) {
                return returnResponse(false, 'record Not found', [], 404, true);
            } else {
                Currencies::where(['id' => $id])->delete();
                return returnResponse(true, 'deleted successfully.', [], 200, true);
            }
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }


    public function getTenantsCurrencies(Request $request)
    {

        /* if(Auth::user()->parent_type!='AdminUser') {
             return returnResponse(false, 'Permission Denied', [] ,Response::HTTP_UNAUTHORIZED);
         }*/
        try {
                // // ================= permissions starts here =====================
                // if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                // $params = new \stdClass();
                // $params->module = 'tenant_configurations';
                // $params->action = 'R';
                // $params->admin_user_id = Auth::user()->id;
        
                // $permission = checkPermissions($params);   
                // if(!$permission){ 
                //     return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                // }
                // }
                // // ================= permissions ends here =====================          

            if(isset($request->tenant_id) && @$request->tenant_id == 0){
                return returnResponse(true, "Record get Successfully", Currencies::on('read_db')->orderBy('id', 'DESC')->get());
            }else{
                if (@$request->tenant_id) {
                    $tenant_id = $request->tenant_id;
                } else {
                    $tenant_id = Auth::user()->tenant_id;
                }
                $tenant_base_currency = DB::connection('read_db')->table('tenant_credentials')->where(['tenant_id' => $tenant_id, 'key' => 'TENANT_BASE_CURRENCY'])->value('value');
                $configurations = TenantConfigurations::select('allowed_currencies')->where('tenant_id', $tenant_id)->get();
                $configurations = $configurations->toArray();
                if ($configurations) {
    
                    $configurations = $configurations[0];
                    if (strlen($configurations['allowed_currencies']) > 1) {
    
                        $configurations = str_replace('{', '', $configurations['allowed_currencies']);
                        $configurations = str_replace('}', '', $configurations);
                        $configurations = explode(',', $configurations);
                        if (@$configurations[0] && count($configurations) > 0) {
                            $CurrenciesValue = Currencies::orderBy('code', 'ASC')->select('id', 'name', 'code', 'exchange_rate')->whereIn('id', $configurations)->get();
                            $CurrenciesValue = $CurrenciesValue->toArray();
                            $code = array_column($CurrenciesValue, 'code');
                            $index = array_search('chips', $code);
                            if($index !== false){
                              $CurrenciesValue = [$CurrenciesValue[$index]];
                            }
    
                            if($tenant_base_currency != null && $tenant_base_currency != 'EUR'){
                                $temp = array_filter($CurrenciesValue, function ($data) use ($tenant_base_currency) {
                                    return $data['code'] == $tenant_base_currency;
                                });
    
                                $CurrenciesValue = array_values($temp);
                            }
                         return returnResponse(true, "Record get Successfully", $CurrenciesValue);
                        } else {
                            return returnResponse(true, "Currencies not found ", []);
                        }
                    } else {
                        return returnResponse(true, "Currencies not found ", []);
                    }
                } else {
                    return returnResponse(true, "Currencies not found ", []);
                }
            }


        } catch (\Exception $e) {
           
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }


    public function listConversionHistory(Request $request) {
        try {
            $page = $request->has('page') ? $request->post('page') : 1;
            $limit = $request->has('size') ? $request->post('size') : 10;
            $filter['sortBy'] = $request->has('sort_by') ? $request->post('sort_by') : 'created_at';
            $filter['sortOrder'] = $request->has('order') ? $request->post('order') : 'desc';

            if ($page == 1) {
                $page = 0;
            }
            $filter['time_period'] = $request->has('time_period') ? $request->post('time_period') : [];
            $filter['time_period_updated_at'] = $request->has('time_period_updated_at') ? $request->post('time_period_updated_at') : [];


            $filter['time_zone_name'] = $request->time_zone_name?? 'UTC +00:00';
            $filter['time_period'] = $time_period = $request['datetime'] ?? [];
            $filter['time_period_updated_at'] = $time_period = $request['datetimeUpdatedAt'] ?? [];

            // $filter['time_type'] = $request['time_type'] ?? 'today';

            $query = ConversionHistory::query();
            if ($filter['time_period']) {
                $formatted_start_date = convertToUTC($filter['time_period']['start_date'], $filter['time_zone_name'])->format('Y-m-d H:i:s.v');
                $formatted_end_date = convertToUTC($filter['time_period']['end_date'], $filter['time_zone_name'])->format('Y-m-d H:i:s.v');
                $query->whereBetween('created_at',[ $formatted_start_date, str_replace("000", "999", $formatted_end_date)]);
            }
            if ($filter['time_period_updated_at']) {
                $formatted_start_date = convertToUTC($filter['time_period_updated_at']['start_date'], $filter['time_zone_name'])->format('Y-m-d H:i:s.v');
                $formatted_end_date = convertToUTC($filter['time_period_updated_at']['end_date'], $filter['time_zone_name'])->format('Y-m-d H:i:s.v');
                $query->whereBetween('updated_at',[ $formatted_start_date, str_replace("000", "999", $formatted_end_date)]);
            }
            if ($request->code) {
                $query->where('currency_id', $request->code);
            }
            if ($request->search) {
                // $query->where('id', $request->search);
                $query->where('id', 'ilike', '%' . $request->search . '%');
            }

            if ($request->exchange_rate) {
                $query->where('old_exchange_rate', 'ilike', '%' . $request->exchange_rate . '%');
                $query->orwhere('new_exchange_rate', 'ilike', '%' . $request->exchange_rate . '%');
            }
            // $query->where('tenant_id', Auth::user()->tenant_id);

            $record['data'] = $query->with('currency:id,code')->orderBy($filter['sortBy'], $filter['sortOrder'])->paginate($limit);
            return returnResponse(true, "Record get Successfully", $record, 200, true);
        } catch (\Exception $e) {
            DB::rollBack();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function currencyResponse(Request $request)  {
        try {
            $currencyResponse = CurrencyResponse::where('id', $request->id)->get();
            $currencyResponse[0]->response = json_decode($currencyResponse[0]->response, true);
            return returnResponse(true, "Record get Successfully", $currencyResponse, 200, true);
        }
        catch (\Exception $e) {
            DB::rollBack();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

}
