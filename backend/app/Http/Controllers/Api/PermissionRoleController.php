<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\PermissionRole;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PermissionRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            // ================= permissions starts here =====================
            if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'permission_role';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;

                $permission = checkPermissions($params);   
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
            // ================= permissions ends here =====================  
            $record = PermissionRole::on('read_db')->when(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager", function ($query) {
                return $query->where('tenant_id',Auth::User()->tenant_id,);
            });

            if($request->tenant_id){
                $record->where('tenant_id', $request->tenant_id);
            }

            if($request->search_role_name) {
                $record->where('role_name', 'ilike', '%' . $request->search_role_name . '%');

            }

            if($request->role_type) {
                $record->where('role_type', $request->role_type);    
            }
            $record = $record->orderBy('updated_at', 'desc')->paginate($request->size ?? 10);
            return returnResponse(true, "Record get Successfully", $record);
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'permission_role';
            $params->action = 'C';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
        // ================= permissions ends here =====================  
        $validator = Validator::make($request->all(), [
            'role_name' => 'required',
            'permission' => 'required',
            'role_type' => 'required'
        ]);

        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), 400);
        }

        $existingRecord = PermissionRole::on('read_db')->whereRaw('LOWER(REPLACE(REPLACE(REPLACE(role_name, \' \', \'\'), \'-\', \'\'), \'_\', \'\')) = ?',
                            [strtolower(preg_replace('/[\s\-_]+/', '', $request->role_name))])
                            ->where('tenant_id',($request->tenant_id)? $request->tenant_id : Auth::User()->tenant_id)
                            ->first();

        if ($existingRecord) {
            return returnResponse(false, 'Role name has already been taken.', [], 400, true);
        }

        $insertData = [
            'role_name' => $request->role_name,
            'permission' => $request->permission,
            'tenant_id' => ($request->tenant_id)? $request->tenant_id : Auth::User()->tenant_id,
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id,
            'created_by_type' => request()->User()->parent_type,
            'role_type' => $request->role_type,
        ];
        try {
            $createdValues = PermissionRole::create($insertData);
            if ($createdValues) {
                return returnResponse(true, 'insert successfully.', $insertData, 200, true);
            } else {
                return returnResponse(false, 'insert unsuccessfully.', [], 403, true);
            }
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PermissionRole $PermissionRole
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            // ================= permissions starts here =====================
            if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'permission_role';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;

                $permission = checkPermissions($params);   
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
            // ================= permissions ends here =====================  
            if ($id == '') {
                return returnResponse(true, "Record get Successfully", PermissionRole::on('read_db')->all());
            } else {
                return returnResponse(true, "Record get Successfully", PermissionRole::on('read_db')->find($id)->toArray(), 200, true);
            }
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\PermissionRole $permissionRole
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PermissionRole $permissionRole)
    {

        try {
            // ================= permissions starts here =====================
            if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'permission_role';
                $params->action = 'U';
                $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;

                $permission = checkPermissions($params);   
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
            // ================= permissions ends here =====================  

            $permissionRole = $permissionRole::on('read_db')->where('id', $request->id)->first();
            $permissionRoleArray = $permissionRole->toArray();
            if (!count($permissionRoleArray)) {
                return returnResponse(false, 'record Not found', [], 404, true);
            } else {

                $processedStr1 = strtolower(preg_replace('/[\s\-_]+/', '', $permissionRole->role_name));
                $processedStr2 = strtolower(preg_replace('/[\s\-_]+/', '', $request->role_name));

                if ($processedStr1 !== $processedStr2) {
                    $existingRecord = PermissionRole::on('read_db')->whereRaw('LOWER(REPLACE(REPLACE(REPLACE(role_name, \' \', \'\'), \'-\', \'\'), \'_\', \'\')) = ?',
                        [strtolower(preg_replace('/[\s\-_]+/', '', $request->role_name))])
                        ->where('tenant_id', $permissionRole->tenant_id)
                        ->first();

                    if ($existingRecord) {
                        return returnResponse(false, 'Role name has already been taken.', [], 400, true);
                    }
                }


                // if($permissionRole->role_name != $request->role_name){
                //     $existingRecord = PermissionRole::where('role_name', $request->input('role_name'))
                //         ->where('tenant_id',$permissionRole->tenant_id)
                //         ->first();
                    
                //     if ($existingRecord) {
                //         return returnResponse(false, 'The role_name has already been taken.', [], 400, true);
                //     }
                // }

                $postData['role_name'] = $request->role_name;
                $postData['permission'] = $request->permission;
                
                PermissionRole::where(['id' => $request->id])->update($postData);

                return returnResponse(true, 'Update successfully.', PermissionRole::find($request->id), 200, true);
            }
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PermissionRole $permissionRole
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $permissionRole = PermissionRole::where('id', $id)->first();
            $permissionRoleArray = $permissionRole->toArray();
            if (!count($permissionRoleArray)) {
                return returnResponse(false, 'record Not found', [], 404, true);
            } else {

                PermissionRole::where(['id' => $id])->delete();
                return returnResponse(true, 'deleted successfully.', [], 200, true);
            }
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    public function getPermissionRoles(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'role_type' => 'required'
            ]);

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), 400);
            }

            $record = PermissionRole::on('read_db')->where(
                'tenant_id',Auth::User()->tenant_id,
            )->where('role_type', $request->role_type)
            ->select('id','role_name as name')
            ->orderBy('updated_at', 'desc')->get();
            return returnResponse(true, "Record get Successfully", $record);
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function getPermissionsRoleWise(Request $request) {
        try {
                $validator = Validator::make($request->all(), [
                    'role_type' => 'required'
                ]);
        
                if ($validator->fails()) {
                    return returnResponse(false, '', $validator->errors(), 400);
                }
    
                $ownerPermissions = getPermissionsForAdminUser(Auth::user()->id,Auth::user()->tenant_id);
                $allowedPermissions = $ownerPermissions;

                // fetching permissions for agent
                if($request->role_type == 2){
                    foreach (AGENT_NOT_ALLOWED_PERMISSIONS_MODULE as $key) {
                        if (property_exists($allowedPermissions, $key)) {
                            unset($allowedPermissions->$key);
                        }
                    }
                }  

                // fetching permissions for sub-admin
                if($request->role_type == 3){
                    foreach (SUB_ADMIN_NOT_ALLOWED_PERMISSIONS_MODULE as $key) {
                        if (property_exists($allowedPermissions, $key)) {
                            unset($allowedPermissions->$key);
                        }
                    }
                }  
                
                $data['permissions'] = $allowedPermissions;
                return returnResponse(true, "Record get Successfully", $data);
            }
        catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function getPermissionsTenantAndRoleWise(Request $request) {
        try {
                $validator = Validator::make($request->all(), [
                    'role_type' => 'required',
                    'tenant_id' => 'required'
                ]);
        
                if ($validator->fails()) {
                    return returnResponse(false, '', $validator->errors(), 400);
                }
    
                $ownerPermissions = DB::connection('read_db')->table('tenant_permissions')
                ->where('tenant_id', $request->tenant_id)
                ->value('permission');
                $allowedPermissions = json_decode($ownerPermissions);

                // fetching permissions for agent
                if($request->role_type == 2){
                    foreach (AGENT_NOT_ALLOWED_PERMISSIONS_MODULE as $key) {
                        if (property_exists($allowedPermissions, $key)) {
                            unset($allowedPermissions->$key);
                        }
                    }
                }  

                // fetching permissions for sub-admin
                if($request->role_type == 3){
                    foreach (SUB_ADMIN_NOT_ALLOWED_PERMISSIONS_MODULE as $key) {
                        if (property_exists($allowedPermissions, $key)) {
                            unset($allowedPermissions->$key);
                        }
                    }
                }  
                
                $data['permissions'] = $allowedPermissions;
                return returnResponse(true, "Record get Successfully", $data);
            }
        catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }
}
