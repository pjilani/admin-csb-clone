<?php

namespace App\Http\Controllers\Api\Payout;

use App\Http\Controllers\Controller;
use App\Models\QueueLog;
use App\Models\RequestResponseLogs;
use App\Models\Transactions;
use App\Models\User;
use App\Models\UserBonus;
use App\Models\Wallets;
use App\Models\WithdrawalTransactions;
use App\Models\WithdrawRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Support\Facades\Redis;
use App\Services\NotificationService;
use Illuminate\Support\Facades\Log;

class PayoutCallbacksController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function payoutStatus(Request $request)
  {
      $headers = $request->header();
      $body = $request->all();
      $jsonRequest = [
        'headers' => $headers,
        'body' => $body,
      ];

      // check the withdrawal status
      $withdrawal = WithdrawRequest::where('fd_transaction_id', $request->fdTransactionID)->first();
    try {
      $validatorArray = [
        'userName' => 'required',
        'payeeAccountNumber' => 'required',
        'payeeName' => 'required',
        'fdTransactionID' => 'required',
        'fundTransferBy' => 'required',
        'transactionDate' => 'required',
        'fundTransferID' => 'required',
        'status' => 'required',
        'amount' => 'required'
      ];

      Log::info([
        'payout_Callback_request' => $request->all()
      ]);


      $validator = Validator::make($request->all(), $validatorArray);

      if ($validator->fails()) {
        return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
      }


      DB::beginTransaction();
      if ($withdrawal) {
        if ($withdrawal['status'] == "approved") {
          DB::rollBack();
          RequestResponseLogs::create([
            'request_json' => json_encode($jsonRequest),
            'response_json' => json_encode(['Error' => WITHDRAW_ALREADY_PROCCESSED]),
            'service' => PAYOUT_SERVICE ,
            'url' => $request->url(),
            'tenant_id' => $withdrawal->tenant_id,
            'response_code' => Response::HTTP_EXPECTATION_FAILED,
            'response_status' => TRANSACTION_FAILED,
            'error_code' => Response::HTTP_EXPECTATION_FAILED
          ]);
          return returnResponse(false, 'Withdrawal Request Already Approved For This Transaction ID', [], Response::HTTP_EXPECTATION_FAILED);

        } else if ($withdrawal['status'] == "pending_by_gateway") {

          $type = 'payment_gateway';
          
          $transaction = Transactions::find($withdrawal['transaction_id']);
          if (empty($transaction)) {
            DB::rollBack();
            RequestResponseLogs::create([
              'request_json' => json_encode($jsonRequest),
              'response_json' => json_encode(['Error' => TRANSACTION_NOT_FOUND]),
              'service' => PAYOUT_SERVICE ,
              'url' => $request->url(),
              'tenant_id' => $withdrawal->tenant_id,
              'response_code' => Response::HTTP_NOT_FOUND,
              'response_status' => TRANSACTION_FAILED,
              'error_code' => Response::HTTP_NOT_FOUND
            ]);
            return returnResponse(false, 'Transaction not Found', [], Response::HTTP_NOT_FOUND);
          }


          WithdrawRequest::find($withdrawal['id'])->update(
            ['withdrawal_type'=>$type,
            'status' => ($request->status == 'Completed' ? 'approved' : ($request->status == 'Failed' ? 'rejected_by_gateway' : 'pending_by_gateway')),
            'actioned_at' => date('Y-m-d H:i:s')]);

          $status = "approved by Payment Gateway";
          if($request->status == 'Completed') {

              $userBonuses = UserBonus::select('id')->where(['user_id' => $withdrawal['user_id'], 'status' => 'active'])->pluck('id')->toArray();
              if (count($userBonuses) > 0) {
                UserBonus::whereIn('id', $userBonuses)->update(['status' => 'cancelled','updated_at' => date('Y-m-d H:i:s')]);
              }

              if($transaction){
                  $transaction->transaction_id =$request->fundTransferID;
                  $transaction->status ='success';
                  $transaction->comments = 'Approved By Payment gateway';
                  $transaction->update();

              }


               // Update Withdrawal Amount in tenant wallet (Not use in any case)
                // Find Tenant wallet id 

                $tenantWallet = Wallets::select('withdrawal_amount','currency_id','id')->where('owner_id',$transaction->actionee_id)->where('currency_id',$transaction->source_currency_id)->lockForUpdate()->first();
                $newTenantWithdrawalAmount = (float)$tenantWallet->withdrawal_amount + (float)$transaction->amount;
                $tenantWallet->refresh();
                $tenantWallet->withdrawal_amount = $newTenantWithdrawalAmount;
                $tenantWallet->save();


                 // Create Withdrawal Transaction
                $withdrawal_transaction = $transaction;
                if($withdrawal_transaction){
                  unset($withdrawal_transaction->id);
                  $withdrawal_transaction->target_wallet_id = $tenantWallet->id;
                  $withdrawal_transaction->target_currency_id = $tenantWallet->currency_id;
                  $withdrawal_transaction->target_before_balance = $tenantWallet->withdrawal_amount;
                  $withdrawal_transaction->target_after_balance = $newTenantWithdrawalAmount;
                  $withdrawal_transaction->updated_at = date('Y-m-d H:i:s');
                  $withdrawal_transaction->transaction_id = getUUIDTransactionId();
                  WithdrawalTransactions::create($withdrawal_transaction->toArray());
                }





          }elseif ($request->status == 'Failed'){
              if($transaction){
                  $status = 'rejected by Payment Gateway';
                  $transaction->status ='cancelled';
                  $transaction->comments = 'Rejected By payment Gateway';
                  $transaction->update();

                      $data = Wallets::where('id', $transaction->source_wallet_id)
                      ->lockForUpdate()
                      ->first();
                      $data->refresh();
                      $updatedAmount = $withdrawal['amount'] + $data->amount;
                      $data->amount  = $updatedAmount;
                      $data->save();
                  // User::storeElasticSearchData($withdrawal['user_id']);
                  $queueLog = QueueLog::create([
                    'type' => USER_TRANSACTION,
                    'ids' => json_encode([$withdrawal['user_id']])
                ]);
                try {
                    $redis = Redis::connection();
                    $jsonArray=[];
                    $jsonArray['QueueLog']= [
                        'queueLogId'=> $queueLog->id
                    ];
                    $redis->publish('QUEUE_WORKER', json_encode($jsonArray)
                    );
                } catch (\Throwable $th) {
                //throw $th;
                }

              }
          }
        }
          
          if(@$withdrawal['transaction_id']){
              // Transactions::storeElasticSearchData($withdrawal['transaction_id']);
              $queueLog = QueueLog::create([
                'type' => CASINO_TRANSACTION,
                'ids' => json_encode([$withdrawal['transaction_id']])
            ]);
            try {
                $redis = Redis::connection();
                $jsonArray=[];
                $jsonArray['QueueLog']= [
                    'queueLogId'=> $queueLog->id
                ];
                $redis->publish('QUEUE_WORKER', json_encode($jsonArray)
                );
            } catch (\Throwable $th) {
            //throw $th;
            }

              $receiverId = $withdrawal['user_id'];
              $receiverType = USER_TYPE;
              $message = "Your withdrawal request of ".$withdrawal['amount']." has been ".$status;
              $id = NotificationService::addNotification($receiverId, $receiverType, $message);

             try {
              $redis = Redis::connection();
              $jsonArray=[];
              $jsonArray['PlayerNotification']= [
                  'message' => $message,
                  'userId' => $withdrawal['user_id'],
                  'senderType'=>ADMIN_TYPE,
                  'referenceId'=>$receiverId,
                  'referenceType'=>USER_TYPE,
                  'id'=>@$id->id

              ];
              $redis->publish('PLAYER_NOTIFICATION_CHANNEL', json_encode($jsonArray)
              );
             } catch (\Throwable $th) {
              //throw $th;
             }

          }
          DB::commit();
          RequestResponseLogs::create([
            'request_json' => json_encode($jsonRequest),
            'response_json' => json_encode(['message' => WITHDRAW_SUCCESS, 'status' => Response::HTTP_OK]),
            'service' => PAYOUT_SERVICE ,
            'url' => $request->url(),
            'tenant_id' => $withdrawal->tenant_id,
            'response_code' => Response::HTTP_OK,
            'response_status' => 'success',
            'error_code' => Response::HTTP_OK
          ]);
          return returnResponse(true, 'Withdraw Request updated successfully', [], Response::HTTP_OK);
        
        
      } else {
        DB::rollBack();
        RequestResponseLogs::create([
          'request_json' => json_encode($jsonRequest),
          'response_json' => json_encode(['message' => WITHDRAW_NOT_FOUND, 'status' => Response::HTTP_EXPECTATION_FAILED]),
          'service' => PAYOUT_SERVICE ,
          'url' => $request->url(),
          'tenant_id' => $withdrawal->tenant_id,
          'response_code' => Response::HTTP_EXPECTATION_FAILED,
          'response_status' => TRANSACTION_FAILED,
          'error_code' => Response::HTTP_EXPECTATION_FAILED
        ]);
        return returnResponse(false, 'No Withdrawal Request Found For This Transaction ID', [], Response::HTTP_EXPECTATION_FAILED);
      }
    } catch (\Exception $e) {
      DB::rollBack();
      Log::info([
        'payout_Callback_error' => $e
      ]);
      RequestResponseLogs::create([
        'request_json' => json_encode($jsonRequest),
        'response_json' => json_encode([
          'Error' => $e->getMessage(),
          'LineNo' => $e->getLine(),
          'FileName' => $e->getFile()
        ]),
        'service' => PAYOUT_SERVICE ,
        'url' => $request->url(),
        'tenant_id' => $withdrawal->tenant_id,
        'response_code' => Response::HTTP_EXPECTATION_FAILED,
        'response_status' => TRANSACTION_FAILED,
        'error_code' => Response::HTTP_EXPECTATION_FAILED
      ]);

      if (!App::environment(['local'])) { //, 'staging'
        return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
      } else {
        return returnResponse(false, ERROR_MESSAGE, [
          'Error' => $e->getMessage(),
          'LineNo' => $e->getLine(),
          'FileName' => $e->getFile()
        ], Response::HTTP_EXPECTATION_FAILED);
      }
    }
  }

  public function payoutStatusIndigrit(Request $request)
  {
      $headers = $request->header();
      $body = $request->all();
      $jsonRequest = [
        'headers' => $headers,
        'body' => $body,
      ];
      DB::beginTransaction();
      $tenant_id = User::on('read_db')->where('id', $request->customer_id)->select('id','tenant_id')->first()->tenant_id;

    try {
      $validatorArray = [
        'customer_id' => 'required',
        'paid_out_at' => 'required',
        'payout_id' => 'required',
        'status' => 'required',
        'amount' => 'required'
      ];

      Log::info([
        'payout_Callback_request' => $request->all()
      ]);

      $validator = Validator::make($request->all(), $validatorArray);

      if ($validator->fails()) {
        return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
      }

      // check the withdrawal status
      $withdrawal = WithdrawRequest::on('read_db')->where('payment_transaction_id', $request->payout_id)->first();
      if ($withdrawal) {
        $status = CANCELLED;
        if ($withdrawal['status'] == WITHDRAW_APPORVED) {
          RequestResponseLogs::create([
            'request_json' => json_encode($jsonRequest),
            'response_json' => json_encode(['Error' => WITHDRAW_ALREADY_PROCCESSED]),
            'service' => INDIGRIT_SERVICE ,
            'url' => $request->url(),
            'tenant_id' => $tenant_id,
            'response_code' => Response::HTTP_EXPECTATION_FAILED,
            'response_status' => TRANSACTION_FAILED,
            'error_code' => Response::HTTP_EXPECTATION_FAILED
          ]);
          return returnResponse(false, WITHDRAW_ALREADY_PROCCESSED, [], Response::HTTP_EXPECTATION_FAILED);
        }
         else if ($withdrawal['status'] == PENDING_BY_GATEWAY) {

          $type = WITHDRAW_TYPE;
          
          $transaction = Transactions::select('id', 'source_wallet_id', 'comments', 'status', 'actionee_id', 'amount', 'source_currency_id', 'target_wallet_id', 'target_currency_id', 'target_before_balance', 'target_after_balance', 'updated_at', 'transaction_id')->find($withdrawal['transaction_id']);
          if (empty($transaction)) {
            RequestResponseLogs::create([
              'request_json' => json_encode($jsonRequest),
              'response_json' => json_encode(['Error' => TRANSACTION_NOT_FOUND]),
              'service' => INDIGRIT_SERVICE ,
              'url' => $request->url(),
              'tenant_id' => $tenant_id,
              'response_code' => Response::HTTP_NOT_FOUND,
              'response_status' => TRANSACTION_FAILED,
              'error_code' => Response::HTTP_NOT_FOUND
            ]);
            return returnResponse(false, TRANSACTION_NOT_FOUND, [], Response::HTTP_NOT_FOUND);
          }

          WithdrawRequest::find($withdrawal['id'])->update(
            ['withdrawal_type'=>$type,
            'status' => ($request->status == SUCCESS ? APPROVED : ($request->status == FAILED ? REJECTED_BY_GATEWAY : PENDING_BY_GATEWAY)),
            'actioned_at' => date('Y-m-d H:i:s')]);

          $status = 'approved by Payment Gateway';
          if($request->status == SUCCESS) {

              $userBonuses = UserBonus::on('read_db')->where(['user_id' => $withdrawal['user_id'], 'status' => ACTIVE])
                  ->get();
              if (count($userBonuses) > 0) {
                  foreach ($userBonuses as $key => $userBonus) {
                      $userBonusesObj = UserBonus::where(['id' => $userBonus['id']]);
                      $userBonusesObj->update(['status' => CANCELLED, 'updated_at'=>date('Y-m-d H:i:s')]);
                  }
              }

              if($transaction){
                  // $transaction->transaction_id =$request->tracking_id;
                  $transaction->status = SUCCESS;
                  $transaction->comments = APPROVED_BY_PAYMENT_GATEWAY;
                  $transaction->update();

              }

               // Update Withdrawal Amount in tenant wallet (Not use in any case)
                // Find Tenant wallet id 

              try {
                $tenantWallet = Wallets::where('owner_id',$transaction->actionee_id)->where('currency_id',$transaction->source_currency_id)->lockForUpdate()->first();
                $newTenantWithdrawalAmount = (float)$tenantWallet->withdrawal_amount + (float)$transaction->amount;
                $tenantWallet->refresh();
                $tenantWallet->withdrawal_amount = $newTenantWithdrawalAmount;
                $tenantWallet->save();
               
                 // Create Withdrawal Transaction
                $withdrawal_transaction = $transaction;
                if($withdrawal_transaction){
                  unset($withdrawal_transaction->id);
                  $withdrawal_transaction->target_wallet_id = $tenantWallet->id;
                  $withdrawal_transaction->target_currency_id = $tenantWallet->currency_id;
                  $withdrawal_transaction->target_before_balance = $tenantWallet->withdrawal_amount;
                  $withdrawal_transaction->target_after_balance = $newTenantWithdrawalAmount;
                  $withdrawal_transaction->updated_at = date('Y-m-d H:i:s');
                  $withdrawal_transaction->transaction_id = getUUIDTransactionId();
                  WithdrawalTransactions::create($withdrawal_transaction->toArray());
                }
              } catch (\Throwable $th) {
                RequestResponseLogs::create([
                  'request_json' => json_encode($jsonRequest),
                  'response_json' => json_encode([
                    'Error' => $th->getMessage(),
                    'FileName' => $th->getFile(),
                    'LineNo' => $th->getLine(),
                  ]),
                  'service' => INDIGRIT_SERVICE ,
                  'url' => $request->url(),
                  'tenant_id' => $tenant_id,
                  'response_code' => Response::HTTP_EXPECTATION_FAILED,
                  'response_status' => TRANSACTION_FAILED,
                  'error_code' => Response::HTTP_EXPECTATION_FAILED
                ]);
                Log::info([
                  'payout_Callback_withdrawal_error' => $th
                ]);
              }

          }elseif ($request->status == FAILED){
              if($transaction){
                  $status = REJECTED_BY_PAYMENT_GATEWAY;
                  $transaction->status = CANCELLED;
                  $transaction->comments = REJECTION_COMMENT;
                  $transaction->update();

                  $data = Wallets::where('id', $transaction->source_wallet_id)
                      ->lockForUpdate()
                      ->first();
                  $data->refresh();
                  $updatedAmount = $withdrawal['amount'] + $data->amount;
                  $data->amount  = $updatedAmount;
                  $data->save();
                  // User::storeElasticSearchData($withdrawal['user_id']);
                     //Entry in queueLog
               $queueLog = QueueLog::create([
                'type' => USER_TRANSACTION,
                'ids' => json_encode([$withdrawal['user_id']])
            ]);
            try {
                $redis = Redis::connection();
                $jsonArray=[];
                $jsonArray['QueueLog']= [
                    'queueLogId'=> $queueLog->id
                ];
                $redis->publish('QUEUE_WORKER', json_encode($jsonArray)
                );
            } catch (\Throwable $th) {
            //throw $th;
            }

              }
          }
        }
          
          if(@$withdrawal['transaction_id']){
            $transaction = Transactions::select('id', 'source_wallet_id', 'comments', 'status', 'actionee_id', 'amount', 'source_currency_id', 'target_wallet_id', 'target_currency_id', 'target_before_balance', 'target_after_balance', 'updated_at', 'transaction_id')->find($withdrawal['transaction_id']);
              // Transactions::storeElasticSearchData($withdrawal['transaction_id']);
               //Entry in queueLog
               $queueLog = QueueLog::create([
                'type' => CASINO_TRANSACTION,
                'ids' => json_encode([$transaction->id])
            ]);
            try {
                $redis = Redis::connection();
                $jsonArray=[];
                $jsonArray['QueueLog']= [
                    'queueLogId'=> $queueLog->id
                ];
                $redis->publish('QUEUE_WORKER', json_encode($jsonArray)
                );
            } catch (\Throwable $th) {
            //throw $th;
            }

              $receiverId = $withdrawal['user_id'];
              $receiverType = USER_TYPE;
              $message = "Your withdrawal request of ".$withdrawal['amount']." has been ".$status;
              $id = NotificationService::addNotification($receiverId, $receiverType, $message);

             try {
              $redis = Redis::connection();
              $jsonArray=[];
              $jsonArray['PlayerNotification']= [
                  'message' => $message,
                  'userId' => $withdrawal['user_id'],
                  'senderType'=>ADMIN_TYPE,
                  'referenceId'=>$receiverId,
                  'referenceType'=>USER_TYPE,
                  'id'=>@$id->id

              ];
              $redis->publish('PLAYER_NOTIFICATION_CHANNEL', json_encode($jsonArray)
              );
             } catch (\Throwable $th) {
             }

          }

          RequestResponseLogs::create([
            'request_json' => json_encode($jsonRequest),
            'response_json' => json_encode(['message' => WITHDRAW_SUCCESS, 'status' => Response::HTTP_OK]),
            'service' => INDIGRIT_SERVICE ,
            'url' => $request->url(),
            'tenant_id' => $tenant_id,
            'response_code' => Response::HTTP_OK,
            'response_status' => 'success',
            'error_code' => Response::HTTP_OK
          ]);

          DB::commit();
          return returnResponse(true, WITHDRAW_SUCCESS, [], Response::HTTP_OK);
        
        
      } else {
          RequestResponseLogs::create([
            'request_json' => json_encode($jsonRequest),
            'response_json' => json_encode(['message' => WITHDRAW_NOT_FOUND, 'status' => Response::HTTP_EXPECTATION_FAILED]),
            'service' => INDIGRIT_SERVICE ,
            'url' => $request->url(),
            'tenant_id' => $tenant_id,
            'response_code' => Response::HTTP_EXPECTATION_FAILED,
            'response_status' => TRANSACTION_FAILED,
            'error_code' => Response::HTTP_EXPECTATION_FAILED
          ]);
        return returnResponse(false, WITHDRAW_NOT_FOUND, [], Response::HTTP_EXPECTATION_FAILED);
      }
    } catch (\Exception $e) {
      DB::rollBack();
      Log::info([
        'payout_Callback_error' => $e
      ]);
      RequestResponseLogs::create([
        'request_json' => json_encode($jsonRequest),
        'response_json' => json_encode([
          'Error' => $e->getMessage(),
          'LineNo' => $e->getLine(),
          'FileName' => $e->getFile()
        ]),
        'service' => INDIGRIT_SERVICE ,
        'url' => $request->url(),
        'tenant_id' => $tenant_id,
        'response_code' => Response::HTTP_EXPECTATION_FAILED,
        'response_status' => TRANSACTION_FAILED,
        'error_code' => Response::HTTP_EXPECTATION_FAILED
      ]);
      
      if (!App::environment(['local'])) { //, 'staging'
        return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
      } else {
        return returnResponse(false, ERROR_MESSAGE, [
          'Error' => $e->getMessage(),
          'LineNo' => $e->getLine(),
          'FileName' => $e->getFile()
        ], Response::HTTP_EXPECTATION_FAILED);
      }
    }
  }

  public function payoutStatusSky(Request $request)
  {
      $headers = $request->header();
      $body = $request->all();
      $jsonRequest = [
        'headers' => $headers,
        'body' => $body,
      ];
      DB::beginTransaction();
      $tenant_id = WithdrawRequest::on('read_db')->where('fd_transaction_id', $request->Merchant_RefID)->select('tenant_id')->first();

    try {

      $withdrawal = WithdrawRequest::on('read_db')->where('fd_transaction_id', $request->Merchant_RefID)->first();
      if ($withdrawal) {
        $status = CANCELLED;
        if ($withdrawal['status'] == WITHDRAW_APPORVED) {
          RequestResponseLogs::create([
            'request_json' => json_encode($jsonRequest),
            'response_json' => json_encode(['Error' => WITHDRAW_ALREADY_PROCCESSED]),
            'service' => SKY_SERVICE ,
            'url' => $request->url(),
            'tenant_id' => $tenant_id->tenant_id,
            'response_code' => Response::HTTP_EXPECTATION_FAILED,
            'response_status' => TRANSACTION_FAILED,
            'error_code' => Response::HTTP_EXPECTATION_FAILED
          ]);
          return returnResponse(false, WITHDRAW_ALREADY_PROCCESSED, [], Response::HTTP_EXPECTATION_FAILED);
        }
         else if ($withdrawal['status'] == PENDING_BY_GATEWAY) {

          $type = WITHDRAW_TYPE;
          
          $transaction = Transactions::select('id', 'source_wallet_id', 'comments', 'status', 'actionee_id', 'amount', 'source_currency_id', 'target_wallet_id', 'target_currency_id', 'target_before_balance', 'target_after_balance', 'updated_at', 'transaction_id')->find($withdrawal['transaction_id']);
          if (empty($transaction)) {
            RequestResponseLogs::create([
              'request_json' => json_encode($jsonRequest),
              'response_json' => json_encode(['Error' => TRANSACTION_NOT_FOUND]),
              'service' => SKY_SERVICE ,
              'url' => $request->url(),
              'tenant_id' => $tenant_id->tenant_id,
              'response_code' => Response::HTTP_NOT_FOUND,
              'response_status' => TRANSACTION_FAILED,
              'error_code' => Response::HTTP_NOT_FOUND
            ]);
            return returnResponse(false, TRANSACTION_NOT_FOUND, [], Response::HTTP_NOT_FOUND);
          }

          WithdrawRequest::find($withdrawal['id'])->update(
            ['withdrawal_type'=>$type,
            'status' => ($request->TxnStatus == 'Success' ? APPROVED : ($request->TxnStatus == 'Failed' ? REJECTED_BY_GATEWAY : PENDING_BY_GATEWAY)),
            'actioned_at' => date('Y-m-d H:i:s')]);

          $status = 'approved by Payment Gateway';
          if($request->TxnStatus == 'Success') {

              $userBonuses = UserBonus::on('read_db')->where(['user_id' => $withdrawal['user_id'], 'status' => ACTIVE])
                  ->get();
              if (count($userBonuses) > 0) {
                  foreach ($userBonuses as $key => $userBonus) {
                      $userBonusesObj = UserBonus::where(['id' => $userBonus['id']]);
                      $userBonusesObj->update(['status' => CANCELLED, 'updated_at'=>date('Y-m-d H:i:s')]);
                  }
              }

              if($transaction){
                  $transaction->status = SUCCESS;
                  $transaction->comments = APPROVED_BY_PAYMENT_GATEWAY;
                  $transaction->update();

              }

               // Update Withdrawal Amount in tenant wallet (Not use in any case)
                // Find Tenant wallet id 

              try {
                $tenantWallet = Wallets::where('owner_id',$transaction->actionee_id)->where('currency_id',$transaction->source_currency_id)->lockForUpdate()->first();
                $newTenantWithdrawalAmount = (float)$tenantWallet->withdrawal_amount + (float)$transaction->amount;
                $tenantWallet->refresh();
                $tenantWallet->withdrawal_amount = $newTenantWithdrawalAmount;
                $tenantWallet->save();
               
                 // Create Withdrawal Transaction
                $withdrawal_transaction = $transaction;
                if($withdrawal_transaction){
                  unset($withdrawal_transaction->id);
                  $withdrawal_transaction->target_wallet_id = $tenantWallet->id;
                  $withdrawal_transaction->target_currency_id = $tenantWallet->currency_id;
                  $withdrawal_transaction->target_before_balance = $tenantWallet->withdrawal_amount;
                  $withdrawal_transaction->target_after_balance = $newTenantWithdrawalAmount;
                  $withdrawal_transaction->updated_at = date('Y-m-d H:i:s');
                  $withdrawal_transaction->transaction_id = getUUIDTransactionId();
                  WithdrawalTransactions::create($withdrawal_transaction->toArray());
                }
              } catch (\Throwable $th) {
                RequestResponseLogs::create([
                  'request_json' => json_encode($jsonRequest),
                  'response_json' => json_encode([
                    'Error' => $th->getMessage(),
                    'FileName' => $th->getFile(),
                    'LineNo' => $th->getLine(),
                  ]),
                  'service' => SKY_SERVICE ,
                  'url' => $request->url(),
                  'tenant_id' => $tenant_id->tenant_id,
                  'response_code' => Response::HTTP_EXPECTATION_FAILED,
                  'response_status' => TRANSACTION_FAILED,
                  'error_code' => Response::HTTP_EXPECTATION_FAILED
                ]);
                Log::info([
                  'payout_Callback_withdrawal_error' => $th
                ]);
              }

          }elseif ($request->TxnStatus == 'Failed'){
              if($transaction){
                  $status = REJECTED_BY_PAYMENT_GATEWAY;
                  $transaction->status = CANCELLED;
                  $transaction->comments = REJECTION_COMMENT;
                  $transaction->update();

                  $data = Wallets::where('id', $transaction->source_wallet_id)
                      ->lockForUpdate()
                      ->first();    
                  $data->refresh();
                  $updatedAmount = $withdrawal['amount'] + $data->amount;
                  $data->amount  = $updatedAmount;
                  $data->save();
                  // User::storeElasticSearchData($withdrawal['user_id']);
                     //Entry in queueLog
               $queueLog = QueueLog::create([
                'type' => USER_TRANSACTION,
                'ids' => json_encode([$withdrawal['user_id']])
            ]);
            try {
                $redis = Redis::connection();
                $jsonArray=[];
                $jsonArray['QueueLog']= [
                    'queueLogId'=> $queueLog->id
                ];
                $redis->publish('QUEUE_WORKER', json_encode($jsonArray)
                );
            } catch (\Throwable $th) {
            //throw $th;
            }

              }
          }
        }
          
          if(@$withdrawal['transaction_id']){
            $transaction = Transactions::select('id', 'source_wallet_id', 'comments', 'status', 'actionee_id', 'amount', 'source_currency_id', 'target_wallet_id', 'target_currency_id', 'target_before_balance', 'target_after_balance', 'updated_at', 'transaction_id')->find($withdrawal['transaction_id']);
              // Transactions::storeElasticSearchData($withdrawal['transaction_id']);
               //Entry in queueLog
               $queueLog = QueueLog::create([
                'type' => CASINO_TRANSACTION,
                'ids' => json_encode([$transaction->id])
            ]);
            try {
                $redis = Redis::connection();
                $jsonArray=[];
                $jsonArray['QueueLog']= [
                    'queueLogId'=> $queueLog->id
                ];
                $redis->publish('QUEUE_WORKER', json_encode($jsonArray)
                );
            } catch (\Throwable $th) {
            //throw $th;
            }

              $receiverId = $withdrawal['user_id'];
              $receiverType = USER_TYPE;
              $message = "Your withdrawal request of ".$withdrawal['amount']." has been ".$status;
              $id = NotificationService::addNotification($receiverId, $receiverType, $message);

             try {
              $redis = Redis::connection();
              $jsonArray=[];
              $jsonArray['PlayerNotification']= [
                  'message' => $message,
                  'userId' => $withdrawal['user_id'],
                  'senderType'=>ADMIN_TYPE,
                  'referenceId'=>$receiverId,
                  'referenceType'=>USER_TYPE,
                  'id'=>@$id->id

              ];
              $redis->publish('PLAYER_NOTIFICATION_CHANNEL', json_encode($jsonArray)
              );
             } catch (\Throwable $th) {
             }

          }

          RequestResponseLogs::create([
            'request_json' => json_encode($jsonRequest),
            'response_json' => json_encode(['message' => WITHDRAW_SUCCESS, 'status' => Response::HTTP_OK]),
            'service' => SKY_SERVICE ,
            'url' => $request->url(),
            'tenant_id' => $tenant_id->tenant_id,
            'response_code' => Response::HTTP_OK,
            'response_status' => 'success',
            'error_code' => Response::HTTP_OK
          ]);

          DB::commit();
          return returnResponse(true, WITHDRAW_SUCCESS, [], Response::HTTP_OK);
        
        
      } else {
          RequestResponseLogs::create([
            'request_json' => json_encode($jsonRequest),
            'response_json' => json_encode(['message' => WITHDRAW_NOT_FOUND, 'status' => Response::HTTP_EXPECTATION_FAILED]),
            'service' => SKY_SERVICE ,
            'url' => $request->url(),
            'tenant_id' => $tenant_id->tenant_id,
            'response_code' => Response::HTTP_EXPECTATION_FAILED,
            'response_status' => TRANSACTION_FAILED,
            'error_code' => Response::HTTP_EXPECTATION_FAILED
          ]);
        return returnResponse(false, WITHDRAW_NOT_FOUND, [], Response::HTTP_EXPECTATION_FAILED);
      }
    } catch (\Exception $e) {
      DB::rollBack();
      Log::info([
        'payout_Callback_error' => $e
      ]);
      RequestResponseLogs::create([
        'request_json' => json_encode($jsonRequest),
        'response_json' => json_encode([
          'Error' => $e->getMessage(),
          'LineNo' => $e->getLine(),
          'FileName' => $e->getFile()
        ]),
        'service' => SKY_SERVICE ,
        'url' => $request->url(),
        'tenant_id' => $tenant_id->tenant_id,
        'response_code' => Response::HTTP_EXPECTATION_FAILED,
        'response_status' => TRANSACTION_FAILED,
        'error_code' => Response::HTTP_EXPECTATION_FAILED
      ]);
      
      if (!App::environment(['local'])) { //, 'staging'
        return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
      } else {
        return returnResponse(false, ERROR_MESSAGE, [
          'Error' => $e->getMessage(),
          'LineNo' => $e->getLine(),
          'FileName' => $e->getFile()
        ], Response::HTTP_EXPECTATION_FAILED);
      }
    }
  }
}
