<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Languages;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class LanguagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $record = Languages::all();

            return returnResponse(true, "Record get Successfully", $record);
        } catch (\Exception $e) {
            
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'code' => 'required|unique:languages',
            ]);

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), 400);
            }

            $insertData = [
                'name' => $request->name,
                'code' => $request->code,
            ];
            $createdValues = Languages::create($insertData);
            if ($createdValues) {
                return returnResponse(true, 'insert successfully.', $insertData, 200, true);
            } else {
                return returnResponse(false, 'insert unsuccessfully.', [], 403, true);
            }
        } catch (\Exception $e) {
            
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Languages $languages
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if ($id == '') {
                return returnResponse(true, "Record get Successfully", Languages::all());
            } else {
                return returnResponse(true, "Record get Successfully", Languages::find($id)->toArray(), 200, true);
            }
        } catch (\Exception $e) {
            
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Languages $languages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Languages $languages)
    {
        try {
            $languages = $languages->where('id', $request->id)->first();
            $languagesArray = $languages->toArray();
            if (!count($languagesArray)) {
                return returnResponse(false, 'record Not found', [], 404, true);
            } else {

                $input = $request->all();
                $validatorArray = [];
                $postData = [];


                if (@$input['name'] != $languagesArray['name']) {
                    $validatorArray['name'] = 'required';
                    $postData['name'] = @$input['name'];
                }
                if (@$input['code'] != $languagesArray['code']) {
                    $validatorArray['code'] = 'required|unique:languages';
                    $postData['code'] = @$input['code'];
                }
                if (count($validatorArray)) {
                    $validator = Validator::make($request->all(), $validatorArray);

                    if ($validator->fails()) {
                        return returnResponse(false, '', $validator->errors(), 400);
                    }
                } else {

                    if (count($postData)) {
                        Languages::where(['id' => $request->id])->update($postData);
                    }

                    return returnResponse(true, 'Update successfully.', Languages::find($request->id), 200, true);
                }
            }
        } catch (\Exception $e) {
        
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Languages $languages
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $languages = Languages::where('id', $id)->first();
            $languagesArray = $languages->toArray();
            if (!count($languagesArray)) {
                return returnResponse(false, 'record Not found', [], 404, true);
            } else {

                Languages::where(['id' => $id])->delete();
                return returnResponse(true, 'deleted successfully.', [], 200, true);
            }
        } catch (\Exception $e) {
           
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

}
