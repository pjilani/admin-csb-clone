<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\UserSettings;
use Illuminate\Http\Response;
use Validator;
use App\Models\UserDocuments;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use App\Services\NotificationService;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Redis;


class UserSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $record = UserSettings::all();
            return returnResponse(true, "Record get Successfully", $record);
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'players';
                if($request->type == 'update'){
                    if($request->id == 0){
                        $params->action = 'C';
                    }
                    else {
                        $params->action = 'U';
                    }
                }
                else {
                    $params->action = 'U';
                }
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'type' => 'required',
                'key' => 'required',
                'user_id' => 'required',
            ]);

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
            }

            $insertData = [
                'key' => $request->key,
                'value' => $request->value,
                'user_id' => $request->user_id,
                'updated_at' => date('Y-m-d H:i:s'),
            ];

            if($request->type == 'update'){
              if($request->id == 0){
                //Need to create the record
                $insertData['created_at'] = date('Y-m-d H:i:s');
                $createdValues = UserSettings::create($insertData);
              }else{
                // need to update the records
                $createdValues = UserSettings::where("id", $request->id)->update($insertData);
              }
            }else{
              //need to reset the records
              $insertData['value'] = null;
              $insertData['updated_at'] = null;
              $createdValues = UserSettings::where("id", $request->id)->update($insertData);
            }

            // $insertData = [
            //     'key' => $request->key,
            //     'value' => $request->value,
            //     'duration' => $request->duration,
            //     'user_id' => $request->user_id,
            //     'description' => $request->description,
            // ];

            // $userSettings = UserSettings::where(['key' => $request->key, 'user_id' => $request->user_id])->first();
            // if (!empty($userSettings)) {
            //     return returnResponse(false, 'Setting insert unsuccessfully (Already exist).', [], Response::HTTP_FAILED_DEPENDENCY, true);
            // }

            // $createdValues = UserSettings::create($insertData);
            if ($createdValues) {
                return returnResponse(true, 'Limit updated successfully.', $insertData, Response::HTTP_OK, true);
            } else {
                return returnResponse(false, 'Limit not updated.', [], Response::HTTP_FAILED_DEPENDENCY, true);
            }
        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id = '')
    {
        try {
            if ($id) {
                return returnResponse(true, "Record get Successfully", UserSettings::find($id)->toArray(), Response::HTTP_OK, true);
            }
            throw new \Exception('Record not found ');
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        try {
            $userSettings = new UserSettings();

            $userSettings = $userSettings->where('id', $request->id)->first();


            if ($userSettings && $userSettings->toArray() && empty($userSettings)) {
                throw new \Exception('Record not found ');
            } else {
               
                $userSettings = UserSettings::where(['key' => $request->key, 'user_id' => $request->user_id])->whereNotIn('id', [$request->id])->first();
                if (!empty($userSettings)) {
                    return returnResponse(false, 'Setting insert unsuccessfully (Already exist).', [], Response::HTTP_FAILED_DEPENDENCY, true);
                }

                $updateData = [
                    'key' => $request->key,
                    'value' => $request->value,
                    'duration' => $request->duration,
                    'description' => @$request->description,
                ];

                UserSettings::where(['id' => $request->id, 'user_id' => $request->user_id])->update($updateData);

                return returnResponse(true, 'Update successfully.', UserSettings::find($request->id), Response::HTTP_OK, true);
            }
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $themes = UserSettings::where('id', $id)->first();
            $themesArray = $themes->toArray();
            if (!count($themesArray)) {
                return returnResponse(false, 'record Not found', [], Response::HTTP_NOT_FOUND, true);
            } else {

                UserSettings::where(['id' => $id])->delete();
                return returnResponse(true, 'deleted successfully.', [], Response::HTTP_OK, true);
            }

        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function documentsStatus(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                'document_id' => 'required',
                'status' => 'required',
            ]);

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
            } else {
                $input = $request->all();
                $userPlayer=User::find($request->user_id);
            }
            if ($input['status'] == "rejected") {

                if (@$input['reason'] == '') {
                    return returnResponse(false, '', ["status" => [
                        "If Status is rejected so Please add reason"
                    ]], Response::HTTP_BAD_REQUEST);
                }
                $message = "$userPlayer->first_name $userPlayer->last_name document is rejected by " . Auth::user()->first_name . " " . Auth::user()->last_name;

            } else {
                $message = "$userPlayer->first_name $userPlayer->last_name document is approved by " . Auth::user()->first_name . " " . Auth::user()->last_name;
            }

            $receiverId = $request->user_id;
            $receiverType = USER_TYPE;
            NotificationService::addNotification($receiverId, $receiverType, $message);
            $userDocuments = UserDocuments::where('id', $input['document_id'])
                ->where('user_id', $input['user_id'])
                ->where('status', "pending")
                ->first();

            if (!$userDocuments) {
                return returnResponse(false, MESSAGE_RECORD_NOT_FOUND, [], Response::HTTP_NOT_FOUND, true);
            } else {
                $updateArray = [];

                $updateArray['status'] = $input['status'];
                if($updateArray['status']=="approved"){
                    $updateArray['is_verified'] = true;
                }

                if ($input['status'] == "rejected") {
                    $updateArray['reason'] = $input['reason'];
                    $updateArray['is_verified'] = false;
                }
                $userDocuments::where('id', $input['document_id'])
                    ->where('user_id', $input['user_id'])
                    ->update($updateArray);

                try {
                  $redis = Redis::connection();

                  $redis->publish('PLAYER_NOTIFICATION_CHANNEL_'.$input['user_id'], json_encode([
                  // $redis->set('PLAYER_NOTIFICATION_CHANNEL_'.$input['user_id'], json_encode([
                          'senderId' => Auth::User()->id,
                          'senderType' => ADMIN_TYPE,
                          'referenceType' => 'UserDocument',
                          'referenceId' => $input['document_id'],
                          'message' => $message
                      ])
                  );
                  } catch (\Throwable $th) {
                  //throw $th;
                }

                return returnResponse(true, MESSAGE_UPDATED, [], Response::HTTP_OK, true);
            }

        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                'LineNo' => $e->getLine(),
                'FileName' => $e->getFile()
            ], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }
}
