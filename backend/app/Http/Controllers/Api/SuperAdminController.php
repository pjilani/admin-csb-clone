<?php

namespace App\Http\Controllers\Api;

use App\Aws3;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\BetTransaction;
use App\Models\Currencies;
use App\Models\DepositWithdrawJob;
use App\Models\Menu\MenuTenantSettingModel;
use App\Models\PageBanners;
use App\Models\QueueLog;
use App\Models\Reports\TenantReportConfigurations;
use App\Models\RequestResponseLogs;
use App\Models\RequestResponseClone;
use App\Models\Sport\TenantSportsBetSetting;
use App\Models\Super;
use App\Models\SuperMailConfiguration;
use App\Models\TenantBlockedSports;
use App\Models\TenantConfigurations;
use App\Models\TenantCredentials;
use App\Models\TenantCredentialsKeys;
use App\Models\Tenants;
use App\Models\TenantThemeSettings;
use App\Models\Transactions;
use App\Models\User;
use App\Services\SportsService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use League\Csv\Reader;
use Validator;

class SuperAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function listAdmins(Request $request)
    {
      
        try {
          
            $page = $request->has('page') ? $request->post('page') : 1;
            $limit = $request->has('size') ? $request->post('size') : 10;


            $filter['name'] = $request->has('name') ? $request->post('name') : '';
            $filter['role'] = $request->has('role') ? $request->post('role') : '';
            // $filter['active'] = $request->has('active') ? $request->post('active') : '';
            $id = 0;
            if($request->id && $request->id != ""){
                $id = $request->id;
            }
            $record = Super::getList($limit, $page, $filter,$id);


            return returnResponse(true, "Record get Successfully", $record);
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }


    public function getMailConfiguration(Request $request)
    {
      
        try {
          
           
            $record = SuperMailConfiguration::getMailConfiguration();


            return returnResponse(true, "Record get Successfully", $record);
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    public function updateMailConfiguration(Request $request)
    {
      
        try {
          
           $data = $request->all();
           if($data){
            $record = SuperMailConfiguration::updateMailConfiguration($data);
            return returnResponse(true, "Mail Configuration Updated Successfully", $record);
          }else{
             return returnResponse(false, "Something Went Wrong");

           }
            
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }
   
   
    public function roleList(Request $request)
    {
        try {
           

            $record = DB::table('super_roles')->get();;


            return returnResponse(true, "Record get Successfully", $record);
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:super_admin_users',
            'password' => 'required',
            'role' => 'required|int'

        ]);

        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), 400);
        }

        $isSaveALl = false;
        DB::beginTransaction();
        try {

            $parent_type = "Manager";
            if($request->role == 1){
              $parent_type = "SuperAdminUser";
            }
            $password = '';
            if ($request->password && $request->password != '') {
                $password = bcrypt(base64_decode($request->password));
            }
            $insertData = [
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'parent_type' => $parent_type,
                'encrypted_password' => $password,
                'parent_id' => Auth::user()->id,
                'created_at' => date('Y-m-d H:i:s'),
            ];

            $createdValues = Super::create($insertData);
            if ($createdValues) {
                $insertedAdminId = $createdValues->id;

              

                $super_admin_role=[
                    "super_admin_user_id"=>$insertedAdminId,
                    "super_role_id" =>$request->role
                ];
                DB::table('super_admin_users_super_roles')->insert($super_admin_role);
                $isSaveALl = true;
            }

        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
        DB::commit();
        if ($isSaveALl) {
            return returnResponse(true, 'Manager insert successfully.', $insertData, 200, true);
        } else {
            return returnResponse(false, 'Manager insert unsuccessfully.', [], 403, true);
        }

    }


   
    /**
     * Update the specified resource in storage.  tenant_configurations,tenant_credentials,tenant_theme_settings we need to work
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Tenants $tenants
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tenants $tenants)
    {
      
        try {
            DB::beginTransaction();
            $admins = Super::where('id', $request->id)->first();
            $adminsArray = $admins->toArray();
            if (!count($adminsArray)) {
                return returnResponse(false, 'record Not found', [], 404, true);
            } else {

                $input = $request->all();
                $postData = [];
                $validatorArray = array();
                if (@$input['first_name'] != $adminsArray['first_name']) {
                    $validatorArray['first_name'] = 'required';
                    $postData['first_name'] = @$input['first_name'];
                }
                if (@$input['last_name'] != $adminsArray['last_name']) {
                    $validatorArray['last_name'] = 'required';
                    $postData['last_name'] = @$input['last_name'];
                }
                
                if (@$input['email'] != $adminsArray['email']) {
                    $validatorArray['email'] = 'required|unique:super_admin_users';
                    $postData['email'] = @$input['email'];
                }

             


                if (count($validatorArray)) {
                    $validator = Validator::make($request->all(), $validatorArray);

                    if ($validator->fails()) {
                        return returnResponse(false, '', $validator->errors(), 400);
                    }
                }

                $parent_type = "Manager";
                if($request->role == 1){
                  $parent_type = "SuperAdminUser";
                }
                 
                if ($request->password && $request->password != '') {
                  $password = bcrypt(base64_decode($request->password));
                  $postData['encrypted_password'] = $password;
                }

                // $postData['parent_type'] = $parent_type;
                // $postData['parent_id'] = Auth::user()->id;
                
                Super::where(['id' => $request->id])->update($postData);

                if($request->id) {
                  $adminId = $request->id;
                  $admins_roles = DB::table('super_admin_users_super_roles')->where('super_admin_user_id', $adminId)->first();
                  if($admins_roles){

                    // if($admins_roles->super_role_id != $request->role){
                    //   DB::table('super_admin_users_super_roles')->where("super_admin_user_id",$adminId)->update(["super_role_id" => $request->role]);
                    // }

                  }else{
                    $super_admin_role=[
                      "super_admin_user_id"=>$adminId,
                      "super_role_id" =>$request->role
                    ];
                    DB::table('super_admin_users_super_roles')->insert($super_admin_role);
                  }

              
                }
                $isSaveALl = true;
                DB::commit();

                return returnResponse(true, 'Update successfully.', [], 200, true);
            }

        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tenants $tenants
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $admins = Super::where('id', $request->id)->get();
            $adminArray = $admins->toArray();
            if (!count($adminArray)) {
                return returnResponse(false, 'record Not found', [], 404, true);
            } else {
                Super::where(['id' => $request->id])->update(['active' => $request->status]);
                return returnResponse(true, 'Manger Updated successfully.', [], 200, true);
            }
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function logList(Request $request) {
         
        try {
          
            $page = $request->has('page') ? $request->post('page') : 1;
            $limit = $request->has('size') ? $request->post('size') : 10;

            if ($page == 1) {
                $page = 0;
            }

            $query = RequestResponseLogs::on('read_db')->newQuery();

            if ($request->time_period) {
                $query->whereBetween('created_at', [getTimeZoneWiseTimeISO($request->time_period['start_date']), str_replace("000000", "999999", getTimeZoneWiseTimeISO($request->time_period['end_date']))]);
            }

            if($request->tenant_id){
                $query->where('tenant_id', $request->tenant_id);
            }

            if($request->service) {
                $query->where('service', 'ilike', '%' . $request->service . '%');

            }

            if($request->id) {
                $query->where('id', $request->id);
            }

            if($request->search) {
                $query->where('url', 'ilike', '%' . $request->search . '%');
            }

            if($request->res_status) {
                $query->where('response_status', 'ilike', '%' . $request->res_status . '%');
            }

            if($request->res_code) {
                $query->where('response_code', 'ilike', '%' . $request->res_code . '%');
            }

            if($request->error_code) {
                $query->where('error_code', 'ilike', '%' . $request->error_code . '%');
            }

            if($request->request_json) {
                $query->whereRaw("request_json::text ILIKE ?", ['%' . $request->request_json . '%']);
            }

            if($request->response_json) {
                $query->whereRaw("response_json::text ILIKE ?", ['%' . $request->response_json . '%']);
            }

            if($request->order && $request->sort_by){
                $query->orderBy($request->sort_by, $request->order);
            }

            $record = $query->paginate($limit);
            return returnResponse(true, "Record get Successfully", $record);
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    public function oldLogList(Request $request) {

        try {

            $page = $request->has('page') ? $request->post('page') : 1;
            $limit = $request->has('size') ? $request->post('size') : 10;

            if ($page == 1) {
                $page = 0;
            }

            $query = RequestResponseClone::query();

            
            if ($request->time_period) {
                $query->whereBetween('created_at', [getTimeZoneWiseTimeISO($request->time_period['start_date']), str_replace("000000", "999999", getTimeZoneWiseTimeISO($request->time_period['end_date']))]);
            }

            if($request->tenant_id){
                $query->where('tenant_id', $request->tenant_id);
            }

            if($request->service) {
                $query->where('service', 'ilike', '%' . $request->service . '%');

            }

            if($request->id) {
                $query->where('id', $request->id);    
            }

            if($request->search) {
                $query->where('url', 'ilike', '%' . $request->search . '%');    
            }

            if($request->res_status) {
                $query->where('response_status', 'ilike', '%' . $request->res_status . '%');    
            }

            if($request->res_code) {
                $query->where('response_code', 'ilike', '%' . $request->res_code . '%');    
            }

            if($request->error_code) {
                $query->where('error_code', 'ilike', '%' . $request->error_code . '%');    
            }
            
            if($request->request_json) {
                $query->whereRaw("request_json::text ILIKE ?", ['%' . $request->request_json . '%']);
            }

            if($request->response_json) {
                $query->whereRaw("response_json::text ILIKE ?", ['%' . $request->response_json . '%']);
            }
            
            if($request->order && $request->sort_by){
                $query->orderBy($request->sort_by, $request->order);
            }

            $record = $query->paginate($limit);
            return returnResponse(true, "Record get Successfully", $record);
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    public function logDownload(Request $request) {

        try {
            setUnlimited("4G");

            $page = $request->has('page') ? $request->post('page') : 1;
            $limit = $request->has('size') ? $request->post('size') : 10;

            if ($page == 1) {
                $page = 0;
            }

            $query = DB::connection('read_db')->table('request_response_log');

            if ($request->time_period) {
                $query->whereBetween('created_at', [getTimeZoneWiseTimeISO($request->time_period['start_date']), str_replace("000000", "999999", getTimeZoneWiseTimeISO($request->time_period['end_date']))]);
            }

            if($request->tenant_id){
                $query->where('tenant_id', $request->tenant_id);
            }

            if($request->service) {
                $query->where('service', 'ilike', '%' . $request->service . '%');

            }

            if($request->id) {
                $query->where('id', $request->id);
            }

            if($request->search) {
                $query->where('url', 'ilike', '%' . $request->search . '%');
            }

            if($request->res_status) {
                $query->where('response_status', 'ilike', '%' . $request->res_status . '%');
            }

            if($request->res_code) {
                $query->where('response_code', 'ilike', '%' . $request->res_code . '%');
            }

            if($request->error_code) {
                $query->where('error_code', 'ilike', '%' . $request->error_code . '%');
            }

            if($request->request_json) {
                $query->whereRaw("request_json::text ILIKE ?", ['%' . $request->request_json . '%']);
            }

            if($request->response_json) {
                $query->whereRaw("response_json::text ILIKE ?", ['%' . $request->response_json . '%']);
            }

            if($request->order && $request->sort_by){
                $query->orderBy($request->sort_by, $request->order);
            }

            $record = $query->get();

            $columns = array(
                'ID',
                'Created At',
                'Updated At',
                'Tenant Name',
                'Service',
                'Url',
                'Response Status',
                'Response Code',
                'Error Code',
                'Request Jsons',
                'Response Jsons',
            );

            $path = Storage::path('csv/');
            if (!is_dir($path)) {
                mkdir($path, 0775, true);
                chmod($path, 0775);
            }


            $fileName = time() . '.csv';
            $file = fopen($path . $fileName, 'w');
            fputcsv($file, $columns);

            $tenant = DB::connection('read_db')->table('tenants')
                ->pluck('name','id')->toArray();

            foreach ($record as $row) {
//                 du($row);
                 $row=(array)$row;


                fputcsv($file, array(

                        $row["id"],
                        ($row["created_at"]?? '-'),
                        ($row["updated_at"]?? '-'),
                        ($tenant[$row['tenant_id']] ?? '-'),
                        ($row["service"]?? '-'),
                        ($row["url"]?? '-'),
                        $row["response_status"],
                        ($row["response_code"]?? '-'),
                        ($row['error_code']?? '-'),
                        $row['request_json'],
                        $row["response_json"],
                    )
                );
            }
            fclose($file);
            // $result = ["url" => (request()->secure() ? secure_url(('storage/app/csv/' . $fileName)): url('storage/app/csv/' . $fileName))];
            $result = ["url" => secure_url('storage/app/csv/' . $fileName)];


            return returnResponse(true, "Record get Successfully", $result);
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }


    public function oldLogDownload(Request $request){
        try {
            setUnlimited("4G");

           $page = $request->has('page') ? $request->post('page') : 1;
           $limit = $request->has('size') ? $request->post('size') : 10;

           if ($page == 1) {
               $page = 0;
           }


            $query = RequestResponseClone::query();

           if ($request->time_period) {
               $query->whereBetween('created_at', [getTimeZoneWiseTimeISO($request->time_period['start_date']), str_replace("000000", "999999", getTimeZoneWiseTimeISO($request->time_period['end_date']))]);
           }

           if($request->tenant_id){
               $query->where('tenant_id', $request->tenant_id);
           }

           if($request->service) {
               $query->where('service', 'ilike', '%' . $request->service . '%');

           }

           if($request->id) {
               $query->where('id', $request->id);
           }

           if($request->search) {
               $query->where('url', 'ilike', '%' . $request->search . '%');
           }

           if($request->res_status) {
               $query->where('response_status', 'ilike', '%' . $request->res_status . '%');
           }

           if($request->res_code) {
               $query->where('response_code', 'ilike', '%' . $request->res_code . '%');
           }

           if($request->error_code) {
               $query->where('error_code', 'ilike', '%' . $request->error_code . '%');
           }

           if($request->request_json) {
               $query->whereRaw("request_json::text ILIKE ?", ['%' . $request->request_json . '%']);
           }

           if($request->response_json) {
               $query->whereRaw("response_json::text ILIKE ?", ['%' . $request->response_json . '%']);
           }

           if($request->order && $request->sort_by){
               $query->orderBy($request->sort_by, $request->order);
           }

           $record = $query->get();

           $columns = array(
               'ID',
               'Created At',
               'Updated At',
               'Tenant Name',
               'Service',
               'Url',
               'Response Status',
               'Response Code',
               'Error Code',
               'Request Jsons',
               'Response Jsons',
           );

           $path = Storage::path('csv/');
           if (!is_dir($path)) {
               mkdir($path, 0775, true);
               chmod($path, 0775);
           }


           $fileName = time() . '.csv';
           $file = fopen($path . $fileName, 'w');
           fputcsv($file, $columns);

            $tenant = DB::connection('read_db')
                ->table('tenants')
                ->pluck('name','id')->toArray();

           foreach ($record as $row) {
               // dd($row);
               // $row=(array)$row;
               $tenantName = $tenant[$row["tenant_id"]];
               fputcsv($file, array(

                       $row["id"],
                       ($row["created_at"]?? '-'),
                       ($row["updated_at"]?? '-'),
                       ($tenantName ?? '-'),
                       ($row["service"]?? '-'),
                       ($row["url"]?? '-'),
                       $row["response_status"],
                       ($row["response_code"]?? '-'),
                       ($row['error_code']?? '-'),
                       $row['request_json'],
                       $row["response_json"],
                   )
               );
           }
           fclose($file);
           // $result = ["url" => (request()->secure() ? secure_url(('storage/app/csv/' . $fileName)): url('storage/app/csv/' . $fileName))];
           $result = ["url" => secure_url('storage/app/csv/' . $fileName)];


           return returnResponse(true, "Record get Successfully", $result);
       } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    public function queueLogs(Request $request) {

        try {

            $page = $request->has('page') ? $request->post('page') : 1;
            $limit = $request->has('size') ? $request->post('size') : 10;

            if ($page == 1) {
                $page = 0;
            }

            $record = QueueLog::on('read_db')
                // ->when($request->time_period, function ($query) use ($request) {
                //     return $query->whereBetween('created_at', [
                //         getTimeZoneWiseTimeISO($request->time_period['start_date']),
                //         str_replace("000000", "999999", getTimeZoneWiseTimeISO($request->time_period['end_date']))
                //     ]);
                // })
                ->when($request->type, function ($query) use ($request) {
                    return $query->where('type', $request->type);
                })
                ->when($request->status != '', function ($query) use ($request) {
                    return $query->where('status', $request->status);
                })
                ->when($request->order && $request->sort_by, function ($query) use ($request) {
                    return $query->orderBy($request->sort_by, $request->order);
                })
                ->paginate($limit);

            return returnResponse(true, "Record get Successfully", $record);
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    public function createQueueLogs(Request $request){
        try {
            $validatorArray = [
                'type' => 'required',
                'csv' => 'required',
            ];

            if($request->csv == 'true'){
                $validatorArray['csvFile'] = 'required|file|mimes:csv,txt';
            }else{
                $validatorArray['ids'] = 'required';
            }

            $validator = Validator::make($request->all(), $validatorArray);

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), 400);
            }

            $ids = [];

            if($request->csv == 'true'){
                $csv = Reader::createFromPath($request->file('csvFile')->path());
                $csv->setDelimiter(',');

                $id_column_exists = null;

                if (empty($csv->fetchOne(0)) || count($csv->fetchOne(0)) == 0)
                {
                    return returnResponse(false, 'Invalid Csv',[], Response::HTTP_BAD_REQUEST);
                }

                foreach ($csv->fetchOne(0) as $i => $column_name) {
                    if (stripos($column_name, 'ID') !== false) $id_column_exists = true;
                }

                if (!$id_column_exists)
                {
                    return returnResponse(false, 'Invalid Csv', [], Response::HTTP_BAD_REQUEST);
                }

                $csv->setHeaderOffset(0);

                $csv = collect($csv->getRecords());

                $filteredCsv = $csv->filter(function($data) {
                    return filter_var($data['ID'], FILTER_VALIDATE_INT) !== false && $data['ID'] > 0;
                });

                $ids = $filteredCsv->pluck('ID')->all();

            }else{
                $ids = $request->ids;
            }

            $type = $request->type;

            switch ($type) {
                case 'casino_transaction':
                    $existingIds = Transactions::on('read_db')->whereIn('id', $ids)->pluck('id')->toArray();
                    break;
                case 'bet_transaction':
                    $existingIds = BetTransaction::on('read_db')->whereIn('id', $ids)->pluck('id')->toArray();
                    break;
                case 'user_transaction':
                    $existingIds = User::on('read_db')->whereIn('id', $ids)->pluck('id')->toArray();
                    break;
                case 'bulk_deposit_withdraw':
                case 'player_commission':
                    $existingIds = DepositWithdrawJob::on('read_db')->whereIn('id', $ids)->pluck('id')->toArray();
                    break;
                default:
                    $existingIds = [];
                    break;
            }

            if (!empty($existingIds)) {
                $existingIds = array_map('strval', $existingIds);
                $queueLog = QueueLog::create([
                    'type' => $type,
                    'ids' => json_encode($existingIds),
                ]);

                return returnResponse(true, 'Queue Log Inserted Successfully', $queueLog, 200, true);
            }else{
                return returnResponse(false, 'ids entered does not match,', [], 400);
            }

        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
           if (!App::environment(['local'])) {//, 'staging'
               return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
           } else {
               return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                   'LineNo' => $e->getLine(),
                   'FileName' => $e->getFile()
               ], Response::HTTP_EXPECTATION_FAILED);
           }
       }
}
