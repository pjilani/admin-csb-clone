<?php

namespace App\Http\Controllers\Api\Casino;

use App\Http\Controllers\Controller;
use App\Models\Casino\CasinoTables;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Casino\CasinoItems;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Aws3;
use App\Models\TenantThemeSettings;
use Illuminate\Support\Str;
class CasinoItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
        $params = new \stdClass();
        $params->module = 'casino_management';
        $params->action = 'R';
        $params->admin_user_id = Auth::user()->id;
        $params->tenant_id = Auth::user()->tenant_id;

        $permission = checkPermissions($params);   
        if(!$permission){ 
            return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
        }
        }
        // ================= permissions ends here =====================          
      
        $record = CasinoItems::all();
        return returnResponse(true,"Record get Successfully",$record);
    }

    public function getItems(Request $request)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'R';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 
        $record = CasinoItems::where(function ($q) use ($request) {
            if($request->search != '') {
                $q->where('name','ilike', '%' . $request->search . '%')->orWhere('uuid','ilike', '%' . $request->search . '%');
            }       
        })->where('tenant_id',Auth::user()->tenant_id)->paginate($request->size ?? 10);
        return returnResponse(true,"Record get Successfully",$record);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function create()
    {
        //
    }*/

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'C';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'active' => 'required',
            'uuid' => 'required',
            'image' => 'required',
        ]);

        if($validator->fails()){
            return returnResponse(false, '', $validator->errors() ,Response::HTTP_BAD_REQUEST);
        }
        try {
            $insertData = [

                'name'  => $request->name,
                'active'  => $request->active,
                'uuid'  => @$request->uuid,
            ];
            if(Auth::user()->parent_type=='AdminUser' && Auth::user()->tenant_id) {
                $insertData['tenant_id'] = Auth::user()->tenant_id;
            }else{
                throw new \Exception('tenant_id not found ');
            }

             // fetch Provider Id
             $uuidGameIdGameId = CasinoTables::select('cg.game_id','cg.casino_provider_id','casino_tables.table_id')->where('casino_tables.table_id',$request->uuid)->join('casino_games as cg','cg.game_id','=','casino_tables.game_id')->first();

             if($uuidGameIdGameId){
               $insertData['provider'] = $uuidGameIdGameId['casino_provider_id'];
             }else{
              throw new \Exception('No provider is assign to this Casino table');
             }

            if (@$request->file('image')) {

                $aws3 = new Aws3();

                $fileName = Str::uuid() . '____' . $request->file('image')->getClientOriginalName();
                $fileName = str_replace(' ', '_', $fileName);
                $fileNamePath = "tenants/" . $insertData['tenant_id'] . "/items/" . $fileName;

                $PathS3Key = $aws3->uploadFile($fileNamePath, $request->file('image')->getPathname(), $request->file('image')->getClientMimeType());
                $insertData['image'] = @$PathS3Key;
            }


           
            $createdValues = CasinoItems::create($insertData);
            if($createdValues)
            {
                return returnResponse(true,'insert successfully.',$createdValues,Response::HTTP_OK,true);
            }
            else{
                return returnResponse(false,'insert unsuccessfully.',[],Response::HTTP_FAILED_DEPENDENCY,true);
            }
        }catch (\Exception $e){
            return returnResponse(false, $e->getMessage(), [] ,Response::HTTP_EXPECTATION_FAILED,true);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Casino\CasinoItems  $casinoItems
     * @return \Illuminate\Http\Response
     */
    public function show($id='')
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'R';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 
        try {
            if($id==''){
              $tenant = TenantThemeSettings::where('tenant_id',Auth::user()->tenant_id)->first();
              $assigned_providers_values = explode(',',($tenant->assigned_providers));
              $data = CasinoItems::where('tenant_id',Auth::user()->tenant_id)->whereIn('provider', $assigned_providers_values)->get();
                return returnResponse(true,"Record get Successfully",$data);
            }else{
                return returnResponse(true,"Record get Successfully",CasinoItems::find($id),Response::HTTP_OK,true);
            }
        }catch (\Exception $e){
            return returnResponse(false, $e->getMessage(), [] ,Response::HTTP_EXPECTATION_FAILED,true);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Casino\CasinoProviders  $casinoProviders
     * @return \Illuminate\Http\Response
     */
   /* public function edit(CasinoProviders $casinoProviders)
    {
        //
    }*/

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Casino\CasinoItems  $casinoItems
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request, CasinoItems $casinoItems)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'U';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'active' => 'required',
            'uuid' => 'required',
            // 'image' => 'required',
        ]);

        if($validator->fails()){
            return returnResponse(false, '', $validator->errors() ,Response::HTTP_BAD_REQUEST);
        }
        try {
            $insertData = [

                'name'  => @$request->name,
                'active'  => @$request->active,
                'uuid'  => @$request->uuid,
            ];
            if(Auth::user()->parent_type=='AdminUser' && Auth::user()->tenant_id) {
                $insertData['tenant_id'] = Auth::user()->tenant_id;
            }else{
                throw new \Exception('tenant_id not found ');
            }


             // fetch Provider Id
             $uuidGameIdGameId = CasinoTables::select('cg.game_id','cg.casino_provider_id','casino_tables.table_id')->where('casino_tables.table_id',$request->uuid)->join('casino_games as cg','cg.game_id','=','casino_tables.game_id')->first();

             if($uuidGameIdGameId){
               $insertData['provider'] = $uuidGameIdGameId['casino_provider_id'];
             }else{
              throw new \Exception('No provider is assign to this Casino table');
             }

            if (@$request->file('image')) {

                $aws3 = new Aws3();

                $fileName = Str::uuid() . '____' . $request->file('image')->getClientOriginalName();
                $fileName = str_replace(' ', '_', $fileName);
                $fileNamePath = "tenants/" . $insertData['tenant_id'] . "/itema/" . $fileName;

                $PathS3Key = $aws3->uploadFile($fileNamePath, $request->file('image')->getPathname(), $request->file('image')->getClientMimeType());
                $insertData['image'] = @$PathS3Key;
            }


            $createdValues = CasinoItems::where('id',$id)->update($insertData);
            if($createdValues)
            {
                return returnResponse(true,'Update successfully.',$createdValues,Response::HTTP_OK,true);
            }
            else{
                return returnResponse(false,'Update unsuccessfully.',[],Response::HTTP_FAILED_DEPENDENCY,true);
            }
        }catch (\Exception $e){
            return returnResponse(false, $e->getMessage(), [] ,Response::HTTP_EXPECTATION_FAILED,true);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Casino\CasinoProviders  $casinoProviders
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,CasinoItems $casinoItems)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'D';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 
        $CasinoMenus=CasinoItems::where('id',$id)->first();
        if(!$CasinoMenus){
            return returnResponse(false,'record Not found',[],Response::HTTP_NOT_FOUND,true);
        }else{

            CasinoItems::where(['id'=>$id])->delete();
            return returnResponse(true,'deleted successfully.',[],Response::HTTP_OK,true);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTablesWithId()
    {

        try {
            // ================= permissions starts here =====================
            if (request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager") {
                $params = new \stdClass();
                $params->module = 'casino_management';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;

                $permission = checkPermissions($params);
                if (!$permission) {
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
            }
            // ================= permissions ends here =====================

            $tenant = TenantThemeSettings::on('read_db')->where('tenant_id', Auth::user()->tenant_id)->first();
            $assigned_providers_values = ($tenant->assigned_providers != null ? explode(',', ($tenant->assigned_providers)) : []);

            $data = CasinoTables::on('read_db')
                ->select('casino_tables.table_id', 'cg.casino_provider_id',
                    DB::raw("concat(casino_tables.name, ' - (',casino_tables.table_id,')') as uuid"))
                ->join('casino_games as cg', 'cg.game_id', '=', 'casino_tables.game_id')
                ->whereIn('cg.casino_provider_id', $assigned_providers_values)
                ->get();
            return returnResponse(true, "Record get Successfully", $data);
        } catch (\Exception $e) {
            return returnResponse(false, $e->getMessage(), [], Response::HTTP_EXPECTATION_FAILED, true);
        }

    }
}
