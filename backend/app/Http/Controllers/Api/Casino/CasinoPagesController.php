<?php

namespace App\Http\Controllers\Api\Casino;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Casino\CasinoPages;
use App\Models\Casino\CasinoPagesMenus;
use App\Models\Casino\CasinoMenuItem;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Aws3;

class CasinoPagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'R';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 
        $record = CasinoPages::all();
        return returnResponse(true, "Record get Successfully", $record);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'C';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'enabled' => 'required',
        ]);

        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
        }
        try {
            if (@$request->file('image')) {
                $aws3 = new Aws3();
                $fileFabName = Str::uuid() . '____' . $request->file('image')->getClientOriginalName();
                $fileFabName = str_replace(' ', '_', $fileFabName);
                $fileFabNamePath = "pages/".$request->id."/logo/".$fileFabName;
                $pageImagePathS3Key = $aws3->uploadFile($fileFabNamePath, $request->file('image')->getPathname(), $request->file('image')->getClientMimeType());
            }
            $insertData = [
                'top_menu_id' => $request->top_menu_id,
                'image'=> (isset($pageImagePathS3Key) ? $pageImagePathS3Key : NULL),
                'enable_instant_game' => (isset($request->enable_instant_game) && $request->enable_instant_game != 'null'  ? $request->enable_instant_game : NULL),
                'title'  => $request->title,
                'enabled'  => $request->enabled
                
            ];
            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $insertData['tenant_id'] = Auth::user()->tenant_id;
            } else {
                throw new \Exception('tenant_id not found ');
            }
            $createdValues = CasinoPages::create($insertData);
            if ($createdValues) {
                return returnResponse(true, 'insert successfully.', $createdValues, Response::HTTP_OK, true);
            } else {
                return returnResponse(false, 'insert unsuccessfully.', [], Response::HTTP_FAILED_DEPENDENCY, true);
            }
        } catch (\Exception $e) {
            return returnResponse(false, $e->getMessage(), [], Response::HTTP_EXPECTATION_FAILED, true);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Casino\CasinoPages  $CasinoPages
     * @return \Illuminate\Http\Response
     */
    public function show($id = '')
    {
        try {
               // ================= permissions starts here =====================
               if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'casino_management';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                   $params->tenant_id = Auth::user()->tenant_id;

                   $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================   
            if ($id == '') {
                return returnResponse(true, "Record get Successfully", CasinoPages::where('tenant_id', Auth::user()->tenant_id)->get());
            } else {
                $data = CasinoPages::find($id);
                $menuData = CasinoPagesMenus::with('menu')->where('page_id', $id)->get();

                if ($menuData->toArray())
                    $data->pages_manu = $menuData;

                return returnResponse(true, "Record get Successfully", $data, Response::HTTP_OK, true);
            }
        } catch (\Exception $e) {
            return returnResponse(false, $e->getMessage(), [], Response::HTTP_EXPECTATION_FAILED, true);
        }
    }

    public function pagesById($id = '')
    {
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'casino_management';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            if ($id == '') {
                return returnResponse(false, 'Top menu Id not found', [], Response::HTTP_EXPECTATION_FAILED, true);
            } else {
                $data = CasinoPages::select('pages.*', 'ci.uuid', 'ci.name')->where('pages.tenant_id', Auth::user()->tenant_id)->where('pages.top_menu_id', $id)->leftJoin('casino_items as ci', 'ci.id', '=', 'pages.enable_instant_game')->get();


                return returnResponse(true, "Record get Successfully", $data, Response::HTTP_OK, true);
            }
        } catch (\Exception $e) {
            return returnResponse(false, $e->getMessage(), [], Response::HTTP_EXPECTATION_FAILED, true);
        }
    }

    public function all()
    {
        try {
            // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'R';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 
            $data = CasinoPages::select('id', DB::raw("concat(game_id, ' - ',name) as game_name_id"))
                ->get();
            return returnResponse(true, "Record get Successfully", $data);
        } catch (\Exception $e) {
            return returnResponse(false, $e->getMessage(), [], Response::HTTP_EXPECTATION_FAILED, true);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Casino\CasinoProviders  $casinoProviders
     * @return \Illuminate\Http\Response
     */
    public function edit(CasinoPages $CasinoPages)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Casino\CasinoPages  $CasinoPages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CasinoPages $CasinoPages)
    {
        try {
            // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'U';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 
            $CasinoPages = $CasinoPages->where('id', $request->id)->first();

            if (!$CasinoPages) {
                return returnResponse(false, 'record Not found', [], Response::HTTP_NOT_FOUND, true);
            } else {
                if (@$request->file('image')) {
                    $aws3 = new Aws3();
                    $fileFabName = Str::uuid() . '____' . $request->file('image')->getClientOriginalName();
                    $fileFabName = str_replace(' ', '_', $fileFabName);
                    $fileFabNamePath = "pages/".$request->id."/logo/".$fileFabName;
                    $pageImagePathS3Key = $aws3->uploadFile($fileFabNamePath, $request->file('image')->getPathname(), $request->file('image')->getClientMimeType());
                }
                $updateData = [
                    'top_menu_id' => $request->top_menu_id,
                    'image'=> (isset($pageImagePathS3Key) ? $pageImagePathS3Key : NULL),
                    'enable_instant_game' => (isset($request->enable_instant_game) && $request->enable_instant_game != 'null'  ? $request->enable_instant_game : NULL),
                    'title'  => $request->title,
                    'enabled' => $request->enabled,
                ];
                CasinoPages::where(['id' => $request->id])->update($updateData);

                return returnResponse(true, 'Update successfully.', CasinoPages::find($request->id), Response::HTTP_OK, true);
            }
        } catch (\Exception $e) {
            return returnResponse(false, $e->getMessage(), [], Response::HTTP_EXPECTATION_FAILED, true);
        }
    }

    public function updatePageOrder(Request $request)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'U';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 
        if (empty($request->orders)) {
            return returnResponse(false, 'Orders is required', [], 403, true);
        }

        foreach ($request->orders as $key => $id) {
            CasinoPages::find($id)->update(['order' => $key + 1]);
        }

        return returnResponse(true, 'Update successfully.', [], Response::HTTP_OK, true);
    }

    public function updatePageMenuOrder(Request $request)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'U';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 
        if (empty($request->orders)) {
            return returnResponse(false, 'Orders is required', [], 403, true);
        }

        foreach ($request->orders as $key => $id) {
            CasinoPagesMenus::find($id)->update(['menu_order' => $key + 1]);
        }

        return returnResponse(true, 'Update successfully.', [], Response::HTTP_OK, true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Casino\CasinoProviders  $casinoProviders
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'D';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 
        $CasinoPages = CasinoPages::where('id', $id)->first();
        if (!$CasinoPages) {
            return returnResponse(false, 'record Not found', [], Response::HTTP_NOT_FOUND, true);
        } else {

            CasinoPages::where(['id' => $id])->delete();
            return returnResponse(true, 'deleted successfully.', [], Response::HTTP_OK, true);
        }
    }

    /**
     * @param $id
     * @param $page_menu_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showPagesMenus($id, $page_menu_id)
    {
        try {
            // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'R';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 
            if ($id == '' || $page_menu_id == '') {
                return returnResponse(false, "Pages Id Or pages menu id not properly", [], esponse::HTTP_OK, true);
            } else {
                $data = CasinoPages::find($id);
                $data->pages_manu = CasinoPagesMenus::where('page_id', $id)->where('id', $page_menu_id)->get();
                return returnResponse(true, "Record get Successfully", $data, Response::HTTP_OK, true);
            }
        } catch (\Exception $e) {
            return returnResponse(false, $e->getMessage(), [], Response::HTTP_EXPECTATION_FAILED, true);
        }
    }

    /**
     * @param $id
     * @param $page_menu_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showPagesMenusItems(Request $request)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'R';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 
        $validator = Validator::make($request->all(), [
            //                'page_id' => 'required',
            'page_menu_id' => 'required'
        ]);

        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
        }
        try {
            $input = $request->all();
            $data = MenuItems::with('items')->where('page_menu_id', $input['page_menu_id'])->get();
            return returnResponse(true, "Record get Successfully", $data, Response::HTTP_OK, true);
        } catch (Exception $e) {
            return returnResponse(false, $e->getMessage(), [], Response::HTTP_EXPECTATION_FAILED, true);
        }
    }

    /**
     * @param $id
     * @param $page_menu_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function createPagesMenus(Request $request)
    {
        try {
            // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'C';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'page_id' => 'required',
                'casino_menu_id' => 'required',
            ]);

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
            } else {


                $updateData = [
                    'name' => $request->name,
                    'casino_menu_id' => $request->casino_menu_id,
                    'page_id' => $request->page_id,
                ];

                // CasinoPagesMenus::where(['id' => $request->id])->update($updateData);
                $pmenu = CasinoPagesMenus::create($updateData);

                return returnResponse(true, 'add successfully.', CasinoPagesMenus::with('menu')->find($pmenu->id), Response::HTTP_OK, true);
            }
        } catch (\Exception $e) {
            return returnResponse(false, $e->getMessage(), [], Response::HTTP_EXPECTATION_FAILED, true);
        }
    }

    public function showPageMenu($id, $page_menu_id)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'R';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 
        if ($id == '' || $page_menu_id == '') {
            return returnResponse(false, "Pages Id Or pages menu id not properly", [], Response::HTTP_OK, true);
        } else {
            $data = CasinoPages::find($id);
            $data->page_menu = CasinoPagesMenus::with(['menu', 'items', 'items.item'])->where(['page_id' => $id, 'id' => $page_menu_id])->first();
            return returnResponse(true, "Record get Successfully", $data, Response::HTTP_OK, true);
        }
    }

    public function showPageMenuItems(Request $request)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'R';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 
        if ($request->menu_id == '') {
            return returnResponse(false, "pages menu id not properly", [], Response::HTTP_OK, true);
        } else {
            $data = CasinoMenuItem::with(['item'])
                ->where(function ($q) use ($request) {
                    if ($request->search != '') {
                        $q->whereHas('item', function ($r) use ($request) {
                            $r->where('uuid', 'ilike', '%' . $request->search . '%')
                                ->orWhere('name', 'ilike', '%' . $request->search . '%');
                        })->orWhere('name', 'ilike', '%' . $request->search . '%');
                    }
                })->where([
                    'page_menu_id' => $request->menu_id,
                    'active' => true
                ])->paginate($request->size ?? 10);
                
            return returnResponse(true, "Record get Successfully", $data, Response::HTTP_OK, true);
        }
    }

    /**
     * @param $id
     * @param $page_menu_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePagesMenus($page_manu_id, Request $request)
    {
        try {
            // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'U';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'page_id' => 'required',
                'casino_menu_id' => 'required',
            ]);

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
            } else {

                $casinoPagesMenus = CasinoPagesMenus::find($page_manu_id);

                if (empty($casinoPagesMenus)) {
                    return returnResponse(false, 'record Not found', [], Response::HTTP_NOT_FOUND, true);
                } else {
                    $updateData = [
                        'name' => $request->name,
                        'casino_menu_id' => $request->casino_menu_id,
                        'page_id' => $request->page_id,
                    ];

                    $upmenu = CasinoPagesMenus::find($page_manu_id)->update($updateData);

                    return returnResponse(true, 'Update successfully.', CasinoPagesMenus::find($page_manu_id), Response::HTTP_OK, true);
                }
            }
        } catch (\Exception $e) {
            return returnResponse(false, $e->getMessage(), [], Response::HTTP_EXPECTATION_FAILED, true);
        }
    }

    /**
     * @param $page_manu_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deletePagesMenus($page_manu_id)
    {
        try {
            // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'D';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 
            $casinoPagesMenus = CasinoPagesMenus::find($page_manu_id);
            if (!$casinoPagesMenus) {
                return returnResponse(false, 'record Not found', [], Response::HTTP_NOT_FOUND, true);
            } else {

                CasinoPagesMenus::where(['id' => $page_manu_id])->delete();
                return returnResponse(true, 'Deleted successfully.', [], Response::HTTP_OK, true);
            }
        } catch (\Exception $e) {
            return returnResponse(false, $e->getMessage(), [], Response::HTTP_EXPECTATION_FAILED, true);
        }
    }

    // page menu item

    public function createPageMenuItem(Request $request)
    {
        try {
            // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'C';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'page_id' => 'required',
                'menu_id' => 'required',
                'casino_item_id' => 'required',
                'active' => 'required',
                'featured' => 'required'
            ]);

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
            } else {

                $pmenuItem = CasinoMenuItem::create([
                    'name' => $request->name,
                    'page_id' => $request->page_id,
                    'page_menu_id' => $request->menu_id,
                    'casino_item_id' => $request->casino_item_id,
                    'active' => $request->active,
                    'featured' => $request->featured
                ]);

                return returnResponse(true, 'add successfully.', CasinoMenuItem::with('item')->find($pmenuItem->id), Response::HTTP_OK, true);
            }
        } catch (\Exception $e) {
            return returnResponse(false, $e->getMessage(), [], Response::HTTP_EXPECTATION_FAILED, true);
        }
    }


    public function updatePageMenuItem($id, Request $request)
    {
        try {
            // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'U';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'page_id' => 'required',
                'menu_id' => 'required',
                'casino_item_id' => 'required',
                'active' => 'required',
                'featured' => 'required',
                'popular' => 'required'
            ]);

            if ($validator->fails()) {
                return returnResponse(false, '', $validator->errors(), Response::HTTP_BAD_REQUEST);
            } else {

                $pmitem = CasinoMenuItem::find($id);

                if (empty($pmitem)) {
                    return returnResponse(false, 'Not found', [], Response::HTTP_NOT_FOUND, true);
                }

                $pmitem->update([
                    'name' => $request->name,
                    'page_id' => $request->page_id,
                    'page_menu_id' => $request->menu_id,
                    'casino_item_id' => $request->casino_item_id,
                    'active' => $request->active,
                    'featured' => $request->featured,
                    'popular' => $request->popular
                ]);

                return returnResponse(true, 'add successfully.', CasinoMenuItem::with('item')->find($id), Response::HTTP_OK, true);
            }
        } catch (\Exception $e) {
            return returnResponse(false, $e->getMessage(), [], Response::HTTP_EXPECTATION_FAILED, true);
        }
    }

    public function updatePageMenuItemOrder(Request $request)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'U';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 
        if (empty($request->orders)) {
            return returnResponse(false, 'Orders is required', [], 403, true);
        }

        foreach ($request->orders as $key => $id) {
            CasinoMenuItem::find($id)->update(['order' => $key + 1]);
        }

        return returnResponse(true, 'Update successfully.', [], Response::HTTP_OK, true);
    }

    public function deletePageMenuItem($id)
    {
        try {
            // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'D';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 
            $casinoMenuItem = CasinoMenuItem::find($id);
            if (!$casinoMenuItem) {
                return returnResponse(false, 'record Not found', [], Response::HTTP_NOT_FOUND, true);
            } else {

                CasinoMenuItem::where(['id' => $id])->delete();
                return returnResponse(true, 'Deleted successfully.', [], Response::HTTP_OK, true);
            }
        } catch (\Exception $e) {
            return returnResponse(false, $e->getMessage(), [], Response::HTTP_EXPECTATION_FAILED, true);
        }
    }
}
