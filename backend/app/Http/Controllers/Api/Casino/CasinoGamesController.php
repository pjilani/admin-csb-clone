<?php

namespace App\Http\Controllers\Api\Casino;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Casino\CasinoGames;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Validator;
class CasinoGamesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = $request->has('page') ? $request->post('page') : 1;
        $limit = $request->has('size') ? $request->post('size') : 10;
        $search = $request->has('search') ? $request->post('search') : '';
        $filter['provider_id'] = $request->has('provider_id') ? $request->post('provider_id') : '';

        $record = CasinoGames::getList($limit, $page, $filter,$search);

        return returnResponse(true,"Record get Successfully",$record);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'casino_provider_id' => 'required|exists:casino_providers,id',
            'game_id' => 'required',
        ]);

        if($validator->fails()){
            return returnResponse(false, '', $validator->errors() ,Response::HTTP_BAD_REQUEST);
        }
        try {
            $insertData = [

                'name'  => $request->name,
                'casino_provider_id'  => $request->casino_provider_id,
                'game_id'  => $request->game_id,
            ];
            $createdValues = CasinoGames::create($insertData);
            if($createdValues)
            {
                return returnResponse(true,'insert successfully.',$createdValues,Response::HTTP_OK,true);
            }
            else{
                return returnResponse(false,'insert unsuccessfully.',[],Response::HTTP_FAILED_DEPENDENCY,true);
            }
        }catch (\Exception $e){
            return returnResponse(false, $e->getMessage(), [] ,Response::HTTP_EXPECTATION_FAILED,true);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Casino\CasinoGames  $CasinoGames
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if($id==''){
                return returnResponse(true,"Record get Successfully",CasinoGames::all());
            }else{
                return returnResponse(true,"Record get Successfully",CasinoGames::find($id),Response::HTTP_OK,true);
            }
        }catch (\Exception $e){
            return returnResponse(false, $e->getMessage(), [] ,Response::HTTP_EXPECTATION_FAILED,true);
        }
    }

    public function all($casino_provider_id='')
    {
        try {
            if($casino_provider_id!='') {
                $data = CasinoGames::select('id', 'game_id', DB::raw("concat(game_id, ' - ',name) as game_name_id"))
                    ->where('casino_provider_id', $casino_provider_id)
                    ->get();
            }else {
                $data = CasinoGames::select('id', DB::raw("concat(game_id, ' - ',name) as game_name_id"))->get();
            }
            return returnResponse(true,"Record get Successfully",$data);
        }catch (\Exception $e){
            return returnResponse(false, $e->getMessage(), [] ,Response::HTTP_EXPECTATION_FAILED,true);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Casino\CasinoProviders  $casinoProviders
     * @return \Illuminate\Http\Response
     */
    public function edit(CasinoGames $casinoGames)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Casino\CasinoGames  $casinoGames
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CasinoGames $casinoGames)
    {
        try {
            $casinoGames=$casinoGames->where('id',$request->id)->first();

            if(!$casinoGames){
                return returnResponse(false,'record Not found',[],Response::HTTP_NOT_FOUND,true);
            }else{
                $updateData = [
                    'name'  => $request->name,
                    'casino_provider_id'  => $request->casino_provider_id,
                    'game_id'  => $request->game_id,
                ];

                CasinoGames::where(['id'=>$request->id])->update($updateData);

                return returnResponse(true,'Update successfully.',CasinoGames::find($request->id),Response::HTTP_OK,true);
            }
        }catch (\Exception $e){
            return returnResponse(false, $e->getMessage(), [] ,Response::HTTP_EXPECTATION_FAILED,true);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Casino\CasinoProviders  $casinoProviders
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $casinoGames=CasinoGames::where('id',$id)->first();
        if(!$casinoGames){
            return returnResponse(false,'record Not found',[],Response::HTTP_NOT_FOUND,true);
        }else{

            CasinoGames::where(['id'=>$id])->delete();
            return returnResponse(true,'deleted successfully.',[],Response::HTTP_OK,true);
        }
    }
}
