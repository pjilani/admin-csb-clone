<?php

namespace App\Http\Controllers\Api\Casino;

use App\Http\Controllers\Controller;
use App\Models\Casino\CasinoProviders;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
class ProvidersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $record = CasinoProviders::orderBy('name', 'ASC')->get();
        return returnResponse(true,"Record get Successfully",$record);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if($validator->fails()){
            return returnResponse(false, '', $validator->errors() ,400);
        }

        $insertData = [
            'name'  => $request->name,
        ];
        $createdValues = CasinoProviders::create($insertData);
        if($createdValues)
        {
            return returnResponse(true,'insert successfully.',$insertData,200,true);
        }
        else{
            return returnResponse(false,'insert unsuccessfully.',[],403,true);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Casino\CasinoProviders  $casinoProviders
     * @return \Illuminate\Http\Response
     */
    public function show($id='',CasinoProviders $casinoProviders)
    {
        if($id==''){
            return returnResponse(true,"Record get Successfully",CasinoProviders::all());
        }else{
            $data=CasinoProviders::find($id);
            if(!$data){
                return returnResponse(false,"Record not found Successfully",[],Response::HTTP_NOT_FOUND,true);
            }else{
                $data=$data->toArray();
            }
            return returnResponse(true,"Record get Successfully",$data,Response::HTTP_OK);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Casino\CasinoProviders  $casinoProviders
     * @return \Illuminate\Http\Response
     */
    public function edit(CasinoProviders $casinoProviders)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Casino\CasinoProviders  $casinoProviders
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CasinoProviders $casinoProviders)
    {
        $providers=$casinoProviders->where('id',$request->id)->first();
        $providersArray=$providers->toArray();
        if(!count($providersArray)){
            return returnResponse(false,'record Not found',[],404,true);
        }else{
            $input=$request->all();
            $postData['name']=$input['name'];

            CasinoProviders::where(['id'=>$request->id])->update($postData);

            return returnResponse(true,'Update successfully.',CasinoProviders::find($request->id),Response::HTTP_OK,true);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Casino\CasinoProviders  $casinoProviders
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,CasinoProviders $casinoProviders)
    {
        $providers=CasinoProviders::where('id',$id)->first();

        if(!$providers){
            return returnResponse(false,'record Not found',[],404,true);
        }else{

            CasinoProviders::where(['id'=>$id])->delete();
            return returnResponse(true,'deleted successfully.',[],200,true);
        }
    }
}
