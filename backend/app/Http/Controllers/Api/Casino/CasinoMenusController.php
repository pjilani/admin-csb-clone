<?php

namespace App\Http\Controllers\Api\Casino;

use App\Http\Controllers\Controller;
use App\Models\Casino\CasinoProviders;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Casino\CasinoMenus;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\Aws3;
class CasinoMenusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'R';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 

        $record = CasinoMenus::all();
        return returnResponse(true,"Record get Successfully",$record);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function lists()
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'R';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 
        $record = CasinoMenus::select('id','name')->get();
        return returnResponse(true,"Record get Successfully",$record);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'C';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 
        $validator = Validator::make($request->all(), [
            'name' => 'required',
//            'menu_type' => 'required',
            'enabled' => 'required',
            'image' => 'required',
        ]);

        if($validator->fails()){
            return returnResponse(false, '', $validator->errors() ,Response::HTTP_BAD_REQUEST);
        }
        try {
            $insertData = [

                'name'  => $request->name,
                'enabled'  => $request->enabled,
                'menu_order'  => @$request->menu_order,
            ];
            if(Auth::user()->parent_type=='AdminUser' && Auth::user()->tenant_id) {
                $insertData['tenant_id'] = Auth::user()->tenant_id;
            }else{
                throw new \Exception('tenant_id not found ');
            }

            if (@$request->file('image')) {

                $aws3 = new Aws3();

                $fileName = Str::uuid() . '____' . $request->file('image')->getClientOriginalName();
                $fileName = str_replace(' ', '_', $fileName);
                $fileNamePath = "tenants/" . $insertData['tenant_id'] . "/menus/" . $fileName;

                $PathS3Key = $aws3->uploadFile($fileNamePath, $request->file('image')->getPathname(), $request->file('image')->getClientMimeType());
                $insertData['image_url'] = @$PathS3Key;
            }


            $createdValues = CasinoMenus::create($insertData);
            if($createdValues)
            {
                return returnResponse(true,'insert successfully.',$createdValues,Response::HTTP_OK,true);
            }
            else{
                return returnResponse(false,'insert unsuccessfully.',[],Response::HTTP_FAILED_DEPENDENCY,true);
            }
        }catch (\Exception $e){
            return returnResponse(false, $e->getMessage(), [] ,Response::HTTP_EXPECTATION_FAILED,true);
        }
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id='')
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'R';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 
        try {
            if($id==''){
                return returnResponse(true,"Record get Successfully",CasinoMenus::where('tenant_id',Auth::user()->tenant_id)->get());
            }else{
                return returnResponse(true,"Record get Successfully",CasinoMenus::find($id),Response::HTTP_OK,true);
            }
        }catch (\Exception $e){
            return returnResponse(false, $e->getMessage(), [] ,Response::HTTP_EXPECTATION_FAILED,true);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Casino\CasinoProviders  $casinoProviders
     * @return \Illuminate\Http\Response
     */
    public function edit(CasinoProviders $casinoProviders)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'U';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'enabled' => 'required',
        ]);

        if($validator->fails()){
            return returnResponse(false, '', $validator->errors() ,Response::HTTP_BAD_REQUEST);
        }
        try {
            $insertData = [

                'name'  => $request->name,
                'enabled'  => $request->enabled,
                'menu_order'  => @$request->menu_order,
            ];
            if(Auth::user()->parent_type=='AdminUser' && Auth::user()->tenant_id) {
                $insertData['tenant_id'] = Auth::user()->tenant_id;
            }else{
                throw new \Exception('tenant_id not found ');
            }

            if (@$request->file('image')) {

                $aws3 = new Aws3();

                $fileName = Str::uuid() . '____' . $request->file('image')->getClientOriginalName();
                $fileName = str_replace(' ', '_', $fileName);
                $fileNamePath = "tenants/" . $insertData['tenant_id'] . "/menus/" . $fileName;

                $PathS3Key = $aws3->uploadFile($fileNamePath, $request->file('image')->getPathname(), $request->file('image')->getClientMimeType());
                $insertData['image_url'] = @$PathS3Key;
            }


            $createdValues = CasinoMenus::where('id',$id)->update($insertData);
            if($createdValues)
            {
                return returnResponse(true,'Update successfully.',$createdValues,Response::HTTP_OK,true);
            }
            else{
                return returnResponse(false,'Update unsuccessfully.',[],Response::HTTP_FAILED_DEPENDENCY,true);
            }
        }catch (\Exception $e){
            return returnResponse(false, $e->getMessage(), [] ,Response::HTTP_EXPECTATION_FAILED,true);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
            $params = new \stdClass();
            $params->module = 'casino_management';
            $params->action = 'D';
            $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);   
            if(!$permission){ 
                return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
            }
            }
            // ================= permissions ends here ===================== 
        $CasinoMenus=CasinoMenus::where('id',$id)->first();
        if(!$CasinoMenus){
            return returnResponse(false,'record Not found',[],Response::HTTP_NOT_FOUND,true);
        }else{

            CasinoMenus::where(['id'=>$id])->delete();
            return returnResponse(true,'deleted successfully.',[],Response::HTTP_OK,true);
        }
    }
}
