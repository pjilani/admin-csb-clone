<?php

namespace App\Http\Controllers\Api\Casino;

use App\Http\Controllers\Controller;
use App\Models\Casino\CasinoProviders;
use Illuminate\Http\Request;
use App\Models\Casino\CasinoTables;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
class CasinoTablesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = $request->has('page') ? $request->post('page') : 1;
        $limit = $request->has('size') ? $request->post('size') : 10;
        $search = $request->has('search') ? $request->post('search') : '';
        $filter['game_id'] = $request->has('game_id') ? $request->post('game_id') : '';

        $record = CasinoTables::getList($limit, $page, $filter,$search);

        return returnResponse(true,"Record get Successfully",$record);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'is_lobby' => 'required',
            'game_id' => 'required',
            'table_id' => 'required',
        ]);

        if($validator->fails()){
            return returnResponse(false, '', $validator->errors() ,Response::HTTP_BAD_REQUEST);
        }
        try {
            $insertData = [

                'name'  => $request->name,
                'is_lobby'  => $request->is_lobby,
                'game_id'  => $request->game_id,
                'table_id'  => $request->table_id,
            ];
            $createdValues = CasinoTables::create($insertData);
            if($createdValues)
            {
                return returnResponse(true,'insert successfully.',$createdValues,Response::HTTP_OK,true);
            }
            else{
                return returnResponse(false,'insert unsuccessfully.',[],Response::HTTP_FAILED_DEPENDENCY,true);
            }
        }catch (\Exception $e){
            return returnResponse(false, $e->getMessage(), [] ,Response::HTTP_EXPECTATION_FAILED,true);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Casino\CasinoProviders  $casinoProviders
     * @return \Illuminate\Http\Response
     */
    public function show($id='')
    {
        try {
            if($id==''){
                return returnResponse(true,"Record get Successfully",CasinoTables::where('tenant_id',Auth::user()->tenant_id)->get());
            }else{
                return returnResponse(true,"Record get Successfully",CasinoTables::getDetailsById($id),Response::HTTP_OK,true);
            }
        }catch (\Exception $e){
            return returnResponse(false, $e->getMessage(), [] ,Response::HTTP_EXPECTATION_FAILED,true);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Casino\CasinoProviders  $casinoProviders
     * @return \Illuminate\Http\Response
     */
    public function edit(CasinoProviders $casinoProviders)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Casino\CasinoTables  $casinoTables
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CasinoTables $casinoTables)
    {
        try {
            $casinoGames=$casinoTables->where('id',$request->id)->first();

            if(!$casinoGames){
                return returnResponse(false,'record Not found',[],Response::HTTP_NOT_FOUND,true);
            }else{
                $insertData = [

                    'name'  => $request->name,
                    'is_lobby'  => $request->is_lobby,
                    'game_id'  => $request->game_id,
                    'table_id'  => $request->table_id,
                ];

                CasinoTables::where(['id'=>$request->id])->update($insertData);

                return returnResponse(true,'Update successfully.',CasinoTables::find($request->id),Response::HTTP_OK,true);
            }
        }catch (\Exception $e){
            return returnResponse(false, $e->getMessage(), [] ,Response::HTTP_EXPECTATION_FAILED,true);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Casino\CasinoTables  $casinoTables
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $casinoProviders=CasinoTables::where('id',$id)->first();
        if(!$casinoProviders){
            return returnResponse(false,'record Not found',[],Response::HTTP_NOT_FOUND,true);
        }else{

            CasinoTables::where(['id'=>$id])->delete();
            return returnResponse(true,'deleted successfully.',[],Response::HTTP_OK,true);
        }
    }
}
