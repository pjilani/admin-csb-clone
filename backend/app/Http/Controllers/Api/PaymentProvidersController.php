<?php

namespace App\Http\Controllers\Api;

use App\Aws3;
use App\Http\Controllers\Controller;
use App\Models\PaymentProviders;
use App\Models\Super;
use App\Models\Tenants;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Support\Str;
class paymentProvidersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function paymentProvidersList(Request $request)
    {
      
        try {
          
            $page = $request->has('page') ? $request->post('page') : 1;
            $limit = $request->has('size') ? $request->post('size') : 10;


            $filter['search'] = $request->has('search') ? $request->post('search') : '';
            $id = 0;
            if($request->id && $request->id != ""){
                $id = $request->id;
            }
            $record = PaymentProviders::getList($limit, $page, $filter,$id);


            return returnResponse(true, "Record get Successfully", $record);
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }
   
  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'provider_name' => 'required'

        ]);

        if ($validator->fails()) {
            return returnResponse(false, '', $validator->errors(), 400);
        }

        $isSaveALl = false;
        DB::beginTransaction();
        try {
            $values = [];
          

            foreach (json_decode($request->phaseExecutions,true)['PRE'] as $key => $value) {
              $values[] = $value['provider_keys'];
            }
            $insertData = [
                'provider_name' => $request->provider_name,
                'provider_type' => $request->provider_type,
                'active' => $request->status,
                'provider_keys' =>json_encode($values),
                'currencies' => $request->currencies,
                'created_at' => date('Y-m-d H:i:s'),
            ];


            if (@$request->file('logo')) {

              $aws3 = new Aws3();

              $fileName = Str::uuid() . '____' . $request->file('logo')->getClientOriginalName();
              $fileName = str_replace(' ', '_', $fileName);
              $fileNamePath = "tenants/" . rand('1000','9999') . "/logo/" . $fileName;

              $PathS3Key = $aws3->uploadFile($fileNamePath, $request->file('logo')->getPathname(), $request->file('logo')->getClientMimeType());
              $insertData['logo'] = @$PathS3Key;
          }



            $createdValues = PaymentProviders::create($insertData);
            if ($createdValues) {
                $isSaveALl = true;
            }

        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
        DB::commit();
        if ($isSaveALl) {
            return returnResponse(true, 'New Payment Provider created successfully.', $insertData, 200, true);
        } else {
            return returnResponse(false, 'Payment provider created  unsuccessfully.', [], 403, true);
        }

    }


   
    /**
     * Update the specified resource in storage.  tenant_configurations,tenant_credentials,tenant_theme_settings we need to work
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Tenants $tenants
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      
        try {
            DB::beginTransaction();
            $admins = PaymentProviders::where('id', $request->id)->first();
            $adminsArray = $admins->toArray();
            if (!count($adminsArray)) {
                return returnResponse(false, 'record Not found', [], 404, true);
            } else {

                $input = $request->all();
                $postData = [];
                $validatorArray = array();
                if (@$input['provider_name'] != $adminsArray['provider_name']) {
                    $validatorArray['provider_name'] = 'required';
                    $postData['provider_name'] = @$input['provider_name'];
                }
                
                

                if (count($validatorArray)) {
                    $validator = Validator::make($request->all(), $validatorArray);

                    if ($validator->fails()) {
                        return returnResponse(false, '', $validator->errors(), 400);
                    }
                }
                // dd();

                $values = [];
                foreach (json_decode($request->phaseExecutions,true)['PRE'] as $key => $value) {
                    $values[] = $value['provider_keys'];
                }

                $postData['provider_keys'] = json_encode($values);
                $postData['provider_type'] = @$input['provider_type'];
                $postData['updated_at'] = date('Y-m-d H:i:s');
                $postData['active'] = $request->status;
                $postData['currencies'] = $request->currencies;
                // dd($input);

                if (@$request->file('logo')) {
                  
                  $aws3 = new Aws3();
    
                  $fileName = Str::uuid() . '____' . $request->file('logo')->getClientOriginalName();
                  $fileName = str_replace(' ', '_', $fileName);
                  $fileNamePath = "tenants/" . rand('1000','9999') . "/logo/" . $fileName;
    
                  $PathS3Key = $aws3->uploadFile($fileNamePath, $request->file('logo')->getPathname(), $request->file('logo')->getClientMimeType());
                  $postData['logo'] = @$PathS3Key;
              }

    
                PaymentProviders::where(['id' => $request->id])->update($postData);

               
                $isSaveALl = true;
                DB::commit();

                return returnResponse(true, 'Payment provider Updated successfully.', [], 200, true);
            }

        } catch (\Exception $e) {
            DB::rollback();
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tenants $tenants
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $providers = PaymentProviders::where('id', $request->id)->get();
            $providersArray = $providers->toArray();
            if (!count($providersArray)) {
                return returnResponse(false, 'record Not found', [], 404, true);
            } else {
                // if status == false, update tenant_payment_configurations status to false
                if($request->status == false){
                    DB::table('tenant_payment_configurations')
                        ->where('provider_id', $request->id)
                        ->update(['active' => false]);
                }
                PaymentProviders::where(['id' => $request->id])->update(['active' => $request->status]);
                return returnResponse(true, 'Updated successfully.', [], 200, true);
            }
        } catch (\Exception $e) {

            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }
    

}
