<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Controller;
use App\Models\TenantCredentials;
use App\Services\ProviderFilterService;
use App\Services\UnifiedTransactionReportService as UnifiedTransactionReportService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;



class UnifiedTransactionReportController extends Controller
{
    public function getUnifiedTransactionReport(Request $request)
    {
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'unified_transaction_report';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            setUnlimited();
            $result = $this->reportData($request->all());
            $timeZone = $request->time_zone_name?? null;
            if ($timeZone) {
                foreach ($result['hits']['hits'] as $task1 => $task) {

                    $task = $task['_source'];

                    if ($task["created_at"])
                        $result['hits']['hits'][$task1]['_source']["created_at"] = getTimeZoneWiseTime($task['created_at'], $timeZone);
                }
            }
            return returnResponse(true, '', $result, Response::HTTP_OK, true);


        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }


    /**
     * @description downloadUnifiedTransactionReport
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function downloadUnifiedTransactionReport(Request $request)
    {
        setUnlimited();
        $agentId = null;
        try {
                // ================= permissions starts here =====================
            if (request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager") {
                $params = new \stdClass();
                $params->module = 'unified_transaction_report';
                $params->action = 'export';
                $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;

                $permission = checkPermissions($params);
                if (!$permission) {
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
            }
                // ================= permissions ends here =====================          

            $timeZone = $request->time_zone_name ?? null;
            $result = $this->reportData($request->all(), 'download');

            $columns = array(
                'Player Id',
                'Username',
                'Type',
                'Round ID',
                'Agent Name',
                'Creation Date',
                'Game Type',
                'Currency',
                'Bet/Withdraw',
                'Win/Deposit',
                'Balance Before',
                'After Balance',
                'Revenue',
                'Status',
                'Transaction ID',
                'Internal Tracking ID',

            );

            $path = Storage::path('csv/');
            if (!is_dir($path)) {
                mkdir($path, 0775, true);
                chmod($path, 0775);
            }


            $fileName = time() . '.csv';
            $file = fopen($path . $fileName, 'w');
            fputcsv($file, $columns);

            foreach ($result['hits']['hits'] as $task) {
                $task = $task['_source'];
                $playerDetails = $task['player_details'];
                $row['player_id'] = @$playerDetails['player_id'];
                $row['user_name'] = @$playerDetails['user_name'];
                $row["type"] = @$task['type'];
                $row["round_id"] = @$task['round_id'];
                $row['agent_name'] = @$playerDetails['agent_name'];
                $row['created_at'] = @$playerDetails['created_at'];
                $row["game_type"] = @$task['game_type'];
                $row["currency"] = @$task['player_details']['currency'];
                $row["bet_withdraw"] = @$playerDetails['deducted_amount'];
                $row["win_deposit"] = @$playerDetails['added_amount'];
                $row['before_balance'] = @$playerDetails['before_balance'];
                $row['after_balance'] = @$playerDetails['after_balance'];
                $row["revenue"] = @$task['player_details']['revenue'];
                $row["status"] = @$task['status'];
                $row['transaction_id'] = @$task['transaction_id'];
                $row['internal_tracking_id'] = @$task['internal_tracking_id'];

                if ($timeZone) {
                  if ($task["created_at"])
                      $row["created_at"] = getTimeZoneWiseTime(@$task['created_at'], $timeZone);
                  else
                      $row["created_at"] = @$task['created_at'];
              } 
                  else {
                  $row["created_at"] = @$task['created_at'];
              }
                $row["status"] = $task['status'];
                fputcsv($file, array(

                        $row["player_id"],
                        $row["user_name"],
                        $row["type"],
                        $row["round_id"],
                        $row["agent_name"],
                        $row["created_at"],
                        $row["game_type"],
                        $row["currency"],
                        $row["bet_withdraw"],
                        $row["win_deposit"],
                        $row["before_balance"],
                        $row["after_balance"],
                        $row["revenue"],
                        $row["status"],
                        $row["transaction_id"],
                        $row["internal_tracking_id"],

                    )
                );
            }
            fclose($file);
            // $result = ["url" => (request()->secure() ? secure_url(('storage/app/csv/' . $fileName)): url('storage/app/csv/' . $fileName))];
            $result = ["url" => secure_url(('storage/app/csv/' . $fileName))];

            return returnResponse(true, '', $result, Response::HTTP_OK, true);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    private function reportData($request, $download = '')
    {
        $tenant_base_currency = "EUR";
        $agentId = @$request['agent_id'] ?? null;
        $authUser = Auth::User();
        $page = $request['page'] ?? 1;
        $limit = $request['size'] ?? 10;
        $isDirectPlayer = $request['player_type'] ?? 'all';
        $searchKeyword = $request['search'] ?? '';
        $order = $request['order'] ?? 'DESC';
        $sort_by = $request['sort_by'] ?? 'created_at';
        $currency = $request['currency'] ?? null;
        $timeZone = $request['time_zone_name']?? null;
        $action_type = $request['action_type']?? null;
        if ($authUser->parent_type == 'AdminUser' && $authUser->tenant_id) {
            $roles = getRolesDetails();
            $tenantId = @$request['tenant_id']?? $authUser->tenant_id;
            $agentId = @$request['agent_id'] ?? $authUser->id;
            //condition for new sub-admin role
            if(in_array('sub-admin',$roles)) {
                $agentId =  @$request['agent_id'] ?? null;
            }
            $find_tenant_base_currency = TenantCredentials::on('read_db')->where(['key' => 'TENANT_BASE_CURRENCY', 'tenant_id' => $tenantId])->first();
            $tenant_base_currency = '';
            if (isset($find_tenant_base_currency)) {
                $tenant_base_currency = $find_tenant_base_currency['value'];
            }
        } else {
            $tenantId = $request['tenant_id'] ?? null;
        }

        $offset = ($page > 1) ? $page * $limit : 0;
        if ($page > 1) {
            $offset = $limit * ($page - 1);
        }
        $time_period = $request['datetime'] ?? [];
        $time_type = $request['time_type'] ?? 'today';
        if ($time_type != 'custom') {
            $time_period = dateConversionWithTimeZone($time_type);

            $time_period['fromdate'] = convertToUTC(@$time_period['fromdate'], $timeZone)->format('Y-m-d H:i:s.v');
            $time_period['enddate'] = convertToUTC(@$time_period['enddate'], $timeZone)->format('Y-m-d H:i:s.v');

            $time_period['fromdate'] = getTimeZoneWiseTimeISO(@$time_period['fromdate']);
            $time_period['enddate'] = getTimeZoneWiseTimeISO(@$time_period['enddate']);
        } else {
            if (!$time_period) {
                $time_period = dateConversionWithTimeZone($time_type);
                $time_period['fromdate'] = convertToUTC($time_period['fromdate'], $timeZone)->format('Y-m-d H:i:s.v');
                $time_period['enddate'] = convertToUTC($time_period['enddate'], $timeZone)->format('Y-m-d H:i:s.v');
                $time_period['start_date'] = $time_period['fromdate'];
                $time_period['end_date'] = $time_period['enddate'];

            }
            $time_period['fromdate'] = getTimeZoneWiseTimeISO(@$time_period['start_date']);
            $time_period['enddate'] = getTimeZoneWiseTimeISO(@$time_period['end_date']);
            unset($time_period['start_date'], $time_period['end_date']);
        }
        $input = [
            'limit' => $download == 'download' ? 'all' : $limit,
            'offset' => $download == 'download' ? 'all' : $offset,
            'tenant_id' => $tenantId,
            'agentId' => $agentId,
            'parent_id' => $agentId,
            'time_period' => $time_period,
            'owner_id' => $request['owner_id'] ?? null,
            'provider' => $request['game_provider'] ?? null,
            'currency' => $currency,
            'game_provider' => $request['game_provider'] ?? null,
            'game_type' => $request['game_type'] ?? null,
            'searchKeyword' => $searchKeyword,
            'isDirectPlayer' => $isDirectPlayer,
            'sortBy' => $sort_by,
            'sortOrder' => $order,
            'type' => $request['type'] ?? null,
            'internal_error_code' => $request['internal_error_code'] ?? null,
            'action_type' => $action_type,
            'tenant_base_currency' => $tenant_base_currency
        ];

        $result = UnifiedTransactionReportService::fetchResult($input);

        $resultGameProvider = ProviderFilterService::fetchResult($input);

        $result['resultGameProvider'] = $resultGameProvider['aggregations'];
        if (count($result['resultGameProvider']['game_type']['buckets'])) {
            foreach ($result['resultGameProvider']['game_type']['buckets'] as $key => $valueMain) {
                foreach ($result['resultGameProvider']['game_provider']['buckets'] as $key => $value) {
                    $sql = "SELECT  concat(cg.name , ' - ', cg.game_id ) as name FROM casino_games as cg 
                          WHERE 
                            cg.casino_provider_id = (SELECT casino_provider_id FROM casino_providers WHERE casino_providers.name = '{$valueMain["key"]}')
                          AND cg.game_id  IN (SELECT casino_games.game_id FROM casino_games WHERE casino_games.name = ?)  
                          ORDER BY cg.name ASC limit 1";
                    $ResultGetProvidersIdType = DB::connection('read_db')->select($sql, array($value['key']));
                    if (isset($result['resultGameProvider']['game_provider']['buckets'][$key]['name'])) {
                        if ($result['resultGameProvider']['game_provider']['buckets'][$key]['name'] == '') {
                            $result['resultGameProvider']['game_provider']['buckets'][$key]['name'] = @$ResultGetProvidersIdType[0]->name;
                        }
                    } else {
                        $result['resultGameProvider']['game_provider']['buckets'][$key]['name'] = @$ResultGetProvidersIdType[0]->name;
                    }
                }
            }
        }

        $result['tenant_base_currency'] = $tenant_base_currency;

        return $result;

    }

}

