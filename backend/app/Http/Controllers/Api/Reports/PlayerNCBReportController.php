<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Controller;
use App\Models\TenantCredentials;
use App\Services\PlayerNCBReportService as PlayerNCBReportService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;



class PlayerNCBReportController extends Controller
{

    /**
     * @description getplayerNCBReport
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getplayerNCBReport(Request $request)
    {
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'player_ncb_report';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            setUnlimited();
            $data = $this->ncbReportData($request->all());
            $result = $data['data'];
            $result['tenant_base_currency'] = $data['tenant_base_currency'];
            $timeZone = $request->time_zone_name?? null;
            if ($timeZone) {
                foreach ($result['hits']['hits'] as $task1 => $task) {

                    $task = $task['_source'];

                    if ($task["created_at"])
                        $result['hits']['hits'][$task1]['_source']["created_at"] = getTimeZoneWiseTime($task['created_at'], $timeZone);
                }
            }

            return returnResponse(true, '', $result, Response::HTTP_OK, true);

        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    private function ncbReportData($request, $download = '') {
      $tenant_base_currency = "EUR";
      $authUser = Auth::User();
      $page = $request['page'] ?? 1;
      $limit = $request['size'] ?? 10;
      $agentId = @$request['agent_id'] ?? null;
      $searchKeyword = $request['search'] ?? '';
      $isDirectPlayer = (int)$request['isDirectPlayer'] ?? 'all';
      $currency = $request['currency'] ?? null;
      $order_by = $request['order'] ?? 'desc';
      $sort_by = $request['sort_by'] ?? 'player_id';
      $timeZone = $request['time_zone_name']?? null;
      $time_period = $request->time_period ?? [];
      $time_type = $request->time_type ?? 'today';

      if($authUser->parent_type == 'AdminUser' && $authUser->tenant_id) {
        $roles=getRolesDetails();
        $tenantId = @$request['tenant_id']?? $authUser->tenant_id;
            $agentId = @$request['agent_id'] ?? $authUser->id;
        //condition for new sub-admin role
        if(in_array('sub-admin',$roles)) {
                $agentId = @$request['agent_id'] ??null;
        }
        $find_tenant_base_currency = TenantCredentials::on('read_db')->where(['key'=>'TENANT_BASE_CURRENCY','tenant_id'=>$tenantId])->first();
        $tenant_base_currency = '';
        if(isset($find_tenant_base_currency)){
          $tenant_base_currency = $find_tenant_base_currency['value'];
        }
    } else {
        $tenantId =$request['tenant_id'] ?? null;
    }

    $offset = ($page > 1) ? $page * $limit : 0;
    if ($page > 1) {
        $offset = $limit * ($page - 1);
    }

      $time_period = $request['datetime'] ?? [];
      $time_type = $request['time_type'] ?? 'today';

      if ($time_type != 'custom') {
          $time_period = dateConversionWithTimeZone($time_type);
          
          $time_period['fromdate'] = convertToUTC($time_period['fromdate'], $timeZone)->format('Y-m-d H:i:s.v');
          $time_period['enddate'] = convertToUTC($time_period['enddate'], $timeZone)->format('Y-m-d H:i:s.v');

          $time_period['fromdate'] = getTimeZoneWiseTimeISO($time_period['fromdate']);
          $time_period['enddate'] = getTimeZoneWiseTimeISO($time_period['enddate']);
      } else {
        if(!$time_period){
            $time_period = dateConversionWithTimeZone($time_type);
            $time_period['fromdate'] = convertToUTC($time_period['fromdate'], $timeZone)->format('Y-m-d H:i:s.v');
            $time_period['enddate'] = convertToUTC($time_period['enddate'], $timeZone)->format('Y-m-d H:i:s.v');
            $time_period['start_date'] = $time_period['fromdate'];
            $time_period['end_date'] = $time_period['enddate'];

        }
          $time_period['fromdate'] = getTimeZoneWiseTimeISO(@$time_period['start_date']);
          $time_period['enddate'] = getTimeZoneWiseTimeISO(@$time_period['end_date']);
          unset($time_period['start_date'], $time_period['end_date']);
      }

      $input = [
          'limit' => $download == 'download' ? 'all' : $limit,
          'offset' => $download == 'download' ? 'all' : $offset,
          'tenant_id' => $tenantId,
          'agentId' => $agentId,
          'isDirectPlayer' => $isDirectPlayer,
          'searchKeyword' => $searchKeyword,
          'sortBy' => $sort_by,
          'sortOrder' => $order_by,
          'currency' => $currency,
          'owner_id' => $request['owner_id'] ?? '',
          'action_type' => $request->action_type?? null,
          'time_period' => $time_period,
          'time_type' => $request->time_type?? null,
          'action_type' => $request['action_type'] ?? null,
          'tenant_base_currency' => $tenant_base_currency

      ];
      $result['tenant_base_currency'] = $tenant_base_currency;
      $result['data'] =  PlayerNCBReportService::fetchResult($input);  
      return $result;
  }

    public function downloadPlayerNCBReport(Request $request)
    {
        setUnlimited();
        $agentId = null;
        try {
             // ================= permissions starts here =====================
             if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'player_ncb_report';
                $params->action = 'export';
                $params->admin_user_id = Auth::user()->id;
                 $params->tenant_id = Auth::user()->tenant_id;

                 $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================  

            $timeZone = $request->time_zone_name?? null;

            $result = $this->ncbReportData($request->all(), 'download');
            $result = $result['data'];
            $columns = array(
                'Player Id',
                'Username',
                'RoundID',
                'Agent Name',
                'Bonus ID',
                'Creation Date',
                'Currency',
                'Action Type',
                'Amount',
                'NCB Before',
                'NCB After',
                'Transaction ID',
                'Internal Tracking ID'

            );

            $path = Storage::path('csv/');
            if (!is_dir($path)) {
                mkdir($path, 0775, true);
                chmod($path, 0775);
            }

            $fileName = time() . '.csv';
            $file = fopen($path . $fileName, 'w');
            fputcsv($file, $columns);
            
            foreach ($result['hits']['hits'] as $task) {
                $task = $task['_source'];
                $playerDetails = $task['player_details'];

                $row['player_id'] = @$playerDetails['player_id'];
                $row['player_name'] = @$playerDetails['user_name'];
                $row['round_id'] = @$task['round_id'];
                $row['agent_name'] = @$playerDetails['agent_name'];
                $row["bonus_id"] = @$task['bonus_id'];
                $row['created_at'] = @$playerDetails['created_at'];
                $row["currency"] = @$playerDetails['currency'];
                $row['transaction_type'] = @$task['transaction_type'];
                $row["amount"] = @$task['amount'];
                $row['before_balance'] = @$playerDetails['before_balance'];
                $row["after_balance"] = @$playerDetails['after_balance'];
                $row["transaction_id"] = @$task['transaction_id'];
                $row["internal_tracking_id"] = @$task['internal_tracking_id'];

                if ($timeZone) {
                  if ($task["created_at"])
                      $row["created_at"] = getTimeZoneWiseTime(@$task['created_at'], $timeZone);
                  else
                      $row["created_at"] = @$task['created_at'];

              } else {
                  $row["created_at"] = @$task['created_at'];
              }
                $row["status"] = $task['status'];
                fputcsv($file, array(

                        $row["player_id"],
                        $row["player_name"],
                        $row["round_id"],
                        $row["agent_name"],
                        $row["bonus_id"],
                        $row["created_at"],
                        $row["currency"],
                        $row["transaction_type"],
                        $row["amount"],
                        $row["before_balance"],
                        $row["after_balance"],
                        $row["transaction_id"],
                        $row["internal_tracking_id"],

                    )
                );
            }

            fclose($file);
            // $result = ["url" => (request()->secure() ? secure_url(('storage/app/csv/' . $fileName)): url('storage/app/csv/' . $fileName))];
            $result = ["url" => secure_url(('storage/app/csv/' . $fileName))];
            return returnResponse(true, '', $result, Response::HTTP_OK, true);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

}

