<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Controller;
use App\Services\AgentSportRevenueReportService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Services\ProviderFilterService;

class AgentSportRevenueReportController extends Controller
{
    /**
     * @description getAgentRevenueReport
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAgentRevenueReport(Request $request)
    {
        try {
            setUnlimited();
            $resultCalculationPrepare=[];
            $result = $this->agentRevenueReportData($request->all());

            unset($result['hits']);

            $agentId = @$request['agent_id'] ?? null;
            $authUser = Auth::User();
            if ($authUser->parent_type == 'AdminUser' && $authUser->tenant_id) {
                $agentId = @$request['agent_id'] ?? $authUser->id;
            }else{
                if(empty($agentId) || $agentId == null){
                    $agentId = @$request['owner_id'] ?? null;
                }
            }


            if($agentId!=0){

                $resultCalculationPrepare =@$result['aggregations']['reports']['buckets'];

                $getAgentAllCorrectArray =[];
                foreach ($resultCalculationPrepare as $key => $value){
                    $keyEs = explode('-',$value['key']);
                    if(@$keyEs[1]!=$agentId){
                        $keyEs3=@$keyEs[3];
                        @$getAgentAllCorrectArray[$keyEs3]['bet'][$keyEs[1]] =$value['bet']['bet_amount']['value'];
                        @$getAgentAllCorrectArray[$keyEs3]['bet_amount_in_EUR'][$keyEs[1]] =$value['bet']['bet_amount_in_EUR']['value'];
                        @$getAgentAllCorrectArray[$keyEs3]['win'][$keyEs[1]] =$value['win']['win_amount']['value'];
                        @$getAgentAllCorrectArray[$keyEs3]['win_amount_in_EUR'][$keyEs[1]] =$value['win']['win_amount_in_EUR']['value'];
                        @$getAgentAllCorrectArray[$keyEs3]['refund'][$keyEs[1]] =$value['refund']['refund_amount']['value'];
                        @$getAgentAllCorrectArray[$keyEs3]['bet_after_refund'][$keyEs[1]] =$value['bet_after_refund']['value'];
                        @$getAgentAllCorrectArray[$keyEs3]['bet_after_refund_in_EUR'][$keyEs[1]] =$value['bet_after_refund_in_EUR']['value'];
                        @$getAgentAllCorrectArray[$keyEs3]['ggr'][$keyEs[1]] = $value['ggr']['value'];
                        @$getAgentAllCorrectArray[$keyEs3]['ggr_in_EUR'][$keyEs[1]] = $value['ggr_in_EUR']['value'];
                        @$getAgentAllCorrectArray[$keyEs3]['ngr'][$keyEs[1]] = $value['ngr']['value'];
                        @$getAgentAllCorrectArray[$keyEs3]['ngr_in_EUR'][$keyEs[1]] = $value['ngr_in_EUR']['value'];
                        @$getAgentAllCorrectArray[$keyEs3]['final_bonus'][$keyEs[1]] = $value['final_bonus']['value'];
                        @$getAgentAllCorrectArray[$keyEs3]['final_bonus_in_EUR'][$keyEs[1]] = $value['final_bonus_in_EUR']['value'];
                    }
                }
                foreach ($getAgentAllCorrectArray as $key => $value){
                    foreach ($value as $key2 => $valueArray){
                        $getAgentAllCorrectArray[$key][$key2]=array_sum($valueArray);
                    }
                }

                foreach ($resultCalculationPrepare as $mainKey =>$valueArray){
                    $keyEs = explode('-',$valueArray['key']);
                    if(@$keyEs[1] == $agentId){
                        $keyEs3=@$keyEs[3];
                        if(@$getAgentAllCorrectArray[$keyEs3]){
                            $resultCalculationPrepare[$mainKey]['bet']['bet_amount']['value']=
                                @(float)$resultCalculationPrepare[$mainKey]['bet']['bet_amount']['value']-(float)$getAgentAllCorrectArray[$keyEs3]['bet'];

                            $resultCalculationPrepare[$mainKey]['bet']['bet_amount_in_EUR']['value']=
                                @(float)$resultCalculationPrepare[$mainKey]['bet']['bet_amount_in_EUR']['value']-(float)$getAgentAllCorrectArray[$keyEs3]['bet'];

                            $resultCalculationPrepare[$mainKey]['win']['win_amount']['value']=
                                @(float)$resultCalculationPrepare[$mainKey]['win']['win_amount']['value']-(float)$getAgentAllCorrectArray[$keyEs3]['win'];

                            $resultCalculationPrepare[$mainKey]['win']['win_amount_in_EUR']['value']=
                                @(float)$resultCalculationPrepare[$mainKey]['win']['win_amount_in_EUR']['value']-(float)$getAgentAllCorrectArray[$keyEs3]['win'];

                            $resultCalculationPrepare[$mainKey]['refund']['refund_amount']['value']=
                                @(float)$resultCalculationPrepare[$mainKey]['refund']['refund_amount']['value']-(float)$getAgentAllCorrectArray[$keyEs3]['refund'];
                            $resultCalculationPrepare[$mainKey]['bet_after_refund']['value']=
                                @(float)$resultCalculationPrepare[$mainKey]['bet_after_refund']['value']-(float)$getAgentAllCorrectArray[$keyEs3]['bet_after_refund'];
                            $resultCalculationPrepare[$mainKey]['bet_after_refund_in_EUR']['value'] =
                                @(float)$resultCalculationPrepare[$mainKey]['bet_after_refund_in_EUR']['value']-(float)$getAgentAllCorrectArray[$keyEs3]['bet_after_refund_in_EUR'];
                            $resultCalculationPrepare[$mainKey]['ggr']['value'] =
                                @(float)$resultCalculationPrepare[$mainKey]['ggr']['value']-(float)$getAgentAllCorrectArray[$keyEs3]['ggr'];
                            $resultCalculationPrepare[$mainKey]['ggr_in_EUR']['value'] =
                                @(float)$resultCalculationPrepare[$mainKey]['ggr_in_EUR']['value']-(float)$getAgentAllCorrectArray[$keyEs3]['ggr_in_EUR'];
                            $resultCalculationPrepare[$mainKey]['ngr']['value'] =
                                @(float)$resultCalculationPrepare[$mainKey]['ngr']['value']-(float)$getAgentAllCorrectArray[$keyEs3]['ngr'];
                            $resultCalculationPrepare[$mainKey]['ngr_in_EUR']['value']=
                                @(float)$resultCalculationPrepare[$mainKey]['ngr_in_EUR']['value']-(float)$getAgentAllCorrectArray[$keyEs3]['ngr_in_EUR'];

                            $resultCalculationPrepare[$mainKey]['final_bonus']['value']=
                                @(float)$resultCalculationPrepare[$mainKey]['final_bonus']['value']-(float)$getAgentAllCorrectArray[$keyEs3]['final_bonus'];
                            $resultCalculationPrepare[$mainKey]['final_bonus_in_EUR']['value']=
                                @(float)$resultCalculationPrepare[$mainKey]['final_bonus_in_EUR']['value']-(float)$getAgentAllCorrectArray[$keyEs3]['final_bonus_in_EUR'];
                        }
                    }
                }

            }
            $data['resultPrepare'] = $resultCalculationPrepare;
            $data['resultGameProvider'] = $result['resultGameProvider'];
            return returnResponse(true, '', $data, Response::HTTP_OK, true);


        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    /**
     * @description agentRevenueReportData
     * @param $request
     * @param string $download
     * @return array
     */
    private function agentRevenueReportData($request, $download = '')
    {

        $agentId = @$request['agent_id'] ?? null;
        $authUser = Auth::User();

        if ($authUser->parent_type == 'AdminUser' && $authUser->tenant_id) {
//            $roles = getRolesDetails();
            $tenantId = @$request['tenant_id']?? $authUser->tenant_id;
            /*if (!in_array('owner', $roles)) {
                $agentId = @$request['agent_id'] ?? $authUser->id;
            }*/
            $agentId = @$request['agent_id'] ?? $authUser->id;
        } else {
            $tenantId = $request['tenant_id'] ?? null;
             if(empty($agentId) || $agentId == null){
                    $agentId = @$request['owner_id'] ?? null;
             }

        }
        $currency = $request['currency']?? null;
        $time_period = $request['time_period'] ?? [];
        $time_type = $request['time_type'] ?? 'today';

        $timeZone = $request['time_zone_name']?? null;


        if ($time_type != 'custom') {
            $time_period = dateConversionWithTimeZone($time_type);
            $time_period['fromdate'] = getTimeZoneWiseTimeISO(getTimeZoneWiseTime($time_period['fromdate'], $timeZone));
            $time_period['enddate'] = getTimeZoneWiseTimeISO(getTimeZoneWiseTime($time_period['enddate'], $timeZone));
        } else {
            $time_period['fromdate'] = getTimeZoneWiseTimeISO(getTimeZoneWiseTime($time_period['start_date'], $timeZone));
            $time_period['enddate'] = getTimeZoneWiseTimeISO(getTimeZoneWiseTime($time_period['end_date'], $timeZone));
            unset($time_period['start_date'], $time_period['end_date']);
        }
        $input = [
            'tenant_id' => $tenantId,
            'agentId' => $agentId,
            'time_period' => $time_period,
            'owner_id' => $request['owner_id'] ?? null,
            'provider' => $request['provider'] ?? null,
            'currency'=>$currency
        ];

        $result =  AgentSportRevenueReportService::fetchResult($input);

//        $resultGameProvider = ProviderFilterService::fetchResult($input);

        $result['resultGameProvider'] = [];//$resultGameProvider['aggregations'];
      /*  if(count($result['resultGameProvider']['game_type']['buckets'])){

            foreach ( $result['resultGameProvider']['game_type']['buckets'] as $key => $value){

                $sqlGetProvidersId = "SELECT id FROM casino_providers WHERE casino_providers.name = '".$value['key']."'";
                $ResultGetProvidersId= DB::select($sqlGetProvidersId);
                foreach ( $result['resultGameProvider']['game_provider']['buckets'] as $key => $value){

                    $sqlGetProvidersIdType = "SELECT casino_games.game_id FROM casino_games 
                                WHERE casino_games.name = '".$value['key']."'";
                    $ResultGetProvidersIdType= DB::select($sqlGetProvidersIdType);
                    $ResultGetProvidersIdTypeArray = [];
                    foreach ($ResultGetProvidersIdType as $tmpValue){
                        $ResultGetProvidersIdTypeArray[]=$tmpValue->game_id;
                    }
                    $providerId = $ResultGetProvidersId[0]->id;
                    if(count($ResultGetProvidersIdTypeArray) && @$ResultGetProvidersId[0]->id) {
                        $sql = "SELECT  concat(cg.name , ' - ', cg.game_id ) as name FROM casino_games as cg 
                                WHERE 
                                  cg.casino_provider_id = $providerId
                                AND cg.game_id  IN ('" . implode("','", $ResultGetProvidersIdTypeArray) . "') 
                                ORDER BY cg.name ASC";
                        $ResultGetProvidersIdType= DB::select($sql);
                        $result['resultGameProvider']['game_provider']['buckets'][$key]['name'] = $ResultGetProvidersIdType[0]->name;
                    }
                }
            }
        }*/
        return $result;
    }
}
