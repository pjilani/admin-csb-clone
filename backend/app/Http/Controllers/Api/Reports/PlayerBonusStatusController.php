<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Services\PlayerBonusStatusService as PlayerBonusStatusService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;



class PlayerBonusStatusController extends Controller
{
    public function getPlayerBonusStatusReport(Request $request)
    {
        try{
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'bonus_status_per_player_report';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            setUnlimited();
            $result = $this->playerBonusReportData($request);

            /*if($timeZone){
                foreach ($result['hits']['hits'] as $task1=>$task) {

                    $task=$task['_source'];

                    if($task["creation_date"] )
                        $result['hits']['hits'][$task1]['_source']["creation_date"] = getTimeZoneWiseTime($task['creation_date'],$timeZone);
                }
            }*/
            unset($result['hits']);
            return returnResponse(true, '', $result, Response::HTTP_OK, true);

        
    } catch (\Exception $e) {
            if(!App::environment(['local'])){//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            }else{
                return returnResponse(false, ERROR_MESSAGE, ['Error'=>$e->getMessage(),
                    'LineNo'=>$e->getLine(),
                    'FileName'=>$e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
    }

    }


    private function playerBonusReportData($request, $download = '') {

        $authUser = Auth::User();

//        $page = $request->page ?? 1;
//        $limit = $request->size ?? 10;
        $searchKeyword = $request->search ?? '';
        $isDirectPlayer = $request->player_type ?? 'all';
        $time_period = $request['datetime'] ?? [];
        $time_type = $request->time_type ?? 'today';

        $timeZone = $request->time_zone_name?? null;


        if($time_type!='custom'){
            $time_period=dateConversionWithTimeZone($time_type);

            $time_period['fromdate'] = convertToUTC($time_period['fromdate'], $timeZone)->format('Y-m-d H:i:s.v');
            $time_period['enddate'] = convertToUTC($time_period['enddate'], $timeZone)->format('Y-m-d H:i:s.v');
            
            $time_period['fromdate']= getTimeZoneWiseTimeISO($time_period['fromdate'], );
            $time_period['enddate']= getTimeZoneWiseTimeISO($time_period['enddate'], );
        }else{
            if(!$time_period){
                $time_period = dateConversionWithTimeZone($time_type);
                $time_period['fromdate'] = convertToUTC($time_period['fromdate'], $timeZone)->format('Y-m-d H:i:s.v');
                $time_period['enddate'] = convertToUTC($time_period['enddate'], $timeZone)->format('Y-m-d H:i:s.v');
                $time_period['start_date'] = $time_period['fromdate'];
                $time_period['end_date'] = $time_period['enddate'];

            }

            $time_period['fromdate']= getTimeZoneWiseTimeISO(@$time_period['start_date'], );
            $time_period['enddate']= getTimeZoneWiseTimeISO(@$time_period['end_date'], );
            unset($time_period['start_date'],$time_period['end_date']);
        }
        
        $agentId = null;

        if($authUser->parent_type=='AdminUser' && $authUser->tenant_id) { 
            $roles=getRolesDetails(); 
            $tenantId = $authUser->tenant_id;
            $agentId = $request->agent_id ?? $authUser->id;
           
            /*if(!in_array('owner',$roles) && !$agentId) {
                    $agentId = $authUser->id;
            }*/

            //condition for new sub-admin role
            if(in_array('sub-admin',$roles)) {
                    $agentId =  $request->agent_id ??null;
            }


        } else {

            $tenantId = ($request->tenant_id != '' ? $request->tenant_id : '');
            $agentId = ($request->agent_id != '' ? $request->agent_id : '');

            // $agentId = $request->agent_id ?? null;

        }

        $input = [
//            'limit' => $download == 'download' ? 'all' : $limit,
//            'offset' => $download == 'download' ? 'all' : $offset,
            'tenant_id' => $tenantId,
            'agentId' => $agentId,
            'time_period' => $time_period,
            'player_type' => $isDirectPlayer??'all',
            'search' => $searchKeyword,
//            'sortBy' => $request['sort_by'] ?? 'id',
//            'sortOrder' => $request['order'] ?? 'desc',
            'currency' => @$request->currency?@$request->currency: '',
            'owner_id' => @$request->owner_id?@$request->owner_id: '',
        ];

        return PlayerBonusStatusService::fetchResult($input);
    }

}
