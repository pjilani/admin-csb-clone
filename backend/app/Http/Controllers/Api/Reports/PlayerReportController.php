<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Controller;
use App\Models\TenantCredentials;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Models\Admin;
use App\Models\Tenants;
use App\Models\Reports\PlayerReport;
use Illuminate\Support\Facades\Storage;
use App\Services\PlayerReportService as PlayerReportService;
use App\Models\User;
use Illuminate\Support\Facades\App;
use App\Models\Currencies;



class PlayerReportController extends Controller
{

    public function TenantList(Request $request)
    {
        $limit = 20;
        $page = $request->page?? 1;
        $filter = array('active' => 1);
        $responseArry = [];
        try{
        $tenantList = Tenants::getList($limit, $page, $filter);

        if (count($tenantList) > 0) {
            $tenantID = $tenantList['data'][0]->id;
            $responseArry['tenantOwners'] = $this->tenantOwners($tenantID);
        }
        $responseArry['tenantList'] = $tenantList['data'];
        $responseArry['tenantList']['count'] = $tenantList['count'];

        return returnResponse(true, '', $responseArry);
    } catch (\Exception $e) {
        if (!App::environment(['local'])) {//, 'staging'
            return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
        } else {
            return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                'LineNo' => $e->getLine(),
                'FileName' => $e->getFile()
            ], Response::HTTP_EXPECTATION_FAILED);
        }
    }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function TenantOwnerList(Request $request)
    {
        try {
            $tenantID = $request->id?? 0;

            // if ($tenantID == 0)
            //     return returnResponse(false, 'Tenant id is required', [], 400);

            $data = $this->tenantOwners($tenantID);
            if (count($data) > 0) {
                return returnResponse(true, '', $data);
            } else {
                return returnResponse(false, 'No owner found for this tenant', [], 200);
            }
        } catch (\Exception $e) {//$e->getMessage()
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    /**
     * @description getAdminUserHierarchy
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdminUserHierarchy(Request $request)
    {
        try {

            $data = [];
            $roles = [];
            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $roles = getRolesDetails();
                $user_id = Auth::user()->id;
                if (in_array('owner', $roles, true)) {
                    $data = PlayerReport::getAdminUserHierarchy($user_id,Auth::user()->tenant_id);
                } elseif (in_array('agent', $roles, true)) {
                    $data = PlayerReport::getAdminUserHierarchy($user_id, Auth::user()->tenant_id);
                }elseif(in_array('sub-admin',$roles)){
                    
                    //condition for new sub-admin role                     
                    $user_id = getParentAgent(Auth::user()->id);                    
                    $data = PlayerReport::getAdminUserHierarchy($user_id, Auth::user()->tenant_id);
                }
            } else {
                $ownerId = $request->owner_id?? null;
                $tenantId = $request->tenant_id?? null;
                $data = PlayerReport::getSuperAdminUserHierarchy($ownerId,$tenantId);
            }

            return returnResponse(true, '', array_merge([["id"=>'0',"first_name"=>'Select Agent ']],$data));
        } catch (\Exception $e) {//$e->getMessage()
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    /**
     * @description getAdminUserHierarchy
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdminBetHierarchy(Request $request)
    {
        try {
             $ownerId = $request->owner_id?? null;
             $tenantId = $request->tenant_id?? null;
             $data = PlayerReport::getSuperAdminAllUserHierarchy($ownerId,$tenantId);

            return returnResponse(true, '', array_merge([["id"=>'0',"first_name"=>'Select Agent ']],$data));
        } catch (\Exception $e) {//$e->getMessage()
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPlayerReport(Request $request)
    {
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'player_report';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            setUnlimited();
            $timeZone = $request->time_zone_name?? null;

            $result = $this->playerReportData($request->all());
            $tenant_base_currency = $result[1];
            $result = $result[0];
            $result=$result['user'];
            $currencyArray=[];
            $exchangeRates = Currencies::on('read_db')->pluck('exchange_rate','code');
            foreach ($result['aggregations']['currencies']['buckets'] as $taskKey => $taskValue) {
                $currencySource = $exchangeRates[$tenant_base_currency];
                if($taskValue['key'] && $taskValue['key']!='')
                    $ex_rate = $exchangeRates[$taskValue['key']];

                if($taskValue['key']==$tenant_base_currency) {
                    $currencyArray['amount'][] = $taskValue['total_balance']['value'];
                }else {
                    $currencyArray['amount'][] = (float)number_format((float)$taskValue['total_balance']['value'] * ((float)$currencySource/(float)$ex_rate),4,".","");
                }
                $currencyArray['doc_count'][]=$taskValue['doc_count'];
            }

            $result['total_balance'] = @array_sum($currencyArray['amount']);
            $result['tenant_base_currency'] = $tenant_base_currency;

            if ($timeZone) {

                foreach ($result['hits']['hits'] as $task1 => $task) {

                    $task = $task['_source'];

                    if ($task["creation_date"])
                        $result['hits']['hits'][$task1]['_source']["creation_date"] = getTimeZoneWiseTime($task['creation_date'], $timeZone);

                    if ($task["last_login"])
                        $result['hits']['hits'][$task1]['_source']["last_login"] = getTimeZoneWiseTime($task['last_login'], $timeZone);
                }
            }
            return returnResponse(true, '', $result, Response::HTTP_OK, true);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    /**
     * @param $tenantID
     * @return array
     */
    private function tenantOwners($tenantID) {
    //   $all = array('first_name' => 'All', 'last_name' => '', 'tenant_id' => 0);
       
        $ownerData = PlayerReport::getTenantOwners($tenantID);

        return (count($ownerData) > 0) ? array_merge([], $ownerData) : [];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function downloadPlayerReport(Request $request) {
        setUnlimited();
        try {
            // ================= permissions starts here =====================
            if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'player_report';
                $params->action = 'export';
                $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;

                $permission = checkPermissions($params);   
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================    

            $timeZone = $request->time_zone_name?? null;

            $result = $this->playerReportData($request->all(), 'download')[0];

            $columns = array(
                'Player Id',
                'Username',
                'Nick Name',
                'Agent Name',
                'Vip Level',
                'Total Balance',
                'Currency',
                'Total Bets',
                'Total Bet Amount',
                'Country',
                'Created Date',
                'Last Login Date',
                'Status'

            );

            $path = Storage::path('csv/');
            if (!is_dir($path)) {
                mkdir($path, 0775, true);
                chmod($path, 0775);
            }


            $fileName = time() . '.csv';
            $file = fopen($path . $fileName, 'w');
            fputcsv($file, $columns);

            foreach ($result['user']['hits']['hits'] as $task) {
                $task = $task['_source'];

                $row["player_id"] = @$task['player_id'];
                $row["user_name"] = @$task['user_name'];
                $row["nick_name"] = @$task['nick_name'];
                $row["agent_name"] = @$task['agent_name'];
                $row["vip_level"] = @$task['vip_level'];
                $row["total_balance"] = @$task['total_balance'];
                $row["currency"] = @$task['currency'];
                $row["total_bets"] = @$task['total_bets'];
                $row["total_bet_amount"] = @$task['total_bet_amount'];
                $row["country"] = @$task['country'];


                if ($timeZone) {
                    if (@$task["creation_date"])
                        $row["creation_date"] = getTimeZoneWiseTime(@$task['creation_date'], $timeZone);
                    else
                        $row["creation_date"] = @$task['creation_date'];

                    if (@$task["last_login"])
                        $row["last_login"] = getTimeZoneWiseTime(@$task['last_login'], $timeZone);
                    else
                        $row["last_login"] = @$task['last_login'];

                } else {
                    $row["creation_date"] = @$task['creation_date'];
                    $row["last_login"] = @$task['last_login'];

                }
                $row["status"] = @$task['status'];
                fputcsv($file, array(

                        $row["player_id"],
                        $row["user_name"],
                        $row["nick_name"],
                        $row["agent_name"],
                        $row["vip_level"],
                        $row["total_balance"],
                        $row["currency"],
                        $row["total_bets"],
                        $row["total_bet_amount"],
                        $row["country"],
                        $row["creation_date"],
                        $row["last_login"],
                        $row["status"]
                    )
                );
            }
            fclose($file);
            // $result = ["url" => (request()->secure() ? secure_url(('storage/app/csv/' . $fileName)): url('storage/app/csv/' . $fileName))];
            $result = ["url" => secure_url(('storage/app/csv/' . $fileName))];
            return returnResponse(true, '', $result, Response::HTTP_OK, true);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }


    private function playerReportData($request, $download = '') {
        setUnlimited();
        $tenant_base_currency = "EUR";
        $agentId = null;
        $page = $request['page'] ?? 1;
        $limit = $request['size'] ?? 10;
        $searchKeyword = $request['search'] ?? '';
        $action_type = $request['action_type'] ?? '';
        $isDirectPlayer = (int)$request['isDirectPlayer'] ?? 0;

        $currency = $request['currency'] ?? null;
        $order_by = $request['order'] ?? 'desc';
        $sort_by = $request['sort_by'] ?? 'player_id';
        
        $timeZone = $request['time_zone_name']?? null;
        $time_period = $request['datetime'] ?? [];
        $time_type = $request['time_type'] ?? 'today';
  
  
        if($time_type!='custom'){
            $time_period=dateConversionWithTimeZone($time_type);
            
            $time_period['fromdate'] = convertToUTC(@$time_period['fromdate'], $timeZone)->format('Y-m-d H:i:s.v');
            $time_period['enddate'] = convertToUTC(@$time_period['enddate'], $timeZone)->format('Y-m-d H:i:s.v');
  
            $time_period['fromdate']= getTimeZoneWiseTimeISO(@$time_period['fromdate']);
            $time_period['enddate']= getTimeZoneWiseTimeISO(@$time_period['enddate']);
        }else{
            if(!$time_period){
                $time_period = dateConversionWithTimeZone($time_type);
                $time_period['fromdate'] = convertToUTC($time_period['fromdate'], $timeZone)->format('Y-m-d H:i:s.v');
                $time_period['enddate'] = convertToUTC($time_period['enddate'], $timeZone)->format('Y-m-d H:i:s.v');
                $time_period['start_date'] = $time_period['fromdate'];
                $time_period['end_date'] = $time_period['enddate'];

            }
            $time_period['fromdate']= getTimeZoneWiseTimeISO(@$time_period['start_date']);
            $time_period['enddate']= getTimeZoneWiseTimeISO(@$time_period['end_date']);
            unset($time_period['start_date'],$time_period['end_date']);
        }

        $agentIds = null;

        $authUser = Auth::User();

        if ($authUser->parent_type == 'AdminUser' && $authUser->tenant_id) {
            $roles = getRolesDetails();
            $tenantId =@$authUser->tenant_id;
            $find_tenant_base_currency = TenantCredentials::on('read_db')->where(['key'=>'TENANT_BASE_CURRENCY','tenant_id'=>$tenantId])->first();
            $tenant_base_currency = '';
            if(isset($find_tenant_base_currency)){
              $tenant_base_currency = $find_tenant_base_currency['value'];
            }
            if (!in_array('owner', $roles)) {
                //condition for new sub-admin role
                if(in_array('sub-admin',$roles)) {
                    $agentId = $request['agent_id'] ?? null;
                } else {
                    $agentId = $request['agent_id'] ?? @Auth::user()->id;
                }
            } else {
                $agentId = $request['agent_id'] ?? null;
            }


        } else {
            $tenantId = $request['tenant_id'] ?? null;
            $agentId = $request['agent_id'] ?? null;
        }

        // $timeZone = $request['time_zone_name'] ?? null;

        $offset = ($page > 1) ? $page * $limit : 0;

        if ($page > 1) {
            $offset = $limit * ($page - 1);
        }

        $owner_id = $request['owner_id'] ? $request['owner_id'] : null;
        $input = [
            'limit' => $download == 'download' ? 'all' : $limit,
            'offset' => $download == 'download' ? 'all' : $offset,
            'tenant_id' => $tenantId,
            'time_period' => $time_period,
            'agent_id' => $agentId,
            'isDirectPlayer' => $isDirectPlayer,
            'searchKeyword' => $searchKeyword,
            'sortBy' => $sort_by,
            'actionType' => $action_type,
            'sortOrder' => $order_by,
            'currency' => $currency,
            'owner_id' => $owner_id,
        ];

        if ($isDirectPlayer == "0" && $agentId) {
            $input['agentIdTop'] = $agentId;
        } elseif ($isDirectPlayer == "0" && $owner_id) {
            $input['agentIdTop'] = $owner_id;
        }

        return [PlayerReportService::fetchResult($input), $tenant_base_currency];
    }

}
