<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Controller;
use App\Models\Currencies;
use App\Models\TenantCredentials;
use App\Services\AgentRevenueReportService as AgentRevenueReportService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Services\ProviderFilterService;
use Illuminate\Support\Facades\Storage;



class AgentRevenueReportController extends Controller
{
    public function getAgentRevenueReport(Request $request)
    {
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'casino_agent_revenue_report';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            setUnlimited();
            $tenant_base_currency = "EUR";
            $resultCalculationPrepare=[];
            $result = $this->agentRevenueReportData($request->all());

            unset($result['hits']);

            $agentId = @$request['agent_id'] ?? null;
            $authUser = Auth::User();
            if ($authUser->parent_type == 'AdminUser' && $authUser->tenant_id) {

                $agentId = @$request['agent_id'] ?? $authUser->id;
                $find_tenant_base_currency = TenantCredentials::on('read_db')->where(['key'=>'TENANT_BASE_CURRENCY','tenant_id'=>$authUser->tenant_id])->first();
                $tenant_base_currency = '';
                if(isset($find_tenant_base_currency)){
                  $tenant_base_currency = $find_tenant_base_currency['value'];
                }
                
            }else{
                if(empty($agentId) || $agentId == null){
                    $agentId = @$request['owner_id'] ?? null;
                }
            }

            // if($agentId!=0){

                $resultCalculationPrepare =@$result['aggregations']['reports']['buckets'];

                $getAgentAllCorrectArray =[];
                foreach ($resultCalculationPrepare as $key => $value){
                    $keyEs = explode('-',$value['key']);
                    if(@$keyEs[1]!=$agentId){
                        $keyEs3=@$keyEs[3];
                        @$getAgentAllCorrectArray[$keyEs3]['bet'][$keyEs[1]] =$value['bet']['bet_amount']['value'];
                        @$getAgentAllCorrectArray[$keyEs3]['bet_amount_in_EUR'][$keyEs[1]] =$value['bet']['bet_amount_in_EUR']['value'];

                        @$getAgentAllCorrectArray[$keyEs3]['win'][$keyEs[1]] =$value['win']['win_amount']['value'];

                        @$getAgentAllCorrectArray[$keyEs3]['win_amount_in_EUR'][$keyEs[1]] =$value['win']['win_amount_in_EUR']['value'];

                        @$getAgentAllCorrectArray[$keyEs3]['refund'][$keyEs[1]] =$value['refund']['refund_amount']['value'];
                        @$getAgentAllCorrectArray[$keyEs3]['bet_after_refund'][$keyEs[1]] =$value['bet_after_refund']['value'];
                        @$getAgentAllCorrectArray[$keyEs3]['bet_after_refund_in_EUR'][$keyEs[1]] =$value['bet_after_refund_in_EUR']['value'];
                        @$getAgentAllCorrectArray[$keyEs3]['ggr'][$keyEs[1]] = $value['ggr']['value'];
                        @$getAgentAllCorrectArray[$keyEs3]['ggr_in_EUR'][$keyEs[1]] = $value['ggr_in_EUR']['value'];
                        @$getAgentAllCorrectArray[$keyEs3]['ngr'][$keyEs[1]] = $value['ngr']['value'];
                        @$getAgentAllCorrectArray[$keyEs3]['ngr_in_EUR'][$keyEs[1]] = $value['ngr_in_EUR']['value'];
                        @$getAgentAllCorrectArray[$keyEs3]['final_bonus'][$keyEs[1]] = $value['final_bonus']['value'];
                        @$getAgentAllCorrectArray[$keyEs3]['final_bonus_in_EUR'][$keyEs[1]] = $value['final_bonus_in_EUR']['value'];
                    }
                }
                foreach ($getAgentAllCorrectArray as $key => $value){
                    foreach ($value as $key2 => $valueArray){
                        $getAgentAllCorrectArray[$key][$key2]=array_sum($valueArray);
                    }
                }

                foreach ($resultCalculationPrepare as $mainKey =>$valueArray){
                    $keyEs = explode('-',$valueArray['key']);
                    if(@$keyEs[1] == $agentId){
                        $keyEs3=@$keyEs[3];
                        if(@$getAgentAllCorrectArray[$keyEs3]){
                            $resultCalculationPrepare[$mainKey]['bet']['bet_amount']['value']=
                                @(float)$resultCalculationPrepare[$mainKey]['bet']['bet_amount']['value']-(float)$getAgentAllCorrectArray[$keyEs3]['bet'];

                            $resultCalculationPrepare[$mainKey]['bet']['bet_amount_in_EUR']['value']=
                                @(float)$resultCalculationPrepare[$mainKey]['bet']['bet_amount_in_EUR']['value']-(float)$getAgentAllCorrectArray[$keyEs3]['bet_amount_in_EUR'];

                            $resultCalculationPrepare[$mainKey]['win']['win_amount']['value']=
                                @(float)$resultCalculationPrepare[$mainKey]['win']['win_amount']['value']-(float)$getAgentAllCorrectArray[$keyEs3]['win'];

                            $resultCalculationPrepare[$mainKey]['win']['win_amount_in_EUR']['value']=
                                @(float)$resultCalculationPrepare[$mainKey]['win']['win_amount_in_EUR']['value']-(float)$getAgentAllCorrectArray[$keyEs3]['win_amount_in_EUR'];


                            $resultCalculationPrepare[$mainKey]['refund']['refund_amount']['value']=
                                @(float)$resultCalculationPrepare[$mainKey]['refund']['refund_amount']['value']-(float)$getAgentAllCorrectArray[$keyEs3]['refund'];
                            $resultCalculationPrepare[$mainKey]['bet_after_refund']['value']=
                                @(float)$resultCalculationPrepare[$mainKey]['bet_after_refund']['value']-(float)$getAgentAllCorrectArray[$keyEs3]['bet_after_refund'];
                            $resultCalculationPrepare[$mainKey]['bet_after_refund_in_EUR']['value'] =
                                @(float)$resultCalculationPrepare[$mainKey]['bet_after_refund_in_EUR']['value']-(float)$getAgentAllCorrectArray[$keyEs3]['bet_after_refund_in_EUR'];
                            $resultCalculationPrepare[$mainKey]['ggr']['value'] =
                                @(float)$resultCalculationPrepare[$mainKey]['ggr']['value']-(float)$getAgentAllCorrectArray[$keyEs3]['ggr'];
                            $resultCalculationPrepare[$mainKey]['ggr_in_EUR']['value'] =
                                @(float)$resultCalculationPrepare[$mainKey]['ggr_in_EUR']['value']-(float)$getAgentAllCorrectArray[$keyEs3]['ggr_in_EUR'];
                            $resultCalculationPrepare[$mainKey]['ngr']['value'] =
                                @(float)$resultCalculationPrepare[$mainKey]['ngr']['value']-(float)$getAgentAllCorrectArray[$keyEs3]['ngr'];
                            $resultCalculationPrepare[$mainKey]['ngr_in_EUR']['value']=
                                @(float)$resultCalculationPrepare[$mainKey]['ngr_in_EUR']['value']-(float)$getAgentAllCorrectArray[$keyEs3]['ngr_in_EUR'];
                            $resultCalculationPrepare[$mainKey]['final_bonus']['value']=
                                @(float)$resultCalculationPrepare[$mainKey]['final_bonus']['value']-(float)$getAgentAllCorrectArray[$keyEs3]['final_bonus'];

                            $resultCalculationPrepare[$mainKey]['final_bonus_in_EUR']['value']=
                                @(float)$resultCalculationPrepare[$mainKey]['final_bonus_in_EUR']['value']-(float)$getAgentAllCorrectArray[$keyEs3]['final_bonus_in_EUR'];
                        }
                    }
                }

            // }
//            $data['RAW'] = $result['hits'];
            $data['resultPrepare'] = $resultCalculationPrepare;
            $data['resultGameProvider'] = $result['resultGameProvider'];

            $currencyArray=[];
            $currencyArrayNgr=[];
            $exchangeRates = Currencies::on('read_db')->pluck('exchange_rate','code');
            foreach($resultCalculationPrepare as $key => $value){
                $parts = explode("-", $value['key']);
                $currency = $parts[3] ?? null;
                $currencySource = $exchangeRates[$tenant_base_currency];
                if($currency){
                    $ex_rate = $exchangeRates[$currency];
                }
                
                if($currency==$tenant_base_currency) {
                    $currencyArray[] = $value['ggr']['value'];
                    $currencyArrayNgr[] = $value['ngr']['value'];

                }else {
                    $currencyArray[] = (float)number_format((float)$value['ggr']['value'] * ((float)$currencySource/(float)$ex_rate),4,".","");
                    $currencyArrayNgr[] = (float)number_format((float)$value['ngr']['value'] * ((float)$currencySource/(float)$ex_rate),4,".","");
                }
            }
            $totalRevenue = collect($resultCalculationPrepare)
                ->map(function ($m) {
                    $commission = 100 - (float)explode('-', $m['key'])[5];
                    return ($m['ngr']['value'] * $commission) / 100;
                })
                ->reduce(function ($a, $b) {
                    return $a + $b;
                }, 0);
            $data['tenant_base_currency'] = $tenant_base_currency;
            $data['total_ggr'] = array_sum($currencyArray);
            $data['total_ngr'] = array_sum($currencyArrayNgr);
            $data['total_revenue'] = $totalRevenue;
            return returnResponse(true, '', $data, Response::HTTP_OK, true);


        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    /**
     * @description agentRevenueReportData
     * @param $request
     * @param string $download
     * @return array
     */

    public function downloadAgentRevenueReport(Request $request)
    {
        setUnlimited();
        try {
            // ================= permissions starts here =====================
            if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'casino_agent_revenue_report';
                $params->action = 'export';
                $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;

                $permission = checkPermissions($params);   
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================  

            $timeZone = $request->time_zone_name?? null;

            $result = $this->agentRevenueReportData($request->all(), 'download');

            $columns = array(
                'Agent Name',
                'Currency',
                'Bet',
                'Bet in EUR',
                'Win',
                'Win in EUR',
                'GGR',
                'GGR in EUR',
                'Bonus',
                'Bonus in EUR',
                'NGR (GR-Bonus)',
                'NGR(GR-Bonus) in EUR',
                'My Commission',
                'My Revenue',
                'My Revenue in EUR'

            );

            $path = Storage::path('csv/');
            if (!is_dir($path)) {
                mkdir($path, 0775, true);
                chmod($path, 0775);
            }


            $fileName = time() . '.csv';
            $file = fopen($path . $fileName, 'w');
            fputcsv($file, $columns);

            foreach ($result['hits']['hits'] as $task) {
                $task = $task['_source'];
                $playerDetails = $task['player_details'];

                $row['agent_name'] = @$playerDetails['agent_name'];
                $row['currency'] = @$playerDetails['created_at'];
                $row['bet'] = @$playerDetails['amount'];
                $row['bet_in_eur'] = @$playerDetails['currency'];
                $row['win'] = @$playerDetails['before_balance'];
                $row['win_in_eur'] = @$playerDetails['after_balance'];
                $row['ggr'] = @$task['transfer_method'];
                $row['ggr_in_eur'] = @$task['status'];
                $row['bonus'] = @$task['description'];
                $row['bonus_in_eur'] = @$task['transaction_id'];
                $row['ngr'] = @$task['internal_tracking_id'];
                $row['ngr_in_eur'] = @$task['internal_tracking_id'];
                $row['my_commision'] = @$task['type'];
                $row['revenu'] = @$task['type'];
                $row['revenu_in_eur'] = @$task['type'];
              

                if ($timeZone) {
                    if ($task["created_at"])
                        $row["created_at"] = getTimeZoneWiseTime(@$task['created_at'], $timeZone);
                    else
                        $row["created_at"] = @$task['created_at'];


                } else {
                    $row["created_at"] = @$task['created_at'];


                }
                $row["status"] = @$task['status'];
                fputcsv($file, array(
                        $row['agent_name'],
                        $row['currency'],
                        $row['bet'],
                        $row['bet_in_eur'],
                        $row['win'],
                        $row['win_in_eur'],
                        $row['ggr'],
                        $row['ggr_in_eur'],
                        $row['bonus'],
                        $row['bonus_in_eur'],
                        $row['ngr'],
                        $row['ngr_in_eur'],
                        $row['my_commision'],
                        $row['revenu'],
                        $row['revenu_in_eur']


                    )
                );
            }
            fclose($file);
            // $result = ["url" => (request()->secure() ? secure_url(('storage/app/csv/' . $fileName)): url('storage/app/csv/' . $fileName))];
            $result = ["url" => secure_url(('storage/app/csv/' . $fileName))];
            return returnResponse(true, '', $result, Response::HTTP_OK, true);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }


    private function agentRevenueReportData($request, $download = '')
    {

        $agentId = @$request['agent_id'] ?? null;
        $authUser = Auth::User();

        if ($authUser->parent_type == 'AdminUser' && $authUser->tenant_id) {
           $roles = getRolesDetails();
            $tenantId = @$request['tenant_id']?? $authUser->tenant_id;
            /*if (!in_array('owner', $roles)) {
                $agentId = @$request['agent_id'] ?? $authUser->id;
            }*/
            $agentId = @$request['agent_id'] ?? $authUser->id;

             //condition for new sub-admin role
            if(in_array('sub-admin',$roles)) {
                $agentId = @$request['agent_id'] ?? null;
            }
        } else {
            // $tenantId = $request['tenant_id'] ?? null;
            $tenantId = '';
            if($request['tenant_id']){
              $tenantId = $request['tenant_id'];
            }
            if(empty($agentId) || $agentId == null){
              $agentId = @$request['owner_id'] ?? '';
            }
        }

        $time_period = $request['datetime'] ?? [];
        $time_type = $request['time_type'] ?? 'today';

        $currency = $request['currency']?? null;
        $timeZone = $request['time_zone_name']?? null;


        if ($time_type != 'custom') {
            $time_period = dateConversionWithTimeZone($time_type);
            $time_period['fromdate'] = convertToUTC($time_period['fromdate'], $timeZone)->format('Y-m-d H:i:s.v');
            $time_period['enddate'] = convertToUTC($time_period['enddate'], $timeZone)->format('Y-m-d H:i:s.v');
            $time_period['fromdate'] = getTimeZoneWiseTimeISO($time_period['fromdate']);
            $time_period['enddate'] = getTimeZoneWiseTimeISO($time_period['enddate']);
        } else {
            if(!$time_period){
                $time_period = dateConversionWithTimeZone($time_type);
                $time_period['fromdate'] = convertToUTC($time_period['fromdate'], $timeZone)->format('Y-m-d H:i:s.v');
                $time_period['enddate'] = convertToUTC($time_period['enddate'], $timeZone)->format('Y-m-d H:i:s.v');
                $time_period['start_date'] = $time_period['fromdate'];
                $time_period['end_date'] = $time_period['enddate'];

            }
            $time_period['fromdate'] = getTimeZoneWiseTimeISO($time_period['start_date']);
            $time_period['enddate'] = getTimeZoneWiseTimeISO($time_period['end_date']);
            unset($time_period['start_date'], $time_period['end_date']);
        }
        $input = [
            'currency' => $currency,
            'tenant_id' => $tenantId,
            'agentId' => $agentId,
            'parent_id' => $agentId,
            'time_period' => $time_period,
            'owner_id' => $request['owner_id'] ?? '',
            'provider' => $request['game_provider'] ?? null,
            'game_type' => $request['game_type'] ?? null,
            'agent_type' => $request['agent_type'] ?? '',
            'game_table' => $request['game_table'] ?? null,
        ];

        $result =  AgentRevenueReportService::fetchResult($input);

        $resultGameProvider = ProviderFilterService::fetchResult($input);

        $result['resultGameProvider'] = $resultGameProvider['aggregations'];
        if (count($result['resultGameProvider']['game_type']['buckets'])) {

            foreach ($result['resultGameProvider']['game_type']['buckets'] as $key => $valueMain) {
                foreach ($result['resultGameProvider']['game_provider']['buckets'] as $key => $value) {
                    $sql = "SELECT  concat(cg.name , ' - ', cg.game_id ) as name,cg.game_id FROM casino_games as cg 
                                WHERE 
                                  cg.casino_provider_id = (SELECT casino_provider_id FROM casino_providers WHERE casino_providers.name = '{$valueMain["key"]}')
                                AND cg.game_id  IN (SELECT casino_games.game_id FROM casino_games WHERE casino_games.name = ?) 
                                ORDER BY cg.name ASC  limit 1";
                    $ResultGetProvidersIdType = DB::connection('read_db')->select($sql, array($value['key']));
                    $game_id = isset($ResultGetProvidersIdType[0]->game_id) ? $ResultGetProvidersIdType[0]->game_id : 0;
                    if (isset($result['resultGameProvider']['game_provider']['buckets'][$key]['name'])) {
                        if ($result['resultGameProvider']['game_provider']['buckets'][$key]['name'] == '') {
                            $result['resultGameProvider']['game_provider']['buckets'][$key]['name'] = @$ResultGetProvidersIdType[0]->name;
                            $result['resultGameProvider']['game_provider']['buckets'][$key]['game_id'] = $game_id;
                        }
                    } else {
                        $result['resultGameProvider']['game_provider']['buckets'][$key]['name'] = @$ResultGetProvidersIdType[0]->name;
                        $result['resultGameProvider']['game_provider']['buckets'][$key]['game_id'] = $game_id;
                    }

                    $sql = "SELECT concat(ct.name , ' - ', ct.table_id ) as name,ct.table_id
                                    FROM casino_tables as ct
                                    WHERE ct.game_id = '$game_id'";
                    $ResultGetTables = DB::connection('read_db')->select($sql);
                    if (isset($result['resultGameProvider']['game_provider']['buckets'][$key]['tables'])) {
                        if (empty($result['resultGameProvider']['game_provider']['buckets'][$key]['tables'])) {
                            $result['resultGameProvider']['game_provider']['buckets'][$key]['tables'] = $ResultGetTables;
                        }
                    } else {
                        $result['resultGameProvider']['game_provider']['buckets'][$key]['tables'] = $ResultGetTables;
                    }
                }
            }
        }

        return $result;

    }
}
