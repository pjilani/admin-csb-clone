<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Controller;
use App\Models\Currencies;
use App\Models\TenantCredentials;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Services\PlayerRevenueReportService as PlayerRevenueReportService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Services\ProviderFilterService;



class PlayerRevenueReportController extends Controller
{
    public function getPlayerRevenueReport(Request $request)
    {
        try {
                $tenant_base_currency = "EUR";
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'casino_player_revenue_report';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            setUnlimited();
            $page = $request->page ?? 1;
            $limit = $request->size ?? 10;
            $searchKeyword = $request->search ?? '';
            $isDirectPlayer = $request->player_type ?? 'all';

            $currency = $request->currency?? null;
            $start_date = $request->start_date ?? null;
            $end_date = $request->end_date ?? null;

            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $tenantId = $request->tenant_id?? Auth::user()->tenant_id;
                $agentId = $request->agent_id ?? @Auth::user()->id;

                //condition for new sub-admin role
                $roles = getRolesDetails();
                if(in_array('sub-admin',$roles) && @Auth::user()->id === $agentId) {
                        $agentId = null;
                }
                $find_tenant_base_currency = TenantCredentials::on('read_db')->where(['key'=>'TENANT_BASE_CURRENCY','tenant_id'=>$tenantId])->first();
                $tenant_base_currency = '';
                if(isset($find_tenant_base_currency)){
                  $tenant_base_currency = $find_tenant_base_currency['value'];
                }
            } else {
                $tenantId = $request->tenant_id?? null;
                $agentId = $request->agent_id ?? null;
            }
            $timeZone = $request->time_zone_name?? null;

            $offset = ($page > 1) ? $page * $limit : 0;
            if ($page > 1) {
                $offset = $limit * ($page - 1);
            }
            $time_period = $request['datetime'] ?? [];
            $time_type = $request['time_type'] ?? 'today';


            if($time_type!='custom'){
                $time_period=dateConversionWithTimeZone($time_type);

                $time_period['fromdate'] = convertToUTC(@$time_period['fromdate'], $timeZone)->format('Y-m-d H:i:s.v');
                $time_period['enddate'] = convertToUTC(@$time_period['enddate'], $timeZone)->format('Y-m-d H:i:s.v');

                $time_period['fromdate']= getTimeZoneWiseTimeISO(@$time_period['fromdate']);
                $time_period['enddate']= getTimeZoneWiseTimeISO(@$time_period['enddate']);
            }else{
                if(!$time_period){
                    $time_period = dateConversionWithTimeZone($time_type);
                    $time_period['fromdate'] = convertToUTC($time_period['fromdate'], $timeZone)->format('Y-m-d H:i:s.v');
                    $time_period['enddate'] = convertToUTC($time_period['enddate'], $timeZone)->format('Y-m-d H:i:s.v');
                    $time_period['start_date'] = $time_period['fromdate'];
                    $time_period['end_date'] = $time_period['enddate'];
    
                }
                $time_period['fromdate']= getTimeZoneWiseTimeISO(@$time_period['start_date']);
                $time_period['enddate']= getTimeZoneWiseTimeISO(@$time_period['end_date']);
                unset($time_period['start_date'],$time_period['end_date']);
            }

            $input = [
                'limit' => $limit,
                'offset' => $offset,
                'tenant_id' => $tenantId,
                'parent_id' => $agentId,
                'isDirectPlayer' => $isDirectPlayer,
                'searchKeyword' => $searchKeyword,
                'sortBy' => 'created_at',
                'sortOrder' => 'DESC',
                'currency' => $currency,
                'time_period' => $time_period,
                "game_type"=>$request['game_type'] ?? '',
                "game_provider"=>$request['game_provider'] ?? '',
                "table_id"=>$request['table_id'] ?? '',
                'tenant_base_currency' => $tenant_base_currency
            ];
            $result = PlayerRevenueReportService::fetchResult($input);
            $resultGameProvider = ProviderFilterService::fetchResult($input);

            $result['resultGameProvider'] = $resultGameProvider['aggregations'];
            if (count($result['resultGameProvider']['game_type']['buckets'])) {
                foreach ($result['resultGameProvider']['game_type']['buckets'] as $key => $valueMain) {
                    foreach ($result['resultGameProvider']['game_provider']['buckets'] as $key => $value) {
                        $sql = "SELECT  concat(cg.name , ' - ', cg.game_id ) as name,cg.game_id FROM casino_games as cg 
                                WHERE 
                                  cg.casino_provider_id = (SELECT casino_provider_id FROM casino_providers WHERE casino_providers.name = '{$valueMain["key"]}')
                                AND cg.game_id  IN (SELECT casino_games.game_id FROM casino_games WHERE casino_games.name = ?) 
                                ORDER BY cg.name ASC limit 1";
                        $ResultGetProvidersIdType = DB::connection('read_db')->select($sql, array($value['key']));
                        $game_id = @$ResultGetProvidersIdType[0]->game_id;


                        if (isset($result['resultGameProvider']['game_provider']['buckets'][$key]['name'])) {
                            if ($result['resultGameProvider']['game_provider']['buckets'][$key]['name'] == '') {
                                $result['resultGameProvider']['game_provider']['buckets'][$key]['name'] = @$ResultGetProvidersIdType[0]->name;
                                $result['resultGameProvider']['game_provider']['buckets'][$key]['game_id'] = $game_id;
                            }
                        } else {
                            $result['resultGameProvider']['game_provider']['buckets'][$key]['name'] = @$ResultGetProvidersIdType[0]->name;
                            $result['resultGameProvider']['game_provider']['buckets'][$key]['game_id'] = $game_id;
                        }
                        $sql = "SELECT concat(ct.name , ' - ', ct.table_id ) as name,ct.table_id
                                    FROM casino_tables as ct
                                    WHERE ct.game_id = '$game_id'";
                        $ResultGetTables = DB::connection('read_db')->select($sql);

                        if (isset($result['resultGameProvider']['game_provider']['buckets'][$key]['tables'])) {
                            if (empty($result['resultGameProvider']['game_provider']['buckets'][$key]['tables'])) {
                                $result['resultGameProvider']['game_provider']['buckets'][$key]['tables'] = $ResultGetTables;
                            }
                        } else {
                            $result['resultGameProvider']['game_provider']['buckets'][$key]['tables'] = $ResultGetTables;
                        }
                    }
                }
            }

            if($timeZone){
                foreach ($result['aggregations']['reports']['buckets'] as $index => &$value) {

                    $task = $value['top']['hits']['hits'][0]['_source']['player_details'];

                    if(@$task["created_at"] )
                        $value['top']['hits']['hits'][0]['_source']['player_details']['created_at'] = getTimeZoneWiseTime($task['created_at'],$timeZone);
                }
            }
            $result['tenant_base_currency'] = $tenant_base_currency;

               
            $currencyArray=[];
            $currencyArrayNgr = [];
            $exchangeRates = Currencies::on('read_db')->pluck('exchange_rate','code');
            $aggregations = $result['aggregations']['reports']['buckets'];
            foreach($aggregations as $aggregationsvalue){
                $parts = explode("-", $aggregationsvalue['key']);
                $currency = $parts[1] ?? null;
                $currencySource = $exchangeRates[$tenant_base_currency];
                if($currency){
                    $ex_rate = $exchangeRates[$currency];
                }
                if($currency==$tenant_base_currency) {
                    $currencyArray[] = $aggregationsvalue['ggr']['value'];
                    $currencyArrayNgr[] = $aggregationsvalue['ngr']['value'];
                }else {
                    $currencyArray[] = (float)number_format((float)$aggregationsvalue['ggr']['value'] * ((float)$currencySource/(float)$ex_rate),4,".","");
                    $currencyArrayNgr[] = (float)number_format((float)$aggregationsvalue['ngr']['value'] * ((float)$currencySource/(float)$ex_rate),4,".","");
                }
            }
            $result['aggregations']['total_ggr'] = array_sum($currencyArray);
            $result['aggregations']['total_ngr'] = array_sum($currencyArrayNgr);
            return returnResponse(true, '', $result, Response::HTTP_OK, true);


        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    public function downloadPlayerRevenueReport(Request $request)
    {
        setUnlimited();
        $agentId = null;
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                    $params = new \stdClass();
                    $params->module = 'casino_player_revenue_report';
                    $params->action = 'export';
                    $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);   
                    if(!$permission){ 
                        return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                    }
                    }
                    // ================= permissions ends here =====================   

            $searchKeyword = $request->search ?? '';
            $isDirectPlayer = $request->player_type ?? 'all';
            $currency = $request->currency?? null;
            $tenantId = $request->tenant_id?? Auth::user()->tenant_id;
            $timeZone = $request->time_zone_name?? null;
            $start_date = $request->start_date ?? null;
            $end_date = $request->end_date ?? null;


            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $roles = getRolesDetails();

                $tenantId = $request->tenant_id?? Auth::user()->tenant_id;
                if (!in_array('owner', $roles)) {
                    $agentId = $request->agent_id ?? @Auth::user()->id;
                }
                $isDirectPlayer = $isDirectPlayer ? @Auth::user()->id : null;
            } else {
                $tenantId = $request->tenant_id?? null;
                $agentId = $request->agent_id ?? null;
                $isDirectPlayer = $isDirectPlayer ? ($request->owner_id ? $request->owner_id : Auth::user()->id) : null;
            }


            $input = [
                'limit' => 'all',
                'offset' => 'all',
                'tenant_id' => $tenantId,
                'parent_id' => $agentId,
                'isDirectPlayer' => $isDirectPlayer ? @Auth::user()->id : null,
                'searchKeyword' => $searchKeyword,
                'sortBy' => 'player_id',
                'sortOrder' => 'ASC',
                'currency' => $currency,
                'start_date' => $start_date,
                'end_date' => $end_date
            ];

            $result = PlayerRevenueReportService::fetchResult($input);

            $columns = array(
                'Player Name',
                'Currency',
                'Agent Name',
                'Bet',
                'Win',
                'GGR',
                'Bonus',
                'NGR(GGR-Bonus)'.
                'Total Deposit',
                'Total Withdraws',

            );

            $path = Storage::path('csv/');
            if (!is_dir($path)) {
                mkdir($path, 0775, true);
                chmod($path, 0775);
            }


            $fileName = time() . '.csv';
            $file = fopen($path . $fileName, 'w');
            fputcsv($file, $columns);

            foreach ($result['hits']['hits'] as $task) {
                $task = $task['_source'];

                $row["Player Name"] = $task['player_name'];
                 $row['Currency']= $task['currency'];
                 $row['Agent Name']= $task['agent_name'];
                 $row['Bet']= $task['bet'];
                 $row['Win']= $task['win'];
                 $row['GGR']= $task['ggr'];
                 $row['Bonus']= $task['bonus'];
                 $row['NGR(GGR-Bonus)']= $task['ngr'];
                 $row['Total Deposit']= $task['total_deposit'];
                 $row['Total Withdraws']= $task['total_withdraws'];


                if ($timeZone) {
                    if ($task["creation_date"])
                        $row["creation_date"] = getTimeZoneWiseTime($task['creation_date'], $timeZone);
                    else
                        $row["creation_date"] = $task['creation_date'];


                } else {
                    $row["creation_date"] = $task['creation_date'];
                    

                }
                $row["status"] = $task['status'];
                fputcsv($file, array(

                        $row["player_name"],
                        $row["currency"],
                        $row["agent_name"],
                        $row["bet"],
                        $row["win"],
                        $row["ggr"],
                        $row["bonus"],
                        $row["ngr"],
                        $row["total_deposit"],
                        $row["total_withdraws"],
                     
                    )
                );
            }
            fclose($file);
            $result = ["url" => secure_url('storage/app/csv/' . $fileName)];
            return returnResponse(true, '', $result, Response::HTTP_OK, true);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

}
