<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Services\PlayerSportRevenueReportService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use App\Services\ProviderFilterService;
use Illuminate\Support\Facades\DB;

class PlayerSportRevenueReportController extends Controller
{
    public function getPlayerRevenueReport(Request $request)
    {
        try {
            setUnlimited();
            $page = $request->page ?? 1;
            $limit = $request->size ?? 10;
            $searchKeyword = $request->search ?? '';
            $isDirectPlayer = $request->player_type ?? 'all';

            $currency = $request->currency?? null;
            $start_date = $request->start_date ?? null;
            $end_date = $request->end_date ?? null;

            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $tenantId = $request->tenant_id?? Auth::user()->tenant_id;
                $agentId = $request->agent_id ?? @Auth::user()->id;
            } else {
                $tenantId = $request->tenant_id?? null;
                $agentId = $request->agent_id ?? null;
            }
            $timeZone = $request->time_zone_name?? null;

            $offset = ($page > 1) ? $page * $limit : 0;
            if ($page > 1) {
                $offset = $limit * ($page - 1);
            }
            $time_period = $request['time_period'] ?? [];
            $time_type = $request['time_type'] ?? 'today';


            if($time_type!='custom'){
                $time_period=dateConversionWithTimeZone($time_type);
                $time_period['fromdate']= getTimeZoneWiseTimeISO(getTimeZoneWiseTime($time_period['fromdate'], $timeZone));
                $time_period['enddate']= getTimeZoneWiseTimeISO(getTimeZoneWiseTime($time_period['enddate'], $timeZone));
            }else{
                $time_period['fromdate']= getTimeZoneWiseTimeISO(getTimeZoneWiseTime($time_period['start_date'], $timeZone));
                $time_period['enddate']= getTimeZoneWiseTimeISO(getTimeZoneWiseTime($time_period['end_date'], $timeZone));
                unset($time_period['start_date'],$time_period['end_date']);
            }

            $input = [
                'limit' => $limit,
                'offset' => $offset,
                'tenant_id' => $tenantId,
                'parent_id' => $agentId,
                'isDirectPlayer' => $isDirectPlayer,
                'searchKeyword' => $searchKeyword,
                'sortBy' => 'created_at',
                'sortOrder' => 'DESC',
                'currency' => $currency,
                'time_period' => $time_period
            ];


            $result = PlayerSportRevenueReportService::fetchResult($input);
            unset($result['hits']);

//            $resultGameProvider = ProviderFilterService::fetchResult($input);

            $result['resultGameProvider'] = [];//$resultGameProvider['aggregations'];
            /*if(count($result['resultGameProvider']['game_type']['buckets'])){

                foreach ( $result['resultGameProvider']['game_type']['buckets'] as $key => $value){

                    $sqlGetProvidersId = "SELECT id FROM casino_providers WHERE casino_providers.name = '".$value['key']."'";
                    $ResultGetProvidersId= DB::select($sqlGetProvidersId);
                    foreach ( $result['resultGameProvider']['game_provider']['buckets'] as $key => $value){

                        $sqlGetProvidersIdType = "SELECT casino_games.game_id FROM casino_games 
                                WHERE casino_games.name = '".$value['key']."'";
                        $ResultGetProvidersIdType= DB::select($sqlGetProvidersIdType);
                        $ResultGetProvidersIdTypeArray = [];
                        foreach ($ResultGetProvidersIdType as $tmpValue){
                            $ResultGetProvidersIdTypeArray[]=$tmpValue->game_id;
                        }
                        $providerId = $ResultGetProvidersId[0]->id;
                        if(count($ResultGetProvidersIdTypeArray) && @$ResultGetProvidersId[0]->id) {
                            $sql = "SELECT  concat(cg.name , ' - ', cg.game_id ) as name FROM casino_games as cg 
                                WHERE 
                                  cg.casino_provider_id = $providerId
                                AND cg.game_id  IN ('" . implode("','", $ResultGetProvidersIdTypeArray) . "') 
                                ORDER BY cg.name ASC";
                            $ResultGetProvidersIdType= DB::select($sql);
                            $result['resultGameProvider']['game_provider']['buckets'][$key]['name'] = $ResultGetProvidersIdType[0]->name;
                        }
                    }
                }
            }*/

            return returnResponse(true, '', $result, Response::HTTP_OK, true);


        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    public function downloadPlayerRevenueReport(Request $request)
    {
        ini_set('max_execution_time', 0);
        ini_set("memory_limit", "-1");
        $agentId = null;
        try {

            $searchKeyword = $request->search ?? '';
            $isDirectPlayer = $request->player_type ?? 'all';
            $currency = $request->currency?? null;
            $tenantId = $request->tenant_id?? Auth::user()->tenant_id;
            $timeZone = $request->time_zone_name?? null;
            $start_date = $request->start_date ?? null;
            $end_date = $request->end_date ?? null;


            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $roles = getRolesDetails();

                $tenantId = $request->tenant_id?? Auth::user()->tenant_id;
                if (!in_array('owner', $roles)) {
                    $agentId = $request->agent_id ?? @Auth::user()->id;
                }
                $isDirectPlayer = $isDirectPlayer ? @Auth::user()->id : null;
            } else {
                $tenantId = $request->tenant_id?? null;
                $agentId = $request->agent_id ?? null;
                $isDirectPlayer = $isDirectPlayer ? ($request->owner_id ? $request->owner_id : Auth::user()->id) : null;
            }


            $input = [
                'limit' => 'all',
                'offset' => 'all',
                'tenant_id' => $tenantId,
                'parent_id' => $agentId,
                'isDirectPlayer' => $isDirectPlayer ? @Auth::user()->id : null,
                'searchKeyword' => $searchKeyword,
                'sortBy' => 'player_id',
                'sortOrder' => 'ASC',
                'currency' => $currency,
                'start_date' => $start_date,
                'end_date' => $end_date
            ];

            $result = PlayerSportRevenueReportService::fetchResult($input);

            $columns = array(
                'Player Name',
                'Currency',
                'Agent Name',
                'Bet',
                'Win',
//                'GGR',
//                'Bonus',
//                'NGR(GGR-Bonus)'.
                'Total Deposit',
                'Total Withdraws',

            );

            $path = Storage::path('csv/');
            if (!is_dir($path)) {
                mkdir($path, 0775, true);
                chmod($path, 0775);
            }


            $fileName = time() . '.csv';
            $file = fopen($path . $fileName, 'w');
            fputcsv($file, $columns);

            foreach ($result['hits']['hits'] as $task) {
                $task = $task['_source'];

                $row["Player Name"] = $task['player_name'];
                 $row['Currency']= $task['currency'];
                 $row['Agent Name']= $task['agent_name'];
                 $row['Bet']= $task['bet'];
                 $row['Win']= $task['win'];
//                 $row['GGR']= $task['ggr'];
//                 $row['Bonus']= $task['bonus'];
//                 $row['NGR(GGR-Bonus)']= $task['ngr'];
                 $row['Total Deposit']= $task['total_deposit'];
                 $row['Total Withdraws']= $task['total_withdraws'];


                if ($timeZone) {
                    if ($task["creation_date"])
                        $row["creation_date"] = getTimeZoneWiseTime($task['creation_date'], $timeZone);
                    else
                        $row["creation_date"] = $task['creation_date'];


                } else {
                    $row["creation_date"] = $task['creation_date'];
                    

                }
                $row["status"] = $task['status'];
                fputcsv($file, array(

                        $row["player_name"],
                        $row["currency"],
                        $row["agent_name"],
                        $row["bet"],
                        $row["win"],
//                        $row["ggr"],
//                        $row["bonus"],
//                        $row["ngr"],
                        $row["total_deposit"],
                        $row["total_withdraws"],
                     
                    )
                );
            }
            fclose($file);
            $result = ["url" => secure_url('storage/app/csv/' . $fileName)];
            return returnResponse(true, '', $result, Response::HTTP_OK, true);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

}
