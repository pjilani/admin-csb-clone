<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Controller;
use App\Models\TenantCredentials;
use App\Services\PlayerFinancialReportService as PlayerFinancialReportService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;



class PlayerFinancialReportController extends Controller
{

    public function getplayerFinancialReport(Request $request)
    {
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'player_financial_report';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
            setUnlimited();
            $data = $this->playerFinancialReportData($request->all());
            $result = $data['data'];
            $result['tenant_base_currency'] = $data['tenant_base_currency'];
            $timeZone = $request->time_zone_name?? null;

            if ($timeZone) {
                foreach ($result['hits']['hits'] as $task1 => $task) {
                    $task = $task['_source'];
                    if ($task["created_at"])
                        $result['hits']['hits'][$task1]['_source']["created_at"] = getTimeZoneWiseTime($task['created_at'], $timeZone);
                }
            }

            return returnResponse(true, '', $result, Response::HTTP_OK, true);


        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    public function downloadPlayerFinancialReport(Request $request)
    {
        ini_set('max_execution_time', 0);
        ini_set("memory_limit", "-1");
        // $agentId = null;
        try {
               // ================= permissions starts here =====================
               if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'player_financial_report';
                $params->action = 'export';
                $params->admin_user_id = Auth::user()->id;
                   $params->tenant_id = Auth::user()->tenant_id;

                   $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================  

            // $searchKeyword = $request->search ?? '';
            // $isDirectPlayer = $request->isDirectPlayer ?? FALSE;
            // $currency = $request->currency?? null;
            // $tenantId = $request->tenant_id?? Auth::user()->tenant_id;
            $timeZone = $request->time_zone_name?? null;

            // if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
            //     $roles = getRolesDetails();

            //     $tenantId = $request->tenant_id?? Auth::user()->tenant_id;
            //     if (!in_array('owner', $roles)) {
            //         $agentId = $request->agent_id ?? @Auth::user()->id;
            //     }
            //     $isDirectPlayer = $isDirectPlayer ? @Auth::user()->id : null;
            // } else {
            //     $tenantId = $request->tenant_id?? null;
            //     $agentId = $request->agent_id ?? null;
            //     $isDirectPlayer = $isDirectPlayer ? ($request->owner_id != '' ? $request->owner_id : Auth::user()->id) : null;
            // }


            // $input = [
            //     'limit' => 'all',
            //     'offset' => 'all',
            //     'tenant_id' => $tenantId,
            //     'parent_id' => $agentId,
            //     'isDirectPlayer' => $isDirectPlayer ? @Auth::user()->id : null,
            //     'searchKeyword' => $searchKeyword,
            //     'sortBy' => $request->sort_by ?? 'created_at',
            //     'sortOrder' => $request->order ?? 'ASC',
            //     'currency' => $currency,
            //     'owner_id' => $request->owner_id ? $request->owner_id : null,
            //     'action_type' => $request->action_type ?? null,
            //     'status' => $request->status ?? null,
            //     'time_period' => $request->time_period ?? null,
            //     'player_type' => $request->player_type ?? null // add player type
            // ];

            // $result = PlayerFinancialReportService::fetchResult($input);

            $result = $this->playerFinancialReportData($request->all(), 'download');
            $result = $result['data'];

            $columns = array(
                'Player Id',
                'Player Name',
                'Agent Name',
                'Creation Date',
                'Action Type',
                'Amount',
                'Currency',
                'Balance Before',
                'After Balance',
                'Transfer Method',
                'Status',
                'Description',
                'Transaction ID',
                'Internal Tracking ID',

            );

            $path = Storage::path('csv/');
            if (!is_dir($path)) {
                mkdir($path, 0775, true);
                chmod($path, 0775);
            }


            $fileName = time() . '.csv';
            $file = fopen($path . $fileName, 'w');
            fputcsv($file, $columns);

            foreach ($result['hits']['hits'] as $task) {
                $task = $task['_source'];
                $playerDetails = $task['player_details'];

                $row['player_id'] = @$playerDetails['player_id'];
                $row['player_name'] = @$playerDetails['player_name'];

                $row['agent_name'] = @$playerDetails['agent_name'];
                $row['created_at'] = @$playerDetails['created_at'];
                $row['amount'] = @$task['amount'];

                $row['currency'] = @$playerDetails['currency'];
                $row['before_balance'] = @$playerDetails['before_balance'];
                $row['after_balance'] = @$playerDetails['after_balance'];

                $row['transfer_method'] = @$task['transfer_method'];
                $row['status'] = @$task['status'];
                $row['description'] = @$task['description'];
                $row['transaction_id'] = @$task['transaction_id'];
                $row['internal_tracking_id'] = @$task['internal_tracking_id'];
                $row['action_type'] = @$task['transaction_type'];


                if ($timeZone) {
                    if ($task["created_at"])
                        $row["created_at"] = getTimeZoneWiseTime(@$task['created_at'], $timeZone);
                    else
                        $row["created_at"] = @$task['created_at'];


                } else {
                    $row["created_at"] = @$task['created_at'];


                }
                $row["status"] = @$task['status'];
                fputcsv($file, array(
                        $row['player_id'],
                        $row['player_name'],
                        $row['agent_name'],
                        $row['created_at'],
                        $row['action_type'],
                        $row['amount'],
                        $row['currency'],
                        $row['before_balance'],
                        $row['after_balance'],
                        $row['transfer_method'],
                        $row['status'],
                        $row['description'],
                        $row['transaction_id'],
                        $row['internal_tracking_id'],
                    )
                );
            }
            fclose($file);
            // $result = ["url" => (request()->secure() ? secure_url(('storage/app/csv/' . $fileName)): url('storage/app/csv/' . $fileName))];
            $result = ["url" => secure_url(('storage/app/csv/' . $fileName))];
            return returnResponse(true, '', $result, Response::HTTP_OK, true);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }


    private function playerFinancialReportData($request, $download = '') {
        $tenant_base_currency = "EUR";
        $page = $request['page'] ?? 1;
        $limit = $request['size'] ?? 10;
        $agentId = null;
        $searchKeyword = $request['search'] ?? '';
        $isDirectPlayer = (int)$request['isDirectPlayer'] ?? 'all';

        $currency = $request['currency'] ?? null;
        $order_by = $request['order'] ?? 'desc';
        $sort_by = $request['sort_by'] ?? 'player_id';

        $authUser = Auth::User();

        if ($authUser->parent_type == 'AdminUser' && $authUser->tenant_id) {
            $tenantId = @$authUser->tenant_id;
            $agentId = $request['agent_id'] ?? $authUser->id;
            //condition for new sub-admin role
            $roles=getRolesDetails();
            if (in_array('sub-admin', $roles)) {
                $agentId = $request['agent_id'] ?? null;
            } elseif (in_array('owner', $roles)) {
                $agentId = $request['agent_id'] ?? null;
            }
            $find_tenant_base_currency = TenantCredentials::on('read_db')->where(['key'=>'TENANT_BASE_CURRENCY','tenant_id'=>$tenantId])->first();
            $tenant_base_currency = '';
            if(isset($find_tenant_base_currency)){
              $tenant_base_currency = $find_tenant_base_currency['value'];
            }

        } else {
            $tenantId = $request['tenant_id'] ?? '';
            $agentId = $request['agent_id'] ?? '';
        }


        $offset = ($page > 1) ? $page * $limit : 0;
        if ($page > 1) {
            $offset = $limit * ($page - 1);
        }

        $time_period = $request['datetime'] ?? [];
        $time_type = $request['time_type'] ?? 'today';

        $timeZone = $request['time_zone_name']?? null;
        if($time_type!='custom'){
            $time_period=dateConversionWithTimeZone($time_type);

            $time_period['fromdate'] = convertToUTC($time_period['fromdate'], $timeZone)->format('Y-m-d H:i:s.v');
            $time_period['enddate'] = convertToUTC($time_period['enddate'], $timeZone)->format('Y-m-d H:i:s.v');

            $time_period['fromdate']= getTimeZoneWiseTimeISO($time_period['fromdate']);
            $time_period['enddate']= getTimeZoneWiseTimeISO($time_period['enddate']);

        }else{
            if(!$time_period){
                $time_period = dateConversionWithTimeZone($time_type);
                $time_period['fromdate'] = convertToUTC($time_period['fromdate'], $timeZone)->format('Y-m-d H:i:s.v');
                $time_period['enddate'] = convertToUTC($time_period['enddate'], $timeZone)->format('Y-m-d H:i:s.v');
                $time_period['start_date'] = $time_period['fromdate'];
                $time_period['end_date'] = $time_period['enddate'];

            }
            $time_period['fromdate']= getTimeZoneWiseTimeISO($time_period['start_date']);
            $time_period['enddate']= getTimeZoneWiseTimeISO($time_period['end_date']);
            unset($time_period['start_date'],$time_period['end_date']);
        }


        $input = [
            'limit' => $download == 'download' ? 'all' : $limit,
            'offset' => $download == 'download' ? 'all' : $offset,
            'tenant_id' => $tenantId,
            'agentIdTop' => $agentId,
            'isDirectPlayer' => $isDirectPlayer,
            'searchKeyword' => $searchKeyword,
            'sortBy' => $sort_by,
            'sortOrder' => $order_by,
            'currency' => $currency,
            'owner_id' => $request['owner_id'] ?? '',
            'action_type' => $request['action_type'] ?? null,
            'status' => $request['status'] ?? null,
            'time_period' =>$time_period,
            'tenant_base_currency' => $tenant_base_currency

        ];
        $result['tenant_base_currency'] = $tenant_base_currency;
        $result['data'] =  PlayerFinancialReportService::fetchResult($input);
        return $result;
    }

}
