<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Controller;
use App\Models\Tenants;
use App\Services\GgrReportService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class GgrReportController extends Controller
{
    public function getGgrReport(Request $request)
    {
        try {
            // ================= permissions starts here =====================
            if (request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager") {
                $params = new \stdClass();
                $params->module = 'ggr_report';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;

                $permission = checkPermissions($params);
                if (!$permission) {
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
            }
            // ================= permissions ends here =====================  

            if ($request->time_type == 'monthWise' && (!$request['month'] || !$request['year'])) {
                return returnResponse(false, 'Please select both the month and the year.', [], Response::HTTP_EXPECTATION_FAILED);
            }
            $result = GgrReportService::reportData($request->all());
            return returnResponse(true, '', ['data' => $result[0], 'total' => $result[1]], Response::HTTP_OK, true);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) { //, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, [
                    'Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    public function downloadGgrReport(Request $request)
    {
        try {
            // ================= permissions starts here =====================
            if (request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager") {
                $params = new \stdClass();
                $params->module = 'ggr_report';
                $params->action = 'export';
                $params->admin_user_id = Auth::user()->id;
                $params->tenant_id = Auth::user()->tenant_id;

                $permission = checkPermissions($params);
                if (!$permission) {
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
            }
            // ================= permissions ends here =====================          
            if ($request->time_type == 'monthWise' && (!$request['month'] || !$request['year'])) {
                return returnResponse(false, 'Please select both the month and the year.', [], Response::HTTP_EXPECTATION_FAILED);
            }
            $download = true;
            $result = GgrReportService::reportData($request->all(), $download);
            $columns = array();
            if ((request()->User()->parent_type == "SuperAdminUser" || request()->User()->parent_type == "Manager") && ($request->time_type == 'monthWise' && ($request['month'] && $request['year']))) {
                $columns[] = 'Tenants';
            }
             array_push(
                $columns,
                'Total Bet',
                'Total Bet EUR',
                'Total Win',
                'Total Win Eur',
                'GGR',
                'GGR EUR',
                'Currency',
                'Exchange Rate',
            );
            if (($request->time_type == 'monthWise' && ($request['month'] && $request['year']))) {
                $columns[] = 'Date';
            }

            $path = Storage::path('csv/');
            if (!is_dir($path)) {
                mkdir($path, 0775, true);
                chmod($path, 0775);
            }

            $fileName = 'ggr_report_' . str_replace(' ', '_', now()->toDateTimeString()) . '.csv';
            $file = fopen($path . $fileName, 'w');
            if (request()->User()->parent_type == "SuperAdminUser" || request()->User()->parent_type == "Manager") {
                if (!$request->tenant_id) {
                    $tenantName = Tenants::pluck('name')->implode(',');
                } else {
                    $tenantName = Tenants::where('id', $request->tenant_id)->pluck('name')->implode(',');
                }
                $additionalRow = ["Filters",ucfirst($request->time_type), 'Tenant Name', $tenantName];
                if ($request->time_type == 'monthWise' && $request['month'] && $request['year']) {
                    array_splice($additionalRow, 2, 0, implode(',', $request['month']));
                }
                fputcsv($file, $additionalRow);
            }
            fputcsv($file, $columns);
            foreach ($result as $task) {
                if ((request()->User()->parent_type == "SuperAdminUser" || request()->User()->parent_type == "Manager") && ($request->time_type == 'monthWise' && ($request['month'] && $request['year']))) {
                    $row['tenant'] = $task->tenant->name;
                }
                $row['total_bet'] = $task['total_bet'];
                $row['total_bet_eur'] = $task['total_bet_eur'];
                $row['total_win'] = $task['total_win'];
                $row['total_win_eur'] = $task['total_win_eur'];
                $row['total_ggr'] = $task['total_ggr'];
                $row['total_ggr_eur'] = $task['total_ggr_eur'];
                $row['currency'] = $task['currency'];
                $row['exchange_rate'] = $task['exchange_rate'];
                if (($request->time_type == 'monthWise' && ($request['month'] && $request['year']))) {
                    $row['date'] = $task['date'];
                }
                $dataArr = array();
                if ((request()->User()->parent_type == "SuperAdminUser" || request()->User()->parent_type == "Manager") && ($request->time_type == 'monthWise' && ($request['month'] && $request['year']))) {
                    $dataArr[] = $row['tenant'];
                }
                array_push($dataArr, $row['total_bet'],
                $row['total_bet_eur'],
                $row['total_win'],
                $row['total_win_eur'],
                $row['total_ggr'],
                $row['total_ggr_eur'],
                $row['currency'],
                $row['exchange_rate']);
                if (($request->time_type == 'monthWise' && ($request['month'] && $request['year']))) {
                    $dataArr[] = $row['date'];
                }
                fputcsv(
                    $file,
                    $dataArr
                );
            }

            fclose($file);
            // $result = ["url" => (request()->secure() ? secure_url(('storage/app/csv/' . $fileName)) : url('storage/app/csv/' . $fileName))];
            $result = ["url" => secure_url(('storage/app/csv/' . $fileName))];
            return returnResponse(true, '', $result, Response::HTTP_OK, true);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) { //, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, [
                    'Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }
}