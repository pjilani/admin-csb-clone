<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Services\GameTransactionReportService as GameTransactionReportService;
use App\Services\ProviderFilterService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
// Models
use App\Models\TenantCredentials;
use App\Services\EzugiGameDetailsService;
use App\Services\SpribeGameDetailsService;
use App\Services\St8GameDetailsService;

class GameTransactionReportController extends Controller
{
    public function getGameTransactionReport(Request $request)
    {
        try {
            setUnlimited();
                $tenant_base_currency = "EUR";
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'game_transaction_report';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          

            $page = $request->page ?? 1;
            $limit = $request->size ?? 10;
            $searchKeyword = $request->search ?? '';
            $isDirectPlayer = $request->player_type ?? 'all';
            $internal_error_code = $request->internal_error_code ?? null;

            $currency = $request->currency?? null;

            if($request->agent_id == 0){
                $request->agent_id=null;
            }
            $agentId = $request->agent_id ?? @Auth::user()->id;
            if(Auth::user()->parent_type=='AdminUser' && Auth::user()->tenant_id) {
                $roles=getRolesDetails();
                $tenantId = $request->tenant_id?? Auth::user()->tenant_id;
                $find_tenant_base_currency = TenantCredentials::on('read_db')->where(['key'=>'TENANT_BASE_CURRENCY','tenant_id'=>$tenantId])->first();
                $tenant_base_currency = '';
                if(isset($find_tenant_base_currency)){
                  $tenant_base_currency = $find_tenant_base_currency['value'];
                }
                //condition for new sub-admin role
                if(in_array('sub-admin',$roles)) {
                    $agentId = $request->agent_id ?? null;
                }
            }
            else{
                $tenantId =$request->tenant_id?? '';
                $agentId = $request->agent_id ?? '';
            }
            $timeZone = $request->time_zone_name?? null;

            $offset = ($page > 1) ? $page * $limit : 0;
            if ($page > 1) {
                $offset = $limit * ($page - 1);
            }
            $time_period = $request['datetime'] ?? [];
            $time_type = $request['time_type'] ?? 'today';
            if($time_type!='custom'){
                $time_period=dateConversionWithTimeZone($time_type);
                $time_period['fromdate'] = convertToUTC($time_period['fromdate'], $timeZone)->format('Y-m-d H:i:s.v');
                $time_period['enddate'] = convertToUTC($time_period['enddate'], $timeZone)->format('Y-m-d H:i:s.v');
                $time_period['fromdate']= getTimeZoneWiseTimeISO($time_period['fromdate']);
                $time_period['enddate']= getTimeZoneWiseTimeISO($time_period['enddate']);
            }else{
                if(!$time_period){
                    $time_period = dateConversionWithTimeZone($time_type);
                    $time_period['fromdate'] = convertToUTC($time_period['fromdate'], $timeZone)->format('Y-m-d H:i:s.v');
                    $time_period['enddate'] = convertToUTC($time_period['enddate'], $timeZone)->format('Y-m-d H:i:s.v');
                    $time_period['start_date'] = $time_period['fromdate'];
                    $time_period['end_date'] = $time_period['enddate'];
    
                }
                $time_period['fromdate']= getTimeZoneWiseTimeISO($time_period['start_date']);
                $time_period['enddate']= getTimeZoneWiseTimeISO($time_period['end_date']);
                unset($time_period['start_date'],$time_period['end_date']);
            }
            $order_by = $request->order ?? 'desc';
            $sort_by = $request->sort_by ?? 'player_details.player_id';
            $input = [
                'limit' => $limit,
                'offset' => $offset,
                'tenant_id' => $tenantId,
                'parent_id' => $agentId,
                'isDirectPlayer' => $isDirectPlayer,
                'searchKeyword' => $searchKeyword,
                'sortBy' => $sort_by,
                'sortOrder' => $order_by,
                'currency' => $currency,
                'owner_id' => $request->owner_id?$request->owner_id:'',
                'action_type' => $request->action_type?? null,
                'game_provider' => $request->game_provider?? null,
                'game_type' => $request->game_type?? null,
                'time_period' => $time_period,
                'internal_error_code' => $internal_error_code,
                'tenant_base_currency' => $tenant_base_currency
            ];
            $result = GameTransactionReportService::fetchResult($input);
            $resultGameProvider = ProviderFilterService::fetchResult($input);
            $result['resultGameProvider'] = $resultGameProvider['aggregations'];
            if (count($result['resultGameProvider']['game_type']['buckets'])) {
                foreach ($result['resultGameProvider']['game_type']['buckets'] as $key => $valueMain) {
                    foreach ($result['resultGameProvider']['game_provider']['buckets'] as $key => $value) {
                        $sql = "SELECT  concat(cg.name , ' - ', cg.game_id ) as name FROM casino_games as cg 
                          WHERE 
                            cg.casino_provider_id = (SELECT casino_provider_id FROM casino_providers WHERE casino_providers.name = '{$valueMain["key"]}')
                          AND cg.game_id  IN (SELECT casino_games.game_id FROM casino_games WHERE casino_games.name = ?)  
                          ORDER BY cg.name ASC limit 1";
                        $ResultGetProvidersIdType = DB::connection('read_db')->select($sql, array($value['key']));
                        if (isset($result['resultGameProvider']['game_provider']['buckets'][$key]['name'])) {
                            if ($result['resultGameProvider']['game_provider']['buckets'][$key]['name'] == '') {
                                $result['resultGameProvider']['game_provider']['buckets'][$key]['name'] = @$ResultGetProvidersIdType[0]->name;
                            }
                        } else {
                            $result['resultGameProvider']['game_provider']['buckets'][$key]['name'] = @$ResultGetProvidersIdType[0]->name;
                        }
                    }
                }
            }
            if($timeZone){
                foreach ($result['hits']['hits'] as $task1=>$task) {
                    $task=$task['_source'];
                    if($task["created_at"] )
                        $result['hits']['hits'][$task1]['_source']["created_at"] = getTimeZoneWiseTime($task['created_at'],$timeZone);
                }
            }
            $result['tenant_base_currency'] = $tenant_base_currency;
            return returnResponse(true, '', $result, Response::HTTP_OK, true);
        } catch (\Exception $e) {
            if(!App::environment(['local'])){//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            }else{
                return returnResponse(false, ERROR_MESSAGE, ['Error'=>$e->getMessage(),
                    'LineNo'=>$e->getLine(),
                    'FileName'=>$e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    public function downloadGameTransactionReport(Request $request)
    {
        setUnlimited();
        $agentId = null;
        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'game_transaction_report';
                $params->action = 'export';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          

            $searchKeyword = $request->search ?? '';
            $isDirectPlayer = $request->isDirectPlayer ?? FALSE;
            $currency = $request->currency?? null;
            $tenantId = $request->tenant_id?? Auth::user()->tenant_id;
            $timeZone = $request->time_zone_name?? null;

            if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
                $roles = getRolesDetails();

                $tenantId = $request->tenant_id?? Auth::user()->tenant_id;
                if (!in_array('owner', $roles)) {
                    $agentId = $request->agent_id ?? @Auth::user()->id;
                }
                $isDirectPlayer = $isDirectPlayer ? @Auth::user()->id : null;
            } else {
                $tenantId = $request->tenant_id?? null;
                $agentId = $request->agent_id ?? null;
                $isDirectPlayer = $isDirectPlayer ? ($request->owner_id ? $request->owner_id : Auth::user()->id) : null;
            }

            $input = [
                'limit' => 'all',
                'offset' => 'all',
                'tenant_id' => $tenantId,
                'parent_id' => $agentId,
                'isDirectPlayer' => $isDirectPlayer ? @Auth::user()->id : null,
                'searchKeyword' => $searchKeyword,
                'sortBy' => 'created_at',
                'sortOrder' => 'DESC',
                'currency' => $currency,
                'owner_id' => $request->owner_id?$request->owner_id:null,
                'action_type' => $request->action_type?? null,
                'game_provider' => $request->game_provider?? null,
                'time_period' => $request->time_period?? null,
            ];

            $result = $this->gameTransactionReportData($request->all(), 'download');

            $columns = array(
                'Player Id',
                'Player Name',
                'Game Provider',
                'RoundID',
                'Agent Name',
                'Creation Date',
                'Game Type',
                'Currency',
                'Action Type',
                'Amount',
                'Non Cash amount',
                'Balance Before',
                'After Balance',
                'Transaction ID',
                'Internal Tracking ID',
                'GP Error Code',
                'Internal Error Code',
                'Internal Error Description',
            );

            $path = Storage::path('csv/');
            if (!is_dir($path)) {
                mkdir($path, 0775, true);
                chmod($path, 0775);
            }

            $fileName = time() . '.csv';
            $file = fopen($path . $fileName, 'w');
            fputcsv($file, $columns);

            foreach ($result['hits']['hits'] as $task) {
               
                $task = $task['_source'];
                $playerDetails = $task['player_details'];


                $row['player_id'] = @$playerDetails['player_id'];
                $row['player_name'] = @$playerDetails['player_name'];
                $row['game_provider'] = @$task['game_provider'];
                $row['round_id'] = @$task['round_id'];
                $row['agent_name'] = @$playerDetails['agent_name'];
                $row['created_at'] = @$playerDetails['created_at'];
                $row['game_type'] = @$task['game_type'];
                $row['currency'] = @$playerDetails['currency'];
                $row['description'] = @$task['transaction_type'];
                $row['amount'] = @$task['amount'];
                $row['non_cash_amount'] = 0;
                $row['before_balance'] = @$playerDetails['before_balance'];
                $row['after_balance'] = @$playerDetails['after_balance'];
                $row['transaction_id'] = @$task['transaction_id'];
                $row['internal_tracking_id'] = @$task['internal_tracking_id'];
                $row['gp_error_code'] = @$task['gp_error_code'];
                $row['internal_error_code'] = @$task['internal_error_code'];
                $row['internal_error_description'] = @$task['internal_error_description'];
              
                if ($timeZone) {
                    if ($task["created_at"])
                        $row["created_at"] = getTimeZoneWiseTime($task['created_at'], $timeZone);
                    else
                        $row["created_at"] = $task['created_at'];

                } else {
                    $row["created_at"] = $task['created_at'];

                }
                if(array_key_exists('status',$task))
                {
                    $row['status'] = $task['status'];  
                }else{
                    $row['status'] = 'NA';
                }
               
                fputcsv($file, array(

                        $row['player_id'],
                        $row['player_name'],
                        $row['game_provider'],
                        $row['round_id'],
                        $row['agent_name'],
                        $row['created_at'],
                        $row['game_type'],
                        $row['currency'],
                        $row['description'],
                        $row['amount'],
                        $row['non_cash_amount'],
                        $row['before_balance'],
                        $row['after_balance'],  
                        $row['transaction_id'],
                        $row['internal_tracking_id'],
                        $row['gp_error_code'],
                        $row['internal_error_code'],
                        $row['internal_error_description'],          
                    )
                );
                
            }
       
            fclose($file);
            // $result = ["url" => (request()->secure() ? secure_url(('storage/app/csv/' . $fileName)): url('storage/app/csv/' . $fileName))];
            $result = ["url" => secure_url(('storage/app/csv/' . $fileName))];
            return returnResponse(true, '', $result, Response::HTTP_OK, true);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    private function gameTransactionReportData($request, $download = '') {

      $page = $request['page'] ?? 1;
      $limit = $request['size'] ?? 10;
      $agentId = null;
      $searchKeyword = $request['search'] ?? '';
      $isDirectPlayer = $request->isDirectPlayer ?? FALSE;
      $internal_error_code = $request->internal_error_code ?? null;


      $currency = $request['currency'] ?? null;
      $order_by = $request['order'] ?? 'desc';
      $sort_by = $request['sort_by'] ?? 'player_id';

      $authUser = Auth::User();

      if ($authUser->parent_type == 'AdminUser' && $authUser->tenant_id) {
          $tenantId = $request['tenant_id'] ?? $authUser->tenant_id;
          /*$roles = getRolesDetails();

          if (!in_array('owner', $roles)) {
              if ($request['agent_id']) {
                  $agentId = $request['agent_id'] ?? null;
              } else {
                  $agentId = $authUser->id;
              }

          } else {
              $agentId = $request['agent_id'] ?? $authUser->id;
          }*/
          $agentId = $request['agent_id'] ?? $authUser->id;

          //condition for new sub-admin role
          $roles=getRolesDetails();
          if(in_array('sub-admin',$roles)) {
                  $agentId = getParentAgent(Auth::user()->id);
          }

      } else {
          $tenantId = $request['tenant_id'] ?? '';
          $agentId = $request['agent_id'] ?? '';
      }


      $offset = ($page > 1) ? $page * $limit : 0;
      if ($page > 1) {
          $offset = $limit * ($page - 1);
      }
        $find_tenant_base_currency = TenantCredentials::on('read_db')->where(['key'=>'TENANT_BASE_CURRENCY','tenant_id'=>$tenantId])->first();
        $tenant_base_currency = '';
        if(isset($find_tenant_base_currency)){
            $tenant_base_currency = $find_tenant_base_currency['value'];
        }
      $time_period = $request['time_period'] ?? [];
      $time_type = $request['time_type'] ?? 'today';

      $timeZone = $request['time_zone_name']?? null;
      if($time_type!='custom'){
          $time_period=dateConversionWithTimeZone($time_type);
          $time_period['fromdate']= getTimeZoneWiseTimeISO(getTimeZoneWiseTime($time_period['fromdate'], $timeZone));
          $time_period['enddate']= getTimeZoneWiseTimeISO(getTimeZoneWiseTime($time_period['enddate'], $timeZone));
      }else{
          $time_period['fromdate']= getTimeZoneWiseTimeISO(getTimeZoneWiseTime($time_period['start_date'], $timeZone));
          $time_period['enddate']= getTimeZoneWiseTimeISO(getTimeZoneWiseTime($time_period['end_date'], $timeZone));
          unset($time_period['start_date'],$time_period['end_date']);
      }


      $input = [
        'limit' => $download == 'download' ? 'all' : $limit,
        'offset' => $download == 'download' ? 'all' : $offset,
        'tenant_id' => $tenantId,
        'parent_id' => $agentId,
        'isDirectPlayer' => $isDirectPlayer,
        'searchKeyword' => $searchKeyword,
        'sortBy' => $sort_by,
        'sortOrder' => $order_by,
        'currency' => $currency,
        'owner_id' => $request['owner_id'] ?? '',
        'game_provider' => $request['game_provider']?? null,
        'game_type' => $request['game_type']?? null,
        'time_period' => $time_period,
        'internal_error_code' => $internal_error_code,
        'action_type' => $request['action_type'] ?? null,
        'tenant_base_currency' => $tenant_base_currency
    ];

      return GameTransactionReportService::fetchResult($input);
  }

    /**
     * @description show
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request) {

        try {
                // ================= permissions starts here =====================
                if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
                $params = new \stdClass();
                $params->module = 'game_transaction_report';
                $params->action = 'R';
                $params->admin_user_id = Auth::user()->id;
                    $params->tenant_id = Auth::user()->tenant_id;

                    $permission = checkPermissions($params);
                if(!$permission){ 
                    return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
                }
                }
                // ================= permissions ends here =====================          
          
                $request = $request->all();

                $response = [];

                switch ($request['GameProvider']) {
                    case 'Ezugi':
                    case 'Evolution':
                        $response = EzugiGameDetailsService::ezugiRoundInfo($request);
                        break;
                    case 'st8':
                        $response = St8GameDetailsService::st8RoundInfo($request);
                        break;
                    case 'Spribe':
                        $response = SpribeGameDetailsService::spribeRoundInfo($request);
                        break;
                    default:
                        break;
                }


            //End get details
            return returnResponse(true, '', $response, Response::HTTP_OK, true);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }
    
}
