<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Controller;
use App\Models\TenantCredentials;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Services\TipReportService as TipReportService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;



class TipReportController extends Controller
{

    public function getTipReport(Request $request)
    {
        try {
            setUnlimited();

            $result = $this->tipReportData($request->all());

            if(isset($result['hits']))
            unset($result['hits']);


            return returnResponse(true, '', $result, Response::HTTP_OK, true);


        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }

    }

    public function downloadTipReport(Request $request)
    {
        ini_set('max_execution_time', 0);
        ini_set("memory_limit", "-1");
        // $agentId = null;
        try {

            $result = $this->tipReportData($request->all(), 'download');

            $columns = array(
                'Agent',
                'Currency',
                'Tip Amount',
                'Transactions Count'

            );

            $path = Storage::path('csv/');
            if (!is_dir($path)) {
                mkdir($path, 0775, true);
                chmod($path, 0775);
            }


            $fileName = time() . '.csv';
            $file = fopen($path . $fileName, 'w');
            fputcsv($file, $columns);
            $aggregation = @$result['aggregations'];
            $reports = @$aggregation['reports'];
            foreach ($reports['buckets'] as $task) {

                $key = explode('-', $task['key']);
                $row["agent_name"] = @$key[0];
                $row["currency"] = @$key[3];
                $row["tip_amount"] = @$task['tip_amount']['value'];
                $row["transactions_count"] = @$task['doc_count'];

                fputcsv($file, array(

                        $row["agent_name"],
                        $row["currency"],
                        $row["tip_amount"],
                        $row["transactions_count"],

                    )
                );
            }
            fclose($file);
            $result = ["url" => secure_url('storage/app/csv/' . $fileName)];
            return returnResponse(true, '', $result, Response::HTTP_OK, true);
        } catch (\Exception $e) {
            if (!App::environment(['local'])) {//, 'staging'
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ], Response::HTTP_EXPECTATION_FAILED);
            }
        }
    }

    private function tipReportData($request, $download = '') {
        $tenant_base_currency = "EUR";
        $page = $request['page'] ?? 1;
        $limit = $request['size'] ?? 10;
        $searchKeyword = $request['search'] ?? '';
        $isDirectPlayer = $request['player_type'] ?? 'all';

        $currency = $request['currency'] ?? null;

        $agentId = null;

        $authUser = Auth::User();

        if ($authUser->parent_type == 'AdminUser' && $authUser->tenant_id) {
            /*$roles = getRolesDetails();
            if (!in_array('owner', $roles)) {
                $agentId = $request['agent_id'] ?? $authUser->id;
            }else{
                $agentId = $request['agent_id'] ?? $authUser->id;
            }*/
            $tenantId = $request['tenant_id'] ?? $authUser->tenant_id;
            $agentId = $request['agent_id'] ?? $authUser->id;

            //condition for new sub-admin role
            $roles = getRolesDetails();
            if(in_array('sub-admin',$roles) && @Auth::user()->id === $agentId) {
                $agentId = null;
            }
            $find_tenant_base_currency = TenantCredentials::on('read_db')->where(['key'=>'TENANT_BASE_CURRENCY','tenant_id'=>$tenantId])->first();
            $tenant_base_currency = '';
            if(isset($find_tenant_base_currency)){
              $tenant_base_currency = $find_tenant_base_currency['value'];
            }

        } else {
            $tenantId = $request['tenant_id'] ?? '';
            $agentId = $request['agent_id'] ?? '';

        }
        $timeZone = $request['time_zone_name'] ?? null;

        $offset = ($page > 1) ? $page * $limit : 0;
        if ($page > 1) {
            $offset = $limit * ($page - 1);
        }

        $time_period = $request['datetime'] ?? [];
        $time_type = $request['time_type'] ?? 'today';

        if($time_type!='custom'){
            $time_period=dateConversionWithTimeZone($time_type);
            
            $time_period['fromdate'] = convertToUTC(@$time_period['fromdate'], $timeZone)->format('Y-m-d H:i:s.v');
            $time_period['enddate'] = convertToUTC(@$time_period['enddate'], $timeZone)->format('Y-m-d H:i:s.v');
  
            $time_period['fromdate']= getTimeZoneWiseTimeISO(@$time_period['fromdate']);
            $time_period['enddate']= getTimeZoneWiseTimeISO(@$time_period['enddate']);
        }else{
            if(!$time_period){
                $time_period = dateConversionWithTimeZone($time_type);
                $time_period['fromdate'] = convertToUTC($time_period['fromdate'], $timeZone)->format('Y-m-d H:i:s.v');
                $time_period['enddate'] = convertToUTC($time_period['enddate'], $timeZone)->format('Y-m-d H:i:s.v');
                $time_period['start_date'] = $time_period['fromdate'];
                $time_period['end_date'] = $time_period['enddate'];

            }
            $time_period['fromdate']= getTimeZoneWiseTimeISO(@$time_period['start_date']);
            $time_period['enddate']= getTimeZoneWiseTimeISO(@$time_period['end_date']);
            unset($time_period['start_date'],$time_period['end_date']);
        }

        $input = [
            'limit' => $download == 'download' ? 'all' : $limit,
            'offset' => $download == 'download' ? 'all' : $offset,
            'tenant_id' => $tenantId,
            'parent_id' => $agentId,
            'isDirectPlayer' => $isDirectPlayer,
            'searchKeyword' => $searchKeyword,
            'sortBy' => 'created_at',//$request['sort_by'] ?? 'created_at',
            'sortOrder' => 'desc',
            'currency' => $currency,
            'owner_id' => $request['owner_id'] ?? '',
            'time_period' => $time_period,
            'time_type' => $request['time_type'] ?? null,
            'tenant_base_currency' => $tenant_base_currency
        ];

        // print_r($input);die;
        $result =  TipReportService::fetchResult($input);
        $result['tenant_base_currency'] = $tenant_base_currency;
        return $result;
    }


}
