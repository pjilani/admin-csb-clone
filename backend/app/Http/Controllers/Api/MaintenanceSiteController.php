<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Models\Admin;
use App\Models\TenantSiteMaintenance;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;

use App\Models\Currencies;
use App\Models\Tenants;
use Illuminate\Support\Facades\Hash;

class MaintenanceSiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getTenantMaintenanceSetting(Request $request)
    {
      try {
        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
          $params = new \stdClass();
          $params->module = 'site_maintenance';
          $params->action = 'R';
          $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);
          if(!$permission){ 
            return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
          }
        }
      // ================= permissions ends here ===================== 
        $tenant_id = Auth::user()->tenant_id;
        $admin_user_id = Auth::user()->id;
        $data['list'] = TenantSiteMaintenance::where('tenant_id',$tenant_id)->first();
        return returnResponse(true, 'All Data', $data, 200, true);
      } catch (\Exception $e) {
      
          if (!App::environment(['local'])) {//, 'staging'
              return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
          } else {
              return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                  'LineNo' => $e->getLine(),
                  'FileName' => $e->getFile()
              ], Response::HTTP_EXPECTATION_FAILED);
          }
      }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function updateTenantMaintenanceSetting(Request $request)
    {
       
      try {

        // ================= permissions starts here =====================
        if(request()->User()->parent_type != "SuperAdminUser" && request()->User()->parent_type != "Manager"){
          $params = new \stdClass();
          $params->module = 'site_maintenance';
          $params->action = 'U';
          $params->admin_user_id = Auth::user()->id;
            $params->tenant_id = Auth::user()->tenant_id;

            $permission = checkPermissions($params);
          if(!$permission){ 
            return returnResponse(false, ERROR_DENIED, [], PERMISSION_DENIED);
          }
        }
      // ================= permissions ends here ===================== 
        
        $tenant_id = Auth::user()->tenant_id;
        
        // Verify Admin Password
        if(!isset($request->password[0])){
          throw new \ErrorException('Please enter valid password');
        }
        
        if(!Hash::check($request->password[0], Auth::user()->encrypted_password)){
          return returnResponse(false, 'Password Not Matched', [], Response::HTTP_EXPECTATION_FAILED);
        }
        $data = $request->data;
        $modeSetting = TenantSiteMaintenance::where('tenant_id',$tenant_id)->first();
        // update
        
        $data = [
          "title" => $data['title'],
          "announcement_title" => $data['announcement_title'],
          "tenant_id" => $tenant_id,
          "type" => 'maintenance',
          "is_announcement_active" => ($data['active'] ? true : false),
          "site_down" => ($data['site_down'] ? true : false),
          "created_at" => date('Y-m-d H:i:s'),
          "updated_at" => date('Y-m-d H:i:s')
        ];
        if(!$modeSetting){
          TenantSiteMaintenance::create($data);
        }else{
          TenantSiteMaintenance::where('id',$modeSetting['id'])->update($data);
          //edit
        }

        try {
          // websocket API of Node for handling web
            $getTenantSettings = Tenants::find($tenant_id);
            $url  = getNodeURl($getTenantSettings->domain);
            $endpoint = $url.(env('WEB_SOCKET_URL') ? env('WEB_SOCKET_URL') : '/ezugi/check-maintenance');
            $client = new \GuzzleHttp\Client();
            $data = [
              'tenantId' => (int)$tenant_id, 
              'type' => "subscription",
            ];

            $response = $client->post( $endpoint, [
              'headers' => ['Content-Type' => 'application/json'],
              'body' => json_encode($data)
            ]);
        }catch (\Exception $e) {
      
          // if (!App::environment(['local'])) {//, 'staging'
          //     return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
          // } else {
          //     return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage()], Response::HTTP_EXPECTATION_FAILED);
          // }
      }
            
        
        return returnResponse(true, 'Maintenance Setting updated successfully', [], Response::HTTP_OK);

    } catch (\Exception $e) {
        if (!App::environment(['local'])) {//, 'staging'
            return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
        } else {
            return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage(),
                'LineNo' => $e->getLine(),
                'FileName' => $e->getFile()
            ], Response::HTTP_EXPECTATION_FAILED);
        }

    }
    }

    
    
}
