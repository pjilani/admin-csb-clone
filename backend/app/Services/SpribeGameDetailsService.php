<?php

namespace App\Services;

use App\Models\TenantCredentials;
use App\Models\UserToken;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\App;
use Illuminate\Http\Response;

class SpribeGameDetailsService
{
    public static function spribeRoundInfo($request)
    {

        try {      
            $authUser = Auth::User();

            $credentials = TenantCredentials::on('read_db')->select('key', 'value')
            ->where('tenant_id', @$request['tenant_id'] ?? $authUser->tenant_id)
            ->whereRAW("key in ('APP_SPRIBE_OPERATOR_ID','APP_SPRIBE_GAME_DETAILS_URL')")
            ->pluck('value', 'key')->toArray();

            $data = self::getProviderKeyAndIdentifier(@$request['GameType']);

            if(!($credentials['APP_SPRIBE_OPERATOR_ID'] && $credentials['APP_SPRIBE_GAME_DETAILS_URL'] && $request['PlayerToken'])){
                return ['data' => ''];
            }

            $paramString = '?round_id=' . $request['RoundID'] . '&game=' . $data[1] . '&provider=' . $data[0] . '&player_token=' . $request['PlayerToken'] . '&op_player_id=' . @$request['UID'] . '&operator=' . $credentials['APP_SPRIBE_OPERATOR_ID'];
            
            $url = $credentials['APP_SPRIBE_GAME_DETAILS_URL'] . $paramString;

            return ['data' => $url];
        } catch (\Exception $e) {
            if (!App::environment(['local'])) { //, 'staging'
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage()], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage()], Response::HTTP_EXPECTATION_FAILED);
            }
        }

        return;
    }

    public static function getProviderKeyAndIdentifier($inputGameName) {
        $lowercasedInput = strtolower($inputGameName);

        $gameData = [
            ['game_name' => 'Aviator', 'game_identificator' => 'aviator', 'provider_key' => 'spribe_aviator'],
            ['game_name' => 'Dice', 'game_identificator' => 'dice', 'provider_key' => 'spribe_crypto'],
            ['game_name' => 'Goal', 'game_identificator' => 'goal', 'provider_key' => 'spribe_crypto'],
            ['game_name' => 'Plinko', 'game_identificator' => 'plinko', 'provider_key' => 'spribe_crypto'],
            ['game_name' => 'Mines', 'game_identificator' => 'mines', 'provider_key' => 'spribe_crypto'],
            ['game_name' => 'Hi Lo', 'game_identificator' => 'hi-lo', 'provider_key' => 'spribe_crypto'],
            ['game_name' => 'Keno', 'game_identificator' => 'keno', 'provider_key' => 'spribe_crypto'],
            ['game_name' => 'Mini Roulette', 'game_identificator' => 'mini-roulette', 'provider_key' => 'spribe_crypto'],
            ['game_name' => 'Hotline', 'game_identificator' => 'hotline', 'provider_key' => 'spribe_crypto'],
            ['game_name' => 'Poker', 'game_identificator' => 'poker', 'provider_key' => 'spribe_poker'],
            ['game_name' => 'Keno 80', 'game_identificator' => 'multikeno', 'provider_key' => 'spribe_keno'],
        ];
    
        foreach ($gameData as $game) {
            if (strtolower($game['game_name']) === $lowercasedInput) {
                return [$game['provider_key'],$game['game_identificator']];
            }
        }
    
        return ['',''];
    }
}
