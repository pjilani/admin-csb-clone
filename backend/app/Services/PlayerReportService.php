<?php

namespace App\Services;

use Elasticsearch\ClientBuilder;

class PlayerReportService
{

    public static function fetchResult($params)
    {
        $obj = new self();
        $paramsTemp=$params;
        $body = [
            'query' => $obj->queryKey($params),
            'sort' => $obj->sortKey($params['sortBy'], $params['sortOrder']),
            'timeout' => '11s',
            'track_total_hits' => true,
            'aggs' => [
                'total_bet_count' => $obj->totalBetCountKey(),
                'currencies' => $obj->currencyKey(),
            ]
        ];

        if($params['limit']!='all'){
            $body['size'] = $params['limit'];
            $body['from'] = (int)$params['offset'];
        }else{
            $body['size'] = ALLCOUNT;
        }


        $params = [
            'index' => ELASTICSEARCH_INDEX['users'],
            'body' => $body
        ];

        $client = getEsClientObj();
        $return['user'] = $client->search($params);
/*


PUT users/_settings
{
  "max_result_window" : 500000
}
PUT shakespeare
{
  "mappings": {
    "properties": {
      "text_entry": {
        "type": "text",
        "analyzer": "autocomplete"
      }
    }
  },
  "settings": {
    "analysis": {
      "filter": {
        "edge_ngram_filter": {
          "type": "edge_ngram",
          "min_gram": 1,
          "max_gram": 20
        }
      },
      "analyzer": {
        "autocomplete": {
          "type": "custom",
          "tokenizer": "standard",
          "filter": [
            "lowercase",
            "edge_ngram_filter"
          ]
        }
      }
    }
  }
}



*/




        return $return;
    }

    /**
     * @description queryKey
     * @param $params
     * @return array
     */
    private function queryKey($params)
    {
        $mustArry=[];
        $filter=[];

        if(@$params['tenant_id']!='')
        $filter []= ['term' => ['tenant_id' => ['value' => $params['tenant_id']]]];

        if(@$params['agent_id'] && @$params['agentIdTop'] !=$params['agent_id'] ) {
            $filter []=  [
                "term" => [
                    "parent_id" => [
                        "value" => $params['agent_id']
                    ]
                ]
            ];
        }



        if (@$params['agentIdTop'] ) {
            $filter []=  [
                "term" => [
                    "parent_chain_ids" => [
                        "value" => $params['agentIdTop']
                    ]
                ]
            ];
        }




        if(@$params['currency']!='') {
            $mustArry['match'] = ["currency" => $params['currency']];
        }
        if (is_array($params['time_period']) &&
        $params['time_period']['fromdate'] != '' &&
        $params['time_period']['enddate'] != ''
           ) {

        $filter [] = [
            "range" => [
                "creation_date" => [
                    "from" =>$params['time_period']['fromdate'],
                    "include_lower"=> true,
                    "to" => str_replace("000000", "999999", $params['time_period']['enddate']),
                    "include_upper"=> true
                ]
            ]
        ];
    }
        if(@$params['searchKeyword']!=''){

            $filter[]=$this->searchWordQuery($params['searchKeyword']);
        }

        if(@$params['actionType'] ==='first-deposit'){

            $filter[] = [
                "bool" => [
                    "filter" => [
                        "bool" => [
                            "must_not" => [

                                "terms" => [
                                    "first_deposit_amount" => [
                                        "0.0"
                                    ]
                                ]

                            ]
                        ]
                    ]
                ]
            ];
        }
        if(@$params['actionType'] ==='no-bet'){

            $filter[] = [
                "bool" => [
                    "filter" => [
                        "bool" => [
                            "must" => [

                                "terms" => [
                                    "total_bets" => [
                                        "0.0"
                                    ]
                                ]

                            ]
                        ]
                    ]
                ]
            ];
        }

        if(@$params['actionType'] ==='no-deposit-amount'){

            $filter[] = [
                "bool" => [
                    "filter" => [
                        "bool" => [
                            "must" => [

                                "terms" => [
                                    "first_deposit_amount" => [
                                        "0.0"
                                    ]
                                ]

                            ]
                        ]
                    ]
                ]
            ];
        }


        $query = [
            'bool' => [
                'must' => count($mustArry)?$mustArry:['match_all' => new \stdClass]
            ]
        ];

        if(count( $filter)){
            $query['bool']['filter']=$filter;
        }

        return $query;
    }

    /**
     * @description sortKey
     * @param $sortBy
     * @param string $sortOrder
     * @return array
     */
    private function sortKey($sortBy, $sortOrder = 'ASC')
    {
        return [$sortBy => $sortOrder];
    }

    /**
     * @description totalBetCountKey
     * @return array
     */
    private function totalBetCountKey()
    {
        return ['sum' => ['field' => 'total_bets']];
    }

    /**
     * @description currencyKey
     * @return array
     */
    private function currencyKey()
    {

        $currencies = [
            'terms' => ['field' => 'currency', 'size' => 10000],
            'aggs' => ['total_balance' => $this->totalBalanceKey()]
        ];
        return $currencies;

    }

    /**
     * @description totalBalanceKey
     * @return array
     */
    private function totalBalanceKey()
    {
        return ['sum' =>
            [
                'script' => "doc['real_balance'].value + doc['non_cash_balance'].value"
            ]
        ];
    }

    /**
     * @description searchWordQuery
     * @param $str
     * @return array
     */
    private function searchWordQuery($str)
    {

        $result = array(
            "bool" => array(
                "should" => array(
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "player_id_s.word_start" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "player_id_s.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "user_name.word_start" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "user_name.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "nick_name.word_start" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "nick_name.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                    ,
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "phone.word_start" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "phone.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            )
        );


        return $result;
    }

    /**
     * @description totalAmountCountES
     * @param $params
     * @return mixed
     */
    private function totalAmountCountES($params)
    {

        $totalAmountCountES ['query'] = [
            'bool' =>
                array(
                    'must' =>
                        array(
                            'match_all' =>
                                new \stdClass,
                        ),
                    'filter' =>
                        array(
                            0 =>
                                array(
                                    'bool' =>
                                        array(
                                            'must_not' =>
                                                array(
                                                    'bool' =>
                                                        array(
                                                            'must_not' =>
                                                                array(
                                                                    'exists' =>
                                                                        array(
                                                                            'field' => 'player_details',
                                                                        ),
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            1 =>
                                array(
                                    'term' =>
                                        array(
                                            'player_details.demo' =>
                                                array(
                                                    'value' => "false",
                                                ),
                                        ),
                                ),
                        ),
                ),
        ];

        if (@$params['tenant_id'] != '')
            $totalAmountCountES ['query']['bool']['filter'] [] = ['term' => ['tenant_id' => ['value' => $params['tenant_id']]]];

        if (@$params['agent_id']) {
            $totalAmountCountES ['query']['bool']['filter'] [] = [
                "term" => [
                    "player_details.parent_chain_ids" => [
                        "value" => $params['agent_id']
                    ]
                ]
            ];
        }
        if (@$params['agent_id'] && $params['isDirectPlayer'] == 1) {
            $totalAmountCountES ['query']['bool']['filter'] [] = [
                "term" => [
                    "player_details.parent_id" => [
                        "value" => $params['agent_id']
                    ]
                ]
            ];
        }

        $totalAmountCountES ['timeout'] = '11s';
        $totalAmountCountES ['_source'] = false;
        $totalAmountCountES ['size'] = ALLCOUNT;
        $totalAmountCountES ['track_total_hits'] = true;
        $totalAmountCountES ['aggs'] =
            [
                'total_bets' =>
                    array(
                        'filter' =>
                            array(
                                'terms' =>
                                    array(
                                        'transaction_type' =>
                                            array(
                                                0 => 'bet',
                                                1 => 'bet_non_cash',
                                            ),
                                    ),
                            ),
                        'aggs' =>
                            array(
                                'total' =>
                                    array(
                                        'sum' =>
                                            array(
                                                'field' => 'amount_in_currencies.EUR',
                                            ),
                                    ),
                            ),
                    ),
                'total_refund' =>
                    array(
                        'filter' =>
                            array(
                                'terms' =>
                                    array(
                                        'transaction_type' =>
                                            array(
                                                0 => 'refund',
                                                1 => 'refund_non_cash',
                                            ),
                                    ),
                            ),
                        'aggs' =>
                            array(
                                'total' =>
                                    array(
                                        'sum' =>
                                            array(
                                                'field' => 'amount_in_currencies.EUR',
                                            ),
                                    ),
                            ),
                    ),
            ];

        return $totalAmountCountES;
    }
}
