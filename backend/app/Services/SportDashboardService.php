<?php

namespace App\Services;

use Elasticsearch\ClientBuilder;
use Illuminate\Support\Facades\Auth;

class SportDashboardService
{
    public static function fetchResult($params)
    {
        $obj = new self();
        $body = [
            'query' => $obj->queryKey($params),
            'timeout' => '60s',
            '_source' => 'true',
            'track_total_hits' => true,
            'aggs' => [
                'currencies' => [
                    "terms" => [
                        "field" => "player_details.currency",
                        "size" => ALLCOUNT
                    ],
                    "aggs" => [
//                        "chart_result" => $obj->getQueryChartResult($params),
                        "bet" => $obj->getQueryBet($params),
                        "bet_non_cash" => $obj->getNonCashQueryBet($params),
                        "bet_cr" => $obj->getQueryCRBet($params),
                        "bet_non_cash_cr" => $obj->getNonCashQueryCRBet($params),
                        "win" => $obj->getQueryWin($params),
                        "win_non_cash" => $obj->getNonCashQueryWin($params),
                        "win_dr" => $obj->getQueryDRWin($params),
                        "win_non_cash_dr" => $obj->getNonCashDRQueryWin($params),
                        "commission_amount" => $obj->getQueryCommissionAmount($params),
//                        "refund" => $obj->getQueryRefund($params),
//                        "bet_after_refund" => $obj->getQueryBetAfterRefund($params),
//                        "bet_after_refund_in_EUR" => $obj->getQueryBetAfterRefundInEur($params),
                    ]
                ],
            ]
        ];

        $params = [
            'index' => ELASTICSEARCH_INDEX['bet_transaction'],
            'body' => $body
        ];
        
        $client = getEsClientObj();
        $return = $client->search($params);
        return $return;
    }

    /**
     * @description queryKey
     * @param $params
     * @return mixed
     */
    private function queryKey($params)
    {
        $query["bool"] = [
            "must" => ["match_all" => new \stdClass],
        ];


        $query["bool"]['filter'][] = [
            "bool" => [
                "must_not" => [
                    "bool" => [
                        "must_not" => [
                            "exists" => [
                                "field" => "player_details"
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $query["bool"]['filter'][] = [
            "terms" => [
                "payment_for" => [
                    1,
                    2,
                    3,
                    4,
                    5
                ]
            ]
        ];

        $query["bool"]['filter'][] = [
            "bool" => [
                "should" => [
                    [
                        "bool" => [
                            "filter" => [
                                [
                                    "range" => [
                                        'created_at' => [
                                            'from' => $params['time_period']['fromdate'],
                                            'include_lower' => true,
                                            'to' => $params['time_period']['enddate'],//date('Y-m-d\T23:59:59.999\Z', strtotime($params['time_period']['enddate']))
                                            'include_upper' => true
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        if (@$params['tenantId']) {
            $query["bool"]['filter'][] = [
                "term" => [
                    "tenant_id" => [
                        "value" => $params['tenantId']
                    ]
                ]
            ];
        }

        /*if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
            $roles = getRolesDetails(@Auth::user()->id);
            if (!in_array('owner', $roles) && !in_array('sub-admin', $roles)) {
                $query["bool"]['filter'][] = [
                    "term" => [
                        "player_details.parent_chain_ids" => [
                            "value" => $params['agent_id']
                        ]
                    ]
                ];
            } else {
                if (@$params['isDirectPlayer'] == 'all' && $params['agent_id']) {
                    $query["bool"]['filter'][] = [
                        "term" => [
                            "player_details.parent_chain_ids" => [
                                "value" => $params['agent_id']
                            ]
                        ]
                    ];
                }
            }
        } else {
            $roles = getRolesDetails(@$params['agent_id']);
            if (!in_array('owner', $roles) && !in_array('sub-admin', $roles)) {
                $query["bool"]['filter'][] = [
                    "term" => [
                        "player_details.parent_chain_ids" => [
                            "value" => $params['agent_id']
                        ]
                    ]
                ];

            } else {
                if (@$params['isDirectPlayer'] == 'all' && $params['agent_id']) {
                    $query["bool"]['filter'][] = [
                        "term" => [
                            "player_details.parent_chain_ids" => [
                                "value" => $params['agent_id']
                            ]
                        ]
                    ];
                }
            }
        }*/
        if (@$params['agent_id']) {
            $query["bool"]['filter'][] = [
                "term" => [
                    "player_details.parent_chain_ids" => [
                        "value" => $params['agent_id']
                    ]
                ]
            ];

        }
        if (@$params['isDirectPlayer'] == 'direct' && $params['agent_id']) {
            $query["bool"]['filter'][] = [
                "term" => [
                    "player_details.parent_id" => [
                        "value" => $params['agent_id']
                    ]
                ]
            ];
        }

        return $query;
    }

    /*
      TODO
       Bet after refund = bet -refund
       GGR = >  (Bet after refund )- win


     * @description getQueryChartResult
     * @param $params
     */
    private function getQueryChartResult($params)
    {
        $query["filter"] = [
            "terms" => [
                "transaction_type" => [
                    "bet_placement",
                    "won",
                    "cashout",
                    "refund",
                    "lost_by_resettlement"
                ]
            ]
        ];
        $interval = 'hour';
        $intervalParam = $params['interval']??"today";
        if ($intervalParam === 'weekly') {
            $interval = 'day';
        } elseif ($intervalParam === 'monthly') {
            $interval = 'day';
        } elseif ($intervalParam === 'yearly') {
            $interval = 'month';
        }

        $query["aggs"] ["dashboard_charts"]["date_histogram"] = [
            "field" => "created_at",
            "interval" => $interval,
            "min_doc_count" => 0,
            "extended_bounds" => [
                "min" => $params['time_period']['fromdate'],
                "max" => $params['time_period']['enddate'],
            ]
        ];

        $query["aggs"] ["dashboard_charts"]["aggs"] = [

            "bet_for_chart" => [
                "filter" => [
                    "bool" => [
                        "filter" => [
                            [
                                "terms" => [
                                    "transaction_type" => [
                                        "bet_placement",
                                        "bet_non_cash"
                                    ]
                                ]
                            ],
                            [
                                "term" => [
                                    "player_details.demo" => "false"
                                ]
                            ]
                        ]
                    ]
                ],
                "aggs" => [
                    "total" => [
                        "sum" => [
                            "field" => "player_details.deducted_amount"
                        ]
                    ],
                    "total_in_EUR" => [
                        "sum" => [
                            "field" => "amount_in_currencies.EUR"
                        ]
                    ]
                ]
            ],
            "win_for_chart" => [
                "filter" => [
                    "bool" => [
                        "filter" => [
                            [
                                "terms" => [
                                    "transaction_type" => [
                                        "won",
                                        "win_non_cash"
                                    ]
                                ]
                            ],
                            [
                                "term" => [
                                    "player_details.demo" => "false"
                                ]
                            ]
                        ]
                    ]
                ],
                "aggs" => [
                    "total" => [
                        "sum" => [
                            "field" => "player_details.added_amount"
                        ]
                    ],
                    "total_in_EUR" => [
                        "sum" => [
                            "field" => "amount_in_currencies.EUR"
                        ]
                    ]
                ],
            ],
            "refund_for_chart" => [
                "filter" => [
                    "bool" => [
                        "filter" => [
                            [
                                "terms" => [
                                    "transaction_type" => [
                                        "refund",
                                        "refund_non_cash"
                                    ]
                                ]
                            ],
                            [
                                "term" => [
                                    "player_details.demo" => "false"
                                ]
                            ]
                        ]
                    ]
                ],
                "aggs" => [
                    "total" => [
                        "sum" => [
                            "field" => "player_details.added_amount"
                        ]
                    ],
                    "total_in_EUR" => [
                        "sum" => [
                            "field" => "amount_in_currencies.EUR"
                        ]
                    ]
                ]
            ],

            "bet_after_refund_for_chart" => [
                "bucket_script" => [
                    "buckets_path" => [
                        "bet" => "bet_for_chart>total",
                        "refund" => "refund_for_chart>total"
                    ],
                    "script" => "params.bet - params.refund"
                ]
            ],

            "bet_after_refund_for_chart_in_EUR" => [
                "bucket_script" => [
                    "buckets_path" => [
                        "bet" => "bet_for_chart>total_in_EUR",
                        "refund" => "refund_for_chart>total_in_EUR"
                    ],
                    "script" => "params.bet - params.refund"
                ]
            ],

        ];
        return $query;
    }

    /**
     * @param $params
     * @return mixed
     */
    private function getQueryWin($params)
    {
        $query["filter"] = [
            "bool" => [
                "filter" => [
                    [
                        "terms" => [
                            "payment_for" => [
                                2
                            ]
                        ]
                    ],
                    [
                        "terms" => [
                            "transaction_code" => [
                                "ResettleMarket","SettledMarket"
                            ]
                        ]
                    ],
                    [
                        "term" => [
                            "journal_entry" => "CR"
                        ]
                    ],
                    [
                        "term" => [
                            "player_details.demo" => "false"
                        ]
                    ]
                ]
            ]
        ];
        $query["aggs"] = [
            "total_win_amount" => [
                "sum" => [
                    "field" => "amount"
                ]
            ],
            "total_win_amount_in_EUR" => [
                "sum" => [
                    "field" => "amount_in_currencies.EUR"
                ]
            ],
//            "top" => array(
//                "top_hits" => array(
//                    "_source" => array(
//                        "includes" => [
//                            "player_details.player_name",
//                            "player_details.agent_name",
//                            "player_details.phone",
//                            "player_details.player_id",
//                            "player_details.wallet_id",
//                            "player_details.email",
//                            "amount",
//                            "amount_in_currencies",
//                            "non_cash_amount_in_currencies",
//                        ]
//                    )
//                )
//            ),
        ];

        return $query;
    }

    /**
     * @param $params
     * @return mixed
     */
    private function getQueryCommissionAmount($params)
    {
        $query["filter"] = [
            "bool" => [
                "filter" => [
                    [
                        "terms" => [
                            "payment_for" => [
                                2
                            ]
                        ]
                    ],
                    [
                        "terms" => [
                            "transaction_code" => [
                                "ResettleMarket","SettledMarket"
                            ]
                        ]
                    ],
                    [
                        "term" => [
                            "journal_entry" => "CR"
                        ]
                    ],
                    [
                        "term" => [
                            "player_details.demo" => "false"
                        ]
                    ]
                ]
            ]
        ];
        $query["aggs"] = [
            "commission_amount" => [
                "sum" => [
                    "field" => "commission_amount"
                ]
            ],
            "total_commission_amount_in_EUR" => [
                "sum" => [
                    "field" => "commission_amount_in_currencies.EUR"
                ]
            ],
        ];

        return $query;
    }

    /**
     * @param $params
     * @return mixed
     */
    private function getNonCashQueryWin($params)
    {
        $query["filter"] = [
            "bool" => [
                "filter" => [
                    [
                        "terms" => [
                            "payment_for" => [
                                2
                            ]
                        ]
                    ],
                    [
                        "terms" => [
                            "transaction_code" => [
                                "ResettleMarket","SettledMarket"
                            ]
                        ]
                    ],
                    [
                        "term" => [
                            "journal_entry" => "CR"
                        ]
                    ],
                    [
                        "term" => [
                            "player_details.demo" => "false"
                        ]
                    ]
                ]
            ]
        ];
        $query["aggs"] = [
            "total_win_amount" => [
                "sum" => [
                    "field" => "non_cash_amount"
                ]
            ],
            "total_win_amount_in_EUR" => [
                "sum" => [
                    "field" => "non_cash_amount_in_currencies.EUR"
                ]
            ]
        ];

        return $query;
    }


    /**
     * @param $params
     * @return mixed
     */
    private function getQueryDRWin($params)
    {
        $query["filter"] = [
            "bool" => [
                "filter" => [
                    [
                        "terms" => [
                            "payment_for" => [
                                2
                            ]
                        ]
                    ],
                    [
                        "terms" => [
                            "transaction_code" => [
                                "ResettleMarket","SettledMarket"
                            ]
                        ]
                    ],
                    [
                        "term" => [
                            "journal_entry" => "DR"
                        ]
                    ],
                    [
                        "term" => [
                            "player_details.demo" => "false"
                        ]
                    ]
                ]
            ]
        ];
        $query["aggs"] = [
            "total_win_amount" => [
                "sum" => [
                    "field" => "amount"
                ]
            ],
            "total_win_amount_in_EUR" => [
                "sum" => [
                    "field" => "amount_in_currencies.EUR"
                ]
            ]
        ];

        return $query;
    }

    /**
     * @param $params
     * @return mixed
     */
    private function getNonCashDRQueryWin($params)
    {
        $query["filter"] = [
            "bool" => [
                "filter" => [
                    [
                        "terms" => [
                            "payment_for" => [
                                2
                            ]
                        ]
                    ],
                    [
                        "terms" => [
                            "transaction_code" => [
                                "ResettleMarket","SettledMarket"
                            ]
                        ]
                    ],
                    [
                        "term" => [
                            "journal_entry" => "DR"
                        ]
                    ],
                    [
                        "term" => [
                            "player_details.demo" => "false"
                        ]
                    ]
                ]
            ]
        ];
        $query["aggs"] = [
            "total_win_amount" => [
                "sum" => [
                    "field" => "non_cash_amount"
                ]
            ],
            "total_win_amount_in_EUR" => [
                "sum" => [
                    "field" => "non_cash_amount_in_currencies.EUR"
                ]
            ]
        ];

        return $query;
    }

    /**
     * @description getQueryBetAfterRefund
     * @param $params
     */
    private function getQueryBetAfterRefund($params)
    {
        $query["bucket_script"] = [
            "buckets_path" => [
                "bet" => "bet>bet_amount",
                "refund" => "refund>refund_amount"
            ],
            "script" => "params.bet - params.refund"

        ];

        return $query;
    }

    /**
     * @description getQueryBetAfterRefundInEur
     */
    private function getQueryBetAfterRefundInEur($params)
    {
        $query["bucket_script"] = [
            "buckets_path" => [
                "bet" => "bet>bet_amount_in_EUR",
                "refund" => "refund>refund_amount_in_EUR"
            ],
            "script" => "params.bet - params.refund"
        ];
        return $query;
    }

    /**
     * @description getQueryBet
     * @param $params
     * @return mixed
     */
    private function getQueryBet($params){
        $query['filter'] = [
            "bool" => [
                "filter" => [
                    [
                        "terms" => [
                            "payment_for" => [
                                1
                            ]
                        ]
                    ],
                    [
                        "term" => [
                            "journal_entry" => "DR"
                        ]
                    ],
                    [
                        "terms" => [
                            "betslip_details.settlement_status" => [
                                "ResettleMarket", "SettledMarket"
                            ]
                        ]
                    ],
                    [
                        "term" => [
                            "player_details.demo" => "false"
                        ]
                    ]
                ]
            ]
        ];
        $query['aggs'] = [
            "bet_amount" => [
                "sum" => [
                    "field" => "amount"
                ]
            ],
            "bet_amount_in_EUR" => [
                "sum" => [
                    "field" => "amount_in_currencies.EUR"
                ]
            ]
        ];

        return $query;
    }

    private function getNonCashQueryBet($params){
        $query['filter'] = [
            "bool" => [
                "filter" => [
                    [
                        "terms" => [
                            "payment_for" => [
                                1
                            ]
                        ]
                    ],
                    [
                        "term" => [
                            "journal_entry" => "DR"
                        ]
                    ],
                    [
                        "terms" => [
                            "betslip_details.settlement_status" => [
                                "ResettleMarket","SettledMarket"
                            ]
                        ]
                    ],
                    [
                        "term" => [
                            "player_details.demo" => "false"
                        ]
                    ]
                ]
            ]
        ];
        $query['aggs'] = [
            "bet_amount" => [
                "sum" => [
                    "field" => "non_cash_amount"
                ]
            ],
            "bet_amount_in_EUR" => [
                "sum" => [
                    "field" => "non_cash_amount_in_currencies.EUR"
                ]
            ]
        ];

        return $query;
    }

    private function getQueryCRBet($params){
        $query['filter'] = [
            "bool" => [
                "filter" => [
                    [
                        "terms" => [
                            "payment_for" => [
                                1
                            ]
                        ]
                    ],
                    [
                        "term" => [
                            "journal_entry" => "CR"
                        ]
                    ],
                    [
                        "terms" => [
                            "betslip_details.settlement_status" => [
                                "ResettleMarket","SettledMarket"
                            ]
                        ]
                    ],
                    [
                        "term" => [
                            "player_details.demo" => "false"
                        ]
                    ]
                ]
            ]
        ];
        $query['aggs'] = [
            "bet_amount" => [
                "sum" => [
                    "field" => "amount"
                ]
            ],
            "bet_amount_in_EUR" => [
                "sum" => [
                    "field" => "amount_in_currencies.EUR"
                ]
            ]
        ];

        return $query;
    }

    private function getNonCashQueryCRBet($params){
        $query['filter'] = [
            "bool" => [
                "filter" => [
                    [
                        "terms" => [
                            "payment_for" => [
                                1
                            ]
                        ]
                    ],
                    [
                        "term" => [
                            "journal_entry" => "CR"
                        ]
                    ],
                    [
                        "terms" => [
                            "betslip_details.settlement_status" => [
                                "ResettleMarket","SettledMarket"
                            ]
                        ]
                    ],
                    [
                        "term" => [
                            "player_details.demo" => "false"
                        ]
                    ]
                ]
            ]
        ];
        $query['aggs'] = [
            "bet_amount" => [
                "sum" => [
                    "field" => "non_cash_amount"
                ]
            ],
            "bet_amount_in_EUR" => [
                "sum" => [
                    "field" => "non_cash_amount_in_currencies.EUR"
                ]
            ]
        ];

        return $query;
    }

    /**
     * @description getQueryRefund
     * @param $params
     * @return mixed
     */
    private function getQueryRefund($params)
    {
        $query['filter'] = [
            "bool" => [
                "filter" => [
                    [
                        "terms" => [
                            "transaction_type" => [
                                "refund",
                                "refund_non_cash"
                            ]
                        ]
                    ],
                    [
                        "term" => [
                            "player_details.demo" => "false"
                        ]
                    ]
                ]
            ]
        ];
        $query['aggs'] = [
            "refund_amount" => [
                "sum" => [
                    "field" => "player_details.added_amount"
                ]
            ],
            "refund_amount_in_EUR" => [
                "sum" => [
                    "field" => "amount_in_currencies.EUR"
                ]
            ]
        ];


        return $query;
    }
}
