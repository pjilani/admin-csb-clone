<?php

namespace App\Services;

use App\Models\Admin;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Models\Transactions;
use Illuminate\Http\Response;
use App\Models\Notifications;
use App\Models\NotificationReceivers as NotificationReceiversModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;

class NotificationService
{
    /**
     * @param $wallet_id
     * @param $nonCashAmount
     * @param $transaction_type
     * @param $notificationType
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public static function createNotification($walletId, $nonCashAmount, $transactionType, $notificationType)
    {
        
            $wallet = getWallet($walletId);

            $insertData['sender_type'] = Auth::user()->parent_type;
            $insertData['sender_id'] = Auth::user()->id;
            $insertData['reference_type'] = @$wallet[0]->owner_type;
            $insertData['reference_id'] = @$wallet[0]->owner_id;

            $insertData['created_at'] = @date('Y-m-d H:i:s');
            $nameWithEmail='';
            if(@$wallet[0]->owner_type == ADMIN_TYPE){
                $admin = Admin::find($wallet[0]->owner_id);

                $nameWithEmail= @$admin->email ."|".@$admin->first_name ." ".@$admin->last_name;
            }elseif(@$wallet[0]->owner_type == USER_TYPE){
                $user = User::find($wallet[0]->owner_id);
                $nameWithEmail= @$user->email ." | ".@$user->first_name ." ".@$user->last_name;
            }
            if ($transactionType == 'deposit') {

                $insertData['message'] = "$nameWithEmail #" . $nonCashAmount . " ". @$wallet[0]->currency_name." ". DEPOSIT_NOTIFICATION;
            } else {
                $insertData['message'] = "$nameWithEmail #" . $nonCashAmount . " " . @$wallet[0]->currency_name ." ". WITHDRAW__NOTIFICATION;
            }

            $createdValues = Notifications::create($insertData);
            if ($createdValues) {
                $insertReciever['notification_id'] = $createdValues->id;
                $insertReciever['is_read'] = false;
                $insertReciever['created_at'] = @date('Y-m-d H:i:s');
                $insertReciever['receiver_type'] = @$wallet[0]->owner_type;
                $insertReciever['receiver_id'] = @$wallet[0]->owner_id;

                NotificationReceiversModel::create($insertReciever);


                return true;
            }
   
    }


    public static function addNotification($receiverId, $receiverType, $message)
    {
        

            $insertData['sender_type'] = (isset(Auth::user()->parent_type) ? Auth::user()->parent_type : 0);
            $insertData['sender_id'] = (isset(Auth::user()->id) ? Auth::user()->id : 0);;
            $insertData['reference_type'] = $receiverType;
            $insertData['reference_id'] = $receiverId;
            $insertData['message'] = $message;
            $insertData['created_at'] = @date('Y-m-d H:i:s');

            $createdValues = Notifications::create($insertData);
            if ($createdValues) {
                $insertReciever['notification_id'] = $createdValues->id;
                $insertReciever['is_read'] = false;
                $insertReciever['created_at'] = @date('Y-m-d H:i:s');
                $insertReciever['receiver_type'] = $receiverType;
                $insertReciever['receiver_id'] = $receiverId;
                return NotificationReceiversModel::create($insertReciever);
            }

        }
    public static function getNotification($id,$type)
    {
        return NotificationReceiversModel::select('notification_receivers.id', 'notification_receivers.is_read', 'notifications.message', 'notifications.sender_id', 'notifications.sender_type')
        ->leftJoin('notifications', 'notification_receivers.receiver_id', '=', 'notifications.id')
        ->where('receiver_id', '=', $id)
        ->where('receiver_type', '=', $type)
        ->where('is_read', '=', false)
        ->orderBy('id', 'desc')
        ->get();
    }
    
}
