<?php

namespace App\Services;

use Elasticsearch\ClientBuilder;
use Carbon\Carbon;

class UnifiedTransactionReportService
{


    public static function fetchResult($params)
    {
        $obj = new self();

        $body = [
            'query' => $obj->queryKey($params),
            'sort' => $obj->sortKey($params['sortBy'], $params['sortOrder']),
            'timeout' => '60s',
            'track_total_hits' => true,
            'aggs' => [
                'total_bets' => $obj->totalBetsKey($params),
                'total_refund' => $obj->totalRefundKey($params),
                'total_wins' => $obj->totalWinsKey($params),
                'total_tip' => $obj->totalTipKey($params),
                'total_deposit' => $obj->totalDepositKey($params),
                'total_withdraw' => $obj->totalWithdrawKey($params),
                'total_withdraw_cancel' => $obj->totalWithdrawCancelKey($params)


            ]
        ];


        if ($params['limit'] != 'all') {
            $body['size'] = (int)$params['limit'];
            $body['from'] = (int)$params['offset'];
        } else {
            $body['size'] = ALLCOUNT;
        }


        $params = [
            'index' => ELASTICSEARCH_INDEX['transactions'],
            'body' => $body
        ];
      
        $client = getEsClientObj();

        $return = $client->search($params);


        return $return;
    }

    private function totalAmountKey($params)
    {
        return ["sum" => ["field" => "amount_in_currencies.".$params['tenant_base_currency']]];
    }

    private function totalBetsKey($params)
    {
        $total_bets = ["filter" => ["terms" => ["transaction_type" => ["bet", "bet_non_cash"]]],
            "aggs" => ["total" => $this->totalAmountKey($params)]
        ];


        return $total_bets;
    }


    private function totalRefundKey($params)
    {
        $total_refund = ["filter" => ["terms" => ["transaction_type" => ["refund", "refund_non_cash"]]],
            "aggs" => ["total" => $this->totalAmountKey($params)]
        ];

        return $total_refund;
    }


    private function totalWinsKey($params)
    {
        $total_wins = ["filter" => ["terms" => ["transaction_type" => ["win", "win_non_cash"]]],
            "aggs" => ["total" => $this->totalAmountKey($params)]
        ];

        return $total_wins;
    }

    private function totalTipKey($params)
    {
        $total_tip = ["filter" => ["terms" => ["transaction_type" => ["tip", "tip_non_cash"]]],
            "aggs" => ["total" => $this->totalAmountKey($params)]
        ];


        return $total_tip;
    }

    private function totalDepositKey($params)
    {
        $total_deposit = [
            "filter" => [
                "terms" => [
                    "transaction_type" => [
                        "deposit",
                        "non_cash_granted_by_admin"
                    ]
                ]
            ], "aggs" => ["total" => $this->totalAmountKey($params)]

        ];


        return $total_deposit;
    }


    private function totalWithdrawKey($params)
    {
        $total_withdraw = ["filter" => array(
            "bool" => [
                "must" => [
                    [
                        "terms" => [
                            "transaction_type" => ["withdraw", "non_cash_withdraw_by_admin"]
                        ]
                    ],
                    [
                        "term" => [
                            "status" => "success"
                        ]
                    ]
                ],
                "must_not" => [
                    "terms" => [
                        "description" => [
                            "cancelled by admin", "cancelled by the player", "Pending confirmation from admin"
                        ]
                    ]
                ]
            ]
        ),
            "aggs" => ["total" => $this->totalAmountKey($params)]
        ];

        return $total_withdraw;
    }


    public function totalWithdrawCancelKey($params)
    {
        $total_withdraw_cancel = ["filter" => ["term" => ["transaction_type" => "withdraw_cancel"]],
            "aggs" => ["total" => $this->totalAmountKey($params)]
        ];

        return $total_withdraw_cancel;


    }


    private function sortKey($sortBy, $sortOrder = 'ASC')
    {
        return [$sortBy => $sortOrder];
    }


    private function queryKey($params)
    {
        $mustArry = [];
        $filter = [];
        if (@$params['tenant_id'] != '')
            $filter [] = ['term' => ['tenant_id' => ['value' => $params['tenant_id']]]];

        $filter [] = ["bool" =>
            ["must_not" => [
                "bool" => [
                    "must_not" => [
                        "exists" => [
                            "field" => "player_details"
                        ]
                    ]
                ]
            ]
            ]
        ];

        if (@$params['owner_id'] && ($params['agentId']==''|| $params['agentId']==0)) {
            $filter [] = ['term' => ['player_details.parent_chain_ids' => ['value' => $params['owner_id']]]];
        }elseif ($params['isDirectPlayer'] == 'all' && @$params['agentId']) {
            $filter [] = ['term' => ['player_details.parent_chain_ids' => ['value' => $params['agentId']]]];

        } elseif ($params['isDirectPlayer'] == 'direct' && @$params['agentId']) {
            $filter [] = ['term' => ['player_details.parent_id' => ['value' => $params['agentId']]]];
        }

        if (@$params['action_type']) {
            $filter [] = ['term' => ['transaction_type' => ['value' => $params['action_type']]]];
        } else {
            $filter [] = ['terms' =>
                [
                    'transaction_type' => [
                            "deposit",
                            "withdraw",
                            "withdraw_cancel",
                            "deposit_bonus_claim",
                            "non_cash_granted_by_admin",
                            "bet",
                            "win",
                            "refund",
                            "tip",
                            "bet_non_cash",
                            "win_non_cash",
                            "tip_non_cash",
                            "refund_non_cash"
                    ]
                ]
            ];
        }

        if (@$params['type']){
            if(@$params['type'] ==='financial'){
                $filter[] = [
                    "bool" => [
                        "filter" => [
                            "bool" => [
                                "must" => [
                                    "term" => [
                                        "round_id_s" => ""
                                    ]

                                ]
                            ]
                        ]
                    ]
                ];
            } if(@$params['type'] ==='game'){
                $filter[] = [
                    "bool" => [
                        "filter" => [
                            "bool" => [
                                "must_not" => [
                                    "term" => [
                                        "round_id_s" => ""
                                    ]

                                ]
                            ]
                        ]
                    ]
                ];
            }
        }

        if (@$params['internal_error_code'])
            $filter [] = ['term' => ['internal_error_code' => ['value' => $params['internal_error_code']]]];

        if (@$params['game_provider'])
            $filter [] = ['term' => ['game_provider' => ['value' => $params['game_provider']]]];

        if (@$params['game_type'])
            $filter [] = ['term' => ['game_type' => ['value' => $params['game_type']]]];

        if (@$params['currency'] != '') {
            $mustArry["multi_match"] = [
                "query" => $params['currency'],
                "fields" => [
                    "player_details.currency"
                ],
                "type" => "best_fields",
                "operator" => "OR"
            ];
        }

        if (is_array($params['time_period']) &&
            $params['time_period']['fromdate'] != '' &&
            $params['time_period']['enddate'] != ''
        ) {

            $filter [] = [
                "range" => [
                    "created_at" => [
                        "from" =>$params['time_period']['fromdate'],
                        "include_lower"=> true,
                        "to" => str_replace("000000", "999999", $params['time_period']['enddate']),
                        "include_upper"=> true
                    ]
                ]
            ];
        }



        if ($params['searchKeyword'] != '') {
            $filter [] = $this->searchWordQuery($params['searchKeyword']);
        }
        $query = [
            'bool' => [
                'must' => count($mustArry) ? $mustArry : ['match_all' => new \stdClass]
            ]
        ];

        if (count($filter)) {
            $query['bool']['filter'] = $filter;
        }
        return $query;
    }

    private function searchWordQuery($str)
    {
        $result = array(
            "bool" => array(
                "should" => array(
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "player_details.player_id_s.word_start" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "player_details.player_id_s.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "player_details.player_name.word_start" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "player_details.player_name.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "player_details.agent_name.word_start" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "player_details.agent_name.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "round_id_s.word_start" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "round_id_s.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "transaction_id.word_start" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "transaction_id.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                    ,
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "player_details.phone.word_start" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "player_details.phone.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            )
        );
        return $result;
    }


}
