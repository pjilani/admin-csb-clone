<?php

namespace App\Services;

use App\Aws3;
use App\Models\Casino\CasinoGames;
use App\Models\Casino\CasinoItems;
use App\Models\Casino\CasinoMenuItem;
use App\Models\Casino\CasinoMenus;
use App\Models\Casino\CasinoPages;
use App\Models\Casino\CasinoPagesMenus;
use App\Models\Casino\CasinoProviders;
use App\Models\Casino\CasinoTables;
use App\Models\Menu\MenuTenantSettingModel;
use App\Models\MenuMaster;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\TenantThemeSettings;
use App\Models\TenantCredentials;

class St8GamesService
{
    public static function index()
    {
        try {
            setUnlimited('4G');
            $gameProvider = 'st8';
            // $gameProviderName = CasinoProviders::where('name', $gameProvider)->first();
            // if ($gameProviderName) {
            //     $tenants = TenantThemeSettings::whereRaw("'$gameProviderName->id' = ANY (string_to_array(assigned_providers,','))")->get();
            //     if (!empty($tenants) && count($tenants) > 0) {
            //         foreach ($tenants as $tn) {
            //             $tenantIds[] = $tn->tenant_id;
            //         }
            //     }
            // }
            $tenantIds = [17]; // temp
            // foreach ($tenantIds as $tenantId) {

            //     $accessKeyArray = TenantCredentials::select('key', 'value')
            //         ->where('tenant_id', $tenantId)
            //         ->where(function ($query) {
            //             $query->where('key', "ST8_LAUNCH_URL")
            //                 ->orwhere('key', "ST8_SITE_LOBBY")
            //                 ->orwhere('key', "ST8_PRIVATE_KEY")
            //                 ->orWhere('key', "ST8_SITE_KEY");
            //         })
            //         ->pluck('value', 'key')->toArray();
            //     // dd($accessKeyArray);
            //     $launchURL = $accessKeyArray['ST8_LAUNCH_URL'];
            //     $sitelobbyUrl = str_replace("www", 'st8', $accessKeyArray['ST8_SITE_LOBBY']);  //echo str_replace("world","Peter","Hello world!");
            //     $siteKey = $accessKeyArray['ST8_SITE_KEY'];
            //     $privateKey = $accessKeyArray['ST8_PRIVATE_KEY'];

            /* if (empty($launchURL) || empty($sitelobbyUrl) ||empty($siteKey) ||empty($privateKey)) {
                      return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
                  }*/
            // }

            DB::beginTransaction();


            $tenantId = $tenantIds;
            $casinoPageMenuName = [];
            $casinoPageMenuWiseGames = [];

            // $data = "site=samino.staging";

            // $private_key = <<<EOD
            // -----BEGIN RSA PRIVATE KEY-----
            // MIIBOgIBAAJBANDiE2+Xi/WnO+s120NiiJhNyIButVu6zxqlVzz0wy2j4kQVUC4Z
            // RZD80IY+4wIiX2YxKBZKGnd2TtPkcJ/ljkUCAwEAAQJAL151ZeMKHEU2c1qdRKS9
            // sTxCcc2pVwoAGVzRccNX16tfmCf8FjxuM3WmLdsPxYoHrwb1LFNxiNk1MXrxjH3R
            // 6QIhAPB7edmcjH4bhMaJBztcbNE1VRCEi/bisAwiPPMq9/2nAiEA3lyc5+f6DEIJ
            // h1y6BWkdVULDSM+jpi1XiV/DevxuijMCIQCAEPGqHsF+4v7Jj+3HAgh9PU6otj2n
            // Y79nJtCYmvhoHwIgNDePaS4inApN7omp7WdXyhPZhBmulnGDYvEoGJN66d0CIHra
            // I2SvDkQ5CmrzkW5qPaE2oO7BSqAhRZxiYpZFb5CI
            // -----END RSA PRIVATE KEY-----
            // EOD;
            // $signature = '';
            // // compute signature
            // openssl_sign($data, $signature, $private_key, OPENSSL_ALGO_SHA256);

            // $encoded_signature = base64_encode($signature);
            // dd($encoded_signature === 'MEUCIAkeQ4yjHmMDWL8tvJ/R3hZIkdSwKBaqEJjCRO6KONUQAiEArK8ZoJdjCUgXopA4vR3KouDJlcxmtvxn5X8RRLspcAE=');

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://st8-api.hal567.com/api/v1/st8/callback',//$sitelobbyUrl
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',//    "url": '.$launchURL.'"/games?site="'.$siteKey.',
                CURLOPT_POSTFIELDS => '{
    "url": "https://smn001.o.p8d.xyz/api/operator/v1/games?site=samino.staging",
    "method": "get",
    "headers":{
        "x-st8-sign":"MEUCIAkeQ4yjHmMDWL8tvJ/R3hZIkdSwKBaqEJjCRO6KONUQAiEArK8ZoJdjCUgXopA4vR3KouDJlcxmtvxn5X8RRLspcAE="
    }
}',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                ),
            ));

            $response = curl_exec($curl);
print_r($response);
            curl_close($curl);

//            $st8Data = json_decode($response, true);
            $st8Data = json_decode(st8Data(), true);
            $imageGenerationLink = 'https://luckmedia.link/game_code/thumb_3_2.webp';
            foreach ($st8Data['data']['games'] as &$gamesImg) {
               /* $webpImage = str_replace('game_code', $gamesImg['code'], $imageGenerationLink);
                $image = imagecreatefromwebp($webpImage);
                $directory = storage_path('app/public/st8Images/');
                if (!file_exists($directory)) {
                    mkdir($directory, 0777, true);
                }
                $pngImagePath = $directory . Str::uuid() . '____'. $gamesImg['code'] . '.png';
                $res = imagepng($image, $pngImagePath);
                imagedestroy($image);
                if ($res) {
                    $aws3 = new Aws3();
                    $PathS3Key = $aws3->uploadFile('st8Images/' . Str::uuid() . '____'. $gamesImg['code'], $pngImagePath, 'png');
                    $PathS3Key;
                    $gamesImg['image'] = $PathS3Key;
                }
               https://ezugi-main-platform-development.s3.amazonaws.com/st8/games/pgp_sweet_bonanza.png
               */
                $gamesImg['image'] = 'https://ezugi-main-platform-development.s3.amazonaws.com/st8/games/'.$gamesImg['code'].'.png';
            }
            $gameData = [];
            $pagesData = [];
            foreach ($st8Data['data']['categories'] as $category) {
                if ($category['name'] == 'Unknown') continue;
                $gameData[] = [
                    'game_name' => $category['name'],
                    'game_identificator' => Str::snake($category['name']),
                ];
            }
            $tableCollection = collect($st8Data['data']['games']);
            foreach ($gameData as &$data) {
                $value = $tableCollection->where('category', $data['game_name']);
                foreach ($value as $table) {
                    $data['table'][] = [
                        'name' => $table['name'],
                        'id' => $table['code']
                    ];
                }
            }
            $menuMasterData = [
                ['name' => 'Fishing', 'image' => 'https://ezugi-main-platform-staging-storage.s3.amazonaws.com/menus%2F10%2Flogo%2Fa0d8cfee-aa14-40e0-8bb1-cb159d02e2bb____fishing.svg'],
                ['name' => 'Casino', 'image' => 'https://ezugi-main-platform-staging-storage.s3.amazonaws.com/menus%2F3%2Flogo%2F91f05f34-314f-4535-9d87-5545f120f9b2____casino.svg']
            ];

            $pageImages = [
                "Red Tiger" => "https://ezugi-main-platform-staging-storage.s3.amazonaws.com/pages%2F37%2Flogo%2F4c07666e-df40-4f31-8f49-6eee79eda506____dTiger.svg",
                "NetEnt" => "https://ezugi-main-platform-staging-storage.s3.amazonaws.com/pages%2F38%2Flogo%2Fb7fa06e6-cd4c-4c23-9930-39d1e707d9ba____netent-logotype.svg",
                "Evolution Gaming" => '',
                "Play'n Go" => "https://ezugi-main-platform-staging-storage.s3.amazonaws.com/pages%2F51%2Flogo%2F0123fcbd-840d-4628-9f4b-7c36df9340d7____playngo.png",
                "Pragmatic Play" => "https://ezugi-main-platform-staging-storage.s3.amazonaws.com/pages%2F40%2Flogo%2F3c4b484f-0335-40b6-a5a0-71b76c8d4a8e____PP-white-logo.svg",
                "Relax Gaming" => "https://ezugi-main-platform-staging-storage.s3.amazonaws.com/pages%2F54%2Flogo%2Fdc32081d-9be6-4882-bd2e-7d046fa5e6ef____Relax-Gaming-2-1024x576-removebg-preview.png",
                "Gamzix" => "https://ezugi-main-platform-staging-storage.s3.amazonaws.com/pages%2F49%2Flogo%2F20fbb7c6-c3c1-4804-bed0-6730d0a10582____White-logo.png",
                "Playtech" => "https://ezugi-main-platform-staging-storage.s3.amazonaws.com/pages%2F52%2Flogo%2Fc6143094-e7e8-485f-b1a5-24b170c3474e____playtech-logo.png",
                "Nolimit City" => "https://ezugi-main-platform-staging-storage.s3.amazonaws.com/pages%2F41%2Flogo%2Fe509b779-5691-4410-8ad0-3787057965d5____nolimit-city.png",
                "Aviatrix" => '',
                "Jili" => "https://ezugi-main-platform-staging-storage.s3.amazonaws.com/pages%2F50%2Flogo%2Fb7e0c5b2-9c0e-401a-89b6-68cf6214a0c1____logo_jili_us-en.png",
                "3 Oaks Gaming" => "https://ezugi-main-platform-staging-storage.s3.amazonaws.com/pages%2F42%2Flogo%2F3c5acb5b-a5a0-4276-8eeb-2618652e3cd4____3oaks.svg",
                "Spinomenal" => "https://ezugi-main-platform-staging-storage.s3.amazonaws.com/pages%2F55%2Flogo%2Fe9941237-1447-428a-8260-1e495856089b____spinomenal-logo-removebg-preview.png",
                "Big Time Gaming" => "https://ezugi-main-platform-staging-storage.s3.amazonaws.com/pages%2F43%2Flogo%2Fc1d2677b-a787-458c-8713-1fb77dd4a347____BTG_Logo_White.png",
                "Wazdan" => "https://ezugi-main-platform-staging-storage.s3.amazonaws.com/pages%2F56%2Flogo%2F2ffe50dd-eca1-4c25-9135-a0e0ecc2302c____logo.svg",
                "GameBeat" => "https://ezugi-main-platform-staging-storage.s3.amazonaws.com/pages%2F48%2Flogo%2F49d3d205-2a53-4144-a0b9-68c6c84f67b7____gamebeat-logo-400px-removebg-preview.png",
                "Push Gaming" => ''
            ];

            $casinoMenuImages = [
                "Video Slots" => "https://ezugi-main-platform-staging-storage.s3.amazonaws.com/tenants%2F1%2Fmenus%2F0a9a6190-1667-4184-aa96-40380e993e8a____1000216216.png",
                "Live Roulette" => "",
                "Live Poker" => "https://ezugi-main-platform-staging-storage.s3.amazonaws.com/tenants%2F1%2Fmenus%2Fe0e38126-1ff1-4486-8acf-058bcf958a20____tenants_3_menus_647f9612-8786-4aca-8467-43f9ec1da669____poker.png",
                "Live Blackjack" => "https://ezugi-main-platform-staging-storage.s3.amazonaws.com/tenants%2F1%2Fmenus%2F02127504-0bb4-4036-8e65-8e90d0fcad97____tenants_3_menus_db5a6bfd-044e-4397-8520-cd2884a030c1____Blackjack.png",
                "Game Show" => "https://ezugi-main-platform-staging-storage.s3.amazonaws.com/tenants%2F1%2Fmenus%2Fc2112210-82e3-4e60-981f-56f35330b7be____tenants_2_menus_524d827a-e503-4d7c-bab8-9329d73fbdeb____slot.png",
                "Live Dealer" => "https://ezugi-main-platform-staging-storage.s3.amazonaws.com/tenants%2F1%2Fmenus%2F509412ca-ce18-4f79-91f1-0e12601fdbb6____tenants_3_menus_94095209-d390-45b4-b228-59e09539ddbc____ANdar_bhaar.png",
                "Live Baccarat" => "https://ezugi-main-platform-staging-storage.s3.amazonaws.com/tenants%2F1%2Fmenus%2F281bf613-e184-4cfc-b997-8f331913fbfb____tenants_3_menus_9b03c069-a343-4485-99bc-49f8d0b97ade____baccarat_%281%29.png",
                "Live Dragon Tiger" => "",
                "Live Sic Bo" => "https://ezugi-main-platform-staging-storage.s3.amazonaws.com/tenants%2F1%2Fmenus%2F1f5b4b6b-8eca-442c-9cd5-d303459372f5____3508884.png",
                "Live Lobby" => "",
                "Table Games" => "https://ezugi-main-platform-staging-storage.s3.amazonaws.com/tenants%2F1%2Fmenus%2F01685afa-a232-4c34-ae49-aa50916397c3____undertable.png",
                "Blackjack" => "https://ezugi-main-platform-staging-storage.s3.amazonaws.com/tenants%2F1%2Fmenus%2Fe7abddee-6468-4140-97c2-993d65ee748e____tenants_1_menus_16a66d4b-c7a3-47da-9bc9-32addda572a7____HiH8G6qrwiVkNTyH5oCS1tvn.png",
                "Roulette" => "https://ezugi-main-platform-staging-storage.s3.amazonaws.com/tenants%2F1%2Fmenus%2Fea01d47c-76ab-4ce2-8b9b-7411fc486fcc____tenants_1_menus_6b06f3b5-ce43-4fdc-ba65-ca129b1190a1____btMmy7xTwGoEX4yJuQAejPUc.png",
                "Crash Games" => "https://ezugi-main-platform-staging-storage.s3.amazonaws.com/tenants%2F1%2Fmenus%2F6f075e54-84cf-4065-9bf7-2a6af5bed2de____Crash_game.png",
                "Fishing Games" => "https://ezugi-main-platform-staging-storage.s3.amazonaws.com/tenants%2F1%2Fmenus%2F9552e63e-7402-4f52-ab25-0e5ed923e509____Fishing_Games.png",
                "Virtual Sports" => "https://ezugi-main-platform-staging-storage.s3.amazonaws.com/tenants%2F1%2Fmenus%2Fbfa53f24-529e-42b5-bbc9-c563d6fa1562____Virtual_Sports.png",
            ];

            foreach (collect($st8Data['data']['developers'])->where('name', '!=', 'Unknown')->toArray() as $pages) {
                if ($pages['name'] == 'RubyPlay') continue;
                $pagesData[] = ['name' => $pages['name'], 'image' => ''];
            }

            foreach ($pagesData as $key) {
                foreach (collect($st8Data['data']['games'])->where('category', '!=', 'Unknown')->where('developer', $key['name'])->groupBy('category')->map(function ($group, $categoryName) {
                    return $categoryName;
                })->toArray() as $pageMenu) {
                    $casinoPageMenuName[] = [
                        'page' => $key['name'],
                        'name' => $pageMenu
                    ];
                }
            }
            $casinoPageMenuName = collect($casinoPageMenuName)->groupBy('page')->toArray();
            $CasinoProvider = CasinoProviders::firstOrCreate(['name' => $gameProvider]);
            foreach ($gameData as $games) {
                $newGame = CasinoGames::on('read_db')->where('casino_provider_id', $CasinoProvider->id)->where('name', $games['game_name'])->first();
                if (!$newGame) {
                    $newGame = CasinoGames::create([
                        'name' => $games['game_name'],
                        'casino_provider_id' => $CasinoProvider->id,
                        'game_id' => $games['game_identificator']
                    ]);
                }
                foreach ($games['table'] as $tableData) {
                    $getTable = CasinoTables::on('read_db')->where('game_id', $newGame->game_id)->where('table_id', $tableData['id'])->first();
                    if (!$getTable) {
                        $getTable = CasinoTables::create([
                            'name' => $tableData['name'], 'game_id' => $newGame->game_id, 'table_id' => $tableData['id'], 'is_lobby' => true
                        ]);
                    }
                    foreach ($tenantId as $id) {
                        $casinoItem = CasinoItems::where('uuid', $getTable->table_id)->where('tenant_id', $id)->first();
                        if (!$casinoItem) {
                            $filteredSt8Images = collect($st8Data['data']['games'])->where('code', $getTable->table_id)->first();
                            CasinoItems::create([
                                'name' => $getTable->name,
                                'active' => true,
                                'uuid' => $getTable->table_id,
                                'provider' => $CasinoProvider->id,
                                'tenant_id' => $id,
                                'image' => $filteredSt8Images['image']
                            ]);
                        }
                    }
                }
            }

            foreach ($menuMasterData as $menu) {
                $menuMaster = MenuMaster::where('name', $menu['name'])->first();

                if (!$menuMaster) {
                    $menuMaster = MenuMaster::create([
                        'name' => $menu,
                        'path' => '/' . strtolower($menu['name']),
                        'active' => true,
                        'component' => 'Casino',
                        'component_name' => strtolower($menu['name']),
                        'created_at' => now(),
                        'updated_at' => now(),
                        'image' => $menu['image']
                    ]);
                }

                foreach ($tenantId as $id) {
                    $menuTenantSetting = MenuTenantSettingModel::where('tenant_id', $id)->where('menu_id', $menuMaster->id)->first();
                    if (!$menuTenantSetting) {
                        $menuTenantSetting = MenuTenantSettingModel::create([
                            'menu_id' => $menuMaster->id,
                            'tenant_id' => $id,
                            // 'ordering' => ,
                            'created_at' => now(),
                            'updated_at' => now()
                        ]);
                    }

                    if ($menu['name'] == 'Fishing') {
                        $casinoPages = CasinoPages::where('tenant_id', $id)->where('top_menu_id', $menuTenantSetting->id)->where('title', 'Pragmatic Play')->first();
                        if (!$casinoPages) {
                            $casinoPages = CasinoPages::create([
                                'title' => 'Pragmatic Play',
                                'created_at' => now(),
                                'image' => 'https://ezugi-main-platform-staging-storage.s3.amazonaws.com/pages%2F65%2Flogo%2Fceb921be-730d-4dcd-a234-402c74804985____pragmatic-play-logo-removebg-preview.png',
                                'updated_at' => now(),
                                'enabled' => true,
                                // 'order',
                                'tenant_id' => $id,
                                'top_menu_id' => $menuTenantSetting->id,
                                // 'enable_instant_game'
                            ]);
                        }

                        $casinoPageMenu = [];
                        $casinoPageMenuFishing = CasinoPagesMenus::where('page_id', $casinoPages->id)->where('name', 'Fishing Games')->first();
                            $casinoMenu = CasinoMenus::where('name', 'Fishing Games')->where('tenant_id', $id)->first();
                            if (!$casinoMenu) {
                                $casinoMenu = CasinoMenus::create([
                                    'name' => 'Fishing Games',
                                    // 'menu_type',
                                    // 'menu_order',
                                    'enabled' => true,
                                    'created_at' => now(),
                                    'updated_at' => now(),
                                    'tenant_id' => $id,
                                    'image_url' => 'https://ezugi-main-platform-staging-storage.s3.amazonaws.com/tenants%2F1%2Fmenus%2F9552e63e-7402-4f52-ab25-0e5ed923e509____Fishing_Games.png'
                                ]);
                            }
                            $casinoPageMenuFishingInner = CasinoPagesMenus::where('page_id', $casinoPages->id)
                                ->where('casino_menu_id', $casinoMenu->id)
                                ->where('name', 'Fishing Games')
                                ->first();
                            if (!$casinoPageMenuFishingInner) {
                                $casinoPageMenuFishingInner = CasinoPagesMenus::create([
                                    'name' => 'Fishing Games',
                                    'casino_menu_id' => $casinoMenu->id,
                                    'page_id' => $casinoPages->id,
                                    // 'menu_order'
                                ]);
                            }
                        
                        $casinoPageMenuWiseGames = [
                            ['code' => 'jil_royal_fishing', 'name' => 'Royal Fishing'],
                            ['code' => 'pgp_fishing_king', 'name' => 'Fishing King'],
                            ['code' => 'pgp_blue_ocean_3d', 'name' => 'Blue Ocean 3D'],
                            ['code' => 'pgp_fortune_fishing', 'name' => 'Fortune Fishing'],
                            ['code' => 'pgp_pirate_fishing', 'name' => 'Pirate Fishing'],
                        ];

                        foreach ($casinoPageMenuWiseGames as $casinoPageMenuWiseGame) {
                            $casinoItemGet = CasinoItems::where('uuid', $casinoPageMenuWiseGame['code'])->where('tenant_id', $id)->first();
                            $casinoItemExFishing = CasinoMenuItem::where('name', $casinoPageMenuWiseGame['name'])
                            // ->where('page_id', $casinoPages->id)
                            ->where('page_menu_id', $casinoPageMenuFishingInner->id)
                            ->where('casino_item_id', $casinoItemGet->id)
                            ->first();
                            if(!$casinoItemExFishing){
                                $casinoItem = CasinoMenuItem::create([
                                    'name' => $casinoPageMenuWiseGame['name'],
                                    // 'page_id' => $casinoPages->id,
                                    'page_menu_id' => $casinoPageMenuFishingInner->id,
                                    'casino_item_id' => $casinoItemGet->id,
                                    'active' => true,
                                    // 'featured' => $request->featured
                                ]);
                            }
                        }
                    } else {
                        foreach ($pagesData as $page) {

                            $casinoPages = CasinoPages::where('tenant_id', $id)->where('top_menu_id', $menuTenantSetting->id)->where('title', $page['name'])->first();
                            if (!$casinoPages) {
                                $casinoPages = CasinoPages::create([
                                    'title' => $page['name'],
                                    'created_at' => now(),
                                    'image' => $pageImages[$page['name']],
                                    'updated_at' => now(),
                                    'enabled' => true,
                                    // 'order',
                                    'tenant_id' => $id,
                                    'top_menu_id' => $menuTenantSetting->id,
                                    // 'enable_instant_game'
                                ]);
                            }

                            $casinoPageMenu = [];
                            foreach ($casinoPageMenuName[$page['name']] as $name) {
                                $casinoPageMenu[$name['name']] = CasinoPagesMenus::where('page_id', $casinoPages->id)->where('name', $name['name'])->first();
                                if (!$casinoPageMenu[$name['name']]) {
                                    $casinoMenu = CasinoMenus::where('name', $name['name'])->where('tenant_id', $id)->first();
                                    if (!$casinoMenu) {
                                        $casinoMenu = CasinoMenus::create([
                                            'name' => $name['name'],
                                            // 'menu_type',
                                            // 'menu_order',
                                            'enabled' => true,
                                            'created_at' => now(),
                                            'updated_at' => now(),
                                            'tenant_id' => $id,
                                            'image_url' => $casinoMenuImages[$name['name']]
                                        ]);
                                    }
                                    $casinoPageMenu[$name['name']] = CasinoPagesMenus::create([
                                        'name' => $name['name'],
                                        'casino_menu_id' => $casinoMenu->id,
                                        'page_id' => $casinoPages->id,
                                        // 'menu_order'
                                    ]);
                                }
                                $casinoPageMenuWiseGames = collect($st8Data['data']['games'])->where('developer', $name['page'])->where('category', $name['name'])->toArray();

                                foreach ($casinoPageMenuWiseGames as $casinoPageMenuWiseGame) {
                                    $casinoItemGet = CasinoItems::where('uuid', $casinoPageMenuWiseGame['code'])->where('tenant_id', $id)->first();
                                    $casinoItemEx = CasinoMenuItem::where('name', $casinoPageMenuWiseGame['name'])
                                        // ->where('page_id', $casinoPages->id)
                                        ->where('page_menu_id', $casinoPageMenu[$name['name']]->id)
                                        ->where('casino_item_id', $casinoItemGet->id)
                                        ->first();
                                    if (!$casinoItemEx) {
                                        $casinoItem = CasinoMenuItem::create([
                                            'name' => $casinoPageMenuWiseGame['name'],
                                            // 'page_id' => $casinoPages->id,
                                            'page_menu_id' => $casinoPageMenu[$name['name']]->id,
                                            'casino_item_id' => $casinoItemGet->id,
                                            'active' => true,
                                            // 'featured' => $request->featured
                                        ]);
                                    }

                                }
                            }
                        }
                    }
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            du([$e->getMessage(),$e->getLine(),$e->getFile()]);
        }
        return;
    }

    public static function seedImages() {
        try{
            setUnlimited("4G");
            $CasinoProvider = CasinoProviders::select('id')->where('name','st8' )->first();
             $tenantIds = [];
            if ($CasinoProvider) {
                $tenants = TenantThemeSettings::whereRaw("'$CasinoProvider->id' = ANY (string_to_array(assigned_providers,','))")->get();
                if (!empty($tenants) && count($tenants) > 0) {
                    foreach ($tenants as $tn) {
                        $tenantIds[] = $tn->tenant_id;
                    }
                }
            }
            $imageGenerationLink = 'https://luckmedia.link/game_code/thumb_3_2.webp';
            if(!empty($tenantIds)){
                foreach($tenantIds as $id){
                    $casinoItemImage = CasinoItems::select('image', 'uuid', 'id')->where('provider', $CasinoProvider->id)->where('image', null)->where('tenant_id', $id)->get();
                    if(!$casinoItemImage->isEmpty()){
                        foreach($casinoItemImage as $key){
                            $webpImage = str_replace('game_code', $key->uuid, $imageGenerationLink);
                            $image = @imagecreatefromwebp($webpImage);
                            if(!$image){
                               continue;
                            }
                            $directory = storage_path('app/public/st8Images/');
                            if (!file_exists($directory)) {
                                mkdir($directory, 0777, true);
                            }
                            $pngImagePath = $directory . $key->uuid . '.png';
                            $res = imagepng($image, $pngImagePath);
                            imagedestroy($image);
                            if ($res) {
                                $aws3 = new Aws3();
                                $PathS3Key = $aws3->uploadFile('st8/games/' . $key->uuid . '.png', $pngImagePath, 'png');
                                $PathS3Key;
                                $key->image  = $PathS3Key;
                                $key->save();
                            }
                            if (file_exists($pngImagePath)) {
                                unlink($pngImagePath);
                            }  
                        }
                    }
                }
            }
        }
        catch (\Exception $e) {
            du([$e->getMessage(),$e->getLine(),$e->getFile()]);
        }
    }
}
