<?php

namespace App\Services;

use App\Models\Sport;
use App\Models\Event;
use App\Models\League;
use App\Models\Transactions;
use App\Models\User;
use App\Models\TenantThemeSettings;
use App\Models\Casino\CasinoProviders;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use App\Services\SportsService;
use App\Models\Admin;
use App\Models\TenantCredentials;
use App\Models\Wallets;
use Illuminate\Support\Facades\App;

class SportEventService
{

    public static function updateEvent()
    {
        try {
            $tenantIds = [];
            DB::beginTransaction();
            $sportProvider = CasinoProviders::where('name','Jetfair')->first();
            if($sportProvider){
                $tenants = TenantThemeSettings::whereRaw("'$sportProvider->id' = ANY (string_to_array(assigned_providers,','))")->get();
                if(!empty($tenants) && count($tenants)>0){
                    foreach($tenants as $tn){
                        $tenantIds[] = $tn->tenant_id;
                    }
                }
            }
            foreach ($tenantIds as $tenantId){
                
                $accessKeyArray = TenantCredentials::select('key', 'value')
                    ->where('tenant_id', $tenantId)
                    ->where(function ($query) {
                        $query->where('key', "APP_JETFAIR_SECRETKEY")
                        ->orWhere('key', "APP_JETFAIR_EVENT_URL");
                    })
                    ->pluck('value', 'key')->toArray();

                $secretKey = $accessKeyArray['APP_JETFAIR_SECRETKEY'];
                $eventUrl = $accessKeyArray['APP_JETFAIR_EVENT_URL'];

                if (empty($secretKey)) {
                    return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
                }

                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => $eventUrl,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'GET',
                    CURLOPT_HTTPHEADER => array(
                        'secretKey: ' . $secretKey
                    ),
                ));

                $response = curl_exec($curl);
                curl_close($curl);
                $result = json_decode($response);
                if ($result->Status->code == 0) {
                    $getAgentEvents = (array)$result->GetAgentEvents;
                    foreach ($getAgentEvents as $agentEvents => $valueEvents) {
                        $fetchRecord = DB::table('pulls_sports')
                            ->select(['id'])
                            ->where('sport_id',$valueEvents->sportId)
                            ->where('tenant_id', $tenantId)
                            ->first();
                        $insertArray = [];
                        $insertArray['is_deleted'] = false;
                        $insertArray['sport_id'] = $valueEvents->sportId;
                        $insertArray['name_en'] = $valueEvents->sportName;
                        $insertArray['tenant_id'] = $tenantId;
                        if($fetchRecord){
                            Sport::where(['id' => $fetchRecord->id, 'tenant_id'=> $tenantId])->update($insertArray);
                            $sportId = $fetchRecord->id;
                        } else {
                            $createdSportValues = Sport::create($insertArray);
                            $sportId = $createdSportValues->id;
                        }
                        foreach ($valueEvents->tournament as $keyTournament => $tournament) {
                            $fetchLeagueRecord = DB::table('pulls_leagues')
                                ->select(['id'])
                                ->where('league_id',$tournament->tournamentId)
                                ->where('tenant_id', $tenantId)
                                ->first();
                            $insertLeagueArray = [];
                            $insertLeagueArray['is_deleted'] = false;
                            $insertLeagueArray['location_id'] = 61;
                            $insertLeagueArray['sport_id'] = $sportId;
                            $insertLeagueArray['tenant_id'] = $tenantId;
                            $insertLeagueArray['league_id'] = $tournament->tournamentId;
                            $insertLeagueArray['name_en'] = $tournament->tournamentName;

                            if($fetchLeagueRecord){
                                League::where(['id' => $fetchLeagueRecord->id, 'tenant_id' => $tenantId])->update($insertLeagueArray);
                                $leagueId = $fetchLeagueRecord->id;
                            } else {
                                $createdLeagueValues = League::create($insertLeagueArray);
                                $leagueId = $createdLeagueValues->id;
                            }

                            foreach ($tournament->match as $keyMatch => $match) {
                                $fetchMatchRecord = DB::table('pulls_events')
                                    ->select(['id'])
                                    ->where('fixture_id',$match->matchId)
                                    ->where('tenant_id', $tenantId)
                                    ->first();

                                $insertMatchArray = [];
                                $insertMatchArray['is_deleted'] = false;
                                $insertMatchArray['location_id'] = 61;
                                $insertMatchArray['sport_id'] = $sportId;
                                $insertMatchArray['tenant_id'] = $tenantId;
                                $insertMatchArray['sport_name_en'] = $valueEvents->sportName;
                                $insertMatchArray['league_id'] = $leagueId;
                                $insertMatchArray['league_name_en'] = $tournament->tournamentName;
                                $insertMatchArray['fixture_id'] = $match->matchId;
                                $insertMatchArray['fixture_status'] = 1;
                                $insertMatchArray['is_event_blacklisted'] = false;
                                $insertMatchArray['name_en'] = $match->matchName;
                                $insertMatchArray['start_date'] = $match->openDate;
                                $insertMatchArray['market'] = json_encode($match->market);
                                if($fetchMatchRecord){
                                    Event::where(['id' => $fetchMatchRecord->id,'tenant_id'=> $tenantId])
                                        ->update($insertMatchArray);
                                } else {
                                    Event::create($insertMatchArray);
                                }
                            }
                        }
                    }
                }
            }
            DB::commit();
        } catch (\Exception $e) { // something went wrong
            DB::rollback();
            echo $e->getFile();
            echo $e->getLine();
            print_r($e->getMessage());die;
        }

    }

}
