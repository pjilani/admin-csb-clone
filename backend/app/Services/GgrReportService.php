<?php

namespace App\Services;

use App\Models\Admin;
use App\Models\MonthWiseGgr;
use App\Models\Reports\PlayerReport;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\Currencies;
use Illuminate\Support\Facades\Auth;

class GgrReportService
{
    public static function index()
    {

        try {
            DB::beginTransaction();
            $tenantIds = DB::connection('read_db')->table('tenants')->where('active', true)->pluck('id');
            $previousMonth = Carbon::now()->subMonth()->format('F');
            $year = Carbon::now()->subMonth()->format('Y');
            $now = now();

            foreach ($tenantIds as $id) {
                // $agents = PlayerReport::getSuperAdminUserHierarchy('', $id);

                // $agentIds = empty($agents) ? [null] : array_column($agents, 'id');

                // foreach ($agentIds as $agentId) {
                $request = [
                    'tenant_id' => $id,
                    'time_type' => 'lastMonth',
                    // 'agent_id' => $agentId
                ];
                $data = self::reportData($request);

                if (!empty($data)) {
                    $insert = [];
                    foreach ($data as $value) {
                        $insertData = [
                            'total_bet' => $value['total_bet'],
                            'total_bet_eur' => $value['total_bet_eur'],
                            'total_win' => $value['total_win'],
                            'total_win_eur' => $value['total_win_eur'],
                            'total_ggr' => $value['total_ggr'],
                            'total_ggr_eur' => $value['total_ggr_eur'],
                            'currency' => $value['currency'],
                            'exchange_rate' => $value['exchange_rate'],
                            'tenant_id' => $id,
                            'month' => $previousMonth,
                            'year' => $year,
                            'created_at' => $now,
                            'updated_at' => $now
                        ];
                        // if ($agentId !== null) {
                        //     $insertData['agent_id'] = $agentId;
                        // }
                        $insert[] = $insertData;
                    }
                    MonthWiseGgr::insert($insert);
                }
                // }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e);
            Log::error('An error occurred: ' . $e->getMessage());
        }
        return;
    }
    public static function reportData($request, $download = false)
    {
        setUnlimited();
        $agentId = @$request['agent_id'] ?? '';
        $limit = array_key_exists('size', $request) ? $request['size'] : 10;
        $page = array_key_exists('page', $request) ? $request['page'] : 1;
        if ($page == 1) {
            $page = 0;
        }
        $currency = $request['currency'] ?? null;
        $authUser = Auth::User();
        $timeZone = $request['time_zone_name'] ?? 'UTC +00:00';
        if ($authUser && $authUser->parent_type == 'AdminUser' && $authUser->tenant_id) {
            $roles = getRolesDetails();
            $tenantId = @$request['tenant_id'] ?? $authUser->tenant_id;
            $agentId = @$request['agent_id'] ?? $authUser->id;

            if (in_array('sub-admin', $roles)) {
                $agentId = getParentAgent(Auth::user()->id);
            }
        } else {
            $tenantId = $request['tenant_id'] ?? null;
        }

        $time_period = $request['datetime'] ?? [];
        $time_type = $request['time_type'] ?? 'today';


        if ($time_type != 'custom') {
            if ($time_type == 'monthWise' && $request['month'] && $request['year']) {
                // dd($request['month'] , $request['year']);
                // $startDate = new \DateTime("first day of" . $request['month'] . $request['year']);
                // $endDate = new \DateTime("last day of" . $request['month'] . "23:59:59" . $request['year']);
                // $startDate = $startDate->format('Y-m-d H:i:s');
                // $endDate = $endDate->format('Y-m-d H:i:s');
                $query = MonthWiseGgr::on('read_db');
                if ($tenantId) {
                    $query->where('tenant_id', $tenantId);
                }
                if ($currency) {
                    $query->where('currency', $currency);
                }

                // if ($agentId) {
                //     $query->where('agent_id', $agentId);
                // }
                
                $ggrData = $query->select(
                    'total_bet',
                    'total_bet_eur',
                    'total_win',
                    'total_win_eur',
                    'total_ggr',
                    'total_ggr_eur',
                    'currency',
                    'exchange_rate',
                    'tenant_id',
                    DB::raw("CONCAT(month, ' - ', year) AS date")
                )->whereIn('month', $request['month'])->where('year', $request['year'])->with('tenant:id,name');
                if($download){
                    return $query->get();
                }
                $total = $ggrData->count();
                $paginatedData = $ggrData->forPage($page,$limit)->get();
                return [$paginatedData, $total];
            }
            $time_period = dateConversionWithTimeZone($time_type);
            $time_period['fromdate'] = convertToUTC(@$time_period['fromdate'], $timeZone)->format('Y-m-d H:i:s.v');
            $time_period['enddate'] = convertToUTC(@$time_period['enddate'], $timeZone)->format('Y-m-d H:i:s.v');

            $time_period['fromdate'] = getTimeZoneWiseTimeISO(@$time_period['fromdate']);
            $time_period['enddate'] = getTimeZoneWiseTimeISO(@$time_period['enddate']);
        } else {
            if (!$time_period) {
                $time_period = dateConversionWithTimeZone($time_type);
                $time_period['fromdate'] = convertToUTC($time_period['fromdate'], $timeZone)->format('Y-m-d H:i:s.v');
                $time_period['enddate'] = convertToUTC($time_period['enddate'], $timeZone)->format('Y-m-d H:i:s.v');
                $time_period['start_date'] = $time_period['fromdate'];
                $time_period['end_date'] = $time_period['enddate'];
            }
            $time_period['fromdate'] = getTimeZoneWiseTimeISO(@$time_period['start_date']);
            $time_period['enddate'] = getTimeZoneWiseTimeISO(@$time_period['end_date']);
            unset($time_period['start_date'], $time_period['end_date']);
        }
        $input = [
            "isDirectPlayer" => "all",
            'tenantId' => $tenantId,
            'time_period' => $time_period,
            'interval' => $time_type,
            'agent_id' => $agentId,
        ];
        $result = self::fetchResult($input);
        $obj = new self();
        $record['ggr'] = $obj->ggrSeries($result, $time_type);
        $record['stake'] = $obj->stakeSeries($result, $time_type);
        $record['win_amount'] = $obj->winAmount($result, $time_type);
        $data = [];
        $exchangeRates = Currencies::on('read_db')->pluck('exchange_rate','code');
        foreach ($record['ggr'] as $index => $val) {   
            if ($val['name'] == 'Total in Base Currency')
            continue;
            $ex_rate = $exchangeRates[$val['name']];
            $data[] = [
                'total_bet' => $record['stake'][$index]['total'],
                'total_bet_eur' => $record['stake'][$index]['eur_total'],
                'total_win' => @$record['win_amount']['data'][$index]['total'],
                'total_win_eur' => @$record['win_amount']['data'][$index]['eur_total'],
                'total_ggr' => $val['total'],
                'total_ggr_eur' => $val['eur_total'],
                'currency' => $val['name'],
                'exchange_rate' =>number_format((1/$ex_rate),5)
            ];
        }
        $data = collect($data);
        
        if ($currency) {
            $data = $data->filter(fn($item) => $item['currency'] == $currency)->values();
        }
        if($download){
            return $data;
        }
        $total = count($data);
        return [$data->forPage($page,$limit)->values(), $total];
    }

    private function ggrSeries($record, $time_period)
    {

        $series = [];
        $totalDepinEurdata = [];

        if (is_array(@$record['aggregations']['currencies']['buckets']))
            foreach ($record['aggregations']['currencies']['buckets'] as $key => $rec) {

                $series[$key]['name'] = $rec['key'];
                $series[$key]['type'] = 'line';
                $series[$key]['data'] = [];
                $series[$key]['chart_total'] = [];

                if (is_array(@$rec['chart_result']['dashboard_charts']['buckets']))
                    foreach ($rec['chart_result']['dashboard_charts']['buckets'] as $key2 => $recd) {

                        if (!empty($recd['ggr_for_chart_in_EUR']['value'])) {
                            $series[$key]['data'][$key2] = round($recd['ggr_for_chart_in_EUR']['value'], 2);
                        } else {
                            $series[$key]['data'][$key2] = 0;
                        }

                        if (!empty($recd['ggr_for_chart']['value'])) {
                            $series[$key]['chart_total'][$key2] = round($recd['ggr_for_chart']['value'], 2);
                        } else {
                            $series[$key]['chart_total'][$key2] = 0;
                        }
                    }

                $totalDepinEurdata[$key] = $series[$key]['data'];

                $series[$key]['total'] = round(array_sum($series[$key]['chart_total']), 2);
                $series[$key]['eur_total'] = round(array_sum($series[$key]['data']), 2);
            }

        $eurData = [];

        $c = @count($series);

        if (is_array(@$totalDepinEurdata[0]))
            foreach (@$totalDepinEurdata[0] as $ekey => $val) {
                $ecdata = [];

                for ($x = 0; $x <= $c - 1; $x++) {
                    $ecdata[$x] = $totalDepinEurdata[$x][$ekey];
                }

                $eurData[$ekey] = round(array_sum($ecdata), 2);
            }

        $series[$c]['name'] = 'Total in Base Currency';
        $series[$c]['type'] = 'line';
        $series[$c]['data'] = array_sum(@$eurData) > 0 ? @$eurData : $this->timePeriod($time_period); //@$eurData;
        $series[$c]['total'] = round(array_sum(@$eurData), 2);

        return $series;
    }

    private function stakeSeries($record, $time_period)
    {

        $series = [];
        $totalDepinEurdata = [];

        if (is_array(@$record['aggregations']['currencies']['buckets']))
            foreach ($record['aggregations']['currencies']['buckets'] as $key => $rec) {

                $series[$key]['name'] = $rec['key'];
                $series[$key]['type'] = 'line';
                $series[$key]['data'] = [];
                $series[$key]['chart_total'] = [];

                if (is_array(@$rec['chart_result']['dashboard_charts']['buckets']))
                    foreach ($rec['chart_result']['dashboard_charts']['buckets'] as $key2 => $recd) {

                        if (!empty($recd['bet_after_refund_for_chart_in_EUR']['value'])) {
                            $series[$key]['data'][$key2] = round($recd['bet_after_refund_for_chart_in_EUR']['value'], 2);
                        } else {
                            $series[$key]['data'][$key2] = 0;
                        }

                        if (!empty($recd['bet_after_refund_for_chart']['value'])) {
                            $series[$key]['chart_total'][$key2] = round($recd['bet_after_refund_for_chart']['value'], 2);
                        } else {
                            $series[$key]['chart_total'][$key2] = 0;
                        }
                    }

                $totalDepinEurdata[$key] = $series[$key]['data'];

                $series[$key]['total'] = round(array_sum($series[$key]['chart_total']), 2);
                $series[$key]['eur_total'] = round(array_sum($series[$key]['data']), 2);
            }

        $eurData = [];
        $c = @count($series);

        if (is_array(@$totalDepinEurdata[0]))
            foreach (@$totalDepinEurdata[0] as $ekey => $val) {
                $ecdata = [];

                for ($x = 0; $x <= $c - 1; $x++) {
                    $ecdata[$x] = $totalDepinEurdata[$x][$ekey];
                }

                $eurData[$ekey] = round(array_sum($ecdata), 2);
            }

        $series[$c]['name'] = 'Total in Base Currency';
        $series[$c]['type'] = 'line';
        $series[$c]['data'] = array_sum(@$eurData) > 0 ? @$eurData : $this->timePeriod($time_period); //@$eurData;
        $series[$c]['total'] = round(array_sum(@$eurData), 2);

        return $series;
    }

    private function winAmount($record, $time_period)
    {

        $series = [];
        $eurArr = [];

        if (is_array(@$record['aggregations']['currencies']['buckets']))
            foreach ($record['aggregations']['currencies']['buckets'] as $key => $rec) {

                $series[$key]['name'] = $rec['key'];
                $series[$key]['total'] = round($rec['win']['total_win_amount']['value'], 2);
                $series[$key]['eur_total'] = round($rec['win']['total_win_amount_in_EUR']['value'], 2);
                $eurArr[] = $series[$key]['eur_total'];
            }

        return ['data' => $series, 'eur_total' => round(array_sum($eurArr), 2)];
    }
    private function timePeriod($time_period)
    {
        if ($time_period == "today") {
            return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        } else if ($time_period == "weekly") {
            return [0, 0, 0, 0, 0, 0, 0];
        } else if ($time_period == "monthly") {
            $arr = [];
            for ($x = 0; $x <= date('t'); $x++) {
                $arr[$x] = 0;
            }
            return $arr;
        }
        if ($time_period == "yearly") {
            return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        } else {
            return [];
        }
    }



    public static function fetchResult($params)
    {
        $obj = new self();
        $body = [
            'query' => $obj->queryKey($params),
            'timeout' => '60s',
            '_source' => 'false',
            'track_total_hits' => true,
            'aggs' => [
                'currencies' => [
                    "terms" => [
                        "field" => "player_details.currency",
                        "size" => ALLCOUNT
                    ],
                    "aggs" => [
                        "chart_result" => $obj->getQueryChartResult($params),
                        "win" => $obj->getQueryWin($params),
                    ]
                ],
            ]
        ];

        $params = [
            'index' => ELASTICSEARCH_INDEX['transactions_dashboard'],
            'body' => $body
        ];
        $client = getEsClientObj();
        $return = $client->search($params);
        return $return;
    }
    private function getQueryWin($params)
    {
        $query["filter"] = [
            "bool" => [
                "filter" => [
                    [
                        "terms" => [
                            "transaction_type" => [
                                "win",
                                "win_non_cash"
                            ]
                        ]
                    ],
                    [
                        "term" => [
                            "player_details.demo" => "false"
                        ]
                    ]
                ]
            ]
        ];
        $query["aggs"] = [
            "total_win_amount" => [
                "sum" => [
                    "field" => "amount"
                ]
            ],
            "total_win_amount_in_EUR" => [
                "sum" => [
                    "field" => "amount_in_currencies.EUR"
                ]
            ]
        ];

        return $query;
    }

    private function getQueryChartResult($params)
    {
        $query["filter"] = [
            "terms" => [
                "transaction_type" => [
                    "bet",
                    "bet_non_cash",
                    "refund",
                    "refund_non_cash",
                    "win",
                    "win_non_cash",
                    "deposit",
                    "non_cash_granted_by_admin"
                ]
            ]
        ];
        $interval = 'hour';
        $intervalParam = $params['interval'] ?? "today";
        if ($intervalParam === 'weekly' || $intervalParam === 'custom') {
            $interval = 'day';
        } elseif ($intervalParam === 'lastMonth') {
            $interval = 'day';
        } elseif ($intervalParam === 'monthly') {
            $interval = 'day';
        } elseif ($intervalParam === 'yearly') {
            $interval = 'month';
        }

        $query["aggs"]["dashboard_charts"]["date_histogram"] = [
            "field" => "created_at",
            "interval" => $interval,
            "min_doc_count" => 0,
            "extended_bounds" => [
                "min" => $params['time_period']['fromdate'],
                "max" => $params['time_period']['enddate'],
            ]
        ];

        $query["aggs"]["dashboard_charts"]["aggs"] = [

            "deposit_for_chart" => [
                "filter" => [
                    "terms" => [
                        "transaction_type" => ["deposit", "non_cash_granted_by_admin"]
                    ]
                ],
                "aggs" => [
                    "total" => [
                        "sum" =>
                            ["field" => "amount"]

                    ],
                    "total_in_EUR" => [
                        "sum" => [
                            "field" => "amount_in_currencies.EUR"
                        ]
                    ]
                ]
            ],

            "bet_for_chart" => [
                "filter" => [
                    "bool" => [
                        "filter" => [
                            [
                                "terms" => [
                                    "transaction_type" => [
                                        "bet",
                                        "bet_non_cash"
                                    ]
                                ]
                            ],
                            [
                                "term" => [
                                    "player_details.demo" => "false"
                                ]
                            ]
                        ]
                    ]
                ],
                "aggs" => [
                    "total" => [
                        "sum" => [
                            "field" => "amount"
                        ]
                    ],
                    "total_in_EUR" => [
                        "sum" => [
                            "field" => "amount_in_currencies.EUR"
                        ]
                    ]
                ]
            ],

            "win_for_chart" => [
                "filter" => [
                    "bool" => [
                        "filter" => [
                            [
                                "terms" => [
                                    "transaction_type" => [
                                        "win",
                                        "win_non_cash"
                                    ]
                                ]
                            ],
                            [
                                "term" => [
                                    "player_details.demo" => "false"
                                ]
                            ]
                        ]
                    ]
                ],
                "aggs" => [
                    "total" => [
                        "sum" => [
                            "field" => "amount"
                        ]
                    ],
                    "total_in_EUR" => [
                        "sum" => [
                            "field" => "amount_in_currencies.EUR"
                        ]
                    ]
                ],
            ],

            "refund_for_chart" => [
                "filter" => [
                    "bool" => [
                        "filter" => [
                            [
                                "terms" => [
                                    "transaction_type" => [
                                        "refund",
                                        "refund_non_cash"
                                    ]
                                ]
                            ],
                            [
                                "term" => [
                                    "player_details.demo" => "false"
                                ]
                            ]
                        ]
                    ]
                ],
                "aggs" => [
                    "total" => [
                        "sum" => [
                            "field" => "amount"
                        ]
                    ],
                    "total_in_EUR" => [
                        "sum" => [
                            "field" => "amount_in_currencies.EUR"
                        ]
                    ]
                ]

            ],

            "bet_after_refund_for_chart" => [
                "bucket_script" => [
                    "buckets_path" => [
                        "bet" => "bet_for_chart>total",
                        "refund" => "refund_for_chart>total"
                    ],
                    "script" => "params.bet - params.refund"
                ]
            ],

            "bet_after_refund_for_chart_in_EUR" => [
                "bucket_script" => [
                    "buckets_path" => [
                        "bet" => "bet_for_chart>total_in_EUR",
                        "refund" => "refund_for_chart>total_in_EUR"
                    ],
                    "script" => "params.bet - params.refund"
                ]
            ],

            "ggr_for_chart" => [
                "bucket_script" => [
                    "buckets_path" => [
                        "bet_after_refund" => "bet_after_refund_for_chart",
                        "win" => "win_for_chart>total"
                    ],
                    "script" => "params.bet_after_refund - params.win"
                ]

            ],

            "ggr_for_chart_in_EUR" => [
                "bucket_script" => [
                    "buckets_path" => [
                        "bet_after_refund" => "bet_after_refund_for_chart_in_EUR",
                        "win" => "win_for_chart>total_in_EUR"
                    ],
                    "script" => "params.bet_after_refund - params.win"
                ]
            ],

        ];
        return $query;
    }

    private function queryKey($params)
    {
        $query["bool"] = [
            "must" => ["match_all" => new \stdClass],
        ];


        $query["bool"]['filter'][] = [
            "bool" => [
                "must_not" => [
                    "bool" => [
                        "must_not" => [
                            "exists" => [
                                "field" => "player_details"
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $query["bool"]['filter'][] = [
            "terms" => [
                "transaction_type" => [
                    "deposit",
                    "withdraw",
                    "withdraw_cancel",
                    "bet",
                    "bet_non_cash",
                    "win",
                    "win_non_cash",
                    "refund",
                    "refund_non_cash",
                    "non_cash_granted_by_admin",
                    "non_cash_withdraw_by_admin",
                    "dummy_for_admin"
                ]
            ]
        ];

        $query["bool"]['filter'][] = [
            "bool" => [
                "should" => [
                    [
                        "bool" => [
                            "filter" => [
                                [
                                    "range" => [
                                        "created_at" => [
                                            "from" => $params['time_period']['fromdate'],
                                            "include_lower" => true,
                                            "to" => $params['time_period']['enddate'],
                                            "include_upper" => true
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    [
                        "bool" => [
                            "filter" => [
                                [
                                    "term" => [
                                        "transaction_type" => [
                                            "value" => "dummy_for_admin"
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        if (@$params['tenantId']) {
            $query["bool"]['filter'][] = [
                "term" => [
                    "tenant_id" => [
                        "value" => $params['tenantId']
                    ]
                ]
            ];
        }
        if ($params['agent_id']) {
            $parentType = Admin::where('id', $params['agent_id'])->select('parent_type')->first();
            if ($parentType->parent_type == 'AdminUser' && $params['tenantId']) {
                $roles = getRolesDetails($params['agent_id']);

                if (!in_array('owner', $roles)) {

                    $query["bool"]['filter'][] = [
                        "term" => [
                            "player_details.parent_chain_ids" => [
                                "value" => $params['agent_id']
                            ]
                        ]
                    ];

                } else {
                    if (@$params['isDirectPlayer'] == 'all' && $params['agent_id']) {
                        $query["bool"]['filter'][] = [
                            "term" => [
                                "player_details.parent_chain_ids" => [
                                    "value" => $params['agent_id']
                                ]
                            ]
                        ];
                    }
                }



            } else {
                $roles = getRolesDetails(@$params['agent_id']);
                if (!in_array('owner', $roles)) {

                    $query["bool"]['filter'][] = [
                        "term" => [
                            "player_details.parent_chain_ids" => [
                                "value" => $params['agent_id']
                            ]
                        ]
                    ];

                } else {
                    if (@$params['isDirectPlayer'] == 'all' && $params['agent_id']) {
                        $query["bool"]['filter'][] = [
                            "term" => [
                                "player_details.parent_chain_ids" => [
                                    "value" => $params['agent_id']
                                ]
                            ]
                        ];
                    }
                }


            }
        }


        if (@$params['isDirectPlayer'] == 'direct' && $params['agent_id']) {
            $query["bool"]['filter'][] = [
                "term" => [
                    "player_details.parent_id" => [
                        "value" => $params['agent_id']
                    ]
                ]
            ];
        }
        return $query;
    }

}