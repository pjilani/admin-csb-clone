<?php

namespace App\Services;

use App\Models\BetTransaction;
use App\Models\Currencies;
use App\Models\TenantConfigurations;
use App\Models\Transactions;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class BetSportsReportService
{

    /**
     * @description getBetListByES
     * @param $limit
     * @param $page
     * @param $filter
     */
    public static function getBetReportByES($limit, $page, $filter){
        $obj = new self();

        $body = [
            'query' => $obj->queryKey($filter),
            'sort' => $obj->sortKey($filter['sortBy'], $filter['sortOrder']),
            'timeout' => '11s',
            'track_total_hits' => true,
            /*'aggs' => [
                'total_bet_count' => $obj->totalBetCountKey(),
                'currencies' => $obj->currencyKey(),
            ]*/
        ];

        $offset = ($page > 1) ? $page * $limit : 0;

        if ($page > 1) {
            $offset = $limit * ($page - 1);
        }

        if($limit!='all'){
            $body['size'] = $limit;
            $body['from'] = (int)$offset;
        }else{
            $body['size'] = ALLCOUNT;
//            $body['size'] = 100000;
        }

        $params = [
            'index' => ELASTICSEARCH_INDEX['bet_transaction'],
            'body' => $body
        ];
        $client = getEsClientObj();
        return $client->search($params);
    }

    /**
     * @description queryKey
     * @param $params
     * @return array
     */
    private function queryKey($params)
    {
        $mustArry=[];
        $filter=[];

        if (empty($request['tenant_id']) && Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
            $params['tenant_id'] = Auth::user()->tenant_id;
        }

        if(@$params['tenant_id']!='')
            $filter []= ['term' => ['tenant_id' => ['value' => $params['tenant_id']]]];

        if (@$params['action_type'] != '') {
            $filter [] = ['term' => ['payment_for' => $params['action_type']]];
        }
        if (@$params['bet_type'] != '') {
            $filter [] = ['term' => ['betslip_details.bettype' => $params['bet_type'] == 'not_combo' ? 1 : 2]];
        }
        if (@$params['user_id'] != '') {
            $filter [] = ['term' => ['user_id' => $params['user_id']]];
        }
        if(@$params['agent_id']) {
            $filter []=  [
                "term" => [
                    "player_details.parent_chain_ids" => [
                        "value" => $params['agent_id']
                    ]
                ]
            ];
        }

       $str = [];
        $query = [];
        if (@$params['sport_id'] != '') {
            $str []=" ( pe.sport_id = '" . $params['sport_id'] . "') ";
        }

        if (@$params['country'] != '') {
            $str []=" ( pe.location_id = '" . $params['country'] . "') ";
        }

        if (@$params['tournament_id'] != '') {
            $str []=" ( pe.league_id = '" . $params['tournament_id'] . "') ";

        }

        if (@$params['match_id'] != '') {
            $str []=" ( bb.event_id = '" . $params['match_id'] . "') ";
        }

        if (@$params['search'] != '') {
            $str []=" ( bb.bet_id LIKE '" . $params['search'] . "') ";
        }


        if(count($str)) {
            $query = DB::table('bets_bets as bb')
                ->select(['bb.betslip_id'])
                ->leftJoin('bets_betslip as bs', 'bb.betslip_id', '=', 'bs.id')
                ->leftJoin('pulls_events as pe', 'bb.event_id', '=', 'pe.id')
                ->whereRaw(implode(' and ', $str))
                ->pluck('bb.betslip_id')->toArray();
        }

        if (is_array($query) && count($query)) {
            $filter [] = ['terms' => ['betslip_id' => $query]];
        }


        if (@$params['status'] != '') {
             $filter [] = ['term' => ['betslip_details.settlement_status' => $params['status']]];
        }
        if (@$params['stake_min_amt'] != '' || @$params['stake_max_amt'] != '') {


            $r=[];
            if($params['stake_min_amt']){
                $r['from']=(int)$params['stake_min_amt'];
            }
            if($params['stake_max_amt']){
                $r['to']=(int)$params['stake_max_amt'];
            }

            if(count($r)) {
                $filter [] = [
                    "range" => [
                        "betslip_details.stake" => $r
                    ]
                ];
            }
        }
        if (@$params['winning_min_amt'] != '') {
            $filter [] = [
                "range" => [
                    "betslip_details.possible_win_amount" => [
                        "gte" => $params['winning_min_amt']
                    ]
                ]
            ];

        }
        if (@$params['winning_max_amt'] != '') {
            $filter [] = [
                "range" => [
                    "betslip_details.possible_win_amount" => [
                        "lte" => $params['winning_max_amt']
                    ]
                ]
            ];
        }

        if(@$params['start_date'] != '' && @$params['end_date'] != '') {
            $filter[] = [
                'range' => [
                    'created_at' => [
                        'from' => getTimeZoneWiseTimeISO(@$params['start_date']) ,
                        'include_lower'=> true,
                        'to' => str_replace("000000", "999999", getTimeZoneWiseTimeISO(@$params['end_date'])),
                        'include_upper' => true
                    ]
                ]
            ];
        }


        if (@$params['owner_id']) {
            $filter []=  [
                "term" => [
                    "parent_id" => [
                        "value" => $params['owner_id']
                    ]
                ]
            ];
        }


        if(@$params['currency_id']!='') {
            $mustArry['match'] = ["player_details.currency_id" => $params['currency_id']];
        }


        $query = [
            'bool' => [
                'must' => count($mustArry)?$mustArry:['match_all' => new \stdClass]
            ]
        ];

        if(count( $filter)){
            $query['bool']['filter']=$filter;
        }

        return $query;
    }

    /**
     * @description sortKey
     * @param $sortBy
     * @param string $sortOrder
     * @return array
     */
    private function sortKey($sortBy, $sortOrder = 'ASC')
    {
        return [$sortBy => $sortOrder];
    }

    /**
     * @description searchWordQuery
     * @param $str
     * @return array
     */
    private function searchWordQuery($str)
    {

        $result = array(
            "bool" => array(
                "should" => array(
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "internal_tracking_id" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "internal_tracking_id" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                   /* array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "player_details" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "player_details.email.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),*/
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "amount" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "amount" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            )
        );


        return $result;
    }

}
