<?php

namespace App\Services;

use App\Models\Admin;
use Elasticsearch\ClientBuilder;
use Illuminate\Support\Facades\DB;

class TipReportService
{


    public static function fetchResult($params)
    {
        $obj = new self();

        $body = [
            'query' => $obj->queryKey($params),
            'sort' => $obj->sortKey($params['sortBy'], $params['sortOrder']),
            'timeout' => '11s',
            'track_total_hits' => true,
            "_source" => false,
            'aggs' => [
                'reports' => $obj->reportsKey($params),
                'total_tip_count' => $obj->totalTipCountKey(),
                'total_tip_amount' => $obj->totalTipAmountKey($params)

            ]
        ];

        if ($params['limit'] != 'all' and $params['offset'] != 'all') {
            $body['size'] = $params['limit'];
            $body['from'] = $params['offset'] ?? 0;
        } else {
            $body['size'] = ALLCOUNT;
        }


        $params = [
            'index' => ELASTICSEARCH_INDEX['transactions'],
            'body' => $body
        ];
      
        $client = getEsClientObj();
        $return = $client->search($params);
        return $return;
    }

    /**
     * @description queryKey
     * @param $params
     * @return array
     */
    private function queryKey($params)
    {

        $mustArry = [];
        $filter = [];

        if ($params['tenant_id'] != '')
            $filter [] = ['term' => ['tenant_id' => ['value' => $params['tenant_id']]]];


        if (@$params['owner_id'] && ($params['agentId']==''|| $params['agentId']==0)) {
            $filter [] = ['term' => ['player_details.parent_chain_ids' => ['value' => $params['owner_id']]]];
        }elseif ($params['isDirectPlayer'] == 'all' && @$params['parent_id']) {
            $filter [] = ['term' => ['player_details.parent_chain_ids' => ['value' => $params['parent_id']]]];

        } elseif ($params['isDirectPlayer'] == 'direct' && @$params['parent_id']) {
            $filter [] = ['term' => ['player_details.parent_id' => ['value' => $params['parent_id']]]];
        }



        $filter [] = ["bool" =>
            ["must_not" => [
                "bool" => [
                    "must_not" => [
                        "exists" => [
                            "field" => "player_details"
                        ]
                    ]
                ]
            ]
            ]
        ];

        $filter [] = [
            "terms" => [
                "transaction_type" => [
                    "tip",
                    "tip_non_cash"
                ]
            ]
        ];


        if (is_array($params['time_period']) &&
            $params['time_period']['fromdate'] != '' &&
            $params['time_period']['enddate'] != ''
        ) {

            $filter [] = [
                "range" => [
                    "created_at" => [
                        "from" => $params['time_period']['fromdate'],
                        "include_lower" => true,
                        "to" => str_replace("000000", "999999", $params['time_period']['enddate']),
                        "include_upper" => true
                    ]
                ]
            ];
        }


        if (@$params['currency'] != '') {
            $filter [] = ['term' => ["player_details.currency" => ['value' => $params['currency']]]];
        }


        $query = [
            'bool' => [
                'must' => count($mustArry) ? $mustArry : ['match_all' => new \stdClass]
            ]
        ];

        if (count($filter)) {
            $query['bool']['filter'] = $filter;
        }
        return $query;
    }

    private function sortKey($sortBy, $sortOrder = 'ASC')
    {
        return [$sortBy => $sortOrder];
    }

    private function reportsKey($params)
    {
        $ids = [];
        if($params['isDirectPlayer']=='direct' && $params['parent_id'] != ''){
            $idTopParent = Admin::getAdminDerectUser($params['parent_id']);
            foreach ($idTopParent as $value){
                $ids[]=$value->id;
            }

        }else {
            if($params['owner_id']){
                $Hierarchy = getAdminHierarchy($params['owner_id']);
            }else {
                $Hierarchy = getAdminHierarchy($params['parent_id']);

            }
            $ids = flatten($Hierarchy);
            $ids = array_unique($ids);
        }

        $tip_reports = [
            "terms" => array(
                "field" => "player_details.parent_chain_detailed",
                "script" => $this->scriptReport(),
                "size" => 10000
            ),
            "aggs" => [
                "player_name" => [ "terms" => [ "field" => "player_details.player_name" ]],
                "tip_amount" => $this->tipAmountKey(),
                "tip_amount_in_EUR" => $this->tipAmountEur(),
                "final_sort" => $this->finalSort()
            ]

        ];

        if($ids){
            $tip_reports['terms']['include'] = ".*-".implode('-|.*-',$ids)."-";
        }

        return $tip_reports;
    }

    private function scriptReport()
    {
        return 'String[] fields = _value.splitOnToken(\'-\');
              fields[1].toLowerCase() + \'-\' + fields[2].toLowerCase() + \'-\' +
              fields[1] + \'-\' + fields[2] + \'-\' + fields[0] + \'-\'';

    }

    private function tipAmountKey()
    {
        return ["sum" => ["field" => "player_details.deducted_amount"]];

    }

    private function tipAmountEur()
    {
        return ["sum" => ["field" => "amount_in_currencies.EUR"]];

    }

    private function finalSort()
    {
        return ["bucket_sort" => ["sort" => [["_key" => ["order" => "asc"]]]]];

    }

    private function totalTipCountKey()
    {
        $total_tip_count = [
            "filter" => [
                "terms" => [
                    "transaction_type" => [
                        "tip",
                        "tip_non_cash"
                    ]
                ]
            ]

        ];
        return $total_tip_count;

    }

    public function totalTipAmountKey($params)
    {
        $total_tip_amount = ["sum" => [
            "field" => "amount_in_currencies.".$params['tenant_base_currency']
        ]];

        return $total_tip_amount;
    }
}

?>
