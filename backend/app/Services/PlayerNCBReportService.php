<?php

namespace App\Services;

use Carbon\Carbon;
use Elasticsearch\ClientBuilder;


class PlayerNCBReportService
{

    /**
     * @description fetchResult
     * @param $params
     * @return array
     */
    public static function fetchResult($params)
    {
        $obj = new self();

        $body = [
            'query' => $obj->queryKey($params),
            'sort' => $obj->sortKey($params['sortBy'], $params['sortOrder']),
            'timeout' => '60s',
            'track_total_hits' => true,
            'aggs' => [
                'total_bet_non_cash' => $obj->totalBetNonCash($params),
                'total_refund_non_cash' => $obj->totalRefundNonCash($params),
                'total_win_non_cash' => $obj->totalWinNonCash($params),
                'total_tip_non_cash' => $obj->totalTipNonCash($params),
                'total_non_cash_bonus_claim' => $obj->totalNonCashBonusClaim($params),
                'total_non_cash_granted_by_admin' => $obj->totalNonCashGrantedByAdmin($params),
                'total_non_cash_withdraw_by_admin' => $obj->totalNonCashWithdrawByAdmin($params),
                'total_non_cash_bonus_player_commission_claim' => $obj->totalNonCashPlayerCommissionBonusClaim($params),
            ]
        ];

        if ($params['limit'] != 'all') {
            $body['size'] = (int)$params['limit'];
            $body['from'] = (int)$params['offset'];
        } else {
            $body['size'] = ALLCOUNT;
        }

        $params = [
            'index' => ELASTICSEARCH_INDEX['transactions'],
            'body' => $body
        ];
        $client = getEsClientObj();
        $return = $client->search($params);
        return $return;
    }

    /**
     * @description queryKey
     * @param $params
     * @return array
     */
    private function queryKey($params)
    {

        $mustArry = [];
        $filter = [];

        if (@$params['tenant_id'] != '')
            $filter [] = ['term' => ['tenant_id' => ['value' => $params['tenant_id']]]];

        $filter [] = ["bool" =>
            ["must_not" => [
                "bool" => [
                    "must_not" => [
                        "exists" => [
                            "field" => "player_details"
                        ]
                    ]
                ]
            ]
            ]
        ];

        if ($params['isDirectPlayer']=='all' && @$params['agentId'])
            $filter [] = ['term' => ['player_details.parent_chain_ids' => ['value' => $params['agentId']]]];
        elseif ($params['isDirectPlayer']=='direct' && @$params['agentId'])
            $filter [] = ['term' => ['player_details.parent_id' => ['value' => $params['agentId']]]];




        if (is_array($params['time_period']) &&
            $params['time_period']['fromdate'] != '' &&
            $params['time_period']['enddate'] != ''
        ) {

            $filter [] = [
                "range" => [
                    "created_at" => [
                        "from" =>$params['time_period']['fromdate'],
                        "include_lower"=> true,
                        "to" => str_replace("000000", "999999", $params['time_period']['enddate']),
                        "include_upper"=> true
                    ]
                ]
            ];
        }

        if (@$params['action_type']) {
            $filter [] = ['term' => ['transaction_type' => ['value' => $params['action_type']]]];
        } else {
            $filter [] = ['terms' =>
                [
                    'transaction_type' => [
                        "non_cash_bonus_claim",
                        "non_cash_granted_by_admin",
                        "non_cash_withdraw_by_admin",
                        "bet_non_cash",
                        "win_non_cash",
                        "tip_non_cash",
                        "refund_non_cash",
                        "commission_received"
                    ]
                ]
            ];
        }


        if (@$params['currency'] != '') {
            $mustArry["multi_match"] = [
                "query" => $params['currency'],
                "fields" => [
                    "source_currency",
                    "target_currency"
                ],
                "type" => "best_fields",
                "operator" => "OR"
            ];

        }


        if ($params['searchKeyword'] != '') {

            $filter [] =  $this->searchWordQuery($params['searchKeyword']);
        }

        $query = [
            'bool' => [
                'must' => count($mustArry) ? $mustArry : ['match_all' => new \stdClass]
            ]
        ];

        if (count($filter)) {
            $query['bool']['filter'] = $filter;
        }
        return $query;
    }

    private function searchWordQuery($str)
    {

        $result = array(
            "bool" => array(
                "should" => array(
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "player_details.player_id_s.word_start" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "player_details.player_id_s.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "player_details.player_name.word_start" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "player_details.player_name.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "transaction_id.word_start" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "transaction_id.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "player_details.phone.word_start" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "player_details.phone.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            )
        );


        return $result;
    }

    private function sortKey($sortBy, $sortOrder = 'ASC')
    {
        return [$sortBy => $sortOrder];
    }

    private function totalBetNonCash($params)
    {

        $total_bet_non_cash = ["filter" => ["term" => ["transaction_type" => "bet_non_cash"]],
            "aggs" => ["total" => $this->totalAmountKey($params)]
        ];

        return $total_bet_non_cash;

    }

    private function totalAmountKey($params)
    {
        return ["sum" => ["field" => "amount_in_currencies.".$params['tenant_base_currency']]];
    }

    private function totalRefundNonCash($params)
    {
        $total_refund_non_cash = ["filter" => ["term" => ["transaction_type" => "refund_non_cash"]],
            "aggs" => ["total" => $this->totalAmountKey($params)]

        ];

        return $total_refund_non_cash;
    }

    private function totalWinNonCash($params)
    {
        $total_win_non_cash = ["filter" => ["term" => ["transaction_type" => "win_non_cash"]],
            "aggs" => ["total" => $this->totalAmountKey($params)]
        ];


        return $total_win_non_cash;
    }

    private function totalTipNonCash($params)
    {
        $total_tip_non_cash = ["filter" => ["term" => ["transaction_type" => "tip_non_cash"]],
            "aggs" => ["total" => $this->totalAmountKey($params)]
        ];
        return $total_tip_non_cash;
    }

    private function totalNonCashBonusClaim($params)
    {

        $total_non_cash_bonus_claim = ["filter" => ["term" => ["transaction_type" => "non_cash_bonus_claim"]],
            "aggs" => ["total" => $this->totalAmountKey($params)]
        ];
        return $total_non_cash_bonus_claim;
    }
    private function totalNonCashPlayerCommissionBonusClaim($params)
    {

        $total_non_cash_bonus_claim = ["filter" => ["term" => ["transaction_type" => "commission_received"]],
            "aggs" => ["total" => $this->totalAmountKey($params)]
        ];
        return $total_non_cash_bonus_claim;
    }

    private function totalNonCashGrantedByAdmin($params)
    {

        $total_non_cash_granted_by_admin = ["filter" => ["term" => ["transaction_type" => "non_cash_granted_by_admin"]],
            "aggs" => ["total" => $this->totalAmountKey($params)]
        ];

        return $total_non_cash_granted_by_admin;

    }

    private function totalNonCashWithdrawByAdmin($params)
    {
        $total_non_cash_withdraw_by_admin = ["filter" => ["term" => ["transaction_type" => "non_cash_withdraw_by_admin"]],
            "aggs" => ["total" => $this->totalAmountKey($params)]
        ];

        return $total_non_cash_withdraw_by_admin;
    }


}