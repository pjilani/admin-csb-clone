<?php

namespace App\Services;

use App\Models\ConversionHistory;
use App\Models\Currencies;
use App\Models\CurrencyResponse;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ExchangeRateService
{

    public static function index()
    {
        DB::beginTransaction();
    
        try {
            $base_url = env('BASE_URL');
            $base_currency = env('BASE_CURRENCY');
            $guzzle_client = new Client(['base_uri' => $base_url]);
            $app_id = env('APP_ID');
            $response = $guzzle_client->request('GET', '/latest.json?app_id=' . $app_id . '&base='. $base_currency . '&prettyprint=false&show_alternative=false', [
                'headers' => [
                    'Content-Type' => 'application/json',
                ]
            ])->getBody()->getContents();
            $response = json_decode($response);
            $getcurrencyResponse = CurrencyResponse::orderBy('id', 'desc')->first();

            // insert in currency response
            $currencyResponse = CurrencyResponse::create(['response' => json_encode($response)]);

            // update exchange rate
            $findCurrency = Currencies::select('exchange_rate', 'internal_code', 'code', 'id', 'currency_response_id')->get();
            foreach ($findCurrency as $currency) {
                
                if (!array_key_exists($currency->internal_code, $response->rates)) {
                    continue;
                }
                $currency->exchange_rate = round(@$response->rates->{@$currency->internal_code}, 5);
                if($currency->currency_response_id){
                    $currency->currency_response_id = @$getcurrencyResponse->id;
                }
                if(!$currency->currency_response_id){
                    $currency->currency_response_id = @$currencyResponse->id;
                }
                $currency->save();

                // insert in conversion history
                ConversionHistory::create([
                    'currency_id' => $currency->id,
                    'old_exchange_rate' => $currency->exchange_rate,
                    'new_exchange_rate' => round($response->rates->{$currency->internal_code}, 5),
                    'old_currency_response_id' => $currency->currency_response_id,
                    'new_currency_response_id' => $currencyResponse->id,
                ]);
            }
    
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('An error occurred: ' . $e->getMessage());
        }
    
        return;
    }
}
