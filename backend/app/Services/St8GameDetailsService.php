<?php

namespace App\Services;

use App\Models\TenantCredentials;
use App\Models\Tenants;
use GuzzleHttp\Client;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\App;

class St8GameDetailsService
{
    public static function st8RoundInfo($request)
    {

        try {
           
            $authUser = Auth::User();

            $tenant = Tenants::on('read_db')->select('domain')->where('id', @$request['tenant_id'] ?? $authUser->tenant_id)->first();

            $url = $tenant->domain;
            //remove www.
            $substringUrl = substr($url, 4);
            if($substringUrl == 'hal567.com'){
                $modifiedDomain = "https://st8-api." . $substringUrl;
            }else{
                $modifiedDomain = "https://st8." . $substringUrl;
            }
            $st8_microservice_url = $modifiedDomain . "/api/v1/";

            // $paramString = 'round=88517f47-a23c-5333-a005-e284118fba71&transaction_id=461310ae-3509-5c29-baf9-ce5b7bb6ea56';
            $paramString = 'round=' . $request['RoundID'] . '&transaction_id=' . $request['TransactionID'];

            // $st8Params = [
            //     'round' => '88517f47-a23c-5333-a005-e284118fba71',
            //     'transaction_id' => '461310ae-3509-5c29-baf9-ce5b7bb6ea56'
            // ];

            $st8Params = [
                'round' => $request['RoundID'],
                'transactionId' => $request['TransactionID']
            ];

            $guzzle_client = new Client(['base_uri' => $st8_microservice_url]);
            $response = $guzzle_client->request('POST', 'st8/getRoundInfo', [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
                'json' => $st8Params
            ])->getBody()->getContents();

            $response = json_decode($response);
            if($response->status == 'ok'){
                return $response;
            }
            else{
                return;
            }
        } catch (\Exception $e) {
            if (!App::environment(['local'])) { //, 'staging'
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage()], Response::HTTP_EXPECTATION_FAILED);
            } else {
                return returnResponse(false, ERROR_MESSAGE, ['Error' => $e->getMessage()], Response::HTTP_EXPECTATION_FAILED);
            }
        }

        return;
    }
}
