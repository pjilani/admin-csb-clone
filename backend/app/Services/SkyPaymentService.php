<?php

namespace App\Services;

use App\Models\QueueLog;
use App\Models\RequestResponseLogs;
use App\Models\Transactions;
use App\Models\UserBonus;
use App\Models\Wallets;
use App\Models\WithdrawalTransactions;
use App\Models\WithdrawRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Response;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;

class SkyPaymentService
{
    public static function index()
    {
        try {
            DB::beginTransaction();
            $withdrawalRequest = WithdrawRequest::on('read_db')
                ->where('payment_provider_name', 'skyPayment')->where('status', 'pending_by_gateway')
                ->where('verified_status', 'verified')->where('withdrawal_type', 'payment_gateway')->get();

            $skyWithdrawProviders = DB::connection('read_db')->table('payment_providers')
                ->join('tenant_payment_configurations', 'payment_providers.id', '=', 'tenant_payment_configurations.provider_id')
                ->where('payment_providers.provider_name', 'Sky Withdraw Provider')
                ->select('payment_providers.id', 'tenant_payment_configurations.provider_key_values')
                ->first();

            $paymentCredentials = json_decode($skyWithdrawProviders->provider_key_values);

            foreach ($withdrawalRequest as $request) {
                $tenant_id = $request->tenant_id;

                $guzzle_client = new Client(['base_uri' => $paymentCredentials->baseUrl]);
                $apiResponse = $guzzle_client->request('GET', 'getStatus?api_token=' . $paymentCredentials->payoutApiKey . '&client_id=' . $request->payment_transaction_id, [
                    'headers' => [
                        'Accept' => 'application/json',
                    ]
                ])->getBody()->getContents();
                $apiResponse = json_decode($apiResponse);

                $transaction = Transactions::on('read_db')->find($request->transaction_id);
                if (empty($transaction)) {
                    RequestResponseLogs::create([
                        'request_json' => json_encode($apiResponse),
                        'response_json' => json_encode(['Error' => TRANSACTION_NOT_FOUND]),
                        'service' => SKY_SERVICE,
                        // 'url' => $request->url(),
                        'tenant_id' => $tenant_id,
                        'response_code' => Response::HTTP_NOT_FOUND,
                        'response_status' => TRANSACTION_FAILED,
                        'error_code' => Response::HTTP_NOT_FOUND
                    ]);
                    continue;
                }

                WithdrawRequest::find($request->id)->update(
                    [
                        'withdrawal_type' => WITHDRAW_TYPE,
                        'status' => ($apiResponse->STATUS == SUCCESS ? APPROVED : ($apiResponse->STATUS == FAILED ? REJECTED_BY_GATEWAY : PENDING_BY_GATEWAY)),
                        'actioned_at' => date('Y-m-d H:i:s')
                    ]
                );

                $status = APPROVED_BY_PAYMENT_GATEWAY;

                if ($apiResponse->STATUS == 'SUCCESS') {

                    $userBonuses = UserBonus::on('read_db')->where(['user_id' => $request->user_id, 'status' => ACTIVE])
                        ->get();
                    if (count($userBonuses) > 0) {
                        foreach ($userBonuses as $key => $userBonus) {
                            $userBonusesObj = UserBonus::where(['id' => $userBonus['id']]);
                            $userBonusesObj->update(['status' => CANCELLED, 'updated_at' => date('Y-m-d H:i:s')]);
                        }
                    }

                    if ($transaction) {
                        $transaction->status = SUCCESS;
                        $transaction->comments = APPROVED_BY_PAYMENT_GATEWAY;
                        $transaction->update();
                    }


                    try {
                        $tenantWallet = Wallets::where('owner_id', $transaction->actionee_id)->where('currency_id', $transaction->source_currency_id)->lockForUpdate()->first();
                        $newTenantWithdrawalAmount = (float)$tenantWallet->withdrawal_amount + (float)$transaction->amount;
                        $tenantWallet->refresh();
                        $tenantWallet->withdrawal_amount = $newTenantWithdrawalAmount;
                        $tenantWallet->save();

                        // Create Withdrawal Transaction
                        $withdrawal_transaction = $transaction;
                        if ($withdrawal_transaction) {
                            unset($withdrawal_transaction->id);
                            $withdrawal_transaction->target_wallet_id = $tenantWallet->id;
                            $withdrawal_transaction->target_currency_id = $tenantWallet->currency_id;
                            $withdrawal_transaction->target_before_balance = $tenantWallet->withdrawal_amount;
                            $withdrawal_transaction->target_after_balance = $newTenantWithdrawalAmount;
                            $withdrawal_transaction->updated_at = date('Y-m-d H:i:s');
                            $withdrawal_transaction->transaction_id = getUUIDTransactionId();
                            WithdrawalTransactions::create($withdrawal_transaction->toArray());
                        }
                    } catch (\Throwable $th) {
                        RequestResponseLogs::create([
                            'request_json' => json_encode($apiResponse),
                            'response_json' => json_encode([
                                'Error' => $th->getMessage(),
                                'FileName' => $th->getFile(),
                                'LineNo' => $th->getLine(),
                            ]),
                            'service' => SKY_SERVICE,
                            // 'url' => $request->url(),
                            'tenant_id' => $tenant_id,
                            'response_code' => Response::HTTP_EXPECTATION_FAILED,
                            'response_status' => TRANSACTION_FAILED,
                            'error_code' => Response::HTTP_EXPECTATION_FAILED
                        ]);
                    }
                } else {

                    if ($transaction) {
                        $status = REJECTED_BY_PAYMENT_GATEWAY;
                        $transaction->status = CANCELLED;
                        $transaction->comments = REJECTION_COMMENT;
                        $transaction->update();

                        $data = DB::table('wallets')
                            ->where('id', $transaction->source_wallet_id)
                            ->lockForUpdate()
                            ->first();
                        $data->refresh();
                        $updatedAmount = $request->amount + $data->amount;
                        $data->amount  = $updatedAmount;
                        $data->save();
                        //Entry in queueLog
                        $queueLog = QueueLog::create([
                            'type' => USER_TRANSACTION,
                            'ids' => json_encode([$request->user_id])
                        ]);
                        try {
                            $redis = Redis::connection();
                            $jsonArray = [];
                            $jsonArray['QueueLog'] = [
                                'queueLogId' => $queueLog->id
                            ];
                            $redis->publish(
                                'QUEUE_WORKER',
                                json_encode($jsonArray)
                            );
                        } catch (\Throwable $th) {
                            //throw $th;
                        }
                    }
                    RequestResponseLogs::create([
                        'request_json' => json_encode($apiResponse),
                        'response_json' => json_encode(['message' => WITHDRAW_FAILED, 'status' => Response::HTTP_EXPECTATION_FAILED]),
                        'service' => SKY_SERVICE,
                        //   'url' => $request->url(),
                        'tenant_id' => $tenant_id,
                        'response_code' => Response::HTTP_EXPECTATION_FAILED,
                        'response_status' => TRANSACTION_FAILED,
                        'error_code' => Response::HTTP_EXPECTATION_FAILED
                    ]);
                    continue;
                }
                if ($request->transaction_id) {
                    //Entry in queueLog
                    $queueLog = QueueLog::create([
                        'type' => CASINO_TRANSACTION,
                        'ids' => json_encode([$transaction->id])
                    ]);
                    try {
                        $redis = Redis::connection();
                        $jsonArray = [];
                        $jsonArray['QueueLog'] = [
                            'queueLogId' => $queueLog->id
                        ];
                        $redis->publish(
                            'QUEUE_WORKER',
                            json_encode($jsonArray)
                        );
                    } catch (\Throwable $th) {
                        //throw $th;
                    }

                    $receiverId = $$request->user_id;
                    $receiverType = USER_TYPE;
                    $message = "Your withdrawal request of " . $request->amount . " has been " . $status;
                    $id = NotificationService::addNotification($receiverId, $receiverType, $message);

                    try {
                        $redis = Redis::connection();
                        $jsonArray = [];
                        $jsonArray['PlayerNotification'] = [
                            'message' => $message,
                            'userId' => $$request->user_id,
                            'senderType' => ADMIN_TYPE,
                            'referenceId' => $receiverId,
                            'referenceType' => USER_TYPE,
                            'id' => @$id->id

                        ];
                        $redis->publish(
                            'PLAYER_NOTIFICATION_CHANNEL',
                            json_encode($jsonArray)
                        );
                    } catch (\Throwable $th) {
                        RequestResponseLogs::create([
                            'request_json' => json_encode($apiResponse),
                            'response_json' => json_encode([
                                'Error' => $th->getMessage(),
                                'FileName' => $th->getFile(),
                                'LineNo' => $th->getLine(),
                            ]),
                            'service' => SKY_SERVICE,
                            // 'url' => $request->url(),
                            'tenant_id' => $tenant_id,
                            'response_code' => Response::HTTP_EXPECTATION_FAILED,
                            'response_status' => TRANSACTION_FAILED,
                            'error_code' => Response::HTTP_EXPECTATION_FAILED
                        ]);
                    }
                }

                RequestResponseLogs::create([
                    'request_json' => json_encode($apiResponse),
                    'response_json' => json_encode(['message' => WITHDRAW_SUCCESS, 'status' => Response::HTTP_OK]),
                    'service' => SKY_SERVICE,
                    // 'url' => $request->url(),
                    'tenant_id' => $tenant_id,
                    'response_code' => Response::HTTP_OK,
                    'response_status' => 'success',
                    'error_code' => Response::HTTP_OK
                ]);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            RequestResponseLogs::create([
                // 'request_json' => json_encode($jsonRequest),
                'response_json' => json_encode([
                    'Error' => $e->getMessage(),
                    'LineNo' => $e->getLine(),
                    'FileName' => $e->getFile()
                ]),
                'service' => SKY_SERVICE,
                // 'url' => $request->url(),
                // 'tenant_id' => $tenant_id,
                'response_code' => Response::HTTP_EXPECTATION_FAILED,
                'response_status' => TRANSACTION_FAILED,
                'error_code' => Response::HTTP_EXPECTATION_FAILED
            ]);
            Log::error('An error occurred: ' . $e->getMessage());
        }
        return;
    }
}
