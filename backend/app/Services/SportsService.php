<?php

namespace App\Services;

use App\Models\BetTransaction;
use App\Models\Currencies;
use App\Models\TenantConfigurations;
use App\Models\Transactions;
use App\Models\UserBonus;
use Illuminate\Support\Facades\DB;

class SportsService
{

    /**
     * @description getSportsCountry
     * @return array
     */
    public static function getSportsCountry(){

        $sql = "SELECT array_to_string(array_agg( DISTINCT pe.location_id), ',') as location_id,
            pe.sport_id,
            (SELECT name_en FROM  pulls_sports where id= pe.sport_id) as name_en
        FROM pulls_events pe
        group by pe.sport_id order by name_en asc ";
    
        $result = DB::select($sql);

       return $result;
    }

    /**
     * @description getSportsData
     * @param $id
     * @param array $filter
     * @return mixed
     */
    public static function getSportsData($id,$filter=[])
    {
        $pagination='';
        $whereStr='where ps.sport_id in('.SPORT_ENABLED_ID.') ';
        if(count($filter)){
            if($filter['size'] && $filter['page']) {

                $offset = ($filter['page'] > 1) ? $filter['page'] * $filter['size'] : 0;
                if ($filter['page'] > 1) {
                    $offset = $filter['size'] * ($filter['page'] - 1);
                }

                $limit=$filter['size'];
                $pagination = " limit $limit offset $offset ";

                if(@$filter['search']!=''){
                    $whereStr .=" and lower(ps.name_en) like '%".$filter['search']."%' ";
                }

                if(isset($filter['enabled']) && $filter['enabled']=='enable'){
                    $whereStr .=" and tbs.is_deleted= true";
                }elseif(isset($filter['enabled']) &&  $filter['enabled']=='disable'){
                    $whereStr .=" and tbs.is_deleted is null";
                }
            }
        }

        $tenant_id='';
        if($id!=''){
            $tenant_id=" and tbs.tenant_id = $id";
        }
       $sql="select ps.name_en,ps.sport_id,tbs.pulls_sport_id as id,ps.id,tbs.is_deleted,
            (SELECT count(league_id) 
            FROM pulls_events
            where sport_id =ps.id ) as league,
            (SELECT count(distinct market_id)
            FROM bets_bets
            where event_id in (SELECT distinct id
                        FROM pulls_events where sport_id =ps.id)) as market
                        
            FROM pulls_sports as ps 
            left join tenant_blocked_sports as tbs on (ps.id=tbs.pulls_sport_id $tenant_id)  
            $whereStr
            order by ps.id desc
            $pagination
        ";
        $result['result'] = DB::select($sql);


        $sqlCount="select count(ps.id) as count
            FROM pulls_sports as ps 
            left join tenant_blocked_sports as tbs on (ps.id=tbs.pulls_sport_id $tenant_id)  
            $whereStr
            
        ";
        $result['count'] = DB::select($sqlCount)[0];


        return $result;
    }

    /**
     * @description getParticipantName
     * @param $eventId
     * @return mixed
     */
    public static function getParticipantName($eventId)
    {

        $sql = "SELECT array_to_string(array_agg(participant_id), ',') as id
                        FROM pulls_eventparticipants
                        where event_id  = ?";

        $participantId = DB::select($sql, [$eventId]);
        if (!empty($participantId[0]->id)) {
            $ids = "" . @$participantId[0]->id;
            $sqlForName = "
                        SELECT array_to_string(array_agg(pp.name_en), ' Vs ') as participant_name
                        FROM pulls_participants as pp
                        where 
                        pp.id in ( $ids) 
                    ";
    
            $participantName = DB::select($sqlForName);    
            return @$participantName[0]->participant_name;
        }
        else {
            return;
        }
    }


    /* Elastic search data */
    /**
     * @description storeElasticSearchData
     * @param $transaction_id
     */
    public static function storeElasticBetSearchData($transaction_id,$rolloverAmount=0)
    {

        $ObjTransaction = new Transactions();
        $Obj = new BetTransaction;
        $Obj = $Obj->WhereRaw("bets_transactions.id = " . $transaction_id);
        $Obj = $Obj->with(['user.wallet.currency', 'betslip.bets.event']);

        $txnInfo = $Obj->first();
        if(!$txnInfo){
            return;
        }
        $target_wallet = null;

        if($txnInfo->target_wallet_id && $txnInfo->target_wallet_id!=''){
            $target_wallet = $ObjTransaction->walletOwnerInfo($txnInfo->target_wallet_id, $txnInfo->tenant_id);
            if($target_wallet) {
                @$target_wallet->before_balance = (float)@$txnInfo->target_before_balance;
                @$target_wallet->after_balance =(float)@$txnInfo->target_after_balance;
                @$target_wallet->non_cash_before_balance = (float)@$txnInfo->target_non_cash_before_balance;
                @$target_wallet->non_cash_after_balance =(float)@$txnInfo->target_non_cash_after_balance;
                unset($target_wallet->owner_id);
            }
        }



        $source_wallet = null;
        if($txnInfo->source_wallet_id && $txnInfo->source_wallet_id!=''){
            $source_wallet = $ObjTransaction->walletOwnerInfo($txnInfo->source_wallet_id, $txnInfo->tenant_id);
            if($source_wallet) {
                @$source_wallet->before_balance = (float)@$txnInfo->source_before_balance;
                @$source_wallet->after_balance = (float)@$txnInfo->source_after_balance;
                @$source_wallet->non_cash_before_balance = (float)@$txnInfo->source_non_cash_before_balance;
                @$source_wallet->non_cash_after_balance = (float)@$txnInfo->source_non_cash_after_balance;
                unset($source_wallet->owner_id);
            }
        }
        $betslip=[];
        if($txnInfo->betslip) {
            if( $txnInfo->betslip->bets[0]->event){
                $eventArr["league_name_en"] = $txnInfo->betslip->bets[0]->event->league_name_en;
                $eventArr["name_en"] = $txnInfo->betslip->bets[0]->event->name_en;
                $eventArr["sport_name_en"] = $txnInfo->betslip->bets[0]->event->sport_name_en;
                $eventArr["start_date"] = $txnInfo->betslip->bets[0]->event->start_date;
                $newEvent = $eventArr;
                unset($txnInfo->betslip->bets[0]->event);
                $txnInfo->betslip->bets[0]->event = $newEvent;
            }
            $betslip = $txnInfo->betslip->toArray();
        }
        
        $user = [];
        if($txnInfo->user_id){
            if($txnInfo->user_id) {
                $user = $ObjTransaction->getPlayerDetails($txnInfo->user_id, $txnInfo->tenant_id);
                @$user->amount = !empty($user->amount)?(float)$user->amount:0.00;
                @$user->non_cash_amount = (float)$user->non_cash_amount;
//                @$user->before_balance = (float)$txnInfo->current_balance;
                if (@$txnInfo->journal_entry == 'DR') {
                    @$user->deducted_amount = (float)$txnInfo->amount + (float)$txnInfo->non_cash_amount;
                    $user->added_amount = 0.0;
                    //$user->revenue = 0.0;
//                    @$user->after_balance = ((float)$txnInfo->current_balance) - ((float)$txnInfo->amount + (float)$txnInfo->non_cash_amount);;
                } elseif (@$txnInfo->journal_entry == 'CR') {
                    @$user->deducted_amount = (float)0;
                    @$user->revenue =  @$user->added_amount = floatval(@$txnInfo->amount) + (float)@$txnInfo->non_cash_amount;
                    @$user->after_balance = ((float)@$txnInfo->current_balance) + ((float)@$txnInfo->amount + (float)@$txnInfo->non_cash_amount);
                }

                if ($txnInfo->payment_for === 7) {
                    $user->deposit_bonus_details = [
                        "unconverted_active_deposit_bonus" => (float)$txnInfo->amount,
                        "active_deposit_bonus_remaining_rollover" => (float)$rolloverAmount
                    ];
                }else{
                    $user->deposit_bonus_details = null;
                }
            }
        }
       
        $data = array(
            'internal_tracking_id' => (int)$transaction_id,
            'is_deleted' => (boolean)$txnInfo->is_deleted,
            'amount' => (float)$txnInfo->amount,
            'journal_entry' =>$txnInfo->journal_entry,
            'status' => $txnInfo->status,
            'reference' => $txnInfo->reference,
            'description' => $txnInfo->description,
            'user_id' => $txnInfo->user_id,
            'player_details' => $user,
            'betslip_id' => $txnInfo->betslip_id,
            'betslip_details' => $betslip,
            'created_at' => getTimeZoneWiseTimeISO($txnInfo->created_at),
            'updated_at' => getTimeZoneWiseTimeISO($txnInfo->updated_at),
            'tenant_id' => $txnInfo->tenant_id,
            'target_currency' => $txnInfo->target_currency_id ?@Currencies::find($txnInfo->target_currency_id)->code:null,
            'target_wallet' => $target_wallet,
            'source_currency' => $txnInfo->source_currency_id ?@Currencies::find($txnInfo->source_currency_id)->code:null,
            'source_wallet'=>$source_wallet,
            'transaction_type' => getActionTypeName($txnInfo->payment_for),
            'transaction_id' => $txnInfo->transaction_id,
            'conversion_rate' => (float)$txnInfo->conversion_rate,
            'payment_for' => $txnInfo->payment_for,
            'non_cash_amount' => (float)$txnInfo->non_cash_amount,
            'current_balance' => (float)$txnInfo->current_balance,
            'transaction_code' => $txnInfo->transaction_code,
            'market_id' => $txnInfo->market_id,
            'net_pl' => $txnInfo->net_pl,
            'reverse_transaction_id' => $txnInfo->reverse_transaction_id,
            'commission_amount' => $txnInfo->commission_amount,
            'commission_per' => $txnInfo->commission_per,
            'runner_name' => $txnInfo->runner_name,
            'amount_in_currencies' => ($txnInfo->other_currency_amount != "" ? json_decode($txnInfo->other_currency_amount,true) : []),
            'non_cash_amount_in_currencies' => ($txnInfo->other_currency_non_cash_amount != "" ? json_decode($txnInfo->other_currency_non_cash_amount,true) : [])
//            'source_before_balance' => $txnInfo->source_before_balance,
//            'source_after_balance' => $txnInfo->source_after_balance,
//            'target_after_balance' => $txnInfo->target_after_balance,
//            'target_before_balance' => $txnInfo->target_before_balance,
        );

        // $configurations = TenantConfigurations::select('allowed_currencies')->where('tenant_id', $txnInfo->tenant_id)->get();
        // $configurations = $configurations->toArray();
        // $CurrenciesValue=[];
        // if ($configurations) {

        //     $configurations = $configurations[0];
        //     if (strlen($configurations['allowed_currencies']) > 1) {
        //         $configurations = str_replace('{', '', $configurations['allowed_currencies']);
        //         $configurations = str_replace('}', '', $configurations);
        //         $configurations = explode(',', $configurations);

        //         if (@$configurations[0] && count($configurations) > 0) {
        //             $CurrenciesValue = Currencies::select('code')->whereIn('id', $configurations)->pluck('code');
        //             $CurrenciesValue = $CurrenciesValue->toArray();
        //         }
        //     }
        // }
       
        // $currenciesTempArray = $data['amount_in_currencies'] = null;
        // $nonCashCurrenciesTempArray = $data['non_cash_amount_in_currencies'] = null;
        // $exNonCashTransactionCurrency = '';
        // $exMainTransactionCurrency = '';
        // if($txnInfo->source_currency_id != ''){
        //     $exMainTransactionCurrency = $data['source_currency'];
        // }elseif($txnInfo->target_currency_id != ''){
        //     $exMainTransactionCurrency = $data['target_currency'];
        // }
        // $amount_currency_ex = Currencies:: getExchangeRateCurrency($exMainTransactionCurrency);

        // $amount = (float)$data['amount'];
        // $nonCashAmount = (float)$data['non_cash_amount'];
        // foreach ($CurrenciesValue as $key => $value) {
        //     if($exMainTransactionCurrency != $value){
        //         $getExchangeRateCurrency = Currencies:: getExchangeRateCurrency($value);
        //         $currenciesTempArray[$value] = (float)$amount * ($amount_currency_ex / $getExchangeRateCurrency);
        //         $nonCashCurrenciesTempArray[$value] = (float)$nonCashAmount * ($amount_currency_ex / $getExchangeRateCurrency);
        //     }else{
        //         $currenciesTempArray[$value] = (float)$amount;
        //         $nonCashCurrenciesTempArray[$value] = (float)$nonCashAmount;
        //     }

        // }
        
       
        // $data['amount_in_currencies'] = $currenciesTempArray;
        // $data['non_cash_amount_in_currencies'] = $nonCashCurrenciesTempArray;
        $params = [
            'body' => $data,
            'index' => ELASTICSEARCH_INDEX['bet_transaction'],
            'type' => ELASTICSEARCH_TYPE,
            'id' => $transaction_id,
        ];


        $client = getEsClientObj();
        $client->index($params);
        unset($obj,$objEs,$client);
    }

    /**
     * @description getBetListByES
     * @param $limit
     * @param $page
     * @param $filter
     */
    public static function getBetListByES($limit, $page, $filter){
        $obj = new self();

        $body = [
            'query' => $obj->queryKey($filter),
            'sort' => $obj->sortKey($filter['sortBy'], $filter['sortOrder']),
            'timeout' => '11s',
            'track_total_hits' => true,
            /*'aggs' => [
                'total_bet_count' => $obj->totalBetCountKey(),
                'currencies' => $obj->currencyKey(),
            ]*/
        ];

        $offset = ($page > 1) ? $page * $limit : 0;

        if ($page > 1) {
            $offset = $limit * ($page - 1);
        }

        if($limit!='all'){
            $body['size'] = $limit;
            $body['from'] = (int)$offset;
        }else{
            $body['size'] = ALLCOUNT;
//            $body['size'] = 100000;
        }

        $params = [
            'index' => ELASTICSEARCH_INDEX['bet_transaction'],
            'body' => $body
        ];
        $client = getEsClientObj();
        return $client->search($params);
    }

    /**
     * @description queryKey
     * @param $params
     * @return array
     */
    private function queryKey($params)
    {
        $mustArry=[];
        $filter=[];

        if(@$params['tenant_id']!='')
            $filter []= ['term' => ['tenant_id' => ['value' => $params['tenant_id']]]];

        if (@$params['action_type'] != '') {
            $filter [] = ['term' => ['payment_for' => $params['action_type']]];
        }
      /*  if(@$params['agent_id'] && @$params['agentIdTop'] !=$params['agent_id'] ) {
            $filter []=  [
                "term" => [
                    "parent_id" => [
                        "value" => $params['agent_id']
                    ]
                ]
            ];
        }*/

        if (@$params['login_type'] ===  ADMIN_TYPE && @$params['login_id'] ) {

            $roles = getRolesDetails($params['login_id']);
                if (!in_array('owner', $roles)) {
                $filter []=  [
                    "term" => [
                        "player_details.parent_chain_ids" => [
                            "value" => $params['login_id']
                        ]
                    ]
                ];
            }
        }

//        du($filter);
        /*elseif (@$params['owner_id']) {
            $filter []=  [
                "term" => [
                    "parent_id" => [
                        "value" => $params['owner_id']
                    ]
                ]
            ];
        }

*/

        if(@$params['emailsearch']!='') {
            $mustArry['match'] = ["player_details.email" => $params['emailsearch']];
        }
        if(@$params['currency_id']!='') {
            $mustArry['match'] = ["player_details.currency_id" => $params['currency_id']];
        }
        $strSearch = @$params['searchKeyword'];
        if($strSearch!='' && is_numeric($strSearch)){
            $filter[]=$this->searchWordQuery($strSearch);
        }else{
            $str = '';
            if(!empty($strSearch)) {

                if(@$params['tenant_id']!='')
                    $str = "and  tenant_id = ".$params['tenant_id'];


                $query = DB::table('users')
                   /* ->whereRaw("( (email LIKE '%" . $strSearch . "%') OR
                    ((first_name LIKE '%" . $strSearch . "%') OR
                    (last_name LIKE '%" . $strSearch . "%') )
                    ")*/
                    ->whereRaw("( email LIKE '%" . $strSearch . "%') $str")
                    ->pluck('id')->toArray();

                if(is_array($query) && count($query)){
                    $filter =[];


                    $filter [] = ['terms' => ['user_id' => $query]];
                }
            }
        }

        if(array_key_exists('time_period', $params) && array_key_exists('time_period', $params) ) {
            if(@$params['time_period']['fromdate'] != '' && @$params['time_period']['enddate'] != '') {
                $filter[] = [
                    'range' => [
                        'created_at' => [
                            'from' => getTimeZoneWiseTimeISO($params['time_period']['fromdate']) ,
                            'include_lower'=> true,
                            'to' => str_replace("000000", "999999", getTimeZoneWiseTimeISO($params['time_period']['enddate'])),
                            'include_upper' => true
                        ]
                    ]
                ];
            }
        }


        $query = [
            'bool' => [
                'must' => count($mustArry)?$mustArry:['match_all' => new \stdClass]
            ]
        ];

        if(count( $filter)){
            $query['bool']['filter']=$filter;
        }

        return $query;
    }

    /**
     * @description sortKey
     * @param $sortBy
     * @param string $sortOrder
     * @return array
     */
    private function sortKey($sortBy, $sortOrder = 'ASC')
    {
        return [$sortBy => $sortOrder];
    }

    /**
     * @description searchWordQuery
     * @param $str
     * @return array
     */
    private function searchWordQuery($str)
    {

        $result = array(
            "bool" => array(
                "should" => array(
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "internal_tracking_id" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "internal_tracking_id" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "market_id" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "market_id" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                   /* array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "player_details" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "player_details.email.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),*/
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "amount" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "amount" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            )
        );


        return $result;
    }

    public static function storeElasticBulkSportTransactionData($transactions)
    {
        $bulkActions = [];
        $allCurrency = Currencies::select('id','code')->get();
        foreach ($transactions as $transaction) {
            $transaction_id = $transaction->id;
            $ObjTransaction = new Transactions();
            $Obj = new BetTransaction;
            $Obj = $Obj->WhereRaw("bets_transactions.id = " . $transaction_id);
            $Obj = $Obj->with(['user.wallet.currency', 'betslip.bets.event']);
    
            $txnInfo = $Obj->first();
            if(!$txnInfo){
                return;
            }
            $target_wallet = null;
            
            if($txnInfo->target_wallet_id && $txnInfo->target_wallet_id!=''){
                $target_wallet = $ObjTransaction->walletOwnerInfo($txnInfo->target_wallet_id, $txnInfo->tenant_id);
                if($target_wallet) {
                    @$target_wallet->before_balance = (float)@$txnInfo->target_before_balance;
                    @$target_wallet->after_balance =(float)@$txnInfo->target_after_balance;
                    @$target_wallet->non_cash_before_balance = (float)@$txnInfo->target_non_cash_before_balance;
                    @$target_wallet->non_cash_after_balance =(float)@$txnInfo->target_non_cash_after_balance;
                    unset($target_wallet->owner_id);
                }
            }
    
    
    
            $source_wallet = null;
            if($txnInfo->source_wallet_id && $txnInfo->source_wallet_id!=''){
                $source_wallet = $ObjTransaction->walletOwnerInfo($txnInfo->source_wallet_id, $txnInfo->tenant_id);
                if($source_wallet) {
                    @$source_wallet->before_balance = (float)@$txnInfo->source_before_balance;
                    @$source_wallet->after_balance = (float)@$txnInfo->source_after_balance;
                    @$source_wallet->non_cash_before_balance = (float)@$txnInfo->source_non_cash_before_balance;
                    @$source_wallet->non_cash_after_balance = (float)@$txnInfo->source_non_cash_after_balance;
                    unset($source_wallet->owner_id);
                }
            }
            $betslip=[];
            if($txnInfo->betslip) {
                if( $txnInfo->betslip->bets[0]->event){
                    $eventArr["league_name_en"] = $txnInfo->betslip->bets[0]->event->league_name_en;
                    $eventArr["name_en"] = $txnInfo->betslip->bets[0]->event->name_en;
                    $eventArr["sport_name_en"] = $txnInfo->betslip->bets[0]->event->sport_name_en;
                    $eventArr["start_date"] = $txnInfo->betslip->bets[0]->event->start_date;
                    $newEvent = $eventArr;
                    unset($txnInfo->betslip->bets[0]->event);
                    $txnInfo->betslip->bets[0]->event = $newEvent;
                }
                $betslip = $txnInfo->betslip->toArray();
            }
            
            $user = [];
            if($txnInfo->user_id){
                if($txnInfo->user_id) {
                    $user = $ObjTransaction->getPlayerDetails($txnInfo->user_id, $txnInfo->tenant_id);
                    @$user->amount = !empty($user->amount)?(float)$user->amount:0.00;
                    @$user->non_cash_amount = (float)$user->non_cash_amount;
    //                @$user->before_balance = (float)$txnInfo->current_balance;
                    if (@$txnInfo->journal_entry == 'DR') {
                        @$user->deducted_amount = (float)$txnInfo->amount + (float)$txnInfo->non_cash_amount;
                        $user->added_amount = 0.0;
                        //$user->revenue = 0.0;
    //                    @$user->after_balance = ((float)$txnInfo->current_balance) - ((float)$txnInfo->amount + (float)$txnInfo->non_cash_amount);;
                    } elseif (@$txnInfo->journal_entry == 'CR') {
                        @$user->deducted_amount = (float)0;
                        @$user->revenue =  @$user->added_amount = floatval(@$txnInfo->amount) + (float)@$txnInfo->non_cash_amount;
                        @$user->after_balance = ((float)@$txnInfo->current_balance) + ((float)@$txnInfo->amount + (float)@$txnInfo->non_cash_amount);
                    }
    
                    if ($txnInfo->payment_for === 7) {
                        //$userBonus = UserBonus::where('transaction_id',$txnInfo->id)->first();
                        $user->deposit_bonus_details = [
                            "unconverted_active_deposit_bonus" => (float)$txnInfo->amount,
                            "active_deposit_bonus_remaining_rollover" => (float)0
                        ];
                    }else{
                        $user->deposit_bonus_details = null;
                    }
                }
            }
            $sourceCurrency = null;
            $targetCurrency = null;
            if($txnInfo->source_currency_id!=''){
                $sourceCurrencyId = $txnInfo->source_currency_id;
                $sourceCurrency = $allCurrency->filter(function($item) use ($sourceCurrencyId) {
                    return $item->id==$sourceCurrencyId;
                })->first();
            }
            
            if($txnInfo->target_currency_id!=''){
                $targetCurrencyId = $txnInfo->target_currency_id;
                $targetCurrency = $allCurrency->filter( function($item) use ($targetCurrencyId) {
                    return $item->id==$targetCurrencyId;
                })->first();
            }
           
            $data = array(
                'internal_tracking_id' => (int)$transaction_id,
                'is_deleted' => (boolean)$txnInfo->is_deleted,
                'amount' => (float)$txnInfo->amount,
                'journal_entry' =>$txnInfo->journal_entry,
                'status' => $txnInfo->status,
                'reference' => $txnInfo->reference,
                'description' => $txnInfo->description,
                'user_id' => $txnInfo->user_id,
                'player_details' => $user,
                'betslip_id' => $txnInfo->betslip_id,
                'betslip_details' => $betslip,
                'created_at' => getTimeZoneWiseTimeISO($txnInfo->created_at),
                'updated_at' => getTimeZoneWiseTimeISO($txnInfo->updated_at),
                'tenant_id' => $txnInfo->tenant_id,
                'target_currency' => $targetCurrency ? $targetCurrency->code:null,
                'target_wallet' => $target_wallet,
                'source_currency' => $sourceCurrency ? $sourceCurrency->code:null,
                'source_wallet'=>$source_wallet,
                'transaction_type' => getActionTypeName($txnInfo->payment_for),
                'transaction_id' => $txnInfo->transaction_id,
                'conversion_rate' => (float)$txnInfo->conversion_rate,
                'payment_for' => $txnInfo->payment_for,
                'non_cash_amount' => (float)$txnInfo->non_cash_amount,
                'current_balance' => (float)$txnInfo->current_balance,
                'transaction_code' => $txnInfo->transaction_code,
                'market_id' => $txnInfo->market_id,
                'net_pl' => $txnInfo->net_pl,
                'reverse_transaction_id' => $txnInfo->reverse_transaction_id,
                'commission_amount' => $txnInfo->commission_amount,
                'commission_per' => $txnInfo->commission_per,
                'runner_name' => $txnInfo->runner_name,
                'amount_in_currencies' => ($txnInfo->other_currency_amount != "" ? json_decode($txnInfo->other_currency_amount,true) : []),
                'non_cash_amount_in_currencies' => ($txnInfo->other_currency_non_cash_amount != "" ? json_decode($txnInfo->other_currency_non_cash_amount,true) : [])
            );
    
            $bulkActions[] = $data; // Add the document data directly
       }
     
       $params = ['body' => []];
       $client = getEsClientObj();
       for ($i = 0; $i < count($transactions); $i++) {
          $params['body'][] = [
               'index' => [
                   '_index' => ELASTICSEARCH_INDEX['bet_transaction'],
                   '_type' => ELASTICSEARCH_TYPE,
                   '_id'    => $bulkActions[$i]['internal_tracking_id']
               ]
           ];

           $params['body'][] = $bulkActions[$i];
           // Every 1000 documents stop and send the bulk request
           if ($i % 1000 == 0) {
            
               $responses = $client->bulk($params);

               // erase the old bulk request
               $params = ['body' => []];

               // unset the bulk response when you are done to save memory
               unset($responses);
           }
       }

       // Send the last batch if it exists
       if (!empty($params['body'])) {
           $responses = $client->bulk($params);
       }
      // echo "casino transaction re-index successfully done";
      
       
    }
}
