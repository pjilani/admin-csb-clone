<?php

namespace App\Services;

use Elasticsearch\ClientBuilder;
use Illuminate\Support\Facades\Auth;
class UserService
{
    /**
     * @description fetchResult
     * @param $params
     * @return array
     */
    public static function fetchResult($params)
    {
        $obj = new self();
        $body = [
            'query' => $obj->queryKey($params),
            'timeout' => '11s',
            '_source' => 'false',
            'size' => ALLCOUNT,
            'track_total_hits' => true,
            'aggs' => [
                "active_player_chart" => $obj->getQueryActivePlayerChart($params),
                "currencies" => $obj->getQueryCurrencies($params),
                "players" => $obj->getQueryPlayers($params)
            ]
        ];

        $params = [
            'index' => ELASTICSEARCH_INDEX['users'],
            'body' => $body
        ];
       
        $client = getEsClientObj();
        $return = $client->search($params);
        return $return;
    }

    public static function fetchUserBalanceResult($params)
    {
        $obj = new self();
        $body = [
            'query' => $obj->queryKey($params),
            'timeout' => '11s',
            '_source' => 'false',
            'size' => ALLCOUNT,
            'track_total_hits' => true,
            'aggs' => [
                "currencies" => $obj->getQueryCurrencies($params)
            ]
        ];

        $params = [
            'index' => ELASTICSEARCH_INDEX['users'],
            'body' => $body
        ];
       
        $client = getEsClientObj();
        $return = $client->search($params);
        return $return;
    }
    
    
    
    public static function fetchPlayerResult($players_ids,$params)
    {
        $obj = new self();
        $limit = 10;
        $page = $params['page'] ?? 1;


        $offset = ($page > 1) ? $page * $limit : 0;

        if ($page > 1) {
            $offset = $limit * ($page - 1);
        }
        $body = [
            'query' => $obj->queryPlayersKey($players_ids),
            'timeout' => '60s',
            '_source' => true,
//            'size' => ALLCOUNT,
            "sort" => [
              [
                "total_bet_amount"=> [
                  "order"=> "desc"
                ]
              ]
            ],
            'track_total_hits' => true,
             
        ];
        $body['size'] = $limit;
        $body['from'] = (int)$offset;
        $params = [
            'index' => ELASTICSEARCH_INDEX['users'],
            'body' => $body
        ];
       
        $client = getEsClientObj();
        $return = $client->search($params);
        return $return;
    }

    /**
     * @description queryKey
     * @param $params
     * @return mixed
     */
    private function queryKey($params)
    {
        $query["bool"] = [
            "must" => ["match_all" => new \stdClass],
        ];


        $query["bool"]['filter'][] =
            [

                    "term" => [
                        "status" => [
                            "value" => "Active"
                        ]
                    ]

            ];

        if(@$params['tenantId']) {
            $query["bool"]['filter'][] = [
                "term" => [
                    "tenant_id" => [
                        "value" => $params['tenantId']
                    ]
                ]
            ];
        }

        /*if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
            $roles = getRolesDetails(@Auth::user()->id);
            if (!in_array('owner', $roles) && !in_array('sub-admin', $roles)) {
                $query["bool"]['filter'][] = [
                    "term" => [
                        "parent_chain_ids" => [
                            "value" => $params['agent_id']
                        ]
                    ]
                ];
            } else {
                if (@$params['isDirectPlayer'] == 'all' && $params['agent_id']) {
                    $query["bool"]['filter'][] = [
                        "term" => [
                            "parent_chain_ids" => [
                                "value" => $params['agent_id']
                            ]
                        ]
                    ];
                }
            }
        } else {
            $roles = getRolesDetails(@$params['agent_id']);
            if (!in_array('owner', $roles) && !in_array('sub-admin', $roles)) {
                $query["bool"]['filter'][] = [
                    "term" => [
                        "parent_chain_ids" => [
                            "value" => $params['agent_id']
                        ]
                    ]
                ];
            } else {
                if (@$params['isDirectPlayer'] == 'all' && $params['agent_id']) {
                    $query["bool"]['filter'][] = [
                        "term" => [
                            "parent_chain_ids" => [
                                "value" => $params['agent_id']
                            ]
                        ]
                    ];
                }
            }
        }*/
        if(@$params['agent_id']){
            $query["bool"]['filter'][] = [
                "term" => [
                    "parent_chain_ids" => [
                        "value" => $params['agent_id']
                    ]
                ]
            ];
        }
        return $query;
    }
   
   
    private function queryPlayersKey($players_ids)
    {
      $query["bool"]['filter'][] =
            [
              "terms" => [
                  "_id" => $players_ids
              ]
            ];
      return $query;
    }

    /**
     * @description getQueryActivePlayerChart
     * @param $params
     * @return mixed
     */
    private function getQueryActivePlayerChart($params)
    {
        $query['filter'] = [
            "range" => [
                "creation_date" => [
                    "from" => $params['time_period']['fromdate'],
                    "include_lower" => true,
                    "to" => str_replace("000000", "999999", $params['time_period']['enddate']),
                    "include_upper" => true
                ]
            ]
        ];

        $interval='hour';
        $intervalParam=$params['interval']??"today";
        if($intervalParam==='weekly' || $intervalParam==='custom'){
            $interval='day';
        }elseif ($intervalParam==='monthly'){
            $interval='day';
        }elseif ($intervalParam==='yearly'){
            $interval='month';
        }



        $query['aggs'] = [
            "active_player" => [
                "date_histogram" => [
                    "field" => "creation_date",
                    "interval" => "$interval",
                    "min_doc_count" => 0,
                    "extended_bounds" => [
                        "min" => $params['time_period']['fromdate'],
                        "max" => str_replace("000000", "999999", $params['time_period']['enddate']),
                    ]
                ]
            ]
        ];

        return $query;

    }

    /**
     * @description getQueryCurrencies
     * @param $params
     */
    private function getQueryCurrencies($params)
    {
        $query['terms'] = [
            "field" => "currency",
            "size" => 10000
        ];

        $query['aggs'] = [
            "real_balance" => [
                "sum" => [
                    "field" => "real_balance"
                ]
            ],
            "bonus_balance" => [
                "sum" => [
                    "field" => "non_cash_balance"
                ]
            ]
        ];

        return $query;
    }

    /**
     * @description getQueryPlayers
     * @param $params
     */
    private function getQueryPlayers($params)
    {
        $query["filter"] = [
            "range" => [
                "creation_date" => [
                    "from" => $params['time_period']['fromdate'],
                    "include_lower" => true,
                    "to" => str_replace("000000", "999999", $params['time_period']['enddate']),
                    "include_upper" => true
                ]
            ]
        ];
        $query["aggs"] = [
            "total_players" => [
                "cardinality" => [
                    "field" => "player_id",
                    "precision_threshold" => 40000
                ]
            ]
        ];
        return $query;
    }
}


