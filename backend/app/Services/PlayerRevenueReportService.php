<?php

namespace App\Services;

use Elasticsearch\ClientBuilder;

class PlayerRevenueReportService
{


    public static function fetchResult($params)
    {
        $obj = new self();

        $body = [
            'query' => $obj->queryKey($params),
            'sort' => $obj->sortKey($params['sortBy'], $params['sortOrder']),
            'timeout' => '60s',
            '_source' => 'false',
            'track_total_hits' => "false",
            'aggs' => [
                'reports' => $obj->reportsKey($params, $obj),
                'total_bets_in_EUR' => $obj->totalBetsinEURKey($params),
                'total_refund_in_EUR' => $obj->total_refund_in_EURKey($params),
                'total_wins_in_EUR' => $obj->totalWinsInEURKey($params),
                'total_bonus_claim_in_EUR' => $obj->totalBonusClaimInEURKey($params),
                'total_bonus_withdraw_in_EUR' => $obj->totalBonusWithdrawInEURKey($params),
                'total_record_count' => $obj->totalRecordCountKey()

            ]
        ];


        if ($params['limit'] != 'all') {
            $body['size'] = (int)$params['limit'];
            $body['from'] = (int)$params['offset'];
        } else {
            $body['size'] = ALLCOUNT;

        }

        $params = [
            'index' => ELASTICSEARCH_INDEX['transactions'],
            'body' => $body
        ];
       
        $client = getEsClientObj();

        $return = $client->search($params);


        return $return;
    }

    private function sortPosition($sortKey) {

      switch($sortKey) {
        case 'player_name':
        case 'currency':
          $script = "doc['player_details.player_name'].value.toLowerCase().concat('-', doc['player_details.currency'].value, '-', doc['player_details.agent_name'].value ) ";
        case 'agent_name':
          $script = "doc['player_details.agent_name'].value.toLowerCase() + '-' + doc['player_details.currency'].value + '-' + doc['player_details.player_name'].value";
        default:
          $script = "doc['player_details.player_name'].value.toLowerCase() + '-' + doc['player_details.currency'].value + '-' + doc['player_details.agent_name'].value";
      }

      return $script;
    }
 


    private function reportsKey($params, &$obj)
    {

        $reports = [
          "terms" => array(
            "script" => $obj->sortPosition($params['sortBy']),
            "size" => ALLCOUNT
          ),
          "aggs" => array(
            "top" => array(
                "top_hits" => array(
                    "_source" => array(
                        "includes" => [
                            "player_details.player_name",
                            "player_details.agent_name",
                            "player_details.phone",
                            "player_details.player_id",
                            "player_details.wallet_id",
                            "player_details.email",
                            "player_details.created_at",
                        ]
                    )
                )
            ),
            "bet" => array(
                "filter" => array(
                    "terms" => array(
                        "transaction_type" => [
                            "bet",
                            "bet_non_cash"
                        ]
                    )
                ),
                "aggs" => array(
                    "bet_amount" => array(
                        "sum" => array(
                            "field" => "amount"
                        )
                    ),
                    "bet_amount_in_EUR" => array(
                        "sum" => array(
                            "field" => "amount_in_currencies.EUR"
                        )
                    )
                ),
            ),
            "win" => array(
                "filter" => array(
                    "terms" => array(
                        "transaction_type" => [
                            "win",
                            "win_non_cash"
                        ]
                    )
                ),
                "aggs" => array(
                    "win_amount" => array(
                        "sum" => array(
                            "field" => "amount"
                        )
                    ),
                    "win_amount_in_EUR" => array(
                        "sum" => array(
                            "field" => "amount_in_currencies.EUR"
                        )
                    )
                )
            ),
            "refund" => array(
                "filter" => array(
                    "terms" => array(
                        "transaction_type" => [
                            "refund",
                            "refund_non_cash"
                        ]
                    )
                ),
                "aggs" => array(
                    "refund_amount" => array(
                        "sum" => array(
                            "field" => "amount"
                        )
                    ),
                    "refund_amount_in_EUR" => array(
                        "sum" => array(
                            "field" => "amount_in_currencies.EUR"
                        )
                    )
                )
            ),
            "bonus_claim" => array(
                "filter" => array(
                    "terms" => array(
                        "transaction_type" => [
                            "deposit_bonus_claim",
                            "non_cash_bonus_claim",
                            "non_cash_granted_by_admin"
                        ]
                    )
                ),
                "aggs" => array(
                    "bonus_claim_amount" => array(
                        "sum" => array(
                            "field" => "amount"
                        )
                    ),
                    "bonus_claim_amount_in_EUR" => array(
                        "sum" => array(
                            "field" => "amount_in_currencies.EUR"
                        )
                    )
                )
            ),
            "bonus_withdraw" => array(
                "filter" => array(
                    "terms" => array(
                        "transaction_type" => [
                            "non_cash_withdraw_by_admin"
                        ]
                    )
                ),
                "aggs" => array(
                    "bonus_withdraw_amount" => array(
                        "sum" => array(
                            "field" => "amount"
                        )
                    ),
                    "bonus_withdraw_amount_in_EUR" => array(
                        "sum" => array(
                            "field" => "amount_in_currencies.EUR"
                        )
                    )
                )
            ),
            "bet_after_refund" => array(
                "bucket_script" => array(
                    "buckets_path" => array(
                        "bet" => "bet>bet_amount",
                        "refund" => "refund>refund_amount"
                    ),
                    "script" => "params.bet - params.refund"
                )
            ),
          "bet_after_refund_in_EUR" => array(
              "bucket_script" => array(
                  "buckets_path" => array(
                      "bet_in_EUR" => "bet>bet_amount_in_EUR",
                      "refund_in_EUR" => "refund>refund_amount_in_EUR"
                  ),
                  "script" => "params.bet_in_EUR - params.refund_in_EUR"
              )
          ),
        "ggr" => array(
            "bucket_script" => array(
                "buckets_path" => array(
                    "bet_after_refund" => "bet_after_refund",
                    "win" => "win>win_amount"
                ),
                "script" => "params.bet_after_refund - params.win"
            )
        ),
          "ggr_in_EUR" => array(
              "bucket_script" => array(
                  "buckets_path" => array(
                      "bet_after_refund_in_EUR" => "bet_after_refund_in_EUR",
                      "win_amount_in_EUR" => "win>win_amount_in_EUR"
                  ),
                  "script" => "params.bet_after_refund_in_EUR - params.win_amount_in_EUR"
              )
          ),
            "final_bonus" => array(
                "bucket_script" => array(
                    "buckets_path" => array(
                        "bonus_claim" => "bonus_claim>bonus_claim_amount",
                        "bonus_withdraw" => "bonus_withdraw>bonus_withdraw_amount"
                    ),
                    "script" => "params.bonus_claim - params.bonus_withdraw"
                )
            ),
              "final_bonus_in_EUR" => array(
                  "bucket_script" => array(
                      "buckets_path" => array(
                          "bonus_claim_in_EUR" => "bonus_claim>bonus_claim_amount_in_EUR",
                          "bonus_withdraw_in_EUR" => "bonus_withdraw>bonus_withdraw_amount_in_EUR"
                      ),
                      "script" => "params.bonus_claim_in_EUR - params.bonus_withdraw_in_EUR"
                  )
              ),
            "ngr" => array(
                "bucket_script" => array(
                    "buckets_path" => array(
                        "final_bonus" => "final_bonus",
                        "ggr" => "ggr"
                    ),
                    "script" => "params.ggr - params.final_bonus"
                )
            ),
              "ngr_in_EUR" => array(
                  "bucket_script" => array(
                      "buckets_path" => array(
                          "final_bonus_in_EUR" => "final_bonus_in_EUR",
                          "ggr_in_EUR" => "ggr_in_EUR"
                      ),
                      "script" => "params.ggr_in_EUR - params.final_bonus_in_EUR"
                  )
              ),
            "deposit" => array(
                "filter" => array(
                    "terms" => array(
                        "transaction_type" => ["deposit", "non_cash_granted_by_admin"]
                    )
                ),
                "aggs" => array(
                    "deposit_amount" => array(
                        "sum" => array(
                            "field" => "amount"
                        )
                    )
                )
            ),
            "withdraw" => array(
                "filter" => array(
                    "bool" => [
                        "must" => [
                            [
                                "terms" => [
                                    "transaction_type" => ["withdraw", "non_cash_withdraw_by_admin"]
                                ]
                            ],
                            [
                                "term" => [
                                    "status" => "success"
                                ]
                            ]
                        ],
                        "must_not" => [
                            "terms" => [
                                "description" => [
                                    "cancelled by admin", "cancelled by the player","Pending confirmation from admin"
                                ]
                            ]
                        ]
                    ]
                ),
                "aggs" => array(
                    "withdraw_amount" => array(
                        "sum" => array(
                            "field" => "amount"
                        )
                    )
                )
            ),
            "withdraw_cancel" => array(
                "filter" => array(
                    "term" => array(
                        "transaction_type" => "withdraw_cancel"
                    )
                ),
                "aggs" => array(
                    "withdraw_cancel_amount" => array(
                        "sum" => array(
                            "field" => "amount"
                        )
                    )
                )
            ),
            "final_withdraw" => array(
                "bucket_script" => array(
                    "buckets_path" => array(
                        "withdraw" => "withdraw>withdraw_amount"
                    ),
                    "script" => "params.withdraw"
                )
            ),
            "final_sort" => array(
                "bucket_sort" => array(
                    "sort" => [
                        array(
                            "_key" => array(
                                "order" => "asc"
                            )
                        )
                    ],
                    "size" => ALLCOUNT,
                    "from" => 0
                )
            )
          )
        ];

        return $reports;

    }

    private function totalBetsinEURKey($params)
    {
        $total_bets_in_EUR = [
            "filter" => array(
                "terms" => array(
                    "transaction_type" => [
                        "bet",
                        "bet_non_cash"
                    ]
                )
            ),
            "aggs" => array(
                "bet_amount" => array(
                    "sum" => array(
                        "field" => "amount_in_currencies.".$params["tenant_base_currency"]
                    )
                )
            )
        ];


        return $total_bets_in_EUR;
    }

    private function total_refund_in_EURKey($params)
    {
        $total_refund_in_EUR = ["filter" => array(
            "terms" => array(
                "transaction_type" => [
                    "refund",
                    "refund_non_cash"
                ]
            )
        ),
            "aggs" => array(
                "refund_amount" => array(
                    "sum" => array(
                        "field" => "amount_in_currencies.".$params["tenant_base_currency"]
                    )
                )
            )
        ];

        return $total_refund_in_EUR;

    }

    private function totalWinsInEURKey($params)
    {
        $total_wins_in_EUR = [
            "filter" => array(
                "terms" => array(
                    "transaction_type" => [
                        "win",
                        "win_non_cash"
                    ]
                )
            ),
            "aggs" => array(
                "win_amount" => array(
                    "sum" => array(
                        "field" => "amount_in_currencies.".$params["tenant_base_currency"]
                    )
                )
            )
        ];

        return $total_wins_in_EUR;
    }

    private function totalBonusClaimInEURKey($params)
    {
        $total_bonus_claim_in_EUR = [
            "filter" => array(
                "terms" => array(
                    "transaction_type" => [
                        "deposit_bonus_claim",
                        "non_cash_bonus_claim",
                        "non_cash_granted_by_admin"
                    ]
                )
            ),
            "aggs" => array(
                "bonus_claim_amount" => array(
                    "sum" => array(
                        "field" => "amount_in_currencies.".$params["tenant_base_currency"]
                    )
                )
            )
        ];

        return $total_bonus_claim_in_EUR;
    }


    private function totalBonusWithdrawInEURKey($params)
    {
        $total_bonus_withdraw_in_EUR = [
            "filter" => array(
                "terms" => array(
                    "transaction_type" => [
                        "non_cash_withdraw_by_admin"
                    ]
                )
            ),
            "aggs" => array(
                "bonus_withdraw_amount" => array(
                    "sum" => array(
                        "field" => "amount_in_currencies.".$params["tenant_base_currency"]
                    )
                )
            )

        ];

        return $total_bonus_withdraw_in_EUR;
    }

    private function totalRecordCountKey()
    {
        $total_record_count = [
            "cardinality" => [
                "field" => "player_details.player_id",
                "precision_threshold" => ALLCOUNT
            ]
        ];


        return $total_record_count;
    }

    private function sortKey($sortBy, $sortOrder = 'ASC')
    {
        return [$sortBy => $sortOrder];
    }


    private function queryKey($params)
    {
      $obj = new self();

        $mustArry = [];
        $filter = [];
        $must = [];

        if ($params['tenant_id'] != '') {
          $filter []= ['term' => ['tenant_id' => ['value' => $params['tenant_id']]]];
        }

        $filter [] = [
            "bool" => [
                "must_not" => [
                    "bool" => [
                        "must_not" => [
                            "exists" => [
                                "field" => "player_details"
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $filter [] = [
            "term" => [
                "player_details.demo" => ["value" => "false"]
            ]
        ];

       $filter [] = [
          "bool" =>[
            "must_not" => [
              "terms" => [
                "transaction_type"=> ["dummy_for_admin", "tip", "tip_non_cash"]
              ]
            ]
          ]
        ];
        if ($params['parent_id'] != '' && $params['parent_id'] != '0') {
            $filter[] = [
                "term" => [
                    "player_details.parent_chain_ids" => [
                        "value" => $params['parent_id']
                    ]
                ]
            ];
        }

        if ($params['isDirectPlayer']=='direct' && $params['parent_id'] != '')
            $filter []= ['term' => ['player_details.parent_id' => ['value' => $params['parent_id']]]];

        if(array_key_exists('game_provider', $params) && $params['game_provider'] != '') {
          $filter [] = [
            'term' => [
              'game_provider' => ['value' => $params['game_provider'] ]
            ]
          ];
        }

        if(array_key_exists('game_type', $params) && $params['game_type'] != '') {
         $filter [] = [
            'term' => [
              'game_type' => ['value' => $params['game_type'] ]
            ]
          ];
        }

        if(array_key_exists('table_id', $params) && $params['table_id'] != '') {
         $filter [] = [
            'term' => [
              'table_id' => ['value' => $params['table_id'] ]
            ]
          ];
        }

        if(array_key_exists('time_period', $params) && array_key_exists('time_period', $params) ) {
          if($params['time_period']['fromdate'] != '' && $params['time_period']['enddate'] != '') {
            $filter[] = [
              'range' => [
                'created_at' => [
                  'from' => $params['time_period']['fromdate'] ,
                  'include_lower'=> true,
                  'to' => str_replace("000000", "999999", $params['time_period']['enddate']),
                  'include_upper' => true
                ]
              ]
            ];
          }
        }


        if (@$params['currency'] != '') {
            $filter [] = ['term' => ["player_details.currency" => ['value' => $params['currency']]]];
        }


        if (array_key_exists('searchKeyword', $params) && $params['searchKeyword'] != '') {
          $query = $obj->searchWordQuery($params['searchKeyword']);
        } else {
          $query = [
              'bool' => [
                  'must' => ['match_all' => new \stdClass]
              ]
          ];
        }
        if (count($filter)) {
            $query['bool']['filter'] = $filter;
        }
        return $query;
    }


    private function searchWordQuery($word) {
      $obj = new self();

      $prepareShould = [];
      $keys = ['player_name', 'agent_name', 'phone', 'player_id_s'];
      foreach($keys as $value) {
        $prepareShould [] = [
          'dis_max' => [
            'queries' => [
              [
                'bool' => [
                  'must' => [
                    'bool' => [
                      'should' => $obj->searchMatchPhrase($value, $word)
                    ]
                  ],
                  'should' =>  $obj->searchMatchAnalysed($value, $word)
                ]
              ]
            ]
          ]
        ];
      }

      $query = [
        'bool' => [
          'must' => [
            'bool' => [
              'should'=> $prepareShould
            ]
          ]
        ]
      ];
      return $query;
    }

    private function searchMatchPhrase($userType, $word) {
      $analyzer = 'searchkick_word_search';
      return [
          'match' => [
            "player_details.$userType.word_start"=> [
              'query' => $word,
              'boost' => 10,
              'operator' => 'and',
              'analyzer' => $analyzer
            ]
          ]
      ];
    }

    private function searchMatchAnalysed($userType, $word) {
      $analyzer = 'searchkick_word_search';
      return [
          'match' => [
            "player_details.$userType.analyzed"=> [
              'query' => $word,
              'boost' => 10,
              'operator' => 'and',
              'analyzer' => $analyzer
            ]
          ]
      ];
    }
}

