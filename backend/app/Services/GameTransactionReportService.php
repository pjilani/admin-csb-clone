<?php

namespace App\Services;

use Elasticsearch\ClientBuilder;

class GameTransactionReportService
{



    public static function fetchResult($params)
    {
        $obj = new self();
      
        $body = [
            'query' => $obj->queryKey($params),
            'sort' => $obj->sortKey($params['sortBy'], $params['sortOrder']),
            'timeout' => '11s',
            'track_total_hits' => true,
            'aggs' => [
                'total_bets' => $obj->totalBetsKey($params),
                'total_refund' => $obj->totalRefundKey($params),
                'total_wins' => $obj->totalWinsKey($params),
                'total_tip' => $obj->totalTipKey($params)
             
            ]
        ];

        if($params['limit']!='all'){
            $body['size'] = (int)$params['limit'];
            $body['from'] = (int)$params['offset'];
        }else{
            $body['size'] = ALLCOUNT;
        }


        $params = [
            'index' => ELASTICSEARCH_INDEX['transactions'],
            'body' => $body
        ];
        $client = getEsClientObj();
        $return = $client->search($params);


        return $return;
    }


    private function totalAmountKey($params)
    {
        return ["sum" => [ "field" => "amount_in_currencies.".$params["tenant_base_currency"]]];
    }
    private function totalBetsKey($params)
    {
       
        $totalBets = [ "filter" => ["terms" => [ "transaction_type" => ["bet","bet_non_cash"] ]],
                        "aggs" => ["total" => $this->totalAmountKey($params)]    
                    ];
        return $totalBets;
    }

    private function totalRefundKey($params)
    {
        $totalRefund = ["filter" =>  ["terms" =>  ["transaction_type" =>  ["refund", "refund_non_cash"]]],
                          "aggs" => ["total" => $this->totalAmountKey($params)]    
                         ];
        return $totalRefund;
    }
    private function totalWinsKey($params)
    {
        $totalWins = [  "filter" => [ "terms" => ["transaction_type" => [ "win","win_non_cash"] ]],
                          "aggs" => ["total" => $this->totalAmountKey($params)]    
                    ];
        return $totalWins;
    }
    private function totalTipKey($params)
    {
        $totalTip =  [ "filter" => [ "terms" => [ "transaction_type" => ["tip","tip_non_cash" ]]],
                        "aggs" => ["total" => $this->totalAmountKey($params)]    
                      ];
        return $totalTip;
    }


    private function sortKey($sortBy, $sortOrder = 'ASC')
    {
        return [$sortBy => $sortOrder];
    }

    private function queryKey($params)
    {
      
        $mustArry=[];
        $filter=[];
        

        if($params['tenant_id']!='')
        $filter []= ['term' => ['tenant_id' => ['value' => $params['tenant_id']]]];

        $filter [] = ["bool" =>
            ["must_not" => [
                "bool" => [
                    "must_not" => [
                        "exists" => [
                            "field" => "player_details"
                        ]
                    ]
                ]
            ]
            ]
        ];

        if ($params['isDirectPlayer']=='direct' && $params['parent_id'] != ''){
          $filter []= ['term' => ['player_details.parent_id' => ['value' => $params['parent_id']]]];
        }

        if($params['owner_id'])
            $filter [] = ['term' => ['player_details.parent_chain_ids' => ['value' => $params['owner_id']]]];


        if($params['parent_id'])
            $filter []= ['term' => ['player_details.parent_chain_ids' => ['value' => $params['parent_id']]]];
        
        
        if($params['game_provider'])
            $filter []= ['term' => ['game_provider' => ['value' => $params['game_provider']]]];
        
        
        if($params['game_type'])
            $filter []= ['term' => ['game_type' => ['value' => $params['game_type']]]];

       
        if($params['currency']!='') {
            $filter []= ['term' => ['player_details.currency' => ['value' => $params['currency']]]];
        }

        if($params['searchKeyword']!=''){

            $filter[]= $this->searchWordQuery($params['searchKeyword']);
        }

        if (is_array($params['time_period']) &&
            $params['time_period']['fromdate'] != '' &&
            $params['time_period']['enddate'] != ''
        ) {

            $filter [] = [
                'range' => [
                    'created_at' => [
                        "from" =>  $params['time_period']['fromdate'],
                        "include_lower" => true,
                        "to" => str_replace("000000", "999999", $params['time_period']['enddate']),
                        "include_upper" => true
                    ]
                ]
            ];

        }
        if(@$params['internal_error_code']!=''){

            $filter [] = [
                'term' => [
                    'internal_error_code' =>
                        (string)$params['internal_error_code']

                ]
            ];
        }


        if(@$params['action_type']){

            $filter [] = [
                'term' => [
                    'transaction_type' =>
                        $params['action_type']

                ]
            ];
        }else {


            $filter [] = [
                'terms' => [
                    'transaction_type' => [
                        "bet",
                        "win",
                        "refund",
                        "tip",
                        "bet_non_cash",
                        "win_non_cash",
                        "tip_non_cash",
                        "refund_non_cash"
                    ]
                ]
            ];
        }

        $query = [
          'bool' => [
              'must' => count($mustArry)?$mustArry:['match_all' => new \stdClass]
          ]
      ];

      if(count( $filter)){
          $query['bool']['filter']=$filter;
      }
      return $query;
    }

    private function searchWordQuery($str)
    {

        $result = array(
            "bool" => array(
                "should" => array(
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "player_details.player_id_s.word_start" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "player_details.player_id_s.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "player_details.player_name.word_start" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "player_details.player_name.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "player_details.agent_name.word_start" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "player_details.agent_name.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "transaction_id.word_start" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "transaction_id.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "round_id_s.word_start" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "round_id_s.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "actionee_id.word_start" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "actionee_id.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "player_details.phone.word_start" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "player_details.phone.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            )
        );


        return $result;
    }


}
