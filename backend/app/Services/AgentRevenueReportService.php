<?php

namespace App\Services;

use App\Models\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AgentRevenueReportService
{


    public static function fetchResult($params)
    {
        $obj = new self();

        $body = [
            'query' => $obj->queryKey($params),
            'timeout' => '60s',
            "_source" =>"false",
            'track_total_hits' => true,
            'aggs' => [
                'reports' => $obj->reportsKey($params),
            ]
        ];
        $body['size'] = ALLCOUNT;

        $params = [
            'index' => ELASTICSEARCH_INDEX['transactions'],
            'body' => $body
        ];
        $client = getEsClientObj();
        $return = $client->search($params);

        return $return;
    }

    private function queryKey($params)
    {
        $mustArry = [];
        $filter = [];


        if (@$params['tenant_id'] != '')
            $filter [] = ['term' => ['tenant_id' => ['value' => $params['tenant_id']]]];


        $filter [] =[
                "bool" => [
                    "must_not" => [
                        'terms' => [
                            'transaction_type' => [
                                "tip",
                                "tip_non_cash"
                            ]
                        ]
                    ]
                ]
        ];

        $filter [] = [
            "bool" => [
                "must_not" => [
                    "bool" => [
                        "must_not" => [
                            'exists' => [
                                'field' => "player_details"

                            ]
                        ]
                    ]

                ]
            ]
        ];

        $filter [] = ['term' =>
            [
                'player_details.demo' =>
                ['value' => "false"]
            ]
        ];


        $filter [] = array (
            'bool' =>
                array (
                    'should' =>
                        array (
                            0 =>
                                array (
                                    'bool' =>
                                        array (
                                            'filter' =>
                                                array (
                                                    0 =>
                                                        array (
                                                            'range' =>
                                                                array (
                                                                    'created_at' =>
                                                                        array (
                                                                            'from' => $params['time_period']['fromdate'],
                                                                            'include_lower' => true,
                                                                            'include_upper' => true,
                                                                            'to' => str_replace("000000", "999999", $params['time_period']['enddate']),
                                                                        ),
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                            1 =>
                                array (
                                    'bool' =>
                                        array (
                                            'filter' =>
                                                array (
                                                    0 =>
                                                        array (
                                                            'term' =>
                                                                array (
                                                                    'transaction_type' =>
                                                                        array (
                                                                            'value' => 'dummy_for_admin',
                                                                        ),
                                                                ),
                                                        ),
                                                ),
                                        ),
                                ),
                        ),
                ),
        );

        /*$checkRollId = @$params['owner_id'];
        if(empty(@$params['owner_id'])){
            $checkRollId = @$params['agentId'];
        }
        if(!empty($checkRollId)) {
            $roles = getRolesDetails($checkRollId);
            if (!in_array('owner', $roles) && !in_array('sub-admin', $roles)) {
                if (@$params['owner_id'])
                    $filter [] = ['term' => ['player_details.parent_chain_ids' => ['value' => $params['owner_id']]]];

                if (@$params['agentId'])
                    $filter [] = ['term' => ['player_details.parent_chain_ids' => ['value' => $params['agentId']]]];
            }
        }*/
        if (@$params['owner_id'] && @$params['agentId'] =='')
            $filter [] = ['term' => ['player_details.parent_chain_ids' => ['value' => $params['owner_id']]]];

        if (@$params['agentId'])
            $filter [] = ['term' => ['player_details.parent_chain_ids' => ['value' => $params['agentId']]]];


        if (@$params['provider'])
            $filter [] = ['term' => ['game_provider' => ['value' => $params['provider']]]];

        if (@$params['game_type'])
            $filter [] = ['term' => ['game_type' => ['value' => $params['game_type']]]];


        if (@$params['game_table'])
            $filter [] = ['term' => ['table_id' => ['value' => $params['game_table']]]];

        if (@$params['currency'] != '') {
            $filter [] = ['term' => ["player_details.currency" => ['value' => $params['currency']]]];
        }

        $query = [
            'bool' => [
                'must' => count($mustArry) ? $mustArry : ['match_all' => new \stdClass]
            ]
        ];

        if (count($filter)) {
            $query['bool']['filter'] = $filter;
        }
        return $query;
    }

    private function reportsKey($params)
    {
        return $reports = [
            "terms" => [
                "field" => "player_details.parent_chain_detailed",
                "script" => $this->scriptReportKey(),
                "include" => $this->agentIncludeKey($params),
                "size" => ALLCOUNT
            ],
            "aggs" => array(
                "bet" => $this->betKey(),
                "win" => $this->winKey(),
                "refund" => array(
                    "filter" => array(
                        "terms" => array(
                            "transaction_type" => [
                                "refund",
                                "refund_non_cash"
                            ]
                        )
                    ),
                    "aggs" => array(
                        "refund_amount" => $this->refundAmountKey(),
                        "refund_amount_in_EUR" => $this->winAmountInEur()
                    )
                ),
                "bonus_claim" => array(
                    "filter" => array(
                        "terms" => [
                            "transaction_type" => [
                                "deposit_bonus_claim",
                                "non_cash_bonus_claim",
                                "non_cash_granted_by_admin"
                            ]
                        ]
                    ),
                    "aggs" => array(
                        "bonus_claim_amount" => $this->bonusClaimAmount(),
                        "bonus_claim_amount_in_EUR" => $this->bonusClaimAmountInEUR()
                    )
                ),
                "bonus_withdraw" => array(
                    "filter" => array(
                        "terms" => array(
                            "transaction_type" => [
                                "non_cash_withdraw_by_admin"
                            ]
                        )
                    ),
                    "aggs" => array(
                        "bonus_withdraw_amount" => $this->betAmountBonus(),
                        "bonus_withdraw_amount_in_EUR" => $this->betAmountEur()
                    )
                ),
                "bet_after_refund" => array(
                    "bucket_script" => array(
                        "buckets_path" => $this->bucketPathAfterRefundKey(),
                        "script" => $this->bucketScriptKey()
                    )
                ),
                "bet_after_refund_in_EUR" => array(
                    "bucket_script" => array(
                        "buckets_path" => $this->bucketPathAfterRefundEurKey(),
                        "script" => $this->bucketScriptKey()
                    )
                ),
                "ggr" => array(
                    "bucket_script" => array(
                        "buckets_path" => $this->bucketPathGgrKey(),
                        "script" => $this->bucketPathGgrScriptKey()
                    )
                ),
                "ggr_in_EUR" => array(
                    "bucket_script" => array(
                        "buckets_path" => $this->bucketPathGgrEurKey(),
                        "script" => $this->bucketPathGgrScriptKey()
                    )
                ),
                "final_bonus" => array(
                    "bucket_script" => array(
                        "buckets_path" => $this->bucketPathFinalBonusKey(),
                        "script" => $this->bucketPathFinalBonusScriptKey()
                    )
                ),
                "final_bonus_in_EUR" => array(
                    "bucket_script" => array(
                        "buckets_path" => $this->bucketPathFinalBonusEurKey(),
                        "script" => $this->bucketPathFinalBonusScriptKey()
                    )
                ),
                "ngr" => array(
                    "bucket_script" => array(
                        "buckets_path" => $this->bucketPathNgrKey(),
                        "script" => $this->bucketPathNgrScriptKey()
                    )
                ),
                "ngr_in_EUR" => array(
                    "bucket_script" => array(
                        "buckets_path" => $this->bucketPathNgrEurKey(),
                        "script" => $this->bucketPathNgrScriptKey()
                    )
                ),
                "deposit" => $this->depositKey(),
                "withdraw" => $this->withdrawKey()
            )
        ];


        return $reports;
    }

    private function scriptReportKey()
    {
        return '
    String[] fields = _value.splitOnToken(\'-\');
    if(fields[1].contains(\'admin\') && fields[0].contains(\'2\')){
      fields[3] = \'000.00\';
    }
    fields[1].toLowerCase() + \'-\' + fields[0] + \'-\' + fields[1] + \'-\'
    + fields[2] + \'-\' + fields[2].toLowerCase() + \'-\' + fields[3]
  ';
    }

    private function agentIncludeKey($params)
    {
        $parent_id=@$params['agentId'];
        $agent_type = null;
        $authUser = Auth::User();
        if ($authUser->parent_type != 'AdminUser' && @$params['agentId']=='') {
          $parent_id=(@$params['owner_id'] != '' ?$params['owner_id'] : 0);
        }
        $AgentList = Admin::on('read_db')
            ->where('parent_type','AdminUser')
            ->select(DB::raw("CONCAT(lower(agent_name),'-',id,'.*')"));
        if($parent_id){
            $AgentList = $AgentList->whereRaw("(parent_id = {$parent_id} or id = {$parent_id} )");
        }

        if(@$params['agent_type']!== ''){
            $AgentList = $AgentList->where('agent_type',(int)@$params['agent_type']);
        }
        $AgentList = $AgentList->whereRaw(' id in (SELECT admin_user_id FROM "admin_users_admin_roles" WHERE "admin_role_id" = 2) ');
        $AgentList = $AgentList->pluck('concat')->toArray();
        
        $AgentList1 = Admin::on('read_db')
            ->where('parent_type','AdminUser')
            ->select(DB::raw("CONCAT(lower(agent_name),'-',id,'.*')"));
        
        if(@$params['agent_type']!== ''){
            $AgentList1 = $AgentList1->where('agent_type',(int)@$params['agent_type']);
        }

        if($parent_id){
            $AgentList1 = $AgentList1->whereRaw("(parent_id = {$parent_id} or id ={$parent_id})");
        }
        $AgentList1 = $AgentList1->whereRaw(' id in (SELECT admin_user_id FROM "admin_users_admin_roles" WHERE "admin_role_id" = 2) ');
        $AgentList1 = $AgentList1->pluck('concat')->toArray();

        if(count($AgentList)) {
            $AgentList=array_merge($AgentList,$AgentList1,$AgentList1);
            return implode('|', $AgentList);
        }else{
            return $AgentList1;
        }
    }

    private function betKey()
    {
        return ["filter" => [
            "terms" => [
                "transaction_type" => [
                    "bet",
                    "bet_non_cash"
                ]]],
            "aggs" => [
                "bet_amount" => $this->betAmount(),
                "bet_amount_in_EUR" => $this->betAmountEur()
            ]
        ];
    }

    private function betAmount()
    {
        return ["sum" => ["field" => "amount"]];
    }

    private function betAmountEur()
    {
        return ["sum" => ["field" => "amount_in_currencies.EUR"]];
    }

    private function betAmountBonus()
    {
        return ["sum" => ["field" => "amount"]];
    }




    private function winKey()
    {
        return [
            "filter" => [
                "terms" => [
                    "transaction_type" => [
                        "win",
                        "win_non_cash"
                    ]
                ]
            ],
            "aggs" => [
                "win_amount" => $this->winAmount(),
                "win_amount_in_EUR" => $this->winAmountInEur()
            ]
        ];
    }

    private function winAmount()
    {
        return [
            "sum" => [
                "field" => "amount"
            ]
        ];
    }

    private function winAmountInEur()
    {
        return [
            "sum" => [
                "field" => "amount_in_currencies.EUR"
            ]
        ];
    }

    private function refundAmountKey()
    {
        return ["sum" => ["field" => "amount"]];
    }

    private function bonusClaimAmount()
    {

        return [
            "sum" => [
                "field" => "amount"
            ]
        ];


    }
    private function bonusClaimAmountInEUR()
    {
        return [
            "sum" => [
                "field" => "amount_in_currencies.EUR"
            ]
        ];
    }

    private function bucketPathAfterRefundKey()
    {

        return [
            "bet" => "bet>bet_amount",
            "refund" => "refund>refund_amount"
        ];
    }

    private function bucketScriptKey()
    {

        return ("params.bet - params.refund");
    }

    private function bucketPathAfterRefundEurKey()
    {

        return [
            "bet" => "bet>bet_amount_in_EUR",
            "refund" => "refund>refund_amount_in_EUR"
        ];
    }

    private function bucketPathGgrKey()
    {
        return [
            "bet_after_refund" => "bet_after_refund",
            "win" => "win>win_amount"
        ];
    }

    private function bucketPathGgrScriptKey()
    {

        return ("params.bet_after_refund - params.win");

    }

    private function bucketPathGgrEurKey()
    {
        return [
            "bet_after_refund" => "bet_after_refund_in_EUR",
            "win" => "win>win_amount_in_EUR"
        ];
    }

    private function bucketPathFinalBonusKey()
    {
        return [
            "bonus_claim" => "bonus_claim>bonus_claim_amount",
            "bonus_withdraw" => "bonus_withdraw>bonus_withdraw_amount"
        ];

    }

    private function bucketPathFinalBonusScriptKey()
    {
        return ("params.bonus_claim - params.bonus_withdraw");
    }

    private function bucketPathFinalBonusEurKey()
    {
        return [
            "bonus_claim" => "bonus_claim>bonus_claim_amount_in_EUR",
            "bonus_withdraw" => "bonus_withdraw>bonus_withdraw_amount_in_EUR"
        ];
    }

    private function bucketPathNgrKey()
    {
        return [
            "final_bonus" => "final_bonus",
            "ggr" => "ggr"
        ];
    }

    private function bucketPathNgrScriptKey()
    {
        return ("params.ggr - params.final_bonus");
    }

    private function bucketPathNgrEurKey()
    {
        return [
            "final_bonus" => "final_bonus_in_EUR",
            "ggr" => "ggr_in_EUR"
        ];
    }

    private function sortKey($sortBy, $sortOrder = 'ASC')
    {
        return [$sortBy => $sortOrder];
    }

    private function withdrawKey()
    {
        return [
            "filter" => 
                [
                    "bool" => [
                        "must" => [
                            [
                                "terms" => [
                                    "transaction_type" => ["withdraw", "non_cash_withdraw_by_admin"]
                                ]
                            ],
                            [
                                "term" => [
                                    "status" => "success"
                                ]
                            ]
                        ],
                        "must_not" => [
                            "terms" => [
                                "description" => [
                                    "cancelled by admin", "cancelled by the player","Pending confirmation from admin"
                                ]
                            ]
                        ]
                    ]
                ],
            "aggs" => [
                "withdraw_amount" =>[
                    "sum" => [
                        "field" => "amount"
                    ]
                ]
            ]        
        ];
    }

    private function depositKey(){
      return array(
        "filter" => array(
            "terms" => array(
                "transaction_type" => ["deposit","non_cash_granted_by_admin"],
            )
        ),
        "aggs" => array(
            "deposit_amount" => array(
                "sum" => array(
                    "field" => "amount"
                )
            )
        )
      );
    }
}

