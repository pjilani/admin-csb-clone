<?php

namespace App\Services;

use App\Models\TenantCredentials;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class EzugiGameDetailsService
{
    public static function ezugiRoundInfo($request){
        $authUser = Auth::User();

            $accessKeyArray = TenantCredentials::on('read_db')->select('key', 'value')
            ->where('tenant_id', @$request['tenant_id'] ?? $authUser->tenant_id)
            ->whereRAW("key in ('APP_EZUGI_API_ACCESS_TOKEN','APP_EZUGI_API_ID','APP_EZUGI_API_USERNAME','APP_EZUGI_OPERATOR_ID','APP_EZUGI_GAME_DETAILS_URL')")
            ->pluck('value', 'key')->toArray();

            $accessKey = $accessKeyArray['APP_EZUGI_API_ACCESS_TOKEN'];
            $buildQueryArray['DataSet'] = "user_round_details";
            $buildQueryArray['APIID'] = $apiId = $accessKeyArray['APP_EZUGI_API_ID'];
            $buildQueryArray['APIUser'] = $apiUser = $accessKeyArray['APP_EZUGI_API_USERNAME'];
            $buildQueryArray['OperatorID'] = $operatorId = $accessKeyArray['APP_EZUGI_OPERATOR_ID'];
            $buildQueryArray['RoundID'] = $request['RoundID'];
            $buildQueryArray['TransactionID'] = $request['TransactionID'];
            $buildQueryArray['UID'] = $request['UID'];
            $buildQueryArray['language'] = 'en';

            if (empty($accessKey) || empty($apiId) || empty($apiUser) || empty($operatorId)) {
                return returnResponse(false, ERROR_MESSAGE, [], Response::HTTP_EXPECTATION_FAILED);
            }
            $params = $accessKey . http_build_query($buildQueryArray);
            $token = hash('sha256', $params);

            //Start get details

                    $curlParam=$buildQueryArray;
                    $curlParam['RequestToken'] = $token;
                    $curl = curl_init();

                    curl_setopt_array($curl, array(
                        CURLOPT_URL => $accessKeyArray['APP_EZUGI_GAME_DETAILS_URL'] ?? 'https://boint.tableslive.com/api/get/',
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => '',
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'POST',
//                        CURLOPT_POSTFIELDS => 'DataSet=user_round_details&APIID=10427002-7937da43&APIUser=gammastack2&OperatorID=10427002&RoundID=35537524&TransactionID=c3001cb6-9ac5-40da-a957-9161dc64af4f&UID=96&language=en&RequestToken=27fff23e90699fbdf3d717e1d85aa2e91f5642e3bf7cc11735b8d1587d4bd3f7',
                        CURLOPT_POSTFIELDS => http_build_query($curlParam),
                        CURLOPT_HTTPHEADER => array(
                            'Content-Type: application/x-www-form-urlencoded'
                        ),
                    ));

                    $response = curl_exec($curl);

                    curl_close($curl);

                    return @json_decode($response);
    }
}
