<?php

namespace App\Services;

use Elasticsearch\ClientBuilder;

class PlayerFinancialReportService
{


    public static function fetchResult($params)
    {
        
        $obj = new self();
        $body = [
            'query' => $obj->queryKey($params),
            'sort' => $obj->sortKey($params['sortBy'], $params['sortOrder']),
            'timeout' => '60s',
            'track_total_hits' => true,
            'aggs' => [
                'total_deposit' => $obj->totalDepositKey($params),
                'total_withdraw' => $obj->totalWithdrawKey($params),
                'total_withdraw_cancel' => $obj->totalWithdrawCancel($params)

            ]
        ];

        if ($params['limit'] != 'all') {
            $body['size'] = (int)$params['limit'];
            $body['from'] = (int)$params['offset'];
        } else {
            $body['size'] = ALLCOUNT;
        }
        $params = [
            'index' => ELASTICSEARCH_INDEX['transactions'],
            'body' => $body
        ];
        $client = getEsClientObj();
        $return = $client->search($params);
        return $return;
    }


    private function totalDepositKey($params)
    {
        if (@$params['action_type'] === 'deposit_cash_admin') {
            $filterDepositKey = [
                "bool" => [
                    "must" => [
                        [
                            "terms" => [
                                "actionee.type" => [
                                    "AdminUser"
                                ]
                            ]
                        ],
                        [
                            "term" => [
                                "transfer_method" => "manual"
                            ]
                        ],
                        [
                            "term" => [
                                "transaction_type" => "deposit"
                            ]
                        ]
                    ],
                    "must_not" => [
                        "terms" => [
                            "description" => [
                                "Deposit Request Approved"
                            ]
                        ]
                    ]
                ]
            ];

        } elseif (@$params['action_type'] === 'deposit_non_cash_admin') {
            $filterDepositKey = [
                "term" => [
                    "transaction_type" => "non_cash_granted_by_admin"
                ]
            ];

        } elseif (@$params['action_type'] === 'deposit_manual_user') {
            $filterDepositKey = [
                "bool" => [
                    "filter" => [
                        [
                            "terms" => [
                                "description" => [
                                    "Deposit Request Approved"
                                ]
                            ]
                        ],
                        [
                            "term" => [
                                "transaction_type" => "deposit"
                            ]
                        ],
                        [
                            "term" => [
                                "transfer_method" => "manual"
                            ]
                        ]
                    ]
                ]
            ];
        } elseif (@$params['action_type'] === 'deposit_gateway_user') {
            $filterDepositKey = [
                "bool" => [
                    "filter" => [
                        [
                            "terms" => [
                                "description" => [
                                    "Deposit Request"
                                ]
                            ]
                        ],
                        [
                            "term" => [
                                "transaction_type" => "deposit"
                            ]
                        ]
                    ]
                ]
            ];

        } else {
            $filterDepositKey = [
                "terms" => [
                    "transaction_type" => [
                        "deposit", "non_cash_granted_by_admin"
                    ]
                ]
            ];
        }


        $totalDeposit = [
            "filter" => $filterDepositKey,
            "aggs" => [
                "total" => $this->totalAmountKey($params)
            ]
        ];
        return $totalDeposit;
    }


    private function totalWithdrawKey($params)
    {
        if (@$params['action_type'] === 'withdraw_gateway_user') {
            $totalWithdraw = [
                "bool" => [
                    "filter" => [
                        [
                            "terms" => [
                                "description" => ["Approved By Payment gateway"]
                            ]
                        ],
                        [
                            "term" => [
                                "transaction_type" => "withdraw"
                            ]
                        ]
                    ]
                ]
            ];

        } elseif (@$params['action_type'] === 'withdraw_cash_manual_user') {
            $totalWithdraw = [
                "bool" => [
                    "filter" => [
                        [
                            "terms" => [
                                "description" => [
                                    "approved by admin"
                                ]
                            ]
                        ],
                        [
                            "term" => [
                                "transaction_type" => "withdraw"
                            ]
                        ]
                    ]
                ]
            ];

        } elseif (@$params['action_type'] === 'withdraw_non_cash_admin') {
            $totalWithdraw = [
                "term" => [
                    "transaction_type" => "non_cash_withdraw_by_admin"
                ]
            ];
        } elseif (@$params['action_type'] === 'withdraw_cash_admin') {
            $totalWithdraw = [
                "bool" => [
                    "must" => [
                        [
                            "terms" => [
                                "actionee.type" => [
                                    "AdminUser"
                                ]
                            ]
                        ],
                        [
                            "term" => [
                                "transaction_type" => "withdraw"
                            ]
                        ],
                        [
                            "term" => [
                                "status" => "success"
                            ]
                        ]
                    ],
                    "must_not" => [
                        "terms" => [
                            "description" => [
                                "approved by admin", "cancelled by admin", "Approved By Payment gateway"
                            ]
                        ]
                    ]
                ]
            ];

        }  else {
            $totalWithdraw = [
                "bool" => [
                    "must" => [
                        [
                            "terms" => [
                                "transaction_type" => ["withdraw", "non_cash_withdraw_by_admin"]
                            ]
                        ],
                        [
                            "term" => [
                                "status" => "success"
                            ]
                        ]
                    ],
                    "must_not" => [
                        "terms" => [
                            "description" => [
                                "cancelled by admin","cancelled by the player","Pending confirmation from admin"
                            ]
                        ]
                    ]
                ]
            ];
        }

        $total_withdraw = [
                "filter" => $totalWithdraw,
                "aggs" => ["total" => $this->totalAmountKey($params)]
            ];
        return $total_withdraw;

    }


    private function totalWithdrawCancel($params)
    {

        $total_withdraw_cancel = ["filter" => ["term" => ["transaction_type" => "withdraw_cancel"]],
            "aggs" => ["total" => $this->totalAmountKey($params)]

        ];

        return $total_withdraw_cancel;

    }

    private function totalAmountKey($params)
    {
       return ["sum" => ["field" => "amount_in_currencies.".$params['tenant_base_currency']]];

    }

    private function sortKey($sortBy, $sortOrder = 'ASC')
    {
        return [$sortBy => $sortOrder];
    }


    private function queryKey($params)
    {
       
        $mustArry = [];
        $filter = [];
        if ($params['tenant_id'] != '')
            $filter [] = ['term' => ['tenant_id' => ['value' => $params['tenant_id']]]];

        $filter [] = ["bool" =>
            ["must_not" => [
                "bool" => [
                    "must_not" => [
                        "exists" => [
                            "field" => "player_details"
                        ]
                    ]
                ]
            ]
            ]
        ];
        if (@$params['action_type'] === 'deposit_cash_admin') {
            $filter [] = [
                "bool" => [
                    "must" => [
                        [
                            "terms" => [
                                "actionee.type" => [
                                    "AdminUser"
                                ]
                            ]
                        ],
                        [
                            "term" => [
                                "transfer_method" => "manual"
                            ]
                        ],
                        [
                            "term" => [
                                "transaction_type" => "deposit"
                            ]
                        ]
                    ],
                    "must_not" => [
                        "terms" => [
                            "description" => [
                                "Deposit Request Approved"
                            ]
                        ]
                    ]
                ]
            ];

        } elseif (@$params['action_type'] === 'deposit_non_cash_admin') {
            $filter [] = [
                "term" => [
                    "transaction_type" => "non_cash_granted_by_admin"
                ]
            ];

        } elseif (@$params['action_type'] === 'deposit_manual_user') {
            $filter [] = [
                "bool" => [
                    "filter" => [
                        [
                            "terms" => [
                                "description" => [
                                    "Deposit Request Approved"
                                ]
                            ]
                        ],
                        [
                            "term" => [
                                "transaction_type" => "deposit"
                            ]
                        ],
                        [
                            "term" => [
                                "transfer_method" => "manual"
                            ]
                        ]
                    ]
                ]
            ];
        } elseif (@$params['action_type'] === 'deposit_gateway_user') {
            $filter [] = [
                "bool" => [
                    "filter" => [
                        [
                            "terms" => [
                                "description" => [
                                    "Deposit Request"
                                ]
                            ]
                        ],
                        [
                            "term" => [
                                "transaction_type" => "deposit"
                            ]
                        ]
                    ]
                ]
            ];

        } elseif (@$params['action_type'] === 'withdraw_gateway_user') {
            $filter [] = [
                "bool" => [
                    "filter" => [
                        [
                            "terms" => [
                                "description" => ["Approved By Payment gateway"]
                            ]
                        ],
                        [
                            "term" => [
                                "transaction_type" => "withdraw"
                            ]
                        ]
                    ]
                ]
            ];

        } elseif (@$params['action_type'] === 'withdraw_cash_manual_user') {
            $filter [] =$query['filter'] = [
                "bool" => [
                    "filter" => [
                        [
                            "terms" => [
                                "description" => [
                                    "approved by admin"
                                ]
                            ]
                        ],
                        [
                            "term" => [
                                "transaction_type" => "withdraw"
                            ]
                        ]
                    ]
                ]
            ];

        } elseif (@$params['action_type'] === 'withdraw_non_cash_admin') {
            $filter [] = [
                "term" => [
                    "transaction_type" => "non_cash_withdraw_by_admin"
                ]
            ];
        } elseif (@$params['action_type'] === 'withdraw_cash_admin') {
            $filter [] = [
                "bool" => [
                    "must" => [
                        [
                            "terms" => [
                                "actionee.type" => [
                                    "AdminUser"
                                ]
                            ]
                        ],
                        [
                            "term" => [
                                "transaction_type" => "withdraw"
                            ]
                        ],
                        [
                            "term" => [
                                "status" => "success"
                            ]
                        ]
                    ],
                    "must_not" => [
                        "terms" => [
                            "description" => [
                                "approved by admin", "cancelled by admin","Approved By Payment gateway"
                            ]
                        ]
                    ]
                ]
            ];

        } elseif (@$params['action_type'] == 'deposit') {
            $filter [] = [
                'terms' =>
                    [
                        'transaction_type' => [
                            "deposit",
                            "non_cash_granted_by_admin",
                        ]
                    ]
            ];

        } elseif (@$params['action_type'] == 'withdraw') {

            $filter [] = [
                "bool" => [
                    "must" => [

                        [
                            "terms" => [
                                "transaction_type" => ["withdraw", "non_cash_withdraw_by_admin"]
                            ]
                        ],
                        [
                            "term" => [
                                "status" => "success"
                            ]
                        ]
                    ],
                    "must_not" => [
                        "terms" => [
                            "description" => [
                                "Pending confirmation from admin", "cancelled by admin"
                            ]
                        ]
                    ]
                ]
            ];

        } elseif (@$params['action_type'] != '') {
            $filter [] = ['term' => ['transaction_type' => $params['action_type']]];

        } else {
            $filter [] = [
                'terms' =>
                    [
                        'transaction_type' => [
                            "deposit",
                            "withdraw",
                            "withdraw_cancel",
                            "deposit_bonus_claim",
                            "non_cash_granted_by_admin",
                            "non_cash_withdraw_by_admin"
                        ]
                    ]
            ];

        }

        if(@$params['isDirectPlayer'] =='direct' && @$params['agent_id'] && @$params['agentIdTop'] !=$params['agent_id'] ) {
            $filter []=  [
                "term" => [
                    "parent_id" => [
                        "value" => $params['agent_id']
                    ]
                ]
            ];
        }

        if (@$params['owner_id'] && @$params['agentIdTop'] =='' ) {
            $filter []=  [
                "term" => [
                    "player_details.parent_chain_ids" => [
                        "value" => $params['owner_id']
                    ]
                ]
            ];
        }elseif (@$params['agentIdTop'] ) {
            $filter []=  [
                "term" => [
                    "player_details.parent_chain_ids" => [
                        "value" => $params['agentIdTop']
                    ]
                ]
            ];
        }

        if(@$params['status']!='')
        {
            $filter [] = ['term' => ['status' => $params['status']]];

        }




        if (is_array($params['time_period']) &&
            $params['time_period']['fromdate'] != '' &&
            $params['time_period']['enddate'] != ''
        ) {

            $filter [] = [
                "range" => [
                    "created_at" => [
                        "from" =>$params['time_period']['fromdate'],
                        "include_lower"=> true,
                        "to" => str_replace("000000", "999999", $params['time_period']['enddate']),
                        "include_upper"=> true
                    ]
                ]
            ];
        }

        if(@$params['currency']!='') {
            $mustArry["multi_match"] = [
                "query" => $params['currency'],
                "fields" => [
                    "player_details.currency",
//                "source_currency",
//                "target_currency"
                ],
                "type" => "best_fields",
                "operator" => "OR"
            ];

        }



        if ($params['searchKeyword'] != '') {

            $filter [] =  $this->searchWordQuery($params['searchKeyword']);
        }


        $query = [
            'bool' => [
                'must' => count($mustArry) ? $mustArry : ['match_all' => new \stdClass]
            ]
        ];
        if (count($filter)) {
            $query['bool']['filter'] = $filter;
        }
        return $query;
    }

    private function searchWordQuery($str)
    {

        $result = array(
            "bool" => array(
                "should" => array(
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "player_details.player_id_s.word_start" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "player_details.player_id_s.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "player_details.player_name.word_start" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "player_details.player_name.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "player_details.agent_name.word_start" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "player_details.agent_name.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "transaction_id.word_start" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "transaction_id.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                    ,
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "player_details.phone.word_start" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "player_details.phone.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                )
            )
        );


        return $result;
    }

    
}
