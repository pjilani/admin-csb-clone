<?php

namespace App\Services;

use App\Models\TenantConfigurations;
use App\Models\Currencies;
use App\Models\Transactions;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use App\Services\SportsService;
use App\Models\Admin;
use App\Models\Wallets;

class TransactionsService
{

    public static function admins() {
        $allAdmins = Admin::with('wallets:currency_id,owner_id', 'wallets.currency:id,code')
            ->select(['id', 'first_name', 'last_name', 'agent_name', 'email', 'parent_id', 'tenant_id', 'created_at'])
            ->get();
        $admins = array();

        $c = 0;
        foreach($allAdmins as $key => $admin) {
            if(!empty($admin->wallets) && count($admin->wallets) > 0) {
                foreach($admin->wallets as $wkey => $wallet) {
                    $param['tenant_id'] = $admin->tenant_id;
                    $param['id'] = $admin->id;
                    $param['currency'] = @$wallet->currency->code;
                    $userParentIds = User::getAdminParentDetails($param);
                    $admins[$c]['tenant_id'] = $admin->tenant_id;
                    $admins[$c]['created_at'] = getTimeZoneWiseTimeISO($admin->created_at);
                    $admins[$c]['transaction_type'] = 'dummy_for_admin';

                    $admins[$c]['player_details'] = [
                        'agent_full_name' => $admin->first_name.' '.$admin->last_name,
                        'agent_name' => $admin->agent_name,
                        'agent_email' => $admin->email,
                        'currency' => $wallet->currency->code,
                        'demo' => false,
                        'parent_id' => $admin->id,
                        'parent_chain_ids'=>$userParentIds['parentChainIdsArray']??[],
                        'parent_chain_detailed'=>$userParentIds['parentChainDetailsArray']??[]
                    ];

                    $c++;
                }
            }
        }
        return $admins;
    }

    /**
     * @description getUserWalletDetails
     * @param $wallet_id
     * @return array
     */
    public static function getUserWalletDetails($wallet_id){
        $sqlCheck = "SELECT 
                        w.*,u.id as user_id,u.tenant_id
                        FROM wallets as w
                        LEFT JOIN users as u on u.id = w.owner_id and owner_type='User'
                        where w.id = ?;                      
                        ";

        $targetWallet = DB::select($sqlCheck, [$wallet_id]);
        $targetWallet = @$targetWallet[0];
        return $targetWallet;
    }

    public static function bulkNoCashTransactionHistory($wallet_id, $non_cash_amount, $type, $request)
    {
        $non_cash_amount = (float) $non_cash_amount;
        $targetWallet = self::getUserWalletDetails($wallet_id);
        if($type==5 || $type==19) {
            $target_before_balance = ((float)$targetWallet->non_cash_amount - (float)$non_cash_amount);
        } else {
            $target_before_balance = ((float)$targetWallet->non_cash_amount + (float)$non_cash_amount);
        }
        $adminData = Admin::where('id', $request->actionee_id)->select('email', 'first_name', 'last_name', 'agent_name')->first();
        $getExchangeRate = Currencies::where('id',$targetWallet->currency_id)->first();
        $insertData = [
            'actionee_type' => $request->actionee_type,
            'actionee_id' => $request->actionee_id,
            'status' => 'success',
            // 'conversion_rate' => $getExchangeRate->exchange_rate,
            'amount'=> (float) $non_cash_amount,
            'tenant_id' => $request->tenant_id,
            'created_at' => @date('Y-m-d H:i:s'),
            'updated_at' => @date('Y-m-d H:i:s'),
            // 'comments' => ($type == 5 || $type==19) ? 'bulk deposit' : 'bulk withdraw',
            'comments' => ($type == 5) ? 'bulk deposit' : (($type == 19) ? 'Non cash deposited from player commission' : (($type == 6) ? 'bulk withdraw' : '')),
            // 'comments' => 'Non cash bonus '.($type==5?'deposited':'withdrawn').' by '.$adminData->agent_name??$adminData->first_name.' '.$adminData->last_name.'('.$adminData->email.')	',
            'transaction_type' => $type,
            'transaction_id' => getUUIDTransactionId(),
            'payment_method' => 'manual'
        ];

        if($type==5 || $type==19) {
            $insertData['target_currency_id'] = $targetWallet->currency_id;
            $insertData['target_wallet_id'] = $wallet_id;
            $insertData['target_before_balance'] = ($target_before_balance ? $target_before_balance : 0);
            $insertData['target_after_balance'] = (float)$targetWallet->non_cash_amount;
        }else{
            $insertData['source_currency_id'] = $targetWallet->currency_id;
            $insertData['source_wallet_id'] = $wallet_id;
            $insertData['source_before_balance'] = ($target_before_balance ? $target_before_balance : 0);
            $insertData['source_after_balance'] = (float)$targetWallet->non_cash_amount;
        }
//  =======================================Add Other Currency Amount =======================================
$configurations = TenantConfigurations::select('allowed_currencies')->where('tenant_id', $request->tenant_id)->get();
$configurations = $configurations->toArray();
$CurrenciesValue=[];
if ($configurations) {
    $configurations = $configurations[0];
    if (strlen($configurations['allowed_currencies']) > 1) {
        $configurations = str_replace('{', '', $configurations['allowed_currencies']);
        $configurations = str_replace('}', '', $configurations);
        $configurations = explode(',', $configurations);

        if (@$configurations[0] && count($configurations) > 0) {
            $CurrenciesValue = Currencies::select('code')->whereIn('id', $configurations)->pluck('code');
            $CurrenciesValue = $CurrenciesValue->toArray();
        }
    }
}
$currenciesTempArray = $insertData['other_currency_amount'] = [];
$exMainTransactionCurrecy = Currencies::select('code')->where('id', $targetWallet->currency_id)->pluck('code');
$exMainTransactionCurrecy = $exMainTransactionCurrecy->toArray();;
$amount_currency_ex = Currencies:: getExchangeRateCurrency($exMainTransactionCurrecy);
foreach ($CurrenciesValue as $key => $value) {
    if($exMainTransactionCurrecy != $value){
        $getExchangeRateCurrency = Currencies:: getExchangeRateCurrency($value);
        $currenciesTempArray[$value] = (float)number_format($non_cash_amount * ($getExchangeRateCurrency / $amount_currency_ex ),4,".","");
        // $currenciesTempArray[$value] = (float)number_format($non_cash_amount * ($amount_currency_ex / $getExchangeRateCurrency),4,".","");
    }else{
        $currenciesTempArray[$value] = $non_cash_amount;
    }
}
$insertData['other_currency_amount'] = json_encode($currenciesTempArray);
//  =======================================Add Other Currency Amount ENd =======================================
        $createdValues = Transactions::create($insertData);
        return $createdValues;
    }


    /**
     * @description noCashTransactionHistory
     * @param $wallet_id
     * @param $non_cash_amount
     * @param $type
     * @return mixed
     */
    public static function noCashTransactionHistory($wallet_id, $non_cash_amount, $type)
    {
        $non_cash_amount = (float) $non_cash_amount;
        $targetWallet = self::getUserWalletDetails($wallet_id);
        if($type==5) {
            $target_before_balance = ((float)$targetWallet->non_cash_amount - (float)$non_cash_amount);
        } else {
            $target_before_balance = ((float)$targetWallet->non_cash_amount + (float)$non_cash_amount);
        }
        //$getExchangeRate = Currencies::where('id',$targetWallet->currency_id)->first();
        $insertData = [
            'actionee_type' => @Auth::user()->parent_type,
            'actionee_id' => @Auth::user()->id,
            'status' => 'success',
            // 'conversion_rate' => $getExchangeRate->exchange_rate,
            'amount'=> (float) $non_cash_amount,
            'tenant_id' => $targetWallet->tenant_id,
            'created_at' => @date('Y-m-d H:i:s'),
            'updated_at' => @date('Y-m-d H:i:s'),
            'comments' => 'Non cash bonus '.($type==5?'deposited':'withdrawn').' by '.@Auth::user()->agent_name??@Auth::user()->first_name.' '.@Auth::user()->last_name.'('.@Auth::user()->email.')	',
            'transaction_type' => $type,
            'transaction_id' => getUUIDTransactionId(),
            'payment_method' => 'manual'
        ];

        if($type==5) {
            $insertData['target_currency_id'] = $targetWallet->currency_id;
            $insertData['target_wallet_id'] = $wallet_id;
            $insertData['target_before_balance'] = ($target_before_balance ? $target_before_balance : 0);
            $insertData['target_after_balance'] = (float)$targetWallet->non_cash_amount;
        }else{
            $insertData['source_currency_id'] = $targetWallet->currency_id;
            $insertData['source_wallet_id'] = $wallet_id;
            $insertData['source_before_balance'] = ($target_before_balance ? $target_before_balance : 0);
            $insertData['source_after_balance'] = (float)$targetWallet->non_cash_amount;
        }
//  =======================================Add Other Currency Amount =======================================
        $configurations = TenantConfigurations::select('allowed_currencies')->where('tenant_id', $targetWallet->tenant_id)->get();
        $configurations = $configurations->toArray();
        $CurrenciesValue=[];
        if ($configurations) {
            $configurations = $configurations[0];
            if (strlen($configurations['allowed_currencies']) > 1) {
                $configurations = str_replace('{', '', $configurations['allowed_currencies']);
                $configurations = str_replace('}', '', $configurations);
                $configurations = explode(',', $configurations);

                if (@$configurations[0] && count($configurations) > 0) {
                    $CurrenciesValue = Currencies::select('code')->whereIn('id', $configurations)->pluck('code');
                    $CurrenciesValue = $CurrenciesValue->toArray();
                }
            }
        }
        $currenciesTempArray = $insertData['other_currency_amount'] = [];
        $exMainTransactionCurrecy = Currencies::select('code')->where('id', $targetWallet->currency_id)->pluck('code');
        $exMainTransactionCurrecy = $exMainTransactionCurrecy->toArray();;
        $amount_currency_ex = Currencies:: getExchangeRateCurrency($exMainTransactionCurrecy);
        foreach ($CurrenciesValue as $key => $value) {
            if($exMainTransactionCurrecy != $value){
                $getExchangeRateCurrency = Currencies:: getExchangeRateCurrency($value);
                $currenciesTempArray[$value] = (float)number_format($non_cash_amount * ($getExchangeRateCurrency / $amount_currency_ex ),4,".","");
                // $currenciesTempArray[$value] = (float)number_format($non_cash_amount * ($amount_currency_ex / $getExchangeRateCurrency),4,".","");
            }else{
                $currenciesTempArray[$value] = $non_cash_amount;
            }
        }
        $insertData['other_currency_amount'] = json_encode($currenciesTempArray);
//  =======================================Add Other Currency Amount ENd =======================================
        $createdValues = Transactions::create($insertData);
        return $createdValues;
    }

    /**
     * @description reIndex
     */
    public static function reIndex()
    {
        $errors = [];
        DB::table('transactions')->select('id')->orderBy('id')
            ->chunkById(100, function ($transactions, $errors) {
                foreach ($transactions as $transaction) {
                    try {
                        Transactions::storeElasticSearchData($transaction->id);
                    } catch (Exception $e) {
                        $errors[] = ['transaction_id' => $transaction->id, 'error' => $e->getMessage()];
                    }
                }
            });
        echo "Admin dummy_for_admin Start Reindexing";
      
        $admins = self::admins();
        foreach ($admins as $admins) {
            try {
                $data=$admins;
                $params = [
                    'body' => $data,
                    'index' => ELASTICSEARCH_INDEX['transactions'],
                    'type' => ELASTICSEARCH_TYPE,
                    'id' => "dummy_for_admin_".@$admins['player_details']['parent_id']."_".@$admins['player_details']['currency']
                ];
                $client = getEsClientObj();
                $client->index($params);

            } catch (Exception $e) {
                $errors["dummy_for_admin_".@$admins['player_details']['parent_id']] =
                    ['adminID' => @$admins['player_details']['currency'], 'error' => $e->getMessage()];
            }
        }

        return $errors;

    }

    public static function reIndexAdminDummyTransaction()
    {
        echo "Admin dummy_for_admin Start Reindexing";
        $errors = [];
        $admins = self::admins();
        foreach ($admins as $admins) {
            try {
                $data=$admins;
                $params = [
                    'body' => $data,
                    'index' => ELASTICSEARCH_INDEX['transactions'],
                    'type' => ELASTICSEARCH_TYPE,
                    'id' => "dummy_for_admin_".@$admins['player_details']['parent_id']."_".@$admins['player_details']['currency']
                ];
                $client = getEsClientObj();
                $client->index($params);

            } catch (Exception $e) {
                $errors["dummy_for_admin_".@$admins['player_details']['parent_id']] =
                    ['adminID' => @$admins['player_details']['currency'], 'error' => $e->getMessage()];
            }
        }

        return $errors;

    }


    /**
     * @description reIndexAll
     */
    public static function reIndexAll($endpoint='reindex-transaction')
    {
        $url = URL::to('/')."/".$endpoint;
//        background_curl_request($url);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            /*CURLOPT_HTTPHEADER => array(
                'Cookie: XSRF-TOKEN=eyJpdiI6ImNMZFA0aEVKc1lrbEkveVZPSFVBVVE9PSIsInZhbHVlIjoiU21KYmJwVjQvbFZZY3pHRURrc00xM0VVbnBlVk9ydFJhL0xsMkErRFQ5alVaVXFDa3dScWxQN1g0djFHTEhtVDZIeDFmMUtmWkJ1R0pkL1JDSElQQkxjU2tRUDhDNFhtZFIrMHpydkhhTFd6bytUdWQzZlRaMTg2dzJNdW01L0wiLCJtYWMiOiI0NDU1OTQyN2I4OTNiZWIwYzAwZmNhZTYzMmE1MTlkMjg5MWIzMzhkMTJmODczY2ZiN2Q4Y2E2ZTY2MmU3YTdjIiwidGFnIjoiIn0%3D; admincsb_session=eyJpdiI6ImNsNFRCZUlWbFQ1Vms4RzB6SjRxeWc9PSIsInZhbHVlIjoiQ2VEL0FjVWZNM3dzSk05cmtqLzI4UmtnQVZFbUtoeHJIUXZHMzhyRkUyanZLeGN6U3ZvaEo2OWt0SFIzK0dMVEVjVHNCMjNLOUdQdHBvT0hzUlZWL3dkOGNjWWpzRE9TYTdRYXJjajA5dllDT3pZSW5jQXRqUElheTlwOXlqZVEiLCJtYWMiOiJhM2ZiNzQ0ZGVkYTE3YzU5Njg5ZGZiNDgzMjBjMzIzNjNlNTdmOTExMGI2Y2ZhZmIyNzY5YWVjNDQ4NGQxNzAwIiwidGFnIjoiIn0%3D'
            ),*/
        ));
        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }

    /**
     * @description reIndex
     */
    public static function reBetIndex()
    {
        $result=DB::table('bets_transactions')->select('id')->orderBy('id');

        $result->chunkById(100, function ($transactions, $errors) {

            foreach ($transactions as $transaction) {
                try {
                    SportsService::storeElasticBetSearchData($transaction->id);
                } catch (Exception $e) {
                    $errors[] = ['transaction_id' => $transaction->id, 'error' => $e->getMessage()];
                }
            }
        });
    }

    public static function getTransactionInfoDashboard($param=[])
    {
        $sqlIntervel="TO_CHAR(t.created_at :: DATE, 'yyyy/mm/dd') as created_at_date";
        $sqlIntervelSport="TO_CHAR(tb.created_at :: DATE, 'yyyy/mm/dd') as created_at_date";
        $fromdate = date('Y-m-d H:i:s',strtotime($param['time_period']['fromdate']));
        $enddate = date('Y-m-d H:i:s',strtotime($param['time_period']['enddate']));
        if($param['interval']=='yearly'){
            $sqlIntervel=  " TO_CHAR(t.created_at :: DATE, 'mm') as created_at_date";
            $sqlIntervelSport=  " TO_CHAR(tb.created_at :: DATE, 'mm') as created_at_date";
        }elseif ($param['interval']=='today'){
            $sqlIntervel=  " TO_CHAR(t.created_at :: timestamp, 'HH24') as created_at_date";
            $sqlIntervelSport=  " TO_CHAR(tb.created_at :: timestamp, 'HH24') as created_at_date";
            $fromdate = $param['time_period']['fromdate'];
            $enddate = $param['time_period']['enddate'];
        }

        $agent_id = 0;
        if (@$param['agent_id'] != 0 && @$param['agent_id']) {
            $agent_id =$param['agent_id'];

        } else {
            if (Auth::user()->parent_type == 'AdminUser') {
                $agent_id = Auth::user()->id;
            }
        }
        $sqlForLinkCount = "";
        $whereSQL = "";
        $whereSQLSport = "";

        if($agent_id !=0 ) {
            $roles = getRolesDetails($agent_id);
            if (!in_array('owner', $roles)) {
                $sqlForLinkCount = " SELECT id FROM users where parent_id = $agent_id and parent_type ='AdminUser'";
                $whereSQL = ' and t.actionee_id in (' . $sqlForLinkCount . ')';
                $whereSQLSport = ' and tb.user_id in (' . $sqlForLinkCount . ')';
            }
        }
        $tenantId = '';
        $tenantIdTB = '';
        if($param['tenantId']!=''){
            $tenantId = " and t.tenant_id =".$param['tenantId'];
            $tenantIdTB = " and tb.tenant_id =".$param['tenantId'];

        }

        $sql ='
                    select count(TBR.total_active) as total_active_user,created_at_date from (
            
                select DISTINCT t.actionee_id as total_active,
             '.$sqlIntervel.'
             from "transactions" as "t"
            where "t"."actionee_type" = \'User\'
            and "t"."created_at" >= '."'".$fromdate."'".' and "t"."created_at" <= '."'".$enddate."'". $whereSQL.$tenantId.'
            
            UNION

            select DISTINCT tb.user_id as total_active,
             '.$sqlIntervelSport.'
             from "bets_transactions" as "tb"
            where "tb"."created_at" >= '."'".$fromdate."'".' and "tb"."created_at" <= '."'".$enddate."'". $whereSQLSport.$tenantIdTB.'
            
            ) as TBR
            group by TBR.created_at_date;
                    ';

        $users = DB::connection('read_db')->select($sql);

        return $users;
    }

    public static function getListByES($limit, $page, $filter){
        $obj = new self();

        $body = [
            'query' => $obj->queryKeyIndex($filter),
            'sort' => $obj->sortKey($filter['sortBy'], $filter['sortOrder']),
            'timeout' => '60s',
            'track_total_hits' => true,
        ];

        $offset = ($page > 1) ? $page * $limit : 0;

        if ($page > 1) {
            $offset = $limit * ($page - 1);
        }

        if($limit!='all'){
            $body['size'] = $limit;
            $body['from'] = (int)$offset;
        }else{
            $body['size'] = ALLCOUNT;
        }

        $params = [
            'index' => ELASTICSEARCH_INDEX['transactions'],
            'body' => $body
        ];
        $client = getEsClientObj();
        return $client->search($params);
    }

    private function queryKeyIndex($params)
    {
        $mustArry = [];
        $filter = [];

        if (@$params['tenant_id'] != '')
            $filter [] = ['term' => ['tenant_id' => ['value' => $params['tenant_id']]]];

        if (@$params['round_id_s'] != '')
            $filter [] = ['term' => ['round_id_s' => ['value' => $params['round_id_s']]]];

        if(@$params['round_id']!='')
            $filter []= ['term' => ['round_id_s' => ['value' => $params['round_id_s']]]];
           
        if(@$params['transaction_id']!='')
        $filter []= ['term' => ['transaction_id' => ['value' => $params['transaction_id']]]];

        if (@$params['action_type'] != '') {
            $filter [] = ['term' => ['transaction_type' => $params['action_type']]];
        }

        if (@$params['login_type'] === ADMIN_TYPE && @$params['login_id']) {
            $roles = getRolesDetails($params['login_id']);
            if (!in_array('owner', $roles) && !in_array('sub-admin', $roles)) {
                $filter [] = [
                    "term" => [
                        "player_details.parent_chain_ids" => [
                            "value" => $params['login_id']
                        ]
                    ]
                ];
            }
        }
        if ($params['currency_id'] != '') {
            $mustArry['bool']['should'][] = [
                'match' => ['source_currency' => $params['currency_id']]
            ];

            $mustArry['bool']['should'][] = [
                'match' => ['target_currency' => $params['currency_id']]
            ];
        }
        if(@$params['id']!='')
        $filter []= ['term' => ['internal_tracking_id' => ['value' => $params['id']]]];

        if(@$params['amount']!='')
        $filter []= ['term' => ['amount' => ['value' => $params['amount']]]];

        $strSearch = @$params['search'];
        if ($strSearch != '' && is_numeric($strSearch)) {
            $filter[] = $this->searchWordQuery($strSearch);
        } else {
            $str = '';
            if (!empty($strSearch)) {

                if (@$params['tenant_id'] != '')
                    $str = "and  tenant_id = " . $params['tenant_id'];


                $query = DB::connection('read_db')->table('users')
                    ->where(function ($query) use ($strSearch, $str) {
                        $query->whereRaw("(email LIKE '%" . $strSearch . "%') $str")
                            ->orWhereRaw("(user_name LIKE '%" . $strSearch . "%') $str");
                    })->pluck('id')->toArray();


                if (is_array($query) && count($query)) {
                    $filter = [];


                    $filter [] = ['terms' => ['player_details.player_id_s' => $query]];
                }
            }
        }

        if (array_key_exists('time_period', $params) && array_key_exists('time_period', $params)) {
            if (@$params['time_period']['fromdate'] != '' && @$params['time_period']['enddate'] != '') {
                $filter[] = [
                    'range' => [
                        'created_at' => [
                            'from' => $params['time_period']['fromdate'],
                            'include_lower' => true,
                            'to' => str_replace("000000", "999999", $params['time_period']['enddate']),
                            'include_upper' => true
                        ]
                    ]
                ];
            }
        }

        $filter [] = [
            "bool" => [
                "must_not" => [
                    "terms" => [
                        "transaction_type" => ["dummy_for_admin"]
                    ]
                ]
            ]
        ];

        $filter [] = [
            "bool" => [
                "must_not" => [
                    "terms" => [
                        "target_wallet_owner.type" => ["SuperAdminUser"]
                    ]
                ]
            ]
        ];

        if (@$params['player_id'] != '') {
            $filter [] = ['term' => ['player_details.player_id' => ['value' => $params['player_id']]]];
        } else {
            $filter [] = [
                "bool" => [
                    "must_not" => [
                        "terms" => [
                            "transaction_type" => [
                                "exchange_place_bet_non_cash_debit",
                                "exchange_place_bet_cash_debit",
                                "exchange_place_bet_cash_credit",
                                "exchange_refund_cancel_bet_non_cash_debit",
                                "exchange_refund_cancel_bet_cash_debit",
                                "exchange_refund_cancel_bet_non_cash_credit",
                                "exchange_refund_cancel_bet_cash_credit",
                                "exchange_refund_market_cancel_non_cash_debit",
                                "exchange_refund_market_cancel_cash_debit",
                                "exchange_refund_market_cancel_non_cash_credit",
                                "exchange_refund_market_cancel_cash_credit",
                                "exchange_settle_market_cash_credit",
                                "exchange_settle_market_cash_debit",
                                "exchange_resettle_market_cash_credit",
                                "exchange_resettle_market_cash_debit",
                                "exchange_cancel_settled_market_cash_credit",
                                "exchange_cancel_settled_market_cash_debit",
                                "exchange_deposit_bonus_claim"
                            ]
                        ]
                    ]
                ]
            ];
        }


        $query = [
            'bool' => [
                'must' => count($mustArry) ? $mustArry : ['match_all' => new \stdClass]
            ]
        ];

        if (count($filter)) {
            $query['bool']['filter'] = $filter;
        }

        return $query;
    }

    private function sortKey($sortBy, $sortOrder = 'ASC')
    {
        return [$sortBy => $sortOrder];
    }

    private function searchWordQuery($str)
    {

        $result = array(
            "bool" => array(
                "should" => array(
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "internal_tracking_id" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "internal_tracking_id" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "market_id" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "market_id" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),
                   /* array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "player_details" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "player_details.email.analyzed" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    ),*/
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "amount" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "amount" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                    ,
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "player_details.player_id" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "player_details.player_id" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                    ,
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "player_details.phone" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "player_details.phone" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                    ,
                    array(
                        "dis_max" => array(
                            "queries" => array(
                                array(
                                    "bool" => array(
                                        "must" => array(
                                            "bool" => array(
                                                "should" => array(
                                                    array(
                                                        "match" => array(
                                                            "round_id" => array(
                                                                "query" => $str,
                                                                "boost" => 10,
                                                                "operator" => "and",
                                                                "analyzer" => "searchkick_word_search"
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        "should" => array(
                                            "match" => array(
                                                "round_id" => array(
                                                    "query" => $str,
                                                    "boost" => 10,
                                                    "operator" => "and",
                                                    "analyzer" => "searchkick_word_search"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            )
        );


        return $result;
    }

    public static function bulkCasinoTransactionReIndexOld()
    {
        setUnlimited("6G");
        $errors = [];
        $result=DB::connection('read_db')->table('transactions')->select('id')->orderBy('id');

        $result->chunkById(5000, function ($transactions, $errors) {
            try {
                Transactions::storeElasticBulkCasinoTransactionData($transactions);
            } catch (\Exception $e) {
                echo $e->getMessage();
                $errors[] = ['transaction' => $transactions, 'error' => $e->getMessage()];
            }
        });
        
       echo "casino transaction re-index successfully done ";
        
        echo "Admin dummy_for_admin Start Reindexing ";
      
        $admins = self::admins();
        foreach ($admins as $admins) {
            try {
                $data=$admins;
                $params = [
                    'body' => $data,
                    'index' => 'transactions',//ELASTICSEARCH_INDEX['transactions'],
                    'type' => ELASTICSEARCH_TYPE,
                    'id' => "dummy_for_admin_".@$admins['player_details']['parent_id']."_".@$admins['player_details']['currency']
                ];
                $client = getEsClientObj();
                $client->index($params);

            } catch (Exception $e) {
                $errors["dummy_for_admin_".@$admins['player_details']['parent_id']] =
                    ['adminID' => @$admins['player_details']['currency'], 'error' => $e->getMessage()];
            }
        }
       
        return $errors;
    }

    public static function bulkCasinoTransactionReIndex()
    {
        setUnlimited("6G");
        $errors = [];
        $reIndex = DB::connection('read_db')->table('reindex_offset')->where('status','pending')->where('type','casino_transaction')->orderBy('id','ASC')->first();
        //echo "<pre>";print_r($reIndex);die;
        $limit = 5000;
        $offset = $reIndex->offset;
        $result=DB::connection('read_db')->table('transactions')->select('id')->take($limit)->skip($offset)->orderBy('id')->get();
        try {
            DB::table('reindex_offset')->where('id',$reIndex->id)->update(array('status'=>'in progress'));
            Transactions::storeElasticBulkCasinoTransactionData($result);
        } catch (\Exception $e) {
            echo $e->getMessage(); die;
            $errors[] = ['transaction' => $result, 'error' => $e->getMessage()];
        }
        DB::table('reindex_offset')->where('id',$reIndex->id)->update(array('status'=>'complete'));

        echo "casino transaction re-index successfully done ";

       return $errors;
    }

    public static function bulkUserTransactionReIndex()
    {
        setUnlimited("6G");
        $errors = [];
        $reIndex = DB::table('reindex_offset')->where('status','pending')->where('type','user_transaction')->orderBy('id','ASC')->first();
        //echo "<pre>";print_r($reIndex);die;
        $limit = 5000;
        $offset = $reIndex->offset;
        $result=DB::connection('read_db')->table('users')->select('id')->take($limit)->skip($offset)->orderBy('id')->get();
        try {
            DB::table('reindex_offset')->where('id',$reIndex->id)->update(array('status'=>'in progress'));
            User::storeElasticBulkUserTransactionData($result);
        } catch (\Exception $e) {
            echo $e->getMessage(); die;
            $errors[] = ['transaction' => $result, 'error' => $e->getMessage()];
        }
        DB::table('reindex_offset')->where('id',$reIndex->id)->update(array('status'=>'complete'));

        echo "user transaction re-index successfully done ";

       return $errors;
    }

    public static function bulkSportTransactionReIndex()
    {
        setUnlimited("6G");
        $errors = [];
        $reIndex = DB::table('reindex_offset')->where('status','pending')->where('type','bet_transaction')->orderBy('id','ASC')->first();
        //echo "<pre>";print_r($reIndex);die;
        $limit = 5000;
        $offset = $reIndex->offset;
        $result=DB::connection('read_db')->table('bets_transactions')->select('id')->take($limit)->skip($offset)->orderBy('id')->get();
        try {
            DB::table('reindex_offset')->where('id',$reIndex->id)->update(array('status'=>'in progress'));
            SportsService::storeElasticBulkSportTransactionData($result);
        } catch (\Exception $e) {
            echo $e->getMessage(); die;
            $errors[] = ['transaction' => $result, 'error' => $e->getMessage()];
        }
        DB::table('reindex_offset')->where('id',$reIndex->id)->update(array('status'=>'complete'));

        echo "sports transaction re-index successfully done ";

       return $errors;
    }

}
