<?php

namespace App\Services;

use Elasticsearch\ClientBuilder;
use Illuminate\Support\Facades\Auth;
// use \App\Helper\helper;
class DashboardService
{
    public static function financialResult($params)
    {
        $obj = new self();
        $body = [
            'query' => $obj->queryKey($params),
            'timeout' => '60s',
            '_source' => 'false',
            'track_total_hits' => true,
            'aggs' => [
                'currencies' => [
                    "terms" => [
                        "field" => "player_details.currency",
                        "size" => ALLCOUNT
                    ],
                    "aggs" => [
                        "chart_result" => $obj->getDepositChartResult($params),
                        "deposit" => $obj->getQueryDeposit($params),
                        "deposit_cash_admin" => $obj->getQueryCashDeposit($params),
                        "deposit_non_cash_admin" => $obj->getQueryNonCashDeposit($params),
                        "deposit_manual_cash_user" => $obj->getQueryManualCashDepositUser($params),
                        "deposit_auto_cash_user" => $obj->getQueryAutoCashDepositUser($params),
                        "withdraw" => $obj->getQueryWithdraw($params),
                        "withdraw_cash_admin" => $obj->getQueryCashWithdraw($params),
                        "withdraw_non_cash_admin" => $obj->getQueryNonCashWithdraw($params),
                        "withdraw_auto_cash_user" => $obj->getQueryAutoCashWithdraw($params),
                        "withdraw_manual_cash_user" => $obj->getQueryManualCashWithdraw($params),
                        "withdraw_cancel" => $obj->getQueryWithdrawCancel($params),
                        "final_withdraw" => $obj->getQueryWithdrawFinal($params),
                        "final_withdraw_in_EUR" => $obj->getQueryWithdrawInEur($params)
                    ]
                ],
            ]
        ];

        $params = [
            'index' => ELASTICSEARCH_INDEX['transactions_dashboard'],
            'body' => $body
        ];
        $client = getEsClientObj();
        $return = $client->search($params);
        return $return;
    }


    public static function gamingResult($params)
    {
        $obj = new self();
        $body = [
            'query' => $obj->queryKey($params),
            'timeout' => '60s',
            '_source' => 'false',
            'track_total_hits' => true,
            'aggs' => [
                'currencies' => [
                    "terms" => [
                        "field" => "player_details.currency",
                        "size" => ALLCOUNT
                    ],
                    "aggs" => [
                        "chart_result" => $obj->getStackChartResult($params),
                        "bet" => $obj->getQueryBet($params),
                        "win" => $obj->getQueryWin($params),
                        "refund" => $obj->getQueryRefund($params),
                        "bet_after_refund" => $obj->getQueryBetAfterRefund($params),
                        "bet_after_refund_in_EUR" => $obj->getQueryBetAfterRefundInEur($params),
                        "ggr" => $obj->getQueryGGR($params),
                        "ggr_in_EUR" => $obj->getQueryGGRInEur($params)
                    ]
                ],
            ]
        ];

        $params = [
            'index' => ELASTICSEARCH_INDEX['transactions_dashboard'],
            'body' => $body
        ];
        $client = getEsClientObj();
        $return = $client->search($params);
        return $return;
    }
    
    public static function activeUserResult($params)
    {
        $obj = new self();
        $body = [
            'query' => $obj->queryKey($params),
            'timeout' => '60s',
            '_source' => 'false',
            'track_total_hits' => true,
            'aggs' => [
                "active_player_chart" => $obj->getQueryActivePlayerChart($params),
            ]
        ];

        $params = [
            'index' => ELASTICSEARCH_INDEX['transactions_dashboard'],
            'body' => $body
        ];
        $client = getEsClientObj();
        $return = $client->search($params);
        return $return;
    }
    public static function fetchResult($params)
    {
        $obj = new self();
        $body = [
            'query' => $obj->queryKey($params),
            'timeout' => '60s',
            '_source' => 'false',
            'track_total_hits' => true,
            'aggs' => [
                "active_player_chart" => $obj->getQueryActivePlayerChart($params),
                'currencies' => [
                    "terms" => [
                        "field" => "player_details.currency",
                        "size" => ALLCOUNT
                    ],
                    "aggs" => [
                        "chart_result" => $obj->getQueryChartResult($params),
                        "bet" => $obj->getQueryBet($params),
                        "win" => $obj->getQueryWin($params),
                        "refund" => $obj->getQueryRefund($params),
                        "deposit" => $obj->getQueryDeposit($params),
                        "deposit_cash_admin" => $obj->getQueryCashDeposit($params),
                        "deposit_non_cash_admin" => $obj->getQueryNonCashDeposit($params),
                        "deposit_manual_cash_user" => $obj->getQueryManualCashDepositUser($params),
                        "deposit_auto_cash_user" => $obj->getQueryAutoCashDepositUser($params),
                        "withdraw" => $obj->getQueryWithdraw($params),
                        "withdraw_cash_admin" => $obj->getQueryCashWithdraw($params),
                        "withdraw_non_cash_admin" => $obj->getQueryNonCashWithdraw($params),
                        "withdraw_auto_cash_user" => $obj->getQueryAutoCashWithdraw($params),
                        "withdraw_manual_cash_user" => $obj->getQueryManualCashWithdraw($params),
                        "withdraw_cancel" => $obj->getQueryWithdrawCancel($params),
                        "final_withdraw" => $obj->getQueryWithdrawFinal($params),
                        "final_withdraw_in_EUR" => $obj->getQueryWithdrawInEur($params),
                        "bet_after_refund" => $obj->getQueryBetAfterRefund($params),
                        "bet_after_refund_in_EUR" => $obj->getQueryBetAfterRefundInEur($params),
                        "ggr" => $obj->getQueryGGR($params),
                        "ggr_in_EUR" => $obj->getQueryGGRInEur($params)
                    ]
                ],
            ]
        ];

        $params = [
            'index' => ELASTICSEARCH_INDEX['transactions_dashboard'],
            'body' => $body
        ];
        $client = getEsClientObj();
        $return = $client->search($params);
        return $return;
    }

    public static function fetchActivePlayerResult($params)
    {
        $obj = new self();
        $body = [
            'query' => $obj->queryKey($params),
            'timeout' => '60s',
            '_source' => 'false',
            'track_total_hits' => true,
            'aggs' => [
                "active_player_chart" => $obj->getQueryActivePlayerChart($params),
            ]
        ];

        $params = [
            'index' => ELASTICSEARCH_INDEX['transactions_dashboard'],
            'body' => $body
        ];
        $client = getEsClientObj();
        $return = $client->search($params);
        return $return;
    }
    
    
    
    public static function fetchWageredResult($params)
    {
        $obj = new self();

        $source = true;
        if((boolean)@$params['is_dashboard'] === 'true'){
            $source =false;
        }
        $body = [
            'query' => $obj->queryWagerdKey($params),
            'timeout' => '60s',
            '_source' => $source,
            'size' => ALLCOUNT,
            'track_total_hits' => true,
            'aggs' => [ 
                "uniq_players" => [
                  "terms" => [ "field" => "player_details.player_id","size" => ALLCOUNT ],
                  
                ],
                'currencies' => [
                    "terms" => [
                        "field" => "player_details.currency",
                        "size" => ALLCOUNT
                    ],
                    "aggs" => [
                        "bet" => $obj->getQueryBet($params),
                    ]
                ],
            ]
        ];

        $params = [
            'index' => ELASTICSEARCH_INDEX['transactions_dashboard'],
            'body' => $body
        ];
        $client = getEsClientObj();
        $return = $client->search($params);
        return $return;
    }

    /**
     * @description getQueryActivePlayerChart
     * @param $params
     * @return mixed
     */
    private function getQueryActivePlayerChart($params)
    {
        $query['filter'] = [
            "range" => [
                "creation_date" => [
                    "gte" => $params['time_period']['fromdate'],
                    "lte" => $params['time_period']['enddate']
                ]
            ]
        ];

        $interval='hour';
        $intervalParam=$params['interval']??"today";
        if($intervalParam==='weekly' || $intervalParam==='custom'){
            $interval='day';
        }elseif ($intervalParam==='lastMonth'){
          $interval='day';
        }elseif ($intervalParam==='monthly'){
            $interval='day';
        }elseif ($intervalParam==='yearly'){
            $interval='month';
        }



        $query['aggs'] = [
            "active_player" => [
                "date_histogram" => [
                    "field" => "creation_date",
                    "interval" => "$interval",
                    "min_doc_count" => 0,
                    "extended_bounds" => [
                        "min" => $params['time_period']['fromdate'],
                        "max" => $params['time_period']['enddate'],
                    ]
                ]
            ]
        ];

        return $query;

    }

    /**
     * @description queryKey
     * @param $params
     * @return mixed
     */
    private function queryKey($params)
    {
        $query["bool"] = [
            "must"=>[ "match_all"=> new \stdClass],
        ];


        $query["bool"]['filter'][] = [
            "bool" => [
                "must_not" => [
                    "bool" => [
                        "must_not" => [
                            "exists" => [
                                "field" => "player_details"
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $query["bool"]['filter'][] = [
            "terms" => [
                "transaction_type" => [
                    "deposit",
                    "withdraw",
                    "withdraw_cancel",
                    "bet",
                    "bet_non_cash",
                    "win",
                    "win_non_cash",
                    "refund",
                    "refund_non_cash",
                    "non_cash_granted_by_admin",
                    "non_cash_withdraw_by_admin",
                    "dummy_for_admin"
                ]
            ]
        ];

        $query["bool"]['filter'][] = [
            "bool" => [
                "should" => [
                    [
                        "bool" => [
                            "filter" => [
                                [
                                    "range" => [
                                        "created_at" => [
                                            "from" => $params['time_period']['fromdate'],
                                            "include_lower" => true,
                                            "to" => $params['time_period']['enddate'],
                                            "include_upper" => true
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    [
                        "bool" => [
                            "filter" => [
                                [
                                    "term" => [
                                        "transaction_type" => [
                                            "value" => "dummy_for_admin"
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        if (@$params['tenantId']) {
            $query["bool"]['filter'][] = [
                "term" => [
                    "tenant_id" => [
                        "value" => $params['tenantId']
                    ]
                ]
            ];
        }


        if (@$params['agent_id']) {
            $query["bool"]['filter'][] = [
                "term" => [
                    "player_details.parent_chain_ids" => [
                        "value" => $params['agent_id']
                    ]
                ]
            ];
        }
        if (@$params['isDirectPlayer'] == 'direct' && $params['agent_id']) {
            $roles = getRolesDetails($params['agent_id']);
            if (!in_array('owner', $roles) && !in_array('sub-admin', $roles)) {
                $query["bool"]['filter'][] = [
                    "term" => [
                        "player_details.parent_id" => [
                            "value" => $params['agent_id']
                        ]
                    ]
                ];
            }
        }

        return $query;
    }
   
   
   
    private function queryWagerdKey($params)
    {
        $query["bool"] = [
            "must"=>[ "match_all"=> new \stdClass],
        ];


        $query["bool"]['filter'][] = [
            "bool" => [
                "must_not" => [
                    "bool" => [
                        "must_not" => [
                            "exists" => [
                                "field" => "player_details"
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $query["bool"]['filter'][] = [
            "terms" => [
                "transaction_type" => [
                   
                    "bet",
                    "bet_non_cash"
                ]
            ]
        ];

        $query["bool"]['filter'][] = [
            "bool" => [
                "should" => [
                    [
                        "bool" => [
                            "filter" => [
                                [
                                    "range" => [
                                        "created_at" => [
                                            "from" => $params['time_period']['fromdate'],
                                            "include_lower" => true,
                                            "to" => $params['time_period']['enddate'],
                                            "include_upper" => true
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    [
                        "bool" => [
                            "filter" => [
                                [
                                    "term" => [
                                        "transaction_type" => [
                                            "value" => "dummy_for_admin"
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        if(@$params['tenantId']) {
            $query["bool"]['filter'][] = [
                "term" => [
                    "tenant_id" => [
                        "value" => $params['tenantId']
                    ]
                ]
            ];
        }

/*        if (@$params['agentIdTop']) {
            $query["bool"]['filter'][] = [
                "term" => [
                    "player_details.parent_chain_ids" => [
                        "value" => $params['agentIdTop']
                    ]
                ]
            ];
        } else {
            if(@$params['agent_id']) {
                $query["bool"]['filter'][] = [
                    "term" => [
                        "player_details.parent_id" => [
                            "value" => $params['agent_id']
                        ]
                    ]
                ];
            }
        }*/

        /*if (Auth::user()->parent_type == 'AdminUser' && Auth::user()->tenant_id) {
            $roles = getRolesDetails(@Auth::user()->id);

            if (!in_array('owner', $roles)) {

                $query["bool"]['filter'][] = [
                    "term" => [
                        "player_details.parent_chain_ids" => [
                            "value" => $params['agent_id']
                        ]
                    ]
                ];

            }
            else{
                    if(@$params['isDirectPlayer']=='all' && $params['agent_id']){
                        $query["bool"]['filter'][] = [
                            "term" => [
                                "player_details.parent_chain_ids" => [
                                    "value" => $params['agent_id']
                                ]
                            ]
                        ];
                    }
            }



        }else{
            $roles = getRolesDetails(@$params['agent_id']);
            if (!in_array('owner', $roles)) {

                $query["bool"]['filter'][] = [
                    "term" => [
                        "player_details.parent_chain_ids" => [
                            "value" => $params['agent_id']
                        ]
                    ]
                ];

            }else{
                if(@$params['isDirectPlayer']=='all' && $params['agent_id']){
                    $query["bool"]['filter'][] = [
                        "term" => [
                            "player_details.parent_chain_ids" => [
                                "value" => $params['agent_id']
                            ]
                        ]
                    ];
                }
            }


        }*/
        if (@$params['agent_id']) {

            $query["bool"]['filter'][] = [
                "term" => [
                    "player_details.parent_chain_ids" => [
                        "value" => $params['agent_id']
                    ]
                ]
            ];

        }

        if(@$params['isDirectPlayer']=='direct' && $params['agent_id']){
            $query["bool"]['filter'][] = [
                "term" => [
                    "player_details.parent_id" => [
                        "value" => $params['agent_id']
                    ]
                ]
            ];
        }

  return $query;
    }

    /*
     *
   Bet after refund = bet -refund
   GGR = >  (Bet after refund )- win

    /**
     * @description getQueryChartResult
     * @param $params
     */
    private function getQueryChartResult($params)
    {
        $query["filter"] = [
            "terms" => [
                "transaction_type" => [
                    "bet",
                    "bet_non_cash",
                    "refund",
                    "refund_non_cash",
                    "win",
                    "win_non_cash",
                    "deposit",
                    "non_cash_granted_by_admin"
                ]
            ]
        ];
        $interval='hour';
        $intervalParam=$params['interval']??"today";
        if($intervalParam==='weekly' || $intervalParam==='custom'){
            $interval='day';
        }elseif ($intervalParam==='lastMonth'){
          $interval='day';
        }elseif ($intervalParam==='monthly'){
            $interval='day';
        }elseif ($intervalParam==='yearly'){
            $interval='month';
          }

        $query["aggs"] ["dashboard_charts"]["date_histogram"] = [
            "field" => "created_at",
            "interval" => $interval,
            "min_doc_count" => 0,
            "extended_bounds" => [
                "min" => $params['time_period']['fromdate'],
                "max" => $params['time_period']['enddate'],
            ]
        ];

        $query["aggs"] ["dashboard_charts"]["aggs"] = [

            "deposit_for_chart" => [
                "filter" => [
                    "terms" => [
                        "transaction_type" => ["deposit","non_cash_granted_by_admin"]
                    ]
                ],
                "aggs" => [
                    "total" => [
                        "sum" =>
                            ["field" => "amount"]

                    ],
                    "total_in_EUR" => [
                        "sum" => [
                            "field" => "amount_in_currencies.EUR"
                        ]
                    ]
                ]
            ],

            "bet_for_chart" =>[
                "filter" => [
                    "bool" => [
                        "filter" => [
                            [
                                "terms" => [
                                    "transaction_type" => [
                                        "bet",
                                        "bet_non_cash"
                                    ]
                                ]
                            ],
                            [
                                "term" => [
                                    "player_details.demo"=>"false"
                                       ]
                                    ]
                                 ]
                              ]
                ],
                "aggs" => [
                    "total" => [
                        "sum" => [
                            "field" => "amount"
                        ]
                    ],
                    "total_in_EUR" => [
                        "sum" => [
                            "field" => "amount_in_currencies.EUR"
                        ]
                    ]
                ]
            ],

            "win_for_chart" => [
                "filter" => [
                    "bool" => [
                        "filter" => [
                            [
                                "terms" => [
                                    "transaction_type" => [
                                        "win",
                                        "win_non_cash"
                                    ]
                                ]
                            ],
                            [
                                "term" => [
                                    "player_details.demo" => "false"
                                ]
                            ]
                        ]
                    ]
                ],
                "aggs" => [
                    "total" => [
                        "sum" => [
                            "field" => "amount"
                        ]
                    ],
                    "total_in_EUR" => [
                        "sum" => [
                            "field" => "amount_in_currencies.EUR"
                        ]
                    ]
                ],
            ],

            "refund_for_chart" => [
                "filter" => [
                    "bool" => [
                        "filter" => [
                            [
                                "terms" => [
                                    "transaction_type" => [
                                        "refund",
                                        "refund_non_cash"
                                    ]
                                ]
                            ],
                            [
                                "term" => [
                                    "player_details.demo" => "false"
                                ]
                            ]
                        ]
                    ]
                ],
                "aggs" => [
                    "total" => [
                        "sum" => [
                            "field" => "amount"
                        ]
                    ],
                    "total_in_EUR" => [
                        "sum" => [
                            "field" => "amount_in_currencies.EUR"
                        ]
                    ]
                ]

            ],

            "bet_after_refund_for_chart" => [
                "bucket_script" => [
                    "buckets_path" => [
                        "bet" => "bet_for_chart>total",
                        "refund" => "refund_for_chart>total"
                    ],
                    "script" => "params.bet - params.refund"
                ]
            ],

            "bet_after_refund_for_chart_in_EUR" => [
                "bucket_script" => [
                    "buckets_path" => [
                        "bet" => "bet_for_chart>total_in_EUR",
                        "refund" => "refund_for_chart>total_in_EUR"
                    ],
                    "script" => "params.bet - params.refund"
                ]
            ],

            "ggr_for_chart" => [
                "bucket_script" => [
                    "buckets_path" => [
                        "bet_after_refund" => "bet_after_refund_for_chart",
                        "win" => "win_for_chart>total"
                    ],
                    "script" => "params.bet_after_refund - params.win"
                ]

            ],

            "ggr_for_chart_in_EUR" => [
                "bucket_script" => [
                    "buckets_path" => [
                        "bet_after_refund" => "bet_after_refund_for_chart_in_EUR",
                        "win" => "win_for_chart>total_in_EUR"
                    ],
                    "script" => "params.bet_after_refund - params.win"
                ]
            ],

        ];
        return $query;
    }

    private function getStackChartResult($params)
    {
        $query["filter"] = [
            "terms" => [
                "transaction_type" => [
                    "bet",
                    "bet_non_cash",
                    "refund",
                    "refund_non_cash",
                    "win",
                    "win_non_cash",
                ]
            ]
        ];
        $interval='hour';
        $intervalParam=$params['interval']??"today";
        if($intervalParam==='weekly' || $intervalParam==='custom'){
            $interval='day';
        }elseif ($intervalParam==='lastMonth'){
          $interval='day';
        }elseif ($intervalParam==='monthly'){
            $interval='day';
        }elseif ($intervalParam==='yearly'){
            $interval='month';
          }

        $query["aggs"] ["dashboard_charts"]["date_histogram"] = [
            "field" => "created_at",
            "interval" => $interval,
            "min_doc_count" => 0,
            "extended_bounds" => [
                "min" => $params['time_period']['fromdate'],
                "max" => $params['time_period']['enddate'],
            ]
        ];

        $query["aggs"] ["dashboard_charts"]["aggs"] = [
            "bet_for_chart" =>[
                "filter" => [
                    "bool" => [
                        "filter" => [
                            [
                                "terms" => [
                                    "transaction_type" => [
                                        "bet",
                                        "bet_non_cash"
                                    ]
                                ]
                            ],
                            [
                                "term" => [
                                    "player_details.demo"=>"false"
                                       ]
                                    ]
                                 ]
                              ]
                ],
                "aggs" => [
                    "total" => [
                        "sum" => [
                            "field" => "amount"
                        ]
                    ],
                    "total_in_EUR" => [
                        "sum" => [
                            "field" => "amount_in_currencies.EUR"
                        ]
                    ]
                ]
            ],

            "win_for_chart" => [
                "filter" => [
                    "bool" => [
                        "filter" => [
                            [
                                "terms" => [
                                    "transaction_type" => [
                                        "win",
                                        "win_non_cash"
                                    ]
                                ]
                            ],
                            [
                                "term" => [
                                    "player_details.demo" => "false"
                                ]
                            ]
                        ]
                    ]
                ],
                "aggs" => [
                    "total" => [
                        "sum" => [
                            "field" => "amount"
                        ]
                    ],
                    "total_in_EUR" => [
                        "sum" => [
                            "field" => "amount_in_currencies.EUR"
                        ]
                    ]
                ],
            ],

            "refund_for_chart" => [
                "filter" => [
                    "bool" => [
                        "filter" => [
                            [
                                "terms" => [
                                    "transaction_type" => [
                                        "refund",
                                        "refund_non_cash"
                                    ]
                                ]
                            ],
                            [
                                "term" => [
                                    "player_details.demo" => "false"
                                ]
                            ]
                        ]
                    ]
                ],
                "aggs" => [
                    "total" => [
                        "sum" => [
                            "field" => "amount"
                        ]
                    ],
                    "total_in_EUR" => [
                        "sum" => [
                            "field" => "amount_in_currencies.EUR"
                        ]
                    ]
                ]

            ],

            "bet_after_refund_for_chart" => [
                "bucket_script" => [
                    "buckets_path" => [
                        "bet" => "bet_for_chart>total",
                        "refund" => "refund_for_chart>total"
                    ],
                    "script" => "params.bet - params.refund"
                ]
            ],

            "bet_after_refund_for_chart_in_EUR" => [
                "bucket_script" => [
                    "buckets_path" => [
                        "bet" => "bet_for_chart>total_in_EUR",
                        "refund" => "refund_for_chart>total_in_EUR"
                    ],
                    "script" => "params.bet - params.refund"
                ]
            ],

            "ggr_for_chart" => [
                "bucket_script" => [
                    "buckets_path" => [
                        "bet_after_refund" => "bet_after_refund_for_chart",
                        "win" => "win_for_chart>total"
                    ],
                    "script" => "params.bet_after_refund - params.win"
                ]

            ],

            "ggr_for_chart_in_EUR" => [
                "bucket_script" => [
                    "buckets_path" => [
                        "bet_after_refund" => "bet_after_refund_for_chart_in_EUR",
                        "win" => "win_for_chart>total_in_EUR"
                    ],
                    "script" => "params.bet_after_refund - params.win"
                ]
            ],

        ];
        return $query;
    }


    private function getDepositChartResult($params)
    {
        $query["filter"] = [
            "terms" => [
                "transaction_type" => [
                    "deposit",
                    "non_cash_granted_by_admin"
                ]
            ]
        ];
        $interval='hour';
        $intervalParam=$params['interval']??"today";
        if($intervalParam==='weekly' || $intervalParam==='custom'){
            $interval='day';
        }elseif ($intervalParam==='lastMonth'){
          $interval='day';
        }elseif ($intervalParam==='monthly'){
            $interval='day';
        }elseif ($intervalParam==='yearly'){
            $interval='month';
          }

        $query["aggs"] ["dashboard_charts"]["date_histogram"] = [
            "field" => "created_at",
            "interval" => $interval,
            "min_doc_count" => 0,
            "extended_bounds" => [
                "min" => $params['time_period']['fromdate'],
                "max" => $params['time_period']['enddate'],
            ]
        ];

        $query["aggs"] ["dashboard_charts"]["aggs"] = [

            "deposit_for_chart" => [
                "filter" => [
                    "terms" => [
                        "transaction_type" => ["deposit","non_cash_granted_by_admin"]
                    ]
                ],
                "aggs" => [
                    "total" => [
                        "sum" =>
                            ["field" => "amount"]

                    ],
                    "total_in_EUR" => [
                        "sum" => [
                            "field" => "amount_in_currencies.EUR"
                        ]
                    ]
                ]
            ]
        ];
        return $query;
    }

    /**
     * @description getQueryBet
     * @param $params
     * @return mixed
     */
    private function getQueryBet($params){
        $query['filter'] = [
            "bool" => [
                "filter" => [
                    [
                        "terms" => [
                            "transaction_type" => [
                                "bet",
                                "bet_non_cash"
                            ]
                        ]
                    ],
                    [
                        "term" => [
                            "player_details.demo" => "false"
                        ]
                    ]
                ]
            ]
        ];
        $query['aggs'] = [
            "bet_amount" => [
                "sum" => [
                    "field" => "amount"
                ]
            ],
            "bet_amount_in_EUR" => [
                "sum" => [
                    "field" => "amount_in_currencies.EUR"
                ]
            ]
        ];

        return $query;
    }

    /**
     * @description getQueryWin
     * @param $params
     */
    private function getQueryWin($params)
    {
        $query["filter"] = [
            "bool" => [
                "filter" => [
                    [
                        "terms" => [
                            "transaction_type" => [
                                "win",
                                "win_non_cash"
                            ]
                        ]
                    ],
                    [
                        "term" => [
                            "player_details.demo" => "false"
                        ]
                    ]
                ]
            ]
        ];
        $query["aggs"] = [
            "total_win_amount" => [
                "sum" => [
                    "field" => "amount"
                ]
            ],
            "total_win_amount_in_EUR" => [
                "sum" => [
                    "field" => "amount_in_currencies.EUR"
                ]
            ]
        ];

        return $query;
    }

    /**
     * @description getQueryRefund
     * @param $params
     * @return mixed
     */
    private function getQueryRefund($params)
    {
        $query['filter'] = [
            "bool" => [
                "filter" => [
                    [
                        "terms" => [
                            "transaction_type" => [
                                "refund",
                                "refund_non_cash"
                            ]
                        ]
                    ],
                    [
                        "term" => [
                            "player_details.demo" => "false"
                        ]
                    ]
                ]
            ]
        ];
        $query['aggs'] = [
            "refund_amount" => [
                "sum" => [
                    "field" => "amount"
                ]
            ],
            "refund_amount_in_EUR" => [
                "sum" => [
                    "field" => "amount_in_currencies.EUR"
                ]
            ]
        ];


        return $query;
    }

    /**
     * @description getQueryDeposit
     * @param $params
     * @return mixed
     */
    private function getQueryDeposit($params)
    {
        $query['filter'] = [
            "bool" => [
                "filter" => [
                    [
                        "terms" => [
                            "description" => [
                                "approved by admin", "Approved By Payment gateway", "Deposit Request", "Deposit Request Approved"
                            ]
                        ],
                    ],
                    [
                        "terms" => [
                            "transaction_type" => ["non_cash_granted_by_admin", "deposit"]
                        ]
                    ]
                ]
            ]
        ];
        $query["aggs"] = [
            "total_deposit" => [
                "sum" => [
                    "field" => "amount"
                ]
            ],
            "total_deposit_in_EUR" => [
                "sum" => [
                    "field" => "amount_in_currencies.EUR"
                ]
            ]
        ];

        return $query;
    }
    private function getQueryCashDeposit($params)
    {
        $query['filter'] = [
            "bool" => [
                "must" => [
                    [
                        "terms" => [
                            "actionee.type" => [
                                "AdminUser"
                            ]
                        ]
                    ],
                    [
                        "term" => [
                            "transfer_method" => "manual"
                        ]
                    ],
                    [
                        "term" => [
                            "transaction_type" => "deposit"
                        ]
                    ]
                ],
                "must_not" => [
                    "terms" => [
                        "description" => [
                            "Deposit Request Approved"
                        ]
                    ]
                ]
            ]
        ];
        $query["aggs"] = [
            "total_deposit" => [
                "sum" => [
                    "field" => "amount"
                ]
            ],
            "total_deposit_in_EUR" => [
                "sum" => [
                    "field" => "amount_in_currencies.EUR"
                ]
            ]
        ];

        return $query;
    }
    private function getQueryAutoCashDepositUser($params)
    {
        $query['filter'] = [
            "bool" => [
                "filter" => [
                    [
                        "term" => [ "transaction_type" => "deposit" ]
                    ],
                    [
                        "term" => [ "description" => "Deposit Request" ]
                    ],
                    [
                        "term" => [ "status" => "success" ]
                    ]
                ]
            ]
        ];
        $query["aggs"] = [
            "total_deposit" => [
                "sum" => [
                    "field" => "amount"
                ]
            ],
            "total_deposit_in_EUR" => [
                "sum" => [
                    "field" => "amount_in_currencies.EUR"
                ]
            ]
        ];

        return $query;
    }
    private function getQueryManualCashDepositUser($params)
    {
        $query['filter'] = [
            "bool" => [
                "filter" => [
                    [
                        "terms" => [
                            "description" => [
                                "Deposit Request Approved"
                            ]
                        ]
                    ],
                    [
                        "term" => [
                            "transaction_type" => "deposit"
                        ]
                    ],
                    [
                        "term" => [
                            "transfer_method" => "manual"
                        ]
                    ]
                ]
            ]
        ];
        $query["aggs"] = [
            "total_deposit" => [
                "sum" => [
                    "field" => "amount"
                ]
            ],
            "total_deposit_in_EUR" => [
                "sum" => [
                    "field" => "amount_in_currencies.EUR"
                ]
            ]
        ];

        return $query;
    }
    private function getQueryNonCashDeposit($params)
    {
        $query["filter"] = [
            "term" => [
                "transaction_type" => "non_cash_granted_by_admin"
            ]
        ];
        $query["aggs"] = [
            "total_deposit" => [
                "sum" => [
                    "field" => "amount"
                ]
            ],
            "total_deposit_in_EUR" => [
                "sum" => [
                    "field" => "amount_in_currencies.EUR"
                ]
            ]
        ];

        return $query;
    }

    /**
     * @description getQueryWithdraw
     * @param $params
     * @return mixed
     */
    private function getQueryWithdraw($params)
    {
        $query['filter'] = [
            "bool" => [
                "filter" => [
                    [
                        "term" => [
                            "status" => "success"
                        ]
                    ],
                    [
                        "terms" => [
                            "description" => [
                                "approved by admin", "Approved By Payment gateway"
                            ]
                        ]
                    ],
                    [
                        "terms" => [
                            "transaction_type" => ["withdraw", "non_cash_withdraw_by_admin"]
                        ]
                    ]
                ]
            ]
        ];

        $query["aggs"] = [
            "total_withdraw" => [
                "sum" => [
                    "field" => "amount"
                ]
            ],
            "total_withdraw_in_EUR" => [
                "sum" => [
                    "field" => "amount_in_currencies.EUR"
                ]
            ]
        ];

        return $query;
    }

    private function getQueryCashWithdraw($params)
    {
        $query['filter'] = [
            "bool" => [
                "must" => [
                    [
                        "term" => [
                            "status" => "success"
                        ]
                    ],
                    [
                        "terms" => [
                            "actionee.type" => [
                                "AdminUser"
                            ]
                        ]
                    ],
                    [
                        "term" => [
                            "transaction_type" => "withdraw"
                        ]
                    ]
                ],
                "must_not" => [
                    "terms" => [
                        "description" => [
                            "approved by admin", "cancelled by admin", "Approved By Payment gateway"
                        ]
                    ]
                ]
            ]
        ];

        $query["aggs"] = [
            "total_withdraw" => [
                "sum" => [
                    "field" => "amount"
                ]
            ],
            "total_withdraw_in_EUR" => [
                "sum" => [
                    "field" => "amount_in_currencies.EUR"
                ]
            ]
        ];
        return $query;
    }

    private function getQueryNonCashWithdraw($params)
    {
        $query["filter"] = [
            "term" => [
                "transaction_type" => "non_cash_withdraw_by_admin"
            ]
        ];
        $query["aggs"] = [
            "total_withdraw" => [
                "sum" => [
                    "field" => "amount"
                ]
            ],
            "total_withdraw_in_EUR" => [
                "sum" => [
                    "field" => "amount_in_currencies.EUR"
                ]
            ]
        ];

        return $query;
    }

    private function getQueryAutoCashWithdraw($params)
    {
        $query['filter'] = [
            "bool" => [
                "filter" => [
                    [
                        "terms" => [
                            "description" => ["Approved By Payment gateway"]
                        ]
                    ],
                    [
                        "term" => [
                            "transaction_type" => "withdraw"
                        ]
                    ]
                ]
            ]
        ];
        $query["aggs"] = [
            "total_withdraw" => [
                "sum" => [
                    "field" => "amount"
                ]
            ],
            "total_withdraw_in_EUR" => [
                "sum" => [
                    "field" => "amount_in_currencies.EUR"
                ]
            ]
        ];

        return $query;
    }

    private function getQueryManualCashWithdraw($params)
    {
        $query['filter'] = [
            "bool" => [
                "filter" => [
                    [
                        "terms" => [
                            "description" => [
                                "approved by admin"
                            ]
                        ]
                    ],
                    [
                        "term" => [
                            "transaction_type" => "withdraw"
                        ]
                    ]
                ]
            ]
        ];
        $query["aggs"] = [
            "total_withdraw" => [
                "sum" => [
                    "field" => "amount"
                ]
            ],
            "total_withdraw_in_EUR" => [
                "sum" => [
                    "field" => "amount_in_currencies.EUR"
                ]
            ]
        ];

        return $query;
    }

    /**
     * @description getQueryWithdrawCancel
     * @param $params
     * @return mixed
     */
    private function getQueryWithdrawCancel($params)
    {
        $query["filter"] = [
            "term" => [
                "transaction_type" => "withdraw_cancel"
            ]
        ];
        $query["aggs"] = [
            "withdraw_cancel_amount" => [
                "sum" => [
                    "field" => "amount"
                ]
            ],
            "withdraw_cancel_amount_in_EUR" => [
                "sum" => [
                    "field" => "amount_in_currencies.EUR"
                ]
            ]
        ];
        return $query;
    }

    /**
     * @description getQueryWithdrawFinal
     * @param $params
     * @return mixed
     */
    private function getQueryWithdrawFinal($params)
    {
        $query["bucket_script"] =
            [
                "buckets_path" => [
                    "withdraw" => "withdraw>total_withdraw",
                    "withdraw_cancel" => "withdraw_cancel>withdraw_cancel_amount"
                ],
                "script" =>"params.withdraw"
            ];
        return $query;
    }

    /**
     * @description getQueryWithdrawInEur
     * @param $params
     */
    private function getQueryWithdrawInEur($params)
    {
        $query["bucket_script"] = [

            "buckets_path" => [
                "withdraw" => "withdraw>total_withdraw_in_EUR",
                "withdraw_cancel" => "withdraw_cancel>withdraw_cancel_amount_in_EUR"
            ],
            "script" => "params.withdraw"
        ];

        return $query;
    }

    /**
     * @description getQueryBetAfterRefund
     * @param $params
     */
    private function getQueryBetAfterRefund($params)
    {
        $query["bucket_script"] = [
            "buckets_path" => [
                "bet" => "bet>bet_amount",
                "refund" => "refund>refund_amount"
            ],
            "script" => "params.bet - params.refund"

        ];

        return $query;
    }

    /**
     * @description getQueryBetAfterRefundInEur
     */
    private function getQueryBetAfterRefundInEur($params)
    {
        $query["bucket_script"] = [
            "buckets_path" => [
                "bet" => "bet>bet_amount_in_EUR",
                "refund" => "refund>refund_amount_in_EUR"
            ],
            "script" => "params.bet - params.refund"
        ];
        return $query;
    }

    /**
     * @description getQueryGGR
     * @param $params
     * @return mixed
     */
    private function getQueryGGR($params)
    {
        $query["bucket_script"] = [
            "buckets_path" => [
                "bet_after_refund" => "bet_after_refund",
                "win" => "win>total_win_amount"
            ],
            "script" => "params.bet_after_refund - params.win"
        ];

        return $query;
    }

    /**
     * @description getQueryGGRInEur
     * @param $params
     * @return mixed
     */
    private function getQueryGGRInEur($params){
        $query["bucket_script"] = [
            "buckets_path"=>[
                "bet_after_refund"=>"bet_after_refund_in_EUR",
                 "win"=>"win>total_win_amount_in_EUR"
            ],
            "script"=>"params.bet_after_refund - params.win"
        ];
        return $query;
    }
}
