<?php

namespace App\Services;

use Elasticsearch\ClientBuilder;

class PlayerBonusStatusService
{


    public static function fetchResult($params)
    {
        $obj = new self();

        $body = [

            'query' => $obj->queryKey($params),
//            'sort' => $obj->sortKey($params['sortBy'], $params['sortOrder']),
            'timeout' => '60s',
            '_source' =>  "false",
            'track_total_hits' => true,
            'aggs' => [
                'reports' => $obj->reportsKey($params),
                'total_deposit_bonus_claimed' => $obj->totalDepositBonusClaimed(),
                'total_non_cash_bonus_claimed' => $obj->totalNonCashBonusClaimed(),
                'total_non_cash_granted_by_admin' => $obj->total_non_cash_granted_by_admin(),
                'total_non_cash_withdraw_by_admin' => $obj->totalNonCashWithdrawByAdmin(),
                'total_non_cash_player_commission' => $obj->totalNonCashPlayerCommission(),
                'total_record_count' => $obj->totalRcordCount()

            ]
        ];

        /*if ($params['limit'] != 'all') {
            $body['size'] = (int)$params['limit'];
            $body['from'] = (int)$params['offset'];
        } else {
            $body['size'] = ALLCOUNT;
        }*/


        $params = [
            'index' => ELASTICSEARCH_INDEX['transactions'],
            'body' => $body
        ];
        $client = getEsClientObj();
//        du($params);
        $return = $client->search($params);


        return $return;
    }

    private function queryKey($params)
    {

        $mustArry = [];
        $filter = [];

        if ($params['tenant_id'] != '') {
            $filter [] = ['term' => [
                'tenant_id' => [
                    'value' => $params['tenant_id']
                        ]
                    ]
                ];
        }

        $filter []= ['bool' =>
            ['must_not' =>
                [
                    'bool' => [
                        "must_not" =>[
                            "exists" => [
                                "field" => "player_details"
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $filter [] =
            ['terms' =>
                ['transaction_type' =>
                    [
                        "non_cash_granted_by_admin",
                        "non_cash_bonus_claim",
                        "deposit_bonus_claim",
                        "non_cash_withdraw_by_admin",
                        "commission_received"
                    ]
                ]
            ];

        if(is_array($params['time_period']) &&
            $params['time_period']['fromdate'] !='' &&
            $params['time_period']['enddate'] !=''
        ) {


            $filter [] = [
                "range" => [
                    "created_at" => [
                        "from" => $params['time_period']['fromdate'],
                        "include_lower" => true,
                        "to" => str_replace("000000", "999999", $params['time_period']['enddate']),
                        "include_upper" => true
                    ]
                ]
            ];
        }

        
        if($params['currency'] && $params['currency'] != '')
        $filter []= ['term' => ['player_details.currency' => ['value' => $params['currency']]]];

        if($params['agentId'] && $params['agentId'] != '')
        $filter []= ['term' => ['player_details.parent_chain_ids' => ['value' => $params['agentId']]]];

        if ($params['player_type'] == 'direct')
            $filter [] = ['term' => ['player_details.parent_id' => ['value' => $params['agentId']]]];
//        elseif (@$params['agentId']!='')
//            $filter []= ['term' => ['player_details.parent_chain_ids' => ['value' => $params['agentId']]]];


        if($params['search']!='') {
            $searchQuery = [
                "must" => [
                    "bool" => [
                        "should" => [
                            [
                                "dis_max" => [
                                    "queries" => [
                                        [
                                            "bool" => [
                                                "must" => [
                                                    "bool" => [
                                                        "should" => [
                                                            [
                                                                "match" => [
                                                                    "player_details.player_id_s.word_start" => [
                                                                        "query" => (string)$params['search'],
                                                                        "boost" => 10,
                                                                        "operator" => "and",
                                                                        "analyzer" => "searchkick_word_search"
                                                                    ]
                                                                ]
                                                            ]
                                                        ]
                                                    ]
                                                ],
                                                "should" => [
                                                    "match" => [
                                                        "player_details.player_id_s.analyzed" => [
                                                            "query" =>  (string)$params['search'],
                                                            "boost" => 10,
                                                            "operator" => "and",
                                                            "analyzer" => "searchkick_word_search"
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            [
                                "dis_max" => [
                                    "queries" => [
                                        [
                                            "bool" => [
                                                "must" => [
                                                    "bool" => [
                                                        "should" => [
                                                            [
                                                                "match" => [
                                                                    "player_details.player_name.word_start" => [
                                                                        "query" =>  (string)$params['search'],
                                                                        "boost" => 10,
                                                                        "operator" => "and",
                                                                        "analyzer" => "searchkick_word_search"
                                                                    ]
                                                                ]
                                                            ]
                                                        ]
                                                    ]
                                                ],
                                                "should" => [
                                                    "match" => [
                                                        "player_details.player_name.analyzed" => [
                                                            "query" =>  (string)$params['search'],
                                                            "boost" => 10,
                                                            "operator" => "and",
                                                            "analyzer" => "searchkick_word_search"
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            [
                                "dis_max" => [
                                    "queries" => [
                                        [
                                            "bool" => [
                                                "must" => [
                                                    "bool" => [
                                                        "should" => [
                                                            [
                                                                "match" => [
                                                                    "player_details.agent_name.word_start" => [
                                                                        "query" =>  (string)$params['search'],
                                                                        "boost" => 10,
                                                                        "operator" => "and",
                                                                        "analyzer" => "searchkick_word_search"
                                                                    ]
                                                                ]
                                                            ]
                                                        ]
                                                    ]
                                                ],
                                                "should" => [
                                                    "match" => [
                                                        "player_details.agent_name.analyzed" => [
                                                            "query" =>  (string)$params['search'],
                                                            "boost" => 10,
                                                            "operator" => "and",
                                                            "analyzer" => "searchkick_word_search"
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            [
                                "dis_max" => [
                                    "queries" => [
                                        [
                                            "bool" => [
                                                "must" => [
                                                    "bool" => [
                                                        "should" => [
                                                            [
                                                                "match" => [
                                                                    "player_details.phone.word_start" => [
                                                                        "query" =>  (string)$params['search'],
                                                                        "boost" => 10,
                                                                        "operator" => "and",
                                                                        "analyzer" => "searchkick_word_search"
                                                                    ]
                                                                ]
                                                            ]
                                                        ]
                                                    ]
                                                ],
                                                "should" => [
                                                    "match" => [
                                                        "player_details.phone.analyzed" => [
                                                            "query" =>  (string)$params['search'],
                                                            "boost" => 10,
                                                            "operator" => "and",
                                                            "analyzer" => "searchkick_word_search"
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];
        }else{
            $searchQuery = [
                'must' => ['match_all' => new \stdClass]
            ];
        }
        $query = [
            'bool' => $searchQuery
        ];
        if (count($filter)) {
            $query['bool']['filter'] = $filter;
        }
        return $query;
    }

    private function sortKey($sortBy, $sortOrder = 'ASC')
    {
        return [$sortBy => $sortOrder];
    }

    private function reportsKey($params)
    {


        $reports = array(
            "terms" => array(
                "script" => 'doc["player_details.player_name"].value.toLowerCase() + "-" + doc["player_details.player_id"].value',
                "size" => 20000
            ),
            "aggs" => array(
                "player_id" => array(
                    "max" => array(
                        "field" => "player_details.player_id"
                    )
                ),
                "top" => array(
                    "top_hits" => array(
                        "_source" => array(
                            "includes" => [
                                "player_details.player_name",
                                "player_details.agent_name",
                                "player_details.email",
                                "player_details.agent_email",
                                "player_details.currency"
                            ]
                        ),
                        "size" => 1
                    )
                ),
                "deposit_bonus_claimed" => array(
                    "filter" => array(
                        "bool" => array(
                            "filter" => [
                                array(
                                    "term" => array(
                                        "transaction_type" => "deposit_bonus_claim"
                                    )
                                ),
                                array(
                                    "term" => array(
                                        "status" => "success"
                                    )
                                )
                            ]
                        )
                    ),
                    "aggs" => array(
                        "total" => array(
                            "sum" => array(
                                "field" => "amount"
                            )
                        )
                    )
                ),
                "non_cash_bonus_claimed" => array(
                    "filter" => array(
                        "term" => array(
                            "transaction_type" => "non_cash_bonus_claim"
                        )
                    ),
                    "aggs" => array(
                        "total" => array(
                            "sum" => array(
                                "field" => "player_details.non_cash_amount"
                            )
                        )
                    )
                ),
                "non_cash_granted_by_admin" => array(
                    "filter" => array(
                        "term" => array(
                            "transaction_type" => "non_cash_granted_by_admin"
                        )
                    ),
                    "aggs" => array(
                        "total" => array(
                            "sum" => array(
                                "field" => "player_details.non_cash_amount"
                            )
                        )
                    )
                ),
                "non_cash_withdraw_by_admin" => array(
                    "filter" => array(
                        "term" => array(
                            "transaction_type" => "non_cash_withdraw_by_admin"
                        )
                    ),
                    "aggs" => array(
                        "total" => array(
                            "sum" => array(
                                "field" => "player_details.non_cash_amount"
                            )
                        )
                    )
                ),
                "non_cash_player_commission" => array(
                    "filter" => array(
                        "term" => array(
                            "transaction_type" => "commission_received"
                        )
                    ),
                    "aggs" => array(
                        "total" => array(
                            "sum" => array(
                                "field" => "amount"
                            )
                        )
                    )
                ),
                "active_deposit_bonus" => array(
                    "filter" => array(
                        "term" => array(
                            "status" => "pending"
                        )
                    ),
                    "aggs" => array(
                        "unconverted_total" => array(
                            "sum" => array(
                                "field" => "player_details.deposit_bonus_details.unconverted_active_deposit_bonus"
                            )
                        ),
                        "remaining_rollover_total" => array(
                            "sum" => array(
                                "field" => "player_details.deposit_bonus_details.active_deposit_bonus_remaining_rollover"
                            )
                        )
                    )
                ),
                "total_non_cash_by_admin" => array(
                    "bucket_script" => array(
                        "buckets_path" => array(
                            "non_cash_granted_by_admin" => "non_cash_granted_by_admin>total",
                            "non_cash_withdraw_by_admin" => "non_cash_withdraw_by_admin>total"
                        ),
                        "script" => "params.non_cash_granted_by_admin - params.non_cash_withdraw_by_admin"
                    )
                ),
                "total_bonus_claimed" => array(
                    "bucket_script" => array(
                        "buckets_path" => array(
                            "deposit_bonus_claimed" => "deposit_bonus_claimed>total",
                            "non_cash_bonus_claimed" => "non_cash_bonus_claimed>total",
                            "total_non_cash_by_admin" => "total_non_cash_by_admin"
                        ),
                        "script" => "params.deposit_bonus_claimed + params.non_cash_bonus_claimed + params.total_non_cash_by_admin"
                    )
                ),
                "final_sort" => array(
                    "bucket_sort" => array(
                        "sort" => [
                            array(
                                "player_id.value" => array(
                                    "order" => "asc"
                                )
                            )
                        ],
//                        "size" => (int)$params['limit'],
//                        "from" => (int)$params['offset']
                    )
                )
            )
        );
        return $reports;
    }

    public function totalDepositBonusClaimed()
    {
        $total_deposit_bonus_claimed = [
            "filter" => array(
                "bool" => array(
                    "filter" => [
                        array(
                            "term" => array(
                                "transaction_type" => "deposit_bonus_claim"
                            )
                        ),
                        array(
                            "term" => array(
                                "status" => "success"
                            )
                        )
                    ]
                )
            ),
            "aggs" => array(
                "total" => array(
                    "sum" => array(
                        "field" => "amount_in_currencies.EUR"
                    )
                )
            )
        ];
        return $total_deposit_bonus_claimed;
    }

    public function totalNonCashBonusClaimed()
    {
        $total_non_cash_bonus_claimed = [
            "filter" => array(
                "term" => array(
                    "transaction_type" => "non_cash_bonus_claim"
                )
            ),
            "aggs" => array(
                "total" => array(
                    "sum" => array(
                        "field" => "amount_in_currencies.EUR"
                    )
                )
            )
        ];

        return $total_non_cash_bonus_claimed;
    }

    public function totalNonCashWithdrawByAdmin()
    {
        $total_non_cash_withdraw_by_admin = [
            "filter" => array(
                "term" => array(
                    "transaction_type" => "non_cash_withdraw_by_admin"
                )
            ),
            "aggs" => array(
                "total" => array(
                    "sum" => array(
                        "field" => "amount_in_currencies.EUR"
                    )
                )
            )
        ];

        return $total_non_cash_withdraw_by_admin;

    }

    public function totalNonCashPlayerCommission()
    {
        $total_non_cash_withdraw_by_admin = [
            "filter" => array(
                "term" => array(
                    "transaction_type" => "commission_received"
                )
            ),
            "aggs" => array(
                "total" => array(
                    "sum" => array(
                        "field" => "amount_in_currencies.EUR"
                    )
                )
            )
        ];

        return $total_non_cash_withdraw_by_admin;

    }

    public function totalRcordCount()
    {

        $total_record_count = [
            "cardinality" => [
                "field" => "player_details.player_id",
                "precision_threshold" => 40000
            ]
        ];

        return $total_record_count;
    }

    public function total_non_cash_granted_by_admin()
    {
        $total_non_cash_granted_by_admin = [
            "filter" => array(
                "term" => array(
                    "transaction_type" => "non_cash_granted_by_admin"
                )
            ),
            "aggs" => array(
                "total" => array(
                    "sum" => array(
                        "field" => "amount_in_currencies.EUR"
                    )
                )
            )

        ];


        return $total_non_cash_granted_by_admin;

    }


}
