<?php

namespace App\Services;

use App\Models\PlayerCommission;
use App\Models\TenantConfigurations;
use App\Models\UserSettings;
use Carbon\Carbon;
use Elasticsearch\ClientBuilder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\TenantCredentials;

class PlayerCommissionService
{
    public static function index()
    {
        try {
            $tenantIds = DB::connection('read_db')->table('tenants')->where('active', true)->pluck('id'); // fetching active tenants
            foreach ($tenantIds as $id) {
                // checking permissions for all tenants
                $params = new \stdClass();
                $params->module = 'players_commission';
                $params->action = 'R';
                $params->tenant_id = $id;
                $permission = checkTenantPermissions($params);
                if (!$permission) {
                    continue;
                }

                $tenantInterval = TenantConfigurations::on('read_db')->where('tenant_id', $id)->select('player_commission_interval')->first();
                $find_tenant_base_currency = TenantCredentials::on('read_db')->where(['key'=>'TENANT_BASE_CURRENCY','tenant_id'=>$id])->first();
                $tenant_base_currency = '';
                if(isset($find_tenant_base_currency)){
                    $tenant_base_currency = $find_tenant_base_currency['value'];
                }
                $time_period = [];
                if ($tenantInterval->player_commission_interval){
                    if ($tenantInterval->player_commission_interval === 'daily') {
                        $time_period['fromdate'] = Carbon::today()->subDay()->toISOString();
                        $time_period['enddate'] = Carbon::today()->endOfDay()->subDay()->toISOString(); 
                    }
    
                    if ($tenantInterval->player_commission_interval === 'weekly') {
                        $time_period['fromdate'] = Carbon::now()->startOfWeek()->toISOString();
                        $time_period['enddate'] = Carbon::today()->endOfWeek()->toISOString(); 
                    }
    
                    if ($tenantInterval->player_commission_interval === 'monthly') {
                        $time_period['fromdate'] = Carbon::now()->startOfMonth()->toISOString();
                        $time_period['enddate'] = Carbon::today()->endOfMonth()->toISOString(); 
                    }
                }
                else {
                    $time_period['fromdate'] = Carbon::today()->toISOString();
                    $time_period['enddate'] = Carbon::today()->endOfDay()->toISOString(); 
                }
                $input = [
                    'limit' => 'all',
                    'offset' => 'all',
                    'tenant_id' => $id,
                    'parent_id' => '',
                    'searchKeyword' => '',
                    'sortBy' => 'created_at',
                    'sortOrder' => 'DESC',
                    'isDirectPlayer' => 'all',
                    'time_period' => $time_period,
                    "game_type" => '',
                    "game_provider" => '',
                    "table_id" => '',
                    "tenant_base_currency" => $tenant_base_currency
                ];
                $result = PlayerRevenueReportService::fetchResult($input);
                $data = [];
                if (!empty($result['aggregations']['reports']['buckets'])) {
                    foreach ($result['aggregations']['reports']['buckets'] as $index => &$value) {
                        $player = $value['top']['hits']['hits'][0]['_source']['player_details'];
                        if($player) {
                            $commissionPercentage = UserSettings::on('read_db')->where('user_id', $player['player_id'])->where('key', 'commissionPercentage')->select('value')->first();
                            $value['top']['hits']['hits'][0]['_source']['player_details']['commission_percentage'] = $commissionPercentage ? (float)$commissionPercentage->value : 0;
                            if ((float)$value['ngr']['value'] < 0) {
                                $value['top']['hits']['hits'][0]['_source']['player_details']['commission_amount'] = 0;
                            } else {
                                $value['top']['hits']['hits'][0]['_source']['player_details']['commission_amount'] = (float)$value['ngr']['value'] ? (((float)$value['ngr']['value'] * $value['top']['hits']['hits'][0]['_source']['player_details']['commission_percentage']) / 100) : 0;
                            }
                            if($value['ngr']['value'] > 0 && $value['top']['hits']['hits'][0]['_source']['player_details']['commission_percentage'] > 0)
                            {
                                    $checkplayer = PlayerCommission::on('read_db')->where('user_id', $player['player_id'])->whereBetween('created_at', [$time_period['fromdate'], $time_period['enddate']])->first();
                                    $getweekId = null;
                                    $getmonthId = null;
                                    if($tenantInterval->player_commission_interval === 'weekly'){
                                        $getweekId  = DB::connection('read_db')->table('week_details')->whereDate('start_date', $time_period['fromdate'])->whereDate('end_date', $time_period['enddate'])->select('id')->first();
                                    }
                                    if($tenantInterval->player_commission_interval === 'monthly'){
                                        $getmonthId  = DB::connection('read_db')->table('month_details')->whereDate('start_date', $time_period['fromdate'])->whereDate('end_date', $time_period['enddate'])->select('id')->first();
                                    }
                                    if(!$checkplayer){
                                        $interval = $tenantInterval->player_commission_interval ?? 'daily';
                                        $data[$index] = [
                                            'ngr' => round($value['ngr']['value'], 2) ,
                                            'commission_percentage' => $value['top']['hits']['hits'][0]['_source']['player_details']['commission_percentage'],
                                            'commission_amount' => $value['top']['hits']['hits'][0]['_source']['player_details']['commission_amount'],
                                            'user_id' => $player['player_id'],
                                            'week_id' => $getweekId->id ?? null,
                                            'actioned_at' => $interval === 'daily' ? $time_period['fromdate'] = Carbon::today()->subDay()->toISOString() : null,
                                            'month_id' => $getmonthId->id ?? null,
                                            'created_at' => now(),
                                            'interval_type' => $interval,
                                            'updated_at' => now()
                                        ];
                                    }
                            }
                        }
                    }
                    if($data){
                        $insert = collect($data);
                        $chunks = $insert->chunk(1000);
                        foreach ($chunks as $chunk)
                        {
                            PlayerCommission::insert($chunk->toArray());
                        }
                        // PlayerCommission::insert($data);
                    }
                }
            }
        } catch (\Exception $e) {
            Log::error('An error occurred: ' . $e->getMessage());
        }
        return;
    }
}
