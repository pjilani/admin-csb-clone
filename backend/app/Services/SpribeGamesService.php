<?php

namespace App\Services;

use App\Models\Casino\CasinoGames;
use App\Models\Casino\CasinoItems;
use App\Models\Casino\CasinoMenuItem;
use App\Models\Casino\CasinoMenus;
use App\Models\Casino\CasinoPages;
use App\Models\Casino\CasinoPagesMenus;
use App\Models\Casino\CasinoProviders;
use App\Models\Casino\CasinoTables;
use App\Models\Menu\MenuTenantSettingModel;
use App\Models\MenuMaster;
use Illuminate\Support\Facades\DB;

class SpribeGamesService
{
    public static function index()
    {
        try {
            DB::beginTransaction();

            $gameProvider = 'Spribe';
            $tenantId = [28];
            $casinoPageMenuName = ['Aviator', 'Turbo'];
            $gameData = [
                ['game_name' => 'Aviator', 'game_identificator' => 'aviator'],
                ['game_name' => 'Dice', 'game_identificator' => 'dice'],
                ['game_name' => 'Goal', 'game_identificator' => 'goal'],
                ['game_name' => 'Plinko', 'game_identificator' => 'plinko'],
                ['game_name' => 'Mines', 'game_identificator' => 'mines'],
                ['game_name' => 'Hi Lo', 'game_identificator' => 'hi-lo'],
                ['game_name' => 'Keno', 'game_identificator' => 'keno'],
                ['game_name' => 'Mini Roulette', 'game_identificator' => 'mini-roulette'],
                ['game_name' => 'Hotline', 'game_identificator' => 'hotline'],
                // ['game_name' => 'Poker', 'game_identificator' => 'poker'],
                // ['game_name' => 'Keno 80', 'game_identificator' => 'multikeno'],
            ];

            $stageImages = [
                'aviator'  => 'https://spribe.co/assets/images/games/Av@2x.png',
                'dice' => 'https://spribe.co/assets/images/games/Di@2x.png',
                'goal' => 'https://spribe.co/assets/images/games/Go@2x.png',
                'plinko' => 'https://spribe.co/assets/images/games/Pl@2x.png',
                'mines' => 'https://spribe.co/assets/images/games/Mi@2x.png',
                'hi-lo' => 'https://spribe.co/assets/images/games/Hi@2x.png',
                'keno' => 'https://spribe.co/assets/images/games/Ke@2x.png',
                'mini-roulette' => 'https://spribe.co/assets/images/games/Mr@2x.png',
                'hotline' => 'https://spribe.co/assets/images/games/Ho.png',
                // 'poker' => 'https://spribe.co/assets/images/games/game-poker-wide.png?v=2.5.17',
                // 'multikeno' => 'https://spribe.co/assets/images/games/MKe@2x.png',
            ];

            $data = CasinoProviders::firstOrCreate(['name' => $gameProvider]);

            foreach ($gameData as $value) {
                $game = CasinoGames::on('read_db')->where('casino_provider_id', $data->id)->where('name', $value['game_name'])->first();
                if (!$game) {
                    $newGame = CasinoGames::create([
                        'name' => $value['game_name'],
                        'casino_provider_id' => $data->id,
                        'game_id' => $value['game_identificator']
                    ]);
                    $table = CasinoTables::on('read_db')->where('game_id', $newGame->game_id)->where('table_id', $newGame->game_id)->first();
                    if (!$table) {
                        $table = CasinoTables::create([
                            'name' => strtoupper($newGame->name), 'game_id' => $newGame->game_id, 'table_id' => $newGame->game_id, 'is_lobby' => true
                        ]);
                    } //super
                    foreach ($tenantId as $id) {
                        $casinoItem = CasinoItems::where('uuid', $table->table_id)->where('tenant_id', $id)->first();
                        if (!$casinoItem) {
                            CasinoItems::create([
                                'name'  => $table->name,
                                'active'  => true,
                                'uuid'  => $table->table_id,
                                'provider' => $data->id,
                                'tenant_id' => $id,
                                'image' => $stageImages[$table->game_id]
                            ]);
                        }
                    }
                }
            }

            $menuMaster = MenuMaster::where('name', 'Crash')->first();
            if (!$menuMaster) {
                $menuMaster = MenuMaster::create([
                    'name' => 'Crash',
                    'path' => '/crash',
                    'active' => true,
                    'component' => 'Casino',
                    'component_name' => 'Crash',
                    'created_at' => now(),
                    'updated_at' => now(),
                    'image' => 'https://ezugi-main-platform-staging-storage.s3.amazonaws.com/menus%2F8%2Flogo%2F7d0fd399-faf0-4d12-a808-e6217552bda8____crash.svg'
                ]);
            }

            foreach ($tenantId as $id) {
                $menuTenantSetting = MenuTenantSettingModel::where('tenant_id', $id)->where('menu_id', $menuMaster->id)->first();
                if (!$menuTenantSetting) {
                    $menuTenantSetting = MenuTenantSettingModel::create([
                        'menu_id' => $menuMaster->id,
                        'tenant_id' => $id,
                        // 'ordering' => ,
                        'created_at' => now(),
                        'updated_at' => now()
                    ]);
                }

                $casinoPages = CasinoPages::where('tenant_id', $id)->where('top_menu_id', $menuTenantSetting->id)->where('title', 'Spribe')->first();
                if (!$casinoPages) {
                    $casinoPages = CasinoPages::create([
                        'title' => 'Spribe',
                        'created_at' => now(),
                        'image' => 'https://ezugi-main-platform-staging-storage.s3.amazonaws.com/pages%2F47%2Flogo%2Ff8eafa7a-7471-49f9-aca4-e11a08d16954____spribe.png',
                        'updated_at' => now(),
                        'enabled' => true,
                        // 'order',
                        'tenant_id' => $id,
                        'top_menu_id' => $menuTenantSetting->id,
                        // 'enable_instant_game'
                    ]);
                }

                $casinoPageMenu = [];
                foreach ($casinoPageMenuName as $name) {
                    $casinoPageMenu[$name] = CasinoPagesMenus::where('page_id', $casinoPages->id)->where('name', $name)->first();
                    if (!$casinoPageMenu[$name]) {
                        $casinoMenu = CasinoMenus::where('name', $name)->where('tenant_id', $id)->first();
                        if(!$casinoMenu){
                            $casinoMenu = CasinoMenus::create([
                                'name' => $name,
                                // 'menu_type',
                                // 'menu_order',
                                'enabled' => true,
                                'created_at' => now(),
                                'updated_at'  => now(),
                                'tenant_id' => $id,
                                'image_url' => 'https://ezugi-main-platform-staging-storage.s3.amazonaws.com/tenants%2F1%2Fmenus%2Fdefd9818-2dc9-40da-a167-ca4bd4e70369____menu.png'
                            ]);
                        }
                        $casinoPageMenu[$name]  = CasinoPagesMenus::create([
                            'name' => $name,
                            'casino_menu_id' => $casinoMenu->id,
                            'page_id' => $casinoPages->id,
                            // 'menu_order'
                        ]);

                    }
                    
                    
                    if($name == 'Turbo'){
                        foreach($gameData as $game){
                            if($game['game_identificator'] == 'aviator') continue;
                            $casinoItem =  CasinoItems::where('uuid', $game['game_identificator'])->where('tenant_id', $id)->first();
                            $casinoItemExistTurbo = CasinoMenuItem::where('name', $game['game_identificator'])->where('page_menu_id',$casinoPageMenu[$name]->id)->where('casino_item_id',$casinoItem->id)->first(); 
                            if(!$casinoItemExistTurbo){
                                $casinoItem = CasinoMenuItem::create([
                                    'name' => $game['game_identificator'],
                                    'page_id' => $casinoPages->id,
                                    'page_menu_id' => $casinoPageMenu[$name]->id,
                                    'casino_item_id' => $casinoItem->id,
                                    'active' => true,
                                    // 'featured' => $request->featured
                                ]);
                            }
                        }
                    }else{
                        $casinoItem = CasinoItems::where('name', strtoupper($name))->where('tenant_id', $id)->first();
                        $casinoItemExist = CasinoMenuItem::where('name', strtolower($name))->where('page_menu_id',$casinoPageMenu[$name]->id)->where('casino_item_id',$casinoItem->id)->first(); 
                        if(!$casinoItemExist){
                            $casinoItem = CasinoMenuItem::create([
                                'name' => strtolower($name),
                                'page_id' => $casinoPages->id,
                                'page_menu_id' => $casinoPageMenu[$name]->id,
                                'casino_item_id' => $casinoItem->id,
                                'active' => true,
                                // 'featured' => $request->featured
                            ]);
                        }
                    }
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
        }
        return;
    }
}
