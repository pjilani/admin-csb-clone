<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\FaqCategory;
use Illuminate\Support\Facades\Auth;

class UniqueSlug implements Rule
{
    protected $ignoreId;

    public function __construct($ignoreId = null)
    {
        $this->ignoreId = $ignoreId;
    }

    public function passes($attribute, $value)
    {
        $query = FaqCategory::where('slug', $value)
            ->where('tenant_id', Auth::user()->tenant_id);

        if ($this->ignoreId) {
            $query->where('id', '!=', $this->ignoreId);
        }

        $slugCheck = $query->exists();

        return !$slugCheck;
    }

    public function message()
    {
        return 'The slug has already been taken.';
    }
}