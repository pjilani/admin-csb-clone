<?php

namespace App\Console\Commands;

use App\Services\GgrReportService;
use Illuminate\Console\Command;

class monthWiseGgr extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'month-wise:ggr';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get month wise ggr report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        GgrReportService::index();
        $this->info('Ggr report seeding complete.');
        return Command::SUCCESS;
    }
}