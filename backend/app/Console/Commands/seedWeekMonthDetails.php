<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class seedWeekMonthDetails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seed:details';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert new records in week and month details table.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            DB::beginTransaction();
            $currentYear = now()->year;
            $weekDetails = [];
            for ($week = 1; $week <= 52; $week++) {
                $startDate = Carbon::now()->setISODate($currentYear, $week)->startOfWeek();
                $endDate = $startDate->copy()->endOfWeek();
                $weekDetails[] = [
                    'week_count' => $week,
                    'start_date' => $startDate->toDateTimeString(),
                    'end_date' => $endDate->toDateTimeString(),
                    'created_at' => now(),
                    'updated_at' => now(),
                ];
            }
            DB::table('week_details')->insert($weekDetails);

            $monthDetails = [];
            for ($month = 1; $month <= 12; $month++) {
                $startDate = Carbon::create(date('Y'), $month, 1)->startOfMonth();
                $endDate = Carbon::create(date('Y'), $month,1)->endOfMonth();
                $monthDetails[] = [
                    'month_count' => $month,
                    'start_date' => $startDate->toDateTimeString(),
                    'end_date' => $endDate->toDateTimeString(),
                    'created_at' => now(),
                    'updated_at' => now(),
                ];
            }
            DB::table('month_details')->insert($monthDetails);
            DB::commit();
            $this->info('Seeding successfully done.');
        } catch (\Exception $e) {
            DB::rollBack();
            $this->error('Error executing SQL query: ' . $e->getMessage());
        }
    }
}