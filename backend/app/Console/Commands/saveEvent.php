<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\SportEventService;

class saveEvent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sport:event';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'sport Jetfair event details saving in db';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        SportEventService::updateEvent();
        $this->info('Getting Sport Event Information Complete');
        return Command::SUCCESS;
    }
}
