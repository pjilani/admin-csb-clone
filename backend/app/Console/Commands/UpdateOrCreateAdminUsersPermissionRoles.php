<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdateOrCreateAdminUsersPermissionRoles extends Command
{
    protected $signature = 'update:admin_users_permission_roles';

    protected $description = 'Update or create admin users permission roles';

    public function handle()
    {
        $tenantIds = DB::table('tenants')->pluck('id');
        // $tenantIds = [16];

        foreach ($tenantIds as $tenantId) {
            // For Agents
            $adminUserIds = DB::table('admin_users_admin_roles')
                ->join('admin_users', 'admin_users_admin_roles.admin_user_id', '=', 'admin_users.id')
                ->where('admin_users_admin_roles.admin_role_id', 2)
                ->where('admin_users.tenant_id', $tenantId)
                ->pluck('admin_user_id');

            $permissionRole = DB::table('permission_role')
                ->where('role_name', 'Agent-role')
                ->where('tenant_id', $tenantId)
                ->first();

            if ($permissionRole) {
                foreach ($adminUserIds as $adminUserId) {
                    DB::table('admin_users_permission_roles')
                        ->updateOrInsert(
                            ['admin_user_id' => $adminUserId],
                            ['permission_role_id' => $permissionRole->id]
                        );
                }
            }


            //For Sub-admin
            $adminUserIds = DB::table('admin_users_admin_roles')
                ->join('admin_users', 'admin_users_admin_roles.admin_user_id', '=', 'admin_users.id')
                ->whereIn('admin_users_admin_roles.admin_role_id', [3,4,5])
                ->where('admin_users.tenant_id', $tenantId)
                ->pluck('admin_user_id');

                
            $permissionRole = DB::table('permission_role')
            ->where('role_name', 'Sub-admin-role')
            ->where('tenant_id', $tenantId)
            ->first();
                

            if ($permissionRole) {
                foreach ($adminUserIds as $adminUserId) {
                    DB::table('admin_users_permission_roles')
                        ->updateOrInsert(
                            ['admin_user_id' => $adminUserId],
                            ['permission_role_id' => $permissionRole->id]
                        );
                }
            }
        }

        $this->info('Admin users permission roles updated or created successfully.');
    }
}
