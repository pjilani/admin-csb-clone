<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class requestResponseLogsCron extends Command
{
  protected $signature = 'copy:request-logs';

    protected $description = 'Copy request logs to request_clone table';

    public function handle()
    {
        //For weekly data transfer
        // $fromDate = Carbon::now()->startOfWeek()->toDateTimeString();
        // $toDate = Carbon::now()->subWeek()->toDateTimeString();

//        $fromDate = Carbon::now()->subMonths(2)->firstOfMonth()->toDateTimeString();
//        $toDate = Carbon::now()->subMonths(2)->endOfMonth()->toDateTimeString();
        $fromDate = '2023-10-01 00:00:00';
        $toDate = '2024-01-08 23:59:59';
        $timestamp = date('YmdHis'); // Generates a timestamp in the format YearMonthDayHourMinuteSecond
        $path = sys_get_temp_dir();
        $filename = "$path/request_response_{$timestamp}.csv";
        $query = "COPY (SELECT request_json, response_json, service, url, tenant_id, response_code, response_status, error_code, created_at, updated_at FROM request_response_log WHERE updated_at BETWEEN '$fromDate' AND '$toDate') TO '{$filename}' WITH DELIMITER ',' CSV HEADER;";
         $deleteQuery = "DELETE FROM request_response_log WHERE updated_at BETWEEN '$fromDate' AND '$toDate';";

        try {
            DB::connection('read_db')->statement($query);
            DB::connection('pgsql2')->statement("COPY request_response_logs(request_json, response_json, service, url, tenant_id, response_code, response_status, error_code, created_at, updated_at) FROM '{$filename}' DELIMITER ',' CSV HEADER;");
            DB::statement($deleteQuery);
            $this->info('Logs moved successfully.');
        }catch (\Exception $e) {
            $this->error('Error executing SQL query: ' . $e->getMessage());
        }

    }
}
