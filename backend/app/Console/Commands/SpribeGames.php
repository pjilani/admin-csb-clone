<?php

namespace App\Console\Commands;

use App\Services\SpribeGamesService;
use Illuminate\Console\Command;

class SpribeGames extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'spribe:games';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new games';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        SpribeGamesService::index();
        $this->info('Games seeding is done.');
        return Command::SUCCESS;
    }
}