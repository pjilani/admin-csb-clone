<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\UserBonus;
use \Carbon\Carbon;

class UserBonusExpire extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:bonus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'user bonus expiring update in db';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $userBonusesObj = UserBonus::where(['status' => 'active'])
            ->whereRaw('expires_at < now()');
        $userBonusesObj->update(['status' => 'expired', 'updated_at'=> date('Y-m-d H:i:s')]);
        $this->info('Getting User Bonus Information Complete');
        return Command::SUCCESS;
    }
}
