<?php

namespace App\Console\Commands;

use App\Services\TransactionsService;
use Illuminate\Console\Command;

class bulkUserTransaction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bulk:userTransaction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reindex Elasticsearch Command for user transaction in bulk';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        TransactionsService::bulkUserTransactionReIndex();
        $this->info('Reindex Elasticsearch Information\'s: Done');
        return true;
    }
}
