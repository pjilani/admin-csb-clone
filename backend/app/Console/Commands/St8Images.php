<?php

namespace App\Console\Commands;

use App\Services\St8GamesService;
use Illuminate\Console\Command;

class St8Images extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'st8:images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed st8 games images';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        St8GamesService::seedImages();
        $this->info('Image seeding is done.');
        return Command::SUCCESS;
    }
}