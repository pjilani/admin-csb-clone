<?php

namespace App\Console\Commands;

use App\Services\ExchangeRateService;
use Illuminate\Console\Command;
use GuzzleHttp\Client;

class currencyConversion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'conversion:rates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update currency conversion rates';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $guzzle_client;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ExchangeRateService::index();
        return Command::SUCCESS;
    }
}
