<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\QueueLog;
use \Carbon\Carbon;

class deleteQueueLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:queue-logs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete old queue logs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $today = Carbon::now()->toDateTimeString();
        try {
            QueueLog::where('created_at','<', $today)->where('status', 1)->delete();
            $this->info('Queue logs deleted successfully.');
        } catch (\Exception $e) {
            $this->error('Error executing SQL query: ' . $e->getMessage());
        }
    }
}
