<?php

namespace App\Console\Commands;

use App\Services\TransactionsService;
use Illuminate\Console\Command;

class bulkCasinoTransaction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bulk:casinoTransaction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reindex Elasticsearch Command for casino and wallet transaction in bulk';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        TransactionsService::bulkCasinoTransactionReIndex();
        $this->info('Reindex Elasticsearch Information\'s: Done');
        return true;
    }
}
