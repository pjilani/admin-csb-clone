<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class copyCurrencyResponse extends Command
{
    protected $signature = 'copy:currency-logs';

    protected $description = 'Copy currency logs to request_clone table';

    public function handle()
    {
        //For weekly data transfer
        // $fromDate = Carbon::now()->startOfWeek()->toDateTimeString();
        // $toDate = Carbon::now()->subWeek()->toDateTimeString();
        $fromDate = Carbon::now()->subMonths(1)->firstOfMonth()->toDateTimeString();
        $toDate = Carbon::now()->subMonths(1)->endOfMonth()->toDateTimeString();
        $timestamp = date('YmdHis'); // Generates a timestamp in the format YearMonthDayHourMinuteSecond
        $filename = "/tmp/currency_response_{$timestamp}.csv";
        $query = "COPY (SELECT response, created_at, updated_at FROM currency_response WHERE updated_at BETWEEN '$fromDate' AND '$toDate') TO '{$filename}' WITH DELIMITER ',' CSV HEADER;";
         $deleteQuery = "DELETE FROM currency_response WHERE updated_at BETWEEN '$fromDate' AND '$toDate';";

        try {
            DB::connection('read_db')->statement($query);
            DB::connection('pgsql2')->statement("COPY currency_response(response, created_at, updated_at) FROM '{$filename}' DELIMITER ',' CSV HEADER;");
            DB::statement($deleteQuery);
            $this->info('Logs moved successfully.');
        }catch (\Exception $e) {
            $this->error('Error executing SQL query: ' . $e->getMessage());
        }

}

}