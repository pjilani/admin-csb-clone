<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Services\TransactionsService;

class casinoTransaction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bet:wallet';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reindex Elasticsearch Command for casino and wallet transaction';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        TransactionsService::reIndexAll();
        $this->info('Reindex Elasticsearch Information\'s: Done');
        return true;
    }
}
