<?php

namespace App\Console\Commands;

use App\Models\Currencies;
use App\Models\TenantConfigurations;
use App\Models\Transactions;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class otherCurrencyAmount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'otherCurrency:amount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adding other currencies to transactions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        DB::table('transactions')
            ->select('id' ,'target_currency_id', 'amount', 'tenant_id', 'other_currency_amount', 'server_id')
            ->whereRAW('(transaction_type = 5 or transaction_type = 6)')
            ->whereNull('other_currency_amount')
            ->orWhere('other_currency_amount', '=', '[]')
            ->chunkById(100, function ($transactions, $errors) {
                try {
                    DB::beginTransaction();
                    foreach ($transactions as $item) {

                        $configurations = TenantConfigurations::select('allowed_currencies')->where('tenant_id', $item->tenant_id)->get();
                        $configurations = $configurations->toArray();
                        $CurrenciesValue = [];
                        if ($configurations) {

                            $configurations = $configurations[0];
                            if (strlen($configurations['allowed_currencies']) > 1) {
                                $configurations = str_replace('{', '', $configurations['allowed_currencies']);
                                $configurations = str_replace('}', '', $configurations);
                                $configurations = explode(',', $configurations);

                                if (@$configurations[0] && count($configurations) > 0) {
                                    $CurrenciesValue = Currencies::select('code')->whereIn('id', $configurations)->pluck('code');
                                    $CurrenciesValue = $CurrenciesValue->toArray();
                                }
                            }
                        }
                        $currenciesTempArray = $insertData['other_currency_amount'] = [];
                        $exMainTransactionCurrecy = Currencies::select('code')->where('id', $item->target_currency_id)->pluck('code');
                        $exMainTransactionCurrecy = $exMainTransactionCurrecy->toArray();
                        $amount_currency_ex = Currencies::getExchangeRateCurrency($exMainTransactionCurrecy);
                        foreach ($CurrenciesValue as $key => $value) {
                            if ($exMainTransactionCurrecy != $value) {
                                $getExchangeRateCurrency = Currencies::getExchangeRateCurrency($value);
                                $currenciesTempArray[$value] = (float)number_format((float)$item->amount * ($amount_currency_ex / $getExchangeRateCurrency), 4, ".", "");
                            } else {
                                $currenciesTempArray[$value] = (float)$item->amount;
                            }
                        }
                        DB::table('transactions')
                            ->where('id', $item->id)
                            ->update([
                                'other_currency_amount' => json_encode($currenciesTempArray),
                                'server_id' => 1,
                            ]);

                    }
                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    $errors[] = ['transaction_id' => $item->id, 'error' => $e->getMessage()];
                }
            });
        return Command::SUCCESS;
    }
}
