<?php

namespace App\Console\Commands;

use App\Services\PlayerCommissionService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class playerCommission extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'player:commission';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert in player commission';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        PlayerCommissionService::index();
        $this->info('Insertion in player commission complete.');
        return Command::SUCCESS;
    }
}
