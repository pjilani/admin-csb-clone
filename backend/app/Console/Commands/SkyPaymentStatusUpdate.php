<?php

namespace App\Console\Commands;

use App\Services\SkyPaymentService;
use Illuminate\Console\Command;

class SkyPaymentStatusUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sky:payment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updating status of transactions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        SkyPaymentService::index();
        $this->info('Transaction status updated successsfully.');
        return Command::SUCCESS;
    }
}
